﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using CraveBuilder.Configuration;
using Dionysos;
using Dionysos.IO;
using Obymobi.Logging;

namespace CraveBuilder
{
	public class AndroidBuilder
	{
		#region Consts

		private const string BUILDS_DIR = "D:\\Builds";

		const string EMENU_FOLDER_DROP_BOX = "Crave eMenu";
		const string AGENT_FOLDER_DROP_BOX = "CraveAgent";
		const string SUPPORTTOOLS_FOLDER_DROP_BOX = "CraveSupportTools";

		#endregion

		#region Fields

		private readonly RichTextBox tbOutput;

		private readonly string dropBoxBaseDir;

		private string ApiVersion { get; set; }
		private string ApkVersion { get; set; }
		private bool PublishToDropBox { get; set; }

		private string ReleaseDirLocal { get; set; }
		private string ReleaseDirDropBox { get; set; }

		public bool IsBuilding { get; private set; }
		public bool IsBuildComplete { get; private set; }

		private Process antProcess;

		#endregion

        #region Properties


        public string AndroidBaseDir { get; set; }

        private string EmenuBaseDir 
        {
            get { return AndroidBaseDir + "\\CraveDemo"; }
        }

        private string AgentBaseDir
        {
            get { return AndroidBaseDir + "\\CraveAgent"; }
        }

        private string SupportToolsBaseDir
        {
            get { return AndroidBaseDir + "\\CraveSupportTools"; }
        }

        private string ToolsBaseDir
        {
            get { return AndroidBaseDir + "\\Tools"; }
        }

        #endregion

        #region Events

        public delegate void BuildCompletedHandler();
		public event BuildCompletedHandler BuildCompletedEvent;
		private void OnBuildCompletedEvent()
		{
			if (BuildCompletedEvent != null) BuildCompletedEvent();
		}

		#endregion

		#region Constructor

		public AndroidBuilder(RichTextBox outputBox)
		{
			this.tbOutput = outputBox;

			this.dropBoxBaseDir = ConfigurationManager.GetString(CraveBuilderConfigConstants.DropBoxDir);
			if (!this.dropBoxBaseDir.IsNullOrWhiteSpace() && !Directory.Exists(this.dropBoxBaseDir))
			{
				this.WriteToLog("*** WARNING: DROPBOX DIRECTORY '{0}' DOES NOT EXIST", this.dropBoxBaseDir);
				MessageBox.Show(string.Format("DROPBOX DIRECTORY '{0}' DOES NOT EXIST", this.dropBoxBaseDir));
			}
		}

		#endregion

		#region Public Methods (Building)

		public void BuildCraveEmenu(string apiVersion, string apkVersion, bool publishToDropBox)
		{
			this.WriteToLog("*** Starting Emenu Build ***");

			this.IsBuildComplete = false;
			this.ApiVersion = apiVersion;
			this.ApkVersion = apkVersion;
			this.PublishToDropBox = publishToDropBox;

			this.ReleaseDirLocal = string.Format("{0}\\CraveEmenu\\1.20{1}", BUILDS_DIR, apkVersion);
			this.ReleaseDirDropBox = string.Format("{0}\\{1}", this.dropBoxBaseDir, EMENU_FOLDER_DROP_BOX);

			if (!Directory.Exists(ReleaseDirDropBox))
			{
				this.WriteToLog("*** WARNING: DROPBOX RELEASE DIRECTORY '{0}' DOES NOT EXIST", this.ReleaseDirDropBox);
				MessageBox.Show(string.Format("DROPBOX RELEASE DIRECTORY '{0}' DOES NOT EXIST", this.ReleaseDirDropBox));

				BuildComplete();
			}
			else
			{
				// Finally we add the version folder (will be created when the build is actually copied)
				this.ReleaseDirDropBox = string.Format("{0}\\1.20{1}", this.ReleaseDirDropBox, apkVersion);

				// Execute the beast
				string cmdArgs = string.Format("-Dconfig.version={0} -Dconfig.api={1} crave-build", this.ApkVersion, this.ApiVersion);
                RunAnt(cmdArgs, EmenuBaseDir);
			}
		}

		public void BuildCraveAgent(string apiVersion, string apkVersion, bool publishToDropBox)
		{
			this.WriteToLog("*** Starting CraveAgent Build ***");

			this.IsBuildComplete = false;
			this.ApiVersion = apiVersion;
			this.ApkVersion = apkVersion;
			this.PublishToDropBox = publishToDropBox;

			this.ReleaseDirLocal = string.Format("{0}\\CraveAgent\\1.20{1}", BUILDS_DIR, apkVersion);
			this.ReleaseDirDropBox = string.Format("{0}\\{1}", this.dropBoxBaseDir, AGENT_FOLDER_DROP_BOX);

			if (!Directory.Exists(ReleaseDirDropBox))
			{
				this.WriteToLog("*** WARNING: DROPBOX RELEASE DIRECTORY '{0}' DOES NOT EXIST", this.ReleaseDirDropBox);
				MessageBox.Show(string.Format("DROPBOX RELEASE DIRECTORY '{0}' DOES NOT EXIST", this.ReleaseDirDropBox));

				BuildComplete();
			}
			else
			{
				// Finally we add the version folder (will be created when the build is actually copied)
				this.ReleaseDirDropBox = string.Format("{0}\\1.20{1}", this.ReleaseDirDropBox, apkVersion);

				// Execute the beast
				string cmdArgs = string.Format("-Dconfig.version={0} -Dconfig.api={1} crave-build", this.ApkVersion, this.ApiVersion);
                RunAnt(cmdArgs, AgentBaseDir);
			}
		}

		public void BuildCraveSupportTools(string apiVersion, string apkVersion, bool publishToDropBox)
		{
			this.WriteToLog("*** Starting CraveAgent Build ***");

			this.IsBuildComplete = false;
			this.ApiVersion = apiVersion;
			this.ApkVersion = apkVersion;
			this.PublishToDropBox = publishToDropBox;

			this.ReleaseDirLocal = string.Format("{0}\\CraveSupportTools\\1.20{1}", BUILDS_DIR, apkVersion);
			this.ReleaseDirDropBox = string.Format("{0}\\{1}", this.dropBoxBaseDir, SUPPORTTOOLS_FOLDER_DROP_BOX);

			if (!Directory.Exists(ReleaseDirDropBox))
			{
				this.WriteToLog("*** WARNING: DROPBOX RELEASE DIRECTORY '{0}' DOES NOT EXIST", this.ReleaseDirDropBox);
				MessageBox.Show(string.Format("DROPBOX RELEASE DIRECTORY '{0}' DOES NOT EXIST", this.ReleaseDirDropBox));

				BuildComplete();
			}
			else
			{
				// Finally we add the version folder (will be created when the build is actually copied)
				this.ReleaseDirDropBox = string.Format("{0}\\1.20{1}", this.ReleaseDirDropBox, apkVersion);

				// Execute the beast
				string cmdArgs = string.Format("-Dconfig.version={0} -Dconfig.api={1} crave-build", this.ApkVersion, this.ApiVersion);
				RunAnt(cmdArgs, SupportToolsBaseDir);
			}
		}

		public void StopBuild()
		{
			if (this.antProcess != null && !this.antProcess.HasExited)
			{
				Process[] currentProcesses = Process.GetProcesses();
				foreach (Process p in currentProcesses)
				{
					string s = p.ProcessName;
					s = s.ToLower();
					if (String.Compare(s, "aapt", StringComparison.Ordinal) == 0)
					{
						p.Kill();
						p.Close();
					}
				}

				this.antProcess.Kill();
				this.antProcess.Close();

				BuildCleanUp();

				this.WriteToLog("*** BUILD STOPPED BY USER ***");
			}
		}

		#endregion

		#region Private Methods (Building)

        private void SetCraveSourceDirectory()
        {
            var buildFilePath = Path.Combine(ToolsBaseDir, "crave-build.xml");
            var buildFile = File.ReadAllLines(buildFilePath);
            for (int i = 0; i < buildFile.Length; i++)
            {
                var line = buildFile[i];
                if (line.Contains("name=\"crave.source\""))
                {
                    buildFile[i] = string.Format("<property name=\"crave.source\" value=\"{0}\" />", AndroidBaseDir);
                    break;
                }
            }
            File.WriteAllLines(buildFilePath, buildFile);
        }

		private void RunAnt(string arguments, string workingDirectory)
		{
            SetCraveSourceDirectory();
		    
			this.WriteToLog("Executing command:");
			this.WriteToLog("'ant {0}'", arguments);

			this.IsBuilding = true;

			Task.Factory.StartNew(() =>
				{
					var startInfo = new ProcessStartInfo();
					startInfo.FileName = "cmd";
					startInfo.Arguments = "/C ant " + arguments;
					startInfo.WorkingDirectory = workingDirectory;
					startInfo.UseShellExecute = false;
					startInfo.RedirectStandardOutput = true;
					startInfo.CreateNoWindow = true;

					try
					{
						// Start the process with the info we specified.
						// Call WaitForExit and then the using statement will close.
						using (this.antProcess = Process.Start(startInfo))
						{
							using (StreamReader reader = this.antProcess.StandardOutput)
							{
								while (!this.antProcess.HasExited)
								{
									string str = reader.ReadLine();
									if (this.tbOutput != null)
									{
										this.tbOutput.Invoke((MethodInvoker)delegate
											{
												this.tbOutput.Text += str + "\r\n";
											});
									}
								}
							}
						}

						this.IsBuildComplete = true;
					}
					catch (Exception ex)
					{
						this.WriteToLog(ex.Message);
					}

					BuildComplete();
				});
		}

		private void BuildComplete()
		{
			if (this.IsBuildComplete && this.PublishToDropBox)
			{
				try
				{
					this.WriteToLog("Copying build to DropBox.");
					this.WriteToLog("'{0}', '{1}'", this.ReleaseDirLocal, this.ReleaseDirDropBox);
					FileUtil.CopyDirectory(this.ReleaseDirLocal, this.ReleaseDirDropBox);
					this.WriteToLog("Build published on DropBox");
				}
				catch (DirectoryNotFoundException ex)
				{
					this.WriteToLog("ERROR: {0}", ex.Message);
				}
			}

			BuildCleanUp();

			// Dispatch event
			this.OnBuildCompletedEvent();

			if (this.IsBuildComplete)
				this.WriteToLog("*** Finished Build ***");
			else
				this.WriteToLog("*** BUILD FAILED ***");
		}

		private void BuildCleanUp()
		{
			this.WriteToLog("Cleaning up set variables...");

			this.antProcess = null;
			this.IsBuilding = false;

			this.ApiVersion = "";
			this.ApkVersion = "";
			this.PublishToDropBox = false;
			this.ReleaseDirDropBox = "";
			this.ReleaseDirLocal = "";
		}
		#endregion

		#region Private Methods

		/// <summary>
		/// Writes the specified contents to the log file
		/// </summary>
		/// <param name="contents">The contents to write to the logfile</param>
		/// <param name="args"></param>
		private void WriteToLog(string contents, params object[] args)
		{
			// Write to file
			DesktopLogger.Debug(contents, args);

			if (this.tbOutput != null)
			{
				if (this.tbOutput.InvokeRequired)
				{
					this.tbOutput.BeginInvoke((MethodInvoker)(() => this.WriteToLog(contents, args)));
				}
				else
				{
					this.tbOutput.AppendText(DateTime.Now.ToShortTimeString() + " - " + string.Format(contents, args) + "\r\n");
				}
			}
		}

		#endregion
	}
}
