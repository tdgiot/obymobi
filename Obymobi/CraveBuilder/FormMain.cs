﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CraveBuilder.Configuration;
using Dionysos;
using Dionysos.Web;
using Obymobi.Constants;
using Obymobi.Logging;
using MessageBox = System.Windows.Forms.MessageBox;

namespace CraveBuilder
{
	public partial class FormMain : Form
	{
		#region Fields

		private Builder builder = null;
		private List<Application> applications = new List<Application>();

		private const string BASE_URL_DEV = "http://dev.crave-emenu.com";
		private const string BASE_URL_TEST = "http://test.crave-emenu.com";
		private const string BASE_URL_LIVE = "https://app.crave-emenu.com";

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FormMain"/> class.
		/// </summary>
		public FormMain()
		{
			InitializeComponent();

			// Set the title
			this.Text += " - " + Dionysos.Global.ApplicationInfo.ApplicationVersion;

			this.builder = new Builder(this.tbLog);

            // Get the available folders
            var dotnetDirectories = Directory.GetDirectories("d:\\development\\", "dotnet*").ToList();
            dotnetDirectories.Insert(0,string.Empty);
            this.cbDotnetFolder.DataSource = dotnetDirectories;            

			// Hookup the events
			this.HookupEvents();
		}

		#endregion

		#region Methods

		private void GetCurrentVersions()
		{
            if (Constants.DotNetPath.IsNullOrWhiteSpace())
            {
                // No folder selected
                this.lblLastVersionCraveCloud.Text = string.Empty;
                this.lblLastVersionCraveOnSiteServer.Text = string.Empty;
                this.lblLastVersionCraveOnSiteServerInstaller.Text = string.Empty;
                this.lblLastVersionCraveOnSiteAgent.Text = string.Empty;
                this.lblLastVersionCraveOnSiteAgentInstaller.Text = string.Empty;
            }
            else
            {
                string version;

                // Obymobi Webserivce
                this.lblLastVersionCraveCloud.Text = this.builder.GetLatestVersion(this[Constants.ObymobiWebservice], true);
                if (this.builder.GetCurrentApplicationVersion(this[Constants.ObymobiWebservice], out version))
                    this.lblSourceVersionCraveCloud.Text = version;

                // Crave On-site Server
                this.lblLastVersionCraveOnSiteServer.Text = this.builder.GetLatestVersion(this[Constants.CraveOnsiteServer], true);
                this.lblLastVersionCraveOnSiteServerInstaller.Text = this.builder.GetLatestVersion(this[Constants.CraveOnsiteServer], false);
                if (this.builder.GetCurrentApplicationVersion(this[Constants.CraveOnsiteServer], out version))
                    this.lblSourceVersionOnsiteServer.Text = version;

                // Crave On-site Agent
                this.lblLastVersionCraveOnSiteAgent.Text = this.builder.GetLatestVersion(this[Constants.CraveOnsiteAgent], true);
                this.lblLastVersionCraveOnSiteAgentInstaller.Text = this.builder.GetLatestVersion(this[Constants.CraveOnsiteAgent], false);
                if (this.builder.GetCurrentApplicationVersion(this[Constants.CraveOnsiteAgent], out version))
                    this.lblSourceVersionOnsiteAgent.Text = version;

                // Crave On-site SupportTools
                this.lblLastVersionCraveOnSiteSupportTools.Text = this.builder.GetLatestVersion(this[Constants.CraveOnsiteSupportTools], true);
                this.lblLastVersionCraveOnSiteSupportToolsInstaller.Text = this.builder.GetLatestVersion(this[Constants.CraveOnsiteSupportTools], false);
                if (this.builder.GetCurrentApplicationVersion(this[Constants.CraveOnsiteSupportTools], out version))
                    this.lblSourceVersionOnsiteSupportTools.Text = version;
            }
		}

		private void HookupEvents()
		{
			// Version
			this.btnUpdateCraveOnsiteServerVersion.Click += btnUpdateCraveOnsiteServerVersion_Click;
			this.btnUpdateCraveOnsiteAgentVersion.Click += btnUpdateCraveOnsiteAgentVersion_Click;
            this.btnUpdateCraveOnsiteSupportToolsVersion.Click += btnUpdateCraveOnsiteSupportToolsVersion_Click;
			this.btnUpdateAllSoftwareVersion.Click += btnUpdateAllSoftwareVersion_Click;

			// Compile
			this.llOpenObymobiSolution.Click += llOpenObymobiSolution_Click;

            this.cbReleasesSelectAll.CheckStateChanged += ReleaseSoftwareAllCheckStateChanged;
		    this.cbCraveCloud.CheckStateChanged += ReleaseSoftwareCheckStateChanged;
		    this.cbCraveOnSiteAgent.CheckStateChanged += ReleaseSoftwareCheckStateChanged;
		    this.cbCraveOnSiteServer.CheckStateChanged += ReleaseSoftwareCheckStateChanged;
            this.cbCraveOnSiteSupportTools.CheckStateChanged += ReleaseSoftwareCheckStateChanged;

			this.btnCreateReleasesForSelectedSoftware.Click += btnCreateReleasesForSelectedSoftware_Click;
			this.cbObfuscateAllSoftware.CheckedChanged += cbObfuscateAllSoftware_CheckedChanged;
			this.cbCreateInstallersForAllSoftware.CheckedChanged += cbCreateInstallersForAllSoftware_CheckedChanged;
			this.cbPublishAllSoftware.CheckedChanged += cbPublishAllSoftware_CheckedChanged;

			// Sync database
			this.llOpenSQLDelta.Click += llOpenSQLDelta_Click;

			// Remote desktop
			this.llConnectToPrimaryWebserver.Click += llConnectToPrimaryWebserver_Click;
			this.llConnectToSecondaryWebserver.Click += llConnectToSecondaryWebserver_Click;

			// Test
			this.llOpenWebserviceDev.Click += llOpenWebserviceDev_Click;
			this.llOpenCmsDev.Click += llOpenCmsDev_Click;
			this.llOpenConsoleDev.Click += llOpenConsoleDev_Click;
			this.llOpenMobileDev.Click += llOpenMobileDev_Click;
			this.llOpenNocDev.Click += llOpenNocDev_Click;
			this.llOpenWebserviceTest.Click += llOpenWebserviceTest_Click;
			this.llOpenCmsTest.Click += llOpenCmsTest_Click;
			this.llOpenConsoleTest.Click += llOpenConsoleTest_Click;
			this.llOpenMobileTest.Click += llOpenMobileTest_Click;
			this.llOpenNocTest.Click += llOpenNocTest_Click;
			this.llOpenWebserviceLive.Click += llOpenWebserviceLive_Click;
			this.llOpenCmsLive.Click += llOpenCmsLive_Click;
			this.llOpenConsoleLive.Click += llOpenConsoleLive_Click;
			this.llOpenMobileLive.Click += llOpenMobileLive_Click;
			this.llOpenNocLive.Click += llOpenNocLive_Click;

			// Pivotal
			this.llGoToPivotal.Click += llGoToPivotal_Click;

			// Menu
			this.buildAllToolStripMenuItem.Click += buildAllToolStripMenuItem_Click;
			this.androidToolStripMenuItem.Click += androidToolStripMenuItem_Click;
			this.createNewWebserviceVersionToolStripMenuItem.Click += createNewWebserviceVersionToolStripMenuItem_Click;

            // NewRelic
		    this.ucNewRelicDeployment.OnLog += (sender, s) => WriteToLog(s);
		}        

	    private bool checkboxLock;
        private void ReleaseSoftwareAllCheckStateChanged(object sender, EventArgs eventArgs)
        {
            if (checkboxLock)
                return;

            checkboxLock = true;

            var isChecked = this.cbReleasesSelectAll.Checked;
            this.cbCraveCloud.Checked = isChecked;
            this.cbCraveOnSiteAgent.Checked = isChecked;
            this.cbCraveOnSiteServer.Checked = isChecked;
            this.cbCraveOnSiteSupportTools.Checked = isChecked;

            checkboxLock = false;
        }

	    private void ReleaseSoftwareCheckStateChanged(object sender, EventArgs eventArgs)
	    {
            if (checkboxLock)
	            return;

	        checkboxLock = true;

	        bool allChecked = this.cbCraveCloud.Checked &&
	                          this.cbCraveOnSiteAgent.Checked &&
	                          this.cbCraveOnSiteServer.Checked && 
                              this.cbCraveOnSiteSupportTools.Checked;
	        this.cbReleasesSelectAll.Checked = allChecked;

	        checkboxLock = false;
	    }

	    /// <summary>
	    /// Writes the specified contents to the log file
	    /// </summary>
	    /// <param name="contents">The contents to write to the logfile</param>
	    /// <param name="args"></param>
	    private void WriteToLog(string contents, params object[] args)
		{
            if (this.tbLog.InvokeRequired)
            {
                this.tbLog.Invoke(new MethodInvoker(() => WriteToLog(contents, args)));
                return;
            }

			// Write to file
			DesktopLogger.Debug(contents, LoggingLevel.Debug, args);

			// and add it to the log textbox
			this.tbLog.Text = string.Format(contents, args) + "\r\n" + this.tbLog.Text;
		}

		public void UpdateProgress(string progress, params object[] args)
		{
			this.lblProgress.Text = string.Format(progress, args);
		}

        private void UpdateAllWebsiteVersions()
        {
            this.WriteToLog("Updating version for all web projects...");

            foreach (var app in this.applications)
            {
                if (app.IsWebsite)
                {
                    this.builder.UpdateVersion(app, this.tbVersion.Text);
                }
            }

            this.WriteToLog("Versions for all web projects updated");
        }

		private void UpdateAllSoftwareVersion()
		{
			this.WriteToLog("Updating version for all software...");

			this.builder.UpdateVersion(this[Constants.ObymobiWebservice], this.tbVersion.Text);
			this.builder.UpdateVersion(this[Constants.ObymobiCms], this.tbVersion.Text);
			this.builder.UpdateVersion(this[Constants.CraveOnsiteServer], this.tbVersion.Text);
			this.builder.UpdateVersion(this[Constants.CraveOnsiteAgent], this.tbVersion.Text);
            this.builder.UpdateVersion(this[Constants.CraveOnsiteSupportTools], this.tbVersion.Text);
			this.builder.UpdateVersion(this[Constants.Messaging], this.tbVersion.Text);
			this.builder.UpdateVersion(this[Constants.CraveMobileNocV2], this.tbVersion.Text);
            this.builder.UpdateVersion(this[Constants.CraveNocService], this.tbVersion.Text);

			this.WriteToLog("Versions for all software updated");
		}

		private void CreateReleasesForSelectedSoftware()
		{
			this.WriteToLog("Creating releases for selected software...");

			int releaseCount = 0;

			if (this.cbCraveCloud.Checked)
				releaseCount += 5;
			if (this.cbCraveOnSiteServer.Checked)
				releaseCount++;
			if (this.cbCraveOnSiteAgent.Checked)
				releaseCount++;
		    if (this.cbCraveOnSiteSupportTools.Checked)
		        releaseCount++;

			int counter = 1;

			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			if (this.cbCraveCloud.Checked && this.IsCodeVersionUpToDate(this[Constants.ObymobiWebservice]))
			{
			    foreach (var app in this.applications)
			    {
                    if (app.IsWebsite)
                    {
                        this.UpdateProgress("Creating release {0} of {1}", counter, releaseCount);
                        this.builder.CreateRelease(app, this.tbVersion.Text, false, false, false, this.progressBar);
                        counter++;
                    }
			    }

                this.UpdateProgress("Creating CraveCloud zip file");
                var appDir = Path.Combine(this.builder.ReleaseDir, "_CraveCloud", this.tbVersion.Text);

			    var zipPath = this.builder.CreateZipFromFolderContents(appDir, "CraveCloud", this.tbVersion.Text, this.progressBar);

                if (this.cbPublishCraveCloud.Checked)
                {
                    var cloudApp = new Application("CraveCloud", string.Empty, string.Empty, string.Empty, string.Empty, true, string.Empty);
                    this.builder.PublishRelease(new Release(cloudApp, this.tbVersion.Text, zipPath, true));
                }

                if (this.cbFtpCloudSoftware.Checked)
                {
                    WriteToLog("Starting FTP upload");
                    this.UpdateProgress("Uploading to FTP");

                    // Refresh the UI
                    System.Windows.Forms.Application.DoEvents();

                    try
                    {
                        string zipFileName = string.Format("CraveCloud-{0}.zip", tbVersion.Text.Replace("1.", string.Empty));

                        string ftpHost = ConfigurationManager.GetString(CraveBuilderConfigConstants.FtpHost) + "/uploads/_CraveCloud/" + zipFileName;
                        string ftpUser = ConfigurationManager.GetString(CraveBuilderConfigConstants.FtpUsername);
                        string ftpPassword = ConfigurationManager.GetPassword(CraveBuilderConfigConstants.FtpPassword);

                        string ftpResult;
                        if (ftpUser.Length > 0)
                            ftpResult = FtpHelper.UploadFile(ftpHost, ftpUser, ftpPassword, zipPath);
                        else
                            ftpResult = FtpHelper.UploadFile(ftpHost, zipPath);

                        WriteToLog("FTP Result: {0}", ftpResult);
                    }
                    catch (Exception ex)
                    {
                        WriteToLog("Failed to upload file: {0}", ex.Message);
                        MessageBox.Show(string.Format("Failed to upload file: {0}", ex.Message), "FTP Upload Failed", MessageBoxButtons.OK);
                    }
                }
			}

			if (this.cbCraveOnSiteServer.Checked && this.IsCodeVersionUpToDate(this[Constants.CraveOnsiteServer]))
			{
				this.UpdateProgress("Creating release {0} of {1}", counter, releaseCount);
				this.builder.CreateRelease(this[Constants.CraveOnsiteServer], this.tbVersion.Text, this.cbObfuscateCraveOnsiteServer.Checked, this.cbCreateInstallerCraveOnsiteServer.Checked, this.cbPublishCraveOnsiteServer.Checked, this.progressBar);
				counter++;
			}

			if (this.cbCraveOnSiteAgent.Checked && this.IsCodeVersionUpToDate(this[Constants.CraveOnsiteAgent]))
			{
				this.UpdateProgress("Creating release {0} of {1}", counter, releaseCount);
				this.builder.CreateRelease(this[Constants.CraveOnsiteAgent], this.tbVersion.Text, this.cbObfuscateCraveOnsiteAgent.Checked, this.cbCreateInstallerCraveOnsiteAgent.Checked, this.cbPublishCraveOnsiteAgent.Checked, this.progressBar);
			}

		    if (this.cbCraveOnSiteSupportTools.Checked && this.IsCodeVersionUpToDate(this[Constants.CraveOnsiteSupportTools]))
		    {
                this.UpdateProgress("Creating release {0} of {1}", counter, releaseCount);
                this.builder.CreateRelease(this[Constants.CraveOnsiteSupportTools], this.tbVersion.Text, this.cbObfuscateCraveOnsiteSupportTools.Checked, this.cbCreateInstallerCraveOnsiteSupportTools.Checked, this.cbPublishCraveOnsiteSupportTools.Checked, this.progressBar);
		    }

		    stopwatch.Stop();

			this.UpdateProgress("Releases created for selected software in {0}", stopwatch.Elapsed.ToString());
			this.WriteToLog("Releases created for selected software");
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="CraveBuilder.Application"/> with the specified name.
		/// </summary>
		public Application this[string name]
		{
			get
			{
				Application application = null;
				foreach (var app in this.applications)
				{
					if (app.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
						application = app;
				}
				return application;
			}
		}

		/// <summary>
		/// Gets or sets the progress bar.
		/// </summary>
		public ProgressBar ProgressBar
		{
			get
			{
				return this.progressBar;
			}
			set
			{
				this.progressBar = value;
			}
		}

		#endregion

		#region Event handlers

		private void buildAllToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.WriteToLog("BUILDING SELECTED SOFTWARE...");

			//this.UpdateAllSoftwareVersion();
			this.CreateReleasesForSelectedSoftware();

			this.WriteToLog("DONE BUILDING SELECTED SOFTWARE");
		}

		private void androidToolStripMenuItem_Click(object sender, EventArgs e)
		{
			var frmAndroid = new FormAndroid();
			frmAndroid.ShowDialog(this);
		}

		private void createNewWebserviceVersionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			var formWebserviceCreator = new FormWebserviceCreator();
			formWebserviceCreator.ShowDialog();
		}

		#region Version

		private void btnGetNewReleaseVersionNumber_Click(object sender, EventArgs e)
		{
			this.tbVersion.Text = "1." + DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + "00";
		}

		private void btnGetNewBuildVersionNumber_Click(object sender, EventArgs e)
		{
			// Get max build number and increase that
			var versionNumbers = new List<string>();
			versionNumbers.Add(this.lblLastVersionCraveOnSiteAgent.Text);
			versionNumbers.Add(this.lblLastVersionCraveOnSiteAgentInstaller.Text);
			versionNumbers.Add(this.lblLastVersionCraveOnSiteServer.Text);
			versionNumbers.Add(this.lblLastVersionCraveOnSiteServerInstaller.Text);
            versionNumbers.Add(this.lblLastVersionCraveOnSiteSupportTools.Text);
            versionNumbers.Add(this.lblLastVersionCraveOnSiteSupportToolsInstaller.Text);
			versionNumbers.Add(this.lblLastVersionCraveCloud.Text);
			versionNumbers.Add(this.lblLastVersionObymobiWebserviceInstaller.Text);
            versionNumbers.Add(this.lblSourceVersionCraveCloud.Text);
            versionNumbers.Add(this.lblSourceVersionOnsiteServer.Text);
            versionNumbers.Add(this.lblSourceVersionOnsiteAgent.Text);

		    versionNumbers.Remove("None");

			string lastVersion = versionNumbers.Max();
			this.tbVersion.Text = lastVersion;
			this.btnIncreaseBuildNumber_Click(null, null);
		}

		private void btnIncreaseBuildNumber_Click(object sender, EventArgs e)
		{
			if (this.tbVersion.Text.IsNullOrWhiteSpace())
				MessageBox.Show("Enter a version number to increase the build number of", "Version missing");
			else
			{
				try
				{
					string original = this.tbVersion.Text;
					string buildPart = original.Substring(original.Length - 2);
					int buildNumber = Convert.ToInt32(buildPart);

					if (buildNumber == 99)
						MessageBox.Show("The maximum build number has been reached: 99", "Build number to high");
					else
					{
						buildNumber++;

						this.tbVersion.Text = string.Format("{0}{1}", original.Substring(0, original.Length - 2),
						buildNumber.ToString().PadLeft(2, '0'));
					}
				}
				catch
				{
					MessageBox.Show("Couldn't increase the build number of the version.", "Could not increase build");
				}
			}
		}

		private bool VersionIsSet()
		{
			bool toReturn = false;
			if (!this.tbVersion.Text.IsNullOrWhiteSpace())
			{
				toReturn = true;
			}
            else if (!this.tbVersion.Text.StartsWith("1."))
            {
                MessageBox.Show("Version number must start with '1.' i.e. 1.2013051005", "Version number invalid");
            }
            else
            {
                MessageBox.Show("Enter a version number first", "Version number missing");
            }
			return toReturn;
		}

		private void btnUpdateCraveOnsiteServerVersion_Click(object sender, EventArgs e)
		{
			if (this.VersionIsSet())
				this.builder.UpdateVersion(this[Constants.CraveOnsiteServer], this.tbVersion.Text);
		}

		private void btnUpdateCraveOnsiteAgentVersion_Click(object sender, EventArgs e)
		{
			if (this.VersionIsSet())
				this.builder.UpdateVersion(this[Constants.CraveOnsiteAgent], this.tbVersion.Text);
		}

        private void btnUpdateCraveOnsiteSupportToolsVersion_Click(object sender, EventArgs e)
        {
            if (this.VersionIsSet())
                this.builder.UpdateVersion(this[Constants.CraveOnsiteSupportTools], this.tbVersion.Text);
        }

		private void btnUpdateAllSoftwareVersion_Click(object sender, EventArgs e)
		{
			if (this.VersionIsSet())
				this.UpdateAllSoftwareVersion();
		}

        private void btnUpdateCraveCloudVersion_Click(object sender, EventArgs e)
        {
            if (this.VersionIsSet())
                this.UpdateAllWebsiteVersions();
        }

		#endregion

		#region Compile

		private void llOpenObymobiSolution_Click(object sender, EventArgs e)
		{
			if (File.Exists(Constants.SolutionFile))
				System.Diagnostics.Process.Start(Constants.SolutionFile);
			else
				this.WriteToLog("Solution file '{0}' does not exist!", Constants.SolutionFile);
		}

		#endregion

		#region Create release

		private bool IsCodeVersionUpToDate(Application application)
		{
			bool toReturn = false;

			string versionInCode;
			if (!this.builder.GetCurrentApplicationVersion(application, out versionInCode))
			{
				MessageBox.Show("Could not determine version of: " + application.Name, "Version unkown");
			}
			else if (versionInCode != this.tbVersion.Text)
			{
				MessageBox.Show(string.Format("You're creating a build of version '{0}' while the version in code is: '{1}' for application '{2}'",
					this.tbVersion.Text, versionInCode, application.Name), "Incorrect version");
			}
			else
				toReturn = true;

			return toReturn;
		}

		private void btnCreateReleasesForSelectedSoftware_Click(object sender, EventArgs e)
		{
			if (this.VersionIsSet())
			{
				this.CreateReleasesForSelectedSoftware();
			}
		}

		private void cbCreateInstallersForAllSoftware_CheckedChanged(object sender, EventArgs e)
		{
			bool isChecked = this.cbCreateInstallersForAllSoftware.Checked;

			this.cbCreateInstallerCraveOnsiteServer.Checked = isChecked;
			this.cbCreateInstallerCraveOnsiteAgent.Checked = isChecked;
		    this.cbCreateInstallerCraveOnsiteSupportTools.Checked = isChecked;
		}

		private void cbObfuscateAllSoftware_CheckedChanged(object sender, EventArgs e)
		{
			bool isChecked = this.cbObfuscateAllSoftware.Checked;

            this.cbObfuscateCraveOnsiteServer.Checked = isChecked;
			this.cbObfuscateCraveOnsiteAgent.Checked = isChecked;
		    this.cbObfuscateCraveOnsiteSupportTools.Checked = isChecked;
		}

		private void cbPublishAllSoftware_CheckedChanged(object sender, EventArgs e)
		{
			bool isChecked = this.cbPublishAllSoftware.Checked;

			this.cbPublishCraveCloud.Checked = isChecked;
			this.cbPublishCraveOnsiteServer.Checked = isChecked;
			this.cbPublishCraveOnsiteAgent.Checked = isChecked;
		    this.cbPublishCraveOnsiteSupportTools.Checked = isChecked;
		}

		#endregion

		#region Sync database

		private void llOpenSQLDelta_Click(object sender, EventArgs e)
		{
			string sqlDeltaExePath = ConfigurationManager.GetString(CraveBuilderConfigConstants.SQLDeltaExePath);

			if (File.Exists(sqlDeltaExePath))
				System.Diagnostics.Process.Start(sqlDeltaExePath);
			else
			{
				this.WriteToLog(@"*** WARNING: SQL DELTA NOT FOUND AT PATH '{0}'! Check the SQL Delta executable path in 'CraveBuilder.config' or install SQL Delta from 'S:\SQL Delta\SQL Delta 4.0 - Geen activatie\SQLDelta42\setup.exe'", sqlDeltaExePath.ToUpper());
				MessageBox.Show(string.Format("SQL DELTA NOT FOUND AT PATH\r\n'{0}'!\r\n\r\nCheck the SQL Delta executable path in 'CraveBuilder.config' or install SQL Delta from 'S:\\SQL Delta\\SQL Delta 4.0 - Geen activatie\\SQLDelta42\\setup.exe'", sqlDeltaExePath.ToUpper()));
			}
		}

		#endregion

		#region Remote desktop

		private void llConnectToPrimaryWebserver_Click(object sender, EventArgs e)
		{
			string rdpFilePath = Path.Combine(System.Windows.Forms.Application.StartupPath, "RDP\\Studio.rdp");
			if (!File.Exists(rdpFilePath))
				MessageBox.Show(string.Format("Remote Desktop file could not be found at '{0}'", rdpFilePath));
			else
			{
				System.Diagnostics.Process p = new System.Diagnostics.Process();
				p.StartInfo.FileName = rdpFilePath;
				p.Start();
			}
		}

		private void llConnectToSecondaryWebserver_Click(object sender, EventArgs e)
		{
			string rdpFilePath = Path.Combine(System.Windows.Forms.Application.StartupPath, "RDP\\ChuckNorris.rdp");
			if (!File.Exists(rdpFilePath))
				MessageBox.Show(string.Format("Remote Desktop file could not be found at '{0}'", rdpFilePath));
			else
			{
				System.Diagnostics.Process p = new System.Diagnostics.Process();
				p.StartInfo.FileName = rdpFilePath;
				p.Start();
			}
		}

		#endregion

		#region Test

		private void llOpenWebserviceDev_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/api/{1}/CraveService.asmx", BASE_URL_DEV, ObymobiConstants.API_VERSION));
		}

		private void llOpenCmsDev_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/management/", BASE_URL_DEV));
		}

		private void llOpenConsoleDev_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/console/", BASE_URL_DEV));
		}

		private void llOpenMobileDev_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/mobile/", BASE_URL_DEV));
		}

		private void llOpenNocDev_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/noc/", BASE_URL_DEV));
		}

		private void llOpenWebserviceTest_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/api/{1}/CraveService.asmx", BASE_URL_TEST, ObymobiConstants.API_VERSION));
		}

		private void llOpenCmsTest_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/management/", BASE_URL_TEST));
		}

		private void llOpenConsoleTest_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/console/", BASE_URL_TEST));
		}

		private void llOpenMobileTest_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/mobile/", BASE_URL_TEST));
		}

		private void llOpenNocTest_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/noc/", BASE_URL_TEST));
		}

		private void llOpenWebserviceLive_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/api/{1}/CraveService.asmx", BASE_URL_LIVE, ObymobiConstants.API_VERSION));
		}

		private void llOpenCmsLive_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/management/", BASE_URL_LIVE));
		}

		private void llOpenConsoleLive_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/console/", BASE_URL_LIVE));
		}

		private void llOpenMobileLive_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/mobile/", BASE_URL_LIVE));
		}

		private void llOpenNocLive_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(string.Format("{0}/noc/", BASE_URL_LIVE));
		}

		#endregion

		#region Pivotal

		private void llGoToPivotal_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(@"https://www.pivotaltracker.com");
		}

		#endregion

        private void cbDotnetFolder_SelectedValueChanged(object sender, EventArgs e)
        {
            Constants.DotNetPath = this.cbDotnetFolder.SelectedValue.ToString();            

            this.applications = new List<Application>();
            if (!Constants.DotNetPath.IsNullOrWhiteSpace())
            {
                this.applications.Add(new Application(Constants.CraveNocService, "nocservice", @"StartupManager.cs", string.Empty, string.Empty, true, string.Empty));
                this.applications.Add(new Application(Constants.Services, "Obymobi.Services.WebApplication", @"Global.asax.cs", string.Empty, string.Empty, true, string.Empty));
                this.applications.Add(new Application(Constants.Messaging, "Obymobi.Messaging.WebApplication", @"Global.asax.cs", string.Empty, string.Empty, true, string.Empty));
                this.applications.Add(new Application(Constants.ObymobiWebservice, "api", @"App_Code\Global.asax.cs", string.Empty, string.Empty, true, string.Empty));
                this.applications.Add(new Application(Constants.ObymobiCms, "Obymobi.Cms.WebApplication", @"Global.asax.cs", string.Empty, string.Empty, true, string.Empty));
                this.applications.Add(new Application(Constants.CraveMobileNocV2, "Obymobi.Noc.WebApplication", @"Global.asax.cs", string.Empty, string.Empty, true, string.Empty));                

                if (Constants.DotNetPathBranch.IsNullOrWhiteSpace())
                {
                    this.applications.Add(new Application(Constants.CraveOnsiteServer, string.Empty, "Program.cs", Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\CraveOnsiteServer.iss"), @"bin\Release", false, Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\Reactor.NET\CraveOnsiteServer.nrproj")));
                    this.applications.Add(new Application(Constants.CraveOnsiteAgent, string.Empty, "Program.cs", Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\CraveOnsiteAgent.iss"), @"bin\Release", false, Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\Reactor.NET\CraveOnsiteAgent.nrproj")));
                    this.applications.Add(new Application(Constants.CraveOnsiteSupportTools, string.Empty, "Program.cs", Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\CraveOnsiteSupportTools.iss"), @"bin\Release", false, Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\Reactor.NET\CraveOnsiteSupportTools.nrproj")));
                }
                else
                {
                    string issFilePostfix = Constants.DotNetPathBranch + "00.iss";
                    string nrprojFilePostfix = Constants.DotNetPathBranch + "00.nrproj";
                    this.applications.Add(new Application(Constants.CraveOnsiteServer, string.Empty, "Program.cs", Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\CraveOnsiteServer." + issFilePostfix), @"bin\Release", false, Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\Reactor.NET\CraveOnsiteServer." + nrprojFilePostfix)));
                    this.applications.Add(new Application(Constants.CraveOnsiteAgent, string.Empty, "Program.cs", Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\CraveOnsiteAgent." + issFilePostfix), @"bin\Release", false, Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\Reactor.NET\CraveOnsiteAgent." + nrprojFilePostfix)));
                    this.applications.Add(new Application(Constants.CraveOnsiteSupportTools, string.Empty, "Program.cs", Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\CraveOnsiteSupportTools." + issFilePostfix), @"bin\Release", false, Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\Reactor.NET\CraveOnsiteSupportTools." + nrprojFilePostfix)));
                }

                // Update the Current Versions
                this.GetCurrentVersions();
            }
        }

        private void generateChangelogFromSvnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var diag = new FormGenerateSvnChangelog();
            diag.ShowDialog();
        }

        private void configurationToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var diag = new FormConfiguration();
            diag.ShowDialog();
        }

		#endregion

        private void label8_Click(object sender, EventArgs e)
        {

        }
	}
}
