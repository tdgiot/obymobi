﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Dionysos;
using System.Globalization;
using System.Reflection;
using Dionysos.Reflection;
using System.ComponentModel;
using Obymobi.Logic.Model;
using System.Text.RegularExpressions;
using Microsoft.Build.BuildEngine;
using VSLangProj;

namespace CraveBuilder
{
    /// <summary>
    /// Class which creates new versions of the Obymobi webservice including models
    /// </summary>
    public class WebserviceCreator
    {
        #region Fields

        private CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");

        private static string WEBSERVICE_PATH = Path.Combine(Constants.DotNetPath, @"Obymobi\ObymobiWebService");
        private static string WEBSERVICE_ASMX_FILENAME = "CraveService.asmx";
        private static string WEBSERVICE_CODE_BEHIND_FILENAME = "CraveService.cs";

        private static string OBYMOBI_LOGIC_LEGACY_PATH = Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Logic.Legacy");
        private static string OBYMOBI_LOGIC_LEGACY_PROJECT_FILE = "Obymobi.Logic.Legacy.csproj";

        private static string OBYMOBI_WEBSERVICETESTER_PATH = Path.Combine(Constants.DotNetPath, @"Obymobi.WebserviceTester\Obymobi.WebserviceTester");
        private static string OBYMOBI_WEBSERVICETESTER_PROJECT_FILE = "Obymobi.Logic.WebserviceTester.csproj";

        private static string MODEL_PATH = Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Core\Logic\Model");

        private double latestVersionDouble = -1;
        private string latestVersionString = string.Empty;
        private string latestVersionStringNamespace = string.Empty;
        private string newVersionString = string.Empty;
        private string newVersionStringNamespace = string.Empty;
        private string latestVersionModelDir = string.Empty;
        private string latestConverterClassesVersionString = string.Empty;
        private string newVersionAsmx = string.Empty;

        private Assembly modelAssembly = null;
        private Dictionary<string, Type> models = new Dictionary<string, Type>();
        private List<string> excludedModels = new List<string>();

        private List<string> newWebservicesDirs = new List<string>();
        private List<string> newWebserviceFiles = new List<string>();

        private readonly string CopyDefaultFieldsInCopyDefaultFieldsNeedle = "//COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS";
        private readonly string CopyDefaultFieldsInConvertLegacyToModelNeedle = "//COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL";

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the <see cref="WebserviceCreator"/> class
        /// </summary>
        public WebserviceCreator()
        {
            this.excludedModels.Add("ModelBase");
            this.excludedModels.Add("LoginStatus");
        }

        #endregion

        #region Methods

        private double GetLatestVersion()
        {
            double latestVersion = -1;

            // Check whether the webservice directory exists
            if (!Directory.Exists(WEBSERVICE_PATH))
                MessageBox.Show(string.Format("Webservice directory '{0}' does not exist!", WEBSERVICE_PATH));
            else
            {
                // Get the sub directories of the webservice directory
                string[] subDirs = Directory.GetDirectories(WEBSERVICE_PATH);
                foreach (string subDir in subDirs)
                {
                    string dirName = subDir.Substring(subDir.LastIndexOf(@"\")+1);
                    string filePath = Path.Combine(subDir, WEBSERVICE_ASMX_FILENAME);

                    double version = -1;
                    if (double.TryParse(dirName, NumberStyles.Any, this.culture, out version) && version > latestVersion && File.Exists(filePath))
                        latestVersion = version;
                }
            }

            return latestVersion;
        }

        public string GetLatestVersionString()
        {
            return this.GetLatestVersion().ToString("N1", this.culture);
        }

        public string GetLatestConverterClassesVersionString()
        {
            return (this.GetLatestVersion() - 1).ToString("N1", this.culture);
        }

        public string GetNewVersionString()
        {
            double latestVersion = this.GetLatestVersion();

            double newVersion = latestVersion+1;
            return newVersion.ToString("N1", this.culture);
        }

        public void CreateNewWebserviceVersion(string versionString)
        {
            double newVersionDouble = -1;
            if (!double.TryParse(versionString, NumberStyles.Any, this.culture, out newVersionDouble))
                MessageBox.Show("Version number is not in a proper format, use format X.Y");
            else
            {
                // Get the version numbers
                this.GetVersionNumbers(newVersionDouble);

                // Get the models
                this.GetModels();

                // Copy latest version directory
                if (!this.CopyLatestWebserviceVersionDirectory())
                    return;

                // Copy latest version code-behind directory
                if (!this.CopyLatestCodeBehindVersionDirectory())
                    return;

                // Copy the models to the previous version folder and change the namespace
                if (!this.CopyModelsToModelArchive())
                    return;

                // Copy latest webservice code behind files for the webservice tester
                this.CopyLatestWebserviceCodeBehindForWebserviceTester();

                // Generate the converter classes
                this.GenerateConverterClasses();

                // Update the converter classes of the previous version get the fieldvalues + FieldsToExclude from new converter version
                this.UpdatePreviousConverterClasses();

                // Update the project file with new directory & file paths
                this.UpdateLegacyProjectFile();

                MessageBox.Show("Webservice version created successfully!");
            }
        }

        private void GetVersionNumbers(double newVersion)
        {
            this.latestVersionDouble = this.GetLatestVersion();

            this.latestVersionString = this.GetLatestVersionString();
            if (this.latestVersionString.EndsWith("0"))
                this.latestVersionStringNamespace = this.latestVersionString.Substring(0, this.latestVersionString.IndexOf("."));
            else
                this.latestVersionStringNamespace = this.latestVersionString.Replace(".", "_");

            this.newVersionString = newVersion.ToString("N1", this.culture);
            if (this.newVersionString.EndsWith("0"))
                this.newVersionStringNamespace = this.newVersionString.Substring(0, this.newVersionString.IndexOf("."));
            else
                this.newVersionStringNamespace = this.newVersionString.Replace(".", "_");

            this.latestConverterClassesVersionString = this.GetLatestConverterClassesVersionString();
        }

        private void GetModels()
        {
            // Get the model types
            this.models = new Dictionary<string, Type>();

            // Get the model assembly
            Type terminalType = typeof(Obymobi.Logic.Model.Terminal);
            this.modelAssembly = Assembly.GetAssembly(terminalType);

            // Get the model types
            foreach (Type type in this.modelAssembly.GetTypes())
            {
                if (type.Namespace == terminalType.Namespace)
                {
                    object instance = InstanceFactory.CreateInstance(modelAssembly, type);
                    if (instance != null && instance is ModelBase)
                        this.models.Add(type.Name, type);
                }
            }
        }

        private bool CopyLatestWebserviceVersionDirectory()
        {
            bool success = false;

            string latestVersionDir = Path.Combine(WEBSERVICE_PATH, this.latestVersionString);
            string latestVersionAsmx = Path.Combine(latestVersionDir, WEBSERVICE_ASMX_FILENAME);
            if (!Directory.Exists(latestVersionDir))
                MessageBox.Show(string.Format("Latest webservice version directory '{0}' does not exist!", latestVersionDir));
            else if (!File.Exists(latestVersionAsmx))
                MessageBox.Show(string.Format("Latest webservice version asmx file '{0}' does not exist!", latestVersionAsmx));
            else
            {
                string newVersionDirWebservice = Path.Combine(WEBSERVICE_PATH, this.newVersionString);                
                this.newVersionAsmx = Path.Combine(newVersionDirWebservice, WEBSERVICE_ASMX_FILENAME);
                    
                if (File.Exists(this.newVersionAsmx))
                    MessageBox.Show(string.Format("New webservice version asmx file '{0}' already exists!", this.newVersionAsmx));
                else
                {
                    try
                    {
                        // Create the directory
                        // and copy the asmx file
                        if (!Directory.Exists(newVersionDirWebservice))
                            Directory.CreateDirectory(newVersionDirWebservice);

                        File.Copy(latestVersionAsmx, this.newVersionAsmx);

                        if (File.Exists(this.newVersionAsmx))
                        {
                            string contents = string.Format("<%@ WebService Language=\"C#\" CodeBehind=\"~/App_Code/WebServices/{0}/CraveService.cs\" Class=\"ObymobiWebservice.WebServices.v{1}.CraveService\" %>", this.newVersionString, this.newVersionStringNamespace);
                            File.WriteAllText(this.newVersionAsmx, contents);

                            success = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Something went wrong when trying to copy the latest webservice version directory!\r\n\r\nException:\r\n" + ex.Message);
                    }
                }
            }

            return success;
        }

        private bool CopyLatestCodeBehindVersionDirectory()
        {
            bool success = false;

            string latestVersionDir = Path.Combine(WEBSERVICE_PATH, "App_Code", "WebServices", this.latestVersionString);
            string latestVersionCodeBehind = Path.Combine(latestVersionDir, WEBSERVICE_CODE_BEHIND_FILENAME);
            if (!Directory.Exists(latestVersionDir))
                MessageBox.Show(string.Format("Latest code behind version directory '{0}' does not exist!", latestVersionDir));
            else if (!File.Exists(latestVersionCodeBehind))
                MessageBox.Show(string.Format("Latest webservice version code behind file '{0}' does not exist!", latestVersionCodeBehind));
            else
            {
                string newVersionDir = Path.Combine(WEBSERVICE_PATH, "App_Code", "WebServices", this.newVersionString);
                string newVersionDirLegacy = Path.Combine(OBYMOBI_LOGIC_LEGACY_PATH, "WebServices", this.newVersionString);
                string newVersionCodeBehind = Path.Combine(newVersionDir, WEBSERVICE_CODE_BEHIND_FILENAME);
                
                if (File.Exists(newVersionCodeBehind))
                    MessageBox.Show(string.Format("New webservice version code behind file '{0}' already exists!", newVersionCodeBehind));
                else
                {
                    try
                    {
                        // Create the directory
                        // and copy the asmx file
                        if (!Directory.Exists(newVersionDir))
                            Directory.CreateDirectory(newVersionDir);

                        // Create new directory for future models
                        if (!Directory.Exists(newVersionDirLegacy))
                        {
                            Directory.CreateDirectory(newVersionDirLegacy);
                            this.newWebservicesDirs.Add(newVersionDirLegacy);
                        }
                            
                        File.Copy(latestVersionCodeBehind, newVersionCodeBehind);

                        // Change the namespace in the new code behind file
                        if (File.Exists(newVersionCodeBehind))
                        {
                            string contents = File.ReadAllText(newVersionCodeBehind);
                            contents = contents.Replace(string.Format("namespace ObymobiWebservice.WebServices.v{0}", this.latestVersionStringNamespace), string.Format("namespace ObymobiWebservice.WebServices.v{0}", this.newVersionStringNamespace));
                            File.WriteAllText(newVersionCodeBehind, contents);

                            // Change the import in the latest code behind file
                            if (File.Exists(latestVersionCodeBehind))
                            {
                                contents = File.ReadAllText(latestVersionCodeBehind);

                                // Replace the model namespace
                                contents = contents.Replace("using Obymobi.Logic.Model;", string.Format("using Obymobi.Logic.Model.v{0};\r\nusing Obymobi.Logic.Model.v{0}.Converters;", this.latestVersionStringNamespace));

                                // Replace the API version number in the New Relic helper method
                                string newRelicApiVersionCurrent = string.Format("/api/{0}/", this.latestVersionString);
                                string newRelicApiVersionNew = string.Format("/api/{0}/", this.newVersionString);
                                contents = contents.Replace(newRelicApiVersionCurrent, newRelicApiVersionNew);

                                string[] lines = contents.Split('\n', StringSplitOptions.RemoveEmptyEntries);
                                List<string> methods = new List<string>();
                                List<string> xmlHelperLines = new List<string>();
                                bool inMethod = false;
                                int startIndex = -1;
                                int endIndex = -1;

                                foreach (string line in lines)
                                {
                                    string trimmedLine = line.Trim();
                                    if (trimmedLine.StartsWith("public ObyTypedResult<"))
                                    {
                                        inMethod = true;
                                        startIndex = contents.IndexOf(line);
                                    }

                                    if (trimmedLine == @"}" && inMethod)
                                    {
                                        endIndex = contents.IndexOf(line, startIndex);
                                        methods.Add(contents.Substring(startIndex, (endIndex-startIndex)+line.Length));
                                        inMethod = false;
                                    }

                                    if (line.Contains("XmlHelper.Deserialize<"))
                                        xmlHelperLines.Add(line);
                                }

                                foreach (string method in methods)
                                {
                                    string contentsFromMethod = method;

                                    int index = -1;
                                    string model = string.Empty;
                                    string methodCall = string.Empty;

                                    // Find the model type
                                    if (contentsFromMethod.Contains("ObyTypedResult<"))
                                    {
                                        index = contentsFromMethod.IndexOf("ObyTypedResult<") + 15;
                                        model = contentsFromMethod.Substring(index, contentsFromMethod.IndexOf(">") - index);

                                        if (this.models.Keys.Contains(model) && (contentsFromMethod.Contains("return this.webserviceHandler.") || contentsFromMethod.Contains("return webserviceHandler.")))
                                        {
                                            if (contentsFromMethod.Contains("return this.webserviceHandler."))
                                                index = contentsFromMethod.LastIndexOf("this.webserviceHandler.") + 23;
                                            else if (contentsFromMethod.Contains("return webserviceHandler."))
                                                index = contentsFromMethod.LastIndexOf("webserviceHandler.") + 18;

                                            string partOfMethod = contentsFromMethod.Substring(index); 
                                            
                                            methodCall = partOfMethod.Substring(0, partOfMethod.IndexOf(";"));

                                            if (contentsFromMethod.Contains("return this.webserviceHandler."))
                                                contents = contents.Replace(string.Format("return this.webserviceHandler.{0};", methodCall), string.Format("return new {0}Converter().ConvertResultToLegacyResult(this.webserviceHandler.{1});", model, methodCall));
                                            else if (contentsFromMethod.Contains("return webserviceHandler."))
                                                contents = contents.Replace(string.Format("return webserviceHandler.{0};", methodCall), string.Format("return new {0}Converter().ConvertResultToLegacyResult(this.webserviceHandler.{1});", model, methodCall));
                                        }
                                    }
                                }

                                foreach (string xmlHelperLine in xmlHelperLines)
                                {
                                    int start = xmlHelperLine.IndexOf("XmlHelper.Deserialize<")+22;
                                    int end = xmlHelperLine.IndexOf(">");

                                    string model = xmlHelperLine.Substring(start, (end-start));

                                    bool array = false;
                                    if (model.EndsWith("[]"))
                                    {
                                        array = true;
                                        model = model.Replace("[]", string.Empty);
                                    }

                                    if (this.models.Keys.Contains(model))
                                    {
                                        string part = xmlHelperLine.Trim().Substring(xmlHelperLine.Trim().IndexOf(" ") + 1);
                                        string variableName = part.Substring(0, part.IndexOf(" "));

                                        if (!array)
                                            contents = contents.Replace(xmlHelperLine.Trim(), string.Format("Obymobi.Logic.Model.{0} {1} = new {0}Converter().ConvertLegacyXmlToModel(xml);", model, variableName));
                                        else
                                            contents = contents.Replace(xmlHelperLine.Trim(), string.Format("Obymobi.Logic.Model.{0}[] {1} = new {0}Converter().ConvertLegacyXmlArrayToModelArray(xml);", model, variableName));
                                    }
                                }
       
                                File.WriteAllText(latestVersionCodeBehind, contents);

                                success = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Something went wrong when trying to copy the latest webservice version directory!\r\n\r\nException:\r\n" + ex.Message);
                    }
                }
            }

            return success;
        }

        private bool CopyLatestWebserviceCodeBehindForWebserviceTester()
        {
            bool success = false;

            string projectFile = Path.Combine(OBYMOBI_WEBSERVICETESTER_PATH, OBYMOBI_WEBSERVICETESTER_PROJECT_FILE);
            string latestWebserviceBehindDir = Path.Combine(OBYMOBI_WEBSERVICETESTER_PATH, "WebServices", this.latestVersionString);
            string latestVersionCodeBehind = Path.Combine(latestWebserviceBehindDir, WEBSERVICE_CODE_BEHIND_FILENAME);
            if (!File.Exists(projectFile))
                MessageBox.Show(string.Format("The project file of the WebserviceTester couldn't be found. File: '{0}'", projectFile));
            if (!Directory.Exists(latestWebserviceBehindDir))
                MessageBox.Show(string.Format("Latest WebserviceTester craveservice code-behind directory for version '{0}' does not exist!", latestWebserviceBehindDir));
            else if (!File.Exists(latestVersionCodeBehind))
                MessageBox.Show(string.Format("Latest WebserviceTester craveservice code-behind file for version '{0}' does not exist!", latestVersionCodeBehind));
            else
            {
                string newVersionDir = Path.Combine(OBYMOBI_WEBSERVICETESTER_PATH, "WebServices", this.newVersionString);
                string newVersionCodeBehind = Path.Combine(newVersionDir, WEBSERVICE_CODE_BEHIND_FILENAME);

                if (File.Exists(newVersionCodeBehind))
                    MessageBox.Show(string.Format("New webservice version code-behind for the WebserviceTester file '{0}' already exists!", newVersionCodeBehind));
                else
                {
                    try
                    {
                        List<string> newDirectories = new List<string>();
                        List<string> newFiles = new List<string>();

                        // Create directory
                        if (!Directory.Exists(newVersionDir))
                        {
                            Directory.CreateDirectory(newVersionDir);
                            newDirectories.Add(newVersionDir);
                        }                            

                        // Copy the latest codebehind file for the new version
                        File.Copy(latestVersionCodeBehind, newVersionCodeBehind);

                        if (File.Exists(newVersionCodeBehind))
                        {
                            newFiles.Add(newVersionCodeBehind);

                            // Replace the version number in the new codebehind (CraveService.cs file) in the WebserviceTester
                            string latestVersionNumber = this.latestVersionString.Substring(0, this.latestVersionString.IndexOf("."));
                            string newVersionNumber = this.newVersionString.Substring(0, this.newVersionString.IndexOf("."));

                            string contents = File.ReadAllText(newVersionCodeBehind);
                            contents = contents.Replace(latestVersionNumber, newVersionNumber);
                            File.WriteAllText(newVersionCodeBehind, contents);

                            if (File.Exists(latestVersionCodeBehind))
                            { 
                                // Add a conversion to the methods for the latest codebehind (CraveService.cs file) in the WebserviceTester
                                contents = File.ReadAllText(latestVersionCodeBehind);

                                string[] lines = contents.Split('\n', StringSplitOptions.RemoveEmptyEntries);
                                List<string> methods = new List<string>();
                                bool inMethod = false;
                                int startIndex = -1;
                                int endIndex = -1;
                                
                                foreach (string line in lines)
                                {
                                    string trimmedLine = line.Trim();
                                    if (trimmedLine.StartsWith("public WebserviceResult<"))
                                    {
                                        inMethod = true;
                                        startIndex = contents.IndexOf(line);
                                    }

                                    if (trimmedLine == @"}" && inMethod)
                                    {
                                        endIndex = contents.IndexOf(line, startIndex);
                                        methods.Add(contents.Substring(startIndex, (endIndex - startIndex) + line.Length));
                                        inMethod = false;
                                    }
                                }

                                foreach (string method in methods)
                                {
                                    string contentsFromMethod = method;

                                    int index = -1;
                                    string model = string.Empty;

                                    if (contentsFromMethod.Contains("WebserviceResult<"))
                                    {
                                        index = contentsFromMethod.IndexOf("WebserviceResult<") + 17;
                                        model = contentsFromMethod.Substring(index, contentsFromMethod.IndexOf(">") - index);

                                        if (this.models.Keys.Contains(model))
                                        {
                                            string newModel = string.Format("Obymobi.Logic.Model.v{0}.{1}", latestVersionNumber, model);
                                            string newConverter = string.Format("Obymobi.Logic.Model.v{0}.Converters.{1}Converter()", latestVersionNumber, model);

                                            // Convert models to old version specific models
                                            contents = contents.Replace(string.Format("ObyTypedResult<{0}>", model), string.Format("ObyTypedResult<{0}>", newModel));
                                            contents = contents.Replace(string.Format("ConvertModel<{0}[]>", model), string.Format("ConvertModel<{0}[]>", newModel));
                                            contents = contents.Replace(string.Format("ObyTypedResult = obyTypedResult; // {0}", model), string.Format("ObyTypedResult = new {0}.ConvertLegacyResultToResult(obyTypedResult);", newConverter));
                                        }
                                    }
                                }

                                File.WriteAllText(latestVersionCodeBehind, contents);                                
                            }
                        }

                        // Add the new directories & files to the csproj file of the WebserviceTester.
                        success = this.UpdateProjectFile(projectFile, OBYMOBI_WEBSERVICETESTER_PATH, newDirectories, newFiles);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Something went wrong while trying to copy the latest code behind file for the webservie of the WebserviceTester!\r\n\r\nException:\r\n" + ex.Message);
                    }
                }
            }
            return success;
        }

        private bool CopyModelsToModelArchive()
        {
            bool success = true;

            if (!Directory.Exists(MODEL_PATH))
            {
                MessageBox.Show(string.Format("Model directory '{0}' does not exist!", MODEL_PATH));
                success = false;
            }
            else
            {
                this.latestVersionModelDir = Path.Combine(OBYMOBI_LOGIC_LEGACY_PATH, "WebServices", this.latestVersionString, "Model");
                if (Directory.Exists(this.latestVersionModelDir))
                {
                    MessageBox.Show(string.Format("Latest version model directory '{0}' already exists!", this.latestVersionModelDir));
                    success = false;
                }
                else
                {
                    try
                    {
                        // Create the archive model directory
                        Directory.CreateDirectory(this.latestVersionModelDir);                        

                        // Copy the current models into the archive model directory
                        foreach (string modelPath in Directory.GetFiles(MODEL_PATH))
                        {
                            if (!this.excludedModels.Contains(Path.GetFileNameWithoutExtension(modelPath)))
                            {
                                string archiveModelPath = Path.Combine(this.latestVersionModelDir, Path.GetFileName(modelPath));

                                // Copy the model into the archive model directory
                                File.Copy(modelPath, archiveModelPath);
                                
                                if (File.Exists(archiveModelPath))  
                                {
                                    this.newWebserviceFiles.Add(archiveModelPath);

                                    // Change the namespace of the archived model
                                    string contents = File.ReadAllText(archiveModelPath);
                                    contents = contents.Replace("namespace Obymobi.Logic.Model", string.Format("namespace Obymobi.Logic.Model.v{0}", this.latestVersionStringNamespace));
                                    File.WriteAllText(archiveModelPath, contents);
                                }
                            }
                        }

                        string pmsModelPath = Path.Combine(MODEL_PATH, "PMS");
                        if (Directory.Exists(pmsModelPath))
                        {
                            string latestVersionPmsModelDir = Path.Combine(this.latestVersionModelDir, "PMS");
                            if (!Directory.Exists(latestVersionPmsModelDir))
                            {
                                // Create the archive model PMS directory
                                Directory.CreateDirectory(latestVersionPmsModelDir);
                                
                                // Copy the current models into the archive model directory
                                foreach (string modelPath in Directory.GetFiles(pmsModelPath))
                                {
                                    if (!this.excludedModels.Contains(Path.GetFileNameWithoutExtension(modelPath)))
                                    {
                                        string archiveModelPath = Path.Combine(latestVersionPmsModelDir, Path.GetFileName(modelPath));

                                        // Copy the model into the archive model directory
                                        File.Copy(modelPath, archiveModelPath);

                                        if (File.Exists(archiveModelPath))
                                        {
                                            this.newWebserviceFiles.Add(archiveModelPath);

                                            // Change the namespace of the archived model
                                            string contents = File.ReadAllText(archiveModelPath);
                                            contents = contents.Replace("namespace Obymobi.Logic.Model", string.Format("namespace Obymobi.Logic.Model.v{0}", this.latestVersionStringNamespace));
                                            File.WriteAllText(archiveModelPath, contents);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Something went wrong when trying to copy the models to the model archive directory!\r\n\r\nException:\r\n" + ex.Message);
                        success = false;
                    }
                }
            }

            return success;
        }

        private void GenerateConverterClasses()
        {
            string converterDir = Path.Combine(this.latestVersionModelDir, "Converters");
            if (Directory.Exists(converterDir))
                MessageBox.Show(string.Format("Latest version converter directory '{0}' already exists!", converterDir));
            else
            {
                try
                {
                    // Create the converters directory
                    Directory.CreateDirectory(converterDir);

                    // Create a converter class for each of the models
                    foreach (string modelPath in Directory.GetFiles(MODEL_PATH))
                    {
                        string modelName = Path.GetFileNameWithoutExtension(modelPath);
                        if (!this.excludedModels.Contains(modelName))
                        {
                            string converterName = string.Format("{0}Converter", modelName);
                            string converterPath = Path.Combine(converterDir, converterName+".cs");

                            if (this.models.Keys.Contains(modelName))
                            {
                                object modelInstance = InstanceFactory.CreateInstance(this.modelAssembly, this.models[modelName]);
                                if (modelInstance != null)
                                {
                                    string contents = this.GetConverterClassContent(modelName, modelInstance);
                                    File.WriteAllText(converterPath, contents);
                                    this.newWebserviceFiles.Add(converterPath);
                                }
                            }
                        }
                    }

                    string pmsConverterDir = Path.Combine(converterDir, "PMS");

                    // Create the PMS converters directory
                    Directory.CreateDirectory(pmsConverterDir);

                    // Create a converter class for each of the models
                    foreach (string modelPath in Directory.GetFiles(Path.Combine(MODEL_PATH, "PMS")))
                    {
                        string modelName = Path.GetFileNameWithoutExtension(modelPath);
                        if (!this.excludedModels.Contains(modelName))
                        {
                            string converterName = string.Format("{0}Converter", modelName);
                            string converterPath = Path.Combine(pmsConverterDir, converterName + ".cs");

                            if (this.models.Keys.Contains(modelName))
                            {
                                object modelInstance = InstanceFactory.CreateInstance(this.modelAssembly, this.models[modelName]);
                                if (modelInstance != null)
                                {
                                    string contents = this.GetConverterClassContent(modelName, modelInstance);
                                    File.WriteAllText(converterPath, contents);
                                    this.newWebserviceFiles.Add(converterPath);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Something went wrong when trying to create the converter classes!\r\n\r\nException:\r\n" + ex.Message);
                }
            }
        }

        private bool UpdatePreviousConverterClasses()
        { 
            bool success = true;
            
            string latestConvertersDirectory = Path.Combine(OBYMOBI_LOGIC_LEGACY_PATH, "WebServices", this.latestConverterClassesVersionString, "Model", "Converters");
            string latestConvertersPmsDirectory = Path.Combine(latestConvertersDirectory, "PMS");
            if (!Directory.Exists(latestConvertersDirectory))
            {
                MessageBox.Show(string.Format("Converter directory of version '{0}' was not found!. Path: '{1}'.", this.latestConverterClassesVersionString, latestConvertersDirectory));
                success = false;
            }
            else if (!Directory.Exists(latestConvertersPmsDirectory))
            {
                MessageBox.Show(string.Format("PMS converter directory of version '{0}' was not found!. Path: '{1}'.", this.latestConverterClassesVersionString, latestConvertersPmsDirectory));
                success = false;
            }
            else
            {
                string latestVersionStr = this.latestVersionString.ToString().Replace(".0", "");

                // Converter classes
                foreach (string converterPath in Directory.GetFiles(latestConvertersDirectory))
                {
                    string converterName = Path.GetFileNameWithoutExtension(converterPath);
                    if (!this.excludedModels.Contains(converterName))
                    {
                        string contents = File.ReadAllText(converterPath);
                        
                        string replaceWith1 = string.Format("new Obymobi.Logic.Model.v{0}.Converters.{1}().CopyDefaultFields(fieldsToExclude, fieldValues);", latestVersionStr, converterName);
                        string replaceWith2 = string.Format("new Obymobi.Logic.Model.v{0}.Converters.{1}().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);", latestVersionStr, converterName);

                        contents = contents.Replace(this.CopyDefaultFieldsInCopyDefaultFieldsNeedle, replaceWith1);
                        contents = contents.Replace(this.CopyDefaultFieldsInConvertLegacyToModelNeedle, replaceWith2);

                        File.WriteAllText(converterPath, contents);
                    }
                }

                // PMS converter classes
                foreach (string pmsConverterPath in Directory.GetFiles(latestConvertersPmsDirectory))
                {
                    string pmsConverterName = Path.GetFileNameWithoutExtension(pmsConverterPath);
                    if (!this.excludedModels.Contains(pmsConverterName))
                    {
                        string contents = File.ReadAllText(pmsConverterPath);

                        string replaceWith1 = string.Format("new Obymobi.Logic.Model.v{0}.Converters.{1}().CopyDefaultFields(fieldsToExclude, fieldValues);", latestVersionStr, pmsConverterName);
                        string replaceWith2 = string.Format("new Obymobi.Logic.Model.v{0}.Converters.{1}().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);", latestVersionStr, pmsConverterName);

                        contents = contents.Replace(this.CopyDefaultFieldsInCopyDefaultFieldsNeedle, replaceWith1);
                        contents = contents.Replace(this.CopyDefaultFieldsInConvertLegacyToModelNeedle, replaceWith2);

                        File.WriteAllText(pmsConverterPath, contents);
                    }
                }
            }
            
            return success;
        }

        private bool UpdateLegacyProjectFile()
        {
            string legacyProjectFile = Path.Combine(OBYMOBI_LOGIC_LEGACY_PATH, OBYMOBI_LOGIC_LEGACY_PROJECT_FILE);
            return UpdateProjectFile(legacyProjectFile, OBYMOBI_LOGIC_LEGACY_PATH, this.newWebservicesDirs, this.newWebserviceFiles);
        }

        private bool UpdateProjectFile(string projectFile, string prefixToRemove, List<string> newDirectories, List<string> newFiles)
        {
            bool success = true;

            if (!File.Exists(projectFile))
            {
                MessageBox.Show(string.Format("Project file wasn't found! Path: '{0}'.", projectFile));
                success = false;
            }
            else if (newDirectories.Count > 0 || newFiles.Count > 0) 
            {
                try
                {
                    Project project = new Project();                    
                    project.Load(projectFile);

                    if (newDirectories.Count > 0)
                    {
                        BuildItemGroup dirGroup = project.AddNewItemGroup();
                        foreach (string dir in newDirectories)
                        {
                            string dirname = dir.Replace(prefixToRemove, "");
                            if (!dirname.EndsWith(@"\"))
                                dirname += @"\";

                            dirGroup.AddNewItem("Folder", dirname);
                        }
                    }
                    if (newFiles.Count > 0)
                    {
                        BuildItemGroup fileGroup = project.AddNewItemGroup();
                        foreach (string file in newFiles)
                        {
                            string filename = file.Replace((prefixToRemove + @"\"), "");
                            fileGroup.AddNewItem("Compile", filename);
                        }
                    }
                    project.Save(projectFile, Encoding.UTF8);
                }
                catch (Exception ex)
                {
                    success = false;
                    MessageBox.Show(string.Format("Something went wrong trying to load the project file. Path: '{0}'. Exception: '{1}'.", projectFile, ex.Message));
                }                
            }

            return success;
        }

        private string GetConverterClassContent(string modelName, object modelInstance)
        {
            string template = string.Empty;

            // Namespace
            template += string.Format("namespace Obymobi.Logic.Model.v{0}.Converters\r\n", this.latestVersionStringNamespace);
            template += "{\r\n";

            // Class definition
            template += string.Format("	public class {0}Converter : ModelConverterBase<Obymobi.Logic.Model.v{1}.{0}, Obymobi.Logic.Model.{0}>\r\n", modelName, this.latestVersionStringNamespace);
            template += "	{\r\n";

            // Constructor
            template += string.Format("        public {0}Converter()\r\n", modelName);
            template += "        {\r\n";
            template += "            // Set the excluded fields\r\n";
            template += "            \r\n";
            template += "            // Set the field values\r\n";
            template += "            \r\n";
            template += "        }\r\n\r\n";

            // Method CopyDefaultFields
            template += "        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)\r\n";
            template += "        {\r\n";
            template += "            // Copy fields from new version\r\n";
            template += string.Format("            {0}\r\n", this.CopyDefaultFieldsInCopyDefaultFieldsNeedle);
            template += "            \r\n";
            template += "            // Copy fields to exclude\r\n";
            template += "            foreach (string fieldToExclude in this.FieldsToExclude)\r\n";
            template += "            {\r\n";
            template += "               if (!fieldsToExclude.Contains(fieldToExclude))\r\n";
            template += "                   fieldsToExclude.Add(fieldToExclude);\r\n";
            template += "            }\r\n";
            template += "            \r\n";
            template += "            // Copy field values\r\n";
            template += "            foreach (var fieldValue in this.FieldValues)\r\n";
            template += "            {\r\n";
            template += "               if (!fieldValues.ContainsKey(fieldValue.Key))\r\n";
            template += "                   fieldValues.Add(fieldValue.Key, fieldValue.Value);\r\n";
            template += "            }\r\n";

            template += "        }\r\n\r\n";

            // Method ConvertModelToLegacyModel
            template += string.Format("        public override Obymobi.Logic.Model.v{0}.{1} ConvertModelToLegacyModel(Obymobi.Logic.Model.{1} source)\r\n", this.latestVersionStringNamespace, modelName);
            template += "        {\r\n";
            template += string.Format("            Obymobi.Logic.Model.v{0}.{1} target = null;\r\n\r\n", this.latestVersionStringNamespace, modelName);
            template += "            if (source != null)\r\n";
            template += "            {\r\n";
            template += string.Format("                target = new Obymobi.Logic.Model.v{0}.{1}();\r\n", this.latestVersionStringNamespace, modelName);

            bool complexProperty = false;

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(modelInstance))
            {
                if (property.IsReadOnly)
                {
                    // Fuck that shit
                    complexProperty = false;
                }
                else if (property.PropertyType.Name.Contains("Int32[]") || property.PropertyType.Name.Contains("String[]") || property.PropertyType.Name.Contains("Byte[]"))
                {
                    if (complexProperty)
                        template += "\r\n";

                    complexProperty = false;

                    // Model property
                    template += string.Format("                target.{0} = source.{0};\r\n", property.Name);
                }
                else if (property.PropertyType.Name.Contains("[]"))
                {
                    complexProperty = true;
                    string propertyType = property.PropertyType.Name.Replace("[]", "");

                    // Array
                    template += string.Format("\r\n                if (source.{0} != null)\r\n", property.Name);
                    template += "                {\r\n";
                    template += "                    // Create the converter class for this and convert this array\r\n";
                    template += string.Format("                    Obymobi.Logic.Model.v{0}.Converters.{1}Converter {2}Converter = new Obymobi.Logic.Model.v{0}.Converters.{1}Converter();\r\n", this.latestVersionStringNamespace, propertyType, property.Name.TurnFirstToLower(false));
                    template += string.Format("                    target.{0} = ({1}[]){2}Converter.ConvertArrayToLegacyArray(source.{0});\r\n", property.Name, propertyType, property.Name.TurnFirstToLower(false));
                    template += "                }\r\n";
                }
                else if (this.models.Keys.Contains(property.PropertyType.Name))
                {
                    complexProperty = true;
                    string propertyType = property.PropertyType.Name;

                    // Model property
                    template += string.Format("\r\n                if (source.{0} != null)\r\n", property.Name);
                    template += "                {\r\n";
                    template += "                    // Create the converter class for this and convert this model\r\n";
                    template += string.Format("                    Obymobi.Logic.Model.v{0}.Converters.{1}Converter {2}Converter = new Obymobi.Logic.Model.v{0}.Converters.{1}Converter();\r\n", this.latestVersionStringNamespace, propertyType, property.Name.TurnFirstToLower(false));
                    template += string.Format("                    target.{0} = ({1}){2}Converter.ConvertModelToLegacyModel(source.{0});\r\n", property.Name, propertyType, property.Name.TurnFirstToLower(false));
                    template += "                }\r\n";
                }
                else
                {
                    if (complexProperty)
                        template += "\r\n";

                    complexProperty = false;

                    // Normal property
                    template += string.Format("                target.{0} = source.{0};\r\n", property.Name);
                }
            }

            template += "            }\r\n\r\n";
            template += "            return target;\r\n";
            template += "        }\r\n\r\n";

            // Method ConvertLegacyModelToModel
            template += string.Format("        public override Obymobi.Logic.Model.{1} ConvertLegacyModelToModel(Obymobi.Logic.Model.v{0}.{1} source)\r\n", this.latestVersionStringNamespace, modelName);
            template += "        {\r\n";
            template += string.Format("            Obymobi.Logic.Model.{0} target = null;\r\n", modelName);
            template += "\r\n";
            template += "            if (source != null)\r\n";
            template += "            {\r\n";
            template += string.Format("                target = new Obymobi.Logic.Model.{0}();\r\n\r\n", modelName);
            template += "                // Copy default values from new version\r\n";
            template += string.Format("                {0}\r\n\r\n", this.CopyDefaultFieldsInConvertLegacyToModelNeedle);
            template += "                // Copy the fields to the model\r\n";
            template += "                this.CopyFieldsToModel(source, target);\r\n";
            template += "            }\r\n";
            template += "\r\n";
            template += "            return target;\r\n";
            template += "        }\r\n";

            template += "	}\r\n";
            template += "}\r\n";

            return template;
        }

        #endregion
    }
}
