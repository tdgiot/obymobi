﻿namespace CraveBuilder
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tbStart = new System.Windows.Forms.TabPage();
            this.lblSourceVersionOnsiteAgent = new System.Windows.Forms.Label();
            this.lblSourceVersionOnsiteServer = new System.Windows.Forms.Label();
            this.lblSourceVersionCraveCloud = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDotnetFolder = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnIncreaseBuildNumber = new System.Windows.Forms.Button();
            this.btnGetNewBuildVersionNumber = new System.Windows.Forms.Button();
            this.btnGetNewReleaseVersionNumber = new System.Windows.Forms.Button();
            this.lblLastVersionCraveOnSiteAgentInstaller = new System.Windows.Forms.Label();
            this.lblLastVersionCraveOnSiteServerInstaller = new System.Windows.Forms.Label();
            this.lblLastVersionObymobiWebserviceInstaller = new System.Windows.Forms.Label();
            this.lblLastVersionCraveOnSiteAgent = new System.Windows.Forms.Label();
            this.lblLastVersionCraveOnSiteServer = new System.Windows.Forms.Label();
            this.lblLastVersionCraveCloud = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnUpdateAllSoftwareVersion = new System.Windows.Forms.Button();
            this.btnUpdateCraveOnsiteAgentVersion = new System.Windows.Forms.Button();
            this.btnUpdateCraveOnsiteServerVersion = new System.Windows.Forms.Button();
            this.btnUpdateCraveCloudVersion = new System.Windows.Forms.Button();
            this.tbVersion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tpBuild = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblSetBuildConfigurationToDebug = new System.Windows.Forms.Label();
            this.lblUpdateWebreferencesPosWebserviceAndOssWebservice = new System.Windows.Forms.Label();
            this.lblUpdateWebreferencePosWebservice = new System.Windows.Forms.Label();
            this.lblSetBuildConfigurationToRelease = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblBuildCraveOnsiteAgent = new System.Windows.Forms.Label();
            this.lblBuildObymobiRequestService = new System.Windows.Forms.Label();
            this.lblBuildObymobiCms = new System.Windows.Forms.Label();
            this.lblBuildObymobiWebservice = new System.Windows.Forms.Label();
            this.lblUpdateWebreferences = new System.Windows.Forms.Label();
            this.llOpenObymobiSolution = new System.Windows.Forms.LinkLabel();
            this.tpCreateReleases = new System.Windows.Forms.TabPage();
            this.cbFtpCloudSoftware = new System.Windows.Forms.CheckBox();
            this.cbReleasesSelectAll = new System.Windows.Forms.CheckBox();
            this.cbCraveOnSiteAgent = new System.Windows.Forms.CheckBox();
            this.cbCraveOnSiteServer = new System.Windows.Forms.CheckBox();
            this.cbCraveCloud = new System.Windows.Forms.CheckBox();
            this.cbPublishAllSoftware = new System.Windows.Forms.CheckBox();
            this.cbCreateInstallersForAllSoftware = new System.Windows.Forms.CheckBox();
            this.cbObfuscateAllSoftware = new System.Windows.Forms.CheckBox();
            this.cbPublishCraveCloud = new System.Windows.Forms.CheckBox();
            this.cbPublishCraveOnsiteAgent = new System.Windows.Forms.CheckBox();
            this.cbPublishCraveOnsiteServer = new System.Windows.Forms.CheckBox();
            this.cbCreateInstallerCraveOnsiteAgent = new System.Windows.Forms.CheckBox();
            this.cbCreateInstallerCraveOnsiteServer = new System.Windows.Forms.CheckBox();
            this.cbObfuscateCraveOnsiteAgent = new System.Windows.Forms.CheckBox();
            this.cbObfuscateCraveOnsiteServer = new System.Windows.Forms.CheckBox();
            this.btnCreateReleasesForSelectedSoftware = new System.Windows.Forms.Button();
            this.tpSyncDatabase = new System.Windows.Forms.TabPage();
            this.llOpenSQLDelta = new System.Windows.Forms.LinkLabel();
            this.tpUpload = new System.Windows.Forms.TabPage();
            this.lblUploadFiles = new System.Windows.Forms.Label();
            this.tpRemoteDesktop = new System.Windows.Forms.TabPage();
            this.llConnectToSecondaryWebserver = new System.Windows.Forms.LinkLabel();
            this.llConnectToPrimaryWebserver = new System.Windows.Forms.LinkLabel();
            this.tabTest = new System.Windows.Forms.TabPage();
            this.gbLive = new System.Windows.Forms.GroupBox();
            this.llOpenNocLive = new System.Windows.Forms.LinkLabel();
            this.llOpenMobileLive = new System.Windows.Forms.LinkLabel();
            this.llOpenConsoleLive = new System.Windows.Forms.LinkLabel();
            this.llOpenCmsLive = new System.Windows.Forms.LinkLabel();
            this.llOpenWebserviceLive = new System.Windows.Forms.LinkLabel();
            this.gbTest = new System.Windows.Forms.GroupBox();
            this.llOpenNocTest = new System.Windows.Forms.LinkLabel();
            this.llOpenMobileTest = new System.Windows.Forms.LinkLabel();
            this.llOpenConsoleTest = new System.Windows.Forms.LinkLabel();
            this.llOpenCmsTest = new System.Windows.Forms.LinkLabel();
            this.llOpenWebserviceTest = new System.Windows.Forms.LinkLabel();
            this.gbDevelopment = new System.Windows.Forms.GroupBox();
            this.llOpenNocDev = new System.Windows.Forms.LinkLabel();
            this.llOpenMobileDev = new System.Windows.Forms.LinkLabel();
            this.llOpenConsoleDev = new System.Windows.Forms.LinkLabel();
            this.llOpenCmsDev = new System.Windows.Forms.LinkLabel();
            this.llOpenWebserviceDev = new System.Windows.Forms.LinkLabel();
            this.tpCmsMaintenance = new System.Windows.Forms.TabPage();
            this.lblClearCache = new System.Windows.Forms.Label();
            this.lblRefreshAllViews = new System.Windows.Forms.Label();
            this.lblRefreshInfoInLocalDatabase = new System.Windows.Forms.Label();
            this.tpCmsReleases = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.tpDeliver = new System.Windows.Forms.TabPage();
            this.llGoToPivotal = new System.Windows.Forms.LinkLabel();
            this.lblSetStoriesToDelivered = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ucNewRelicDeployment = new CraveBuilder.SubControls.UcNewRelicDeployment();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.configurationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewWebserviceVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateChangelogFromSvnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.openWebserviceTesterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.androidToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.androidToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnUpdateCraveOnsiteSupportToolsVersion = new System.Windows.Forms.Button();
            this.lblLastVersionCraveOnSiteSupportTools = new System.Windows.Forms.Label();
            this.lblSourceVersionOnsiteSupportTools = new System.Windows.Forms.Label();
            this.lblLastVersionCraveOnSiteSupportToolsInstaller = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbCraveOnSiteSupportTools = new System.Windows.Forms.CheckBox();
            this.cbCreateInstallerCraveOnsiteSupportTools = new System.Windows.Forms.CheckBox();
            this.cbObfuscateCraveOnsiteSupportTools = new System.Windows.Forms.CheckBox();
            this.cbPublishCraveOnsiteSupportTools = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tbStart.SuspendLayout();
            this.tpBuild.SuspendLayout();
            this.tpCreateReleases.SuspendLayout();
            this.tpSyncDatabase.SuspendLayout();
            this.tpUpload.SuspendLayout();
            this.tpRemoteDesktop.SuspendLayout();
            this.tabTest.SuspendLayout();
            this.gbLive.SuspendLayout();
            this.gbTest.SuspendLayout();
            this.gbDevelopment.SuspendLayout();
            this.tpCmsMaintenance.SuspendLayout();
            this.tpCmsReleases.SuspendLayout();
            this.tpDeliver.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tcMain, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbLog, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pnlTop, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 430F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1298, 748);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tbStart);
            this.tcMain.Controls.Add(this.tpBuild);
            this.tcMain.Controls.Add(this.tpCreateReleases);
            this.tcMain.Controls.Add(this.tpSyncDatabase);
            this.tcMain.Controls.Add(this.tpUpload);
            this.tcMain.Controls.Add(this.tpRemoteDesktop);
            this.tcMain.Controls.Add(this.tabTest);
            this.tcMain.Controls.Add(this.tpCmsMaintenance);
            this.tcMain.Controls.Add(this.tpCmsReleases);
            this.tcMain.Controls.Add(this.tpDeliver);
            this.tcMain.Controls.Add(this.tabPage1);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcMain.Location = new System.Drawing.Point(3, 103);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1292, 424);
            this.tcMain.TabIndex = 0;
            // 
            // tbStart
            // 
            this.tbStart.Controls.Add(this.lblLastVersionCraveOnSiteSupportToolsInstaller);
            this.tbStart.Controls.Add(this.lblSourceVersionOnsiteSupportTools);
            this.tbStart.Controls.Add(this.lblLastVersionCraveOnSiteSupportTools);
            this.tbStart.Controls.Add(this.btnUpdateCraveOnsiteSupportToolsVersion);
            this.tbStart.Controls.Add(this.lblSourceVersionOnsiteAgent);
            this.tbStart.Controls.Add(this.lblSourceVersionOnsiteServer);
            this.tbStart.Controls.Add(this.lblSourceVersionCraveCloud);
            this.tbStart.Controls.Add(this.label2);
            this.tbStart.Controls.Add(this.cbDotnetFolder);
            this.tbStart.Controls.Add(this.label13);
            this.tbStart.Controls.Add(this.btnIncreaseBuildNumber);
            this.tbStart.Controls.Add(this.btnGetNewBuildVersionNumber);
            this.tbStart.Controls.Add(this.btnGetNewReleaseVersionNumber);
            this.tbStart.Controls.Add(this.lblLastVersionCraveOnSiteAgentInstaller);
            this.tbStart.Controls.Add(this.lblLastVersionCraveOnSiteServerInstaller);
            this.tbStart.Controls.Add(this.lblLastVersionObymobiWebserviceInstaller);
            this.tbStart.Controls.Add(this.lblLastVersionCraveOnSiteAgent);
            this.tbStart.Controls.Add(this.lblLastVersionCraveOnSiteServer);
            this.tbStart.Controls.Add(this.lblLastVersionCraveCloud);
            this.tbStart.Controls.Add(this.label7);
            this.tbStart.Controls.Add(this.label6);
            this.tbStart.Controls.Add(this.btnUpdateAllSoftwareVersion);
            this.tbStart.Controls.Add(this.btnUpdateCraveOnsiteAgentVersion);
            this.tbStart.Controls.Add(this.btnUpdateCraveOnsiteServerVersion);
            this.tbStart.Controls.Add(this.btnUpdateCraveCloudVersion);
            this.tbStart.Controls.Add(this.tbVersion);
            this.tbStart.Controls.Add(this.label1);
            this.tbStart.Location = new System.Drawing.Point(4, 27);
            this.tbStart.Name = "tbStart";
            this.tbStart.Padding = new System.Windows.Forms.Padding(3);
            this.tbStart.Size = new System.Drawing.Size(1284, 393);
            this.tbStart.TabIndex = 0;
            this.tbStart.Text = "Start";
            this.tbStart.UseVisualStyleBackColor = true;
            // 
            // lblSourceVersionOnsiteAgent
            // 
            this.lblSourceVersionOnsiteAgent.AutoSize = true;
            this.lblSourceVersionOnsiteAgent.Location = new System.Drawing.Point(855, 147);
            this.lblSourceVersionOnsiteAgent.Name = "lblSourceVersionOnsiteAgent";
            this.lblSourceVersionOnsiteAgent.Size = new System.Drawing.Size(100, 18);
            this.lblSourceVersionOnsiteAgent.TabIndex = 44;
            this.lblSourceVersionOnsiteAgent.Text = "1.0000000000";
            // 
            // lblSourceVersionOnsiteServer
            // 
            this.lblSourceVersionOnsiteServer.AutoSize = true;
            this.lblSourceVersionOnsiteServer.Location = new System.Drawing.Point(855, 112);
            this.lblSourceVersionOnsiteServer.Name = "lblSourceVersionOnsiteServer";
            this.lblSourceVersionOnsiteServer.Size = new System.Drawing.Size(100, 18);
            this.lblSourceVersionOnsiteServer.TabIndex = 43;
            this.lblSourceVersionOnsiteServer.Text = "1.0000000000";
            // 
            // lblSourceVersionCraveCloud
            // 
            this.lblSourceVersionCraveCloud.AutoSize = true;
            this.lblSourceVersionCraveCloud.Location = new System.Drawing.Point(855, 77);
            this.lblSourceVersionCraveCloud.Name = "lblSourceVersionCraveCloud";
            this.lblSourceVersionCraveCloud.Size = new System.Drawing.Size(100, 18);
            this.lblSourceVersionCraveCloud.TabIndex = 42;
            this.lblSourceVersionCraveCloud.Text = "1.0000000000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(855, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 18);
            this.label2.TabIndex = 41;
            this.label2.Text = "Source Version";
            // 
            // cbDotnetFolder
            // 
            this.cbDotnetFolder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDotnetFolder.FormattingEnabled = true;
            this.cbDotnetFolder.Location = new System.Drawing.Point(160, 7);
            this.cbDotnetFolder.Name = "cbDotnetFolder";
            this.cbDotnetFolder.Size = new System.Drawing.Size(513, 26);
            this.cbDotnetFolder.TabIndex = 40;
            this.cbDotnetFolder.SelectedValueChanged += new System.EventHandler(this.cbDotnetFolder_SelectedValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(142, 18);
            this.label13.TabIndex = 39;
            this.label13.Text = "Specify dotnet folder";
            // 
            // btnIncreaseBuildNumber
            // 
            this.btnIncreaseBuildNumber.Location = new System.Drawing.Point(529, 37);
            this.btnIncreaseBuildNumber.Name = "btnIncreaseBuildNumber";
            this.btnIncreaseBuildNumber.Size = new System.Drawing.Size(144, 27);
            this.btnIncreaseBuildNumber.TabIndex = 35;
            this.btnIncreaseBuildNumber.Text = "Increase Build #";
            this.btnIncreaseBuildNumber.UseVisualStyleBackColor = true;
            this.btnIncreaseBuildNumber.Click += new System.EventHandler(this.btnIncreaseBuildNumber_Click);
            // 
            // btnGetNewBuildVersionNumber
            // 
            this.btnGetNewBuildVersionNumber.Location = new System.Drawing.Point(407, 38);
            this.btnGetNewBuildVersionNumber.Name = "btnGetNewBuildVersionNumber";
            this.btnGetNewBuildVersionNumber.Size = new System.Drawing.Size(98, 27);
            this.btnGetNewBuildVersionNumber.TabIndex = 34;
            this.btnGetNewBuildVersionNumber.Text = "New Build";
            this.btnGetNewBuildVersionNumber.UseVisualStyleBackColor = true;
            this.btnGetNewBuildVersionNumber.Click += new System.EventHandler(this.btnGetNewBuildVersionNumber_Click);
            // 
            // btnGetNewReleaseVersionNumber
            // 
            this.btnGetNewReleaseVersionNumber.Location = new System.Drawing.Point(266, 37);
            this.btnGetNewReleaseVersionNumber.Name = "btnGetNewReleaseVersionNumber";
            this.btnGetNewReleaseVersionNumber.Size = new System.Drawing.Size(117, 27);
            this.btnGetNewReleaseVersionNumber.TabIndex = 33;
            this.btnGetNewReleaseVersionNumber.Text = "New Release";
            this.btnGetNewReleaseVersionNumber.UseVisualStyleBackColor = true;
            this.btnGetNewReleaseVersionNumber.Click += new System.EventHandler(this.btnGetNewReleaseVersionNumber_Click);
            // 
            // lblLastVersionCraveOnSiteAgentInstaller
            // 
            this.lblLastVersionCraveOnSiteAgentInstaller.AutoSize = true;
            this.lblLastVersionCraveOnSiteAgentInstaller.Location = new System.Drawing.Point(1025, 148);
            this.lblLastVersionCraveOnSiteAgentInstaller.Name = "lblLastVersionCraveOnSiteAgentInstaller";
            this.lblLastVersionCraveOnSiteAgentInstaller.Size = new System.Drawing.Size(20, 18);
            this.lblLastVersionCraveOnSiteAgentInstaller.TabIndex = 28;
            this.lblLastVersionCraveOnSiteAgentInstaller.Text = "...";
            // 
            // lblLastVersionCraveOnSiteServerInstaller
            // 
            this.lblLastVersionCraveOnSiteServerInstaller.AutoSize = true;
            this.lblLastVersionCraveOnSiteServerInstaller.Location = new System.Drawing.Point(1025, 113);
            this.lblLastVersionCraveOnSiteServerInstaller.Name = "lblLastVersionCraveOnSiteServerInstaller";
            this.lblLastVersionCraveOnSiteServerInstaller.Size = new System.Drawing.Size(20, 18);
            this.lblLastVersionCraveOnSiteServerInstaller.TabIndex = 27;
            this.lblLastVersionCraveOnSiteServerInstaller.Text = "...";
            // 
            // lblLastVersionObymobiWebserviceInstaller
            // 
            this.lblLastVersionObymobiWebserviceInstaller.AutoSize = true;
            this.lblLastVersionObymobiWebserviceInstaller.Location = new System.Drawing.Point(1025, 78);
            this.lblLastVersionObymobiWebserviceInstaller.Name = "lblLastVersionObymobiWebserviceInstaller";
            this.lblLastVersionObymobiWebserviceInstaller.Size = new System.Drawing.Size(13, 18);
            this.lblLastVersionObymobiWebserviceInstaller.TabIndex = 24;
            this.lblLastVersionObymobiWebserviceInstaller.Text = "-";
            // 
            // lblLastVersionCraveOnSiteAgent
            // 
            this.lblLastVersionCraveOnSiteAgent.AutoSize = true;
            this.lblLastVersionCraveOnSiteAgent.Location = new System.Drawing.Point(695, 147);
            this.lblLastVersionCraveOnSiteAgent.Name = "lblLastVersionCraveOnSiteAgent";
            this.lblLastVersionCraveOnSiteAgent.Size = new System.Drawing.Size(20, 18);
            this.lblLastVersionCraveOnSiteAgent.TabIndex = 18;
            this.lblLastVersionCraveOnSiteAgent.Text = "...";
            // 
            // lblLastVersionCraveOnSiteServer
            // 
            this.lblLastVersionCraveOnSiteServer.AutoSize = true;
            this.lblLastVersionCraveOnSiteServer.Location = new System.Drawing.Point(695, 112);
            this.lblLastVersionCraveOnSiteServer.Name = "lblLastVersionCraveOnSiteServer";
            this.lblLastVersionCraveOnSiteServer.Size = new System.Drawing.Size(20, 18);
            this.lblLastVersionCraveOnSiteServer.TabIndex = 17;
            this.lblLastVersionCraveOnSiteServer.Text = "...";
            // 
            // lblLastVersionCraveCloud
            // 
            this.lblLastVersionCraveCloud.AutoSize = true;
            this.lblLastVersionCraveCloud.Location = new System.Drawing.Point(695, 77);
            this.lblLastVersionCraveCloud.Name = "lblLastVersionCraveCloud";
            this.lblLastVersionCraveCloud.Size = new System.Drawing.Size(100, 18);
            this.lblLastVersionCraveCloud.TabIndex = 14;
            this.lblLastVersionCraveCloud.Text = "1.0000000000";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1025, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 18);
            this.label7.TabIndex = 13;
            this.label7.Text = "Last installer";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(695, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 18);
            this.label6.TabIndex = 12;
            this.label6.Text = "Dropbox Version";
            // 
            // btnUpdateAllSoftwareVersion
            // 
            this.btnUpdateAllSoftwareVersion.BackColor = System.Drawing.Color.GreenYellow;
            this.btnUpdateAllSoftwareVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateAllSoftwareVersion.Location = new System.Drawing.Point(9, 223);
            this.btnUpdateAllSoftwareVersion.Name = "btnUpdateAllSoftwareVersion";
            this.btnUpdateAllSoftwareVersion.Size = new System.Drawing.Size(664, 29);
            this.btnUpdateAllSoftwareVersion.TabIndex = 9;
            this.btnUpdateAllSoftwareVersion.Text = "Update version for all software";
            this.btnUpdateAllSoftwareVersion.UseVisualStyleBackColor = false;
            // 
            // btnUpdateCraveOnsiteAgentVersion
            // 
            this.btnUpdateCraveOnsiteAgentVersion.Location = new System.Drawing.Point(9, 142);
            this.btnUpdateCraveOnsiteAgentVersion.Name = "btnUpdateCraveOnsiteAgentVersion";
            this.btnUpdateCraveOnsiteAgentVersion.Size = new System.Drawing.Size(664, 29);
            this.btnUpdateCraveOnsiteAgentVersion.TabIndex = 6;
            this.btnUpdateCraveOnsiteAgentVersion.Text = "Update version of Crave On-site Agent";
            this.btnUpdateCraveOnsiteAgentVersion.UseVisualStyleBackColor = true;
            // 
            // btnUpdateCraveOnsiteServerVersion
            // 
            this.btnUpdateCraveOnsiteServerVersion.Location = new System.Drawing.Point(9, 107);
            this.btnUpdateCraveOnsiteServerVersion.Name = "btnUpdateCraveOnsiteServerVersion";
            this.btnUpdateCraveOnsiteServerVersion.Size = new System.Drawing.Size(664, 29);
            this.btnUpdateCraveOnsiteServerVersion.TabIndex = 5;
            this.btnUpdateCraveOnsiteServerVersion.Text = "Update version of Crave On-site Server";
            this.btnUpdateCraveOnsiteServerVersion.UseVisualStyleBackColor = true;
            // 
            // btnUpdateCraveCloudVersion
            // 
            this.btnUpdateCraveCloudVersion.Location = new System.Drawing.Point(9, 72);
            this.btnUpdateCraveCloudVersion.Name = "btnUpdateCraveCloudVersion";
            this.btnUpdateCraveCloudVersion.Size = new System.Drawing.Size(664, 29);
            this.btnUpdateCraveCloudVersion.TabIndex = 2;
            this.btnUpdateCraveCloudVersion.Text = "Update version of Crave Cloud";
            this.btnUpdateCraveCloudVersion.UseVisualStyleBackColor = true;
            this.btnUpdateCraveCloudVersion.Click += new System.EventHandler(this.btnUpdateCraveCloudVersion_Click);
            // 
            // tbVersion
            // 
            this.tbVersion.Location = new System.Drawing.Point(160, 39);
            this.tbVersion.Name = "tbVersion";
            this.tbVersion.Size = new System.Drawing.Size(100, 24);
            this.tbVersion.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Specify the version";
            // 
            // tpBuild
            // 
            this.tpBuild.Controls.Add(this.label8);
            this.tpBuild.Controls.Add(this.label9);
            this.tpBuild.Controls.Add(this.label10);
            this.tpBuild.Controls.Add(this.label12);
            this.tpBuild.Controls.Add(this.label11);
            this.tpBuild.Controls.Add(this.label5);
            this.tpBuild.Controls.Add(this.lblSetBuildConfigurationToDebug);
            this.tpBuild.Controls.Add(this.lblUpdateWebreferencesPosWebserviceAndOssWebservice);
            this.tpBuild.Controls.Add(this.lblUpdateWebreferencePosWebservice);
            this.tpBuild.Controls.Add(this.lblSetBuildConfigurationToRelease);
            this.tpBuild.Controls.Add(this.label3);
            this.tpBuild.Controls.Add(this.lblBuildCraveOnsiteAgent);
            this.tpBuild.Controls.Add(this.lblBuildObymobiRequestService);
            this.tpBuild.Controls.Add(this.lblBuildObymobiCms);
            this.tpBuild.Controls.Add(this.lblBuildObymobiWebservice);
            this.tpBuild.Controls.Add(this.lblUpdateWebreferences);
            this.tpBuild.Controls.Add(this.llOpenObymobiSolution);
            this.tpBuild.Location = new System.Drawing.Point(4, 27);
            this.tpBuild.Name = "tpBuild";
            this.tpBuild.Padding = new System.Windows.Forms.Padding(3);
            this.tpBuild.Size = new System.Drawing.Size(1284, 393);
            this.tpBuild.TabIndex = 1;
            this.tpBuild.Text = "Compile";
            this.tpBuild.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 190);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(479, 18);
            this.label10.TabIndex = 17;
            this.label10.Text = "6. Update web reference \'CraveService\' for project Crave On-site Server";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 165);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(250, 18);
            this.label12.TabIndex = 16;
            this.label12.Text = "5. Set build configuration to \'Release\'";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(208, 18);
            this.label11.TabIndex = 15;
            this.label11.Text = "0. Get latest version from SVN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(622, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(256, 18);
            this.label5.TabIndex = 14;
            this.label5.Text = "14. Build Crave.NocService (Release)";
            // 
            // lblSetBuildConfigurationToDebug
            // 
            this.lblSetBuildConfigurationToDebug.AutoSize = true;
            this.lblSetBuildConfigurationToDebug.Location = new System.Drawing.Point(622, 40);
            this.lblSetBuildConfigurationToDebug.Name = "lblSetBuildConfigurationToDebug";
            this.lblSetBuildConfigurationToDebug.Size = new System.Drawing.Size(524, 18);
            this.lblSetBuildConfigurationToDebug.TabIndex = 13;
            this.lblSetBuildConfigurationToDebug.Text = "12. Update web reference \'NocServiceLoopback\' for project Crave.NocService";
            // 
            // lblUpdateWebreferencesPosWebserviceAndOssWebservice
            // 
            this.lblUpdateWebreferencesPosWebserviceAndOssWebservice.AutoSize = true;
            this.lblUpdateWebreferencesPosWebserviceAndOssWebservice.Location = new System.Drawing.Point(10, 242);
            this.lblUpdateWebreferencesPosWebserviceAndOssWebservice.Name = "lblUpdateWebreferencesPosWebserviceAndOssWebservice";
            this.lblUpdateWebreferencesPosWebserviceAndOssWebservice.Size = new System.Drawing.Size(473, 18);
            this.lblUpdateWebreferencesPosWebserviceAndOssWebservice.TabIndex = 12;
            this.lblUpdateWebreferencesPosWebserviceAndOssWebservice.Text = "8. Update web reference \'CraveService\' for project Crave On-site Agent";
            // 
            // lblUpdateWebreferencePosWebservice
            // 
            this.lblUpdateWebreferencePosWebservice.AutoSize = true;
            this.lblUpdateWebreferencePosWebservice.Location = new System.Drawing.Point(10, 140);
            this.lblUpdateWebreferencePosWebservice.Name = "lblUpdateWebreferencePosWebservice";
            this.lblUpdateWebreferencePosWebservice.Size = new System.Drawing.Size(242, 18);
            this.lblUpdateWebreferencePosWebservice.TabIndex = 11;
            this.lblUpdateWebreferencePosWebservice.Text = "4. Build Crave Mobile NOC (Debug)";
            // 
            // lblSetBuildConfigurationToRelease
            // 
            this.lblSetBuildConfigurationToRelease.AutoSize = true;
            this.lblSetBuildConfigurationToRelease.Location = new System.Drawing.Point(10, 115);
            this.lblSetBuildConfigurationToRelease.Name = "lblSetBuildConfigurationToRelease";
            this.lblSetBuildConfigurationToRelease.Size = new System.Drawing.Size(240, 18);
            this.lblSetBuildConfigurationToRelease.TabIndex = 10;
            this.lblSetBuildConfigurationToRelease.Text = "3. Build Messaging service (Debug)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(622, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(487, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "13. Update web reference \'NocWebservice\' for project Crave.NocService";
            // 
            // lblBuildCraveOnsiteAgent
            // 
            this.lblBuildCraveOnsiteAgent.AutoSize = true;
            this.lblBuildCraveOnsiteAgent.Location = new System.Drawing.Point(10, 268);
            this.lblBuildCraveOnsiteAgent.Name = "lblBuildCraveOnsiteAgent";
            this.lblBuildCraveOnsiteAgent.Size = new System.Drawing.Size(260, 18);
            this.lblBuildCraveOnsiteAgent.TabIndex = 6;
            this.lblBuildCraveOnsiteAgent.Text = "9. Build Crave On-site Agent (Release)";
            // 
            // lblBuildObymobiRequestService
            // 
            this.lblBuildObymobiRequestService.AutoSize = true;
            this.lblBuildObymobiRequestService.Location = new System.Drawing.Point(10, 216);
            this.lblBuildObymobiRequestService.Name = "lblBuildObymobiRequestService";
            this.lblBuildObymobiRequestService.Size = new System.Drawing.Size(266, 18);
            this.lblBuildObymobiRequestService.TabIndex = 4;
            this.lblBuildObymobiRequestService.Text = "7. Build Crave On-site Server (Release)";
            // 
            // lblBuildObymobiCms
            // 
            this.lblBuildObymobiCms.AutoSize = true;
            this.lblBuildObymobiCms.Location = new System.Drawing.Point(10, 90);
            this.lblBuildObymobiCms.Name = "lblBuildObymobiCms";
            this.lblBuildObymobiCms.Size = new System.Drawing.Size(215, 18);
            this.lblBuildObymobiCms.TabIndex = 3;
            this.lblBuildObymobiCms.Text = "2. Build Obymobi CMS (Debug)";
            // 
            // lblBuildObymobiWebservice
            // 
            this.lblBuildObymobiWebservice.AutoSize = true;
            this.lblBuildObymobiWebservice.Location = new System.Drawing.Point(10, 65);
            this.lblBuildObymobiWebservice.Name = "lblBuildObymobiWebservice";
            this.lblBuildObymobiWebservice.Size = new System.Drawing.Size(259, 18);
            this.lblBuildObymobiWebservice.TabIndex = 2;
            this.lblBuildObymobiWebservice.Text = "1. Build Obymobi Webservice (Debug)";
            // 
            // lblUpdateWebreferences
            // 
            this.lblUpdateWebreferences.AutoSize = true;
            this.lblUpdateWebreferences.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdateWebreferences.ForeColor = System.Drawing.Color.Red;
            this.lblUpdateWebreferences.Location = new System.Drawing.Point(288, 10);
            this.lblUpdateWebreferences.Name = "lblUpdateWebreferences";
            this.lblUpdateWebreferences.Size = new System.Drawing.Size(319, 18);
            this.lblUpdateWebreferences.TabIndex = 1;
            this.lblUpdateWebreferences.Text = "Don\'t forget to update the webreferences!";
            // 
            // llOpenObymobiSolution
            // 
            this.llOpenObymobiSolution.AutoSize = true;
            this.llOpenObymobiSolution.Location = new System.Drawing.Point(10, 10);
            this.llOpenObymobiSolution.Name = "llOpenObymobiSolution";
            this.llOpenObymobiSolution.Size = new System.Drawing.Size(272, 18);
            this.llOpenObymobiSolution.TabIndex = 0;
            this.llOpenObymobiSolution.TabStop = true;
            this.llOpenObymobiSolution.Text = "Click here to open the Obymobi solution";
            // 
            // tpCreateReleases
            // 
            this.tpCreateReleases.Controls.Add(this.cbPublishCraveOnsiteSupportTools);
            this.tpCreateReleases.Controls.Add(this.cbCraveOnSiteSupportTools);
            this.tpCreateReleases.Controls.Add(this.cbCreateInstallerCraveOnsiteSupportTools);
            this.tpCreateReleases.Controls.Add(this.cbObfuscateCraveOnsiteSupportTools);
            this.tpCreateReleases.Controls.Add(this.cbFtpCloudSoftware);
            this.tpCreateReleases.Controls.Add(this.cbReleasesSelectAll);
            this.tpCreateReleases.Controls.Add(this.cbCraveOnSiteAgent);
            this.tpCreateReleases.Controls.Add(this.cbCraveOnSiteServer);
            this.tpCreateReleases.Controls.Add(this.cbCraveCloud);
            this.tpCreateReleases.Controls.Add(this.cbPublishAllSoftware);
            this.tpCreateReleases.Controls.Add(this.cbCreateInstallersForAllSoftware);
            this.tpCreateReleases.Controls.Add(this.cbObfuscateAllSoftware);
            this.tpCreateReleases.Controls.Add(this.cbPublishCraveCloud);
            this.tpCreateReleases.Controls.Add(this.cbPublishCraveOnsiteAgent);
            this.tpCreateReleases.Controls.Add(this.cbPublishCraveOnsiteServer);
            this.tpCreateReleases.Controls.Add(this.cbCreateInstallerCraveOnsiteAgent);
            this.tpCreateReleases.Controls.Add(this.cbCreateInstallerCraveOnsiteServer);
            this.tpCreateReleases.Controls.Add(this.cbObfuscateCraveOnsiteAgent);
            this.tpCreateReleases.Controls.Add(this.cbObfuscateCraveOnsiteServer);
            this.tpCreateReleases.Controls.Add(this.btnCreateReleasesForSelectedSoftware);
            this.tpCreateReleases.Location = new System.Drawing.Point(4, 27);
            this.tpCreateReleases.Name = "tpCreateReleases";
            this.tpCreateReleases.Padding = new System.Windows.Forms.Padding(3);
            this.tpCreateReleases.Size = new System.Drawing.Size(1284, 393);
            this.tpCreateReleases.TabIndex = 2;
            this.tpCreateReleases.Text = "Create Releases";
            this.tpCreateReleases.UseVisualStyleBackColor = true;
            // 
            // cbFtpCloudSoftware
            // 
            this.cbFtpCloudSoftware.AutoSize = true;
            this.cbFtpCloudSoftware.Location = new System.Drawing.Point(791, 41);
            this.cbFtpCloudSoftware.Name = "cbFtpCloudSoftware";
            this.cbFtpCloudSoftware.Size = new System.Drawing.Size(119, 22);
            this.cbFtpCloudSoftware.TabIndex = 49;
            this.cbFtpCloudSoftware.Text = "FTP to Chuck";
            this.cbFtpCloudSoftware.UseVisualStyleBackColor = true;
            // 
            // cbReleasesSelectAll
            // 
            this.cbReleasesSelectAll.AutoSize = true;
            this.cbReleasesSelectAll.Checked = true;
            this.cbReleasesSelectAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbReleasesSelectAll.Location = new System.Drawing.Point(20, 13);
            this.cbReleasesSelectAll.Name = "cbReleasesSelectAll";
            this.cbReleasesSelectAll.Size = new System.Drawing.Size(120, 22);
            this.cbReleasesSelectAll.TabIndex = 48;
            this.cbReleasesSelectAll.Text = "(De-)Select all";
            this.cbReleasesSelectAll.UseVisualStyleBackColor = true;
            // 
            // cbCraveOnSiteAgent
            // 
            this.cbCraveOnSiteAgent.AutoSize = true;
            this.cbCraveOnSiteAgent.Checked = true;
            this.cbCraveOnSiteAgent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCraveOnSiteAgent.Location = new System.Drawing.Point(20, 97);
            this.cbCraveOnSiteAgent.Name = "cbCraveOnSiteAgent";
            this.cbCraveOnSiteAgent.Size = new System.Drawing.Size(161, 22);
            this.cbCraveOnSiteAgent.TabIndex = 41;
            this.cbCraveOnSiteAgent.Text = "Crave On-Site Agent";
            this.cbCraveOnSiteAgent.UseVisualStyleBackColor = true;
            // 
            // cbCraveOnSiteServer
            // 
            this.cbCraveOnSiteServer.AutoSize = true;
            this.cbCraveOnSiteServer.Checked = true;
            this.cbCraveOnSiteServer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCraveOnSiteServer.Location = new System.Drawing.Point(20, 69);
            this.cbCraveOnSiteServer.Name = "cbCraveOnSiteServer";
            this.cbCraveOnSiteServer.Size = new System.Drawing.Size(167, 22);
            this.cbCraveOnSiteServer.TabIndex = 40;
            this.cbCraveOnSiteServer.Text = "Crave On-Site Server";
            this.cbCraveOnSiteServer.UseVisualStyleBackColor = true;
            // 
            // cbCraveCloud
            // 
            this.cbCraveCloud.AutoSize = true;
            this.cbCraveCloud.Checked = true;
            this.cbCraveCloud.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCraveCloud.Location = new System.Drawing.Point(20, 41);
            this.cbCraveCloud.Name = "cbCraveCloud";
            this.cbCraveCloud.Size = new System.Drawing.Size(172, 22);
            this.cbCraveCloud.TabIndex = 38;
            this.cbCraveCloud.Text = "Crave Cloud Software";
            this.cbCraveCloud.UseVisualStyleBackColor = true;
            // 
            // cbPublishAllSoftware
            // 
            this.cbPublishAllSoftware.AutoSize = true;
            this.cbPublishAllSoftware.Location = new System.Drawing.Point(607, 169);
            this.cbPublishAllSoftware.Name = "cbPublishAllSoftware";
            this.cbPublishAllSoftware.Size = new System.Drawing.Size(159, 22);
            this.cbPublishAllSoftware.TabIndex = 37;
            this.cbPublishAllSoftware.Text = "Publish on DropBox";
            this.cbPublishAllSoftware.UseVisualStyleBackColor = true;
            // 
            // cbCreateInstallersForAllSoftware
            // 
            this.cbCreateInstallersForAllSoftware.AutoSize = true;
            this.cbCreateInstallersForAllSoftware.Location = new System.Drawing.Point(462, 169);
            this.cbCreateInstallersForAllSoftware.Name = "cbCreateInstallersForAllSoftware";
            this.cbCreateInstallersForAllSoftware.Size = new System.Drawing.Size(125, 22);
            this.cbCreateInstallersForAllSoftware.TabIndex = 36;
            this.cbCreateInstallersForAllSoftware.Text = "Create installer";
            this.cbCreateInstallersForAllSoftware.UseVisualStyleBackColor = true;
            // 
            // cbObfuscateAllSoftware
            // 
            this.cbObfuscateAllSoftware.AutoSize = true;
            this.cbObfuscateAllSoftware.Enabled = false;
            this.cbObfuscateAllSoftware.Location = new System.Drawing.Point(347, 169);
            this.cbObfuscateAllSoftware.Name = "cbObfuscateAllSoftware";
            this.cbObfuscateAllSoftware.Size = new System.Drawing.Size(95, 22);
            this.cbObfuscateAllSoftware.TabIndex = 35;
            this.cbObfuscateAllSoftware.Text = "Obfuscate";
            this.cbObfuscateAllSoftware.UseVisualStyleBackColor = true;
            // 
            // cbPublishCraveCloud
            // 
            this.cbPublishCraveCloud.AutoSize = true;
            this.cbPublishCraveCloud.Location = new System.Drawing.Point(607, 41);
            this.cbPublishCraveCloud.Name = "cbPublishCraveCloud";
            this.cbPublishCraveCloud.Size = new System.Drawing.Size(159, 22);
            this.cbPublishCraveCloud.TabIndex = 30;
            this.cbPublishCraveCloud.Text = "Publish on DropBox";
            this.cbPublishCraveCloud.UseVisualStyleBackColor = true;
            // 
            // cbPublishCraveOnsiteAgent
            // 
            this.cbPublishCraveOnsiteAgent.AutoSize = true;
            this.cbPublishCraveOnsiteAgent.Location = new System.Drawing.Point(607, 97);
            this.cbPublishCraveOnsiteAgent.Name = "cbPublishCraveOnsiteAgent";
            this.cbPublishCraveOnsiteAgent.Size = new System.Drawing.Size(159, 22);
            this.cbPublishCraveOnsiteAgent.TabIndex = 28;
            this.cbPublishCraveOnsiteAgent.Text = "Publish on DropBox";
            this.cbPublishCraveOnsiteAgent.UseVisualStyleBackColor = true;
            // 
            // cbPublishCraveOnsiteServer
            // 
            this.cbPublishCraveOnsiteServer.AutoSize = true;
            this.cbPublishCraveOnsiteServer.Location = new System.Drawing.Point(607, 69);
            this.cbPublishCraveOnsiteServer.Name = "cbPublishCraveOnsiteServer";
            this.cbPublishCraveOnsiteServer.Size = new System.Drawing.Size(159, 22);
            this.cbPublishCraveOnsiteServer.TabIndex = 27;
            this.cbPublishCraveOnsiteServer.Text = "Publish on DropBox";
            this.cbPublishCraveOnsiteServer.UseVisualStyleBackColor = true;
            // 
            // cbCreateInstallerCraveOnsiteAgent
            // 
            this.cbCreateInstallerCraveOnsiteAgent.AutoSize = true;
            this.cbCreateInstallerCraveOnsiteAgent.Location = new System.Drawing.Point(462, 97);
            this.cbCreateInstallerCraveOnsiteAgent.Name = "cbCreateInstallerCraveOnsiteAgent";
            this.cbCreateInstallerCraveOnsiteAgent.Size = new System.Drawing.Size(125, 22);
            this.cbCreateInstallerCraveOnsiteAgent.TabIndex = 25;
            this.cbCreateInstallerCraveOnsiteAgent.Text = "Create installer";
            this.cbCreateInstallerCraveOnsiteAgent.UseVisualStyleBackColor = true;
            // 
            // cbCreateInstallerCraveOnsiteServer
            // 
            this.cbCreateInstallerCraveOnsiteServer.AutoSize = true;
            this.cbCreateInstallerCraveOnsiteServer.Location = new System.Drawing.Point(462, 69);
            this.cbCreateInstallerCraveOnsiteServer.Name = "cbCreateInstallerCraveOnsiteServer";
            this.cbCreateInstallerCraveOnsiteServer.Size = new System.Drawing.Size(125, 22);
            this.cbCreateInstallerCraveOnsiteServer.TabIndex = 24;
            this.cbCreateInstallerCraveOnsiteServer.Text = "Create installer";
            this.cbCreateInstallerCraveOnsiteServer.UseVisualStyleBackColor = true;
            // 
            // cbObfuscateCraveOnsiteAgent
            // 
            this.cbObfuscateCraveOnsiteAgent.AutoSize = true;
            this.cbObfuscateCraveOnsiteAgent.Enabled = false;
            this.cbObfuscateCraveOnsiteAgent.Location = new System.Drawing.Point(347, 97);
            this.cbObfuscateCraveOnsiteAgent.Name = "cbObfuscateCraveOnsiteAgent";
            this.cbObfuscateCraveOnsiteAgent.Size = new System.Drawing.Size(95, 22);
            this.cbObfuscateCraveOnsiteAgent.TabIndex = 22;
            this.cbObfuscateCraveOnsiteAgent.Text = "Obfuscate";
            this.cbObfuscateCraveOnsiteAgent.UseVisualStyleBackColor = true;
            // 
            // cbObfuscateCraveOnsiteServer
            // 
            this.cbObfuscateCraveOnsiteServer.AutoSize = true;
            this.cbObfuscateCraveOnsiteServer.Enabled = false;
            this.cbObfuscateCraveOnsiteServer.Location = new System.Drawing.Point(347, 69);
            this.cbObfuscateCraveOnsiteServer.Name = "cbObfuscateCraveOnsiteServer";
            this.cbObfuscateCraveOnsiteServer.Size = new System.Drawing.Size(95, 22);
            this.cbObfuscateCraveOnsiteServer.TabIndex = 21;
            this.cbObfuscateCraveOnsiteServer.Text = "Obfuscate";
            this.cbObfuscateCraveOnsiteServer.UseVisualStyleBackColor = true;
            // 
            // btnCreateReleasesForSelectedSoftware
            // 
            this.btnCreateReleasesForSelectedSoftware.BackColor = System.Drawing.Color.GreenYellow;
            this.btnCreateReleasesForSelectedSoftware.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateReleasesForSelectedSoftware.Location = new System.Drawing.Point(6, 165);
            this.btnCreateReleasesForSelectedSoftware.Name = "btnCreateReleasesForSelectedSoftware";
            this.btnCreateReleasesForSelectedSoftware.Size = new System.Drawing.Size(316, 29);
            this.btnCreateReleasesForSelectedSoftware.TabIndex = 17;
            this.btnCreateReleasesForSelectedSoftware.Text = "Create releases for selected software";
            this.btnCreateReleasesForSelectedSoftware.UseVisualStyleBackColor = false;
            // 
            // tpSyncDatabase
            // 
            this.tpSyncDatabase.Controls.Add(this.llOpenSQLDelta);
            this.tpSyncDatabase.Location = new System.Drawing.Point(4, 27);
            this.tpSyncDatabase.Name = "tpSyncDatabase";
            this.tpSyncDatabase.Padding = new System.Windows.Forms.Padding(3);
            this.tpSyncDatabase.Size = new System.Drawing.Size(1284, 393);
            this.tpSyncDatabase.TabIndex = 9;
            this.tpSyncDatabase.Text = "Sync database";
            this.tpSyncDatabase.UseVisualStyleBackColor = true;
            // 
            // llOpenSQLDelta
            // 
            this.llOpenSQLDelta.AutoSize = true;
            this.llOpenSQLDelta.Location = new System.Drawing.Point(10, 10);
            this.llOpenSQLDelta.Name = "llOpenSQLDelta";
            this.llOpenSQLDelta.Size = new System.Drawing.Size(200, 18);
            this.llOpenSQLDelta.TabIndex = 1;
            this.llOpenSQLDelta.TabStop = true;
            this.llOpenSQLDelta.Text = "Click here to open SQL Delta";
            // 
            // tpUpload
            // 
            this.tpUpload.Controls.Add(this.lblUploadFiles);
            this.tpUpload.Location = new System.Drawing.Point(4, 27);
            this.tpUpload.Name = "tpUpload";
            this.tpUpload.Padding = new System.Windows.Forms.Padding(3);
            this.tpUpload.Size = new System.Drawing.Size(1284, 393);
            this.tpUpload.TabIndex = 10;
            this.tpUpload.Text = "Upload";
            this.tpUpload.UseVisualStyleBackColor = true;
            // 
            // lblUploadFiles
            // 
            this.lblUploadFiles.AutoSize = true;
            this.lblUploadFiles.Location = new System.Drawing.Point(10, 10);
            this.lblUploadFiles.Name = "lblUploadFiles";
            this.lblUploadFiles.Size = new System.Drawing.Size(415, 18);
            this.lblUploadFiles.TabIndex = 0;
            this.lblUploadFiles.Text = "Upload the releases to the webserver using Total Commander";
            // 
            // tpRemoteDesktop
            // 
            this.tpRemoteDesktop.Controls.Add(this.llConnectToSecondaryWebserver);
            this.tpRemoteDesktop.Controls.Add(this.llConnectToPrimaryWebserver);
            this.tpRemoteDesktop.Location = new System.Drawing.Point(4, 27);
            this.tpRemoteDesktop.Name = "tpRemoteDesktop";
            this.tpRemoteDesktop.Padding = new System.Windows.Forms.Padding(3);
            this.tpRemoteDesktop.Size = new System.Drawing.Size(1284, 393);
            this.tpRemoteDesktop.TabIndex = 6;
            this.tpRemoteDesktop.Text = "Remote Desktop";
            this.tpRemoteDesktop.UseVisualStyleBackColor = true;
            // 
            // llConnectToSecondaryWebserver
            // 
            this.llConnectToSecondaryWebserver.AutoSize = true;
            this.llConnectToSecondaryWebserver.Location = new System.Drawing.Point(10, 38);
            this.llConnectToSecondaryWebserver.Name = "llConnectToSecondaryWebserver";
            this.llConnectToSecondaryWebserver.Size = new System.Drawing.Size(436, 18);
            this.llConnectToSecondaryWebserver.TabIndex = 6;
            this.llConnectToSecondaryWebserver.TabStop = true;
            this.llConnectToSecondaryWebserver.Text = "Click here to connect to the secondary webserver (Chuck Norris)";
            // 
            // llConnectToPrimaryWebserver
            // 
            this.llConnectToPrimaryWebserver.AutoSize = true;
            this.llConnectToPrimaryWebserver.Location = new System.Drawing.Point(10, 10);
            this.llConnectToPrimaryWebserver.Name = "llConnectToPrimaryWebserver";
            this.llConnectToPrimaryWebserver.Size = new System.Drawing.Size(370, 18);
            this.llConnectToPrimaryWebserver.TabIndex = 5;
            this.llConnectToPrimaryWebserver.TabStop = true;
            this.llConnectToPrimaryWebserver.Text = "Click here to connect to the primary webserver (Falkor)";
            // 
            // tabTest
            // 
            this.tabTest.Controls.Add(this.gbLive);
            this.tabTest.Controls.Add(this.gbTest);
            this.tabTest.Controls.Add(this.gbDevelopment);
            this.tabTest.Location = new System.Drawing.Point(4, 27);
            this.tabTest.Name = "tabTest";
            this.tabTest.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest.Size = new System.Drawing.Size(1284, 393);
            this.tabTest.TabIndex = 13;
            this.tabTest.Text = "Test";
            this.tabTest.UseVisualStyleBackColor = true;
            // 
            // gbLive
            // 
            this.gbLive.Controls.Add(this.llOpenNocLive);
            this.gbLive.Controls.Add(this.llOpenMobileLive);
            this.gbLive.Controls.Add(this.llOpenConsoleLive);
            this.gbLive.Controls.Add(this.llOpenCmsLive);
            this.gbLive.Controls.Add(this.llOpenWebserviceLive);
            this.gbLive.Location = new System.Drawing.Point(861, 13);
            this.gbLive.Name = "gbLive";
            this.gbLive.Size = new System.Drawing.Size(404, 163);
            this.gbLive.TabIndex = 14;
            this.gbLive.TabStop = false;
            this.gbLive.Text = "Live";
            // 
            // llOpenNocLive
            // 
            this.llOpenNocLive.AutoSize = true;
            this.llOpenNocLive.Location = new System.Drawing.Point(12, 128);
            this.llOpenNocLive.Name = "llOpenNocLive";
            this.llOpenNocLive.Size = new System.Drawing.Size(190, 18);
            this.llOpenNocLive.TabIndex = 12;
            this.llOpenNocLive.TabStop = true;
            this.llOpenNocLive.Text = "Click here to open the NOC";
            // 
            // llOpenMobileLive
            // 
            this.llOpenMobileLive.AutoSize = true;
            this.llOpenMobileLive.Location = new System.Drawing.Point(12, 101);
            this.llOpenMobileLive.Name = "llOpenMobileLive";
            this.llOpenMobileLive.Size = new System.Drawing.Size(200, 18);
            this.llOpenMobileLive.TabIndex = 11;
            this.llOpenMobileLive.TabStop = true;
            this.llOpenMobileLive.Text = "Click here to open the mobile";
            // 
            // llOpenConsoleLive
            // 
            this.llOpenConsoleLive.AutoSize = true;
            this.llOpenConsoleLive.Location = new System.Drawing.Point(12, 75);
            this.llOpenConsoleLive.Name = "llOpenConsoleLive";
            this.llOpenConsoleLive.Size = new System.Drawing.Size(209, 18);
            this.llOpenConsoleLive.TabIndex = 10;
            this.llOpenConsoleLive.TabStop = true;
            this.llOpenConsoleLive.Text = "Click here to open the console";
            // 
            // llOpenCmsLive
            // 
            this.llOpenCmsLive.AutoSize = true;
            this.llOpenCmsLive.Location = new System.Drawing.Point(12, 49);
            this.llOpenCmsLive.Name = "llOpenCmsLive";
            this.llOpenCmsLive.Size = new System.Drawing.Size(190, 18);
            this.llOpenCmsLive.TabIndex = 9;
            this.llOpenCmsLive.TabStop = true;
            this.llOpenCmsLive.Text = "Click here to open the CMS";
            // 
            // llOpenWebserviceLive
            // 
            this.llOpenWebserviceLive.AutoSize = true;
            this.llOpenWebserviceLive.Location = new System.Drawing.Point(12, 24);
            this.llOpenWebserviceLive.Name = "llOpenWebserviceLive";
            this.llOpenWebserviceLive.Size = new System.Drawing.Size(230, 18);
            this.llOpenWebserviceLive.TabIndex = 8;
            this.llOpenWebserviceLive.TabStop = true;
            this.llOpenWebserviceLive.Text = "Click here to open the webservice";
            // 
            // gbTest
            // 
            this.gbTest.Controls.Add(this.llOpenNocTest);
            this.gbTest.Controls.Add(this.llOpenMobileTest);
            this.gbTest.Controls.Add(this.llOpenConsoleTest);
            this.gbTest.Controls.Add(this.llOpenCmsTest);
            this.gbTest.Controls.Add(this.llOpenWebserviceTest);
            this.gbTest.Location = new System.Drawing.Point(441, 13);
            this.gbTest.Name = "gbTest";
            this.gbTest.Size = new System.Drawing.Size(404, 163);
            this.gbTest.TabIndex = 13;
            this.gbTest.TabStop = false;
            this.gbTest.Text = "Test";
            // 
            // llOpenNocTest
            // 
            this.llOpenNocTest.AutoSize = true;
            this.llOpenNocTest.Location = new System.Drawing.Point(12, 128);
            this.llOpenNocTest.Name = "llOpenNocTest";
            this.llOpenNocTest.Size = new System.Drawing.Size(190, 18);
            this.llOpenNocTest.TabIndex = 12;
            this.llOpenNocTest.TabStop = true;
            this.llOpenNocTest.Text = "Click here to open the NOC";
            // 
            // llOpenMobileTest
            // 
            this.llOpenMobileTest.AutoSize = true;
            this.llOpenMobileTest.Location = new System.Drawing.Point(12, 101);
            this.llOpenMobileTest.Name = "llOpenMobileTest";
            this.llOpenMobileTest.Size = new System.Drawing.Size(200, 18);
            this.llOpenMobileTest.TabIndex = 11;
            this.llOpenMobileTest.TabStop = true;
            this.llOpenMobileTest.Text = "Click here to open the mobile";
            // 
            // llOpenConsoleTest
            // 
            this.llOpenConsoleTest.AutoSize = true;
            this.llOpenConsoleTest.Location = new System.Drawing.Point(12, 75);
            this.llOpenConsoleTest.Name = "llOpenConsoleTest";
            this.llOpenConsoleTest.Size = new System.Drawing.Size(209, 18);
            this.llOpenConsoleTest.TabIndex = 10;
            this.llOpenConsoleTest.TabStop = true;
            this.llOpenConsoleTest.Text = "Click here to open the console";
            // 
            // llOpenCmsTest
            // 
            this.llOpenCmsTest.AutoSize = true;
            this.llOpenCmsTest.Location = new System.Drawing.Point(12, 49);
            this.llOpenCmsTest.Name = "llOpenCmsTest";
            this.llOpenCmsTest.Size = new System.Drawing.Size(190, 18);
            this.llOpenCmsTest.TabIndex = 9;
            this.llOpenCmsTest.TabStop = true;
            this.llOpenCmsTest.Text = "Click here to open the CMS";
            // 
            // llOpenWebserviceTest
            // 
            this.llOpenWebserviceTest.AutoSize = true;
            this.llOpenWebserviceTest.Location = new System.Drawing.Point(12, 24);
            this.llOpenWebserviceTest.Name = "llOpenWebserviceTest";
            this.llOpenWebserviceTest.Size = new System.Drawing.Size(230, 18);
            this.llOpenWebserviceTest.TabIndex = 8;
            this.llOpenWebserviceTest.TabStop = true;
            this.llOpenWebserviceTest.Text = "Click here to open the webservice";
            // 
            // gbDevelopment
            // 
            this.gbDevelopment.Controls.Add(this.llOpenNocDev);
            this.gbDevelopment.Controls.Add(this.llOpenMobileDev);
            this.gbDevelopment.Controls.Add(this.llOpenConsoleDev);
            this.gbDevelopment.Controls.Add(this.llOpenCmsDev);
            this.gbDevelopment.Controls.Add(this.llOpenWebserviceDev);
            this.gbDevelopment.Location = new System.Drawing.Point(19, 13);
            this.gbDevelopment.Name = "gbDevelopment";
            this.gbDevelopment.Size = new System.Drawing.Size(404, 163);
            this.gbDevelopment.TabIndex = 0;
            this.gbDevelopment.TabStop = false;
            this.gbDevelopment.Text = "Development";
            // 
            // llOpenNocDev
            // 
            this.llOpenNocDev.AutoSize = true;
            this.llOpenNocDev.Location = new System.Drawing.Point(12, 128);
            this.llOpenNocDev.Name = "llOpenNocDev";
            this.llOpenNocDev.Size = new System.Drawing.Size(190, 18);
            this.llOpenNocDev.TabIndex = 12;
            this.llOpenNocDev.TabStop = true;
            this.llOpenNocDev.Text = "Click here to open the NOC";
            // 
            // llOpenMobileDev
            // 
            this.llOpenMobileDev.AutoSize = true;
            this.llOpenMobileDev.Location = new System.Drawing.Point(12, 101);
            this.llOpenMobileDev.Name = "llOpenMobileDev";
            this.llOpenMobileDev.Size = new System.Drawing.Size(200, 18);
            this.llOpenMobileDev.TabIndex = 11;
            this.llOpenMobileDev.TabStop = true;
            this.llOpenMobileDev.Text = "Click here to open the mobile";
            // 
            // llOpenConsoleDev
            // 
            this.llOpenConsoleDev.AutoSize = true;
            this.llOpenConsoleDev.Location = new System.Drawing.Point(12, 75);
            this.llOpenConsoleDev.Name = "llOpenConsoleDev";
            this.llOpenConsoleDev.Size = new System.Drawing.Size(209, 18);
            this.llOpenConsoleDev.TabIndex = 10;
            this.llOpenConsoleDev.TabStop = true;
            this.llOpenConsoleDev.Text = "Click here to open the console";
            // 
            // llOpenCmsDev
            // 
            this.llOpenCmsDev.AutoSize = true;
            this.llOpenCmsDev.Location = new System.Drawing.Point(12, 49);
            this.llOpenCmsDev.Name = "llOpenCmsDev";
            this.llOpenCmsDev.Size = new System.Drawing.Size(190, 18);
            this.llOpenCmsDev.TabIndex = 9;
            this.llOpenCmsDev.TabStop = true;
            this.llOpenCmsDev.Text = "Click here to open the CMS";
            // 
            // llOpenWebserviceDev
            // 
            this.llOpenWebserviceDev.AutoSize = true;
            this.llOpenWebserviceDev.Location = new System.Drawing.Point(12, 24);
            this.llOpenWebserviceDev.Name = "llOpenWebserviceDev";
            this.llOpenWebserviceDev.Size = new System.Drawing.Size(230, 18);
            this.llOpenWebserviceDev.TabIndex = 8;
            this.llOpenWebserviceDev.TabStop = true;
            this.llOpenWebserviceDev.Text = "Click here to open the webservice";
            // 
            // tpCmsMaintenance
            // 
            this.tpCmsMaintenance.Controls.Add(this.lblClearCache);
            this.tpCmsMaintenance.Controls.Add(this.lblRefreshAllViews);
            this.tpCmsMaintenance.Controls.Add(this.lblRefreshInfoInLocalDatabase);
            this.tpCmsMaintenance.Location = new System.Drawing.Point(4, 27);
            this.tpCmsMaintenance.Name = "tpCmsMaintenance";
            this.tpCmsMaintenance.Padding = new System.Windows.Forms.Padding(3);
            this.tpCmsMaintenance.Size = new System.Drawing.Size(1284, 393);
            this.tpCmsMaintenance.TabIndex = 7;
            this.tpCmsMaintenance.Text = "CMS Maintenance";
            this.tpCmsMaintenance.UseVisualStyleBackColor = true;
            // 
            // lblClearCache
            // 
            this.lblClearCache.AutoSize = true;
            this.lblClearCache.Location = new System.Drawing.Point(10, 64);
            this.lblClearCache.Name = "lblClearCache";
            this.lblClearCache.Size = new System.Drawing.Size(127, 18);
            this.lblClearCache.TabIndex = 3;
            this.lblClearCache.Text = "3. Clear the cache";
            // 
            // lblRefreshAllViews
            // 
            this.lblRefreshAllViews.AutoSize = true;
            this.lblRefreshAllViews.Location = new System.Drawing.Point(10, 38);
            this.lblRefreshAllViews.Name = "lblRefreshAllViews";
            this.lblRefreshAllViews.Size = new System.Drawing.Size(297, 18);
            this.lblRefreshAllViews.TabIndex = 2;
            this.lblRefreshAllViews.Text = "2. Refresh all the views in master and locally";
            // 
            // lblRefreshInfoInLocalDatabase
            // 
            this.lblRefreshInfoInLocalDatabase.AutoSize = true;
            this.lblRefreshInfoInLocalDatabase.Location = new System.Drawing.Point(10, 12);
            this.lblRefreshInfoInLocalDatabase.Name = "lblRefreshInfoInLocalDatabase";
            this.lblRefreshInfoInLocalDatabase.Size = new System.Drawing.Size(266, 18);
            this.lblRefreshInfoInLocalDatabase.TabIndex = 1;
            this.lblRefreshInfoInLocalDatabase.Text = "1. Refresh the info in the local database";
            // 
            // tpCmsReleases
            // 
            this.tpCmsReleases.Controls.Add(this.label4);
            this.tpCmsReleases.Location = new System.Drawing.Point(4, 27);
            this.tpCmsReleases.Name = "tpCmsReleases";
            this.tpCmsReleases.Padding = new System.Windows.Forms.Padding(3);
            this.tpCmsReleases.Size = new System.Drawing.Size(1284, 393);
            this.tpCmsReleases.TabIndex = 11;
            this.tpCmsReleases.Text = "CMS Releases";
            this.tpCmsReleases.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(212, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "1. Add the releases to the CMS";
            // 
            // tpDeliver
            // 
            this.tpDeliver.Controls.Add(this.llGoToPivotal);
            this.tpDeliver.Controls.Add(this.lblSetStoriesToDelivered);
            this.tpDeliver.Location = new System.Drawing.Point(4, 27);
            this.tpDeliver.Name = "tpDeliver";
            this.tpDeliver.Padding = new System.Windows.Forms.Padding(3);
            this.tpDeliver.Size = new System.Drawing.Size(1284, 393);
            this.tpDeliver.TabIndex = 12;
            this.tpDeliver.Text = "Deliver on Pivotal";
            this.tpDeliver.UseVisualStyleBackColor = true;
            // 
            // llGoToPivotal
            // 
            this.llGoToPivotal.AutoSize = true;
            this.llGoToPivotal.Location = new System.Drawing.Point(10, 10);
            this.llGoToPivotal.Name = "llGoToPivotal";
            this.llGoToPivotal.Size = new System.Drawing.Size(231, 18);
            this.llGoToPivotal.TabIndex = 6;
            this.llGoToPivotal.TabStop = true;
            this.llGoToPivotal.Text = "Click here to open Pivotal Tracker";
            // 
            // lblSetStoriesToDelivered
            // 
            this.lblSetStoriesToDelivered.AutoSize = true;
            this.lblSetStoriesToDelivered.Location = new System.Drawing.Point(10, 37);
            this.lblSetStoriesToDelivered.Name = "lblSetStoriesToDelivered";
            this.lblSetStoriesToDelivered.Size = new System.Drawing.Size(261, 18);
            this.lblSetStoriesToDelivered.TabIndex = 2;
            this.lblSetStoriesToDelivered.Text = "1. Set the finished stories to \'Delivered\'";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ucNewRelicDeployment);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1284, 393);
            this.tabPage1.TabIndex = 14;
            this.tabPage1.Text = "New Relic Deployment";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ucNewRelicDeployment
            // 
            this.ucNewRelicDeployment.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucNewRelicDeployment.Location = new System.Drawing.Point(0, 0);
            this.ucNewRelicDeployment.Margin = new System.Windows.Forms.Padding(4);
            this.ucNewRelicDeployment.Name = "ucNewRelicDeployment";
            this.ucNewRelicDeployment.Size = new System.Drawing.Size(739, 201);
            this.ucNewRelicDeployment.TabIndex = 0;
            // 
            // tbLog
            // 
            this.tbLog.BackColor = System.Drawing.Color.White;
            this.tbLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLog.Location = new System.Drawing.Point(3, 533);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ReadOnly = true;
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(1292, 212);
            this.tbLog.TabIndex = 1;
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.lblProgress);
            this.pnlTop.Controls.Add(this.progressBar);
            this.pnlTop.Controls.Add(this.pbLogo);
            this.pnlTop.Controls.Add(this.menuStrip1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTop.Location = new System.Drawing.Point(3, 3);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(1292, 94);
            this.pnlTop.TabIndex = 2;
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgress.Location = new System.Drawing.Point(186, 68);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(70, 18);
            this.lblProgress.TabIndex = 7;
            this.lblProgress.Text = "Ready...";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(184, 30);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1088, 33);
            this.progressBar.TabIndex = 6;
            // 
            // pbLogo
            // 
            this.pbLogo.Image = global::CraveBuilder.Images.crave_logo;
            this.pbLogo.Location = new System.Drawing.Point(0, 22);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(168, 70);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 5;
            this.pbLogo.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.buildToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.androidToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1292, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.configurationToolStripMenuItem.Text = "Exit";
            // 
            // buildToolStripMenuItem
            // 
            this.buildToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buildAllToolStripMenuItem,
            this.toolStripSeparator1,
            this.configurationToolStripMenuItem1});
            this.buildToolStripMenuItem.Name = "buildToolStripMenuItem";
            this.buildToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.buildToolStripMenuItem.Text = "Build";
            // 
            // buildAllToolStripMenuItem
            // 
            this.buildAllToolStripMenuItem.Name = "buildAllToolStripMenuItem";
            this.buildAllToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.buildAllToolStripMenuItem.Text = "Build All";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(154, 6);
            // 
            // configurationToolStripMenuItem1
            // 
            this.configurationToolStripMenuItem1.Name = "configurationToolStripMenuItem1";
            this.configurationToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.configurationToolStripMenuItem1.Text = "Configuration...";
            this.configurationToolStripMenuItem1.Click += new System.EventHandler(this.configurationToolStripMenuItem1_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNewWebserviceVersionToolStripMenuItem,
            this.generateChangelogFromSvnToolStripMenuItem,
            this.toolStripSeparator2,
            this.openWebserviceTesterToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // createNewWebserviceVersionToolStripMenuItem
            // 
            this.createNewWebserviceVersionToolStripMenuItem.Name = "createNewWebserviceVersionToolStripMenuItem";
            this.createNewWebserviceVersionToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.createNewWebserviceVersionToolStripMenuItem.Text = "Create new webservice version...";
            // 
            // generateChangelogFromSvnToolStripMenuItem
            // 
            this.generateChangelogFromSvnToolStripMenuItem.Name = "generateChangelogFromSvnToolStripMenuItem";
            this.generateChangelogFromSvnToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.generateChangelogFromSvnToolStripMenuItem.Text = "Generate changelog from SVN";
            this.generateChangelogFromSvnToolStripMenuItem.Click += new System.EventHandler(this.generateChangelogFromSvnToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(241, 6);
            // 
            // openWebserviceTesterToolStripMenuItem
            // 
            this.openWebserviceTesterToolStripMenuItem.Name = "openWebserviceTesterToolStripMenuItem";
            this.openWebserviceTesterToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.openWebserviceTesterToolStripMenuItem.Text = "Open webservice tester";
            // 
            // androidToolStripMenuItem1
            // 
            this.androidToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.androidToolStripMenuItem});
            this.androidToolStripMenuItem1.Name = "androidToolStripMenuItem1";
            this.androidToolStripMenuItem1.Size = new System.Drawing.Size(62, 20);
            this.androidToolStripMenuItem1.Text = "Android";
            // 
            // androidToolStripMenuItem
            // 
            this.androidToolStripMenuItem.Name = "androidToolStripMenuItem";
            this.androidToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.androidToolStripMenuItem.Text = "Android Builder";
            // 
            // btnUpdateCraveOnsiteSupportToolsVersion
            // 
            this.btnUpdateCraveOnsiteSupportToolsVersion.Location = new System.Drawing.Point(9, 177);
            this.btnUpdateCraveOnsiteSupportToolsVersion.Name = "btnUpdateCraveOnsiteSupportToolsVersion";
            this.btnUpdateCraveOnsiteSupportToolsVersion.Size = new System.Drawing.Size(664, 29);
            this.btnUpdateCraveOnsiteSupportToolsVersion.TabIndex = 45;
            this.btnUpdateCraveOnsiteSupportToolsVersion.Text = "Update version of Crave On-site SupportTools";
            this.btnUpdateCraveOnsiteSupportToolsVersion.UseVisualStyleBackColor = true;
            // 
            // lblLastVersionCraveOnSiteSupportTools
            // 
            this.lblLastVersionCraveOnSiteSupportTools.AutoSize = true;
            this.lblLastVersionCraveOnSiteSupportTools.Location = new System.Drawing.Point(695, 177);
            this.lblLastVersionCraveOnSiteSupportTools.Name = "lblLastVersionCraveOnSiteSupportTools";
            this.lblLastVersionCraveOnSiteSupportTools.Size = new System.Drawing.Size(20, 18);
            this.lblLastVersionCraveOnSiteSupportTools.TabIndex = 46;
            this.lblLastVersionCraveOnSiteSupportTools.Text = "...";
            // 
            // lblSourceVersionOnsiteSupportTools
            // 
            this.lblSourceVersionOnsiteSupportTools.AutoSize = true;
            this.lblSourceVersionOnsiteSupportTools.Location = new System.Drawing.Point(855, 182);
            this.lblSourceVersionOnsiteSupportTools.Name = "lblSourceVersionOnsiteSupportTools";
            this.lblSourceVersionOnsiteSupportTools.Size = new System.Drawing.Size(100, 18);
            this.lblSourceVersionOnsiteSupportTools.TabIndex = 47;
            this.lblSourceVersionOnsiteSupportTools.Text = "1.0000000000";
            // 
            // lblLastVersionCraveOnSiteSupportToolsInstaller
            // 
            this.lblLastVersionCraveOnSiteSupportToolsInstaller.AutoSize = true;
            this.lblLastVersionCraveOnSiteSupportToolsInstaller.Location = new System.Drawing.Point(1025, 177);
            this.lblLastVersionCraveOnSiteSupportToolsInstaller.Name = "lblLastVersionCraveOnSiteSupportToolsInstaller";
            this.lblLastVersionCraveOnSiteSupportToolsInstaller.Size = new System.Drawing.Size(20, 18);
            this.lblLastVersionCraveOnSiteSupportToolsInstaller.TabIndex = 48;
            this.lblLastVersionCraveOnSiteSupportToolsInstaller.Text = "...";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 296);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(534, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "10. Update web reference \'CraveService\' for project Crave On-site SupportTools";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 323);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(321, 18);
            this.label9.TabIndex = 18;
            this.label9.Text = "11. Build Crave On-site SupportTools (Release)";
            // 
            // cbCraveOnSiteSupportTools
            // 
            this.cbCraveOnSiteSupportTools.AutoSize = true;
            this.cbCraveOnSiteSupportTools.Checked = true;
            this.cbCraveOnSiteSupportTools.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCraveOnSiteSupportTools.Location = new System.Drawing.Point(20, 125);
            this.cbCraveOnSiteSupportTools.Name = "cbCraveOnSiteSupportTools";
            this.cbCraveOnSiteSupportTools.Size = new System.Drawing.Size(214, 22);
            this.cbCraveOnSiteSupportTools.TabIndex = 52;
            this.cbCraveOnSiteSupportTools.Text = "Crave On-Site SupportTools";
            this.cbCraveOnSiteSupportTools.UseVisualStyleBackColor = true;
            // 
            // cbCreateInstallerCraveOnsiteSupportTools
            // 
            this.cbCreateInstallerCraveOnsiteSupportTools.AutoSize = true;
            this.cbCreateInstallerCraveOnsiteSupportTools.Location = new System.Drawing.Point(462, 125);
            this.cbCreateInstallerCraveOnsiteSupportTools.Name = "cbCreateInstallerCraveOnsiteSupportTools";
            this.cbCreateInstallerCraveOnsiteSupportTools.Size = new System.Drawing.Size(125, 22);
            this.cbCreateInstallerCraveOnsiteSupportTools.TabIndex = 51;
            this.cbCreateInstallerCraveOnsiteSupportTools.Text = "Create installer";
            this.cbCreateInstallerCraveOnsiteSupportTools.UseVisualStyleBackColor = true;
            // 
            // cbObfuscateCraveOnsiteSupportTools
            // 
            this.cbObfuscateCraveOnsiteSupportTools.AutoSize = true;
            this.cbObfuscateCraveOnsiteSupportTools.Enabled = false;
            this.cbObfuscateCraveOnsiteSupportTools.Location = new System.Drawing.Point(347, 125);
            this.cbObfuscateCraveOnsiteSupportTools.Name = "cbObfuscateCraveOnsiteSupportTools";
            this.cbObfuscateCraveOnsiteSupportTools.Size = new System.Drawing.Size(95, 22);
            this.cbObfuscateCraveOnsiteSupportTools.TabIndex = 50;
            this.cbObfuscateCraveOnsiteSupportTools.Text = "Obfuscate";
            this.cbObfuscateCraveOnsiteSupportTools.UseVisualStyleBackColor = true;
            // 
            // cbPublishCraveOnsiteSupportTools
            // 
            this.cbPublishCraveOnsiteSupportTools.AutoSize = true;
            this.cbPublishCraveOnsiteSupportTools.Location = new System.Drawing.Point(607, 125);
            this.cbPublishCraveOnsiteSupportTools.Name = "cbPublishCraveOnsiteSupportTools";
            this.cbPublishCraveOnsiteSupportTools.Size = new System.Drawing.Size(159, 22);
            this.cbPublishCraveOnsiteSupportTools.TabIndex = 53;
            this.cbPublishCraveOnsiteSupportTools.Text = "Publish on DropBox";
            this.cbPublishCraveOnsiteSupportTools.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 748);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMain";
            this.Text = "Crave Builder";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tcMain.ResumeLayout(false);
            this.tbStart.ResumeLayout(false);
            this.tbStart.PerformLayout();
            this.tpBuild.ResumeLayout(false);
            this.tpBuild.PerformLayout();
            this.tpCreateReleases.ResumeLayout(false);
            this.tpCreateReleases.PerformLayout();
            this.tpSyncDatabase.ResumeLayout(false);
            this.tpSyncDatabase.PerformLayout();
            this.tpUpload.ResumeLayout(false);
            this.tpUpload.PerformLayout();
            this.tpRemoteDesktop.ResumeLayout(false);
            this.tpRemoteDesktop.PerformLayout();
            this.tabTest.ResumeLayout(false);
            this.gbLive.ResumeLayout(false);
            this.gbLive.PerformLayout();
            this.gbTest.ResumeLayout(false);
            this.gbTest.PerformLayout();
            this.gbDevelopment.ResumeLayout(false);
            this.gbDevelopment.PerformLayout();
            this.tpCmsMaintenance.ResumeLayout(false);
            this.tpCmsMaintenance.PerformLayout();
            this.tpCmsReleases.ResumeLayout(false);
            this.tpCmsReleases.PerformLayout();
            this.tpDeliver.ResumeLayout(false);
            this.tpDeliver.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tbStart;
        private System.Windows.Forms.TabPage tpBuild;
        private System.Windows.Forms.TabPage tpCreateReleases;
        private System.Windows.Forms.TabPage tpRemoteDesktop;
        private System.Windows.Forms.TabPage tpCmsMaintenance;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buildToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buildAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem1;
        private System.Windows.Forms.TextBox tbVersion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdateAllSoftwareVersion;
        private System.Windows.Forms.Button btnUpdateCraveOnsiteAgentVersion;
        private System.Windows.Forms.Button btnUpdateCraveOnsiteServerVersion;
        private System.Windows.Forms.Button btnUpdateCraveCloudVersion;
        private System.Windows.Forms.LinkLabel llOpenObymobiSolution;
        private System.Windows.Forms.Button btnCreateReleasesForSelectedSoftware;
        private System.Windows.Forms.LinkLabel llConnectToSecondaryWebserver;
        private System.Windows.Forms.LinkLabel llConnectToPrimaryWebserver;
        private System.Windows.Forms.Label lblUpdateWebreferences;
        private System.Windows.Forms.Label lblBuildCraveOnsiteAgent;
        private System.Windows.Forms.Label lblBuildObymobiCms;
        private System.Windows.Forms.Label lblBuildObymobiWebservice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUpdateWebreferencesPosWebserviceAndOssWebservice;
        private System.Windows.Forms.Label lblSetBuildConfigurationToRelease;
        private System.Windows.Forms.TabPage tpSyncDatabase;
        private System.Windows.Forms.LinkLabel llOpenSQLDelta;
        private System.Windows.Forms.TabPage tpUpload;
        private System.Windows.Forms.Label lblUploadFiles;
        private System.Windows.Forms.Label lblSetBuildConfigurationToDebug;
        private System.Windows.Forms.Label lblClearCache;
        private System.Windows.Forms.Label lblRefreshAllViews;
        private System.Windows.Forms.Label lblRefreshInfoInLocalDatabase;
        private System.Windows.Forms.TabPage tpCmsReleases;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tpDeliver;
        private System.Windows.Forms.LinkLabel llGoToPivotal;
        private System.Windows.Forms.Label lblSetStoriesToDelivered;
        private System.Windows.Forms.TabPage tabTest;
        private System.Windows.Forms.GroupBox gbLive;
        private System.Windows.Forms.LinkLabel llOpenNocLive;
        private System.Windows.Forms.LinkLabel llOpenMobileLive;
        private System.Windows.Forms.LinkLabel llOpenConsoleLive;
        private System.Windows.Forms.LinkLabel llOpenCmsLive;
        private System.Windows.Forms.LinkLabel llOpenWebserviceLive;
        private System.Windows.Forms.GroupBox gbTest;
        private System.Windows.Forms.LinkLabel llOpenNocTest;
        private System.Windows.Forms.LinkLabel llOpenMobileTest;
        private System.Windows.Forms.LinkLabel llOpenConsoleTest;
        private System.Windows.Forms.LinkLabel llOpenCmsTest;
        private System.Windows.Forms.LinkLabel llOpenWebserviceTest;
        private System.Windows.Forms.GroupBox gbDevelopment;
        private System.Windows.Forms.LinkLabel llOpenNocDev;
        private System.Windows.Forms.LinkLabel llOpenMobileDev;
        private System.Windows.Forms.LinkLabel llOpenConsoleDev;
        private System.Windows.Forms.LinkLabel llOpenCmsDev;
        private System.Windows.Forms.LinkLabel llOpenWebserviceDev;
        private System.Windows.Forms.CheckBox cbObfuscateCraveOnsiteAgent;
        private System.Windows.Forms.CheckBox cbObfuscateCraveOnsiteServer;
        private System.Windows.Forms.CheckBox cbCreateInstallerCraveOnsiteAgent;
        private System.Windows.Forms.CheckBox cbCreateInstallerCraveOnsiteServer;
        private System.Windows.Forms.CheckBox cbPublishCraveOnsiteAgent;
        private System.Windows.Forms.CheckBox cbPublishCraveOnsiteServer;
        private System.Windows.Forms.CheckBox cbPublishAllSoftware;
        private System.Windows.Forms.CheckBox cbCreateInstallersForAllSoftware;
        private System.Windows.Forms.CheckBox cbObfuscateAllSoftware;
        private System.Windows.Forms.CheckBox cbPublishCraveCloud;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lblLastVersionCraveOnSiteAgent;
        private System.Windows.Forms.Label lblLastVersionCraveOnSiteServer;
        private System.Windows.Forms.Label lblLastVersionCraveCloud;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblLastVersionCraveOnSiteAgentInstaller;
        private System.Windows.Forms.Label lblLastVersionCraveOnSiteServerInstaller;
        private System.Windows.Forms.Label lblLastVersionObymobiWebserviceInstaller;
        private System.Windows.Forms.Button btnGetNewBuildVersionNumber;
        private System.Windows.Forms.Button btnGetNewReleaseVersionNumber;
        private System.Windows.Forms.Button btnIncreaseBuildNumber;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblUpdateWebreferencePosWebservice;
        private System.Windows.Forms.Label lblBuildObymobiRequestService;
        private System.Windows.Forms.CheckBox cbCraveOnSiteAgent;
        private System.Windows.Forms.CheckBox cbCraveOnSiteServer;
        private System.Windows.Forms.CheckBox cbCraveCloud;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createNewWebserviceVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem openWebserviceTesterToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem androidToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem androidToolStripMenuItem;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbDotnetFolder;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolStripMenuItem generateChangelogFromSvnToolStripMenuItem;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.CheckBox cbReleasesSelectAll;
        private System.Windows.Forms.CheckBox cbFtpCloudSoftware;
        private System.Windows.Forms.TabPage tabPage1;
        private SubControls.UcNewRelicDeployment ucNewRelicDeployment;
        private System.Windows.Forms.Label lblSourceVersionOnsiteAgent;
        private System.Windows.Forms.Label lblSourceVersionOnsiteServer;
        private System.Windows.Forms.Label lblSourceVersionCraveCloud;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUpdateCraveOnsiteSupportToolsVersion;
        private System.Windows.Forms.Label lblLastVersionCraveOnSiteSupportToolsInstaller;
        private System.Windows.Forms.Label lblSourceVersionOnsiteSupportTools;
        private System.Windows.Forms.Label lblLastVersionCraveOnSiteSupportTools;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbPublishCraveOnsiteSupportTools;
        private System.Windows.Forms.CheckBox cbCraveOnSiteSupportTools;
        private System.Windows.Forms.CheckBox cbCreateInstallerCraveOnsiteSupportTools;
        private System.Windows.Forms.CheckBox cbObfuscateCraveOnsiteSupportTools;

    }
}

