﻿namespace CraveBuilder.Configuration
{
    public static class CraveBuilderConfigConstants
    {
        public const string ReactorExePath = "ReactorExePath";

        public const string InnoSetupExePath = "InnoSetupExePath";

        public const string SQLDeltaExePath = "SQLDeltaExePath";

        public const string ReleasesDir = "ReleasesDir";

        public const string DropBoxDir = "DropBoxDir";

        public const string SourceFiles = "SourceFiles";

        public const string FtpHost = "FtpHost";

        public const string FtpUsername = "FtpUsername";

        public const string FtpPassword = "FtpPassword";
    }
}