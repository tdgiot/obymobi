﻿using Dionysos.Configuration;
using System;

namespace CraveBuilder.Configuration
{
    public class CraveBuilderConfigInfo : ConfigurationItemCollection
    {
        const string sectionName = "CraveBuilder";

        public CraveBuilderConfigInfo()
        {
            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.ReactorExePath, "Path to the executable file of Reactor.NET", @"C:\Program Files (x86)\Eziriz\.NET Reactor\dotNET_Reactor.exe", typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.InnoSetupExePath, "Path to the executable file of InnoSetup", @"C:\Program Files (x86)\Inno Setup 5\ISCC.exe", typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.SQLDeltaExePath, "Path to the executable file of SQL Delta", @"C:\Program Files (x86)\SQL Delta 4\SQLDeltaV4.exe", typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.ReleasesDir, "Path to the releases directory", @"D:\Builds", typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.SourceFiles, "Path to the source files directory", @"D:\Development\dotnet\Obymobi\Obymobi.Files\CraveBuilder", typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.DropBoxDir, "Path to the DropBox releases directory", @"O:\Software Development\Releases", typeof(string)));

            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.FtpHost, "Ftp Host", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.FtpUsername, "Ftp Username", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.FtpPassword, "Ftp Password", string.Empty, typeof(string)));

            //Add(new ConfigurationItem(sectionName, CraveBuilderConfigConstants.DotNetPath, "Path to the DotNet directory", @"D:\Development\DotNet", typeof(string)));
        }
    }
}