﻿namespace CraveBuilder
{
	partial class FormAndroid
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAndroid));
            this.emenuInputFields = new System.Windows.Forms.Panel();
            this.cbAndroidFolder = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbApplication = new System.Windows.Forms.ComboBox();
            this.cbDropbox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAPIVersion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbAPKVersion = new System.Windows.Forms.TextBox();
            this.btnBuild = new System.Windows.Forms.Button();
            this.tbOutput = new System.Windows.Forms.RichTextBox();
            this.emenuInputFields.SuspendLayout();
            this.SuspendLayout();
            // 
            // emenuInputFields
            // 
            this.emenuInputFields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.emenuInputFields.Controls.Add(this.cbAndroidFolder);
            this.emenuInputFields.Controls.Add(this.label1);
            this.emenuInputFields.Controls.Add(this.label5);
            this.emenuInputFields.Controls.Add(this.cmbApplication);
            this.emenuInputFields.Controls.Add(this.cbDropbox);
            this.emenuInputFields.Controls.Add(this.label2);
            this.emenuInputFields.Controls.Add(this.label4);
            this.emenuInputFields.Controls.Add(this.tbAPIVersion);
            this.emenuInputFields.Controls.Add(this.label3);
            this.emenuInputFields.Controls.Add(this.tbAPKVersion);
            this.emenuInputFields.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emenuInputFields.Location = new System.Drawing.Point(12, 12);
            this.emenuInputFields.Name = "emenuInputFields";
            this.emenuInputFields.Size = new System.Drawing.Size(741, 162);
            this.emenuInputFields.TabIndex = 9;
            // 
            // cbAndroidFolder
            // 
            this.cbAndroidFolder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAndroidFolder.FormattingEnabled = true;
            this.cbAndroidFolder.Location = new System.Drawing.Point(144, 3);
            this.cbAndroidFolder.Name = "cbAndroidFolder";
            this.cbAndroidFolder.Size = new System.Drawing.Size(517, 26);
            this.cbAndroidFolder.TabIndex = 13;
            this.cbAndroidFolder.SelectedIndexChanged += new System.EventHandler(this.cbAndroidFolder_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "Android Folder";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 18);
            this.label5.TabIndex = 11;
            this.label5.Text = "Application";
            // 
            // cmbApplication
            // 
            this.cmbApplication.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbApplication.FormattingEnabled = true;
            this.cmbApplication.Items.AddRange(new object[] {
            "CraveEmenu",
            "CraveAgent",
            "CraveSupportTools"});
            this.cmbApplication.Location = new System.Drawing.Point(144, 35);
            this.cmbApplication.Name = "cmbApplication";
            this.cmbApplication.Size = new System.Drawing.Size(164, 26);
            this.cmbApplication.TabIndex = 10;
            this.cmbApplication.SelectedIndexChanged += new System.EventHandler(this.cmbApplication_SelectedIndexChanged);
            // 
            // cbDropbox
            // 
            this.cbDropbox.AutoSize = true;
            this.cbDropbox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbDropbox.Location = new System.Drawing.Point(3, 131);
            this.cbDropbox.Name = "cbDropbox";
            this.cbDropbox.Size = new System.Drawing.Size(155, 22);
            this.cbDropbox.TabIndex = 7;
            this.cbDropbox.Text = "Publish to DropBox";
            this.cbDropbox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "API Version";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(222, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "(ex. 12102803)";
            // 
            // tbAPIVersion
            // 
            this.tbAPIVersion.Location = new System.Drawing.Point(144, 67);
            this.tbAPIVersion.MaxLength = 4;
            this.tbAPIVersion.Name = "tbAPIVersion";
            this.tbAPIVersion.Size = new System.Drawing.Size(37, 24);
            this.tbAPIVersion.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "APK Version";
            // 
            // tbAPKVersion
            // 
            this.tbAPKVersion.Location = new System.Drawing.Point(144, 101);
            this.tbAPKVersion.MaxLength = 8;
            this.tbAPKVersion.Name = "tbAPKVersion";
            this.tbAPKVersion.Size = new System.Drawing.Size(72, 24);
            this.tbAPKVersion.TabIndex = 4;
            this.tbAPKVersion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAPKVersion_KeyPress);
            // 
            // btnBuild
            // 
            this.btnBuild.BackColor = System.Drawing.Color.GreenYellow;
            this.btnBuild.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuild.Location = new System.Drawing.Point(12, 180);
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Size = new System.Drawing.Size(322, 29);
            this.btnBuild.TabIndex = 8;
            this.btnBuild.Text = "Make Build";
            this.btnBuild.UseVisualStyleBackColor = false;
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // tbOutput
            // 
            this.tbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOutput.BackColor = System.Drawing.Color.Black;
            this.tbOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbOutput.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOutput.ForeColor = System.Drawing.Color.LawnGreen;
            this.tbOutput.Location = new System.Drawing.Point(12, 215);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ReadOnly = true;
            this.tbOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.tbOutput.Size = new System.Drawing.Size(741, 353);
            this.tbOutput.TabIndex = 1;
            this.tbOutput.Text = "";
            this.tbOutput.TextChanged += new System.EventHandler(this.tbOutput_TextChanged);
            // 
            // FormAndroid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 580);
            this.Controls.Add(this.btnBuild);
            this.Controls.Add(this.emenuInputFields);
            this.Controls.Add(this.tbOutput);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(781, 618);
            this.Name = "FormAndroid";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Android Builder";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAndroid_FormClosing);
            this.emenuInputFields.ResumeLayout(false);
            this.emenuInputFields.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.RichTextBox tbOutput;
		private System.Windows.Forms.TextBox tbAPIVersion;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnBuild;
		private System.Windows.Forms.CheckBox cbDropbox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbAPKVersion;
        private System.Windows.Forms.Panel emenuInputFields;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox cmbApplication;
        private System.Windows.Forms.ComboBox cbAndroidFolder;
        private System.Windows.Forms.Label label1;
	}
}