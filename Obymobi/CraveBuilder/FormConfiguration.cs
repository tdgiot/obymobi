﻿using System.Windows.Forms;
using CraveBuilder.Configuration;
using Dionysos;
using Dionysos.Web;

namespace CraveBuilder
{
    public partial class FormConfiguration : Form
    {
        public class ConfigurationObject
        {
            public string ReleaseDir
            {
                get { return ConfigurationManager.GetString(CraveBuilderConfigConstants.ReleasesDir); }
                set { ConfigurationManager.SetValue(CraveBuilderConfigConstants.ReleasesDir, value); }
            }

            public string FtpHost
            {
                get { return ConfigurationManager.GetString(CraveBuilderConfigConstants.FtpHost); }
                set { ConfigurationManager.SetValue(CraveBuilderConfigConstants.FtpHost, value); }
            }

            public string FtpUsername
            {
                get { return ConfigurationManager.GetString(CraveBuilderConfigConstants.FtpUsername); }
                set { ConfigurationManager.SetValue(CraveBuilderConfigConstants.FtpUsername, value); }
            }

            public string FtpPassword
            {
                get { return ConfigurationManager.GetPassword(CraveBuilderConfigConstants.FtpPassword); }
                set { ConfigurationManager.SetPassword(CraveBuilderConfigConstants.FtpPassword, value); }
            }
        }

        private readonly ConfigurationObject configObj = new ConfigurationObject();

        public FormConfiguration()
        {
            InitializeComponent();

            // DataBind fields
            tbReleaseDir.DataBindings.Add("Text", configObj, "ReleaseDir");

            tbFtpHost.DataBindings.Add("Text", configObj, "FtpHost");
            tbFtpUsername.DataBindings.Add("Text", configObj, "FtpUsername");
            tbFtpPassword.DataBindings.Add("Text", configObj, "FtpPassword");
        }

        private void btnTestFtpSettings_Click(object sender, System.EventArgs e)
        {
            var result = FtpHelper.TestConnect(configObj.FtpHost, configObj.FtpUsername, configObj.FtpPassword).ToString();

            System.Windows.Forms.MessageBox.Show(result);
        }
    }
}
