﻿namespace CraveBuilder
{
    public class Application
    {
        #region Fields

        private readonly string versionFile = string.Empty;
        private readonly string releaseDirectory = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="externalFolderName"></param>
        /// <param name="versionFile">The version file.</param>
        /// <param name="installerFile"></param>
        /// <param name="releaseDirectory"></param>
        /// <param name="isWebsite"></param>
        /// <param name="obfuscationFile"></param>
        public Application(string name, string externalFolderName, string versionFile, string installerFile, string releaseDirectory, bool isWebsite, string obfuscationFile)
        {
            this.Name = name;
            this.ExternalFolderName = externalFolderName;
            this.versionFile = versionFile;
            this.InstallerFile = installerFile;
            this.releaseDirectory = releaseDirectory;
            this.IsWebsite = isWebsite;
            this.ObfuscationFile = obfuscationFile;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        public string ExternalFolderName { get; private set; }

        /// <summary>
        /// Gets the path.
        /// </summary>
        public string Path
        {
            get
            {
                return System.IO.Path.Combine(Constants.DotNetPath, "Obymobi", this.Name);
            }
        }

        /// <summary>
        /// Gets the full path to the file which contains the version number.
        /// </summary>
        public string VersionFile
        {
            get
            {
                return System.IO.Path.Combine(this.Path, this.versionFile);
            }
        }

        /// <summary>
        /// Gets the installer file.
        /// </summary>
        public string InstallerFile { get; private set; }

        /// <summary>
        /// Gets the release directory.
        /// </summary>
        public string ReleaseDirectory
        {
            get
            {
                return System.IO.Path.Combine(this.Path, this.releaseDirectory);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is website.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is website; otherwise, <c>false</c>.
        /// </value>
        public bool IsWebsite { get; private set; }

        /// <summary>
        /// Gets the obfuscation file.
        /// </summary>
        public string ObfuscationFile { get; private set; }

        #endregion

    }
}
