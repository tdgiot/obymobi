﻿using CraveBuilder.Configuration;
using Dionysos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CraveBuilder
{
    public class Constants
    {
        public static string DotNetPath = string.Empty;
        public static string DotNetPathBranch
        {
            get
            {
                if (Constants.DotNetPath.IsNullOrWhiteSpace())
                {
                    MessageBox.Show("Constants.DotNetPath is Empty");
                    throw new ArgumentNullException("Constants.DotNetPath is Empty");
                }

                var dirInfo = new DirectoryInfo(Constants.DotNetPath);
                string dotnetFolderName = dirInfo.Name;
                if (dotnetFolderName.Contains("."))
                {
                    string branch = dotnetFolderName.Substring(dotnetFolderName.IndexOf(".") + 1);
                    branch = branch.Substring(0, branch.Length - 2);
                    return branch;
                }
                if (dotnetFolderName.Contains("_"))
                {
                    string branch = dotnetFolderName.Substring(dotnetFolderName.IndexOf("_") + 1);
                    branch = branch.Substring(0, branch.Length - 2);
                    return branch;
                }

                return string.Empty;
            }
        }

        public static string ObymobiWebservice = "ObymobiWebservice";
        public static string ObymobiCms = "Obymobi.Cms.WebApplication";
        public static string Services = "Obymobi.Services.WebApplication";
        public static string CraveOnsiteServer = "CraveOnsiteServer";
        public static string CraveOnsiteAgent = "CraveOnsiteAgent";
        public static string CraveOnsiteSupportTools = "CraveOnsiteSupportTools";
        public static string Messaging = "Obymobi.Messaging.WebApplication";
        public static string CraveMobileNocV2 = "Obymobi.Noc.WebApplication";
        public static string CraveNocService = "Crave.NocService";

        public static string SolutionFile = Path.Combine(DotNetPath, @"Obymobi\Obymobi.sln");

        public static string SQLDeltaFile = @"C:\Program Files (x86)\SQL Delta 4\SQLDeltaV4.exe";

        public static string CraveOnsiteServerObfuscationFile = Path.Combine(DotNetPath, @"Obymobi\Obymobi.Files\Reactor.NET\CraveOnsiteServer.nrproj");
        public static string CraveOnsiteAgentObfuscationFile = Path.Combine(DotNetPath, @"Obymobi\Obymobi.Files\Reactor.NET\CraveOnsiteAgent.nrproj");

        public static string CraveOnsiteServerInstallerFile = Path.Combine(DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\CraveOnsiteServer.iss");
        public static string CraveOnsiteAgentInstallerFile = Path.Combine(DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\CraveOnsiteAgent.iss");
    }
}
