﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CraveBuilder.SubControls
{
    public partial class UcNewRelicDeployment : UserControl
    {
        private const string NEWRELIC_DEPLOY_URL = "https://api.newrelic.com/deployments.xml";
        private const string NEWRELIC_DEPLOY_API = "b74881dad8df08f81270e9daa5170dc2c64976ded39d2c6";

        private readonly Dictionary<string, int[]> newRelicApplications = new Dictionary<string, int[]>(3)
            {
                {
                    "Live", new[]
                        {
                            1901742, // API
                            1901746, // Messaging
                            1901755, // Management
                            1901753 // NOC
                        }
                },
                {
                    "Test", new[]
                        {
                            2046276, // API
                            2061292, // Messaging
                            2061521, // Management
                            2061305 // NOC
                        }
                },
                {
                    "Dev", new[]
                        {
                            2160901, // API
                            2160834, // Messaging
                            2160837, // Management
                            2168675 // NOC
                        }
                },
                {
                    "Demo", new[]
                        {
                            2456798, // API
                            2456802, // Messaging
                            2456803, // Management
                            2456816 // NOC
                        }
                }
            };

        public event EventHandler<string> OnLog;

        public UcNewRelicDeployment()
        {
            InitializeComponent();

            this.cbEnvironment.SelectedIndex = 1;

            tbUser.Text = TestUtil.MachineName;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            btnSend.Enabled = false;

            Log("Pushing deploy information to New Relic");

            SendDeploymentInformation(cbEnvironment.SelectedItem.ToString(), tbUser.Text, tbDescription.Text);
        }

        private void SendDeploymentInformation(string environment, string user, string description)
        {
            Task.Factory.StartNew(() =>
                {
                    int[] applicationIds;
                    if (!newRelicApplications.TryGetValue(environment, out applicationIds))
                    {
                        Log("Environment not found");
                        return;
                    }

                    string result = "";

                    foreach (int applicationId in applicationIds)
                    {
                        using (var wb = new WebClient())
                        {
                            wb.Proxy = null;
                            wb.Headers.Add("x-api-key", NEWRELIC_DEPLOY_API);

                            var data = new NameValueCollection();
                            data["deployment[application_id]"] = applicationId.ToString(CultureInfo.InvariantCulture);
                            data["deployment[description]"] = description;
                            data["deployment[user]"] = user;

                            var response = wb.UploadValues(NEWRELIC_DEPLOY_URL, "POST", data);
                            result += string.Format("{0} - {1}{2}", applicationId, Encoding.Default.GetString(response), Environment.NewLine);
                        }
                    }

                    Log(result);
                    Log("Deploy information pushed to New Relic");

                    this.btnSend.Invoke(new MethodInvoker(() => btnSend.Enabled = true));
                });
        }

        private void Log(string message, params object[] args)
        {
            if (OnLog != null)
                OnLog(this, string.Format(message, args));
        }


    }
}
