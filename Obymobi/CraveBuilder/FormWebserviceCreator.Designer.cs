﻿namespace CraveBuilder
{
    partial class FormWebserviceCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormWebserviceCreator));
            this.tbVersion = new System.Windows.Forms.TextBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblLatestVersionCaption = new System.Windows.Forms.Label();
            this.lblLatestVersion = new System.Windows.Forms.Label();
            this.btnCreateNewWebserviceVersion = new System.Windows.Forms.Button();
            this.gbCraveService = new System.Windows.Forms.GroupBox();
            this.gbMobileService = new System.Windows.Forms.GroupBox();
            this.btnCreateNewMobileWebserviceVersion = new System.Windows.Forms.Button();
            this.lblMobileVersion = new System.Windows.Forms.Label();
            this.lblLatestVersionMobile = new System.Windows.Forms.Label();
            this.tbMobileVersion = new System.Windows.Forms.TextBox();
            this.lblLatestVersionMobileCaption = new System.Windows.Forms.Label();
            this.gbCraveService.SuspendLayout();
            this.gbMobileService.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbVersion
            // 
            this.tbVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tbVersion.Location = new System.Drawing.Point(163, 51);
            this.tbVersion.Name = "tbVersion";
            this.tbVersion.Size = new System.Drawing.Size(71, 24);
            this.tbVersion.TabIndex = 3;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblVersion.Location = new System.Drawing.Point(9, 53);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(132, 18);
            this.lblVersion.TabIndex = 2;
            this.lblVersion.Text = "Specify the version";
            // 
            // lblLatestVersionCaption
            // 
            this.lblLatestVersionCaption.AutoSize = true;
            this.lblLatestVersionCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblLatestVersionCaption.Location = new System.Drawing.Point(9, 23);
            this.lblLatestVersionCaption.Name = "lblLatestVersionCaption";
            this.lblLatestVersionCaption.Size = new System.Drawing.Size(100, 18);
            this.lblLatestVersionCaption.TabIndex = 4;
            this.lblLatestVersionCaption.Text = "Latest version";
            // 
            // lblLatestVersion
            // 
            this.lblLatestVersion.AutoSize = true;
            this.lblLatestVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblLatestVersion.Location = new System.Drawing.Point(163, 23);
            this.lblLatestVersion.Name = "lblLatestVersion";
            this.lblLatestVersion.Size = new System.Drawing.Size(71, 18);
            this.lblLatestVersion.TabIndex = 5;
            this.lblLatestVersion.Text = "Unknown";
            // 
            // btnCreateNewWebserviceVersion
            // 
            this.btnCreateNewWebserviceVersion.BackColor = System.Drawing.Color.GreenYellow;
            this.btnCreateNewWebserviceVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateNewWebserviceVersion.Location = new System.Drawing.Point(12, 81);
            this.btnCreateNewWebserviceVersion.Name = "btnCreateNewWebserviceVersion";
            this.btnCreateNewWebserviceVersion.Size = new System.Drawing.Size(439, 29);
            this.btnCreateNewWebserviceVersion.TabIndex = 10;
            this.btnCreateNewWebserviceVersion.Text = "Create new webservice version";
            this.btnCreateNewWebserviceVersion.UseVisualStyleBackColor = false;
            // 
            // gbCraveService
            // 
            this.gbCraveService.Controls.Add(this.btnCreateNewWebserviceVersion);
            this.gbCraveService.Controls.Add(this.lblVersion);
            this.gbCraveService.Controls.Add(this.lblLatestVersion);
            this.gbCraveService.Controls.Add(this.tbVersion);
            this.gbCraveService.Controls.Add(this.lblLatestVersionCaption);
            this.gbCraveService.Location = new System.Drawing.Point(10, 6);
            this.gbCraveService.Name = "gbCraveService";
            this.gbCraveService.Size = new System.Drawing.Size(465, 123);
            this.gbCraveService.TabIndex = 11;
            this.gbCraveService.TabStop = false;
            this.gbCraveService.Text = "CraveService";
            // 
            // gbMobileService
            // 
            this.gbMobileService.Controls.Add(this.btnCreateNewMobileWebserviceVersion);
            this.gbMobileService.Controls.Add(this.lblMobileVersion);
            this.gbMobileService.Controls.Add(this.lblLatestVersionMobile);
            this.gbMobileService.Controls.Add(this.tbMobileVersion);
            this.gbMobileService.Controls.Add(this.lblLatestVersionMobileCaption);
            this.gbMobileService.Location = new System.Drawing.Point(10, 140);
            this.gbMobileService.Name = "gbMobileService";
            this.gbMobileService.Size = new System.Drawing.Size(465, 123);
            this.gbMobileService.TabIndex = 12;
            this.gbMobileService.TabStop = false;
            this.gbMobileService.Text = "MobileService";
            // 
            // btnCreateNewMobileWebserviceVersion
            // 
            this.btnCreateNewMobileWebserviceVersion.BackColor = System.Drawing.Color.GreenYellow;
            this.btnCreateNewMobileWebserviceVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateNewMobileWebserviceVersion.Location = new System.Drawing.Point(12, 81);
            this.btnCreateNewMobileWebserviceVersion.Name = "btnCreateNewMobileWebserviceVersion";
            this.btnCreateNewMobileWebserviceVersion.Size = new System.Drawing.Size(439, 29);
            this.btnCreateNewMobileWebserviceVersion.TabIndex = 10;
            this.btnCreateNewMobileWebserviceVersion.Text = "Create new webservice version";
            this.btnCreateNewMobileWebserviceVersion.UseVisualStyleBackColor = false;
            // 
            // lblMobileVersion
            // 
            this.lblMobileVersion.AutoSize = true;
            this.lblMobileVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblMobileVersion.Location = new System.Drawing.Point(9, 53);
            this.lblMobileVersion.Name = "lblMobileVersion";
            this.lblMobileVersion.Size = new System.Drawing.Size(132, 18);
            this.lblMobileVersion.TabIndex = 2;
            this.lblMobileVersion.Text = "Specify the version";
            // 
            // lblLatestVersionMobile
            // 
            this.lblLatestVersionMobile.AutoSize = true;
            this.lblLatestVersionMobile.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblLatestVersionMobile.Location = new System.Drawing.Point(163, 23);
            this.lblLatestVersionMobile.Name = "lblLatestVersionMobile";
            this.lblLatestVersionMobile.Size = new System.Drawing.Size(71, 18);
            this.lblLatestVersionMobile.TabIndex = 5;
            this.lblLatestVersionMobile.Text = "Unknown";
            // 
            // tbMobileVersion
            // 
            this.tbMobileVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tbMobileVersion.Location = new System.Drawing.Point(163, 51);
            this.tbMobileVersion.Name = "tbMobileVersion";
            this.tbMobileVersion.Size = new System.Drawing.Size(71, 24);
            this.tbMobileVersion.TabIndex = 3;
            // 
            // lblLatestVersionMobileCaption
            // 
            this.lblLatestVersionMobileCaption.AutoSize = true;
            this.lblLatestVersionMobileCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblLatestVersionMobileCaption.Location = new System.Drawing.Point(9, 23);
            this.lblLatestVersionMobileCaption.Name = "lblLatestVersionMobileCaption";
            this.lblLatestVersionMobileCaption.Size = new System.Drawing.Size(100, 18);
            this.lblLatestVersionMobileCaption.TabIndex = 4;
            this.lblLatestVersionMobileCaption.Text = "Latest version";
            // 
            // FormWebserviceCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 309);
            this.Controls.Add(this.gbMobileService);
            this.Controls.Add(this.gbCraveService);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormWebserviceCreator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create new webservice version";
            this.gbCraveService.ResumeLayout(false);
            this.gbCraveService.PerformLayout();
            this.gbMobileService.ResumeLayout(false);
            this.gbMobileService.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbVersion;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblLatestVersionCaption;
        private System.Windows.Forms.Label lblLatestVersion;
        private System.Windows.Forms.Button btnCreateNewWebserviceVersion;
        private System.Windows.Forms.GroupBox gbCraveService;
        private System.Windows.Forms.GroupBox gbMobileService;
        private System.Windows.Forms.Button btnCreateNewMobileWebserviceVersion;
        private System.Windows.Forms.Label lblMobileVersion;
        private System.Windows.Forms.Label lblLatestVersionMobile;
        private System.Windows.Forms.TextBox tbMobileVersion;
        private System.Windows.Forms.Label lblLatestVersionMobileCaption;
    }
}