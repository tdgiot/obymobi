﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CraveBuilder
{
    public class Release
    {
        #region Fields

        private Application application = null;
        private string version = string.Empty;
        private string filePath = string.Empty;
        private bool isUpdate = true;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Release"/> class.
        /// </summary>
        /// <param name="application">The application.</param>
        /// <param name="version">The version.</param>
        /// <param name="filePath">The file path.</param>
        public Release(Application application, string version, string filePath, bool isUpdate)
        {
            this.application = application;
            this.version = version;
            this.filePath = filePath;
            this.isUpdate = isUpdate;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the application.
        /// </summary>
        public Application Application
        {
            get
            {
                return this.application;
            }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        public string Version
        {
            get
            {
                return this.version;
            }
        }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        public string FilePath
        {
            get
            {
                return this.filePath;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is update.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is update; otherwise, <c>false</c>.
        /// </value>
        public bool IsUpdate
        {
            get
            {
                return this.isUpdate;
            }
        }

        #endregion

    }
}
