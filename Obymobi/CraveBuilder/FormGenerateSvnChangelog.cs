﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dionysos;

namespace CraveBuilder
{
    public partial class FormGenerateSvnChangelog : Form
    {
        [DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "SendMessage")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        const int WM_VSCROLL = 277;
        const int SB_BOTTOM = 7;

        readonly Dictionary<string, string> predefinedRepositories = new Dictionary<string, string>
            {
                { "Android", "http://192.168.57.51:666/svn/Android/trunk" },
                { "DotNet", "http://192.168.57.51:666/svn/AllInOne/trunk" }
            };

        public FormGenerateSvnChangelog()
        {
            InitializeComponent();

            // Enable these styles to reduce flicker of output textbox
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            HookupEvents();
            SetGui();
        }

        private void FormGenerateSvnChangelog_Load(object sender, EventArgs e)
        {
            SetupCheck();
        }

        private void SetupCheck()
        {
            var clPath = Path.Combine(System.Windows.Forms.Application.StartupPath, "Tools\\svn2cl");
            if (!Directory.Exists(clPath))
            {
                MessageBox.Show("The folder 'Tools\\svn2cl' doesn't exists. Copy this folder from the source directory, then try again.", "svn2cl error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.Close();
            }
        }

        private void HookupEvents()
        {
            this.rbInputPredefined.CheckedChanged += rbInput_CheckedChanged;
            this.rbInputOther.CheckedChanged += rbInput_CheckedChanged;

            this.rbOutputDropbox.CheckedChanged += rbOutput_CheckedChanged;
            this.rbOutputOther.CheckedChanged += rbOutput_CheckedChanged;
        }

        private void SetGui()
        {
            this.rbInputPredefined.Checked = true;
            this.rbOutputDropbox.Checked = true;

            this.cbInputPredefined.DataSource = predefinedRepositories.Keys.ToList();

            this.tbProcessOutput.Visible = false;
            this.Height = 448;
        }

        #region Events 

        private void rbInput_CheckedChanged(object sender, EventArgs e)
        {
            this.tbInputOtherName.Enabled = sender.Equals(this.rbInputOther);
            this.tbInputOtherUrl.Enabled = sender.Equals(this.rbInputOther);
        }

        private void rbOutput_CheckedChanged(object sender, EventArgs e)
        {
            this.tbOutputDirectory.Enabled = sender.Equals(this.rbOutputOther);
            this.btnOutputSelectDirectory.Enabled = sender.Equals(this.rbOutputOther);
        }

        private void btnInputAdd_Click(object sender, EventArgs e)
        {
            if (this.rbInputPredefined.Checked)
                AddPredefinedRepositry();
            else if (this.rbInputOther.Checked)
                AddOtherRepository();
        }

        private void btnOutputSelectDirectory_Click(object sender, EventArgs e)
        {
            var folderDialog = new FolderBrowserDialog();
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                this.tbOutputDirectory.Text = folderDialog.SelectedPath;
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (this.dgvRepositories.Rows.Count == 0)
            {
                MessageBox.Show(@"No repositories have been choosen.", @"Generate error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string outputDirectory;
            if (this.rbOutputDropbox.Checked)
            {
                outputDirectory = "O:\\Software Development\\Releases\\_Changelogs";
            }
            else
            {
                if (this.tbOutputDirectory.Text.IsNullOrWhiteSpace())
                {
                    MessageBox.Show(@"Output directory can not be empty.", @"Generate error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                outputDirectory = this.tbOutputDirectory.Text;
            }

            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }

            if (this.cbOutputAsHTML.Checked)
            {
                File.Copy(Path.Combine(System.Windows.Forms.Application.StartupPath, "Tools\\svn2cl\\svn2html.css"), Path.Combine(outputDirectory, "svn2html.css"), true);
            }

            // Enable process output textbox
            this.Height = 648;
            this.tbProcessOutput.Text = "";
            this.tbProcessOutput.Visible = true;

            // Disable rest
            this.gbInput.Enabled = false;
            this.gbOutput.Enabled = false;
            this.dgvRepositories.Enabled = false;
            this.btnGenerate.Enabled = false;
            this.btnClose.Enabled = false;

            Task.Factory.StartNew(() =>
                {
                    foreach (DataGridViewRow row in this.dgvRepositories.Rows)
                    {
                        var filename = (string) row.Cells[0].Value + (this.cbOutputAsHTML.Checked ? ".html" : ".txt");
                        var url = (string) row.Cells[1].Value;

                        if (this.tbProcessOutput != null)
                        {
                            this.tbProcessOutput.Invoke((MethodInvoker)(() => this.tbProcessOutput.AppendText("Generating svn changelog for: " + url + "\n")));
                        }

                        string arguments = string.Format("{0} \"{1}\" -o \"{2}\"", (this.cbOutputAsHTML.Checked ? "--html" : ""), url, Path.Combine(outputDirectory, filename));

                        var startInfo = new ProcessStartInfo();
                        startInfo.FileName = "cmd";
                        startInfo.Arguments = "/C CScript.exe //nologo svn2cl.vbs --group-by-day --limit 250 " + arguments;
                        startInfo.WorkingDirectory = Path.Combine(System.Windows.Forms.Application.StartupPath, "Tools\\svn2cl");
                        startInfo.UseShellExecute = false;
                        startInfo.RedirectStandardOutput = true;
                        startInfo.CreateNoWindow = true;

                        try
                        {
                            using (var svnProcess = Process.Start(startInfo))
                            {
                                using (StreamReader reader = svnProcess.StandardOutput)
                                {
                                    while (!svnProcess.HasExited)
                                    {
                                        string str = reader.ReadLine();
                                        if (this.tbProcessOutput != null)
                                        {
                                            this.tbProcessOutput.Invoke((MethodInvoker)(() => this.tbProcessOutput.AppendText(str + "\n")));
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (this.tbProcessOutput != null)
                            {
                                this.tbProcessOutput.Invoke((MethodInvoker)(() => this.tbProcessOutput.AppendText(ex.Message + "\n")));
                            }
                        }
                    }

                    BuildComplete();
                });
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Logic

        private void BuildComplete()
        {
            MessageBox.Show("Generation complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Invoke((MethodInvoker) delegate
                {
                    // Disable process output textbox
                    this.Height = 448;
                    this.tbProcessOutput.Visible = false;

                    // Enable rest
                    this.gbInput.Enabled = true;
                    this.gbOutput.Enabled = true;
                    this.dgvRepositories.Enabled = true;
                    this.btnGenerate.Enabled = true;
                    this.btnClose.Enabled = true;
                });
        }

        private void AddPredefinedRepositry()
        {
            string repoUrl = predefinedRepositories[(string)this.cbInputPredefined.SelectedValue];

            int rowIndex = this.dgvRepositories.Rows.Add();
            this.dgvRepositories.Rows[rowIndex].Cells[0].Value = this.cbInputPredefined.SelectedValue;
            this.dgvRepositories.Rows[rowIndex].Cells[1].Value = repoUrl;
        }

        private void AddOtherRepository()
        {
            if (this.tbInputOtherName.Text.IsNullOrWhiteSpace())
            {
                MessageBox.Show(@"Repository name can not be empty", @"Adding repository", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (this.tbInputOtherUrl.Text.IsNullOrWhiteSpace())
            {
                MessageBox.Show(@"Repository url can not be empty", @"Adding repository", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                int rowIndex = this.dgvRepositories.Rows.Add();
                this.dgvRepositories.Rows[rowIndex].Cells[0].Value = this.tbInputOtherName.Text;
                this.dgvRepositories.Rows[rowIndex].Cells[1].Value = this.tbInputOtherUrl.Text;
            }
        }

        #endregion

        private void tbProcessOutput_TextChanged(object sender, EventArgs e)
        {
            var ptrWparam = new IntPtr(SB_BOTTOM);
            var ptrLparam = new IntPtr(0);
            SendMessage(this.tbProcessOutput.Handle, WM_VSCROLL, ptrWparam, ptrLparam);
        }


    }
}
