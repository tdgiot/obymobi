﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CraveBuilder
{
    public partial class FormWebserviceCreator : Form
    {
        #region Fields

        private WebserviceCreator webserviceCreator = null;
        private MobileWebserviceCreator mobileWebserviceCreator = null;

        #endregion

        #region Constructors

        public FormWebserviceCreator()
        {
            // Initialize the form
            InitializeComponent();

            // Hookup the event
            this.HookupEvents();

            // Initialize the webservice creator
            this.webserviceCreator = new WebserviceCreator();
            this.lblLatestVersion.Text = this.webserviceCreator.GetLatestVersionString();
            this.tbVersion.Text = this.webserviceCreator.GetNewVersionString();

            this.mobileWebserviceCreator = new MobileWebserviceCreator();
            this.lblLatestVersionMobile.Text = this.mobileWebserviceCreator.GetLatestVersionString();
            this.tbMobileVersion.Text = this.mobileWebserviceCreator.GetNewVersionString();
        }

        #endregion

        #region Methods

        private void HookupEvents()
        {
            this.btnCreateNewWebserviceVersion.Click += new EventHandler(btnCreateNewWebserviceVersion_Click);
            this.btnCreateNewMobileWebserviceVersion.Click += btnCreateNewMobileWebserviceVersion_Click;
        }

        #endregion

        #region Event handlers

        private void btnCreateNewWebserviceVersion_Click(object sender, EventArgs e)
        {
            this.webserviceCreator.CreateNewWebserviceVersion(this.tbVersion.Text);
        }

        private void btnCreateNewMobileWebserviceVersion_Click(object sender, EventArgs e)
        {
            this.mobileWebserviceCreator.CreateNewWebserviceVersion(this.tbMobileVersion.Text);
        }

        #endregion
    }
}
