﻿namespace CraveBuilder
{
    partial class FormConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfiguration));
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tbLocations = new System.Windows.Forms.TabPage();
            this.tbWebsiteReleases = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFtpHost = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFtpUsername = new System.Windows.Forms.TextBox();
            this.tbFtpPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbReleaseDir = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbBuildDir = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnTestFtpSettings = new System.Windows.Forms.Button();
            this.tcMain.SuspendLayout();
            this.tbLocations.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tbLocations);
            this.tcMain.Controls.Add(this.tbWebsiteReleases);
            this.tcMain.Controls.Add(this.tabPage1);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcMain.Location = new System.Drawing.Point(0, 0);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(667, 300);
            this.tcMain.TabIndex = 0;
            // 
            // tbLocations
            // 
            this.tbLocations.Controls.Add(this.label6);
            this.tbLocations.Controls.Add(this.tbBuildDir);
            this.tbLocations.Controls.Add(this.label5);
            this.tbLocations.Controls.Add(this.tbReleaseDir);
            this.tbLocations.Location = new System.Drawing.Point(4, 29);
            this.tbLocations.Name = "tbLocations";
            this.tbLocations.Padding = new System.Windows.Forms.Padding(3);
            this.tbLocations.Size = new System.Drawing.Size(659, 267);
            this.tbLocations.TabIndex = 0;
            this.tbLocations.Text = "Locations";
            this.tbLocations.UseVisualStyleBackColor = true;
            // 
            // tbWebsiteReleases
            // 
            this.tbWebsiteReleases.Location = new System.Drawing.Point(4, 29);
            this.tbWebsiteReleases.Name = "tbWebsiteReleases";
            this.tbWebsiteReleases.Padding = new System.Windows.Forms.Padding(3);
            this.tbWebsiteReleases.Size = new System.Drawing.Size(659, 267);
            this.tbWebsiteReleases.TabIndex = 1;
            this.tbWebsiteReleases.Text = "Website Releases";
            this.tbWebsiteReleases.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnTestFtpSettings);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.tbFtpHost);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.tbFtpPassword);
            this.tabPage1.Controls.Add(this.tbFtpUsername);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(659, 267);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "FTP";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Host:";
            // 
            // tbFtpHost
            // 
            this.tbFtpHost.Location = new System.Drawing.Point(129, 9);
            this.tbFtpHost.Name = "tbFtpHost";
            this.tbFtpHost.Size = new System.Drawing.Size(318, 26);
            this.tbFtpHost.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(98, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "ftp://";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Username:";
            // 
            // tbFtpUsername
            // 
            this.tbFtpUsername.Location = new System.Drawing.Point(101, 41);
            this.tbFtpUsername.Name = "tbFtpUsername";
            this.tbFtpUsername.Size = new System.Drawing.Size(346, 26);
            this.tbFtpUsername.TabIndex = 4;
            // 
            // tbFtpPassword
            // 
            this.tbFtpPassword.Location = new System.Drawing.Point(101, 73);
            this.tbFtpPassword.Name = "tbFtpPassword";
            this.tbFtpPassword.Size = new System.Drawing.Size(346, 26);
            this.tbFtpPassword.TabIndex = 5;
            this.tbFtpPassword.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Password:";
            // 
            // tbReleaseDir
            // 
            this.tbReleaseDir.Location = new System.Drawing.Point(150, 6);
            this.tbReleaseDir.Name = "tbReleaseDir";
            this.tbReleaseDir.Size = new System.Drawing.Size(418, 26);
            this.tbReleaseDir.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Release directory:";
            // 
            // tbBuildDir
            // 
            this.tbBuildDir.Location = new System.Drawing.Point(150, 38);
            this.tbBuildDir.Name = "tbBuildDir";
            this.tbBuildDir.Size = new System.Drawing.Size(418, 26);
            this.tbBuildDir.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "Build directory:";
            // 
            // btnTestFtpSettings
            // 
            this.btnTestFtpSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestFtpSettings.Location = new System.Drawing.Point(101, 105);
            this.btnTestFtpSettings.Name = "btnTestFtpSettings";
            this.btnTestFtpSettings.Size = new System.Drawing.Size(129, 26);
            this.btnTestFtpSettings.TabIndex = 7;
            this.btnTestFtpSettings.Text = "Test FTP Settings";
            this.btnTestFtpSettings.UseVisualStyleBackColor = true;
            this.btnTestFtpSettings.Click += new System.EventHandler(this.btnTestFtpSettings_Click);
            // 
            // FormConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 300);
            this.Controls.Add(this.tcMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConfiguration";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuration";
            this.tcMain.ResumeLayout(false);
            this.tbLocations.ResumeLayout(false);
            this.tbLocations.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tbLocations;
        private System.Windows.Forms.TabPage tbWebsiteReleases;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbFtpPassword;
        private System.Windows.Forms.TextBox tbFtpUsername;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbFtpHost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbBuildDir;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbReleaseDir;
        private System.Windows.Forms.Button btnTestFtpSettings;
    }
}