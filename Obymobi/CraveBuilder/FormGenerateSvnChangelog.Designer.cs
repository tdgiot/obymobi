﻿namespace CraveBuilder
{
    partial class FormGenerateSvnChangelog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbInputPredefined = new System.Windows.Forms.ComboBox();
            this.tbInputOtherUrl = new System.Windows.Forms.TextBox();
            this.gbOutput = new System.Windows.Forms.GroupBox();
            this.cbOutputAsHTML = new System.Windows.Forms.CheckBox();
            this.btnOutputSelectDirectory = new System.Windows.Forms.Button();
            this.tbOutputDirectory = new System.Windows.Forms.TextBox();
            this.rbOutputOther = new System.Windows.Forms.RadioButton();
            this.rbOutputDropbox = new System.Windows.Forms.RadioButton();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.dgvRepositories = new System.Windows.Forms.DataGridView();
            this.clmName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmRepositoryUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnInputAdd = new System.Windows.Forms.Button();
            this.gbInput = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbInputOtherName = new System.Windows.Forms.TextBox();
            this.rbInputOther = new System.Windows.Forms.RadioButton();
            this.rbInputPredefined = new System.Windows.Forms.RadioButton();
            this.tbProcessOutput = new System.Windows.Forms.RichTextBox();
            this.gbOutput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRepositories)).BeginInit();
            this.gbInput.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "URL";
            // 
            // cbInputPredefined
            // 
            this.cbInputPredefined.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInputPredefined.FormattingEnabled = true;
            this.cbInputPredefined.Location = new System.Drawing.Point(91, 19);
            this.cbInputPredefined.Name = "cbInputPredefined";
            this.cbInputPredefined.Size = new System.Drawing.Size(431, 21);
            this.cbInputPredefined.TabIndex = 1;
            // 
            // tbInputOtherUrl
            // 
            this.tbInputOtherUrl.Location = new System.Drawing.Point(129, 72);
            this.tbInputOtherUrl.Name = "tbInputOtherUrl";
            this.tbInputOtherUrl.Size = new System.Drawing.Size(220, 20);
            this.tbInputOtherUrl.TabIndex = 2;
            // 
            // gbOutput
            // 
            this.gbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbOutput.Controls.Add(this.cbOutputAsHTML);
            this.gbOutput.Controls.Add(this.btnOutputSelectDirectory);
            this.gbOutput.Controls.Add(this.tbOutputDirectory);
            this.gbOutput.Controls.Add(this.rbOutputOther);
            this.gbOutput.Controls.Add(this.rbOutputDropbox);
            this.gbOutput.Location = new System.Drawing.Point(12, 263);
            this.gbOutput.Name = "gbOutput";
            this.gbOutput.Size = new System.Drawing.Size(528, 103);
            this.gbOutput.TabIndex = 4;
            this.gbOutput.TabStop = false;
            this.gbOutput.Text = "Output";
            // 
            // cbOutputAsHTML
            // 
            this.cbOutputAsHTML.AutoSize = true;
            this.cbOutputAsHTML.Location = new System.Drawing.Point(6, 80);
            this.cbOutputAsHTML.Name = "cbOutputAsHTML";
            this.cbOutputAsHTML.Size = new System.Drawing.Size(56, 17);
            this.cbOutputAsHTML.TabIndex = 11;
            this.cbOutputAsHTML.Text = "HTML";
            this.cbOutputAsHTML.UseVisualStyleBackColor = true;
            // 
            // btnOutputSelectDirectory
            // 
            this.btnOutputSelectDirectory.Location = new System.Drawing.Point(495, 40);
            this.btnOutputSelectDirectory.Name = "btnOutputSelectDirectory";
            this.btnOutputSelectDirectory.Size = new System.Drawing.Size(27, 23);
            this.btnOutputSelectDirectory.TabIndex = 7;
            this.btnOutputSelectDirectory.Text = "...";
            this.btnOutputSelectDirectory.UseVisualStyleBackColor = true;
            this.btnOutputSelectDirectory.Click += new System.EventHandler(this.btnOutputSelectDirectory_Click);
            // 
            // tbOutputDirectory
            // 
            this.tbOutputDirectory.Location = new System.Drawing.Point(106, 42);
            this.tbOutputDirectory.Name = "tbOutputDirectory";
            this.tbOutputDirectory.Size = new System.Drawing.Size(383, 20);
            this.tbOutputDirectory.TabIndex = 2;
            // 
            // rbOutputOther
            // 
            this.rbOutputOther.AutoSize = true;
            this.rbOutputOther.Location = new System.Drawing.Point(6, 42);
            this.rbOutputOther.Name = "rbOutputOther";
            this.rbOutputOther.Size = new System.Drawing.Size(94, 17);
            this.rbOutputOther.TabIndex = 1;
            this.rbOutputOther.TabStop = true;
            this.rbOutputOther.Text = "Other directory";
            this.rbOutputOther.UseVisualStyleBackColor = true;
            // 
            // rbOutputDropbox
            // 
            this.rbOutputDropbox.AutoSize = true;
            this.rbOutputDropbox.Location = new System.Drawing.Point(6, 19);
            this.rbOutputDropbox.Name = "rbOutputDropbox";
            this.rbOutputDropbox.Size = new System.Drawing.Size(96, 17);
            this.rbOutputDropbox.TabIndex = 0;
            this.rbOutputDropbox.Text = "Crave Dropbox";
            this.rbOutputDropbox.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(465, 575);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerate.Location = new System.Drawing.Point(384, 575);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 6;
            this.btnGenerate.Text = "&Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // dgvRepositories
            // 
            this.dgvRepositories.AllowUserToAddRows = false;
            this.dgvRepositories.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRepositories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRepositories.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmName,
            this.clmRepositoryUrl});
            this.dgvRepositories.Location = new System.Drawing.Point(12, 148);
            this.dgvRepositories.Name = "dgvRepositories";
            this.dgvRepositories.Size = new System.Drawing.Size(528, 109);
            this.dgvRepositories.TabIndex = 7;
            // 
            // clmName
            // 
            this.clmName.HeaderText = "Name";
            this.clmName.Name = "clmName";
            this.clmName.ReadOnly = true;
            this.clmName.Width = 120;
            // 
            // clmRepositoryUrl
            // 
            this.clmRepositoryUrl.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmRepositoryUrl.HeaderText = "Repository URL";
            this.clmRepositoryUrl.Name = "clmRepositoryUrl";
            this.clmRepositoryUrl.ReadOnly = true;
            // 
            // btnInputAdd
            // 
            this.btnInputAdd.Location = new System.Drawing.Point(6, 98);
            this.btnInputAdd.Name = "btnInputAdd";
            this.btnInputAdd.Size = new System.Drawing.Size(75, 23);
            this.btnInputAdd.TabIndex = 9;
            this.btnInputAdd.Text = "Add";
            this.btnInputAdd.UseVisualStyleBackColor = true;
            this.btnInputAdd.Click += new System.EventHandler(this.btnInputAdd_Click);
            // 
            // gbInput
            // 
            this.gbInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbInput.Controls.Add(this.label2);
            this.gbInput.Controls.Add(this.tbInputOtherName);
            this.gbInput.Controls.Add(this.rbInputOther);
            this.gbInput.Controls.Add(this.rbInputPredefined);
            this.gbInput.Controls.Add(this.tbInputOtherUrl);
            this.gbInput.Controls.Add(this.btnInputAdd);
            this.gbInput.Controls.Add(this.label1);
            this.gbInput.Controls.Add(this.cbInputPredefined);
            this.gbInput.Location = new System.Drawing.Point(12, 12);
            this.gbInput.Name = "gbInput";
            this.gbInput.Size = new System.Drawing.Size(528, 130);
            this.gbInput.TabIndex = 10;
            this.gbInput.TabStop = false;
            this.gbInput.Text = "Input";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name";
            // 
            // tbInputOtherName
            // 
            this.tbInputOtherName.Location = new System.Drawing.Point(129, 46);
            this.tbInputOtherName.Name = "tbInputOtherName";
            this.tbInputOtherName.Size = new System.Drawing.Size(220, 20);
            this.tbInputOtherName.TabIndex = 12;
            // 
            // rbInputOther
            // 
            this.rbInputOther.AutoSize = true;
            this.rbInputOther.Location = new System.Drawing.Point(6, 47);
            this.rbInputOther.Name = "rbInputOther";
            this.rbInputOther.Size = new System.Drawing.Size(51, 17);
            this.rbInputOther.TabIndex = 11;
            this.rbInputOther.TabStop = true;
            this.rbInputOther.Text = "Other";
            this.rbInputOther.UseVisualStyleBackColor = true;
            // 
            // rbInputPredefined
            // 
            this.rbInputPredefined.AutoSize = true;
            this.rbInputPredefined.Location = new System.Drawing.Point(6, 19);
            this.rbInputPredefined.Name = "rbInputPredefined";
            this.rbInputPredefined.Size = new System.Drawing.Size(79, 17);
            this.rbInputPredefined.TabIndex = 10;
            this.rbInputPredefined.TabStop = true;
            this.rbInputPredefined.Text = "Pre-defined";
            this.rbInputPredefined.UseVisualStyleBackColor = true;
            // 
            // tbProcessOutput
            // 
            this.tbProcessOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProcessOutput.BackColor = System.Drawing.Color.Black;
            this.tbProcessOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProcessOutput.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbProcessOutput.ForeColor = System.Drawing.Color.LawnGreen;
            this.tbProcessOutput.Location = new System.Drawing.Point(12, 372);
            this.tbProcessOutput.Name = "tbProcessOutput";
            this.tbProcessOutput.ReadOnly = true;
            this.tbProcessOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.tbProcessOutput.Size = new System.Drawing.Size(528, 197);
            this.tbProcessOutput.TabIndex = 11;
            this.tbProcessOutput.Text = "";
            this.tbProcessOutput.Visible = false;
            this.tbProcessOutput.TextChanged += new System.EventHandler(this.tbProcessOutput_TextChanged);
            // 
            // FormGenerateSvnChangelog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 610);
            this.Controls.Add(this.tbProcessOutput);
            this.Controls.Add(this.gbInput);
            this.Controls.Add(this.dgvRepositories);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gbOutput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormGenerateSvnChangelog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Generate Changelog from SVN";
            this.Load += new System.EventHandler(this.FormGenerateSvnChangelog_Load);
            this.gbOutput.ResumeLayout(false);
            this.gbOutput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRepositories)).EndInit();
            this.gbInput.ResumeLayout(false);
            this.gbInput.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbInputPredefined;
        private System.Windows.Forms.TextBox tbInputOtherUrl;
        private System.Windows.Forms.GroupBox gbOutput;
        private System.Windows.Forms.Button btnOutputSelectDirectory;
        private System.Windows.Forms.TextBox tbOutputDirectory;
        private System.Windows.Forms.RadioButton rbOutputOther;
        private System.Windows.Forms.RadioButton rbOutputDropbox;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.DataGridView dgvRepositories;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmRepositoryUrl;
        private System.Windows.Forms.Button btnInputAdd;
        private System.Windows.Forms.GroupBox gbInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbInputOtherName;
        private System.Windows.Forms.RadioButton rbInputOther;
        private System.Windows.Forms.RadioButton rbInputPredefined;
        private System.Windows.Forms.CheckBox cbOutputAsHTML;
        private System.Windows.Forms.RichTextBox tbProcessOutput;
    }
}