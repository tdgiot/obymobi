﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CraveBuilder.Testing.Testcases;
using System.Windows.Forms;
using System.Drawing;

namespace CraveBuilder.Testing
{
    /// <summary>
    /// Class which is used to execute webservice test cases
    /// </summary>
    public class Tester
    {
        #region Fields

        private FormMain formMain = null;

        private TimeSpan executionTime = new TimeSpan(0);

        private bool stopOnError = false;
        private int errorCount = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Tester"/> class.
        /// </summary>
        public Tester(FormMain formMain, bool stopOnError)
        {
            this.formMain = formMain;
            this.stopOnError = stopOnError;
        }   

        #endregion

        #region Methods

        public void RunTests(List<Testcase> testcases)
        {
            // Reset the vars
            this.errorCount = 0;
            this.executionTime = new TimeSpan(0);

            int count = 0;

            this.formMain.ProgressBar.Minimum = 1;
            this.formMain.ProgressBar.Maximum = (testcases.Count > 1 ? testcases.Count : 1);

            foreach (Testcase testcase in testcases)
            {
                this.formMain.UpdateProgress("Executing testcase {0} of {1}", count + 1, testcases.Count);

                // Run the testcase
                TestcaseResult result = testcase.Run();

                // Add the execution time
                this.executionTime += result.ExecutionTime;

                //// Update the interface
                //ListViewItem listViewItem = this.formMain.ListView.Items[count];
                //listViewItem.EnsureVisible();

                //// Check whether an error occurred
                //if (result.Success)
                //{
                //    listViewItem.SubItems[6].Text = "Success";
                //    listViewItem.ForeColor = Color.Green;
                //}
                //else
                //{
                //    listViewItem.SubItems[6].Text = "Failure";
                //    listViewItem.ForeColor = Color.Red;

                //    // Increment the error counter
                //    this.errorCount++;

                //    // Check whether we need to stop
                //    if (this.stopOnError)
                //        break;
                //}

                //if (result is ObyTypedTestcaseResult)
                //{
                //    ObyTypedTestcaseResult obyTypedTestcaseResult = result as ObyTypedTestcaseResult;
                //    if (obyTypedTestcaseResult != null)
                //    {
                //        listViewItem.SubItems[2].Text = obyTypedTestcaseResult.ResultType;
                //        listViewItem.SubItems[3].Text = obyTypedTestcaseResult.ResultMessage;
                //        listViewItem.SubItems[4].Text = obyTypedTestcaseResult.ResultCode.ToString();
                //        listViewItem.SubItems[5].Text = obyTypedTestcaseResult.ModelCount.ToString();
                //    }
                //}

                if (this.formMain.ProgressBar.Value < this.formMain.ProgressBar.Maximum)
                    this.formMain.ProgressBar.Value++;

                System.Windows.Forms.Application.DoEvents();

                count++;
            }

            if (this.errorCount == 0)
                this.formMain.UpdateProgress("Testcase{0} succesfully executed in {1}", (testcases.Count > 1 ? "s" : ""), this.executionTime);
            else
                this.formMain.UpdateProgress("{0} testcase{1} failed in {2}", this.errorCount, (this.errorCount > 1 ? "s" : ""), this.executionTime);
        }

        #endregion
    }
}
