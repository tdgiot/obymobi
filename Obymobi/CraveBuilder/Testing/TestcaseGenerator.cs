﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CraveBuilder.Testing.Testcases;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;

namespace CraveBuilder.Testing
{
    /// <summary>
    /// Class which is used to generate test cases with
    /// </summary>
    public class TestcaseGenerator
    {
        #region Fields

        private CraveService.CraveService webservice = null;

        private List<int> companyIds = new List<int>() { 83 };
        private List<Webmethod> webmethods = new List<Webmethod>() { Webmethod.GetMenu };

        private CompanyCollection companyCollection = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TestcaseGenerator"/> class.
        /// </summary>
        public TestcaseGenerator(CraveService.CraveService webservice)
        {
            this.webservice = webservice;
        }

        #endregion

        #region Methods

        public List<Testcase> GenerateTestcases()
        {
            List<Testcase> testcases = new List<Testcase>();

            // Initialize the data
            this.InitializeData();

            foreach (CompanyEntity companyEntity in this.companyCollection)
            {
                foreach (Webmethod webmethod in webmethods)
                {
                    Testcase testcase = this.GenerateTestcase(webmethod, companyEntity);
                    testcase.Company = companyEntity.Name;
                    testcase.Webmethod = webmethod.ToString();

                    if (testcase != null)
                        testcases.Add(testcase);
                }
            }

            return testcases;
        }

        /// <summary>
        /// Generates a testcase for the specified webmethod and company
        /// </summary>
        /// <param name="webmethod">The webmethod to generate a testcase for</param>
        /// <param name="company">The company to generate a testcase for</param>
        /// <returns></returns>
        private Testcase GenerateTestcase(Webmethod webmethod, CompanyEntity company)
        {
            Testcase testcase = null;

            switch (webmethod)
            {
                case Webmethod.GetAdvertisements:
                    testcase = new GetAdvertisementsTestcase(this.webservice, company.CompanyOwnerEntity.Username, company.CompanyOwnerEntity.Password, company.CompanyId, 0, 0);
                    break;
                case Webmethod.GetMenu:
                    testcase = new GetMenuTestcase(this.webservice, company.CompanyOwnerEntity.Username, company.CompanyOwnerEntity.Password, company.CompanyId, 0, 0);
                    break;
            }

            return testcase;
        }

        private void InitializeData()
        {
            PredicateExpression companyFilter = new PredicateExpression();
            //companyFilter.Add(CompanyFields.CompanyId == this.companyIds);
            companyFilter.Add(CompanyFields.UseMonitoring == true);

            this.companyCollection = new CompanyCollection();
            this.companyCollection.GetMulti(companyFilter);
        }

        #endregion
    }
}
