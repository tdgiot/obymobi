﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CraveBuilder.Testing.Testcases
{
    /// <summary>
    /// Class which is used to store the ObyTyped results of a testcase in
    /// </summary>
    public class ObyTypedTestcaseResult : TestcaseResult
    {
        #region Fields

        private string resultType = string.Empty;
        private string resultMessage = string.Empty;
        private int resultCode = 0;
        private int modelCount = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ObyTypedTestcaseResult"/> class.
        /// </summary>
        public ObyTypedTestcaseResult(string resultType, string resultMessage, int resultCode, int modelCount)
        {
            this.resultType = resultType;
            this.resultMessage = resultMessage;
            this.resultCode = resultCode;
            this.modelCount = modelCount;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the type of the result.
        /// </summary>
        public string ResultType
        {
            get
            {
                return this.resultType;
            }
            set
            {
                this.resultType = value;
            }
        }

        /// <summary>
        /// Gets or sets the message of the result.
        /// </summary>
        public string ResultMessage
        {
            get
            {
                return this.resultMessage;
            }
            set
            {
                this.resultMessage = value;
            }
        }

        /// <summary>
        /// Gets or sets the code of the result.
        /// </summary>
        public int ResultCode
        {
            get
            {
                return this.resultCode;
            }
            set
            {
                this.resultCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the amount of models retrieved
        /// </summary>
        public int ModelCount
        {
            get
            {
                return this.modelCount;
            }
            set
            {
                this.modelCount = value;
            }
        }

        #endregion
    }
}
