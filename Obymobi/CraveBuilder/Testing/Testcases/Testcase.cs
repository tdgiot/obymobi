﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CraveBuilder.Testing.Testcases
{
    public abstract class Testcase
    {
        #region Fields

        private CraveService.CraveService webservice = null;

        private string company = string.Empty;
        private string webmethod = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TestcaseBase"/> class.
        /// </summary>
        public Testcase(CraveService.CraveService webservice)
        {
            this.webservice = webservice;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Does specific work for the testcase
        /// </summary>
        /// <returns></returns>
        public abstract TestcaseResult DoWork();

        /// <summary>
        /// Runs the test case
        /// </summary>
        /// <returns></returns>
        public TestcaseResult Run()
        {
            TestcaseResult tcResult = new TestcaseResult();

            // Record the start time
            DateTime start = DateTime.Now;

            try
            {
                tcResult = this.DoWork();
            }
            catch (Exception ex)
            {
                tcResult.Success = false;
                tcResult.Message = ex.Message;
            }

            // Record the execution time
            tcResult.ExecutionTime = DateTime.Now - start;

            return tcResult;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the webservice.
        /// </summary>
        public CraveService.CraveService Webservice
        {
            get
            {
                return this.webservice;
            }
            set
            {
                this.webservice = value;
            }
        }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        public string Company
        {
            get
            {
                return this.company;
            }
            set
            {
                this.company = value;
            }
        }

        /// <summary>
        /// Gets or sets the webmethod.
        /// </summary>
        public string Webmethod
        {
            get
            {
                return this.webmethod;
            }
            set
            {
                this.webmethod = value;
            }
        }

        #endregion
    }
}
