﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CraveBuilder.Testing.Testcases
{
    /// <summary>
    /// Abstract class which is used to store the results of a testcase in
    /// </summary>
    public class TestcaseResult
    {
        #region Fields

        private bool success = false;
        private TimeSpan executionTime = new TimeSpan(0);
        private string message = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TestcaseResult"/> class.
        /// </summary>
        public TestcaseResult()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestcaseResult"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="executionTime">The execution time.</param>
        public TestcaseResult(bool success, TimeSpan executionTime)
        {
            this.success = success;
            this.executionTime = executionTime;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether the testcase was successful
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success
        {
            get
            {
                return this.success;
            }
            set
            {
                this.success = value;
            }
        }

        /// <summary>
        /// Gets or sets the execution time.
        /// </summary>
        /// <value>
        /// The execution time.
        /// </value>
        public TimeSpan ExecutionTime
        {
            get
            {
                return this.executionTime;
            }
            set
            {
                this.executionTime = value;
            }
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message
        {
            get
            {
                return this.message;
            }
            set
            {
                this.message = value;
            }
        }

        #endregion
    }
}
