﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Model;
using Obymobi.Logic;
using CraveBuilder.CraveService;

namespace CraveBuilder.Testing.Testcases
{
    public class GetMenuTestcase : Testcase
    {
        #region Fields

        private string username = string.Empty;
        private string password = string.Empty;
        private int companyId = 0;
        private int terminalId = 0;
        private int clientId = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GetAdvertisementsTestcase"/> class.
        /// </summary>
        /// <param name="webservice">The webservice.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="companyId">The company id.</param>
        /// <param name="terminalId">The terminal id.</param>
        /// <param name="clientId">The client id.</param>
        public GetMenuTestcase(CraveService.CraveService webservice, string username, string password, int companyId, int terminalId, int clientId) : base(webservice)
        {
            this.username = username;
            this.password = password;
            this.companyId = companyId;
            this.terminalId = terminalId;
            this.clientId = clientId;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Does specific work for the testcase
        /// </summary>
        /// <returns></returns>
        public override TestcaseResult DoWork()
        {
            TestcaseResult tcResult = null;

            ObyTypedResultOfProductmenuItem result = this.Webservice.GetMenu(this.username, this.password, this.companyId, this.terminalId, this.clientId);
            if (result != null)
            {
                tcResult = new ObyTypedTestcaseResult(result.ResultType, result.ResultMessage, result.ResultCode, result.ModelCollection.Length);
                if (result.ResultCode == (int)Obymobi.Enums.GenericResult.Success)
                    tcResult.Success = true;
            }

            return tcResult;
        }

        #endregion
    }
}
