﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Obymobi.Logging;
using System.IO;
using Dionysos;
using System.Windows.Forms;
using CraveBuilder.Configuration;
using System.Text.RegularExpressions;

namespace CraveBuilder
{
    public class Builder
    {
        #region Fields

        private TextBox tbLog = null;

        private List<string> excludedWebsiteExtensions = new List<string>() { ".pdb" };
        private List<string> excludedWebsiteDirectories = new List<string>() { "App_Data", "Builds", "Temp", "Downloads", "Error", "exceptions", "Logs", "Media", "Files", "status" };
        private List<string> excludedWebsiteFileNames = new List<string>() { "Web.Authentication.config", "Web.SessionState.config", "Web.AppSettings.config", "Web.SqlServerCatalogNameOverwrites.config", "Web.Webservices.config" };

        private List<string> excludedApplicationExtensions = new List<string>() { ".pdb", ".xml" };
        private List<string> excludedApplicationDirectories = new List<string>() { "Logs", "nl", "Data" };
        private List<string> excludedApplicationFileNames = new List<string>() { "CraveOnsiteServer.config", "CraveOnSiteServer.config", "CraveOnsiteAgent.config", "CraveOnSiteAgent.config", "CraveOnsiteSupportTools.config" };

        private List<Release> releases = new List<Release>();

        private string innoSetupExePath = string.Empty;
        private string reactorExePath = string.Empty;

        private string releasesDir = string.Empty;
        private string dropBoxDir = string.Empty;

        #endregion

        public string ReleaseDir { get { return this.releasesDir; } }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Builder"/> class.
        /// </summary>
        public Builder(TextBox textbox)
        {
            this.tbLog = textbox;

            this.innoSetupExePath = ConfigurationManager.GetString(CraveBuilderConfigConstants.InnoSetupExePath);
            if (!File.Exists(this.innoSetupExePath))
            {
                this.WriteToLog(@"*** WARNING: INNOSETUP NOT FOUND AT PATH '{0}'! Check the InnoSetup executable path in 'CraveBuilder.config' or install InnoSetup from 'S:\InnoSetup\isetup-5.4.3.exe'", innoSetupExePath.ToUpper());
                MessageBox.Show(string.Format("INNOSETUP NOT FOUND AT PATH\r\n'{0}'!\r\n\r\nCheck the InnoSetup executable path in 'CraveBuilder.config' or install InnoSetup from 'S:\\InnoSetup\\isetup-5.4.3.exe'", innoSetupExePath.ToUpper()));
            }

            this.reactorExePath = ConfigurationManager.GetString(CraveBuilderConfigConstants.ReactorExePath);
            if (!File.Exists(reactorExePath))
            {
                this.WriteToLog(@"*** WARNING: REACTOR.NET NOT FOUND AT PATH '{0}'! Check the Reactor.NET executable path in 'CraveBuilder.config' or install Reactor.NET from 'S:\.NET Reactor\dotnet_reactor_setup.exe'", reactorExePath.ToUpper());
                MessageBox.Show(string.Format("REACTOR.NET NOT FOUND AT PATH\r\n'{0}'!\r\n\r\nCheck the Reactor.NET executable path in 'CraveBuilder.config' or install Reactor.NET from 'S:\\.NET Reactor\\dotnet_reactor_setup.exe'", reactorExePath.ToUpper()));
            }

            this.releasesDir = ConfigurationManager.GetString(CraveBuilderConfigConstants.ReleasesDir);
            if (!this.releasesDir.IsNullOrWhiteSpace() && !Directory.Exists(this.releasesDir))
            {
                // Try to create the releases directory
                try
                {
                    Directory.CreateDirectory(this.releasesDir);
                    this.WriteToLog("Releases directory '{0}' created", this.releasesDir);
                }
                catch (Exception ex)
                {
                    this.WriteToLog("*** WARNING: RELEASES DIRECTORY '{0}' COULD NOT BE CREATED. {1}", this.releasesDir, ex.Message);
                    MessageBox.Show(string.Format("RELEASES DIRECTORY '{0}' COULD NOT BE CREATED.\r\n\r\n{1}", this.releasesDir, ex.Message));
                }
            }

            this.dropBoxDir = ConfigurationManager.GetString(CraveBuilderConfigConstants.DropBoxDir);
            if (!this.dropBoxDir.IsNullOrWhiteSpace() && !Directory.Exists(this.dropBoxDir))
            {
                this.WriteToLog("*** WARNING: DROPBOX DIRECTORY '{0}' DOES NOT EXIST", this.dropBoxDir);
                MessageBox.Show(string.Format("DROPBOX DIRECTORY '{0}' DOES NOT EXIST", this.dropBoxDir));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the version of the specified application to the specified version number
        /// </summary>
        /// <param name="application">The application instance to update the version for</param>
        /// <param name="version">The version number to update the application to</param>
        public void UpdateVersion(Application application, string version)
        {
            this.UpdateApplicationVersion(application, version);

            if (application.InstallerFile.Length > 0)
                this.UpdateInstallerVersion(application, version);
        }

        /// <summary>
        /// Updates the version of the specified application to the specified version number
        /// </summary>
        /// <param name="application">The application instance to update the version for</param>
        /// <param name="version">The version number to update the application to</param>
        public void UpdateApplicationVersion(Application application, string version)
        {
            this.WriteToLog("Updating version of application '{0}' to '{1}' - Started", application.Name, version);
            bool success = false;

            // Update the in code version
            string oldVersion = string.Empty;
            if (this.GetCurrentApplicationVersion(application, out oldVersion))
            {
                string fileContents = File.ReadAllText(application.VersionFile);
                fileContents = fileContents.Replace(oldVersion, version);

                // and write the contents
                File.WriteAllText(application.VersionFile, fileContents);
                success = true;
            }

            // Update the version in the Assembly info
            string assemblyInfoPath = Path.Combine(application.Path, "Properties\\AssemblyInfo.cs");
            if (File.Exists(assemblyInfoPath))
            {
                string assemblyInfo = File.ReadAllText(assemblyInfoPath);

                // Update FileVersion
                Regex assemblyInfoVersionRegex = new Regex("AssemblyFileVersion\\(\"(.+?)\"\\)");
                assemblyInfo = assemblyInfoVersionRegex.Replace(assemblyInfo, string.Format("AssemblyFileVersion(\"{0}\")", version));

                // Update Copyright
                Regex copyrightRegex = new Regex("AssemblyCopyright\\(\"(.+?)\"\\)");
                string copyrightText = string.Format("Copyright © Crave Interactive 2008 - {0}", DateTime.Now.Year);
                assemblyInfo = copyrightRegex.Replace(assemblyInfo, string.Format("AssemblyCopyright(\"{0}\")", copyrightText));

                File.WriteAllText(assemblyInfoPath, assemblyInfo);
            }

            if (success)
                this.WriteToLog("Updating version of application '{0}' to '{1}' - Completed OK", application.Name, version);
            else
                this.WriteToLog("Updating version of application '{0}' to '{1}' - FAILED", application.Name, version);
        }

        public bool GetCurrentApplicationVersion(Application application, out string version)
        {
            version = string.Empty;

            if (!File.Exists(application.VersionFile))
                this.WriteToLog("Version file '{0}' does not exist for application '{1}'!", application.VersionFile, application.Name);
            else
            {
                // Get the contents of the version file
                string fileContents = File.ReadAllText(application.VersionFile);

                // Check whether the version file has content 
                // and whether is has the version number specified
                if (fileContents.IsNullOrWhiteSpace() || !fileContents.Contains("Dionysos.Global.ApplicationInfo.ApplicationVersion"))
                    this.WriteToLog("Version file of application '{0}' is empty or does not contain 'Dionysos.Global.ApplicationInfo.ApplicationVersion'!", application.Name);
                else
                {
                    string[] lines = File.ReadAllLines(application.VersionFile);
                    foreach (string line in lines)
                    {
                        // Get the line with the version number
                        if (line.Contains("Dionysos.Global.ApplicationInfo.ApplicationVersion"))
                        {
                            // Get the index of the first quote
                            int firstIndex = line.IndexOf("\"");
                            int lastIndex = line.LastIndexOf("\"");

                            if (firstIndex > 0 && lastIndex > 0)
                            {
                                version = line.Substring(firstIndex + 1, lastIndex - (firstIndex + 1));

                                // Replace the version in the version file
                                break;
                            }
                            else
                                this.WriteToLog("Version number could not be located in version file '{0}' for application '{1}'!", application.VersionFile, application.Name);
                        }
                    }
                }
            }

            return !version.IsNullOrWhiteSpace();
        }

        /// <summary>
        /// Updates the version of the specified application to the specified version number
        /// </summary>
        /// <param name="application">The application instance to update the version for</param>
        /// <param name="version">The version number to update the application to</param>
        public void UpdateInstallerVersion(Application application, string version)
        {
            this.WriteToLog("Updating installer version of application '{0}' to '{1}'", application.Name, version);

            // Check whether the installer file for the application exists
            if (!File.Exists(application.InstallerFile))
                this.WriteToLog("Installer file '{0}' does not exist for application '{1}'!", application.InstallerFile, application.Name);
            else
            {
                // Get the contents of the installer file
                string fileContents = File.ReadAllText(application.InstallerFile);

                // Check whether the installer file has content 
                // and whether is has the version number specified
                if (fileContents.IsNullOrWhiteSpace() || !fileContents.Contains("#define MyAppVersion"))
                    this.WriteToLog("Installer file of application '{0}' is empty or does not contain '#define MyAppVersion'!");
                else
                {
                    string[] lines = File.ReadAllLines(application.InstallerFile);
                    foreach (string line in lines)
                    {
                        // Get the line with the version number
                        if (line.Contains("#define MyAppVersion"))
                        {
                            // Get the index of the first quote
                            int firstIndex = line.IndexOf("\"");
                            int lastIndex = line.LastIndexOf("\"");

                            if (firstIndex > 0 && lastIndex > 0)
                            {
                                string oldVersion = line.Substring(firstIndex + 1, lastIndex - (firstIndex + 1));

                                // Replace the version in the version file
                                fileContents = fileContents.Replace(oldVersion, "v" + version);

                                // and write the contents
                                File.WriteAllText(application.InstallerFile, fileContents);

                                break;
                            }
                            else
                                this.WriteToLog("Version number could not be located in installer file '{0}' for application '{1}'!", application.InstallerFile, application.Name);
                        }
                    }
                }
            }
        }

        public void CreateRelease(Application application, string version, bool obfuscate, bool createInstaller, bool publish, ProgressBar progressBar)
        {
            if (obfuscate)
                this.Obfuscate(application);

            this.WriteToLog("Creating release '{0}' for application '{1}'", version, application.Name);

            // Get all the files for the specified application
            var files = new List<string>();
            this.GetFiles(application.ReleaseDirectory, application, ref files);

            string appDir;
            if (application.IsWebsite)
            {
                appDir = Path.Combine(this.releasesDir, "_CraveCloud", version, application.ExternalFolderName);
            }
            else
            {
                appDir = Path.Combine(this.releasesDir, application.Name);
            }

            // Create the needed directories
            if (!Directory.Exists(appDir))
                Directory.CreateDirectory(appDir);

            string updatesDir = Path.Combine(appDir, "Updates");
            if (!Directory.Exists(updatesDir))
                Directory.CreateDirectory(updatesDir);

            string versionDir;
            string encryptedDir = string.Empty;
            if (!application.IsWebsite)
            {
                versionDir = Path.Combine(updatesDir, version);
                if (Directory.Exists(versionDir))
                    Directory.Delete(versionDir, true);
                Directory.CreateDirectory(versionDir);


                encryptedDir = Path.Combine(appDir, "_SourceFiles\\Encrypted");
                if (!Directory.Exists(encryptedDir))
                    Directory.CreateDirectory(encryptedDir);
            }
            else
                versionDir = appDir;

            progressBar.Minimum = 1;

            if (files.Count > 1)
                progressBar.Maximum = files.Count;
            else
                progressBar.Maximum = 1;

            progressBar.Value = 1;

            // Copy all the files to a temp directory
            foreach (string sourceFilePath in files)
            {
                string fileName = Path.GetFileName(sourceFilePath);

                if (fileName == null)
                    continue;

                string destFilePath = sourceFilePath.Replace(application.ReleaseDirectory, versionDir);
                string extension = Path.GetExtension(sourceFilePath);

                string dirPath = Path.GetDirectoryName(destFilePath);
                string dirName = new DirectoryInfo(dirPath).Name; // dirPath.Substring(dirPath.LastIndexOf("\\") + 1);

                string fileContent = string.Empty;
                bool fileContentChanged = false;

                // Check whether there are still debugger statements in the code
                if (extension.Equals(".js") && File.Exists(sourceFilePath))
                {
                    if (fileContent.IsNullOrWhiteSpace())
                        fileContent = File.ReadAllText(sourceFilePath);
                    if (fileContent.Contains("debugger"))
                    {
                        this.WriteToLog(@"*** WARNING: SOURCE FILE '{0}' CONTAINS 'DEBUGGER' STATEMENT!", sourceFilePath.ToUpper());
                        MessageBox.Show(string.Format("DEBUGGER STATEMENT FOUND IN SOURCE FILE \r\n'{0}'!", sourceFilePath.ToUpper()));
                    }
                }

                bool copy = true;

                if (application.IsWebsite)
                {
                    if (excludedWebsiteExtensions.Contains(extension, true))
                        copy = false;
                    else if (excludedWebsiteDirectories.Contains(dirName, true))
                        copy = false;
                    else if (excludedWebsiteFileNames.Contains(fileName, true))
                        copy = false;
                    else if (dirName.Equals("Bin", StringComparison.InvariantCultureIgnoreCase) && (extension.Equals(".refresh") || extension.Equals(".xml")))
                        copy = false;

                    // Set debug to false
                    if (fileName.Equals("Web.config", StringComparison.OrdinalIgnoreCase))
                    {
                        if (fileContent.IsNullOrWhiteSpace())
                            fileContent = File.ReadAllText(sourceFilePath);

                        // Replace debug false
                        Regex debugAttribute = new Regex("debug=\"(.+?)\"");
                        fileContent = debugAttribute.Replace(fileContent, "debug=\"false\"");
                        fileContentChanged = true;
                    }
                }
                else
                {
                    if (excludedApplicationExtensions.Contains(extension, true))
                        copy = false;
                    else if (excludedApplicationDirectories.Contains(dirName, true))
                        copy = false;
                    else if (excludedApplicationFileNames.Contains(fileName, true))
                        copy = false;
                    else if (fileName.Contains(".vshost", StringComparison.InvariantCultureIgnoreCase))
                        copy = false;
                    else if (extension.Equals(".dll", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (obfuscate && (fileName.StartsWith("Obymobi") || fileName.StartsWith("Dionysos")))
                            copy = false;
                        else if (fileName.StartsWith("CraveOnsiteServer") || fileName.StartsWith("CraveOnsiteAgent"))
                            copy = false;
                    }
                }

                if (copy)
                {
                    // Copy the directories
                    if (!Directory.Exists(dirPath) && !dirName.EndsWith("_Secure"))
                        Directory.CreateDirectory(dirPath);

                    if (dirName.EndsWith("_Secure"))
                    {
                        // Only do something with this directory when it's a obfuscated build
                        if (obfuscate)
                        {
                            // Copy the obfuscated executable to the update directory
                            File.Copy(sourceFilePath, Path.Combine(versionDir, fileName), true);

                            // Copy the obfuscated executable to the encrypted directory for InnoSetup
                            File.Copy(sourceFilePath, Path.Combine(encryptedDir, fileName), true);
                        }
                    }
                    else
                    {
                        // Copy the file to the update directory, or 
                        // write the new content
                        if (fileContentChanged)
                        {
                            File.WriteAllText(destFilePath, fileContent);
                        }
                        else
                        {
                            File.Copy(sourceFilePath, destFilePath, true);
                        }

                        // Copy the file to the encrypted directory for InnoSetup
                        if (!application.IsWebsite)
                            File.Copy(destFilePath, sourceFilePath.Replace(application.ReleaseDirectory, encryptedDir), true);
                    }
                }

                if (progressBar.Value < progressBar.Maximum)
                    progressBar.Value++;

                // Refresh the UI
                System.Windows.Forms.Application.DoEvents();
            }

            string zipFilePath = string.Empty;
            if (!application.IsWebsite)
            {
                // Copy unpack.bat to update zip
                var obySourceFiles = Path.Combine(ConfigurationManager.GetString(CraveBuilderConfigConstants.SourceFiles), application.Name);
                var batchSourceFiles = Path.Combine(obySourceFiles, "_SourceFiles", "Batch");
                foreach (var file in Directory.GetFiles(batchSourceFiles))
                {
                    string filename = Path.GetFileName(file);
                    if (filename == null || filename.Contains("develop"))
                        continue;

                    File.Copy(file, Path.Combine(versionDir, filename), true);
                }

                // Create a zip file
                string zipFileName = string.Format("{0}-{1}.zip", application.Name, version.Replace("1.", string.Empty));
                this.WriteToLog("Creating zip file '{0}' for application '{1}'", zipFileName, application.Name);

                // Refresh the UI
                System.Windows.Forms.Application.DoEvents();

                zipFilePath = Path.Combine(this.releasesDir, zipFileName);
                Dionysos.IO.ZipUtil.ZipDirectory(versionDir, zipFilePath, false, progressBar);

                this.WriteToLog("Release created '{0}' for application '{1}'", version, application.Name);
            }

            // Refresh the UI
            System.Windows.Forms.Application.DoEvents();

            if (publish && zipFilePath.Length > 0)
            {
                // Add the release to the releases list
                this.PublishRelease(new Release(application, version, zipFilePath, true));
            }

            if (createInstaller)
                this.CreateInstaller(application, version, publish);
        }

        public string CreateZipFromFolderContents(string directory, string zipName, string version, ProgressBar progressBar)
        {
            // Create a zip file
            string zipFileName = string.Format("{0}-{1}.zip", zipName, version.Replace("1.", string.Empty));
            this.WriteToLog("Creating zip file '{0}' for application '{1}'", zipFileName, zipName);

            // Refresh the UI
            System.Windows.Forms.Application.DoEvents();

            var zipFilePath = Path.Combine(this.releasesDir, zipFileName);

            Dionysos.IO.ZipUtil.ZipDirectory(directory, zipFilePath, false, progressBar);

            this.WriteToLog("Release created '{0}' for application '{1}'", version, zipName);

            return zipFilePath;
        }

        public void GetFiles(string path, Application application, ref List<string> files)
        {
            // Do the files from the current directory
            foreach (string file in Directory.GetFiles(path))
            {
                files.Add(file);
            }

            // Recursively do the subdirectories
            foreach (string dir in Directory.GetDirectories(path))
            {
                string dirName = dir.Substring(dir.LastIndexOf("\\") + 1);

                if (application.IsWebsite && !this.excludedWebsiteDirectories.Contains(dirName, true))
                    GetFiles(dir, application, ref files);
                else if (!application.IsWebsite && !this.excludedApplicationDirectories.Contains(dirName, true))
                    GetFiles(dir, application, ref files);
            }
        }

        public void Obfuscate(Application application)
        {
            this.WriteToLog("Obfuscating application '{0}'...", application.Name);

            if (!File.Exists(application.ObfuscationFile))
                this.WriteToLog("Obfuscation file '{0}' does not exist for application '{1}'!", application.ObfuscationFile, application.Name);
            else
            {
                if (!File.Exists(reactorExePath))
                    this.WriteToLog("Reactor.NET was not found at path '{0}'!", reactorExePath);
                else
                {
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = reactorExePath;
                    p.StartInfo.Arguments = "-project \"" + application.ObfuscationFile + "\"";
                    p.Start();
                    p.WaitForExit();
                }
            }

            this.WriteToLog("Obfuscating done for application '{0}'", application.Name);

            // Refresh the UI
            System.Windows.Forms.Application.DoEvents();
        }

        public void CreateInstaller(Application application, string version, bool publish)
        {
            this.WriteToLog("Creating installer for application '{0}'...", application.Name);

            // Create the needed directories
            string appDir = Path.Combine(this.releasesDir, application.Name);
            if (!Directory.Exists(appDir))
                Directory.CreateDirectory(appDir);

            string installersDir = Path.Combine(appDir, "Installers");
            if (!Directory.Exists(installersDir))
                Directory.CreateDirectory(installersDir);

            string versionDir = Path.Combine(installersDir, version);
            if (Directory.Exists(versionDir))
                Directory.Delete(versionDir, true);
            Directory.CreateDirectory(versionDir);

            // Check whether the source files dir exists
            string sourceFilesDirRoot = Path.Combine(Dionysos.ConfigurationManager.GetString(CraveBuilderConfigConstants.SourceFiles), application.Name);
            string sourceFilesDir = Path.Combine(sourceFilesDirRoot, "_SourceFiles");
            string batchDir = Path.Combine(sourceFilesDir, "Batch");
            string devExpressDir = Path.Combine(sourceFilesDir, "DevExpress");
            string encryptedDir = Path.Combine(sourceFilesDir, "Encrypted");
            string nlDir = Path.Combine(sourceFilesDir, "nl");

            if (!Directory.Exists(sourceFilesDir))
                this.WriteToLog("Source files directory '{0}' does not exist for application '{1}'!", sourceFilesDir, application.Name);
            else if (!Directory.Exists(batchDir))
                this.WriteToLog("Batch files directory '{0}' does not exist for application '{1}'!", batchDir, application.Name);
            else if (!Directory.Exists(devExpressDir))
                this.WriteToLog("DevExpress files directory '{0}' does not exist for application '{1}'!", devExpressDir, application.Name);
            else if (!Directory.Exists(encryptedDir))
                this.WriteToLog("Encrypted files directory '{0}' does not exist for application '{1}'!", encryptedDir, application.Name);
            else if (!Directory.Exists(nlDir))
                this.WriteToLog("nl files directory '{0}' does not exist for application '{1}'!", nlDir, application.Name);
            else if (!File.Exists(application.InstallerFile))
                this.WriteToLog("Installer file '{0}' does not exist for application '{1}'!", application.InstallerFile, application.Name);
            else
            {
                if (!File.Exists(innoSetupExePath))
                    this.WriteToLog("InnoSetup was not found at path '{0}'!", innoSetupExePath);
                else
                {
                    var startInfo = new ProcessStartInfo();
                    startInfo.FileName = innoSetupExePath;
                    startInfo.Arguments = application.InstallerFile;
                    startInfo.UseShellExecute = false;
                    startInfo.RedirectStandardOutput = true;
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;

                    using (var p = Process.Start(startInfo))
                    {
                        using (StreamReader reader = p.StandardOutput)
                        {
                            while (!p.HasExited)
                            {
                                string str = reader.ReadLine();
                                this.tbLog.Invoke((MethodInvoker) (() => this.WriteToLog(str)));

                                // Refresh the UI
                                System.Windows.Forms.Application.DoEvents();
                            }
                        }
                    }

                    // Copy the installer file to the proper directory
                    string installerFile = Path.Combine(Constants.DotNetPath, @"Obymobi\Obymobi.Files\InnoSetup\Output\setup.exe");
                    if (!File.Exists(installerFile))
                        this.WriteToLog("Installer file '{0}' does not exist!", installerFile);
                    else
                    {
                        string destFilePath = Path.Combine(versionDir, string.Format("{0}-{1}.exe", application.Name, version));
                        File.Copy(installerFile, destFilePath, true);

                        this.WriteToLog("Installer created for application '{0}'...", application.Name);

                        if (publish)
                        {
                            // Add the release to the releases list
                            this.PublishRelease(new Release(application, version, destFilePath, false));
                        }
                    }
                }
            }

            // Refresh the UI
            System.Windows.Forms.Application.DoEvents();
        }

        /// <summary>
        /// Get the lastest version published on Dropbox
        /// </summary>
        /// <param name="application">Application to check for</param>
        /// <param name="update">If true, checks for Updates, otherwise for Installer</param>
        /// <returns></returns>
        public string GetLatestVersion(Application application, bool update)
        {
            if (application.IsWebsite)
            {
                var appDir = Path.Combine(this.dropBoxDir, "_Crave Cloud Software");

                if (Directory.Exists(appDir))
                {
                    // If we're in trunk, than we want the latest version, otherwise we want the lastest version for this branch
                    string branch = Constants.DotNetPathBranch;

                    string[] files;
                    if (branch.IsNullOrWhiteSpace())
                    {
                        // Trunk
                        files = Directory.GetFiles(appDir, "CraveCloud-*"); // Get only with 1.*
                    }
                    else
                    {
                        // Branch
                        files = Directory.GetFiles(appDir, "CraveCloud-" + branch + "*"); // Get only with 1.[branch]*
                    }

                    if (files.Length > 0)
                    {
                        string pathOfLastestVersion = files.Max();
                        return new FileInfo(pathOfLastestVersion).Name;
                    }

                    return "1.0000000000";
                }
            }
            else
            {
                var appDir = Path.Combine(this.dropBoxDir, application.Name);


                if (update)
                    appDir = Path.Combine(appDir, "Updates");
                else
                    appDir = Path.Combine(appDir, "Installers");

                if (Directory.Exists(appDir))
                {
                    // If we're in trunk, than we want the latest version, otherwise we want the lastest version for this branch
                    string branch = Constants.DotNetPathBranch;

                    string[] folders;
                    if (branch.IsNullOrWhiteSpace())
                    {
                        // Trunk
                        folders = Directory.GetDirectories(appDir, "1.*"); // Get only with 1.*
                    }
                    else
                    {
                        // Branch
                        folders = Directory.GetDirectories(appDir, "1." + branch + "*"); // Get only with 1.[branch]*
                    }

                    if (folders.Length > 0)
                    {
                        string pathOfLastestVersion = folders.Max();
                        return new DirectoryInfo(pathOfLastestVersion).Name;
                    }

                    return "1.0000000000";
                }
            }

            return "App dir does not exist";
        }

        public void PublishRelease(Release release)
        {
            string destFilePath;

            if (release.Application.IsWebsite)
            {
                destFilePath = string.Format(@"{0}\_Crave Cloud Software\{1}", this.dropBoxDir, Path.GetFileName(release.FilePath));
            }
            else
            {
                if (release.IsUpdate)
                    destFilePath = string.Format(@"{0}\{1}\Updates\{2}\{3}", this.dropBoxDir, release.Application.Name, release.Version, Path.GetFileName(release.FilePath));
                else
                    destFilePath = string.Format(@"{0}\{1}\Installers\{2}\{3}", this.dropBoxDir, release.Application.Name, release.Version, Path.GetFileName(release.FilePath));
            }

            this.WriteToLog("Publishing release '{0}' for application '{1}' to '{2}'", release.Version, release.Application.Name, destFilePath);

            string destDir = Path.GetDirectoryName(destFilePath);

            if (!Directory.Exists(destDir))
                Directory.CreateDirectory(destDir);

            // Copy the zip file
            File.Copy(release.FilePath, destFilePath, true);

            // Refresh the UI
            System.Windows.Forms.Application.DoEvents();

            this.WriteToLog("Release published");
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        private void WriteToLog(string contents, params object[] args)
        {
            // Write to file
            DesktopLogger.Debug(contents, args);

            // and add it to the log textbox
            this.tbLog.Text = DateTime.Now.ToShortTimeString() + " - " + string.Format(contents, args) + "\r\n" + this.tbLog.Text;
        }

        #endregion
    }
}
