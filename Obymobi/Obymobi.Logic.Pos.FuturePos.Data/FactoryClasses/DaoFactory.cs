﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

using Obymobi.Logic.Pos.FuturePos.Data.DaoClasses;
using Obymobi.Logic.Pos.FuturePos.Data.HelperClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses
{
	/// <summary>
	/// Generic factory for DAO objects. 
	/// </summary>
	public partial class DAOFactory
	{
		/// <summary>
		/// Private CTor, no instantiation possible.
		/// </summary>
		private DAOFactory()
		{
		}

		/// <summary>Creates a new DepartmentDAO object</summary>
		/// <returns>the new DAO object ready to use for Department Entities</returns>
		public static DepartmentDAO CreateDepartmentDAO()
		{
			return new DepartmentDAO();
		}

		/// <summary>Creates a new ItemDAO object</summary>
		/// <returns>the new DAO object ready to use for Item Entities</returns>
		public static ItemDAO CreateItemDAO()
		{
			return new ItemDAO();
		}

		/// <summary>Creates a new ItemDayDAO object</summary>
		/// <returns>the new DAO object ready to use for ItemDay Entities</returns>
		public static ItemDayDAO CreateItemDayDAO()
		{
			return new ItemDayDAO();
		}

		/// <summary>Creates a new ItemIngredientDAO object</summary>
		/// <returns>the new DAO object ready to use for ItemIngredient Entities</returns>
		public static ItemIngredientDAO CreateItemIngredientDAO()
		{
			return new ItemIngredientDAO();
		}

		/// <summary>Creates a new ItemModifierDAO object</summary>
		/// <returns>the new DAO object ready to use for ItemModifier Entities</returns>
		public static ItemModifierDAO CreateItemModifierDAO()
		{
			return new ItemModifierDAO();
		}

		/// <summary>Creates a new ItemPriceDAO object</summary>
		/// <returns>the new DAO object ready to use for ItemPrice Entities</returns>
		public static ItemPriceDAO CreateItemPriceDAO()
		{
			return new ItemPriceDAO();
		}

		/// <summary>Creates a new ItemPrinterDAO object</summary>
		/// <returns>the new DAO object ready to use for ItemPrinter Entities</returns>
		public static ItemPrinterDAO CreateItemPrinterDAO()
		{
			return new ItemPrinterDAO();
		}

		/// <summary>Creates a new LayoutRoomDAO object</summary>
		/// <returns>the new DAO object ready to use for LayoutRoom Entities</returns>
		public static LayoutRoomDAO CreateLayoutRoomDAO()
		{
			return new LayoutRoomDAO();
		}

		/// <summary>Creates a new LayoutTableDAO object</summary>
		/// <returns>the new DAO object ready to use for LayoutTable Entities</returns>
		public static LayoutTableDAO CreateLayoutTableDAO()
		{
			return new LayoutTableDAO();
		}

		/// <summary>Creates a new PriceLevelDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceLevel Entities</returns>
		public static PriceLevelDAO CreatePriceLevelDAO()
		{
			return new PriceLevelDAO();
		}

		/// <summary>Creates a new PriceScheduleDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceSchedule Entities</returns>
		public static PriceScheduleDAO CreatePriceScheduleDAO()
		{
			return new PriceScheduleDAO();
		}

		/// <summary>Creates a new VendorDAO object</summary>
		/// <returns>the new DAO object ready to use for Vendor Entities</returns>
		public static VendorDAO CreateVendorDAO()
		{
			return new VendorDAO();
		}

		/// <summary>Creates a new VersionDAO object</summary>
		/// <returns>the new DAO object ready to use for Version Entities</returns>
		public static VersionDAO CreateVersionDAO()
		{
			return new VersionDAO();
		}

		/// <summary>Creates a new TypedListDAO object</summary>
		/// <returns>The new DAO object ready to use for Typed Lists</returns>
		public static TypedListDAO CreateTypedListDAO()
		{
			return new TypedListDAO();
		}

		#region Included Code

		#endregion
	}
}
