﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using Obymobi.Logic.Pos.FuturePos.Data.HelperClasses;
using Obymobi.Logic.Pos.FuturePos.Data.RelationClasses;
using Obymobi.Logic.Pos.FuturePos.Data.DaoClasses;

using Obymobi.Logic.Pos.FuturePos.Data.EntityClasses;
using Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>general base class for the generated factories</summary>
	[Serializable]
	public partial class EntityFactoryBase : EntityFactoryCore
	{
		private readonly Obymobi.Logic.Pos.FuturePos.Data.EntityType _typeOfEntity;
		
		/// <summary>CTor</summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="typeOfEntity">The type of entity.</param>
		public EntityFactoryBase(string entityName, Obymobi.Logic.Pos.FuturePos.Data.EntityType typeOfEntity) : base(entityName)
		{
			_typeOfEntity = typeOfEntity;
		}

		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.Create((Obymobi.Logic.Pos.FuturePos.Data.EntityType)entityTypeValue);
		}
		
		/// <summary>Creates, using the generated EntityFieldsFactory, the IEntityFields object for the entity to create. </summary>
		/// <returns>Empty IEntityFields object.</returns>
		public override IEntityFields CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(_typeOfEntity);
		}

		/// <summary>Creates the relations collection to the entity to join all targets so this entity can be fetched. </summary>
		/// <param name="objectAlias">The object alias to use for the elements in the relations.</param>
		/// <returns>null if the entity isn't in a hierarchy of type TargetPerEntity, otherwise the relations collection needed to join all targets together to fetch all subtypes of this entity and this entity itself</returns>
		public override IRelationCollection CreateHierarchyRelations(string objectAlias) 
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetHierarchyRelations(ForEntityName, objectAlias);
		}

		/// <summary>This method retrieves, using the InheritanceInfoprovider, the factory for the entity represented by the values passed in.</summary>
		/// <param name="fieldValues">Field values read from the db, to determine which factory to return, based on the field values passed in.</param>
		/// <param name="entityFieldStartIndexesPerEntity">indexes into values where per entity type their own fields start.</param>
		/// <returns>the factory for the entity which is represented by the values passed in.</returns>
		public override IEntityFactory GetEntityFactory(object[] fieldValues, Dictionary<string, int> entityFieldStartIndexesPerEntity)
		{
			return (IEntityFactory)InheritanceInfoProviderSingleton.GetInstance().GetEntityFactory(ForEntityName, fieldValues, entityFieldStartIndexesPerEntity) ?? this;
		}
						
		/// <summary>Creates a new entity collection for the entity of this factory.</summary>
		/// <returns>ready to use new entity collection, typed.</returns>
		public override IEntityCollection CreateEntityCollection()
		{
			return GeneralEntityCollectionFactory.Create(_typeOfEntity);
		}
	}
	
	/// <summary>Factory to create new, empty DepartmentEntity objects.</summary>
	[Serializable]
	public partial class DepartmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DepartmentEntityFactory() : base("DepartmentEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.DepartmentEntity) { }

		/// <summary>Creates a new, empty DepartmentEntity object.</summary>
		/// <returns>A new, empty DepartmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DepartmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDepartment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ItemEntity objects.</summary>
	[Serializable]
	public partial class ItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ItemEntityFactory() : base("ItemEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity) { }

		/// <summary>Creates a new, empty ItemEntity object.</summary>
		/// <returns>A new, empty ItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ItemDayEntity objects.</summary>
	[Serializable]
	public partial class ItemDayEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ItemDayEntityFactory() : base("ItemDayEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemDayEntity) { }

		/// <summary>Creates a new, empty ItemDayEntity object.</summary>
		/// <returns>A new, empty ItemDayEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ItemDayEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewItemDay
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ItemIngredientEntity objects.</summary>
	[Serializable]
	public partial class ItemIngredientEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ItemIngredientEntityFactory() : base("ItemIngredientEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemIngredientEntity) { }

		/// <summary>Creates a new, empty ItemIngredientEntity object.</summary>
		/// <returns>A new, empty ItemIngredientEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ItemIngredientEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewItemIngredient
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ItemModifierEntity objects.</summary>
	[Serializable]
	public partial class ItemModifierEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ItemModifierEntityFactory() : base("ItemModifierEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemModifierEntity) { }

		/// <summary>Creates a new, empty ItemModifierEntity object.</summary>
		/// <returns>A new, empty ItemModifierEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ItemModifierEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewItemModifier
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ItemPriceEntity objects.</summary>
	[Serializable]
	public partial class ItemPriceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ItemPriceEntityFactory() : base("ItemPriceEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPriceEntity) { }

		/// <summary>Creates a new, empty ItemPriceEntity object.</summary>
		/// <returns>A new, empty ItemPriceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ItemPriceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewItemPrice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ItemPrinterEntity objects.</summary>
	[Serializable]
	public partial class ItemPrinterEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ItemPrinterEntityFactory() : base("ItemPrinterEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPrinterEntity) { }

		/// <summary>Creates a new, empty ItemPrinterEntity object.</summary>
		/// <returns>A new, empty ItemPrinterEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ItemPrinterEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewItemPrinter
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LayoutRoomEntity objects.</summary>
	[Serializable]
	public partial class LayoutRoomEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LayoutRoomEntityFactory() : base("LayoutRoomEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.LayoutRoomEntity) { }

		/// <summary>Creates a new, empty LayoutRoomEntity object.</summary>
		/// <returns>A new, empty LayoutRoomEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LayoutRoomEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLayoutRoom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LayoutTableEntity objects.</summary>
	[Serializable]
	public partial class LayoutTableEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LayoutTableEntityFactory() : base("LayoutTableEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.LayoutTableEntity) { }

		/// <summary>Creates a new, empty LayoutTableEntity object.</summary>
		/// <returns>A new, empty LayoutTableEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LayoutTableEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLayoutTable
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceLevelEntity objects.</summary>
	[Serializable]
	public partial class PriceLevelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceLevelEntityFactory() : base("PriceLevelEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.PriceLevelEntity) { }

		/// <summary>Creates a new, empty PriceLevelEntity object.</summary>
		/// <returns>A new, empty PriceLevelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceLevelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceLevel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceScheduleEntity objects.</summary>
	[Serializable]
	public partial class PriceScheduleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceScheduleEntityFactory() : base("PriceScheduleEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.PriceScheduleEntity) { }

		/// <summary>Creates a new, empty PriceScheduleEntity object.</summary>
		/// <returns>A new, empty PriceScheduleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceScheduleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceSchedule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VendorEntity objects.</summary>
	[Serializable]
	public partial class VendorEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VendorEntityFactory() : base("VendorEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.VendorEntity) { }

		/// <summary>Creates a new, empty VendorEntity object.</summary>
		/// <returns>A new, empty VendorEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VendorEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVendor
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VersionEntity objects.</summary>
	[Serializable]
	public partial class VersionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VersionEntityFactory() : base("VersionEntity", Obymobi.Logic.Pos.FuturePos.Data.EntityType.VersionEntity) { }

		/// <summary>Creates a new, empty VersionEntity object.</summary>
		/// <returns>A new, empty VersionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VersionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVersion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new entity collection objects</summary>
	[Serializable]
	public partial class GeneralEntityCollectionFactory
	{
		/// <summary>Creates a new entity collection</summary>
		/// <param name="typeToUse">The entity type to create the collection for.</param>
		/// <returns>A new entity collection object.</returns>
		public static IEntityCollection Create(Obymobi.Logic.Pos.FuturePos.Data.EntityType typeToUse)
		{
			switch(typeToUse)
			{
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.DepartmentEntity:
					return new DepartmentCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity:
					return new ItemCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemDayEntity:
					return new ItemDayCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemIngredientEntity:
					return new ItemIngredientCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemModifierEntity:
					return new ItemModifierCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPriceEntity:
					return new ItemPriceCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPrinterEntity:
					return new ItemPrinterCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.LayoutRoomEntity:
					return new LayoutRoomCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.LayoutTableEntity:
					return new LayoutTableCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.PriceLevelEntity:
					return new PriceLevelCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.PriceScheduleEntity:
					return new PriceScheduleCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.VendorEntity:
					return new VendorCollection();
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.VersionEntity:
					return new VersionCollection();
				default:
					return null;
			}
		}		
	}
	
	/// <summary>Factory to create new, empty Entity objects based on the entity type specified. Uses entity specific factory objects</summary>
	[Serializable]
	public partial class GeneralEntityFactory
	{
		/// <summary>Creates a new, empty Entity object of the type specified</summary>
		/// <param name="entityTypeToCreate">The entity type to create.</param>
		/// <returns>A new, empty Entity object.</returns>
		public static IEntity Create(Obymobi.Logic.Pos.FuturePos.Data.EntityType entityTypeToCreate)
		{
			IEntityFactory factoryToUse = null;
			switch(entityTypeToCreate)
			{
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.DepartmentEntity:
					factoryToUse = new DepartmentEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity:
					factoryToUse = new ItemEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemDayEntity:
					factoryToUse = new ItemDayEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemIngredientEntity:
					factoryToUse = new ItemIngredientEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemModifierEntity:
					factoryToUse = new ItemModifierEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPriceEntity:
					factoryToUse = new ItemPriceEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPrinterEntity:
					factoryToUse = new ItemPrinterEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.LayoutRoomEntity:
					factoryToUse = new LayoutRoomEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.LayoutTableEntity:
					factoryToUse = new LayoutTableEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.PriceLevelEntity:
					factoryToUse = new PriceLevelEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.PriceScheduleEntity:
					factoryToUse = new PriceScheduleEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.VendorEntity:
					factoryToUse = new VendorEntityFactory();
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.VersionEntity:
					factoryToUse = new VersionEntityFactory();
					break;
			}
			IEntity toReturn = null;
			if(factoryToUse != null)
			{
				toReturn = factoryToUse.Create();
			}
			return toReturn;
		}		
	}
	
	/// <summary>Class which is used to obtain the entity factory based on the .NET type of the entity. </summary>
	[Serializable]
	public static class EntityFactoryFactory
	{
		private static readonly Dictionary<Type, IEntityFactory> _factoryPerType = new Dictionary<Type, IEntityFactory>();

		/// <summary>Initializes the <see cref="EntityFactoryFactory"/> class.</summary>
		static EntityFactoryFactory()
		{
			Array entityTypeValues = Enum.GetValues(typeof(Obymobi.Logic.Pos.FuturePos.Data.EntityType));
			foreach(int entityTypeValue in entityTypeValues)
			{
				IEntity dummy = GeneralEntityFactory.Create((Obymobi.Logic.Pos.FuturePos.Data.EntityType)entityTypeValue);
				_factoryPerType.Add(dummy.GetType(), dummy.GetEntityFactory());
			}
		}

		/// <summary>Gets the factory of the entity with the .NET type specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory GetFactory(Type typeOfEntity)
		{
			IEntityFactory toReturn = null;
			_factoryPerType.TryGetValue(typeOfEntity, out toReturn);
			return toReturn;
		}

		/// <summary>Gets the factory of the entity with the Obymobi.Logic.Pos.FuturePos.Data.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory GetFactory(Obymobi.Logic.Pos.FuturePos.Data.EntityType typeOfEntity)
		{
			return GetFactory(GeneralEntityFactory.Create(typeOfEntity).GetType());
		}
	}
	
	/// <summary>Element creator for creating project elements from somewhere else, like inside Linq providers.</summary>
	public class ElementCreator : ElementCreatorBase, IElementCreator
	{
		/// <summary>Gets the factory of the Entity type with the Obymobi.Logic.Pos.FuturePos.Data.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory GetFactory(int entityTypeValue)
		{
			return (IEntityFactory)this.GetFactoryImpl(entityTypeValue);
		}

		/// <summary>Gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory GetFactory(Type typeOfEntity)
		{
			return (IEntityFactory)this.GetFactoryImpl(typeOfEntity);
		}

		/// <summary>Creates a new resultset fields object with the number of field slots reserved as specified</summary>
		/// <param name="numberOfFields">The number of fields.</param>
		/// <returns>ready to use resultsetfields object</returns>
		public IEntityFields CreateResultsetFields(int numberOfFields)
		{
			return new ResultsetFields(numberOfFields);
		}
		
		/// <summary>Gets an instance of the TypedListDAO class to execute dynamic lists and projections.</summary>
		/// <returns>ready to use typedlistDAO</returns>
		public IDao GetTypedListDao()
		{
			return new TypedListDAO();
		}
		
		/// <summary>Obtains the inheritance info provider instance from the singleton </summary>
		/// <returns>The singleton instance of the inheritance info provider</returns>
		public override IInheritanceInfoProvider ObtainInheritanceInfoProviderInstance()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}


		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand)
		{
			return new DynamicRelation(leftOperand);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(IEntityFieldCore leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, string aliasLeftOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, aliasLeftOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (Obymobi.Logic.Pos.FuturePos.Data.EntityType)Enum.Parse(typeof(Obymobi.Logic.Pos.FuturePos.Data.EntityType), rightOperandEntityName, false), aliasRightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperandEntityName">Name of the entity which is used as the left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(string leftOperandEntityName, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation((Obymobi.Logic.Pos.FuturePos.Data.EntityType)Enum.Parse(typeof(Obymobi.Logic.Pos.FuturePos.Data.EntityType), leftOperandEntityName, false), joinType, (Obymobi.Logic.Pos.FuturePos.Data.EntityType)Enum.Parse(typeof(Obymobi.Logic.Pos.FuturePos.Data.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}
		
		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(IEntityFieldCore leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (Obymobi.Logic.Pos.FuturePos.Data.EntityType)Enum.Parse(typeof(Obymobi.Logic.Pos.FuturePos.Data.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}
		
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the Obymobi.Logic.Pos.FuturePos.Data.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(int entityTypeValue)
		{
			return EntityFactoryFactory.GetFactory((Obymobi.Logic.Pos.FuturePos.Data.EntityType)entityTypeValue);
		}
	
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(Type typeOfEntity)
		{
			return EntityFactoryFactory.GetFactory(typeOfEntity);
		}

	}
}
