﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Logic.Pos.FuturePos.Data;
using Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses;
using Obymobi.Logic.Pos.FuturePos.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Item. </summary>
	public partial class ItemRelations
	{
		/// <summary>CTor</summary>
		public ItemRelations()
		{
		}

		/// <summary>Gets all relations of the ItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ItemDayEntityUsingItemId);
			toReturn.Add(this.ItemIngredientEntityUsingItemId);
			toReturn.Add(this.ItemModifierEntityUsingItemId);
			toReturn.Add(this.ItemPriceEntityUsingItemId);
			toReturn.Add(this.ItemPrinterEntityUsingItemId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ItemEntity and ItemDayEntity over the 1:n relation they have, using the relation between the fields:
		/// Item.ItemId - ItemDay.ItemId
		/// </summary>
		public virtual IEntityRelation ItemDayEntityUsingItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ItemDay" , true);
				relation.AddEntityFieldPair(ItemFields.ItemId, ItemDayFields.ItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemDayEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ItemEntity and ItemIngredientEntity over the 1:n relation they have, using the relation between the fields:
		/// Item.ItemId - ItemIngredient.ItemId
		/// </summary>
		public virtual IEntityRelation ItemIngredientEntityUsingItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ItemIngredient" , true);
				relation.AddEntityFieldPair(ItemFields.ItemId, ItemIngredientFields.ItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemIngredientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ItemEntity and ItemModifierEntity over the 1:n relation they have, using the relation between the fields:
		/// Item.ItemId - ItemModifier.ItemId
		/// </summary>
		public virtual IEntityRelation ItemModifierEntityUsingItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ItemModifier" , true);
				relation.AddEntityFieldPair(ItemFields.ItemId, ItemModifierFields.ItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemModifierEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ItemEntity and ItemPriceEntity over the 1:n relation they have, using the relation between the fields:
		/// Item.ItemId - ItemPrice.ItemId
		/// </summary>
		public virtual IEntityRelation ItemPriceEntityUsingItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ItemPrice" , true);
				relation.AddEntityFieldPair(ItemFields.ItemId, ItemPriceFields.ItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemPriceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ItemEntity and ItemPrinterEntity over the 1:n relation they have, using the relation between the fields:
		/// Item.ItemId - ItemPrinter.ItemId
		/// </summary>
		public virtual IEntityRelation ItemPrinterEntityUsingItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ItemPrinter" , true);
				relation.AddEntityFieldPair(ItemFields.ItemId, ItemPrinterFields.ItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemPrinterEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticItemRelations
	{
		internal static readonly IEntityRelation ItemDayEntityUsingItemIdStatic = new ItemRelations().ItemDayEntityUsingItemId;
		internal static readonly IEntityRelation ItemIngredientEntityUsingItemIdStatic = new ItemRelations().ItemIngredientEntityUsingItemId;
		internal static readonly IEntityRelation ItemModifierEntityUsingItemIdStatic = new ItemRelations().ItemModifierEntityUsingItemId;
		internal static readonly IEntityRelation ItemPriceEntityUsingItemIdStatic = new ItemRelations().ItemPriceEntityUsingItemId;
		internal static readonly IEntityRelation ItemPrinterEntityUsingItemIdStatic = new ItemRelations().ItemPrinterEntityUsingItemId;

		/// <summary>CTor</summary>
		static StaticItemRelations()
		{
		}
	}
}
