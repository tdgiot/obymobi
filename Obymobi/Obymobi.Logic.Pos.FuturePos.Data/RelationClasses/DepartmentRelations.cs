﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Logic.Pos.FuturePos.Data;
using Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses;
using Obymobi.Logic.Pos.FuturePos.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Department. </summary>
	public partial class DepartmentRelations
	{
		/// <summary>CTor</summary>
		public DepartmentRelations()
		{
		}

		/// <summary>Gets all relations of the DepartmentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ItemDayEntityUsingDepartmentId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DepartmentEntity and ItemDayEntity over the 1:n relation they have, using the relation between the fields:
		/// Department.DepartmentId - ItemDay.DepartmentId
		/// </summary>
		public virtual IEntityRelation ItemDayEntityUsingDepartmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ItemDay" , true);
				relation.AddEntityFieldPair(DepartmentFields.DepartmentId, ItemDayFields.DepartmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepartmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ItemDayEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDepartmentRelations
	{
		internal static readonly IEntityRelation ItemDayEntityUsingDepartmentIdStatic = new DepartmentRelations().ItemDayEntityUsingDepartmentId;

		/// <summary>CTor</summary>
		static StaticDepartmentRelations()
		{
		}
	}
}
