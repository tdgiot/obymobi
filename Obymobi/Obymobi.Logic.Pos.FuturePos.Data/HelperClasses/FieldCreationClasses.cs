﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses;
using Obymobi.Logic.Pos.FuturePos.Data;

namespace Obymobi.Logic.Pos.FuturePos.Data.HelperClasses
{
	/// <summary>Field Creation Class for entity DepartmentEntity</summary>
	public partial class DepartmentFields
	{
		/// <summary>Creates a new DepartmentEntity.DepartmentId field instance</summary>
		public static EntityField DepartmentId
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.DepartmentId);}
		}
		/// <summary>Creates a new DepartmentEntity.RegionId field instance</summary>
		public static EntityField RegionId
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.RegionId);}
		}
		/// <summary>Creates a new DepartmentEntity.DepartmentName field instance</summary>
		public static EntityField DepartmentName
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.DepartmentName);}
		}
		/// <summary>Creates a new DepartmentEntity.DepartmentDescription field instance</summary>
		public static EntityField DepartmentDescription
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.DepartmentDescription);}
		}
		/// <summary>Creates a new DepartmentEntity.GroupName field instance</summary>
		public static EntityField GroupName
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.GroupName);}
		}
		/// <summary>Creates a new DepartmentEntity.IsHash field instance</summary>
		public static EntityField IsHash
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.IsHash);}
		}
		/// <summary>Creates a new DepartmentEntity.PmsbucketNumber field instance</summary>
		public static EntityField PmsbucketNumber
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.PmsbucketNumber);}
		}
		/// <summary>Creates a new DepartmentEntity.IsUsedOnline field instance</summary>
		public static EntityField IsUsedOnline
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.IsUsedOnline);}
		}
		/// <summary>Creates a new DepartmentEntity.WebTitle field instance</summary>
		public static EntityField WebTitle
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.WebTitle);}
		}
		/// <summary>Creates a new DepartmentEntity.WebDescription field instance</summary>
		public static EntityField WebDescription
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.WebDescription);}
		}
		/// <summary>Creates a new DepartmentEntity.WebThumbPath field instance</summary>
		public static EntityField WebThumbPath
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.WebThumbPath);}
		}
		/// <summary>Creates a new DepartmentEntity.WebImagePath field instance</summary>
		public static EntityField WebImagePath
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.WebImagePath);}
		}
		/// <summary>Creates a new DepartmentEntity.IsDefault field instance</summary>
		public static EntityField IsDefault
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.IsDefault);}
		}
		/// <summary>Creates a new DepartmentEntity.DisplayIndex field instance</summary>
		public static EntityField DisplayIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.DisplayIndex);}
		}
		/// <summary>Creates a new DepartmentEntity.IsParent field instance</summary>
		public static EntityField IsParent
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.IsParent);}
		}
		/// <summary>Creates a new DepartmentEntity.ParentDepartmentId field instance</summary>
		public static EntityField ParentDepartmentId
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.ParentDepartmentId);}
		}
		/// <summary>Creates a new DepartmentEntity.Vduid field instance</summary>
		public static EntityField Vduid
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.Vduid);}
		}
		/// <summary>Creates a new DepartmentEntity.AskId field instance</summary>
		public static EntityField AskId
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.AskId);}
		}
		/// <summary>Creates a new DepartmentEntity.QuantityMultiplier field instance</summary>
		public static EntityField QuantityMultiplier
		{
			get { return (EntityField)EntityFieldFactory.Create(DepartmentFieldIndex.QuantityMultiplier);}
		}
	}

	/// <summary>Field Creation Class for entity ItemEntity</summary>
	public partial class ItemFields
	{
		/// <summary>Creates a new ItemEntity.ItemId field instance</summary>
		public static EntityField ItemId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ItemId);}
		}
		/// <summary>Creates a new ItemEntity.RegionId field instance</summary>
		public static EntityField RegionId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.RegionId);}
		}
		/// <summary>Creates a new ItemEntity.ItemName field instance</summary>
		public static EntityField ItemName
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ItemName);}
		}
		/// <summary>Creates a new ItemEntity.ItemDescription field instance</summary>
		public static EntityField ItemDescription
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ItemDescription);}
		}
		/// <summary>Creates a new ItemEntity.Department field instance</summary>
		public static EntityField Department
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.Department);}
		}
		/// <summary>Creates a new ItemEntity.Upc field instance</summary>
		public static EntityField Upc
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.Upc);}
		}
		/// <summary>Creates a new ItemEntity.ReceiptDesc field instance</summary>
		public static EntityField ReceiptDesc
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ReceiptDesc);}
		}
		/// <summary>Creates a new ItemEntity.PrintDoubleWide field instance</summary>
		public static EntityField PrintDoubleWide
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.PrintDoubleWide);}
		}
		/// <summary>Creates a new ItemEntity.PrintAltColor field instance</summary>
		public static EntityField PrintAltColor
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.PrintAltColor);}
		}
		/// <summary>Creates a new ItemEntity.ItemCount field instance</summary>
		public static EntityField ItemCount
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ItemCount);}
		}
		/// <summary>Creates a new ItemEntity.AlternateItem field instance</summary>
		public static EntityField AlternateItem
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.AlternateItem);}
		}
		/// <summary>Creates a new ItemEntity.FireDelay field instance</summary>
		public static EntityField FireDelay
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.FireDelay);}
		}
		/// <summary>Creates a new ItemEntity.IsModifier field instance</summary>
		public static EntityField IsModifier
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsModifier);}
		}
		/// <summary>Creates a new ItemEntity.OrderPriority field instance</summary>
		public static EntityField OrderPriority
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.OrderPriority);}
		}
		/// <summary>Creates a new ItemEntity.IsStoreChargeable field instance</summary>
		public static EntityField IsStoreChargeable
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsStoreChargeable);}
		}
		/// <summary>Creates a new ItemEntity.AskForPrice field instance</summary>
		public static EntityField AskForPrice
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.AskForPrice);}
		}
		/// <summary>Creates a new ItemEntity.TogoSurcharge field instance</summary>
		public static EntityField TogoSurcharge
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.TogoSurcharge);}
		}
		/// <summary>Creates a new ItemEntity.ItemCost field instance</summary>
		public static EntityField ItemCost
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ItemCost);}
		}
		/// <summary>Creates a new ItemEntity.IsScaleable field instance</summary>
		public static EntityField IsScaleable
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsScaleable);}
		}
		/// <summary>Creates a new ItemEntity.IsNotTippable field instance</summary>
		public static EntityField IsNotTippable
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsNotTippable);}
		}
		/// <summary>Creates a new ItemEntity.PriceIsNegative field instance</summary>
		public static EntityField PriceIsNegative
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.PriceIsNegative);}
		}
		/// <summary>Creates a new ItemEntity.IsPromoItem field instance</summary>
		public static EntityField IsPromoItem
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsPromoItem);}
		}
		/// <summary>Creates a new ItemEntity.BergPlu field instance</summary>
		public static EntityField BergPlu
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.BergPlu);}
		}
		/// <summary>Creates a new ItemEntity.ModifierFollowsParent field instance</summary>
		public static EntityField ModifierFollowsParent
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModifierFollowsParent);}
		}
		/// <summary>Creates a new ItemEntity.ModifierDescription field instance</summary>
		public static EntityField ModifierDescription
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModifierDescription);}
		}
		/// <summary>Creates a new ItemEntity.UseModifierMaxSelect field instance</summary>
		public static EntityField UseModifierMaxSelect
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.UseModifierMaxSelect);}
		}
		/// <summary>Creates a new ItemEntity.ModifierMaxSelect field instance</summary>
		public static EntityField ModifierMaxSelect
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModifierMaxSelect);}
		}
		/// <summary>Creates a new ItemEntity.UseModifierMinSelect field instance</summary>
		public static EntityField UseModifierMinSelect
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.UseModifierMinSelect);}
		}
		/// <summary>Creates a new ItemEntity.ModifierMinSelect field instance</summary>
		public static EntityField ModifierMinSelect
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModifierMinSelect);}
		}
		/// <summary>Creates a new ItemEntity.AllowModifierMaxBypass field instance</summary>
		public static EntityField AllowModifierMaxBypass
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.AllowModifierMaxBypass);}
		}
		/// <summary>Creates a new ItemEntity.UsePizzaStyle field instance</summary>
		public static EntityField UsePizzaStyle
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.UsePizzaStyle);}
		}
		/// <summary>Creates a new ItemEntity.IsTimedItem field instance</summary>
		public static EntityField IsTimedItem
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsTimedItem);}
		}
		/// <summary>Creates a new ItemEntity.TimingIncrement field instance</summary>
		public static EntityField TimingIncrement
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.TimingIncrement);}
		}
		/// <summary>Creates a new ItemEntity.MinimumPrice field instance</summary>
		public static EntityField MinimumPrice
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.MinimumPrice);}
		}
		/// <summary>Creates a new ItemEntity.Vducolor field instance</summary>
		public static EntityField Vducolor
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.Vducolor);}
		}
		/// <summary>Creates a new ItemEntity.ShortDescription field instance</summary>
		public static EntityField ShortDescription
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ShortDescription);}
		}
		/// <summary>Creates a new ItemEntity.DefaultCourse field instance</summary>
		public static EntityField DefaultCourse
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.DefaultCourse);}
		}
		/// <summary>Creates a new ItemEntity.ChineseOutput field instance</summary>
		public static EntityField ChineseOutput
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ChineseOutput);}
		}
		/// <summary>Creates a new ItemEntity.TripleHigh field instance</summary>
		public static EntityField TripleHigh
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.TripleHigh);}
		}
		/// <summary>Creates a new ItemEntity.QuadHigh field instance</summary>
		public static EntityField QuadHigh
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.QuadHigh);}
		}
		/// <summary>Creates a new ItemEntity.DiscountFlags field instance</summary>
		public static EntityField DiscountFlags
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.DiscountFlags);}
		}
		/// <summary>Creates a new ItemEntity.TaxFlags field instance</summary>
		public static EntityField TaxFlags
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.TaxFlags);}
		}
		/// <summary>Creates a new ItemEntity.PrintOptions field instance</summary>
		public static EntityField PrintOptions
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.PrintOptions);}
		}
		/// <summary>Creates a new ItemEntity.RemotePrinters field instance</summary>
		public static EntityField RemotePrinters
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.RemotePrinters);}
		}
		/// <summary>Creates a new ItemEntity.AllowZeroPrice field instance</summary>
		public static EntityField AllowZeroPrice
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.AllowZeroPrice);}
		}
		/// <summary>Creates a new ItemEntity.ModifierPriceLevel field instance</summary>
		public static EntityField ModifierPriceLevel
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModifierPriceLevel);}
		}
		/// <summary>Creates a new ItemEntity.ModifierCount field instance</summary>
		public static EntityField ModifierCount
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModifierCount);}
		}
		/// <summary>Creates a new ItemEntity.IngredientCount field instance</summary>
		public static EntityField IngredientCount
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IngredientCount);}
		}
		/// <summary>Creates a new ItemEntity.PrepTime field instance</summary>
		public static EntityField PrepTime
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.PrepTime);}
		}
		/// <summary>Creates a new ItemEntity.MultModPrice field instance</summary>
		public static EntityField MultModPrice
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.MultModPrice);}
		}
		/// <summary>Creates a new ItemEntity.MultAmount field instance</summary>
		public static EntityField MultAmount
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.MultAmount);}
		}
		/// <summary>Creates a new ItemEntity.MultRound field instance</summary>
		public static EntityField MultRound
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.MultRound);}
		}
		/// <summary>Creates a new ItemEntity.IsNotGratable field instance</summary>
		public static EntityField IsNotGratable
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsNotGratable);}
		}
		/// <summary>Creates a new ItemEntity.ModifierSortType field instance</summary>
		public static EntityField ModifierSortType
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModifierSortType);}
		}
		/// <summary>Creates a new ItemEntity.IsUsedOnline field instance</summary>
		public static EntityField IsUsedOnline
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsUsedOnline);}
		}
		/// <summary>Creates a new ItemEntity.WebTitle field instance</summary>
		public static EntityField WebTitle
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.WebTitle);}
		}
		/// <summary>Creates a new ItemEntity.WebDescription field instance</summary>
		public static EntityField WebDescription
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.WebDescription);}
		}
		/// <summary>Creates a new ItemEntity.WebThumbPath field instance</summary>
		public static EntityField WebThumbPath
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.WebThumbPath);}
		}
		/// <summary>Creates a new ItemEntity.WebImagePath field instance</summary>
		public static EntityField WebImagePath
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.WebImagePath);}
		}
		/// <summary>Creates a new ItemEntity.DisplayIndex field instance</summary>
		public static EntityField DisplayIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.DisplayIndex);}
		}
		/// <summary>Creates a new ItemEntity.ModDisplayOrder field instance</summary>
		public static EntityField ModDisplayOrder
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModDisplayOrder);}
		}
		/// <summary>Creates a new ItemEntity.OverridePrice field instance</summary>
		public static EntityField OverridePrice
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.OverridePrice);}
		}
		/// <summary>Creates a new ItemEntity.SwappedDept field instance</summary>
		public static EntityField SwappedDept
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.SwappedDept);}
		}
		/// <summary>Creates a new ItemEntity.MaxSelectionAllowed field instance</summary>
		public static EntityField MaxSelectionAllowed
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.MaxSelectionAllowed);}
		}
		/// <summary>Creates a new ItemEntity.IsShippable field instance</summary>
		public static EntityField IsShippable
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsShippable);}
		}
		/// <summary>Creates a new ItemEntity.ShipWidth field instance</summary>
		public static EntityField ShipWidth
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ShipWidth);}
		}
		/// <summary>Creates a new ItemEntity.ShipHeight field instance</summary>
		public static EntityField ShipHeight
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ShipHeight);}
		}
		/// <summary>Creates a new ItemEntity.ShipLength field instance</summary>
		public static EntityField ShipLength
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ShipLength);}
		}
		/// <summary>Creates a new ItemEntity.AddPrepTimeToParent field instance</summary>
		public static EntityField AddPrepTimeToParent
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.AddPrepTimeToParent);}
		}
		/// <summary>Creates a new ItemEntity.Vduid field instance</summary>
		public static EntityField Vduid
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.Vduid);}
		}
		/// <summary>Creates a new ItemEntity.HhcolumnCount field instance</summary>
		public static EntityField HhcolumnCount
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.HhcolumnCount);}
		}
		/// <summary>Creates a new ItemEntity.WebDepartmentId field instance</summary>
		public static EntityField WebDepartmentId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.WebDepartmentId);}
		}
		/// <summary>Creates a new ItemEntity.AskId field instance</summary>
		public static EntityField AskId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.AskId);}
		}
		/// <summary>Creates a new ItemEntity.QuantityMultiplier field instance</summary>
		public static EntityField QuantityMultiplier
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.QuantityMultiplier);}
		}
		/// <summary>Creates a new ItemEntity.IsModifierGroup field instance</summary>
		public static EntityField IsModifierGroup
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.IsModifierGroup);}
		}
		/// <summary>Creates a new ItemEntity.ModifierPriceRounding field instance</summary>
		public static EntityField ModifierPriceRounding
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemFieldIndex.ModifierPriceRounding);}
		}
	}

	/// <summary>Field Creation Class for entity ItemDayEntity</summary>
	public partial class ItemDayFields
	{
		/// <summary>Creates a new ItemDayEntity.ItemDayId field instance</summary>
		public static EntityField ItemDayId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemDayFieldIndex.ItemDayId);}
		}
		/// <summary>Creates a new ItemDayEntity.ItemId field instance</summary>
		public static EntityField ItemId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemDayFieldIndex.ItemId);}
		}
		/// <summary>Creates a new ItemDayEntity.DepartmentId field instance</summary>
		public static EntityField DepartmentId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemDayFieldIndex.DepartmentId);}
		}
		/// <summary>Creates a new ItemDayEntity.Date field instance</summary>
		public static EntityField Date
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemDayFieldIndex.Date);}
		}
	}

	/// <summary>Field Creation Class for entity ItemIngredientEntity</summary>
	public partial class ItemIngredientFields
	{
		/// <summary>Creates a new ItemIngredientEntity.ItemIngredientId field instance</summary>
		public static EntityField ItemIngredientId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemIngredientFieldIndex.ItemIngredientId);}
		}
		/// <summary>Creates a new ItemIngredientEntity.ItemId field instance</summary>
		public static EntityField ItemId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemIngredientFieldIndex.ItemId);}
		}
		/// <summary>Creates a new ItemIngredientEntity.IngredientName field instance</summary>
		public static EntityField IngredientName
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemIngredientFieldIndex.IngredientName);}
		}
		/// <summary>Creates a new ItemIngredientEntity.IngredientIndex field instance</summary>
		public static EntityField IngredientIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemIngredientFieldIndex.IngredientIndex);}
		}
		/// <summary>Creates a new ItemIngredientEntity.UnitCount field instance</summary>
		public static EntityField UnitCount
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemIngredientFieldIndex.UnitCount);}
		}
	}

	/// <summary>Field Creation Class for entity ItemModifierEntity</summary>
	public partial class ItemModifierFields
	{
		/// <summary>Creates a new ItemModifierEntity.ItemModifierId field instance</summary>
		public static EntityField ItemModifierId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemModifierFieldIndex.ItemModifierId);}
		}
		/// <summary>Creates a new ItemModifierEntity.ItemId field instance</summary>
		public static EntityField ItemId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemModifierFieldIndex.ItemId);}
		}
		/// <summary>Creates a new ItemModifierEntity.ModifierIndex field instance</summary>
		public static EntityField ModifierIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemModifierFieldIndex.ModifierIndex);}
		}
		/// <summary>Creates a new ItemModifierEntity.ModifierName field instance</summary>
		public static EntityField ModifierName
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemModifierFieldIndex.ModifierName);}
		}
		/// <summary>Creates a new ItemModifierEntity.IsSelected field instance</summary>
		public static EntityField IsSelected
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemModifierFieldIndex.IsSelected);}
		}
	}

	/// <summary>Field Creation Class for entity ItemPriceEntity</summary>
	public partial class ItemPriceFields
	{
		/// <summary>Creates a new ItemPriceEntity.ItemPriceId field instance</summary>
		public static EntityField ItemPriceId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.ItemPriceId);}
		}
		/// <summary>Creates a new ItemPriceEntity.ItemId field instance</summary>
		public static EntityField ItemId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.ItemId);}
		}
		/// <summary>Creates a new ItemPriceEntity.ScheduleIndex field instance</summary>
		public static EntityField ScheduleIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.ScheduleIndex);}
		}
		/// <summary>Creates a new ItemPriceEntity.DefaultPrice field instance</summary>
		public static EntityField DefaultPrice
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.DefaultPrice);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level1Price field instance</summary>
		public static EntityField Level1Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level1Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level2Price field instance</summary>
		public static EntityField Level2Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level2Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level3Price field instance</summary>
		public static EntityField Level3Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level3Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level4Price field instance</summary>
		public static EntityField Level4Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level4Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level5Price field instance</summary>
		public static EntityField Level5Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level5Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level6Price field instance</summary>
		public static EntityField Level6Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level6Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level7Price field instance</summary>
		public static EntityField Level7Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level7Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level8Price field instance</summary>
		public static EntityField Level8Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level8Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level9Price field instance</summary>
		public static EntityField Level9Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level9Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level10Price field instance</summary>
		public static EntityField Level10Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level10Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level11Price field instance</summary>
		public static EntityField Level11Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level11Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level12Price field instance</summary>
		public static EntityField Level12Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level12Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level13Price field instance</summary>
		public static EntityField Level13Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level13Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level14Price field instance</summary>
		public static EntityField Level14Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level14Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level15Price field instance</summary>
		public static EntityField Level15Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level15Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level16Price field instance</summary>
		public static EntityField Level16Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level16Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level17Price field instance</summary>
		public static EntityField Level17Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level17Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level18Price field instance</summary>
		public static EntityField Level18Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level18Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level19Price field instance</summary>
		public static EntityField Level19Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level19Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level20Price field instance</summary>
		public static EntityField Level20Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level20Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level21Price field instance</summary>
		public static EntityField Level21Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level21Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level22Price field instance</summary>
		public static EntityField Level22Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level22Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level23Price field instance</summary>
		public static EntityField Level23Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level23Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level24Price field instance</summary>
		public static EntityField Level24Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level24Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level25Price field instance</summary>
		public static EntityField Level25Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level25Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level26Price field instance</summary>
		public static EntityField Level26Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level26Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level27Price field instance</summary>
		public static EntityField Level27Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level27Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level28Price field instance</summary>
		public static EntityField Level28Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level28Price);}
		}
		/// <summary>Creates a new ItemPriceEntity.Level29Price field instance</summary>
		public static EntityField Level29Price
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPriceFieldIndex.Level29Price);}
		}
	}

	/// <summary>Field Creation Class for entity ItemPrinterEntity</summary>
	public partial class ItemPrinterFields
	{
		/// <summary>Creates a new ItemPrinterEntity.ItemPrinterId field instance</summary>
		public static EntityField ItemPrinterId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPrinterFieldIndex.ItemPrinterId);}
		}
		/// <summary>Creates a new ItemPrinterEntity.ItemId field instance</summary>
		public static EntityField ItemId
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPrinterFieldIndex.ItemId);}
		}
		/// <summary>Creates a new ItemPrinterEntity.PrinterIndex field instance</summary>
		public static EntityField PrinterIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPrinterFieldIndex.PrinterIndex);}
		}
		/// <summary>Creates a new ItemPrinterEntity.ReceiptDescription field instance</summary>
		public static EntityField ReceiptDescription
		{
			get { return (EntityField)EntityFieldFactory.Create(ItemPrinterFieldIndex.ReceiptDescription);}
		}
	}

	/// <summary>Field Creation Class for entity LayoutRoomEntity</summary>
	public partial class LayoutRoomFields
	{
		/// <summary>Creates a new LayoutRoomEntity.LayoutRoomId field instance</summary>
		public static EntityField LayoutRoomId
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutRoomFieldIndex.LayoutRoomId);}
		}
		/// <summary>Creates a new LayoutRoomEntity.LayoutId field instance</summary>
		public static EntityField LayoutId
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutRoomFieldIndex.LayoutId);}
		}
		/// <summary>Creates a new LayoutRoomEntity.RoomIndex field instance</summary>
		public static EntityField RoomIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutRoomFieldIndex.RoomIndex);}
		}
		/// <summary>Creates a new LayoutRoomEntity.RoomName field instance</summary>
		public static EntityField RoomName
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutRoomFieldIndex.RoomName);}
		}
		/// <summary>Creates a new LayoutRoomEntity.ImageFile field instance</summary>
		public static EntityField ImageFile
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutRoomFieldIndex.ImageFile);}
		}
		/// <summary>Creates a new LayoutRoomEntity.PriceLevel field instance</summary>
		public static EntityField PriceLevel
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutRoomFieldIndex.PriceLevel);}
		}
	}

	/// <summary>Field Creation Class for entity LayoutTableEntity</summary>
	public partial class LayoutTableFields
	{
		/// <summary>Creates a new LayoutTableEntity.LayoutTableId field instance</summary>
		public static EntityField LayoutTableId
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.LayoutTableId);}
		}
		/// <summary>Creates a new LayoutTableEntity.LayoutId field instance</summary>
		public static EntityField LayoutId
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.LayoutId);}
		}
		/// <summary>Creates a new LayoutTableEntity.TableIndex field instance</summary>
		public static EntityField TableIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.TableIndex);}
		}
		/// <summary>Creates a new LayoutTableEntity.TableName field instance</summary>
		public static EntityField TableName
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.TableName);}
		}
		/// <summary>Creates a new LayoutTableEntity.ShapeType field instance</summary>
		public static EntityField ShapeType
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.ShapeType);}
		}
		/// <summary>Creates a new LayoutTableEntity.X field instance</summary>
		public static EntityField X
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.X);}
		}
		/// <summary>Creates a new LayoutTableEntity.Y field instance</summary>
		public static EntityField Y
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.Y);}
		}
		/// <summary>Creates a new LayoutTableEntity.SeatCount field instance</summary>
		public static EntityField SeatCount
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.SeatCount);}
		}
		/// <summary>Creates a new LayoutTableEntity.SectionIndex field instance</summary>
		public static EntityField SectionIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.SectionIndex);}
		}
		/// <summary>Creates a new LayoutTableEntity.IsNonSmoking field instance</summary>
		public static EntityField IsNonSmoking
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.IsNonSmoking);}
		}
		/// <summary>Creates a new LayoutTableEntity.JoinTableIndex field instance</summary>
		public static EntityField JoinTableIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.JoinTableIndex);}
		}
		/// <summary>Creates a new LayoutTableEntity.IsX10Enabled field instance</summary>
		public static EntityField IsX10Enabled
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.IsX10Enabled);}
		}
		/// <summary>Creates a new LayoutTableEntity.X10HouseCode field instance</summary>
		public static EntityField X10HouseCode
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.X10HouseCode);}
		}
		/// <summary>Creates a new LayoutTableEntity.X10DeviceCode field instance</summary>
		public static EntityField X10DeviceCode
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.X10DeviceCode);}
		}
		/// <summary>Creates a new LayoutTableEntity.RoomIndex field instance</summary>
		public static EntityField RoomIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(LayoutTableFieldIndex.RoomIndex);}
		}
	}

	/// <summary>Field Creation Class for entity PriceLevelEntity</summary>
	public partial class PriceLevelFields
	{
		/// <summary>Creates a new PriceLevelEntity.PriceLevelId field instance</summary>
		public static EntityField PriceLevelId
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceLevelFieldIndex.PriceLevelId);}
		}
		/// <summary>Creates a new PriceLevelEntity.StoreId field instance</summary>
		public static EntityField StoreId
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceLevelFieldIndex.StoreId);}
		}
		/// <summary>Creates a new PriceLevelEntity.PriceLevelIndex field instance</summary>
		public static EntityField PriceLevelIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceLevelFieldIndex.PriceLevelIndex);}
		}
		/// <summary>Creates a new PriceLevelEntity.PriceLevelName field instance</summary>
		public static EntityField PriceLevelName
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceLevelFieldIndex.PriceLevelName);}
		}
	}

	/// <summary>Field Creation Class for entity PriceScheduleEntity</summary>
	public partial class PriceScheduleFields
	{
		/// <summary>Creates a new PriceScheduleEntity.PriceScheduleId field instance</summary>
		public static EntityField PriceScheduleId
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.PriceScheduleId);}
		}
		/// <summary>Creates a new PriceScheduleEntity.StoreId field instance</summary>
		public static EntityField StoreId
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.StoreId);}
		}
		/// <summary>Creates a new PriceScheduleEntity.PriceScheduleIndex field instance</summary>
		public static EntityField PriceScheduleIndex
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.PriceScheduleIndex);}
		}
		/// <summary>Creates a new PriceScheduleEntity.PriceScheduleName field instance</summary>
		public static EntityField PriceScheduleName
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.PriceScheduleName);}
		}
		/// <summary>Creates a new PriceScheduleEntity.ScheduleDays field instance</summary>
		public static EntityField ScheduleDays
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.ScheduleDays);}
		}
		/// <summary>Creates a new PriceScheduleEntity.StartTime field instance</summary>
		public static EntityField StartTime
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.StartTime);}
		}
		/// <summary>Creates a new PriceScheduleEntity.EndTime field instance</summary>
		public static EntityField EndTime
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.EndTime);}
		}
		/// <summary>Creates a new PriceScheduleEntity.IsSeasonal field instance</summary>
		public static EntityField IsSeasonal
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.IsSeasonal);}
		}
		/// <summary>Creates a new PriceScheduleEntity.SeasonalStartMonth field instance</summary>
		public static EntityField SeasonalStartMonth
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.SeasonalStartMonth);}
		}
		/// <summary>Creates a new PriceScheduleEntity.SeasonalStartDay field instance</summary>
		public static EntityField SeasonalStartDay
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.SeasonalStartDay);}
		}
		/// <summary>Creates a new PriceScheduleEntity.SeasonalEndMonth field instance</summary>
		public static EntityField SeasonalEndMonth
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.SeasonalEndMonth);}
		}
		/// <summary>Creates a new PriceScheduleEntity.SeasonalEndDay field instance</summary>
		public static EntityField SeasonalEndDay
		{
			get { return (EntityField)EntityFieldFactory.Create(PriceScheduleFieldIndex.SeasonalEndDay);}
		}
	}

	/// <summary>Field Creation Class for entity VendorEntity</summary>
	public partial class VendorFields
	{
		/// <summary>Creates a new VendorEntity.VendorId field instance</summary>
		public static EntityField VendorId
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.VendorId);}
		}
		/// <summary>Creates a new VendorEntity.StoreId field instance</summary>
		public static EntityField StoreId
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.StoreId);}
		}
		/// <summary>Creates a new VendorEntity.VendorName field instance</summary>
		public static EntityField VendorName
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.VendorName);}
		}
		/// <summary>Creates a new VendorEntity.Phone field instance</summary>
		public static EntityField Phone
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.Phone);}
		}
		/// <summary>Creates a new VendorEntity.Fax field instance</summary>
		public static EntityField Fax
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.Fax);}
		}
		/// <summary>Creates a new VendorEntity.Address field instance</summary>
		public static EntityField Address
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.Address);}
		}
		/// <summary>Creates a new VendorEntity.City field instance</summary>
		public static EntityField City
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.City);}
		}
		/// <summary>Creates a new VendorEntity.State field instance</summary>
		public static EntityField State
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.State);}
		}
		/// <summary>Creates a new VendorEntity.Zip field instance</summary>
		public static EntityField Zip
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.Zip);}
		}
		/// <summary>Creates a new VendorEntity.Notes field instance</summary>
		public static EntityField Notes
		{
			get { return (EntityField)EntityFieldFactory.Create(VendorFieldIndex.Notes);}
		}
	}

	/// <summary>Field Creation Class for entity VersionEntity</summary>
	public partial class VersionFields
	{
		/// <summary>Creates a new VersionEntity.VersionId field instance</summary>
		public static EntityField VersionId
		{
			get { return (EntityField)EntityFieldFactory.Create(VersionFieldIndex.VersionId);}
		}
		/// <summary>Creates a new VersionEntity.StoreId field instance</summary>
		public static EntityField StoreId
		{
			get { return (EntityField)EntityFieldFactory.Create(VersionFieldIndex.StoreId);}
		}
		/// <summary>Creates a new VersionEntity.CodeVersion field instance</summary>
		public static EntityField CodeVersion
		{
			get { return (EntityField)EntityFieldFactory.Create(VersionFieldIndex.CodeVersion);}
		}
		/// <summary>Creates a new VersionEntity.DatabaseVersion field instance</summary>
		public static EntityField DatabaseVersion
		{
			get { return (EntityField)EntityFieldFactory.Create(VersionFieldIndex.DatabaseVersion);}
		}
	}
	

}