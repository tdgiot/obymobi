﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.HelperClasses
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(13);
			InitDepartmentEntityMappings();
			InitItemEntityMappings();
			InitItemDayEntityMappings();
			InitItemIngredientEntityMappings();
			InitItemModifierEntityMappings();
			InitItemPriceEntityMappings();
			InitItemPrinterEntityMappings();
			InitLayoutRoomEntityMappings();
			InitLayoutTableEntityMappings();
			InitPriceLevelEntityMappings();
			InitPriceScheduleEntityMappings();
			InitVendorEntityMappings();
			InitVersionEntityMappings();
		}

		/// <summary>Inits DepartmentEntity's mappings</summary>
		private void InitDepartmentEntityMappings()
		{
			this.AddElementMapping("DepartmentEntity", @"Tissl", @"dbo", "Department", 19, 0);
			this.AddElementFieldMapping("DepartmentEntity", "DepartmentId", "DepartmentID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("DepartmentEntity", "RegionId", "RegionID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("DepartmentEntity", "DepartmentName", "DepartmentName", false, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("DepartmentEntity", "DepartmentDescription", "DepartmentDescription", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("DepartmentEntity", "GroupName", "GroupName", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("DepartmentEntity", "IsHash", "IsHash", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("DepartmentEntity", "PmsbucketNumber", "PMSBucketNumber", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 6);
			this.AddElementFieldMapping("DepartmentEntity", "IsUsedOnline", "IsUsedOnline", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("DepartmentEntity", "WebTitle", "WebTitle", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("DepartmentEntity", "WebDescription", "WebDescription", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("DepartmentEntity", "WebThumbPath", "WebThumbPath", true, "NVarChar", 254, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("DepartmentEntity", "WebImagePath", "WebImagePath", true, "NVarChar", 254, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("DepartmentEntity", "IsDefault", "IsDefault", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("DepartmentEntity", "DisplayIndex", "DisplayIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("DepartmentEntity", "IsParent", "IsParent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("DepartmentEntity", "ParentDepartmentId", "ParentDepartmentID", true, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 15);
			this.AddElementFieldMapping("DepartmentEntity", "Vduid", "VDUID", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("DepartmentEntity", "AskId", "AskID", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("DepartmentEntity", "QuantityMultiplier", "QuantityMultiplier", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
		}

		/// <summary>Inits ItemEntity's mappings</summary>
		private void InitItemEntityMappings()
		{
			this.AddElementMapping("ItemEntity", @"Tissl", @"dbo", "Item", 76, 0);
			this.AddElementFieldMapping("ItemEntity", "ItemId", "ItemID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("ItemEntity", "RegionId", "RegionID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("ItemEntity", "ItemName", "ItemName", false, "NVarChar", 32, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ItemEntity", "ItemDescription", "ItemDescription", false, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ItemEntity", "Department", "Department", false, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ItemEntity", "Upc", "UPC", false, "NVarChar", 32, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ItemEntity", "ReceiptDesc", "ReceiptDesc", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ItemEntity", "PrintDoubleWide", "PrintDoubleWide", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ItemEntity", "PrintAltColor", "PrintAltColor", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("ItemEntity", "ItemCount", "ItemCount", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 9);
			this.AddElementFieldMapping("ItemEntity", "AlternateItem", "AlternateItem", false, "NVarChar", 32, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ItemEntity", "FireDelay", "FireDelay", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 11);
			this.AddElementFieldMapping("ItemEntity", "IsModifier", "IsModifier", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("ItemEntity", "OrderPriority", "OrderPriority", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 13);
			this.AddElementFieldMapping("ItemEntity", "IsStoreChargeable", "IsStoreChargeable", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("ItemEntity", "AskForPrice", "AskForPrice", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("ItemEntity", "TogoSurcharge", "TogoSurcharge", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("ItemEntity", "ItemCost", "ItemCost", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("ItemEntity", "IsScaleable", "IsScaleable", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("ItemEntity", "IsNotTippable", "IsNotTippable", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("ItemEntity", "PriceIsNegative", "PriceIsNegative", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 20);
			this.AddElementFieldMapping("ItemEntity", "IsPromoItem", "IsPromoItem", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("ItemEntity", "BergPlu", "BergPLU", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("ItemEntity", "ModifierFollowsParent", "ModifierFollowsParent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("ItemEntity", "ModifierDescription", "ModifierDescription", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("ItemEntity", "UseModifierMaxSelect", "UseModifierMaxSelect", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("ItemEntity", "ModifierMaxSelect", "ModifierMaxSelect", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 26);
			this.AddElementFieldMapping("ItemEntity", "UseModifierMinSelect", "UseModifierMinSelect", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("ItemEntity", "ModifierMinSelect", "ModifierMinSelect", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 28);
			this.AddElementFieldMapping("ItemEntity", "AllowModifierMaxBypass", "AllowModifierMaxBypass", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("ItemEntity", "UsePizzaStyle", "UsePizzaStyle", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 30);
			this.AddElementFieldMapping("ItemEntity", "IsTimedItem", "IsTimedItem", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 31);
			this.AddElementFieldMapping("ItemEntity", "TimingIncrement", "TimingIncrement", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 32);
			this.AddElementFieldMapping("ItemEntity", "MinimumPrice", "MinimumPrice", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("ItemEntity", "Vducolor", "VDUColor", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 34);
			this.AddElementFieldMapping("ItemEntity", "ShortDescription", "ShortDescription", true, "NVarChar", 5, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("ItemEntity", "DefaultCourse", "DefaultCourse", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 36);
			this.AddElementFieldMapping("ItemEntity", "ChineseOutput", "ChineseOutput", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("ItemEntity", "TripleHigh", "TripleHigh", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 38);
			this.AddElementFieldMapping("ItemEntity", "QuadHigh", "QuadHigh", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 39);
			this.AddElementFieldMapping("ItemEntity", "DiscountFlags", "DiscountFlags", false, "Binary", 13, 0, 0, false, "", null, typeof(System.Byte[]), 40);
			this.AddElementFieldMapping("ItemEntity", "TaxFlags", "TaxFlags", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 41);
			this.AddElementFieldMapping("ItemEntity", "PrintOptions", "PrintOptions", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 42);
			this.AddElementFieldMapping("ItemEntity", "RemotePrinters", "RemotePrinters", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 43);
			this.AddElementFieldMapping("ItemEntity", "AllowZeroPrice", "AllowZeroPrice", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 44);
			this.AddElementFieldMapping("ItemEntity", "ModifierPriceLevel", "ModifierPriceLevel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 45);
			this.AddElementFieldMapping("ItemEntity", "ModifierCount", "ModifierCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("ItemEntity", "IngredientCount", "IngredientCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
			this.AddElementFieldMapping("ItemEntity", "PrepTime", "PrepTime", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 48);
			this.AddElementFieldMapping("ItemEntity", "MultModPrice", "MultModPrice", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 49);
			this.AddElementFieldMapping("ItemEntity", "MultAmount", "MultAmount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 50);
			this.AddElementFieldMapping("ItemEntity", "MultRound", "MultRound", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 51);
			this.AddElementFieldMapping("ItemEntity", "IsNotGratable", "IsNotGratable", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 52);
			this.AddElementFieldMapping("ItemEntity", "ModifierSortType", "ModifierSortType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 53);
			this.AddElementFieldMapping("ItemEntity", "IsUsedOnline", "IsUsedOnline", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 54);
			this.AddElementFieldMapping("ItemEntity", "WebTitle", "WebTitle", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 55);
			this.AddElementFieldMapping("ItemEntity", "WebDescription", "WebDescription", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 56);
			this.AddElementFieldMapping("ItemEntity", "WebThumbPath", "WebThumbPath", true, "NVarChar", 254, 0, 0, false, "", null, typeof(System.String), 57);
			this.AddElementFieldMapping("ItemEntity", "WebImagePath", "WebImagePath", true, "NVarChar", 254, 0, 0, false, "", null, typeof(System.String), 58);
			this.AddElementFieldMapping("ItemEntity", "DisplayIndex", "DisplayIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 59);
			this.AddElementFieldMapping("ItemEntity", "ModDisplayOrder", "ModDisplayOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 60);
			this.AddElementFieldMapping("ItemEntity", "OverridePrice", "OverridePrice", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 61);
			this.AddElementFieldMapping("ItemEntity", "SwappedDept", "SwappedDept", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 62);
			this.AddElementFieldMapping("ItemEntity", "MaxSelectionAllowed", "MaxSelectionAllowed", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 63);
			this.AddElementFieldMapping("ItemEntity", "IsShippable", "IsShippable", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 64);
			this.AddElementFieldMapping("ItemEntity", "ShipWidth", "ShipWidth", true, "NVarChar", 5, 0, 0, false, "", null, typeof(System.String), 65);
			this.AddElementFieldMapping("ItemEntity", "ShipHeight", "ShipHeight", true, "NVarChar", 5, 0, 0, false, "", null, typeof(System.String), 66);
			this.AddElementFieldMapping("ItemEntity", "ShipLength", "ShipLength", true, "NVarChar", 5, 0, 0, false, "", null, typeof(System.String), 67);
			this.AddElementFieldMapping("ItemEntity", "AddPrepTimeToParent", "AddPrepTimeToParent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 68);
			this.AddElementFieldMapping("ItemEntity", "Vduid", "VDUID", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 69);
			this.AddElementFieldMapping("ItemEntity", "HhcolumnCount", "HHColumnCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 70);
			this.AddElementFieldMapping("ItemEntity", "WebDepartmentId", "WebDepartmentID", true, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 71);
			this.AddElementFieldMapping("ItemEntity", "AskId", "AskID", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 72);
			this.AddElementFieldMapping("ItemEntity", "QuantityMultiplier", "QuantityMultiplier", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 73);
			this.AddElementFieldMapping("ItemEntity", "IsModifierGroup", "IsModifierGroup", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 74);
			this.AddElementFieldMapping("ItemEntity", "ModifierPriceRounding", "ModifierPriceRounding", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 75);
		}

		/// <summary>Inits ItemDayEntity's mappings</summary>
		private void InitItemDayEntityMappings()
		{
			this.AddElementMapping("ItemDayEntity", @"Tissl", @"dbo", "ItemDay", 4, 0);
			this.AddElementFieldMapping("ItemDayEntity", "ItemDayId", "ItemDayID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("ItemDayEntity", "ItemId", "ItemID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("ItemDayEntity", "DepartmentId", "DepartmentID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 2);
			this.AddElementFieldMapping("ItemDayEntity", "Date", "Date", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
		}

		/// <summary>Inits ItemIngredientEntity's mappings</summary>
		private void InitItemIngredientEntityMappings()
		{
			this.AddElementMapping("ItemIngredientEntity", @"Tissl", @"dbo", "ItemIngredient", 5, 0);
			this.AddElementFieldMapping("ItemIngredientEntity", "ItemIngredientId", "ItemIngredientID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("ItemIngredientEntity", "ItemId", "ItemID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("ItemIngredientEntity", "IngredientName", "IngredientName", false, "NVarChar", 33, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ItemIngredientEntity", "IngredientIndex", "IngredientIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ItemIngredientEntity", "UnitCount", "UnitCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits ItemModifierEntity's mappings</summary>
		private void InitItemModifierEntityMappings()
		{
			this.AddElementMapping("ItemModifierEntity", @"Tissl", @"dbo", "ItemModifier", 5, 0);
			this.AddElementFieldMapping("ItemModifierEntity", "ItemModifierId", "ItemModifierID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("ItemModifierEntity", "ItemId", "ItemID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("ItemModifierEntity", "ModifierIndex", "ModifierIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ItemModifierEntity", "ModifierName", "ModifierName", false, "NVarChar", 33, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ItemModifierEntity", "IsSelected", "IsSelected", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
		}

		/// <summary>Inits ItemPriceEntity's mappings</summary>
		private void InitItemPriceEntityMappings()
		{
			this.AddElementMapping("ItemPriceEntity", @"Tissl", @"dbo", "ItemPrice", 33, 0);
			this.AddElementFieldMapping("ItemPriceEntity", "ItemPriceId", "ItemPriceID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("ItemPriceEntity", "ItemId", "ItemID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("ItemPriceEntity", "ScheduleIndex", "ScheduleIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ItemPriceEntity", "DefaultPrice", "DefaultPrice", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ItemPriceEntity", "Level1Price", "Level1Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ItemPriceEntity", "Level2Price", "Level2Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ItemPriceEntity", "Level3Price", "Level3Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ItemPriceEntity", "Level4Price", "Level4Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ItemPriceEntity", "Level5Price", "Level5Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ItemPriceEntity", "Level6Price", "Level6Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ItemPriceEntity", "Level7Price", "Level7Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ItemPriceEntity", "Level8Price", "Level8Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ItemPriceEntity", "Level9Price", "Level9Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ItemPriceEntity", "Level10Price", "Level10Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ItemPriceEntity", "Level11Price", "Level11Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ItemPriceEntity", "Level12Price", "Level12Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ItemPriceEntity", "Level13Price", "Level13Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("ItemPriceEntity", "Level14Price", "Level14Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("ItemPriceEntity", "Level15Price", "Level15Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("ItemPriceEntity", "Level16Price", "Level16Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("ItemPriceEntity", "Level17Price", "Level17Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("ItemPriceEntity", "Level18Price", "Level18Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("ItemPriceEntity", "Level19Price", "Level19Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("ItemPriceEntity", "Level20Price", "Level20Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("ItemPriceEntity", "Level21Price", "Level21Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("ItemPriceEntity", "Level22Price", "Level22Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("ItemPriceEntity", "Level23Price", "Level23Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("ItemPriceEntity", "Level24Price", "Level24Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("ItemPriceEntity", "Level25Price", "Level25Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("ItemPriceEntity", "Level26Price", "Level26Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("ItemPriceEntity", "Level27Price", "Level27Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("ItemPriceEntity", "Level28Price", "Level28Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("ItemPriceEntity", "Level29Price", "Level29Price", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
		}

		/// <summary>Inits ItemPrinterEntity's mappings</summary>
		private void InitItemPrinterEntityMappings()
		{
			this.AddElementMapping("ItemPrinterEntity", @"Tissl", @"dbo", "ItemPrinter", 4, 0);
			this.AddElementFieldMapping("ItemPrinterEntity", "ItemPrinterId", "ItemPrinterID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("ItemPrinterEntity", "ItemId", "ItemID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("ItemPrinterEntity", "PrinterIndex", "PrinterIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ItemPrinterEntity", "ReceiptDescription", "ReceiptDescription", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits LayoutRoomEntity's mappings</summary>
		private void InitLayoutRoomEntityMappings()
		{
			this.AddElementMapping("LayoutRoomEntity", @"Tissl", @"dbo", "LayoutRoom", 6, 0);
			this.AddElementFieldMapping("LayoutRoomEntity", "LayoutRoomId", "LayoutRoomID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("LayoutRoomEntity", "LayoutId", "LayoutID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("LayoutRoomEntity", "RoomIndex", "RoomIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("LayoutRoomEntity", "RoomName", "RoomName", false, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("LayoutRoomEntity", "ImageFile", "ImageFile", false, "NVarChar", 128, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("LayoutRoomEntity", "PriceLevel", "PriceLevel", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 5);
		}

		/// <summary>Inits LayoutTableEntity's mappings</summary>
		private void InitLayoutTableEntityMappings()
		{
			this.AddElementMapping("LayoutTableEntity", @"Tissl", @"dbo", "LayoutTable", 15, 0);
			this.AddElementFieldMapping("LayoutTableEntity", "LayoutTableId", "LayoutTableID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("LayoutTableEntity", "LayoutId", "LayoutID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 1);
			this.AddElementFieldMapping("LayoutTableEntity", "TableIndex", "TableIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("LayoutTableEntity", "TableName", "TableName", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("LayoutTableEntity", "ShapeType", "ShapeType", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 4);
			this.AddElementFieldMapping("LayoutTableEntity", "X", "X", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 5);
			this.AddElementFieldMapping("LayoutTableEntity", "Y", "Y", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 6);
			this.AddElementFieldMapping("LayoutTableEntity", "SeatCount", "SeatCount", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 7);
			this.AddElementFieldMapping("LayoutTableEntity", "SectionIndex", "SectionIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("LayoutTableEntity", "IsNonSmoking", "IsNonSmoking", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("LayoutTableEntity", "JoinTableIndex", "JoinTableIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("LayoutTableEntity", "IsX10Enabled", "IsX10Enabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("LayoutTableEntity", "X10HouseCode", "X10HouseCode", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 12);
			this.AddElementFieldMapping("LayoutTableEntity", "X10DeviceCode", "X10DeviceCode", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 13);
			this.AddElementFieldMapping("LayoutTableEntity", "RoomIndex", "RoomIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits PriceLevelEntity's mappings</summary>
		private void InitPriceLevelEntityMappings()
		{
			this.AddElementMapping("PriceLevelEntity", @"Tissl", @"dbo", "PriceLevel", 4, 0);
			this.AddElementFieldMapping("PriceLevelEntity", "PriceLevelId", "PriceLevelID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("PriceLevelEntity", "StoreId", "StoreID", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PriceLevelEntity", "PriceLevelIndex", "PriceLevelIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PriceLevelEntity", "PriceLevelName", "PriceLevelName", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits PriceScheduleEntity's mappings</summary>
		private void InitPriceScheduleEntityMappings()
		{
			this.AddElementMapping("PriceScheduleEntity", @"Tissl", @"dbo", "PriceSchedule", 12, 0);
			this.AddElementFieldMapping("PriceScheduleEntity", "PriceScheduleId", "PriceScheduleID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("PriceScheduleEntity", "StoreId", "StoreID", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PriceScheduleEntity", "PriceScheduleIndex", "PriceScheduleIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PriceScheduleEntity", "PriceScheduleName", "PriceScheduleName", false, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PriceScheduleEntity", "ScheduleDays", "ScheduleDays", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PriceScheduleEntity", "StartTime", "StartTime", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PriceScheduleEntity", "EndTime", "EndTime", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PriceScheduleEntity", "IsSeasonal", "IsSeasonal", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("PriceScheduleEntity", "SeasonalStartMonth", "SeasonalStartMonth", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("PriceScheduleEntity", "SeasonalStartDay", "SeasonalStartDay", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PriceScheduleEntity", "SeasonalEndMonth", "SeasonalEndMonth", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("PriceScheduleEntity", "SeasonalEndDay", "SeasonalEndDay", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
		}

		/// <summary>Inits VendorEntity's mappings</summary>
		private void InitVendorEntityMappings()
		{
			this.AddElementMapping("VendorEntity", @"Tissl", @"dbo", "Vendor", 10, 0);
			this.AddElementFieldMapping("VendorEntity", "VendorId", "VendorID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("VendorEntity", "StoreId", "StoreID", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("VendorEntity", "VendorName", "VendorName", false, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("VendorEntity", "Phone", "Phone", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("VendorEntity", "Fax", "Fax", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("VendorEntity", "Address", "Address", true, "NVarChar", 64, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("VendorEntity", "City", "City", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("VendorEntity", "State", "State", true, "NVarChar", 2, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("VendorEntity", "Zip", "Zip", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("VendorEntity", "Notes", "Notes", true, "NVarChar", 420, 0, 0, false, "", null, typeof(System.String), 9);
		}

		/// <summary>Inits VersionEntity's mappings</summary>
		private void InitVersionEntityMappings()
		{
			this.AddElementMapping("VersionEntity", @"Tissl", @"dbo", "Version", 4, 0);
			this.AddElementFieldMapping("VersionEntity", "VersionId", "VersionID", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 0);
			this.AddElementFieldMapping("VersionEntity", "StoreId", "StoreID", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("VersionEntity", "CodeVersion", "CodeVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("VersionEntity", "DatabaseVersion", "DatabaseVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

	}
}
