﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (13 + 0));
			InitDepartmentEntityInfos();
			InitItemEntityInfos();
			InitItemDayEntityInfos();
			InitItemIngredientEntityInfos();
			InitItemModifierEntityInfos();
			InitItemPriceEntityInfos();
			InitItemPrinterEntityInfos();
			InitLayoutRoomEntityInfos();
			InitLayoutTableEntityInfos();
			InitPriceLevelEntityInfos();
			InitPriceScheduleEntityInfos();
			InitVendorEntityInfos();
			InitVersionEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits DepartmentEntity's FieldInfo objects</summary>
		private void InitDepartmentEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(DepartmentFieldIndex), "DepartmentEntity");
			this.AddElementFieldInfo("DepartmentEntity", "DepartmentId", typeof(System.Guid), true, false, false, false,  (int)DepartmentFieldIndex.DepartmentId, 0, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "RegionId", typeof(System.Guid), false, false, false, false,  (int)DepartmentFieldIndex.RegionId, 0, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "DepartmentName", typeof(System.String), false, false, false, false,  (int)DepartmentFieldIndex.DepartmentName, 10, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "DepartmentDescription", typeof(System.String), false, false, false, true,  (int)DepartmentFieldIndex.DepartmentDescription, 30, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "GroupName", typeof(System.String), false, false, false, true,  (int)DepartmentFieldIndex.GroupName, 30, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "IsHash", typeof(System.Boolean), false, false, false, false,  (int)DepartmentFieldIndex.IsHash, 0, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "PmsbucketNumber", typeof(System.Byte), false, false, false, false,  (int)DepartmentFieldIndex.PmsbucketNumber, 0, 0, 3);
			this.AddElementFieldInfo("DepartmentEntity", "IsUsedOnline", typeof(System.Boolean), false, false, false, false,  (int)DepartmentFieldIndex.IsUsedOnline, 0, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "WebTitle", typeof(System.String), false, false, false, true,  (int)DepartmentFieldIndex.WebTitle, 30, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "WebDescription", typeof(System.String), false, false, false, true,  (int)DepartmentFieldIndex.WebDescription, 2147483647, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "WebThumbPath", typeof(System.String), false, false, false, true,  (int)DepartmentFieldIndex.WebThumbPath, 254, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "WebImagePath", typeof(System.String), false, false, false, true,  (int)DepartmentFieldIndex.WebImagePath, 254, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "IsDefault", typeof(System.Boolean), false, false, false, false,  (int)DepartmentFieldIndex.IsDefault, 0, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "DisplayIndex", typeof(System.Int32), false, false, false, false,  (int)DepartmentFieldIndex.DisplayIndex, 0, 0, 10);
			this.AddElementFieldInfo("DepartmentEntity", "IsParent", typeof(System.Boolean), false, false, false, false,  (int)DepartmentFieldIndex.IsParent, 0, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "ParentDepartmentId", typeof(Nullable<System.Guid>), false, false, false, true,  (int)DepartmentFieldIndex.ParentDepartmentId, 0, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "Vduid", typeof(System.Int32), false, false, false, false,  (int)DepartmentFieldIndex.Vduid, 0, 0, 10);
			this.AddElementFieldInfo("DepartmentEntity", "AskId", typeof(System.Boolean), false, false, false, false,  (int)DepartmentFieldIndex.AskId, 0, 0, 0);
			this.AddElementFieldInfo("DepartmentEntity", "QuantityMultiplier", typeof(System.Int32), false, false, false, false,  (int)DepartmentFieldIndex.QuantityMultiplier, 0, 0, 10);
		}
		/// <summary>Inits ItemEntity's FieldInfo objects</summary>
		private void InitItemEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ItemFieldIndex), "ItemEntity");
			this.AddElementFieldInfo("ItemEntity", "ItemId", typeof(System.Guid), true, false, false, false,  (int)ItemFieldIndex.ItemId, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "RegionId", typeof(System.Guid), false, false, false, false,  (int)ItemFieldIndex.RegionId, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ItemName", typeof(System.String), false, false, false, false,  (int)ItemFieldIndex.ItemName, 32, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ItemDescription", typeof(System.String), false, false, false, false,  (int)ItemFieldIndex.ItemDescription, 30, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "Department", typeof(System.String), false, false, false, false,  (int)ItemFieldIndex.Department, 10, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "Upc", typeof(System.String), false, false, false, false,  (int)ItemFieldIndex.Upc, 32, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ReceiptDesc", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.ReceiptDesc, 20, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "PrintDoubleWide", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.PrintDoubleWide, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "PrintAltColor", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.PrintAltColor, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ItemCount", typeof(System.Int16), false, false, false, false,  (int)ItemFieldIndex.ItemCount, 0, 0, 5);
			this.AddElementFieldInfo("ItemEntity", "AlternateItem", typeof(System.String), false, false, false, false,  (int)ItemFieldIndex.AlternateItem, 32, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "FireDelay", typeof(System.Int16), false, false, false, false,  (int)ItemFieldIndex.FireDelay, 0, 0, 5);
			this.AddElementFieldInfo("ItemEntity", "IsModifier", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsModifier, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "OrderPriority", typeof(System.Byte), false, false, false, false,  (int)ItemFieldIndex.OrderPriority, 0, 0, 3);
			this.AddElementFieldInfo("ItemEntity", "IsStoreChargeable", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsStoreChargeable, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "AskForPrice", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.AskForPrice, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "TogoSurcharge", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.TogoSurcharge, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ItemCost", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.ItemCost, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "IsScaleable", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsScaleable, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "IsNotTippable", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsNotTippable, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "PriceIsNegative", typeof(System.Byte), false, false, false, false,  (int)ItemFieldIndex.PriceIsNegative, 0, 0, 3);
			this.AddElementFieldInfo("ItemEntity", "IsPromoItem", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsPromoItem, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "BergPlu", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.BergPlu, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "ModifierFollowsParent", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.ModifierFollowsParent, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ModifierDescription", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.ModifierDescription, 30, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "UseModifierMaxSelect", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.UseModifierMaxSelect, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ModifierMaxSelect", typeof(System.Int16), false, false, false, false,  (int)ItemFieldIndex.ModifierMaxSelect, 0, 0, 5);
			this.AddElementFieldInfo("ItemEntity", "UseModifierMinSelect", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.UseModifierMinSelect, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ModifierMinSelect", typeof(System.Int16), false, false, false, false,  (int)ItemFieldIndex.ModifierMinSelect, 0, 0, 5);
			this.AddElementFieldInfo("ItemEntity", "AllowModifierMaxBypass", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.AllowModifierMaxBypass, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "UsePizzaStyle", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.UsePizzaStyle, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "IsTimedItem", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsTimedItem, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "TimingIncrement", typeof(System.Byte), false, false, false, false,  (int)ItemFieldIndex.TimingIncrement, 0, 0, 3);
			this.AddElementFieldInfo("ItemEntity", "MinimumPrice", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.MinimumPrice, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "Vducolor", typeof(System.Byte), false, false, false, false,  (int)ItemFieldIndex.Vducolor, 0, 0, 3);
			this.AddElementFieldInfo("ItemEntity", "ShortDescription", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.ShortDescription, 5, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "DefaultCourse", typeof(System.Byte), false, false, false, false,  (int)ItemFieldIndex.DefaultCourse, 0, 0, 3);
			this.AddElementFieldInfo("ItemEntity", "ChineseOutput", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.ChineseOutput, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "TripleHigh", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.TripleHigh, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "QuadHigh", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.QuadHigh, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "DiscountFlags", typeof(System.Byte[]), false, false, false, false,  (int)ItemFieldIndex.DiscountFlags, 13, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "TaxFlags", typeof(System.Byte), false, false, false, false,  (int)ItemFieldIndex.TaxFlags, 0, 0, 3);
			this.AddElementFieldInfo("ItemEntity", "PrintOptions", typeof(System.Byte), false, false, false, false,  (int)ItemFieldIndex.PrintOptions, 0, 0, 3);
			this.AddElementFieldInfo("ItemEntity", "RemotePrinters", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.RemotePrinters, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "AllowZeroPrice", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.AllowZeroPrice, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ModifierPriceLevel", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.ModifierPriceLevel, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "ModifierCount", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.ModifierCount, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "IngredientCount", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.IngredientCount, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "PrepTime", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.PrepTime, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "MultModPrice", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.MultModPrice, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "MultAmount", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.MultAmount, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "MultRound", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.MultRound, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "IsNotGratable", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsNotGratable, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ModifierSortType", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.ModifierSortType, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "IsUsedOnline", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsUsedOnline, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "WebTitle", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.WebTitle, 30, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "WebDescription", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.WebDescription, 2147483647, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "WebThumbPath", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.WebThumbPath, 254, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "WebImagePath", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.WebImagePath, 254, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "DisplayIndex", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.DisplayIndex, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "ModDisplayOrder", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.ModDisplayOrder, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "OverridePrice", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.OverridePrice, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "SwappedDept", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.SwappedDept, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "MaxSelectionAllowed", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.MaxSelectionAllowed, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "IsShippable", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsShippable, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ShipWidth", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.ShipWidth, 5, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ShipHeight", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.ShipHeight, 5, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ShipLength", typeof(System.String), false, false, false, true,  (int)ItemFieldIndex.ShipLength, 5, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "AddPrepTimeToParent", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.AddPrepTimeToParent, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "Vduid", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.Vduid, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "HhcolumnCount", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.HhcolumnCount, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "WebDepartmentId", typeof(Nullable<System.Guid>), false, false, false, true,  (int)ItemFieldIndex.WebDepartmentId, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "AskId", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.AskId, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "QuantityMultiplier", typeof(System.Int32), false, false, false, false,  (int)ItemFieldIndex.QuantityMultiplier, 0, 0, 10);
			this.AddElementFieldInfo("ItemEntity", "IsModifierGroup", typeof(System.Boolean), false, false, false, false,  (int)ItemFieldIndex.IsModifierGroup, 0, 0, 0);
			this.AddElementFieldInfo("ItemEntity", "ModifierPriceRounding", typeof(System.Byte), false, false, false, false,  (int)ItemFieldIndex.ModifierPriceRounding, 0, 0, 3);
		}
		/// <summary>Inits ItemDayEntity's FieldInfo objects</summary>
		private void InitItemDayEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ItemDayFieldIndex), "ItemDayEntity");
			this.AddElementFieldInfo("ItemDayEntity", "ItemDayId", typeof(System.Guid), true, false, false, false,  (int)ItemDayFieldIndex.ItemDayId, 0, 0, 0);
			this.AddElementFieldInfo("ItemDayEntity", "ItemId", typeof(System.Guid), false, true, false, false,  (int)ItemDayFieldIndex.ItemId, 0, 0, 0);
			this.AddElementFieldInfo("ItemDayEntity", "DepartmentId", typeof(System.Guid), false, true, false, false,  (int)ItemDayFieldIndex.DepartmentId, 0, 0, 0);
			this.AddElementFieldInfo("ItemDayEntity", "Date", typeof(System.DateTime), false, false, false, false,  (int)ItemDayFieldIndex.Date, 0, 0, 0);
		}
		/// <summary>Inits ItemIngredientEntity's FieldInfo objects</summary>
		private void InitItemIngredientEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ItemIngredientFieldIndex), "ItemIngredientEntity");
			this.AddElementFieldInfo("ItemIngredientEntity", "ItemIngredientId", typeof(System.Guid), true, false, false, false,  (int)ItemIngredientFieldIndex.ItemIngredientId, 0, 0, 0);
			this.AddElementFieldInfo("ItemIngredientEntity", "ItemId", typeof(System.Guid), false, true, false, false,  (int)ItemIngredientFieldIndex.ItemId, 0, 0, 0);
			this.AddElementFieldInfo("ItemIngredientEntity", "IngredientName", typeof(System.String), false, false, false, false,  (int)ItemIngredientFieldIndex.IngredientName, 33, 0, 0);
			this.AddElementFieldInfo("ItemIngredientEntity", "IngredientIndex", typeof(System.Int32), false, false, false, false,  (int)ItemIngredientFieldIndex.IngredientIndex, 0, 0, 10);
			this.AddElementFieldInfo("ItemIngredientEntity", "UnitCount", typeof(System.Int32), false, false, false, false,  (int)ItemIngredientFieldIndex.UnitCount, 0, 0, 10);
		}
		/// <summary>Inits ItemModifierEntity's FieldInfo objects</summary>
		private void InitItemModifierEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ItemModifierFieldIndex), "ItemModifierEntity");
			this.AddElementFieldInfo("ItemModifierEntity", "ItemModifierId", typeof(System.Guid), true, false, false, false,  (int)ItemModifierFieldIndex.ItemModifierId, 0, 0, 0);
			this.AddElementFieldInfo("ItemModifierEntity", "ItemId", typeof(System.Guid), false, true, false, false,  (int)ItemModifierFieldIndex.ItemId, 0, 0, 0);
			this.AddElementFieldInfo("ItemModifierEntity", "ModifierIndex", typeof(System.Int32), false, false, false, false,  (int)ItemModifierFieldIndex.ModifierIndex, 0, 0, 10);
			this.AddElementFieldInfo("ItemModifierEntity", "ModifierName", typeof(System.String), false, false, false, false,  (int)ItemModifierFieldIndex.ModifierName, 33, 0, 0);
			this.AddElementFieldInfo("ItemModifierEntity", "IsSelected", typeof(System.Boolean), false, false, false, false,  (int)ItemModifierFieldIndex.IsSelected, 0, 0, 0);
		}
		/// <summary>Inits ItemPriceEntity's FieldInfo objects</summary>
		private void InitItemPriceEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ItemPriceFieldIndex), "ItemPriceEntity");
			this.AddElementFieldInfo("ItemPriceEntity", "ItemPriceId", typeof(System.Guid), true, false, false, false,  (int)ItemPriceFieldIndex.ItemPriceId, 0, 0, 0);
			this.AddElementFieldInfo("ItemPriceEntity", "ItemId", typeof(System.Guid), false, true, false, false,  (int)ItemPriceFieldIndex.ItemId, 0, 0, 0);
			this.AddElementFieldInfo("ItemPriceEntity", "ScheduleIndex", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.ScheduleIndex, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "DefaultPrice", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.DefaultPrice, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level1Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level1Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level2Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level2Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level3Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level3Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level4Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level4Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level5Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level5Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level6Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level6Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level7Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level7Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level8Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level8Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level9Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level9Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level10Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level10Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level11Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level11Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level12Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level12Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level13Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level13Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level14Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level14Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level15Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level15Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level16Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level16Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level17Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level17Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level18Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level18Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level19Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level19Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level20Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level20Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level21Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level21Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level22Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level22Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level23Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level23Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level24Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level24Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level25Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level25Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level26Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level26Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level27Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level27Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level28Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level28Price, 0, 0, 10);
			this.AddElementFieldInfo("ItemPriceEntity", "Level29Price", typeof(System.Int32), false, false, false, false,  (int)ItemPriceFieldIndex.Level29Price, 0, 0, 10);
		}
		/// <summary>Inits ItemPrinterEntity's FieldInfo objects</summary>
		private void InitItemPrinterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ItemPrinterFieldIndex), "ItemPrinterEntity");
			this.AddElementFieldInfo("ItemPrinterEntity", "ItemPrinterId", typeof(System.Guid), true, false, false, false,  (int)ItemPrinterFieldIndex.ItemPrinterId, 0, 0, 0);
			this.AddElementFieldInfo("ItemPrinterEntity", "ItemId", typeof(System.Guid), false, true, false, false,  (int)ItemPrinterFieldIndex.ItemId, 0, 0, 0);
			this.AddElementFieldInfo("ItemPrinterEntity", "PrinterIndex", typeof(System.Int32), false, false, false, false,  (int)ItemPrinterFieldIndex.PrinterIndex, 0, 0, 10);
			this.AddElementFieldInfo("ItemPrinterEntity", "ReceiptDescription", typeof(System.String), false, false, false, true,  (int)ItemPrinterFieldIndex.ReceiptDescription, 20, 0, 0);
		}
		/// <summary>Inits LayoutRoomEntity's FieldInfo objects</summary>
		private void InitLayoutRoomEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(LayoutRoomFieldIndex), "LayoutRoomEntity");
			this.AddElementFieldInfo("LayoutRoomEntity", "LayoutRoomId", typeof(System.Guid), true, false, false, false,  (int)LayoutRoomFieldIndex.LayoutRoomId, 0, 0, 0);
			this.AddElementFieldInfo("LayoutRoomEntity", "LayoutId", typeof(System.Guid), false, false, false, false,  (int)LayoutRoomFieldIndex.LayoutId, 0, 0, 0);
			this.AddElementFieldInfo("LayoutRoomEntity", "RoomIndex", typeof(System.Int32), false, false, false, false,  (int)LayoutRoomFieldIndex.RoomIndex, 0, 0, 10);
			this.AddElementFieldInfo("LayoutRoomEntity", "RoomName", typeof(System.String), false, false, false, false,  (int)LayoutRoomFieldIndex.RoomName, 20, 0, 0);
			this.AddElementFieldInfo("LayoutRoomEntity", "ImageFile", typeof(System.String), false, false, false, false,  (int)LayoutRoomFieldIndex.ImageFile, 128, 0, 0);
			this.AddElementFieldInfo("LayoutRoomEntity", "PriceLevel", typeof(System.Int16), false, false, false, false,  (int)LayoutRoomFieldIndex.PriceLevel, 0, 0, 5);
		}
		/// <summary>Inits LayoutTableEntity's FieldInfo objects</summary>
		private void InitLayoutTableEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(LayoutTableFieldIndex), "LayoutTableEntity");
			this.AddElementFieldInfo("LayoutTableEntity", "LayoutTableId", typeof(System.Guid), true, false, false, false,  (int)LayoutTableFieldIndex.LayoutTableId, 0, 0, 0);
			this.AddElementFieldInfo("LayoutTableEntity", "LayoutId", typeof(System.Guid), false, false, false, false,  (int)LayoutTableFieldIndex.LayoutId, 0, 0, 0);
			this.AddElementFieldInfo("LayoutTableEntity", "TableIndex", typeof(System.Int32), false, false, false, false,  (int)LayoutTableFieldIndex.TableIndex, 0, 0, 10);
			this.AddElementFieldInfo("LayoutTableEntity", "TableName", typeof(System.String), false, false, false, false,  (int)LayoutTableFieldIndex.TableName, 50, 0, 0);
			this.AddElementFieldInfo("LayoutTableEntity", "ShapeType", typeof(System.Byte), false, false, false, false,  (int)LayoutTableFieldIndex.ShapeType, 0, 0, 3);
			this.AddElementFieldInfo("LayoutTableEntity", "X", typeof(System.Int16), false, false, false, false,  (int)LayoutTableFieldIndex.X, 0, 0, 5);
			this.AddElementFieldInfo("LayoutTableEntity", "Y", typeof(System.Int16), false, false, false, false,  (int)LayoutTableFieldIndex.Y, 0, 0, 5);
			this.AddElementFieldInfo("LayoutTableEntity", "SeatCount", typeof(System.Byte), false, false, false, false,  (int)LayoutTableFieldIndex.SeatCount, 0, 0, 3);
			this.AddElementFieldInfo("LayoutTableEntity", "SectionIndex", typeof(System.Int32), false, false, false, false,  (int)LayoutTableFieldIndex.SectionIndex, 0, 0, 10);
			this.AddElementFieldInfo("LayoutTableEntity", "IsNonSmoking", typeof(System.Boolean), false, false, false, false,  (int)LayoutTableFieldIndex.IsNonSmoking, 0, 0, 0);
			this.AddElementFieldInfo("LayoutTableEntity", "JoinTableIndex", typeof(System.Int32), false, false, false, false,  (int)LayoutTableFieldIndex.JoinTableIndex, 0, 0, 10);
			this.AddElementFieldInfo("LayoutTableEntity", "IsX10Enabled", typeof(System.Boolean), false, false, false, false,  (int)LayoutTableFieldIndex.IsX10Enabled, 0, 0, 0);
			this.AddElementFieldInfo("LayoutTableEntity", "X10HouseCode", typeof(System.Byte), false, false, false, false,  (int)LayoutTableFieldIndex.X10HouseCode, 0, 0, 3);
			this.AddElementFieldInfo("LayoutTableEntity", "X10DeviceCode", typeof(System.Byte), false, false, false, false,  (int)LayoutTableFieldIndex.X10DeviceCode, 0, 0, 3);
			this.AddElementFieldInfo("LayoutTableEntity", "RoomIndex", typeof(System.Int32), false, false, false, false,  (int)LayoutTableFieldIndex.RoomIndex, 0, 0, 10);
		}
		/// <summary>Inits PriceLevelEntity's FieldInfo objects</summary>
		private void InitPriceLevelEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(PriceLevelFieldIndex), "PriceLevelEntity");
			this.AddElementFieldInfo("PriceLevelEntity", "PriceLevelId", typeof(System.Guid), true, false, false, false,  (int)PriceLevelFieldIndex.PriceLevelId, 0, 0, 0);
			this.AddElementFieldInfo("PriceLevelEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)PriceLevelFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("PriceLevelEntity", "PriceLevelIndex", typeof(System.Int32), false, false, false, false,  (int)PriceLevelFieldIndex.PriceLevelIndex, 0, 0, 10);
			this.AddElementFieldInfo("PriceLevelEntity", "PriceLevelName", typeof(System.String), false, false, false, false,  (int)PriceLevelFieldIndex.PriceLevelName, 50, 0, 0);
		}
		/// <summary>Inits PriceScheduleEntity's FieldInfo objects</summary>
		private void InitPriceScheduleEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(PriceScheduleFieldIndex), "PriceScheduleEntity");
			this.AddElementFieldInfo("PriceScheduleEntity", "PriceScheduleId", typeof(System.Guid), true, false, false, false,  (int)PriceScheduleFieldIndex.PriceScheduleId, 0, 0, 0);
			this.AddElementFieldInfo("PriceScheduleEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("PriceScheduleEntity", "PriceScheduleIndex", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.PriceScheduleIndex, 0, 0, 10);
			this.AddElementFieldInfo("PriceScheduleEntity", "PriceScheduleName", typeof(System.String), false, false, false, false,  (int)PriceScheduleFieldIndex.PriceScheduleName, 30, 0, 0);
			this.AddElementFieldInfo("PriceScheduleEntity", "ScheduleDays", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.ScheduleDays, 0, 0, 10);
			this.AddElementFieldInfo("PriceScheduleEntity", "StartTime", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.StartTime, 0, 0, 10);
			this.AddElementFieldInfo("PriceScheduleEntity", "EndTime", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.EndTime, 0, 0, 10);
			this.AddElementFieldInfo("PriceScheduleEntity", "IsSeasonal", typeof(System.Boolean), false, false, false, false,  (int)PriceScheduleFieldIndex.IsSeasonal, 0, 0, 0);
			this.AddElementFieldInfo("PriceScheduleEntity", "SeasonalStartMonth", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.SeasonalStartMonth, 0, 0, 10);
			this.AddElementFieldInfo("PriceScheduleEntity", "SeasonalStartDay", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.SeasonalStartDay, 0, 0, 10);
			this.AddElementFieldInfo("PriceScheduleEntity", "SeasonalEndMonth", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.SeasonalEndMonth, 0, 0, 10);
			this.AddElementFieldInfo("PriceScheduleEntity", "SeasonalEndDay", typeof(System.Int32), false, false, false, false,  (int)PriceScheduleFieldIndex.SeasonalEndDay, 0, 0, 10);
		}
		/// <summary>Inits VendorEntity's FieldInfo objects</summary>
		private void InitVendorEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(VendorFieldIndex), "VendorEntity");
			this.AddElementFieldInfo("VendorEntity", "VendorId", typeof(System.Guid), true, false, false, false,  (int)VendorFieldIndex.VendorId, 0, 0, 0);
			this.AddElementFieldInfo("VendorEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)VendorFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("VendorEntity", "VendorName", typeof(System.String), false, false, false, false,  (int)VendorFieldIndex.VendorName, 20, 0, 0);
			this.AddElementFieldInfo("VendorEntity", "Phone", typeof(System.String), false, false, false, true,  (int)VendorFieldIndex.Phone, 16, 0, 0);
			this.AddElementFieldInfo("VendorEntity", "Fax", typeof(System.String), false, false, false, true,  (int)VendorFieldIndex.Fax, 16, 0, 0);
			this.AddElementFieldInfo("VendorEntity", "Address", typeof(System.String), false, false, false, true,  (int)VendorFieldIndex.Address, 64, 0, 0);
			this.AddElementFieldInfo("VendorEntity", "City", typeof(System.String), false, false, false, true,  (int)VendorFieldIndex.City, 16, 0, 0);
			this.AddElementFieldInfo("VendorEntity", "State", typeof(System.String), false, false, false, true,  (int)VendorFieldIndex.State, 2, 0, 0);
			this.AddElementFieldInfo("VendorEntity", "Zip", typeof(System.String), false, false, false, true,  (int)VendorFieldIndex.Zip, 16, 0, 0);
			this.AddElementFieldInfo("VendorEntity", "Notes", typeof(System.String), false, false, false, true,  (int)VendorFieldIndex.Notes, 420, 0, 0);
		}
		/// <summary>Inits VersionEntity's FieldInfo objects</summary>
		private void InitVersionEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(VersionFieldIndex), "VersionEntity");
			this.AddElementFieldInfo("VersionEntity", "VersionId", typeof(System.Guid), true, false, false, false,  (int)VersionFieldIndex.VersionId, 0, 0, 0);
			this.AddElementFieldInfo("VersionEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)VersionFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("VersionEntity", "CodeVersion", typeof(System.Int32), false, false, false, false,  (int)VersionFieldIndex.CodeVersion, 0, 0, 10);
			this.AddElementFieldInfo("VersionEntity", "DatabaseVersion", typeof(System.Int32), false, false, false, false,  (int)VersionFieldIndex.DatabaseVersion, 0, 0, 10);
		}
		
	}
}




