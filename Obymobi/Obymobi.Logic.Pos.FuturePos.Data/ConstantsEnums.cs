﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace Obymobi.Logic.Pos.FuturePos.Data
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Department.</summary>
	public enum DepartmentFieldIndex
	{
		///<summary>DepartmentId. </summary>
		DepartmentId,
		///<summary>RegionId. </summary>
		RegionId,
		///<summary>DepartmentName. </summary>
		DepartmentName,
		///<summary>DepartmentDescription. </summary>
		DepartmentDescription,
		///<summary>GroupName. </summary>
		GroupName,
		///<summary>IsHash. </summary>
		IsHash,
		///<summary>PmsbucketNumber. </summary>
		PmsbucketNumber,
		///<summary>IsUsedOnline. </summary>
		IsUsedOnline,
		///<summary>WebTitle. </summary>
		WebTitle,
		///<summary>WebDescription. </summary>
		WebDescription,
		///<summary>WebThumbPath. </summary>
		WebThumbPath,
		///<summary>WebImagePath. </summary>
		WebImagePath,
		///<summary>IsDefault. </summary>
		IsDefault,
		///<summary>DisplayIndex. </summary>
		DisplayIndex,
		///<summary>IsParent. </summary>
		IsParent,
		///<summary>ParentDepartmentId. </summary>
		ParentDepartmentId,
		///<summary>Vduid. </summary>
		Vduid,
		///<summary>AskId. </summary>
		AskId,
		///<summary>QuantityMultiplier. </summary>
		QuantityMultiplier,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Item.</summary>
	public enum ItemFieldIndex
	{
		///<summary>ItemId. </summary>
		ItemId,
		///<summary>RegionId. </summary>
		RegionId,
		///<summary>ItemName. </summary>
		ItemName,
		///<summary>ItemDescription. </summary>
		ItemDescription,
		///<summary>Department. </summary>
		Department,
		///<summary>Upc. </summary>
		Upc,
		///<summary>ReceiptDesc. </summary>
		ReceiptDesc,
		///<summary>PrintDoubleWide. </summary>
		PrintDoubleWide,
		///<summary>PrintAltColor. </summary>
		PrintAltColor,
		///<summary>ItemCount. </summary>
		ItemCount,
		///<summary>AlternateItem. </summary>
		AlternateItem,
		///<summary>FireDelay. </summary>
		FireDelay,
		///<summary>IsModifier. </summary>
		IsModifier,
		///<summary>OrderPriority. </summary>
		OrderPriority,
		///<summary>IsStoreChargeable. </summary>
		IsStoreChargeable,
		///<summary>AskForPrice. </summary>
		AskForPrice,
		///<summary>TogoSurcharge. </summary>
		TogoSurcharge,
		///<summary>ItemCost. </summary>
		ItemCost,
		///<summary>IsScaleable. </summary>
		IsScaleable,
		///<summary>IsNotTippable. </summary>
		IsNotTippable,
		///<summary>PriceIsNegative. </summary>
		PriceIsNegative,
		///<summary>IsPromoItem. </summary>
		IsPromoItem,
		///<summary>BergPlu. </summary>
		BergPlu,
		///<summary>ModifierFollowsParent. </summary>
		ModifierFollowsParent,
		///<summary>ModifierDescription. </summary>
		ModifierDescription,
		///<summary>UseModifierMaxSelect. </summary>
		UseModifierMaxSelect,
		///<summary>ModifierMaxSelect. </summary>
		ModifierMaxSelect,
		///<summary>UseModifierMinSelect. </summary>
		UseModifierMinSelect,
		///<summary>ModifierMinSelect. </summary>
		ModifierMinSelect,
		///<summary>AllowModifierMaxBypass. </summary>
		AllowModifierMaxBypass,
		///<summary>UsePizzaStyle. </summary>
		UsePizzaStyle,
		///<summary>IsTimedItem. </summary>
		IsTimedItem,
		///<summary>TimingIncrement. </summary>
		TimingIncrement,
		///<summary>MinimumPrice. </summary>
		MinimumPrice,
		///<summary>Vducolor. </summary>
		Vducolor,
		///<summary>ShortDescription. </summary>
		ShortDescription,
		///<summary>DefaultCourse. </summary>
		DefaultCourse,
		///<summary>ChineseOutput. </summary>
		ChineseOutput,
		///<summary>TripleHigh. </summary>
		TripleHigh,
		///<summary>QuadHigh. </summary>
		QuadHigh,
		///<summary>DiscountFlags. </summary>
		DiscountFlags,
		///<summary>TaxFlags. </summary>
		TaxFlags,
		///<summary>PrintOptions. </summary>
		PrintOptions,
		///<summary>RemotePrinters. </summary>
		RemotePrinters,
		///<summary>AllowZeroPrice. </summary>
		AllowZeroPrice,
		///<summary>ModifierPriceLevel. </summary>
		ModifierPriceLevel,
		///<summary>ModifierCount. </summary>
		ModifierCount,
		///<summary>IngredientCount. </summary>
		IngredientCount,
		///<summary>PrepTime. </summary>
		PrepTime,
		///<summary>MultModPrice. </summary>
		MultModPrice,
		///<summary>MultAmount. </summary>
		MultAmount,
		///<summary>MultRound. </summary>
		MultRound,
		///<summary>IsNotGratable. </summary>
		IsNotGratable,
		///<summary>ModifierSortType. </summary>
		ModifierSortType,
		///<summary>IsUsedOnline. </summary>
		IsUsedOnline,
		///<summary>WebTitle. </summary>
		WebTitle,
		///<summary>WebDescription. </summary>
		WebDescription,
		///<summary>WebThumbPath. </summary>
		WebThumbPath,
		///<summary>WebImagePath. </summary>
		WebImagePath,
		///<summary>DisplayIndex. </summary>
		DisplayIndex,
		///<summary>ModDisplayOrder. </summary>
		ModDisplayOrder,
		///<summary>OverridePrice. </summary>
		OverridePrice,
		///<summary>SwappedDept. </summary>
		SwappedDept,
		///<summary>MaxSelectionAllowed. </summary>
		MaxSelectionAllowed,
		///<summary>IsShippable. </summary>
		IsShippable,
		///<summary>ShipWidth. </summary>
		ShipWidth,
		///<summary>ShipHeight. </summary>
		ShipHeight,
		///<summary>ShipLength. </summary>
		ShipLength,
		///<summary>AddPrepTimeToParent. </summary>
		AddPrepTimeToParent,
		///<summary>Vduid. </summary>
		Vduid,
		///<summary>HhcolumnCount. </summary>
		HhcolumnCount,
		///<summary>WebDepartmentId. </summary>
		WebDepartmentId,
		///<summary>AskId. </summary>
		AskId,
		///<summary>QuantityMultiplier. </summary>
		QuantityMultiplier,
		///<summary>IsModifierGroup. </summary>
		IsModifierGroup,
		///<summary>ModifierPriceRounding. </summary>
		ModifierPriceRounding,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ItemDay.</summary>
	public enum ItemDayFieldIndex
	{
		///<summary>ItemDayId. </summary>
		ItemDayId,
		///<summary>ItemId. </summary>
		ItemId,
		///<summary>DepartmentId. </summary>
		DepartmentId,
		///<summary>Date. </summary>
		Date,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ItemIngredient.</summary>
	public enum ItemIngredientFieldIndex
	{
		///<summary>ItemIngredientId. </summary>
		ItemIngredientId,
		///<summary>ItemId. </summary>
		ItemId,
		///<summary>IngredientName. </summary>
		IngredientName,
		///<summary>IngredientIndex. </summary>
		IngredientIndex,
		///<summary>UnitCount. </summary>
		UnitCount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ItemModifier.</summary>
	public enum ItemModifierFieldIndex
	{
		///<summary>ItemModifierId. </summary>
		ItemModifierId,
		///<summary>ItemId. </summary>
		ItemId,
		///<summary>ModifierIndex. </summary>
		ModifierIndex,
		///<summary>ModifierName. </summary>
		ModifierName,
		///<summary>IsSelected. </summary>
		IsSelected,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ItemPrice.</summary>
	public enum ItemPriceFieldIndex
	{
		///<summary>ItemPriceId. </summary>
		ItemPriceId,
		///<summary>ItemId. </summary>
		ItemId,
		///<summary>ScheduleIndex. </summary>
		ScheduleIndex,
		///<summary>DefaultPrice. </summary>
		DefaultPrice,
		///<summary>Level1Price. </summary>
		Level1Price,
		///<summary>Level2Price. </summary>
		Level2Price,
		///<summary>Level3Price. </summary>
		Level3Price,
		///<summary>Level4Price. </summary>
		Level4Price,
		///<summary>Level5Price. </summary>
		Level5Price,
		///<summary>Level6Price. </summary>
		Level6Price,
		///<summary>Level7Price. </summary>
		Level7Price,
		///<summary>Level8Price. </summary>
		Level8Price,
		///<summary>Level9Price. </summary>
		Level9Price,
		///<summary>Level10Price. </summary>
		Level10Price,
		///<summary>Level11Price. </summary>
		Level11Price,
		///<summary>Level12Price. </summary>
		Level12Price,
		///<summary>Level13Price. </summary>
		Level13Price,
		///<summary>Level14Price. </summary>
		Level14Price,
		///<summary>Level15Price. </summary>
		Level15Price,
		///<summary>Level16Price. </summary>
		Level16Price,
		///<summary>Level17Price. </summary>
		Level17Price,
		///<summary>Level18Price. </summary>
		Level18Price,
		///<summary>Level19Price. </summary>
		Level19Price,
		///<summary>Level20Price. </summary>
		Level20Price,
		///<summary>Level21Price. </summary>
		Level21Price,
		///<summary>Level22Price. </summary>
		Level22Price,
		///<summary>Level23Price. </summary>
		Level23Price,
		///<summary>Level24Price. </summary>
		Level24Price,
		///<summary>Level25Price. </summary>
		Level25Price,
		///<summary>Level26Price. </summary>
		Level26Price,
		///<summary>Level27Price. </summary>
		Level27Price,
		///<summary>Level28Price. </summary>
		Level28Price,
		///<summary>Level29Price. </summary>
		Level29Price,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ItemPrinter.</summary>
	public enum ItemPrinterFieldIndex
	{
		///<summary>ItemPrinterId. </summary>
		ItemPrinterId,
		///<summary>ItemId. </summary>
		ItemId,
		///<summary>PrinterIndex. </summary>
		PrinterIndex,
		///<summary>ReceiptDescription. </summary>
		ReceiptDescription,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LayoutRoom.</summary>
	public enum LayoutRoomFieldIndex
	{
		///<summary>LayoutRoomId. </summary>
		LayoutRoomId,
		///<summary>LayoutId. </summary>
		LayoutId,
		///<summary>RoomIndex. </summary>
		RoomIndex,
		///<summary>RoomName. </summary>
		RoomName,
		///<summary>ImageFile. </summary>
		ImageFile,
		///<summary>PriceLevel. </summary>
		PriceLevel,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LayoutTable.</summary>
	public enum LayoutTableFieldIndex
	{
		///<summary>LayoutTableId. </summary>
		LayoutTableId,
		///<summary>LayoutId. </summary>
		LayoutId,
		///<summary>TableIndex. </summary>
		TableIndex,
		///<summary>TableName. </summary>
		TableName,
		///<summary>ShapeType. </summary>
		ShapeType,
		///<summary>X. </summary>
		X,
		///<summary>Y. </summary>
		Y,
		///<summary>SeatCount. </summary>
		SeatCount,
		///<summary>SectionIndex. </summary>
		SectionIndex,
		///<summary>IsNonSmoking. </summary>
		IsNonSmoking,
		///<summary>JoinTableIndex. </summary>
		JoinTableIndex,
		///<summary>IsX10Enabled. </summary>
		IsX10Enabled,
		///<summary>X10HouseCode. </summary>
		X10HouseCode,
		///<summary>X10DeviceCode. </summary>
		X10DeviceCode,
		///<summary>RoomIndex. </summary>
		RoomIndex,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceLevel.</summary>
	public enum PriceLevelFieldIndex
	{
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>PriceLevelIndex. </summary>
		PriceLevelIndex,
		///<summary>PriceLevelName. </summary>
		PriceLevelName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceSchedule.</summary>
	public enum PriceScheduleFieldIndex
	{
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>PriceScheduleIndex. </summary>
		PriceScheduleIndex,
		///<summary>PriceScheduleName. </summary>
		PriceScheduleName,
		///<summary>ScheduleDays. </summary>
		ScheduleDays,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>IsSeasonal. </summary>
		IsSeasonal,
		///<summary>SeasonalStartMonth. </summary>
		SeasonalStartMonth,
		///<summary>SeasonalStartDay. </summary>
		SeasonalStartDay,
		///<summary>SeasonalEndMonth. </summary>
		SeasonalEndMonth,
		///<summary>SeasonalEndDay. </summary>
		SeasonalEndDay,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Vendor.</summary>
	public enum VendorFieldIndex
	{
		///<summary>VendorId. </summary>
		VendorId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>VendorName. </summary>
		VendorName,
		///<summary>Phone. </summary>
		Phone,
		///<summary>Fax. </summary>
		Fax,
		///<summary>Address. </summary>
		Address,
		///<summary>City. </summary>
		City,
		///<summary>State. </summary>
		State,
		///<summary>Zip. </summary>
		Zip,
		///<summary>Notes. </summary>
		Notes,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Version.</summary>
	public enum VersionFieldIndex
	{
		///<summary>VersionId. </summary>
		VersionId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>CodeVersion. </summary>
		CodeVersion,
		///<summary>DatabaseVersion. </summary>
		DatabaseVersion,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Department</summary>
		DepartmentEntity,
		///<summary>Item</summary>
		ItemEntity,
		///<summary>ItemDay</summary>
		ItemDayEntity,
		///<summary>ItemIngredient</summary>
		ItemIngredientEntity,
		///<summary>ItemModifier</summary>
		ItemModifierEntity,
		///<summary>ItemPrice</summary>
		ItemPriceEntity,
		///<summary>ItemPrinter</summary>
		ItemPrinterEntity,
		///<summary>LayoutRoom</summary>
		LayoutRoomEntity,
		///<summary>LayoutTable</summary>
		LayoutTableEntity,
		///<summary>PriceLevel</summary>
		PriceLevelEntity,
		///<summary>PriceSchedule</summary>
		PriceScheduleEntity,
		///<summary>Vendor</summary>
		VendorEntity,
		///<summary>Version</summary>
		VersionEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

