﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Logic.Pos.FuturePos.Data;
using Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses;
using Obymobi.Logic.Pos.FuturePos.Data.DaoClasses;
using Obymobi.Logic.Pos.FuturePos.Data.RelationClasses;
using Obymobi.Logic.Pos.FuturePos.Data.HelperClasses;
using Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Item'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public string LLBLGenProEntityName {
			get { return "ItemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection	_itemDay;
		private bool	_alwaysFetchItemDay, _alreadyFetchedItemDay;
		private Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection	_itemIngredient;
		private bool	_alwaysFetchItemIngredient, _alreadyFetchedItemIngredient;
		private Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection	_itemModifier;
		private bool	_alwaysFetchItemModifier, _alreadyFetchedItemModifier;
		private Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection	_itemPrice;
		private bool	_alwaysFetchItemPrice, _alreadyFetchedItemPrice;
		private Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection	_itemPrinter;
		private bool	_alwaysFetchItemPrinter, _alreadyFetchedItemPrinter;
		private Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.DepartmentCollection _departmentCollectionViaItemDay;
		private bool	_alwaysFetchDepartmentCollectionViaItemDay, _alreadyFetchedDepartmentCollectionViaItemDay;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ItemDay</summary>
			public static readonly string ItemDay = "ItemDay";
			/// <summary>Member name ItemIngredient</summary>
			public static readonly string ItemIngredient = "ItemIngredient";
			/// <summary>Member name ItemModifier</summary>
			public static readonly string ItemModifier = "ItemModifier";
			/// <summary>Member name ItemPrice</summary>
			public static readonly string ItemPrice = "ItemPrice";
			/// <summary>Member name ItemPrinter</summary>
			public static readonly string ItemPrinter = "ItemPrinter";
			/// <summary>Member name DepartmentCollectionViaItemDay</summary>
			public static readonly string DepartmentCollectionViaItemDay = "DepartmentCollectionViaItemDay";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ItemEntityBase() :base("ItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		protected ItemEntityBase(System.Guid itemId):base("ItemEntity")
		{
			InitClassFetch(itemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ItemEntityBase(System.Guid itemId, IPrefetchPath prefetchPathToUse): base("ItemEntity")
		{
			InitClassFetch(itemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		/// <param name="validator">The custom validator object for this ItemEntity</param>
		protected ItemEntityBase(System.Guid itemId, IValidator validator):base("ItemEntity")
		{
			InitClassFetch(itemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_itemDay = (Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection)info.GetValue("_itemDay", typeof(Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection));
			_alwaysFetchItemDay = info.GetBoolean("_alwaysFetchItemDay");
			_alreadyFetchedItemDay = info.GetBoolean("_alreadyFetchedItemDay");

			_itemIngredient = (Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection)info.GetValue("_itemIngredient", typeof(Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection));
			_alwaysFetchItemIngredient = info.GetBoolean("_alwaysFetchItemIngredient");
			_alreadyFetchedItemIngredient = info.GetBoolean("_alreadyFetchedItemIngredient");

			_itemModifier = (Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection)info.GetValue("_itemModifier", typeof(Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection));
			_alwaysFetchItemModifier = info.GetBoolean("_alwaysFetchItemModifier");
			_alreadyFetchedItemModifier = info.GetBoolean("_alreadyFetchedItemModifier");

			_itemPrice = (Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection)info.GetValue("_itemPrice", typeof(Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection));
			_alwaysFetchItemPrice = info.GetBoolean("_alwaysFetchItemPrice");
			_alreadyFetchedItemPrice = info.GetBoolean("_alreadyFetchedItemPrice");

			_itemPrinter = (Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection)info.GetValue("_itemPrinter", typeof(Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection));
			_alwaysFetchItemPrinter = info.GetBoolean("_alwaysFetchItemPrinter");
			_alreadyFetchedItemPrinter = info.GetBoolean("_alreadyFetchedItemPrinter");
			_departmentCollectionViaItemDay = (Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.DepartmentCollection)info.GetValue("_departmentCollectionViaItemDay", typeof(Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.DepartmentCollection));
			_alwaysFetchDepartmentCollectionViaItemDay = info.GetBoolean("_alwaysFetchDepartmentCollectionViaItemDay");
			_alreadyFetchedDepartmentCollectionViaItemDay = info.GetBoolean("_alreadyFetchedDepartmentCollectionViaItemDay");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedItemDay = (_itemDay.Count > 0);
			_alreadyFetchedItemIngredient = (_itemIngredient.Count > 0);
			_alreadyFetchedItemModifier = (_itemModifier.Count > 0);
			_alreadyFetchedItemPrice = (_itemPrice.Count > 0);
			_alreadyFetchedItemPrinter = (_itemPrinter.Count > 0);
			_alreadyFetchedDepartmentCollectionViaItemDay = (_departmentCollectionViaItemDay.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ItemDay":
					toReturn.Add(Relations.ItemDayEntityUsingItemId);
					break;
				case "ItemIngredient":
					toReturn.Add(Relations.ItemIngredientEntityUsingItemId);
					break;
				case "ItemModifier":
					toReturn.Add(Relations.ItemModifierEntityUsingItemId);
					break;
				case "ItemPrice":
					toReturn.Add(Relations.ItemPriceEntityUsingItemId);
					break;
				case "ItemPrinter":
					toReturn.Add(Relations.ItemPrinterEntityUsingItemId);
					break;
				case "DepartmentCollectionViaItemDay":
					toReturn.Add(Relations.ItemDayEntityUsingItemId, "ItemEntity__", "ItemDay_", JoinHint.None);
					toReturn.Add(ItemDayEntity.Relations.DepartmentEntityUsingDepartmentId, "ItemDay_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_itemDay", (!this.MarkedForDeletion?_itemDay:null));
			info.AddValue("_alwaysFetchItemDay", _alwaysFetchItemDay);
			info.AddValue("_alreadyFetchedItemDay", _alreadyFetchedItemDay);
			info.AddValue("_itemIngredient", (!this.MarkedForDeletion?_itemIngredient:null));
			info.AddValue("_alwaysFetchItemIngredient", _alwaysFetchItemIngredient);
			info.AddValue("_alreadyFetchedItemIngredient", _alreadyFetchedItemIngredient);
			info.AddValue("_itemModifier", (!this.MarkedForDeletion?_itemModifier:null));
			info.AddValue("_alwaysFetchItemModifier", _alwaysFetchItemModifier);
			info.AddValue("_alreadyFetchedItemModifier", _alreadyFetchedItemModifier);
			info.AddValue("_itemPrice", (!this.MarkedForDeletion?_itemPrice:null));
			info.AddValue("_alwaysFetchItemPrice", _alwaysFetchItemPrice);
			info.AddValue("_alreadyFetchedItemPrice", _alreadyFetchedItemPrice);
			info.AddValue("_itemPrinter", (!this.MarkedForDeletion?_itemPrinter:null));
			info.AddValue("_alwaysFetchItemPrinter", _alwaysFetchItemPrinter);
			info.AddValue("_alreadyFetchedItemPrinter", _alreadyFetchedItemPrinter);
			info.AddValue("_departmentCollectionViaItemDay", (!this.MarkedForDeletion?_departmentCollectionViaItemDay:null));
			info.AddValue("_alwaysFetchDepartmentCollectionViaItemDay", _alwaysFetchDepartmentCollectionViaItemDay);
			info.AddValue("_alreadyFetchedDepartmentCollectionViaItemDay", _alreadyFetchedDepartmentCollectionViaItemDay);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ItemDay":
					_alreadyFetchedItemDay = true;
					if(entity!=null)
					{
						this.ItemDay.Add((ItemDayEntity)entity);
					}
					break;
				case "ItemIngredient":
					_alreadyFetchedItemIngredient = true;
					if(entity!=null)
					{
						this.ItemIngredient.Add((ItemIngredientEntity)entity);
					}
					break;
				case "ItemModifier":
					_alreadyFetchedItemModifier = true;
					if(entity!=null)
					{
						this.ItemModifier.Add((ItemModifierEntity)entity);
					}
					break;
				case "ItemPrice":
					_alreadyFetchedItemPrice = true;
					if(entity!=null)
					{
						this.ItemPrice.Add((ItemPriceEntity)entity);
					}
					break;
				case "ItemPrinter":
					_alreadyFetchedItemPrinter = true;
					if(entity!=null)
					{
						this.ItemPrinter.Add((ItemPrinterEntity)entity);
					}
					break;
				case "DepartmentCollectionViaItemDay":
					_alreadyFetchedDepartmentCollectionViaItemDay = true;
					if(entity!=null)
					{
						this.DepartmentCollectionViaItemDay.Add((DepartmentEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ItemDay":
					_itemDay.Add((ItemDayEntity)relatedEntity);
					break;
				case "ItemIngredient":
					_itemIngredient.Add((ItemIngredientEntity)relatedEntity);
					break;
				case "ItemModifier":
					_itemModifier.Add((ItemModifierEntity)relatedEntity);
					break;
				case "ItemPrice":
					_itemPrice.Add((ItemPriceEntity)relatedEntity);
					break;
				case "ItemPrinter":
					_itemPrinter.Add((ItemPrinterEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ItemDay":
					this.PerformRelatedEntityRemoval(_itemDay, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ItemIngredient":
					this.PerformRelatedEntityRemoval(_itemIngredient, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ItemModifier":
					this.PerformRelatedEntityRemoval(_itemModifier, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ItemPrice":
					this.PerformRelatedEntityRemoval(_itemPrice, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ItemPrinter":
					this.PerformRelatedEntityRemoval(_itemPrinter, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_itemDay);
			toReturn.Add(_itemIngredient);
			toReturn.Add(_itemModifier);
			toReturn.Add(_itemPrice);
			toReturn.Add(_itemPrinter);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Guid itemId)
		{
			return FetchUsingPK(itemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Guid itemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(itemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Guid itemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(itemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Guid itemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(itemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ItemDayEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection GetMultiItemDay(bool forceFetch)
		{
			return GetMultiItemDay(forceFetch, _itemDay.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ItemDayEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection GetMultiItemDay(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiItemDay(forceFetch, _itemDay.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection GetMultiItemDay(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiItemDay(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection GetMultiItemDay(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedItemDay || forceFetch || _alwaysFetchItemDay) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_itemDay);
				_itemDay.SuppressClearInGetMulti=!forceFetch;
				_itemDay.EntityFactoryToUse = entityFactoryToUse;
				_itemDay.GetMultiManyToOne(null, this, filter);
				_itemDay.SuppressClearInGetMulti=false;
				_alreadyFetchedItemDay = true;
			}
			return _itemDay;
		}

		/// <summary> Sets the collection parameters for the collection for 'ItemDay'. These settings will be taken into account
		/// when the property ItemDay is requested or GetMultiItemDay is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersItemDay(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_itemDay.SortClauses=sortClauses;
			_itemDay.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ItemIngredientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ItemIngredientEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection GetMultiItemIngredient(bool forceFetch)
		{
			return GetMultiItemIngredient(forceFetch, _itemIngredient.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemIngredientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ItemIngredientEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection GetMultiItemIngredient(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiItemIngredient(forceFetch, _itemIngredient.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ItemIngredientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection GetMultiItemIngredient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiItemIngredient(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemIngredientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection GetMultiItemIngredient(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedItemIngredient || forceFetch || _alwaysFetchItemIngredient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_itemIngredient);
				_itemIngredient.SuppressClearInGetMulti=!forceFetch;
				_itemIngredient.EntityFactoryToUse = entityFactoryToUse;
				_itemIngredient.GetMultiManyToOne(this, filter);
				_itemIngredient.SuppressClearInGetMulti=false;
				_alreadyFetchedItemIngredient = true;
			}
			return _itemIngredient;
		}

		/// <summary> Sets the collection parameters for the collection for 'ItemIngredient'. These settings will be taken into account
		/// when the property ItemIngredient is requested or GetMultiItemIngredient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersItemIngredient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_itemIngredient.SortClauses=sortClauses;
			_itemIngredient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ItemModifierEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ItemModifierEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection GetMultiItemModifier(bool forceFetch)
		{
			return GetMultiItemModifier(forceFetch, _itemModifier.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemModifierEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ItemModifierEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection GetMultiItemModifier(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiItemModifier(forceFetch, _itemModifier.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ItemModifierEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection GetMultiItemModifier(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiItemModifier(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemModifierEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection GetMultiItemModifier(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedItemModifier || forceFetch || _alwaysFetchItemModifier) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_itemModifier);
				_itemModifier.SuppressClearInGetMulti=!forceFetch;
				_itemModifier.EntityFactoryToUse = entityFactoryToUse;
				_itemModifier.GetMultiManyToOne(this, filter);
				_itemModifier.SuppressClearInGetMulti=false;
				_alreadyFetchedItemModifier = true;
			}
			return _itemModifier;
		}

		/// <summary> Sets the collection parameters for the collection for 'ItemModifier'. These settings will be taken into account
		/// when the property ItemModifier is requested or GetMultiItemModifier is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersItemModifier(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_itemModifier.SortClauses=sortClauses;
			_itemModifier.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ItemPriceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ItemPriceEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection GetMultiItemPrice(bool forceFetch)
		{
			return GetMultiItemPrice(forceFetch, _itemPrice.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemPriceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ItemPriceEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection GetMultiItemPrice(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiItemPrice(forceFetch, _itemPrice.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ItemPriceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection GetMultiItemPrice(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiItemPrice(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemPriceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection GetMultiItemPrice(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedItemPrice || forceFetch || _alwaysFetchItemPrice) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_itemPrice);
				_itemPrice.SuppressClearInGetMulti=!forceFetch;
				_itemPrice.EntityFactoryToUse = entityFactoryToUse;
				_itemPrice.GetMultiManyToOne(this, filter);
				_itemPrice.SuppressClearInGetMulti=false;
				_alreadyFetchedItemPrice = true;
			}
			return _itemPrice;
		}

		/// <summary> Sets the collection parameters for the collection for 'ItemPrice'. These settings will be taken into account
		/// when the property ItemPrice is requested or GetMultiItemPrice is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersItemPrice(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_itemPrice.SortClauses=sortClauses;
			_itemPrice.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ItemPrinterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ItemPrinterEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection GetMultiItemPrinter(bool forceFetch)
		{
			return GetMultiItemPrinter(forceFetch, _itemPrinter.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemPrinterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ItemPrinterEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection GetMultiItemPrinter(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiItemPrinter(forceFetch, _itemPrinter.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ItemPrinterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection GetMultiItemPrinter(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiItemPrinter(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemPrinterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection GetMultiItemPrinter(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedItemPrinter || forceFetch || _alwaysFetchItemPrinter) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_itemPrinter);
				_itemPrinter.SuppressClearInGetMulti=!forceFetch;
				_itemPrinter.EntityFactoryToUse = entityFactoryToUse;
				_itemPrinter.GetMultiManyToOne(this, filter);
				_itemPrinter.SuppressClearInGetMulti=false;
				_alreadyFetchedItemPrinter = true;
			}
			return _itemPrinter;
		}

		/// <summary> Sets the collection parameters for the collection for 'ItemPrinter'. These settings will be taken into account
		/// when the property ItemPrinter is requested or GetMultiItemPrinter is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersItemPrinter(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_itemPrinter.SortClauses=sortClauses;
			_itemPrinter.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DepartmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DepartmentEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.DepartmentCollection GetMultiDepartmentCollectionViaItemDay(bool forceFetch)
		{
			return GetMultiDepartmentCollectionViaItemDay(forceFetch, _departmentCollectionViaItemDay.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DepartmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.DepartmentCollection GetMultiDepartmentCollectionViaItemDay(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDepartmentCollectionViaItemDay || forceFetch || _alwaysFetchDepartmentCollectionViaItemDay) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_departmentCollectionViaItemDay);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ItemFields.ItemId, ComparisonOperator.Equal, this.ItemId, "ItemEntity__"));
				_departmentCollectionViaItemDay.SuppressClearInGetMulti=!forceFetch;
				_departmentCollectionViaItemDay.EntityFactoryToUse = entityFactoryToUse;
				_departmentCollectionViaItemDay.GetMulti(filter, GetRelationsForField("DepartmentCollectionViaItemDay"));
				_departmentCollectionViaItemDay.SuppressClearInGetMulti=false;
				_alreadyFetchedDepartmentCollectionViaItemDay = true;
			}
			return _departmentCollectionViaItemDay;
		}

		/// <summary> Sets the collection parameters for the collection for 'DepartmentCollectionViaItemDay'. These settings will be taken into account
		/// when the property DepartmentCollectionViaItemDay is requested or GetMultiDepartmentCollectionViaItemDay is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDepartmentCollectionViaItemDay(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_departmentCollectionViaItemDay.SortClauses=sortClauses;
			_departmentCollectionViaItemDay.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ItemDay", _itemDay);
			toReturn.Add("ItemIngredient", _itemIngredient);
			toReturn.Add("ItemModifier", _itemModifier);
			toReturn.Add("ItemPrice", _itemPrice);
			toReturn.Add("ItemPrinter", _itemPrinter);
			toReturn.Add("DepartmentCollectionViaItemDay", _departmentCollectionViaItemDay);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		/// <param name="validator">The validator object for this ItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Guid itemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(itemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_itemDay = new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection();
			_itemDay.SetContainingEntityInfo(this, "Item");

			_itemIngredient = new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection();
			_itemIngredient.SetContainingEntityInfo(this, "Item");

			_itemModifier = new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection();
			_itemModifier.SetContainingEntityInfo(this, "Item");

			_itemPrice = new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection();
			_itemPrice.SetContainingEntityInfo(this, "Item");

			_itemPrinter = new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection();
			_itemPrinter.SetContainingEntityInfo(this, "Item");
			_departmentCollectionViaItemDay = new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.DepartmentCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RegionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ItemName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ItemDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Department", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Upc", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptDesc", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintDoubleWide", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintAltColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ItemCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlternateItem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FireDelay", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsModifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderPriority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsStoreChargeable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AskForPrice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TogoSurcharge", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ItemCost", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsScaleable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsNotTippable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceIsNegative", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPromoItem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BergPlu", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifierFollowsParent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifierDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseModifierMaxSelect", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifierMaxSelect", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseModifierMinSelect", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifierMinSelect", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowModifierMaxBypass", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UsePizzaStyle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTimedItem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimingIncrement", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinimumPrice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Vducolor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultCourse", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChineseOutput", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripleHigh", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QuadHigh", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscountFlags", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxFlags", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RemotePrinters", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowZeroPrice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifierPriceLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifierCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IngredientCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrepTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MultModPrice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MultAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MultRound", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsNotGratable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifierSortType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsUsedOnline", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebThumbPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebImagePath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayIndex", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModDisplayOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OverridePrice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SwappedDept", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxSelectionAllowed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsShippable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShipWidth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShipHeight", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShipLength", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddPrepTimeToParent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Vduid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HhcolumnCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebDepartmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AskId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QuantityMultiplier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsModifierGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifierPriceRounding", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="itemId">PK value for Item which data should be fetched into this Item object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Guid itemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ItemFieldIndex.ItemId].ForcedCurrentValueWrite(itemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ItemRelations Relations
		{
			get	{ return new ItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ItemDay' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathItemDay
		{
			get { return new PrefetchPathElement(new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection(), (IEntityRelation)GetRelationsForField("ItemDay")[0], (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity, (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemDayEntity, 0, null, null, null, "ItemDay", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ItemIngredient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathItemIngredient
		{
			get { return new PrefetchPathElement(new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection(), (IEntityRelation)GetRelationsForField("ItemIngredient")[0], (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity, (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemIngredientEntity, 0, null, null, null, "ItemIngredient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ItemModifier' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathItemModifier
		{
			get { return new PrefetchPathElement(new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection(), (IEntityRelation)GetRelationsForField("ItemModifier")[0], (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity, (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemModifierEntity, 0, null, null, null, "ItemModifier", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ItemPrice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathItemPrice
		{
			get { return new PrefetchPathElement(new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection(), (IEntityRelation)GetRelationsForField("ItemPrice")[0], (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity, (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPriceEntity, 0, null, null, null, "ItemPrice", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ItemPrinter' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathItemPrinter
		{
			get { return new PrefetchPathElement(new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection(), (IEntityRelation)GetRelationsForField("ItemPrinter")[0], (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity, (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPrinterEntity, 0, null, null, null, "ItemPrinter", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Department'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDepartmentCollectionViaItemDay
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ItemDayEntityUsingItemId;
				intermediateRelation.SetAliases(string.Empty, "ItemDay_");
				return new PrefetchPathElement(new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.DepartmentCollection(), intermediateRelation,	(int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity, (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.DepartmentEntity, 0, null, null, GetRelationsForField("DepartmentCollectionViaItemDay"), "DepartmentCollectionViaItemDay", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ItemId property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ItemID"<br/>
		/// Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid ItemId
		{
			get { return (System.Guid)GetValue((int)ItemFieldIndex.ItemId, true); }
			set	{ SetValue((int)ItemFieldIndex.ItemId, value, true); }
		}

		/// <summary> The RegionId property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."RegionID"<br/>
		/// Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Guid RegionId
		{
			get { return (System.Guid)GetValue((int)ItemFieldIndex.RegionId, true); }
			set	{ SetValue((int)ItemFieldIndex.RegionId, value, true); }
		}

		/// <summary> The ItemName property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ItemName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ItemName
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.ItemName, true); }
			set	{ SetValue((int)ItemFieldIndex.ItemName, value, true); }
		}

		/// <summary> The ItemDescription property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ItemDescription"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ItemDescription
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.ItemDescription, true); }
			set	{ SetValue((int)ItemFieldIndex.ItemDescription, value, true); }
		}

		/// <summary> The Department property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."Department"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Department
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.Department, true); }
			set	{ SetValue((int)ItemFieldIndex.Department, value, true); }
		}

		/// <summary> The Upc property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."UPC"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Upc
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.Upc, true); }
			set	{ SetValue((int)ItemFieldIndex.Upc, value, true); }
		}

		/// <summary> The ReceiptDesc property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ReceiptDesc"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptDesc
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.ReceiptDesc, true); }
			set	{ SetValue((int)ItemFieldIndex.ReceiptDesc, value, true); }
		}

		/// <summary> The PrintDoubleWide property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."PrintDoubleWide"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PrintDoubleWide
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.PrintDoubleWide, true); }
			set	{ SetValue((int)ItemFieldIndex.PrintDoubleWide, value, true); }
		}

		/// <summary> The PrintAltColor property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."PrintAltColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PrintAltColor
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.PrintAltColor, true); }
			set	{ SetValue((int)ItemFieldIndex.PrintAltColor, value, true); }
		}

		/// <summary> The ItemCount property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ItemCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 ItemCount
		{
			get { return (System.Int16)GetValue((int)ItemFieldIndex.ItemCount, true); }
			set	{ SetValue((int)ItemFieldIndex.ItemCount, value, true); }
		}

		/// <summary> The AlternateItem property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."AlternateItem"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String AlternateItem
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.AlternateItem, true); }
			set	{ SetValue((int)ItemFieldIndex.AlternateItem, value, true); }
		}

		/// <summary> The FireDelay property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."FireDelay"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 FireDelay
		{
			get { return (System.Int16)GetValue((int)ItemFieldIndex.FireDelay, true); }
			set	{ SetValue((int)ItemFieldIndex.FireDelay, value, true); }
		}

		/// <summary> The IsModifier property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsModifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsModifier
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsModifier, true); }
			set	{ SetValue((int)ItemFieldIndex.IsModifier, value, true); }
		}

		/// <summary> The OrderPriority property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."OrderPriority"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte OrderPriority
		{
			get { return (System.Byte)GetValue((int)ItemFieldIndex.OrderPriority, true); }
			set	{ SetValue((int)ItemFieldIndex.OrderPriority, value, true); }
		}

		/// <summary> The IsStoreChargeable property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsStoreChargeable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsStoreChargeable
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsStoreChargeable, true); }
			set	{ SetValue((int)ItemFieldIndex.IsStoreChargeable, value, true); }
		}

		/// <summary> The AskForPrice property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."AskForPrice"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AskForPrice
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.AskForPrice, true); }
			set	{ SetValue((int)ItemFieldIndex.AskForPrice, value, true); }
		}

		/// <summary> The TogoSurcharge property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."TogoSurcharge"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TogoSurcharge
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.TogoSurcharge, true); }
			set	{ SetValue((int)ItemFieldIndex.TogoSurcharge, value, true); }
		}

		/// <summary> The ItemCost property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ItemCost"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ItemCost
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.ItemCost, true); }
			set	{ SetValue((int)ItemFieldIndex.ItemCost, value, true); }
		}

		/// <summary> The IsScaleable property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsScaleable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsScaleable
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsScaleable, true); }
			set	{ SetValue((int)ItemFieldIndex.IsScaleable, value, true); }
		}

		/// <summary> The IsNotTippable property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsNotTippable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsNotTippable
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsNotTippable, true); }
			set	{ SetValue((int)ItemFieldIndex.IsNotTippable, value, true); }
		}

		/// <summary> The PriceIsNegative property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."PriceIsNegative"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte PriceIsNegative
		{
			get { return (System.Byte)GetValue((int)ItemFieldIndex.PriceIsNegative, true); }
			set	{ SetValue((int)ItemFieldIndex.PriceIsNegative, value, true); }
		}

		/// <summary> The IsPromoItem property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsPromoItem"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPromoItem
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsPromoItem, true); }
			set	{ SetValue((int)ItemFieldIndex.IsPromoItem, value, true); }
		}

		/// <summary> The BergPlu property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."BergPLU"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BergPlu
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.BergPlu, true); }
			set	{ SetValue((int)ItemFieldIndex.BergPlu, value, true); }
		}

		/// <summary> The ModifierFollowsParent property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModifierFollowsParent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ModifierFollowsParent
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.ModifierFollowsParent, true); }
			set	{ SetValue((int)ItemFieldIndex.ModifierFollowsParent, value, true); }
		}

		/// <summary> The ModifierDescription property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModifierDescription"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ModifierDescription
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.ModifierDescription, true); }
			set	{ SetValue((int)ItemFieldIndex.ModifierDescription, value, true); }
		}

		/// <summary> The UseModifierMaxSelect property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."UseModifierMaxSelect"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseModifierMaxSelect
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.UseModifierMaxSelect, true); }
			set	{ SetValue((int)ItemFieldIndex.UseModifierMaxSelect, value, true); }
		}

		/// <summary> The ModifierMaxSelect property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModifierMaxSelect"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 ModifierMaxSelect
		{
			get { return (System.Int16)GetValue((int)ItemFieldIndex.ModifierMaxSelect, true); }
			set	{ SetValue((int)ItemFieldIndex.ModifierMaxSelect, value, true); }
		}

		/// <summary> The UseModifierMinSelect property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."UseModifierMinSelect"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseModifierMinSelect
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.UseModifierMinSelect, true); }
			set	{ SetValue((int)ItemFieldIndex.UseModifierMinSelect, value, true); }
		}

		/// <summary> The ModifierMinSelect property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModifierMinSelect"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 ModifierMinSelect
		{
			get { return (System.Int16)GetValue((int)ItemFieldIndex.ModifierMinSelect, true); }
			set	{ SetValue((int)ItemFieldIndex.ModifierMinSelect, value, true); }
		}

		/// <summary> The AllowModifierMaxBypass property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."AllowModifierMaxBypass"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowModifierMaxBypass
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.AllowModifierMaxBypass, true); }
			set	{ SetValue((int)ItemFieldIndex.AllowModifierMaxBypass, value, true); }
		}

		/// <summary> The UsePizzaStyle property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."UsePizzaStyle"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UsePizzaStyle
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.UsePizzaStyle, true); }
			set	{ SetValue((int)ItemFieldIndex.UsePizzaStyle, value, true); }
		}

		/// <summary> The IsTimedItem property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsTimedItem"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTimedItem
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsTimedItem, true); }
			set	{ SetValue((int)ItemFieldIndex.IsTimedItem, value, true); }
		}

		/// <summary> The TimingIncrement property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."TimingIncrement"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte TimingIncrement
		{
			get { return (System.Byte)GetValue((int)ItemFieldIndex.TimingIncrement, true); }
			set	{ SetValue((int)ItemFieldIndex.TimingIncrement, value, true); }
		}

		/// <summary> The MinimumPrice property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."MinimumPrice"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinimumPrice
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.MinimumPrice, true); }
			set	{ SetValue((int)ItemFieldIndex.MinimumPrice, value, true); }
		}

		/// <summary> The Vducolor property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."VDUColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte Vducolor
		{
			get { return (System.Byte)GetValue((int)ItemFieldIndex.Vducolor, true); }
			set	{ SetValue((int)ItemFieldIndex.Vducolor, value, true); }
		}

		/// <summary> The ShortDescription property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ShortDescription"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 5<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortDescription
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.ShortDescription, true); }
			set	{ SetValue((int)ItemFieldIndex.ShortDescription, value, true); }
		}

		/// <summary> The DefaultCourse property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."DefaultCourse"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte DefaultCourse
		{
			get { return (System.Byte)GetValue((int)ItemFieldIndex.DefaultCourse, true); }
			set	{ SetValue((int)ItemFieldIndex.DefaultCourse, value, true); }
		}

		/// <summary> The ChineseOutput property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ChineseOutput"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ChineseOutput
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.ChineseOutput, true); }
			set	{ SetValue((int)ItemFieldIndex.ChineseOutput, value, true); }
		}

		/// <summary> The TripleHigh property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."TripleHigh"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TripleHigh
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.TripleHigh, true); }
			set	{ SetValue((int)ItemFieldIndex.TripleHigh, value, true); }
		}

		/// <summary> The QuadHigh property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."QuadHigh"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 QuadHigh
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.QuadHigh, true); }
			set	{ SetValue((int)ItemFieldIndex.QuadHigh, value, true); }
		}

		/// <summary> The DiscountFlags property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."DiscountFlags"<br/>
		/// Table field type characteristics (type, precision, scale, length): Binary, 0, 0, 13<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] DiscountFlags
		{
			get { return (System.Byte[])GetValue((int)ItemFieldIndex.DiscountFlags, true); }
			set	{ SetValue((int)ItemFieldIndex.DiscountFlags, value, true); }
		}

		/// <summary> The TaxFlags property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."TaxFlags"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte TaxFlags
		{
			get { return (System.Byte)GetValue((int)ItemFieldIndex.TaxFlags, true); }
			set	{ SetValue((int)ItemFieldIndex.TaxFlags, value, true); }
		}

		/// <summary> The PrintOptions property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."PrintOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte PrintOptions
		{
			get { return (System.Byte)GetValue((int)ItemFieldIndex.PrintOptions, true); }
			set	{ SetValue((int)ItemFieldIndex.PrintOptions, value, true); }
		}

		/// <summary> The RemotePrinters property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."RemotePrinters"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RemotePrinters
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.RemotePrinters, true); }
			set	{ SetValue((int)ItemFieldIndex.RemotePrinters, value, true); }
		}

		/// <summary> The AllowZeroPrice property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."AllowZeroPrice"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowZeroPrice
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.AllowZeroPrice, true); }
			set	{ SetValue((int)ItemFieldIndex.AllowZeroPrice, value, true); }
		}

		/// <summary> The ModifierPriceLevel property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModifierPriceLevel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ModifierPriceLevel
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.ModifierPriceLevel, true); }
			set	{ SetValue((int)ItemFieldIndex.ModifierPriceLevel, value, true); }
		}

		/// <summary> The ModifierCount property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModifierCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ModifierCount
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.ModifierCount, true); }
			set	{ SetValue((int)ItemFieldIndex.ModifierCount, value, true); }
		}

		/// <summary> The IngredientCount property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IngredientCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IngredientCount
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.IngredientCount, true); }
			set	{ SetValue((int)ItemFieldIndex.IngredientCount, value, true); }
		}

		/// <summary> The PrepTime property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."PrepTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PrepTime
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.PrepTime, true); }
			set	{ SetValue((int)ItemFieldIndex.PrepTime, value, true); }
		}

		/// <summary> The MultModPrice property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."MultModPrice"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MultModPrice
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.MultModPrice, true); }
			set	{ SetValue((int)ItemFieldIndex.MultModPrice, value, true); }
		}

		/// <summary> The MultAmount property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."MultAmount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MultAmount
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.MultAmount, true); }
			set	{ SetValue((int)ItemFieldIndex.MultAmount, value, true); }
		}

		/// <summary> The MultRound property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."MultRound"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MultRound
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.MultRound, true); }
			set	{ SetValue((int)ItemFieldIndex.MultRound, value, true); }
		}

		/// <summary> The IsNotGratable property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsNotGratable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsNotGratable
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsNotGratable, true); }
			set	{ SetValue((int)ItemFieldIndex.IsNotGratable, value, true); }
		}

		/// <summary> The ModifierSortType property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModifierSortType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ModifierSortType
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.ModifierSortType, true); }
			set	{ SetValue((int)ItemFieldIndex.ModifierSortType, value, true); }
		}

		/// <summary> The IsUsedOnline property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsUsedOnline"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsUsedOnline
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsUsedOnline, true); }
			set	{ SetValue((int)ItemFieldIndex.IsUsedOnline, value, true); }
		}

		/// <summary> The WebTitle property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."WebTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebTitle
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.WebTitle, true); }
			set	{ SetValue((int)ItemFieldIndex.WebTitle, value, true); }
		}

		/// <summary> The WebDescription property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."WebDescription"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebDescription
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.WebDescription, true); }
			set	{ SetValue((int)ItemFieldIndex.WebDescription, value, true); }
		}

		/// <summary> The WebThumbPath property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."WebThumbPath"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 254<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebThumbPath
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.WebThumbPath, true); }
			set	{ SetValue((int)ItemFieldIndex.WebThumbPath, value, true); }
		}

		/// <summary> The WebImagePath property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."WebImagePath"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 254<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebImagePath
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.WebImagePath, true); }
			set	{ SetValue((int)ItemFieldIndex.WebImagePath, value, true); }
		}

		/// <summary> The DisplayIndex property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."DisplayIndex"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DisplayIndex
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.DisplayIndex, true); }
			set	{ SetValue((int)ItemFieldIndex.DisplayIndex, value, true); }
		}

		/// <summary> The ModDisplayOrder property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModDisplayOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ModDisplayOrder
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.ModDisplayOrder, true); }
			set	{ SetValue((int)ItemFieldIndex.ModDisplayOrder, value, true); }
		}

		/// <summary> The OverridePrice property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."OverridePrice"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OverridePrice
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.OverridePrice, true); }
			set	{ SetValue((int)ItemFieldIndex.OverridePrice, value, true); }
		}

		/// <summary> The SwappedDept property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."SwappedDept"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SwappedDept
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.SwappedDept, true); }
			set	{ SetValue((int)ItemFieldIndex.SwappedDept, value, true); }
		}

		/// <summary> The MaxSelectionAllowed property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."MaxSelectionAllowed"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxSelectionAllowed
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.MaxSelectionAllowed, true); }
			set	{ SetValue((int)ItemFieldIndex.MaxSelectionAllowed, value, true); }
		}

		/// <summary> The IsShippable property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsShippable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsShippable
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsShippable, true); }
			set	{ SetValue((int)ItemFieldIndex.IsShippable, value, true); }
		}

		/// <summary> The ShipWidth property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ShipWidth"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 5<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShipWidth
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.ShipWidth, true); }
			set	{ SetValue((int)ItemFieldIndex.ShipWidth, value, true); }
		}

		/// <summary> The ShipHeight property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ShipHeight"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 5<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShipHeight
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.ShipHeight, true); }
			set	{ SetValue((int)ItemFieldIndex.ShipHeight, value, true); }
		}

		/// <summary> The ShipLength property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ShipLength"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 5<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShipLength
		{
			get { return (System.String)GetValue((int)ItemFieldIndex.ShipLength, true); }
			set	{ SetValue((int)ItemFieldIndex.ShipLength, value, true); }
		}

		/// <summary> The AddPrepTimeToParent property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."AddPrepTimeToParent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AddPrepTimeToParent
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.AddPrepTimeToParent, true); }
			set	{ SetValue((int)ItemFieldIndex.AddPrepTimeToParent, value, true); }
		}

		/// <summary> The Vduid property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."VDUID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Vduid
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.Vduid, true); }
			set	{ SetValue((int)ItemFieldIndex.Vduid, value, true); }
		}

		/// <summary> The HhcolumnCount property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."HHColumnCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HhcolumnCount
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.HhcolumnCount, true); }
			set	{ SetValue((int)ItemFieldIndex.HhcolumnCount, value, true); }
		}

		/// <summary> The WebDepartmentId property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."WebDepartmentID"<br/>
		/// Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Guid> WebDepartmentId
		{
			get { return (Nullable<System.Guid>)GetValue((int)ItemFieldIndex.WebDepartmentId, false); }
			set	{ SetValue((int)ItemFieldIndex.WebDepartmentId, value, true); }
		}

		/// <summary> The AskId property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."AskID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AskId
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.AskId, true); }
			set	{ SetValue((int)ItemFieldIndex.AskId, value, true); }
		}

		/// <summary> The QuantityMultiplier property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."QuantityMultiplier"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 QuantityMultiplier
		{
			get { return (System.Int32)GetValue((int)ItemFieldIndex.QuantityMultiplier, true); }
			set	{ SetValue((int)ItemFieldIndex.QuantityMultiplier, value, true); }
		}

		/// <summary> The IsModifierGroup property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."IsModifierGroup"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsModifierGroup
		{
			get { return (System.Boolean)GetValue((int)ItemFieldIndex.IsModifierGroup, true); }
			set	{ SetValue((int)ItemFieldIndex.IsModifierGroup, value, true); }
		}

		/// <summary> The ModifierPriceRounding property of the Entity Item<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Item"."ModifierPriceRounding"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte ModifierPriceRounding
		{
			get { return (System.Byte)GetValue((int)ItemFieldIndex.ModifierPriceRounding, true); }
			set	{ SetValue((int)ItemFieldIndex.ModifierPriceRounding, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiItemDay()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection ItemDay
		{
			get	{ return GetMultiItemDay(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ItemDay. When set to true, ItemDay is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ItemDay is accessed. You can always execute/ a forced fetch by calling GetMultiItemDay(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchItemDay
		{
			get	{ return _alwaysFetchItemDay; }
			set	{ _alwaysFetchItemDay = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ItemDay already has been fetched. Setting this property to false when ItemDay has been fetched
		/// will clear the ItemDay collection well. Setting this property to true while ItemDay hasn't been fetched disables lazy loading for ItemDay</summary>
		[Browsable(false)]
		public bool AlreadyFetchedItemDay
		{
			get { return _alreadyFetchedItemDay;}
			set 
			{
				if(_alreadyFetchedItemDay && !value && (_itemDay != null))
				{
					_itemDay.Clear();
				}
				_alreadyFetchedItemDay = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ItemIngredientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiItemIngredient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemIngredientCollection ItemIngredient
		{
			get	{ return GetMultiItemIngredient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ItemIngredient. When set to true, ItemIngredient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ItemIngredient is accessed. You can always execute/ a forced fetch by calling GetMultiItemIngredient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchItemIngredient
		{
			get	{ return _alwaysFetchItemIngredient; }
			set	{ _alwaysFetchItemIngredient = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ItemIngredient already has been fetched. Setting this property to false when ItemIngredient has been fetched
		/// will clear the ItemIngredient collection well. Setting this property to true while ItemIngredient hasn't been fetched disables lazy loading for ItemIngredient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedItemIngredient
		{
			get { return _alreadyFetchedItemIngredient;}
			set 
			{
				if(_alreadyFetchedItemIngredient && !value && (_itemIngredient != null))
				{
					_itemIngredient.Clear();
				}
				_alreadyFetchedItemIngredient = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ItemModifierEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiItemModifier()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemModifierCollection ItemModifier
		{
			get	{ return GetMultiItemModifier(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ItemModifier. When set to true, ItemModifier is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ItemModifier is accessed. You can always execute/ a forced fetch by calling GetMultiItemModifier(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchItemModifier
		{
			get	{ return _alwaysFetchItemModifier; }
			set	{ _alwaysFetchItemModifier = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ItemModifier already has been fetched. Setting this property to false when ItemModifier has been fetched
		/// will clear the ItemModifier collection well. Setting this property to true while ItemModifier hasn't been fetched disables lazy loading for ItemModifier</summary>
		[Browsable(false)]
		public bool AlreadyFetchedItemModifier
		{
			get { return _alreadyFetchedItemModifier;}
			set 
			{
				if(_alreadyFetchedItemModifier && !value && (_itemModifier != null))
				{
					_itemModifier.Clear();
				}
				_alreadyFetchedItemModifier = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ItemPriceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiItemPrice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPriceCollection ItemPrice
		{
			get	{ return GetMultiItemPrice(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ItemPrice. When set to true, ItemPrice is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ItemPrice is accessed. You can always execute/ a forced fetch by calling GetMultiItemPrice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchItemPrice
		{
			get	{ return _alwaysFetchItemPrice; }
			set	{ _alwaysFetchItemPrice = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ItemPrice already has been fetched. Setting this property to false when ItemPrice has been fetched
		/// will clear the ItemPrice collection well. Setting this property to true while ItemPrice hasn't been fetched disables lazy loading for ItemPrice</summary>
		[Browsable(false)]
		public bool AlreadyFetchedItemPrice
		{
			get { return _alreadyFetchedItemPrice;}
			set 
			{
				if(_alreadyFetchedItemPrice && !value && (_itemPrice != null))
				{
					_itemPrice.Clear();
				}
				_alreadyFetchedItemPrice = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ItemPrinterEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiItemPrinter()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemPrinterCollection ItemPrinter
		{
			get	{ return GetMultiItemPrinter(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ItemPrinter. When set to true, ItemPrinter is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ItemPrinter is accessed. You can always execute/ a forced fetch by calling GetMultiItemPrinter(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchItemPrinter
		{
			get	{ return _alwaysFetchItemPrinter; }
			set	{ _alwaysFetchItemPrinter = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ItemPrinter already has been fetched. Setting this property to false when ItemPrinter has been fetched
		/// will clear the ItemPrinter collection well. Setting this property to true while ItemPrinter hasn't been fetched disables lazy loading for ItemPrinter</summary>
		[Browsable(false)]
		public bool AlreadyFetchedItemPrinter
		{
			get { return _alreadyFetchedItemPrinter;}
			set 
			{
				if(_alreadyFetchedItemPrinter && !value && (_itemPrinter != null))
				{
					_itemPrinter.Clear();
				}
				_alreadyFetchedItemPrinter = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DepartmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDepartmentCollectionViaItemDay()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.DepartmentCollection DepartmentCollectionViaItemDay
		{
			get { return GetMultiDepartmentCollectionViaItemDay(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DepartmentCollectionViaItemDay. When set to true, DepartmentCollectionViaItemDay is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DepartmentCollectionViaItemDay is accessed. You can always execute a forced fetch by calling GetMultiDepartmentCollectionViaItemDay(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDepartmentCollectionViaItemDay
		{
			get	{ return _alwaysFetchDepartmentCollectionViaItemDay; }
			set	{ _alwaysFetchDepartmentCollectionViaItemDay = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DepartmentCollectionViaItemDay already has been fetched. Setting this property to false when DepartmentCollectionViaItemDay has been fetched
		/// will clear the DepartmentCollectionViaItemDay collection well. Setting this property to true while DepartmentCollectionViaItemDay hasn't been fetched disables lazy loading for DepartmentCollectionViaItemDay</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDepartmentCollectionViaItemDay
		{
			get { return _alreadyFetchedDepartmentCollectionViaItemDay;}
			set 
			{
				if(_alreadyFetchedDepartmentCollectionViaItemDay && !value && (_departmentCollectionViaItemDay != null))
				{
					_departmentCollectionViaItemDay.Clear();
				}
				_alreadyFetchedDepartmentCollectionViaItemDay = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Logic.Pos.FuturePos.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
