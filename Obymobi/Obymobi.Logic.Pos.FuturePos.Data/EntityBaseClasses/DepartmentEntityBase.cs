﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Logic.Pos.FuturePos.Data;
using Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses;
using Obymobi.Logic.Pos.FuturePos.Data.DaoClasses;
using Obymobi.Logic.Pos.FuturePos.Data.RelationClasses;
using Obymobi.Logic.Pos.FuturePos.Data.HelperClasses;
using Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Department'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DepartmentEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public string LLBLGenProEntityName {
			get { return "DepartmentEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection	_itemDay;
		private bool	_alwaysFetchItemDay, _alreadyFetchedItemDay;
		private Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection _itemCollectionViaItemDay;
		private bool	_alwaysFetchItemCollectionViaItemDay, _alreadyFetchedItemCollectionViaItemDay;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ItemDay</summary>
			public static readonly string ItemDay = "ItemDay";
			/// <summary>Member name ItemCollectionViaItemDay</summary>
			public static readonly string ItemCollectionViaItemDay = "ItemCollectionViaItemDay";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DepartmentEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected DepartmentEntityBase() :base("DepartmentEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		protected DepartmentEntityBase(System.Guid departmentId):base("DepartmentEntity")
		{
			InitClassFetch(departmentId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected DepartmentEntityBase(System.Guid departmentId, IPrefetchPath prefetchPathToUse): base("DepartmentEntity")
		{
			InitClassFetch(departmentId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		/// <param name="validator">The custom validator object for this DepartmentEntity</param>
		protected DepartmentEntityBase(System.Guid departmentId, IValidator validator):base("DepartmentEntity")
		{
			InitClassFetch(departmentId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DepartmentEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_itemDay = (Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection)info.GetValue("_itemDay", typeof(Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection));
			_alwaysFetchItemDay = info.GetBoolean("_alwaysFetchItemDay");
			_alreadyFetchedItemDay = info.GetBoolean("_alreadyFetchedItemDay");
			_itemCollectionViaItemDay = (Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection)info.GetValue("_itemCollectionViaItemDay", typeof(Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection));
			_alwaysFetchItemCollectionViaItemDay = info.GetBoolean("_alwaysFetchItemCollectionViaItemDay");
			_alreadyFetchedItemCollectionViaItemDay = info.GetBoolean("_alreadyFetchedItemCollectionViaItemDay");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedItemDay = (_itemDay.Count > 0);
			_alreadyFetchedItemCollectionViaItemDay = (_itemCollectionViaItemDay.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ItemDay":
					toReturn.Add(Relations.ItemDayEntityUsingDepartmentId);
					break;
				case "ItemCollectionViaItemDay":
					toReturn.Add(Relations.ItemDayEntityUsingDepartmentId, "DepartmentEntity__", "ItemDay_", JoinHint.None);
					toReturn.Add(ItemDayEntity.Relations.ItemEntityUsingItemId, "ItemDay_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_itemDay", (!this.MarkedForDeletion?_itemDay:null));
			info.AddValue("_alwaysFetchItemDay", _alwaysFetchItemDay);
			info.AddValue("_alreadyFetchedItemDay", _alreadyFetchedItemDay);
			info.AddValue("_itemCollectionViaItemDay", (!this.MarkedForDeletion?_itemCollectionViaItemDay:null));
			info.AddValue("_alwaysFetchItemCollectionViaItemDay", _alwaysFetchItemCollectionViaItemDay);
			info.AddValue("_alreadyFetchedItemCollectionViaItemDay", _alreadyFetchedItemCollectionViaItemDay);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ItemDay":
					_alreadyFetchedItemDay = true;
					if(entity!=null)
					{
						this.ItemDay.Add((ItemDayEntity)entity);
					}
					break;
				case "ItemCollectionViaItemDay":
					_alreadyFetchedItemCollectionViaItemDay = true;
					if(entity!=null)
					{
						this.ItemCollectionViaItemDay.Add((ItemEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ItemDay":
					_itemDay.Add((ItemDayEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ItemDay":
					this.PerformRelatedEntityRemoval(_itemDay, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_itemDay);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Guid departmentId)
		{
			return FetchUsingPK(departmentId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Guid departmentId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(departmentId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Guid departmentId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(departmentId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Guid departmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(departmentId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DepartmentId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DepartmentRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ItemDayEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection GetMultiItemDay(bool forceFetch)
		{
			return GetMultiItemDay(forceFetch, _itemDay.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ItemDayEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection GetMultiItemDay(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiItemDay(forceFetch, _itemDay.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection GetMultiItemDay(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiItemDay(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection GetMultiItemDay(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedItemDay || forceFetch || _alwaysFetchItemDay) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_itemDay);
				_itemDay.SuppressClearInGetMulti=!forceFetch;
				_itemDay.EntityFactoryToUse = entityFactoryToUse;
				_itemDay.GetMultiManyToOne(this, null, filter);
				_itemDay.SuppressClearInGetMulti=false;
				_alreadyFetchedItemDay = true;
			}
			return _itemDay;
		}

		/// <summary> Sets the collection parameters for the collection for 'ItemDay'. These settings will be taken into account
		/// when the property ItemDay is requested or GetMultiItemDay is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersItemDay(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_itemDay.SortClauses=sortClauses;
			_itemDay.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ItemEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ItemEntity'</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection GetMultiItemCollectionViaItemDay(bool forceFetch)
		{
			return GetMultiItemCollectionViaItemDay(forceFetch, _itemCollectionViaItemDay.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ItemEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection GetMultiItemCollectionViaItemDay(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedItemCollectionViaItemDay || forceFetch || _alwaysFetchItemCollectionViaItemDay) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_itemCollectionViaItemDay);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DepartmentFields.DepartmentId, ComparisonOperator.Equal, this.DepartmentId, "DepartmentEntity__"));
				_itemCollectionViaItemDay.SuppressClearInGetMulti=!forceFetch;
				_itemCollectionViaItemDay.EntityFactoryToUse = entityFactoryToUse;
				_itemCollectionViaItemDay.GetMulti(filter, GetRelationsForField("ItemCollectionViaItemDay"));
				_itemCollectionViaItemDay.SuppressClearInGetMulti=false;
				_alreadyFetchedItemCollectionViaItemDay = true;
			}
			return _itemCollectionViaItemDay;
		}

		/// <summary> Sets the collection parameters for the collection for 'ItemCollectionViaItemDay'. These settings will be taken into account
		/// when the property ItemCollectionViaItemDay is requested or GetMultiItemCollectionViaItemDay is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersItemCollectionViaItemDay(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_itemCollectionViaItemDay.SortClauses=sortClauses;
			_itemCollectionViaItemDay.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ItemDay", _itemDay);
			toReturn.Add("ItemCollectionViaItemDay", _itemCollectionViaItemDay);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		/// <param name="validator">The validator object for this DepartmentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Guid departmentId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(departmentId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_itemDay = new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection();
			_itemDay.SetContainingEntityInfo(this, "Department");
			_itemCollectionViaItemDay = new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepartmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RegionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepartmentName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepartmentDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsHash", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsbucketNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsUsedOnline", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebThumbPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebImagePath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDefault", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayIndex", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsParent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentDepartmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Vduid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AskId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QuantityMultiplier", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="departmentId">PK value for Department which data should be fetched into this Department object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Guid departmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DepartmentFieldIndex.DepartmentId].ForcedCurrentValueWrite(departmentId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDepartmentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DepartmentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DepartmentRelations Relations
		{
			get	{ return new DepartmentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ItemDay' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathItemDay
		{
			get { return new PrefetchPathElement(new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection(), (IEntityRelation)GetRelationsForField("ItemDay")[0], (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.DepartmentEntity, (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemDayEntity, 0, null, null, null, "ItemDay", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Item'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathItemCollectionViaItemDay
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ItemDayEntityUsingDepartmentId;
				intermediateRelation.SetAliases(string.Empty, "ItemDay_");
				return new PrefetchPathElement(new Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection(), intermediateRelation,	(int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.DepartmentEntity, (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity, 0, null, null, GetRelationsForField("ItemCollectionViaItemDay"), "ItemCollectionViaItemDay", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DepartmentId property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."DepartmentID"<br/>
		/// Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid DepartmentId
		{
			get { return (System.Guid)GetValue((int)DepartmentFieldIndex.DepartmentId, true); }
			set	{ SetValue((int)DepartmentFieldIndex.DepartmentId, value, true); }
		}

		/// <summary> The RegionId property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."RegionID"<br/>
		/// Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Guid RegionId
		{
			get { return (System.Guid)GetValue((int)DepartmentFieldIndex.RegionId, true); }
			set	{ SetValue((int)DepartmentFieldIndex.RegionId, value, true); }
		}

		/// <summary> The DepartmentName property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."DepartmentName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DepartmentName
		{
			get { return (System.String)GetValue((int)DepartmentFieldIndex.DepartmentName, true); }
			set	{ SetValue((int)DepartmentFieldIndex.DepartmentName, value, true); }
		}

		/// <summary> The DepartmentDescription property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."DepartmentDescription"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DepartmentDescription
		{
			get { return (System.String)GetValue((int)DepartmentFieldIndex.DepartmentDescription, true); }
			set	{ SetValue((int)DepartmentFieldIndex.DepartmentDescription, value, true); }
		}

		/// <summary> The GroupName property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."GroupName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GroupName
		{
			get { return (System.String)GetValue((int)DepartmentFieldIndex.GroupName, true); }
			set	{ SetValue((int)DepartmentFieldIndex.GroupName, value, true); }
		}

		/// <summary> The IsHash property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."IsHash"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsHash
		{
			get { return (System.Boolean)GetValue((int)DepartmentFieldIndex.IsHash, true); }
			set	{ SetValue((int)DepartmentFieldIndex.IsHash, value, true); }
		}

		/// <summary> The PmsbucketNumber property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."PMSBucketNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte PmsbucketNumber
		{
			get { return (System.Byte)GetValue((int)DepartmentFieldIndex.PmsbucketNumber, true); }
			set	{ SetValue((int)DepartmentFieldIndex.PmsbucketNumber, value, true); }
		}

		/// <summary> The IsUsedOnline property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."IsUsedOnline"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsUsedOnline
		{
			get { return (System.Boolean)GetValue((int)DepartmentFieldIndex.IsUsedOnline, true); }
			set	{ SetValue((int)DepartmentFieldIndex.IsUsedOnline, value, true); }
		}

		/// <summary> The WebTitle property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."WebTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebTitle
		{
			get { return (System.String)GetValue((int)DepartmentFieldIndex.WebTitle, true); }
			set	{ SetValue((int)DepartmentFieldIndex.WebTitle, value, true); }
		}

		/// <summary> The WebDescription property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."WebDescription"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebDescription
		{
			get { return (System.String)GetValue((int)DepartmentFieldIndex.WebDescription, true); }
			set	{ SetValue((int)DepartmentFieldIndex.WebDescription, value, true); }
		}

		/// <summary> The WebThumbPath property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."WebThumbPath"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 254<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebThumbPath
		{
			get { return (System.String)GetValue((int)DepartmentFieldIndex.WebThumbPath, true); }
			set	{ SetValue((int)DepartmentFieldIndex.WebThumbPath, value, true); }
		}

		/// <summary> The WebImagePath property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."WebImagePath"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 254<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebImagePath
		{
			get { return (System.String)GetValue((int)DepartmentFieldIndex.WebImagePath, true); }
			set	{ SetValue((int)DepartmentFieldIndex.WebImagePath, value, true); }
		}

		/// <summary> The IsDefault property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."IsDefault"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsDefault
		{
			get { return (System.Boolean)GetValue((int)DepartmentFieldIndex.IsDefault, true); }
			set	{ SetValue((int)DepartmentFieldIndex.IsDefault, value, true); }
		}

		/// <summary> The DisplayIndex property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."DisplayIndex"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DisplayIndex
		{
			get { return (System.Int32)GetValue((int)DepartmentFieldIndex.DisplayIndex, true); }
			set	{ SetValue((int)DepartmentFieldIndex.DisplayIndex, value, true); }
		}

		/// <summary> The IsParent property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."IsParent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsParent
		{
			get { return (System.Boolean)GetValue((int)DepartmentFieldIndex.IsParent, true); }
			set	{ SetValue((int)DepartmentFieldIndex.IsParent, value, true); }
		}

		/// <summary> The ParentDepartmentId property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."ParentDepartmentID"<br/>
		/// Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Guid> ParentDepartmentId
		{
			get { return (Nullable<System.Guid>)GetValue((int)DepartmentFieldIndex.ParentDepartmentId, false); }
			set	{ SetValue((int)DepartmentFieldIndex.ParentDepartmentId, value, true); }
		}

		/// <summary> The Vduid property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."VDUID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Vduid
		{
			get { return (System.Int32)GetValue((int)DepartmentFieldIndex.Vduid, true); }
			set	{ SetValue((int)DepartmentFieldIndex.Vduid, value, true); }
		}

		/// <summary> The AskId property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."AskID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AskId
		{
			get { return (System.Boolean)GetValue((int)DepartmentFieldIndex.AskId, true); }
			set	{ SetValue((int)DepartmentFieldIndex.AskId, value, true); }
		}

		/// <summary> The QuantityMultiplier property of the Entity Department<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Department"."QuantityMultiplier"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 QuantityMultiplier
		{
			get { return (System.Int32)GetValue((int)DepartmentFieldIndex.QuantityMultiplier, true); }
			set	{ SetValue((int)DepartmentFieldIndex.QuantityMultiplier, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ItemDayEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiItemDay()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemDayCollection ItemDay
		{
			get	{ return GetMultiItemDay(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ItemDay. When set to true, ItemDay is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ItemDay is accessed. You can always execute/ a forced fetch by calling GetMultiItemDay(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchItemDay
		{
			get	{ return _alwaysFetchItemDay; }
			set	{ _alwaysFetchItemDay = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ItemDay already has been fetched. Setting this property to false when ItemDay has been fetched
		/// will clear the ItemDay collection well. Setting this property to true while ItemDay hasn't been fetched disables lazy loading for ItemDay</summary>
		[Browsable(false)]
		public bool AlreadyFetchedItemDay
		{
			get { return _alreadyFetchedItemDay;}
			set 
			{
				if(_alreadyFetchedItemDay && !value && (_itemDay != null))
				{
					_itemDay.Clear();
				}
				_alreadyFetchedItemDay = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ItemEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiItemCollectionViaItemDay()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection ItemCollectionViaItemDay
		{
			get { return GetMultiItemCollectionViaItemDay(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ItemCollectionViaItemDay. When set to true, ItemCollectionViaItemDay is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ItemCollectionViaItemDay is accessed. You can always execute a forced fetch by calling GetMultiItemCollectionViaItemDay(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchItemCollectionViaItemDay
		{
			get	{ return _alwaysFetchItemCollectionViaItemDay; }
			set	{ _alwaysFetchItemCollectionViaItemDay = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ItemCollectionViaItemDay already has been fetched. Setting this property to false when ItemCollectionViaItemDay has been fetched
		/// will clear the ItemCollectionViaItemDay collection well. Setting this property to true while ItemCollectionViaItemDay hasn't been fetched disables lazy loading for ItemCollectionViaItemDay</summary>
		[Browsable(false)]
		public bool AlreadyFetchedItemCollectionViaItemDay
		{
			get { return _alreadyFetchedItemCollectionViaItemDay;}
			set 
			{
				if(_alreadyFetchedItemCollectionViaItemDay && !value && (_itemCollectionViaItemDay != null))
				{
					_itemCollectionViaItemDay.Clear();
				}
				_alreadyFetchedItemCollectionViaItemDay = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Logic.Pos.FuturePos.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Logic.Pos.FuturePos.Data.EntityType.DepartmentEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
