﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Linq;
using System.Collections.Generic;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

using Obymobi.Logic.Pos.FuturePos.Data;
using Obymobi.Logic.Pos.FuturePos.Data.DaoClasses;
using Obymobi.Logic.Pos.FuturePos.Data.EntityClasses;
using Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses;
using Obymobi.Logic.Pos.FuturePos.Data.HelperClasses;
using Obymobi.Logic.Pos.FuturePos.Data.RelationClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.Linq
{
	/// <summary>Meta-data class for the construction of Linq queries which are to be executed using LLBLGen Pro code.</summary>
	public partial class LinqMetaData : ILinqMetaData
	{
		#region Class Member Declarations
		private ITransaction _transactionToUse;
		private FunctionMappingStore _customFunctionMappings;
		private Context _contextToUse;
		#endregion
		
		/// <summary>CTor. Using this ctor will leave the transaction object to use empty. This is ok if you're not executing queries created with this
		/// meta data inside a transaction. If you're executing the queries created with this meta-data inside a transaction, either set the Transaction property
		/// on the IQueryable.Provider instance of the created LLBLGenProQuery object prior to execution or use the ctor which accepts a transaction object.</summary>
		public LinqMetaData() : this(null, null)
		{
		}
		
		/// <summary>CTor. If you're executing the queries created with this meta-data inside a transaction, pass a live ITransaction object to this ctor.</summary>
		/// <param name="transactionToUse">the transaction to use in queries created with this meta-data</param>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(ITransaction transactionToUse) : this(transactionToUse, null)
		{
		}
		
		/// <summary>CTor. If you're executing the queries created with this meta-data inside a transaction, pass a live ITransaction object to this ctor.</summary>
		/// <param name="transactionToUse">the transaction to use in queries created with this meta-data</param>
		/// <param name="customFunctionMappings">The custom function mappings to use. These take higher precedence than the ones in the DQE to use.</param>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(ITransaction transactionToUse, FunctionMappingStore customFunctionMappings)
		{
			_transactionToUse = transactionToUse;
			_customFunctionMappings = customFunctionMappings;
		}
		
		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <param name="typeOfEntity">the type of the entity to get the datasource for</param>
		/// <returns>the requested datasource</returns>
		public IDataSource GetQueryableForEntity(int typeOfEntity)
		{
			IDataSource toReturn = null;
			switch((Obymobi.Logic.Pos.FuturePos.Data.EntityType)typeOfEntity)
			{
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.DepartmentEntity:
					toReturn = this.Department;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemEntity:
					toReturn = this.Item;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemDayEntity:
					toReturn = this.ItemDay;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemIngredientEntity:
					toReturn = this.ItemIngredient;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemModifierEntity:
					toReturn = this.ItemModifier;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPriceEntity:
					toReturn = this.ItemPrice;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.ItemPrinterEntity:
					toReturn = this.ItemPrinter;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.LayoutRoomEntity:
					toReturn = this.LayoutRoom;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.LayoutTableEntity:
					toReturn = this.LayoutTable;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.PriceLevelEntity:
					toReturn = this.PriceLevel;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.PriceScheduleEntity:
					toReturn = this.PriceSchedule;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.VendorEntity:
					toReturn = this.Vendor;
					break;
				case Obymobi.Logic.Pos.FuturePos.Data.EntityType.VersionEntity:
					toReturn = this.Version;
					break;
				default:
					toReturn = null;
					break;
			}
			return toReturn;
		}

		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <typeparam name="TEntity">the type of the entity to get the datasource for</typeparam>
		/// <returns>the requested datasource</returns>
		public DataSource<TEntity> GetQueryableForEntity<TEntity>()
			    where TEntity : class
		{
    		return new DataSource<TEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse);
		}

		/// <summary>returns the datasource to use in a Linq query when targeting DepartmentEntity instances in the database.</summary>
		public DataSource<DepartmentEntity> Department
		{
			get { return new DataSource<DepartmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ItemEntity instances in the database.</summary>
		public DataSource<ItemEntity> Item
		{
			get { return new DataSource<ItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ItemDayEntity instances in the database.</summary>
		public DataSource<ItemDayEntity> ItemDay
		{
			get { return new DataSource<ItemDayEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ItemIngredientEntity instances in the database.</summary>
		public DataSource<ItemIngredientEntity> ItemIngredient
		{
			get { return new DataSource<ItemIngredientEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ItemModifierEntity instances in the database.</summary>
		public DataSource<ItemModifierEntity> ItemModifier
		{
			get { return new DataSource<ItemModifierEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ItemPriceEntity instances in the database.</summary>
		public DataSource<ItemPriceEntity> ItemPrice
		{
			get { return new DataSource<ItemPriceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ItemPrinterEntity instances in the database.</summary>
		public DataSource<ItemPrinterEntity> ItemPrinter
		{
			get { return new DataSource<ItemPrinterEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LayoutRoomEntity instances in the database.</summary>
		public DataSource<LayoutRoomEntity> LayoutRoom
		{
			get { return new DataSource<LayoutRoomEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LayoutTableEntity instances in the database.</summary>
		public DataSource<LayoutTableEntity> LayoutTable
		{
			get { return new DataSource<LayoutTableEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceLevelEntity instances in the database.</summary>
		public DataSource<PriceLevelEntity> PriceLevel
		{
			get { return new DataSource<PriceLevelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceScheduleEntity instances in the database.</summary>
		public DataSource<PriceScheduleEntity> PriceSchedule
		{
			get { return new DataSource<PriceScheduleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VendorEntity instances in the database.</summary>
		public DataSource<VendorEntity> Vendor
		{
			get { return new DataSource<VendorEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VersionEntity instances in the database.</summary>
		public DataSource<VersionEntity> Version
		{
			get { return new DataSource<VersionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
 
		#region Class Property Declarations
		/// <summary> Gets / sets the ITransaction to use for the queries created with this meta data object.</summary>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public ITransaction TransactionToUse
		{
			get { return _transactionToUse;}
			set { _transactionToUse = value;}
		}

		/// <summary>Gets or sets the custom function mappings to use. These take higher precedence than the ones in the DQE to use</summary>
		public FunctionMappingStore CustomFunctionMappings
		{
			get { return _customFunctionMappings; }
			set { _customFunctionMappings = value; }
		}
		
		/// <summary>Gets or sets the Context instance to use for entity fetches.</summary>
		public Context ContextToUse
		{
			get { return _contextToUse;}
			set { _contextToUse = value;}
		}
		#endregion
	}
}