﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses;
using Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses;
using Obymobi.Logic.Pos.FuturePos.Data.DaoClasses;
using Obymobi.Logic.Pos.FuturePos.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PriceSchedule'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PriceScheduleEntity : PriceScheduleEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PriceScheduleEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleId">PK value for PriceSchedule which data should be fetched into this PriceSchedule object</param>
		public PriceScheduleEntity(System.Guid priceScheduleId):
			base(priceScheduleId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleId">PK value for PriceSchedule which data should be fetched into this PriceSchedule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PriceScheduleEntity(System.Guid priceScheduleId, IPrefetchPath prefetchPathToUse):
			base(priceScheduleId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleId">PK value for PriceSchedule which data should be fetched into this PriceSchedule object</param>
		/// <param name="validator">The custom validator object for this PriceScheduleEntity</param>
		public PriceScheduleEntity(System.Guid priceScheduleId, IValidator validator):
			base(priceScheduleId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceScheduleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
