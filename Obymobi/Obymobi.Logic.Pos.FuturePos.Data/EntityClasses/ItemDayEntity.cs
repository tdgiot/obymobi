﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Logic.Pos.FuturePos.Data.FactoryClasses;
using Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses;
using Obymobi.Logic.Pos.FuturePos.Data.DaoClasses;
using Obymobi.Logic.Pos.FuturePos.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Pos.FuturePos.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ItemDay'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ItemDayEntity : ItemDayEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ItemDayEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="itemDayId">PK value for ItemDay which data should be fetched into this ItemDay object</param>
		public ItemDayEntity(System.Guid itemDayId):
			base(itemDayId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="itemDayId">PK value for ItemDay which data should be fetched into this ItemDay object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ItemDayEntity(System.Guid itemDayId, IPrefetchPath prefetchPathToUse):
			base(itemDayId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="itemDayId">PK value for ItemDay which data should be fetched into this ItemDay object</param>
		/// <param name="validator">The custom validator object for this ItemDayEntity</param>
		public ItemDayEntity(System.Guid itemDayId, IValidator validator):
			base(itemDayId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ItemDayEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
