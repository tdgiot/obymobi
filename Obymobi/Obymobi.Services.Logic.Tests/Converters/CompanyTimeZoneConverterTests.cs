﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Services.Logic.Converters;

namespace Obymobi.Services.Logic.Tests.Scheduling.Calculators
{
    [TestFixture]
    public class CompanyTimeZoneConverterTests
    {
        [Test]
        public void UtcToLocalTime_NotExistingCompanyId_ThrowsKeyNotFoundException()
        {
            var companyId = 999;

            var companies = new CompanyCollection();

            var sut = new CompanyTimeZoneConverter(companies);

            var exception = Assert.Throws<KeyNotFoundException>(() => sut.UtcToLocalTime(DateTime.UtcNow, companyId));
            Assert.AreEqual($"Company id {companyId} does not exist in dictionary", exception.Message);
        }

        [TestCase(1, "Europe/London", "2022-03-08 12:00:00", "2022-03-08 12:00:00")]
        [TestCase(1, "Europe/London", "2022-06-08 12:00:00", "2022-06-08 13:00:00")]

        public void UtcToLocalTime_DayLightSavingTime_ReturnsValidDateTime(int companyId, string timezoneOlsonId, DateTime utc, DateTime expected)
        {
            var company = new CompanyEntity
            {
                CompanyId = companyId,
                TimeZoneOlsonId = timezoneOlsonId
            };

            var companies = new CompanyCollection(new[] { company });

            var sut = new CompanyTimeZoneConverter(companies);

            DateTime actual = sut.UtcToLocalTime(utc, company.CompanyId);

            Assert.That(expected, Is.EqualTo(actual));
        }
    }
}
