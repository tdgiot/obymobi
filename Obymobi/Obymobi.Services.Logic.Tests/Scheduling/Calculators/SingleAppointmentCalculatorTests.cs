﻿using System;
using DevExpress.XtraScheduler;
using NUnit.Framework;
using Obymobi.Services.Logic.Tests.Builders;

namespace Obymobi.Services.Logic.Tests.Scheduling.Calculators
{
    [TestFixture]
    public class SingleAppointmentCalculatorTests
    {
        static class A
        {
            public static SingleAppointmentCalculatorBuilder SingleAppointmentCalculator = new SingleAppointmentCalculatorBuilder();
            public static SingleAppointmentBuilder Appointment => new SingleAppointmentBuilder();
        }

        [Test]
        public void Execute_WithInactiveAppointment_ReturnsFalseActiveAppointmentNull()
        {
            var appointment = A.Appointment
                               .W((SingleAppointmentBuilder.Start)DateTime.UtcNow.AddHours(-3))
                               .W((SingleAppointmentBuilder.End)DateTime.UtcNow.AddHours(-2))
                               .Build();

            var sut = A.SingleAppointmentCalculator
                       .Build();

            Appointment activeAppointment = sut.GetActiveAppointment(appointment, DateTime.UtcNow);

            Assert.IsNull(activeAppointment);
        }

        [Test]
        public void Execute_ActiveAppointment_ReturnsTrueActiveAppointmentNotNull()
        {
            var appointment = A.Appointment
                               .W((SingleAppointmentBuilder.Start)DateTime.UtcNow.AddHours(-1))
                               .W((SingleAppointmentBuilder.End)DateTime.UtcNow.AddHours(1))
                               .Build();

            var sut = A.SingleAppointmentCalculator
                       .Build();

            Appointment activeAppointment = sut.GetActiveAppointment(appointment, DateTime.UtcNow);

            Assert.IsNotNull(activeAppointment);
        }
    }
}
