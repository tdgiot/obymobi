﻿using System;
using DevExpress.XtraScheduler;
using Dionysos;
using NUnit.Framework;
using Obymobi.Services.Logic.Tests.Builders;

namespace Obymobi.Services.Logic.Tests.Scheduling.Calculators
{
    [TestFixture]
    public class RecurringAppointmentCalculatorTests
    {
        static class A
        {
            public static RecurringAppointmentCalculatorBuilder RecurringAppointmentCalculator = new RecurringAppointmentCalculatorBuilder();
            public static RecurringAppointmentBuilder Appointment => new RecurringAppointmentBuilder();
            public static RecurrenceInfoBuilder RecurrenceInfo => new RecurrenceInfoBuilder();
        }

        [Test]
        public void Execute_WithInactiveAppointment_ReturnsAppointmentNull()
        {
            var appointment = A.Appointment
                               .W((RecurringAppointmentBuilder.Start)DateTime.UtcNow.AddHours(-1))
                               .W((RecurringAppointmentBuilder.End)DateTime.UtcNow.AddHours(1))
                               .W(A.RecurrenceInfo
                                   .W(RecurrenceType.Daily)
                                   .W(RecurrenceRange.EndByDate)
                                   .W((RecurrenceInfoBuilder.Start)DateTime.UtcNow.AddDays(-2))
                                   .W((RecurrenceInfoBuilder.End)DateTime.UtcNow.AddDays(-1)))
                               .Build();

            var sut = A.RecurringAppointmentCalculator
                       .Build();

            Appointment activeAppointment = sut.GetActiveAppointment(appointment, DateTime.UtcNow);

            Assert.IsNull(activeAppointment);
        }

        [Test]
        public void Execute_ActiveAppointmentWithDeletedException_ReturnsAppointmentNull()
        {
            var appointment = A.Appointment
                               .W((RecurringAppointmentBuilder.Start)DateTime.UtcNow.AddHours(-1))
                               .W((RecurringAppointmentBuilder.End)DateTime.UtcNow.AddHours(1))
                               .W(A.RecurrenceInfo
                                   .W(RecurrenceType.Daily)
                                   .W(RecurrenceRange.EndByDate)
                                   .W((RecurrenceInfoBuilder.Start)DateTime.UtcNow.AddDays(-1))
                                   .W((RecurrenceInfoBuilder.End)DateTime.UtcNow.AddDays(10)))
                               .Build();

            appointment.CreateException(AppointmentType.DeletedOccurrence, 1);

            var sut = A.RecurringAppointmentCalculator
                       .Build();

            Appointment activeAppointment = sut.GetActiveAppointment(appointment, DateTime.UtcNow);

            Assert.IsNull(activeAppointment);
        }

        [Test]
        public void Execute_ActiveAppointment_ReturnsAppointmentNotNull()
        {
            var appointment = A.Appointment
                               .W((RecurringAppointmentBuilder.Start)DateTime.UtcNow.AddHours(-1))
                               .W((RecurringAppointmentBuilder.End)DateTime.UtcNow.AddHours(1))
                               .W(A.RecurrenceInfo
                                   .W(RecurrenceType.Daily)
                                   .W(RecurrenceRange.EndByDate)
                                   .W((RecurrenceInfoBuilder.Start)DateTime.UtcNow.AddDays(-1))
                                   .W((RecurrenceInfoBuilder.End)DateTime.UtcNow.AddDays(10)))
                               .Build();

            var sut = A.RecurringAppointmentCalculator
                       .Build();

            Appointment activeAppointment = sut.GetActiveAppointment(appointment, DateTime.UtcNow);

            Assert.IsNotNull(activeAppointment);
        }

        [Test]
        public void Execute_ActiveAppointmentLondonTimeZone_ReturnsAppointmentNotNull()
        {
            TimeZoneInfo londonTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

            var now = new DateTime(2022, 3, 27, 0, 30, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone);

            var appointment = A.Appointment
                               .W((RecurringAppointmentBuilder.Start)new DateTime(2022, 3, 21, 0, 0, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone))
                               .W((RecurringAppointmentBuilder.End)new DateTime(2022, 3, 21, 1, 0, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone))
                               .W(A.RecurrenceInfo
                                   .W(RecurrenceType.Daily)
                                   .W(RecurrenceRange.NoEndDate)
                                   .W((RecurrenceInfoBuilder.Start)new DateTime(2022, 3, 21, 0, 0, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone))
                                   .W((RecurrenceInfoBuilder.End)new DateTime(2022, 3, 21, 1, 0, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone)))
                               .Build();

            var sut = A.RecurringAppointmentCalculator
                       .Build();

            Appointment activeAppointment = sut.GetActiveAppointment(appointment, now);

            Assert.IsNotNull(activeAppointment);
        }

        [Test]
        public void Execute_ActiveAppointmentLondonTimeZoneDST_ReturnsAppointmentNotNull()
        {
            TimeZoneInfo londonTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

            var now = new DateTime(2022, 6, 7, 23, 30, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone);

            var appointment = A.Appointment
                               .W((RecurringAppointmentBuilder.Start)new DateTime(2022, 3, 21, 0, 0, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone))
                               .W((RecurringAppointmentBuilder.End)new DateTime(2022, 3, 21, 1, 0, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone))
                               .W(A.RecurrenceInfo
                                   .W(RecurrenceType.Daily)
                                   .W(RecurrenceRange.NoEndDate)
                                   .W((RecurrenceInfoBuilder.Start)new DateTime(2022, 3, 21, 0, 0, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone))
                                   .W((RecurrenceInfoBuilder.End)new DateTime(2022, 3, 21, 1, 0, 0, DateTimeKind.Utc).UtcToLocalTime(londonTimeZone)))
                               .Build();

            var sut = A.RecurringAppointmentCalculator
                       .Build();

            Appointment activeAppointment = sut.GetActiveAppointment(appointment, now);

            Assert.IsNotNull(activeAppointment);
        }
    }
}
