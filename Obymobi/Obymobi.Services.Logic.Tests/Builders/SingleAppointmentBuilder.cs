﻿using System;
using AutoFixture;
using DevExpress.XtraScheduler;
using Obymobi.Logic;

namespace Obymobi.Services.Logic.Tests.Builders
{
    internal class SingleAppointmentBuilder
    {
        public SingleAppointmentBuilder() : this(Initialise())
        {

        }

        private SingleAppointmentBuilder(AppointmentData appointmentData) => Data = appointmentData;

        private AppointmentData Data { get; }

        public SingleAppointmentBuilder W(Start start) =>
            new SingleAppointmentBuilder(new AppointmentData
            {
                AppointmentType = Data.AppointmentType,
                Start = start,
                End = Data.End
            });

        public SingleAppointmentBuilder W(End end) =>
            new SingleAppointmentBuilder(new AppointmentData
            {
                AppointmentType = Data.AppointmentType,
                Start = Data.Start,
                End = end
            });

        private static AppointmentData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => AppointmentType.Normal);
            fixture.Register(() => (Start)DateTime.UtcNow);
            fixture.Register(() => (End)DateTime.UtcNow);
            return fixture.Create<AppointmentData>();
        }

        public Appointment Build() => new DevExpressAppointment(Data.AppointmentType) 
        { 
            Start = Data.Start, 
            End = Data.End
        };

        public class AppointmentData
        {
            public AppointmentType AppointmentType { get; set; }
            public Start Start { get; set; }
            public End End { get; set; }
        }

        public readonly struct Start
        {
            private DateTime Value { get; }

            private Start(DateTime value) => Value = value;

            public static implicit operator DateTime(Start start) => start.Value;

            public static implicit operator Start(DateTime value) => new Start(value);
        }

        public readonly struct End
        {
            private DateTime Value { get; }

            private End(DateTime value) => Value = value;

            public static implicit operator DateTime(End end) => end.Value;

            public static implicit operator End(DateTime value) => new End(value);
        }
    }
}
