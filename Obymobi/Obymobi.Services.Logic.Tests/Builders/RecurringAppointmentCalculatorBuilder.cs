﻿using Obymobi.Services.Logic.Scheduling.Calculators;

namespace Obymobi.Services.Logic.Tests.Builders
{
    internal class RecurringAppointmentCalculatorBuilder
    {
        public RecurringAppointmentCalculator Build() => new RecurringAppointmentCalculator();
    }
}
