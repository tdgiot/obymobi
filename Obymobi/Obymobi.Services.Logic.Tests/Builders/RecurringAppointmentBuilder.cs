﻿using System;
using AutoFixture;
using DevExpress.XtraScheduler;
using Obymobi.Logic;

namespace Obymobi.Services.Logic.Tests.Builders
{
    internal class RecurringAppointmentBuilder
    {
        public RecurringAppointmentBuilder() : this(Initialise())
        {

        }

        private RecurringAppointmentBuilder(AppointmentData appointmentData) => Data = appointmentData;

        private AppointmentData Data { get; }

        public RecurringAppointmentBuilder W(Start start) =>
            new RecurringAppointmentBuilder(new AppointmentData
            {
                AppointmentType = Data.AppointmentType,
                Start = start,
                End = Data.End,
                RecurrenceInfo = Data.RecurrenceInfo
            });

        public RecurringAppointmentBuilder W(End end) =>
            new RecurringAppointmentBuilder(new AppointmentData
            {
                AppointmentType = Data.AppointmentType,
                Start = Data.Start,
                End = end,
                RecurrenceInfo = Data.RecurrenceInfo
            });

        public RecurringAppointmentBuilder W(RecurrenceInfoBuilder recurrenceInfoBuilder) =>
            new RecurringAppointmentBuilder(new AppointmentData
            {
                AppointmentType = Data.AppointmentType,
                Start = Data.Start,
                End = Data.End,
                RecurrenceInfo = recurrenceInfoBuilder.Build()
            });

        private static AppointmentData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => AppointmentType.Pattern);
            fixture.Register(() => (Start)DateTime.UtcNow);
            fixture.Register(() => (End)DateTime.UtcNow);
            fixture.Register(() => new RecurrenceInfoBuilder().Build());
            return fixture.Create<AppointmentData>();
        }

        public Appointment Build() => new DevExpressAppointment(Data.AppointmentType) 
        { 
            Start = Data.Start, 
            End = Data.End,
            RecurrenceInfo =
            {
                Type = Data.RecurrenceInfo.Type,
                Range = Data.RecurrenceInfo.Range,
                Start = Data.RecurrenceInfo.Start,
                End = Data.RecurrenceInfo.End,
                OccurrenceCount = Data.RecurrenceInfo.OccurrenceCount,
                Periodicity = Data.RecurrenceInfo.Periodicity,
                DayNumber = Data.RecurrenceInfo.DayNumber,
                WeekDays = Data.RecurrenceInfo.WeekDays,
                WeekOfMonth = Data.RecurrenceInfo.WeekOfMonth,
                Month = Data.RecurrenceInfo.Month
            }
        };

        public class AppointmentData
        {
            public AppointmentType AppointmentType { get; set; }
            public Start Start { get; set; }
            public End End { get; set; }
            public RecurrenceInfo RecurrenceInfo { get; set; }
        }

        public readonly struct Start
        {
            private DateTime Value { get; }

            private Start(DateTime value) => Value = value;

            public static implicit operator DateTime(Start start) => start.Value;

            public static implicit operator Start(DateTime value) => new Start(value);
        }

        public readonly struct End
        {
            private DateTime Value { get; }

            private End(DateTime value) => Value = value;

            public static implicit operator DateTime(End end) => end.Value;

            public static implicit operator End(DateTime value) => new End(value);
        }
    }
}
