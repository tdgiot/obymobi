﻿using System;
using AutoFixture;
using DevExpress.XtraScheduler;

namespace Obymobi.Services.Logic.Tests.Builders
{
    internal class RecurrenceInfoBuilder
    {
        public RecurrenceInfoBuilder() : this(Initialise())
        {

        }

        private RecurrenceInfoBuilder(RecurrenceInfoData recurrenceInfoData) => Data = recurrenceInfoData;

        private RecurrenceInfoData Data { get; }

        public RecurrenceInfoBuilder W(RecurrenceType recurrenceType) =>
            new RecurrenceInfoBuilder(new RecurrenceInfoData
            {
                Type = recurrenceType,
                Range = Data.Range,
                Start = Data.Start,
                End = Data.End,
                OccurrenceCount = Data.OccurrenceCount,
                Periodicity = Data.Periodicity,
                DayNumber = Data.DayNumber,
                WeekDays = Data.WeekDays,
                WeekOfMonth = Data.WeekOfMonth,
                Month = Data.Month
            });

        public RecurrenceInfoBuilder W(RecurrenceRange recurrenceRange) =>
            new RecurrenceInfoBuilder(new RecurrenceInfoData
            {
                Type = Data.Type,
                Range = recurrenceRange,
                Start = Data.Start,
                End = Data.End,
                OccurrenceCount = Data.OccurrenceCount,
                Periodicity = Data.Periodicity,
                DayNumber = Data.DayNumber,
                WeekDays = Data.WeekDays,
                WeekOfMonth = Data.WeekOfMonth,
                Month = Data.Month
            });

        public RecurrenceInfoBuilder W(Start start) =>
            new RecurrenceInfoBuilder(new RecurrenceInfoData
            {
                Type = Data.Type,
                Range = Data.Range,
                Start = start,
                End = Data.End,
                OccurrenceCount = Data.OccurrenceCount,
                Periodicity = Data.Periodicity,
                DayNumber = Data.DayNumber,
                WeekDays = Data.WeekDays,
                WeekOfMonth = Data.WeekOfMonth,
                Month = Data.Month
            });

        public RecurrenceInfoBuilder W(End end) =>
            new RecurrenceInfoBuilder(new RecurrenceInfoData
            {
                Type = Data.Type,
                Range = Data.Range,
                Start = Data.Start,
                End = end,
                OccurrenceCount = Data.OccurrenceCount,
                Periodicity = Data.Periodicity,
                DayNumber = Data.DayNumber,
                WeekDays = Data.WeekDays,
                WeekOfMonth = Data.WeekOfMonth,
                Month = Data.Month
            });

        private static RecurrenceInfoData Initialise()
        {
            Fixture fixture = new Fixture();
            fixture.Register(() => RecurrenceType.Daily);
            fixture.Register(() => RecurrenceRange.NoEndDate);
            fixture.Register(() => (Start)DateTime.UtcNow);
            fixture.Register(() => (End)DateTime.UtcNow);
            fixture.Register(() => (OccurrenceCount)10);
            fixture.Register(() => (Periodicity)1);
            fixture.Register(() => (DayNumber)1);
            fixture.Register(() => WeekDays.EveryDay);
            fixture.Register(() => WeekOfMonth.First);
            fixture.Register(() => (Month)1);
            return fixture.Create<RecurrenceInfoData>();
        }

        public RecurrenceInfo Build() => new RecurrenceInfo
        { 
            Type = Data.Type,
            Range = Data.Range,
            Start = Data.Start, 
            End = Data.End,
            OccurrenceCount = Data.OccurrenceCount,
            Periodicity = Data.Periodicity,
            DayNumber = Data.DayNumber,
            WeekDays = Data.WeekDays,
            WeekOfMonth = Data.WeekOfMonth,
            Month = Data.Month
        };

        public class RecurrenceInfoData
        {
            public RecurrenceType Type { get; set; }
            public RecurrenceRange Range { get; set; }
            public Start Start { get; set; }
            public End End { get; set; }
            public OccurrenceCount OccurrenceCount { get; set; }
            public Periodicity Periodicity { get; set; }
            public DayNumber DayNumber { get; set; }
            public WeekDays WeekDays { get; set; }
            public WeekOfMonth WeekOfMonth { get; set; }
            public Month Month { get; set; }
        }

        public readonly struct Start
        {
            private DateTime Value { get; }

            private Start(DateTime value) => Value = value;

            public static implicit operator DateTime(Start start) => start.Value;

            public static implicit operator Start(DateTime value) => new Start(value);
        }

        public readonly struct End
        {
            private DateTime Value { get; }

            private End(DateTime value) => Value = value;

            public static implicit operator DateTime(End end) => end.Value;

            public static implicit operator End(DateTime value) => new End(value);
        }

        public readonly struct OccurrenceCount
        {
            private int Value { get; }

            private OccurrenceCount(int value) => Value = value;

            public static implicit operator int(OccurrenceCount occurrenceCount) => occurrenceCount.Value;

            public static implicit operator OccurrenceCount(int value) => new OccurrenceCount(value);
        }

        public readonly struct Periodicity
        {
            private int Value { get; }

            private Periodicity(int value) => Value = value;

            public static implicit operator int(Periodicity periodicity) => periodicity.Value;

            public static implicit operator Periodicity(int value) => new Periodicity(value);
        }

        public readonly struct DayNumber
        {
            private int Value { get; }

            private DayNumber(int value) => Value = value;

            public static implicit operator int(DayNumber dayNumber) => dayNumber.Value;

            public static implicit operator DayNumber(int value) => new DayNumber(value);
        }

        public readonly struct Month
        {
            private int Value { get; }

            private Month(int value) => Value = value;

            public static implicit operator int(Month month) => month.Value;

            public static implicit operator Month(int value) => new Month(value);
        }
    }
}
