﻿using Obymobi.Services.Logic.Scheduling.Calculators;

namespace Obymobi.Services.Logic.Tests.Builders
{
    internal class SingleAppointmentCalculatorBuilder
    {
        public SingleAppointmentCalculator Build() => new SingleAppointmentCalculator();
    }
}
