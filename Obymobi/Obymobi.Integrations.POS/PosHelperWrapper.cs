﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using Obymobi.Enums;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// Wrapper for per pos specific logic
    /// </summary>
    public static class PosHelperWrapper
    {
        /// <summary>
        /// Perform Pos Specific aciton on an Order to convert it to a Pos orders
        /// </summary>
        /// <param name="orderEntity">Order Entity</param>
        /// <param name="orderModel">Order Model</param>
        /// <param name="terminalId">The terminal id.</param>
        public static void PrepareOrderForRequestService(OrderEntity orderEntity, Order orderModel, int terminalId)
        {
            POSConnectorType connectorType = PosHelperWrapper.GetRelatedPosConnectorType(orderEntity, terminalId);
            PosHelperWrapper.GetPosHelper(connectorType).PrepareOrderForRequestService(orderEntity, orderModel);
        }

        /// <summary>
        /// Get's called in the Product Validator OnSave for product's that are linked to PosProduct and only when
        /// the PosproductId is changed.
        /// </summary>
        public static void ProductValidateEntityBeforeSave(ProductEntity product)
        {
            POSConnectorType connectorType = PosHelperWrapper.GetRelatedPosConnectorType(product.CompanyEntity);
            PosHelperWrapper.GetPosHelper(connectorType).ProductValidateEntityBeforeSave(product);
        }

        /// <summary>
        /// Get's called in the Posproduct Validator
        /// </summary>
        public static void PosproductValidateEntityBeforeSave(PosproductEntity posproduct)
        {
            POSConnectorType connectorType = PosHelperWrapper.GetRelatedPosConnectorType(posproduct.CompanyEntity);
            PosHelperWrapper.GetPosHelper(connectorType).PosproductValidateEntityBeforeSave(posproduct);
        }

        /// <summary>
        /// Retrieve all importable posproducts (this might exclude some Posproducts that are not
        /// supported)
        /// </summary>
        /// <param name="company">The company.</param>
        /// <param name="prefetchProducts">if set to <c>true</c> prefetch products.</param>
        /// <param name="hideImportedProducts">if set to <c>true</c> [hide imported products].</param>
        /// <returns></returns>
        public static PosproductCollection RetrieveAllImportablePosproducts(CompanyEntity company, bool prefetchProducts, bool hideImportedProducts)
        {
            POSConnectorType connectorType = PosHelperWrapper.GetRelatedPosConnectorType(company);
            return PosHelperWrapper.GetPosHelper(connectorType).RetrieveAllImportablePosproducts(company, prefetchProducts, hideImportedProducts);
        }

        /// <summary>
        /// Verify if the Settings for the POS integration are complete and ready for an export to a config file
        /// </summary>
        public static void VerifySettings(TerminalEntity terminal)
        {
            PosHelperWrapper.GetPosHelper(terminal.POSConnectorTypeEnum).VerifySettings(terminal);
        }

        /// <summary>
        /// Gets the type of the related pos connector.
        /// </summary>
        /// <param name="company">The company.</param>
        /// <returns></returns>
        public static POSConnectorType GetRelatedPosConnectorType(this CompanyEntity company)
        {
            return company.POSConnectorType;
        }

        /// <summary>
        /// Gets the type of the related pos connector.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="terminalId">The terminal id.</param>
        /// <returns></returns>
        public static POSConnectorType GetRelatedPosConnectorType(OrderEntity order, int terminalId)
        {
            TerminalEntity terminal = new TerminalEntity(terminalId);

            if (terminal.IsNew)
                throw new ObymobiException(PosSpecificLogicHelperResult.TerminalNotFoundForOrder, "OrderId: {0}", order.OrderId);

            return terminal.POSConnectorType.ToEnum<POSConnectorType>();
        }

        /// <summary>
        /// Gets the pos helper.
        /// </summary>
        /// <param name="posConnectorType">Type of the pos connector.</param>
        /// <returns></returns>
        public static PosHelper GetPosHelper(POSConnectorType posConnectorType)
        {
            PosHelper toReturn = new DummyPosHelper();

            switch (posConnectorType)
            {
                case POSConnectorType.Agilysys:
                    //toReturn = new Agilysys.AgilysysHelper();
                    break;
            }

            return toReturn;
        }


        /// <summary>
        /// PosSpecificLogicHelperResult enums
        /// </summary>
        public enum PosSpecificLogicHelperResult : int
        {
            /// <summary>
            /// The terminal can't be found for this order
            /// </summary>
            TerminalNotFoundForOrder = 1,
            /// <summary>
            /// The PosHelper isn't implemented for this Pos connector type
            /// </summary>
            PosHelperNotImplementedForPosConnectorType = 2
        }
    }
}
