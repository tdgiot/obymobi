﻿using Dionysos.Configuration;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// Configuration class for POS
    /// </summary>
    public class POSConfigurationInfo : ConfigurationItemCollection
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.POS.POSConfigurationInfo class
        /// </summary>
        public POSConfigurationInfo()
        {
            string sectionName = "POS";

            // Tonit
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.TonitWebserviceUrl, "Url of the Tonit webservice", "http://192.168.57.107:3786/ECServer2CGI.exe/soap/IECServer2", typeof(string)));

            // Vectron
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronClerkNumber, "Number of the Otoucho clerk on the Vectron POS", 888, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronDisplayOutput, "Flag which indicates whether output should be displayed", false, typeof(bool)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronComPort, "Number of the COM port which the Vectron POS is connected to", 0, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronBaudrate, "Baudrate of the COM port connection", "9600", typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronWordLength, "Wordlength of the COM port connection", "8", typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronParity, "Parity of the COM port connection", "Even", typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronStopbits, "Stopbits of the COM port connection", "1", typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronLengthClerkNr, "Length of the clerk number on the Vectron POS", 3, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronLengthGuestCheckNr, "Length of the guest check number on the Vectron POS", 3, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronLengthPluNr, "Length of plu number on the Vectron POS", 3, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronTimeOut, "Timeout of the COM port connection", 1000, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronPathLogFile, "Path of the FlexiPos log file", @"C:\Vectron\FlexiPosOrder\", typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronNameLogfile, "Name of the FlexiPos log file", "FlexiPos.log", typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronDaysLogfile, "Days to keep the FlexiPos log file", 14, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronOtouchoGuestCheckNr, "Number of the Otoucho guest check", 9999, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.VectronDeliverypointLockedPLU, "PLU number of the deliverypoint locked PLU", -1, typeof(short)));

            // Untill
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UntillWebserviceUrl, "Url of the Untill webservice", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UntillWebserviceUser, "Username for the Untill webservice", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UntillWebservicePassword, "Password for the Untill webservice", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UntillPricegroup, "Pricegroup to use", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UntillTestMode, "Test mode, clears the table after each 50 orders", false, typeof(bool)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UntillOtouchoTableNumber, "Number of the Otoucho table to write messages to", -1, typeof(short)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UntillDeliverypointProductPLU, "PLU number of the deliverypoint locked PLU", -1, typeof(short)));

            // Untill2
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.Untill2WebserviceUrl, "Url of the Untill2 webservice", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.Untill2WebserviceUser, "Username for the Untill2 webservice", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.Untill2WebservicePassword, "Password for the Untill2 webservice", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.Untill2Pricegroup, "Pricegroup to use", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.Untill2TestMode, "Test mode, clears the table after each 50 orders", false, typeof(bool)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.Untill2TestModePaymentmethodId, "Test mode - Id of PaymnentMethod", 0, typeof(long)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.Untill2SalesArea, "Sales Area", "", typeof(short)));

            // Relax
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.RelaxWebserviceUrl, "Url of the Relax webservice", string.Empty, typeof(string)));

            // Torex Alpha
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.TorexAlphaIpAddress, "IpAddress of Torex Alpha Server", "", typeof(string)));

            // HoreCat
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.HoreCatWebserviceUrl, "Url of the HoreCat webservice", string.Empty, typeof(string)));

            // Unitouch
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UnitouchIPAddress, "IP address of the Unitouch POS", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UnitouchPort, "Port number of the Unitouch POS", 1026, typeof(int)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UnitouchWaiterId, "Waiter ID of the Unitouch POS", 1, typeof(int)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UnitouchCurrentUser, "Current user number of the Unitouch POS", 1, typeof(int)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.UnitouchCurrentCashReg, "Current cash register number of the Unitouch POS", 1, typeof(int)));

            // Aloha
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaWebserviceUrl, "Url of the Aloha webservice", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaEmployeeNumber, "Employee number of the Aloha POS", 101, typeof(int)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaEmployeePassword, "Password of the employee on the Aloha POS", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaJobCodeID, "Job code ID", 10, typeof(int)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaSessionToken, "Session Token", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaRequestID, "Request ID", 1234, typeof(int)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaIdentifier, "Identifier", "123456789", typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaLocalMode, "LocalMode", false, typeof(bool)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaLocalFile, "LocalFile", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AlohaMarkerID, "Marker ID", 0, typeof(int)));

            // ICR Touch
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.IcrtouchIpAddress, "IP Address of the POS to connect to", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.IcrtouchCashierName, "Name to be used for the Cashier to place the orders", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.IcrtouchCashierNumber, "Number to be used for the Cashier to place the orders", string.Empty, typeof(string)));

            // Future POS
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.FuturePosConnectionString, "Connection string to the database", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.FuturePosPriceLevel, "Price-level for the default import", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.FuturePosLayoutRooms, "Room(s), names or indexes, seperated by comma's to import deliverypoints for.", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.FuturePosApiUrl, "Webservice Url", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.FuturePosApiUsername, "Webservice API username", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.FuturePosApiPassword, "Webservice API password", string.Empty, typeof(string)));

            // Cenium
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.CeniumApiUrl, "Webservice Url", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.CeniumApiUsername, "Webservice API username", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.CeniumApiPassword, "Webservice API password", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.CeniumApiDomain, "Webservice API domain", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.CeniumUseDefaultCredentials, "Use Default Credentials", false, typeof(bool)));

            // MicrosMcp
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpWebserviceUrl, "Webservice Url including trialing slahs", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpApplicationId, "Application Id", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpCultureCode, "Culture Code", "en-US", typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpStoreId, "StoreId", -1, typeof(int)));

            //this.Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpDefaultOrderClass, "MicrosMcpDefaultOrderClass", Adactus.RICO.UISupportServices.Contract.ServiceData.Enumerations.OrderClass.RoomService.ToString(), typeof(string)));
            //this.Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpFulfillmentType, "MicrosMcpFullfilmentType", Adactus.RICO.UISupportServices.Contract.ServiceData.Enumerations.FulfillmentTimeType.ASAP.ToString(), typeof(string)));
            //this.Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpPaymentMethodType, "MicrosMcpPaymentMethodType", Adactus.RICO.UISupportServices.Contract.ServiceData.Enumerations.PaymentMethodType.Cash.ToString(), typeof(string)));
            //this.Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpCustomerEmail, "MicrosMcpCustomerEmail", "mcp-ordering@crave-emenu.com", typeof(string)));
            //this.Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpCustomerPhonenumber, "MicrosMcpCustomerPhonenumber", "+441908487536", typeof(string)));
            //this.Add(new ConfigurationItem(sectionName, POSConfigurationConstants.MicrosMcpCustomerLastname, "MicrosMcpCustomerLastname", "Crave Interactive", typeof(string)));

            // Agilysys
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AgilysysWebserviceUrl, "Webservice Url", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AgilysysClientId, "Client ID", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AgilysysAuthenticationCode, "Authentication Code", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AgilysysEmployeeId, "Employee ID", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.AgilysysCheckTypeId, "Check type ID", string.Empty, typeof(string)));

            // Simphony
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.SimphonyWebserviceUrl, "Webservice Url", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.SimphonyEmployeeObjectNum, "Employee Number", 0, typeof(int)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.SimphonyRevenueCenters, "Revenue Centers", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.SimphonyUseOldPostTransactionMethod, "Use legacy transaction method", false, typeof(bool)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.SimphonySyncInStockProductsOnly, "Only sync products in stock", false, typeof(bool)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.SimphonyVendorCode, "Vendor code", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.SimphonyTenderMediaObjectNum, "Tender object number", 0, typeof(int)));
            Add(new ConfigurationItem(sectionName, POSConfigurationConstants.SimphonyOrderType, "Order type id", 0, typeof(int)));
        }

        #endregion
    }
}
