﻿
namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// Configuration class which contains constants for the names of configuration items
    /// </summary>
    public class POSConfigurationConstants
    {
        #region Tonit

        /// <summary>
        /// Url of the Tonit webservice
        /// </summary>
        public const string TonitWebserviceUrl = "TonitWebserviceUrl";

        #endregion

        #region Vectron

        /// <summary>
        /// Number of the Otoucho clerk on the Vectron POS
        /// </summary>
        public const string VectronClerkNumber = "VectronClerkNumber";

        /// <summary>
        /// Flag which indicates whether output should be displayed
        /// </summary>
        public const string VectronDisplayOutput = "VectronDisplayOutput";

        /// <summary>
        /// Number of the COM port which the Vectron POS is connected to
        /// </summary>
        public const string VectronComPort = "VectronComPort";

        /// <summary>
        /// Baudrate of the COM port connection
        /// </summary>
        public const string VectronBaudrate = "VectronBaudrate";

        /// <summary>
        /// Wordlength of the COM port connection
        /// </summary>
        public const string VectronWordLength = "VectronWordLength";

        /// <summary>
        /// Parity of the COM port connection
        /// </summary>
        public const string VectronParity = "VectronParity";

        /// <summary>
        /// Stopbits of the COM port connection
        /// </summary>
        public const string VectronStopbits = "VectronStopbits";

        /// <summary>
        /// Length of the clerk number on the Vectron POS
        /// </summary>
        public const string VectronLengthClerkNr = "VectronLengthClerkNr";

        /// <summary>
        /// Length of the guest check number on the Vectron POS
        /// </summary>
        public const string VectronLengthGuestCheckNr = "VectronLengthGuestCheckNr";

        /// <summary>
        /// Length of plu number on the Vectron POS
        /// </summary>
        public const string VectronLengthPluNr = "VectronLengthPluNr";

        /// <summary>
        /// Timeout of the COM port connection
        /// </summary>
        public const string VectronTimeOut = "VectronTimeOut";

        /// <summary>
        /// Path of the FlexiPos log file
        /// </summary>
        public const string VectronPathLogFile = "VectronPathLogFile";

        /// <summary>
        /// Name of the FlexiPos log file
        /// </summary>
        public const string VectronNameLogfile = "VectronNameLogfile";

        /// <summary>
        /// Days to keep the FlexiPos log file
        /// </summary>
        public const string VectronDaysLogfile = "VectronDaysLogfile";

        /// <summary>
        /// Guest check number of the Otoucho table
        /// </summary>
        public const string VectronOtouchoGuestCheckNr = "VectronOtouchoGuestCheckNr";

        /// <summary>
        /// PLU which notifies that the deliverypoint is locked
        /// </summary>
        public const string VectronDeliverypointLockedPLU = "VectronDeliverypointLockedPLU";

        #endregion

        #region Untill

        /// <summary>
        /// Untill Webservice Url
        /// </summary>
        public const string UntillWebserviceUrl = "UntillWebserviceUrl";

        /// <summary>
        /// Untill Webservice Url
        /// </summary>
        public const string UntillWebserviceUser = "UntillWebserviceUser";

        /// <summary>
        /// Untill Webservice Url
        /// </summary>
        public const string UntillWebservicePassword = "UntillWebservicePassword";

        /// <summary>
        /// Untill Pricegroup
        /// </summary>
        public const string UntillPricegroup = "UntillPricegroup";

        /// <summary>
        /// Untill Test Mode
        /// </summary>
        public const string UntillTestMode = "UntillTestMode";

        /// <summary>
        /// Guest check number of the Otoucho table
        /// </summary>
        /// 
        public const string UntillOtouchoTableNumber = "UntillOtouchoTableNumber";

        /// <summary>
        /// PLU which notifies that the deliverypoint is locked
        /// </summary>
        public const string UntillDeliverypointProductPLU = "UntillDeliverypointProductPLU";

        #endregion

        #region Untill2

        /// <summary>
        /// Untill2 Webservice Url
        /// </summary>
        public const string Untill2WebserviceUrl = "Untill2WebserviceUrl";

        /// <summary>
        /// Untill2 Webservice Url
        /// </summary>
        public const string Untill2WebserviceUser = "Untill2WebserviceUser";

        /// <summary>
        /// Untill2 Webservice Url
        /// </summary>
        public const string Untill2WebservicePassword = "Untill2WebservicePassword";

        /// <summary>
        /// Untill2 Pricegroup
        /// </summary>
        public const string Untill2Pricegroup = "Untill2Pricegroup";

        /// <summary>
        /// Untill2 Test Mode
        /// </summary>
        public const string Untill2TestMode = "Untill2TestMode";

        /// <summary>
        /// Untill2 Test Mode
        /// </summary>
        public const string Untill2TestModePaymentmethodId = "Untill2TestModePaymentmethodId";

        /// <summary>
        /// Untill2 Test Mode
        /// </summary>
        public const string Untill2SalesArea = "Untill2SalesArea";

        #endregion

        #region Relax

        /// <summary>
        /// Relax Webservice Url
        /// </summary>
        public const string RelaxWebserviceUrl = "RelaxWebserviceUrl";

        #endregion

        #region TorexAlpha

        /// <summary>
        /// Torex Alpha Webservice Url
        /// </summary>
        public const string TorexAlphaIpAddress = "TorexAlphaIpAddress";

        #endregion

        #region HoreCat

        /// <summary>
        /// Horecat Webservice Url
        /// </summary>
        public const string HoreCatWebserviceUrl = "HoreCatWebserviceUrl";

        #endregion

        #region Unitouch

        /// <summary>
        /// Unitouch IP address
        /// </summary>
        public const string UnitouchIPAddress = "UnitouchIPAddress";

        /// <summary>
        /// Unitouch Port
        /// </summary>
        public const string UnitouchPort = "UnitouchPort";

        /// <summary>
        /// Unitouch Waiter ID
        /// </summary>
        public const string UnitouchWaiterId = "UnitouchWaiterId";

        /// <summary>
        /// Unitouch Current User
        /// </summary>
        public const string UnitouchCurrentUser = "UnitouchCurrentUser";

        /// <summary>
        /// Unitouch Current Cash Reg
        /// </summary>
        public const string UnitouchCurrentCashReg = "UnitouchCurrentCashReg";

        #endregion

        #region Aloha

        /// <summary>
        /// Aloha Webservice Url
        /// </summary>
        public const string AlohaWebserviceUrl = "AlohaWebserviceUrl";

        /// <summary>
        /// Aloha Employee Number
        /// </summary>
        public const string AlohaEmployeeNumber = "AlohaEmployeeNumber";

        /// <summary>
        /// Aloha Employee Password
        /// </summary>
        public const string AlohaEmployeePassword = "AlohaEmployeePassword";

        /// <summary>
        /// Aloha JobCode ID
        /// </summary>
        public const string AlohaJobCodeID = "AlohaJobCodeID";

        /// <summary>
        /// Aloha Session Token
        /// </summary>
        public const string AlohaSessionToken = "AlohaSessionToken";

        /// <summary>
        /// Aloha Request ID
        /// </summary>
        public const string AlohaRequestID = "AlohaRequestID";

        /// <summary>
        /// Aloha Identifier
        /// </summary>
        public const string AlohaIdentifier = "AlohaIdentifier";

        /// <summary>
        /// Aloha Local Mode
        /// </summary>
        public const string AlohaLocalMode = "AlohaLocalMode";

        /// <summary>
        /// Aloha Local File
        /// </summary>
        public const string AlohaLocalFile = "AlohaLocalFile";

        /// <summary>
        /// Aloha marker id
        /// </summary>
        public const string AlohaMarkerID = "AlohaMarkerID";

        #endregion

        #region IcrTouch

        /// <summary>
        /// Aloha ICR Toucho Ip Address
        /// </summary>
        public const string IcrtouchIpAddress = "IcrtouchIpAddress";

        /// <summary>
        /// Aloha ICR Toucho Cashier Name
        /// </summary>
        public const string IcrtouchCashierName = "IcrtouchCashierName";

        /// <summary>
        /// Aloha ICR Toucho Cashier Number
        /// </summary>
        public const string IcrtouchCashierNumber = "IcrtouchCashierNumber";

        #endregion

        #region FuturePos

        /// <summary>
        /// Future Pos Connection String of the POS
        /// </summary>
        public const string FuturePosConnectionString = "FuturePosConnectionString";

        /// <summary>
        /// Future Pos Price Level to Use
        /// </summary>
        public const string FuturePosPriceLevel = "FuturePosPriceLevel";

        /// <summary>
        /// Future Pos Layout Rooms
        /// </summary>
        public const string FuturePosLayoutRooms = "FuturePosLayoutRooms";

        /// <summary>
        /// Future Pos API url
        /// </summary>
        public const string FuturePosApiUrl = "FuturePosApiUrl";

        /// <summary>
        /// Future Pos API Username
        /// </summary>
        public const string FuturePosApiUsername = "FuturePosApiUsername";

        /// <summary>
        /// Future Pos API Password
        /// </summary>
        public const string FuturePosApiPassword = "FuturePosApiPassword";

        #endregion

        #region Cenium

        /// <summary>
        /// Cenium API url
        /// </summary>
        public const string CeniumApiUrl = "CeniumApiUrl";

        /// <summary>
        /// Cenium API Username
        /// </summary>
        public const string CeniumApiUsername = "CeniumApiUsername";

        /// <summary>
        /// Cenium API Password
        /// </summary>
        public const string CeniumApiPassword = "CeniumApiPassword";

        /// <summary>
        /// Cenium API Password
        /// </summary>
        public const string CeniumApiDomain = "CeniumApiDomain";

        /// <summary>
        /// Use default credentials
        /// </summary>
        public const string CeniumUseDefaultCredentials = "CeniumUseDefaultCredentials";

        #endregion

        #region MicrosMcp

        public const string MicrosMcpApplicationId = "MicrosMcpApplicationId";
        public const string MicrosMcpCultureCode = "MicrosMcpCultureCode";
        public const string MicrosMcpWebserviceUrl = "MicrosMcpWebserviceUrl";
        public const string MicrosMcpStoreId = "MicrosMcpStoreId";

        public const string MicrosMcpDefaultOrderClass = "MicrosMcpDefaultOrderClass";
        public const string MicrosMcpFulfillmentType = "MicrosMcpFulfillmentType";
        public const string MicrosMcpCustomerEmail = "MicrosMcpCustomerEmail";
        public const string MicrosMcpCustomerPhonenumber = "MicrosMcpCustomerPhonenumber";
        public const string MicrosMcpCustomerLastname = "MicrosMcpCustomerLastname";
        public const string MicrosMcpPaymentMethodType = "MicrosMcpPaymentMethodType";

        #endregion

        #region Agilysys

        public const string AgilysysWebserviceUrl = "AgilsysWebserviceUrl";
        public const string AgilysysClientId = "AgilsysClientId";
        public const string AgilysysAuthenticationCode = "AgilsysAuthenticationCode";
        public const string AgilysysEmployeeId = "AgilsysEmployeeId";
        public const string AgilysysCheckTypeId = "AgilsysCheckTypeId";

        #endregion

        #region Simphony

        public const string SimphonyWebserviceUrl = "SimphonyWebserviceUrl";
        public const string SimphonyEmployeeObjectNum = "SimphonyEmployeeObjectNum";
        public const string SimphonyRevenueCenters = "SimphonyRevenueCenters";
        public const string SimphonyUseOldPostTransactionMethod = "SimphonyUseOldPostTransactionMethod";
        public const string SimphonySyncInStockProductsOnly = "SimphonySyncInStockProductsOnly";
        public const string SimphonyVendorCode = "SimphonyVendorCode";
        public const string SimphonyTenderMediaObjectNum = "SimphonyTenderMediaObjectNum";
        public const string SimphonyOrderType = "SimphonyOrderType";

        #endregion
    }
}
