﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Obymobi.Logic.POS.HoreCat
{
    /// <summary>
    /// XMLResponse class
    /// </summary>
    public class XMLResponse
    {
        #region Fields

        private int resultCode = -1;
        private string message = string.Empty;
        private DataSet data = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XMLResponse"/> class.
        /// </summary>
        public XMLResponse()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the result code.
        /// </summary>
        /// <value>The result code.</value>
        public int ResultCode
        {
            get
            {
                return this.resultCode;
            }
            set
            {
                this.resultCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get
            {
                return this.message;
            }
            set
            {
                this.message = value;
            }
        }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
        public DataSet Data
        {
            get
            {
                return this.data;
            }
            set
            {
                this.data = value;
            }
        }

        #endregion
    }
}
