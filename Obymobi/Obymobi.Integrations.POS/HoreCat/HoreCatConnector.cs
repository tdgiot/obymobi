﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Logic.Loggers;
using Obymobi.Logic.Model;
using System.Data;
using System.IO;
using Obymobi.Enums;
using System.Xml;

namespace Obymobi.Logic.POS.HoreCat
{
    /// <summary>
    /// 
    /// </summary>
    public class HoreCatConnector : IPOSConnector
    {
        #region Fields

        private string webserviceUrl = string.Empty;
        private HoreCatWebservice.HCGrenosService webservice = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HoreCatConnector"/> class.
        /// </summary>
        public HoreCatConnector()
        {
            this.WriteToLogExtended("Constructor", "Start");

            this.webserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.HoreCatWebserviceUrl);

            if (this.webserviceUrl.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "Webservice url is empty");

            this.WriteToLogExtended("Constructor", "End");
        }

        #endregion

        #region Methods

        private XMLResponse ProcessWebserviceResult(string result)
        {
            // Create and initialize the XML response
            XMLResponse response = new XMLResponse();

            // Create and initialize a StringReader for the XML string
            StringReader stringReader = new StringReader(result);

            // Create and initialize a DataSet instance
            // and read the XML into the dataset
            DataSet resultDataSet = new DataSet();
            resultDataSet.ReadXml(stringReader);

            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            xmlDoc.Load(stringReader);

            DataTable responseTable = resultDataSet.Tables["Response"];
            if (responseTable != null && responseTable.Rows.Count > 0)
            {
                DataRow responseRow = responseTable.Rows[0];
                if (responseRow != null)
                {
                    int resultCode = -1;
                    if (int.TryParse(responseRow["ResultCode"].ToString(), out resultCode))
                        response.ResultCode = resultCode;
                    response.Message = responseRow["Message"].ToString();
                }
            }

            response.Data = resultDataSet;

            return response;
        }

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public Model.Posproduct[] GetPosproducts()
        {
            this.WriteToLogExtended("GetPosproducts", "Start");
            List<Posproduct> products = new List<Posproduct>();

            bool hasError = false;
            string errorMessage = string.Empty;

            try
            {
                XMLResponse response = ProcessWebserviceResult(this.Webservice.GetProducts(string.Empty));
                if (response.ResultCode != 0)
                {
                    hasError = true;
                    errorMessage = string.Format("The HoreCat webservice returned an invalid response: {0}", response.Message);
                }
                else
                {
                    // Products
                    DataTable productTable = response.Data.Tables["Product"];
                    if (productTable != null && productTable.Rows.Count > 0)
                    {
                        foreach (DataRow productRow in productTable.Rows)
                        {
                            // Row indentifier
                            string product_Id = string.Empty;
                            if (productTable.Columns["Product_Id"] != null)
                                product_Id = productRow["Product_Id"].ToString();

                            #region Create and initalize the Posproduct instance
                            Posproduct posproduct = new Posproduct();
                            posproduct.ExternalId = productRow["ProductId"].ToString();                 // ProductId
                            posproduct.Name = productRow["Name"].ToString();                            // Name
                            decimal price = 0;                                                          // Price
                            if (decimal.TryParse(productRow["Price"].ToString(), out price))
                                posproduct.PriceIn = (price / 100);
                            posproduct.ExternalPoscategoryId = productRow["CategoryId"].ToString();     // ExternalCategoryId
                            int sortOrder = 0;                                                          // SortOrder
                            if (int.TryParse(productRow["SortOrder"].ToString(), out sortOrder))
                                posproduct.SortOrder = sortOrder;
                            posproduct.Description = productRow["Description"].ToString();              // Description
                            posproduct.VatTariff = 1;                                                   // VatTariff
                            #endregion

                            if (product_Id.Length > 0)
                            {
                                // Product modifiers groups
                                foreach (DataRow prodModifierGroupRow in response.Data.Tables["ProdModifierGroups"].Select("Product_Id = " + product_Id))
                                {
                                    // Row identifier
                                    string prodModifierGroup_Id = prodModifierGroupRow["ProdModifierGroups_Id"].ToString();

                                    // Create and initialize a list for the alterations
                                    List<Posalteration> alterations = new List<Posalteration>();

                                    // Product modifier group
                                    foreach (DataRow prodModGroupRow in response.Data.Tables["ProdModGroup"].Select("ProdModifierGroups_Id = " + prodModifierGroup_Id))
                                    {
                                        // Row identifier
                                        string prodModGroup_Id = prodModGroupRow["ProdModGroup_Id"].ToString();

                                        #region Create and initalize the Posalteration instance
                                        Posalteration alteration = new Posalteration();
                                        alteration.ExternalId = prodModGroupRow["ProdModGroupId"].ToString();       // ExternalId
                                        alteration.Name = prodModGroupRow["Name"].ToString();                       // Name
                                        bool required = false;                                                      // Required
                                        if (bool.TryParse(prodModGroupRow["Required"].ToString(), out required))
                                        {
                                            if (required)
                                                alteration.MinOptions = 1;
                                            else
                                                alteration.MinOptions = 0;
                                        }
                                        bool multiselect = false;                                                   // Multiselect
                                        if (bool.TryParse(prodModGroupRow["Multiselect"].ToString(), out multiselect))
                                        {
                                            if (multiselect)
                                                alteration.MaxOptions = 25;
                                            else
                                                alteration.MaxOptions = 1;
                                        }
                                        #endregion

                                        foreach (DataRow prodModifiersRow in response.Data.Tables["ProdModifiers"].Select("ProdModGroup_Id = " + prodModGroup_Id))
                                        {
                                            // Row identifier
                                            string prodModifiers_Id = prodModifiersRow["ProdModifiers_Id"].ToString();

                                            // Create and initialize the alteration options
                                            List<Posalterationoption> alterationoptions = new List<Posalterationoption>();

                                            foreach (DataRow prodModifierRow in response.Data.Tables["ProdModifier"].Select("ProdModifiers_Id = " + prodModGroup_Id))
                                            {
                                                #region Create and initialize the alteration option

                                                Posalterationoption alterationoption = new Posalterationoption();
                                                alterationoption.ExternalId = prodModifierRow["ProdModId"].ToString();              // External Id
                                                alterationoption.Name = prodModifierRow["Name"].ToString();                         // Name
                                                int priceAddition = 0;
                                                if (int.TryParse(prodModifierRow["PriceAddition"].ToString(), out priceAddition))   // Price addition
                                                    alterationoption.PriceIn = (Convert.ToDecimal(priceAddition) / 100m);

                                                #endregion

                                                // Add the alteration option
                                                alterationoptions.Add(alterationoption);
                                            }

                                            // Set the alteration options
                                            alteration.Posalterationoptions = alterationoptions.ToArray();
                                        }

                                        // Add the alteration
                                        alterations.Add(alteration);
                                    }

                                    // Set the alterations to the product
                                    posproduct.Posalterations = alterations.ToArray();
                                }
                            }

                            // Add the product 
                            products.Add(posproduct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                errorMessage = string.Format("An exception was thrown while trying to get the products from the HoreCat webservice. Exception: {0}", ex.Message);
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, ex, errorMessage);
            }

            if (hasError)
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, errorMessage);


            this.WriteToLogExtended("GetPosproducts", "End");

            return products.ToArray();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public Model.Poscategory[] GetPoscategories()
        {
            this.WriteToLogExtended("GetPoscategories", "Start");
            List<Poscategory> categories = new List<Poscategory>();

            bool hasError = false;
            string errorMessage = string.Empty;

            try
            {
                XMLResponse response = ProcessWebserviceResult(this.Webservice.GetCategories("<XMLRequest></XMLRequest>"));
                if (response.ResultCode != 0)
                {
                    hasError = true;
                    errorMessage = string.Format("The HoreCat webservice returned an invalid response: {0}", response.Message);
                }
                else
                {
                    if (response.Data.Tables.Count > 3 && response.Data.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row in response.Data.Tables[3].Rows)
                        {
                            Poscategory poscategory = new Poscategory();
                            poscategory.ExternalId = row.ItemArray.GetValue(0).ToString();
                            poscategory.Name = row.ItemArray.GetValue(1).ToString();

                            categories.Add(poscategory);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                errorMessage = string.Format("An exception was thrown while trying to get the categories from the HoreCat webservice. Exception: {0}", ex.Message);
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, ex, errorMessage);
            }

            if (hasError)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, errorMessage);

            this.WriteToLogExtended("GetPoscategories", "End");

            return categories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            return null;
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            this.WriteToLogExtended("GetPosdeliverypoints", "Start");
            List<Posdeliverypoint> deliverypoints = new List<Posdeliverypoint>();

            bool hasError = false;
            string errorMessage = string.Empty;

            try
            {
                XMLResponse response = ProcessWebserviceResult(this.Webservice.GetDeliverypoints("<XMLRequest></XMLRequest>"));
                if (response.ResultCode != 0)
                {
                    hasError = true;
                    errorMessage = string.Format("The HoreCat webservice returned an invalid response: {0}", response.Message);
                }
                else
                {
                    if (response.Data.Tables.Count > 3 && response.Data.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row in response.Data.Tables[3].Rows)
                        {
                            Posdeliverypoint posdeliverypoint = new Posdeliverypoint();
                            posdeliverypoint.ExternalId = row.ItemArray.GetValue(0).ToString();
                            posdeliverypoint.Name = row.ItemArray.GetValue(1).ToString();

                            deliverypoints.Add(posdeliverypoint);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                errorMessage = string.Format("An exception was thrown while trying to get the deliverypoints from the HoreCat webservice. Exception: {0}", ex.Message);
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints, errorMessage);
            }

            if (hasError)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints, errorMessage);

            this.WriteToLogExtended("GetPosdeliverypoints", "End");

            return deliverypoints.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>An Obymobi.Logic.Model.Posorder instance</returns>
        public Model.Posorder GetPosorder(string deliverypointNumber)
        {
            return null;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            OrderProcessingError processingError = OrderProcessingError.None;

            this.WriteToLogExtended("SaveOrder", "Start");

            string errorMessage = string.Empty;

            try
            {
                // Create and initialize a StringBuilder
                // to create the XML string with
                StringBuilder xmlRequest = new StringBuilder();

                // XML Request
                xmlRequest.Append("<XMLRequest>");

                // Order
                xmlRequest.Append("<Order>");
                xmlRequest.AppendFormat("<OrderId>{0}</OrderId>", posorder.PosorderId);
                xmlRequest.AppendFormat("<DeliverypointId>{0}</DeliverypointId>", posorder.PosdeliverypointExternalId);

                // Order items
                xmlRequest.Append("<OrderItems>");

                if (posorder.Posorderitems != null && posorder.Posorderitems.Length > 0)
                {
                    for (int i = 0; i < posorder.Posorderitems.Length; i++)
                    {
                        Posorderitem orderitem = posorder.Posorderitems[i];

                        xmlRequest.Append("<OrderItem>");
                        xmlRequest.AppendFormat("<OrderItemId>{0}</OrderItemId>", orderitem.PosorderitemId);
                        xmlRequest.AppendFormat("<ProductId>{0}</ProductId>", orderitem.PosproductExternalId);

                        string description = orderitem.Description;
                        description = description.Replace("'", "");
                        description = description.Replace("\"", "");
                        description = description.Replace("&", "");

                        xmlRequest.AppendFormat("<ProductName>{0}</ProductName>", description);
                        xmlRequest.AppendFormat("<Quantity>{0}</Quantity>", orderitem.Quantity);

                        // Order item alterations
                        xmlRequest.Append("<OrderItemMods>");

                        if (orderitem.Posalterationitems != null && orderitem.Posalterationitems.Length > 0)
                        {
                            foreach (Posalterationitem alterationitem in orderitem.Posalterationitems)
                            {
                                xmlRequest.Append("<OrderItemMod>");
                                xmlRequest.AppendFormat("<OrderItemModId>{0}</OrderItemModId>", alterationitem.PosalterationitemId);
                                xmlRequest.AppendFormat("<ProdModId>{0}</ProdModId>", alterationitem.ExternalPosalterationoptionId);
                                xmlRequest.Append("</OrderItemMod>");
                            }
                        }

                        xmlRequest.Append("</OrderItemMods>");

                        xmlRequest.Append("</OrderItem>");
                    }
                }

                xmlRequest.Append("</OrderItems>");
                xmlRequest.Append("</Order>");
                xmlRequest.Append("</XMLRequest>");


                XMLResponse response = ProcessWebserviceResult(this.Webservice.ProcessOrder(xmlRequest.ToString()));
                if (response.ResultCode != 0)
                {
                    processingError = OrderProcessingError.PosErrorApiInterfaceException;
                    errorMessage = string.Format("The HoreCat webservice returned an invalid response: {0}", response.Message);
                }
            }
            catch (Exception ex)
            {
                processingError = OrderProcessingError.PosErrorApiInterfaceException;
                errorMessage = string.Format("An exception was thrown while trying to save the order to the HoreCat webservice. Exception: {0}", ex.Message);
                throw new POSException(processingError, ex, errorMessage);
            }

            if (processingError != OrderProcessingError.None)
                throw new POSException(processingError, errorMessage);

            this.WriteToLogExtended("SaveOrder", "End");

            return processingError;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            return true;
        }

        ///// <summary>
        ///// Prints an Obymobi status report
        ///// </summary>
        ///// <param name="terminalStatus"></param>
        ///// <returns>
        ///// True if printing the status report was succesful, False if not
        ///// </returns>
        //public bool PrintStatusReport(Status.TerminalStatus terminalStatus)
        //{
        //    this.WriteToLogExtended("PrintStatusReport", "Start");

        //    bool hasError = false;
        //    string errorMessage = string.Empty;

        //    try
        //    {
        //        StringBuilder xmlRequest = new StringBuilder();

        //        xmlRequest.Append("<XMLRequest>");
        //        xmlRequest.Append("<TextMessage>");

        //        bool internet = terminalStatus.Internet;
        //        bool webservice = terminalStatus.Webservice;
        //        bool database = terminalStatus.Database;
        //        bool configuration = terminalStatus.UsernameSpecified && terminalStatus.PasswordSpecified && terminalStatus.CompanyIdSpecified && terminalStatus.TerminalIdSpecified && (terminalStatus.ConfigurationValid.HasValue && terminalStatus.ConfigurationValid.Value);

        //        xmlRequest.AppendLine();

        //        // Header
        //        if (internet && webservice && database && configuration)
        //            xmlRequest.AppendLine("STATUS OTOUCHO: OK");
        //        else
        //            xmlRequest.AppendLine("STATUS OTOUCHO: NIET GEREED!");

        //        xmlRequest.AppendLine();

        //        // Internet
        //        if (internet)
        //            xmlRequest.AppendLine("Internet: OK");
        //        else
        //            xmlRequest.AppendLine("Internet: FOUT");

        //        // Webservice
        //        if (webservice)
        //            xmlRequest.AppendLine("Webservice: OK");
        //        else
        //            xmlRequest.AppendLine("Webservice: FOUT");

        //        // Database
        //        if (database)
        //            xmlRequest.AppendLine("Gegevensoverdracht: OK");
        //        else
        //            xmlRequest.AppendLine("Gegevensoverdracht: FOUT");

        //        // Configuration
        //        if (configuration)
        //            xmlRequest.AppendLine("Logingegevens: OK");
        //        else
        //            xmlRequest.AppendLine("Logingegevens: FOUT");

        //        xmlRequest.Append("</TextMessage>");
        //        xmlRequest.Append("</XMLRequest>");

        //        XMLResponse response = ProcessWebserviceResult(this.Webservice.PrintReceipt(xmlRequest.ToString()));
        //        if (response != null && response.ResultCode != 0)
        //        {
        //            hasError = true;
        //            errorMessage = string.Format("The HoreCat webservice returned an invalid response: {0}", response.Message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        hasError = true;
        //        errorMessage = string.Format("An exception was thrown while trying to print a status report. Exception: {0}", ex.Message);
        //    }

        //    if (hasError)
        //        throw new POSException(OrderProcessingError.PosErrorPrintingError, errorMessage);

        //    this.WriteToLogExtended("PrintStatusReport", "End");

        //    return !hasError;
        //}

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            return true;
        }

        /// <summary>
        /// Writes to log normal.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Debug("HoreCatConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes to log extended.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("HoreCatConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the HoreCat webservice
        /// </summary>
        public HoreCatWebservice.HCGrenosService Webservice
        {
            get
            {
                if (this.webservice == null)
                {
                    this.webservice = new Obymobi.Logic.HoreCatWebservice.HCGrenosService();
                    this.webservice.Url = this.webserviceUrl;
                }
                return this.webservice;
            }
        }

        /// <summary>
        /// Gets or sets a flag which indicates whether an error occurred
        /// </summary>
        /// <value></value>
        public bool HasError
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        /// <value></value>
        public string ErrorMessage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
