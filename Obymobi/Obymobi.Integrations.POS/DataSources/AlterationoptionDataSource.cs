﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Integrations.POS.DataSources
{
    public class AlterationoptionDataSource
    {
        /// <summary>
        /// Gets the alteration option id by the posalterationoption external id.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="externalId">The external id.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public int GetAlterationoptionIdByPosalterationoptionExternalId(int companyId, string externalId, Transaction transaction, AlterationDialogMode alterationDialogMode)
        {
            int alterationoptionId = -1;
            int alterationVersion = alterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationoptionFields.ExternalId == externalId);
            filter.Add(PosalterationoptionFields.CompanyId == companyId);
            filter.Add(AlterationoptionFields.CompanyId == companyId);
            filter.Add(AlterationoptionFields.Version == alterationVersion);

            RelationCollection relations = new RelationCollection();
            relations.Add(PosalterationoptionEntity.Relations.AlterationoptionEntityUsingPosalterationoptionId);

            AlterationoptionCollection alterationoptions = new AlterationoptionCollection();
            alterationoptions.AddToTransaction(transaction);
            alterationoptions.GetMulti(filter, relations);

            if (alterationoptions.Count == 0)
                throw new ObymobiException(GetAlterationoptionIdResult.NoAlterationoptionFound, "No alteration option found for the specified external id '{0}'.", externalId);
            else if (alterationoptions.Count == 1)
                alterationoptionId = alterationoptions[0].AlterationoptionId;
            else
                throw new ObymobiException(GetAlterationoptionIdResult.MultipleAlterationoptionsFound, "Multiple alteration options found for the specified external id '{0}'.", externalId);

            return alterationoptionId;
        }
    }
}
