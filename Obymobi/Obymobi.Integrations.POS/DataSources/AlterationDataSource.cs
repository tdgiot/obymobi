﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Integrations.POS.DataSources
{
    public class AlterationDataSource
    {
        public int GetAlterationIdByPosalterationExternalId(int companyId, string externalId, Transaction transaction, AlterationDialogMode alterationDialogMode)
        {
            int alterationId = -1;
            int alterationVersion = alterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationFields.ExternalId == externalId);
            filter.Add(PosalterationFields.CompanyId == companyId);

            PredicateExpression joinFilter = new PredicateExpression();
            joinFilter.Add(AlterationFields.CompanyId == companyId);
            joinFilter.Add(AlterationFields.Version == alterationVersion);

            RelationCollection relations = new RelationCollection();
            relations.Add(PosalterationEntity.Relations.AlterationEntityUsingPosalterationId).CustomFilter = joinFilter;

            AlterationCollection alterations = new AlterationCollection();
            alterations.AddToTransaction(transaction);
            alterations.GetMulti(filter, relations);

            if (alterations.Count == 0)
                throw new ObymobiException(GetAlterationIdResult.NoAlterationFound, "No alteration found for the specified external id '{0}'.", externalId);
            else if (alterations.Count == 1)
                alterationId = alterations[0].AlterationId;
            else
                throw new ObymobiException(GetAlterationIdResult.MultipleAlterationsFound, "Multiple alterations found for the specified external id '{0}'.", externalId);

            return alterationId;
        }
    }
}
