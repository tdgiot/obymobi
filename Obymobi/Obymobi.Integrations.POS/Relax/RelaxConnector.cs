﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Logic.Loggers;
using Obymobi.Logic.Model;
using Obymobi.Enums;

namespace Obymobi.Logic.POS.Relax
{
    /// <summary>
    /// RelaxConnector class
    /// </summary>
    public class RelaxConnector : IPOSConnector
    {
        #region Fields

        private string webserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.RelaxWebserviceUrl);
        private RelaxWebservice.wsaWellnessService webservice = null;
        private bool initialized = false;
        private string sha = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RelaxConnector"/> class.
        /// </summary>
        public RelaxConnector()
        {
            this.WriteToLogExtended("Constructor", "Start");

            if (this.webserviceUrl.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "Webservice url is empty");

            this.Initialize();

            this.WriteToLogExtended("Constructor", "End");
        }

        #endregion

        #region Methods



        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void Initialize()
        {
            this.WriteToLogExtended("Initialize", "Start");

            int idSysModule = 4;
            string canCd = "PDA_Login";
            int idSysUser = 200;
            string computerName = "N73";
            string ipAddress = "";
            string password = "alex";
            int sysDivision = 6;
            bool? oke = false;
            string errorText = string.Empty;
            string result = string.Empty;

            try
            {
                // Login the user
                // and retrieve the SHA encryption key
                result = this.Webservice.setSYS_UserLogin(idSysModule, canCd, idSysUser, computerName, ipAddress, password, sysDivision, out oke, out this.sha, out errorText);

                // Set the initialize flag
                this.initialized = true;
            }
            catch (Exception ex)
            {
                this.WriteToLogNormal("Initialize", ex.Message);
            }

            this.WriteToLogExtended("Initialize", "End");
        }

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public Model.Posproduct[] GetPosproducts()
        {
            this.WriteToLogExtended("GetPosproducts", "Start");
            List<Posproduct> products = new List<Posproduct>();

            RelaxWebservice.getPOS_PLU_ttPOS_PLURow[] PLUs = null;
            bool? success = false;
            string errorCode = string.Empty;
            string errorText = string.Empty;
            string result = string.Empty;

            if (!this.initialized || sha.IsNullOrWhiteSpace())
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, "SHA encryption key is not set!");

            try
            {
                result = this.Webservice.getPOS_PLU(this.sha, 2, 4, ref PLUs, out success, out errorCode, out errorText);

                if (success.HasValue && success.Value)
                {
                    foreach (var PLU in PLUs)
                    {
                        if (PLU.ID.HasValue && PLU.strName.Length > 0)
                        {
                            Posproduct posproduct = new Posproduct();
                            posproduct.ExternalId = PLU.ID.ToString();
                            posproduct.ExternalPoscategoryId = PLU.ID_RES_PLU_SubGroup.ToString();
                            posproduct.Name = PLU.strName;
                            posproduct.PriceIn = 0;
                            posproduct.SortOrder = (PLU.intOrder.HasValue ? PLU.intOrder.Value : 0);
                            posproduct.VatTariff = 1;

                            products.Add(posproduct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.WriteToLogNormal("GetPosproducts", ex.Message);
            }

            this.WriteToLogExtended("GetPosproducts", "End");

            return products.ToArray();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public Model.Poscategory[] GetPoscategories()
        {
            this.WriteToLogExtended("GetPoscategories", "Start");
            List<Poscategory> categories = new List<Poscategory>();

            RelaxWebservice.getPOS_Subgroup_ttPOS_SubgroupRow[] subgroups = null;
            bool? success = false;
            string errorCode = string.Empty;
            string errorText = string.Empty;
            string result = string.Empty;

            if (!this.initialized || sha.IsNullOrWhiteSpace())
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, "SHA encryption key is not set!");

            try
            {
                result = this.Webservice.getPOS_Subgroup(this.sha, ref subgroups, out success, out errorCode, out errorText);

                if (success.HasValue && success.Value)
                {
                    foreach (var subgroup in subgroups)
                    {
                        if (subgroup.ID.HasValue && subgroup.strName.Length > 0)
                        {
                            Poscategory poscategory = new Poscategory();
                            poscategory.ExternalId = subgroup.ID.ToString();
                            poscategory.Name = subgroup.strName;

                            categories.Add(poscategory);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.WriteToLogNormal("GetPoscategories", ex.Message);
            }

            this.WriteToLogExtended("GetPoscategories", "End");

            return categories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            return null;
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            this.WriteToLogExtended("GetPosdeliverypoints", "Start");
            List<Posdeliverypoint> deliverypoints = new List<Posdeliverypoint>();

            RelaxWebservice.getPOS_Table_ttPOS_TableRow[] tables = null;
            bool? success = false;
            int? layout = 2;
            string errorCode = string.Empty;
            string errorText = string.Empty;
            string result = string.Empty;

            if (!this.initialized || sha.IsNullOrWhiteSpace())
                throw new POSException(OrderProcessingError.PosErrorConfigurationIncomplete, "SHA encryption key is not set!");

            try
            {
                result = this.Webservice.getPOS_Table(this.sha, layout, ref tables, out success, out errorCode, out errorText);

                if (success.HasValue && success.Value)
                {
                    foreach (var table in tables)
                    {
                        if (table.ID.HasValue && table.strTableName.Length > 0)
                        {
                            Posdeliverypoint posdeliverypoint = new Posdeliverypoint();
                            posdeliverypoint.ExternalId = table.ID.ToString();
                            posdeliverypoint.Name = table.strTableName;

                            deliverypoints.Add(posdeliverypoint);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.WriteToLogNormal("GetPosdeliverypoints", ex.Message);
            }

            this.WriteToLogExtended("GetPosdeliverypoints", "End");

            return deliverypoints.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public Model.Posorder GetPosorder(string deliverypointNumber)
        {
            return null;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            // Requires rework!
            throw new NotImplementedException();

            //this.WriteToLogExtended("SaveOrder", "Start");

            //string action = string.Empty;
            //int? division = 6;
            //int? userRevenue = null;
            //int? layout = 2;
            //int? table = null;
            //int? device = null;
            //RelaxWebservice.setPOS_TableOrder_ttPOS_TableOrderRow[] orderRows = null;
            //bool? success = false;
            //string errorCode = string.Empty;
            //string errorText = string.Empty;

            //// Set the deliverypoint
            //int deliverypoint = -1;
            //if (int.TryParse(posorder.PosdeliverypointExternalId, out deliverypoint))
            //    table = deliverypoint;
            //else
            //    throw new POSException(OrderProcessingError.PosErrorNonParseableOrConvertableDataType, string.Format("The POS external deliverypoint '{0}' could not be parsed into an int!", posorder.PosdeliverypointExternalId));

            //// Set the orderitems
            //orderRows = new RelaxWebservice.setPOS_TableOrder_ttPOS_TableOrderRow[posorder.Posorderitems.Length];
            //for (int i = 0; i < posorder.Posorderitems.Length; i++)
            //{
            //    Posorderitem posorderitem = posorder.Posorderitems[i];

            //    RelaxWebservice.setPOS_TableOrder_ttPOS_TableOrderRow orderRow = new RelaxWebservice.setPOS_TableOrder_ttPOS_TableOrderRow();
            //    orderRow.ID_POS_Table = table;
            //    orderRow.ID_POS_Layout = layout;

            //    int plu = -1;
            //    if (int.TryParse(posorderitem.PosproductExternalId, out plu))
            //        orderRow.ID_RES_PLU = plu;
            //    else
            //        throw new POSException(OrderProcessingError.PosErrorNonParseableOrConvertableDataType, string.Format("The POS product external id '{0}' could not be parsed into an int!", posorderitem.PosproductExternalId));

            //    orderRow.intQuantity = posorderitem.Quantity;

            //    orderRows[i] = orderRow;
            //}

            //try
            //{
            //    this.Webservice.setPOS_TableOrder(this.sha, action, division, userRevenue, layout, table, device, ref orderRows, out success, out errorCode, out errorText);

            //    if (success.HasValue && success.Value)
            //    {
            //    }
            //}
            //catch (Exception ex)
            //{
            //    this.WriteToLogExtended("SaveOrder", ex.Message);
            //    throw new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, ex, ex.Message, null);
            //}

            //this.WriteToLogExtended("SaveOrder", "End");

            //return OrderProcessingError.None;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            return true;
        }

        /// <summary>
        /// Writes to log normal.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Debug("RelaxConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes to log extended.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("RelaxConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Untill webservice
        /// </summary>
        public RelaxWebservice.wsaWellnessService Webservice
        {
            get
            {
                if (this.webservice == null)
                {
                    this.webservice = new RelaxWebservice.wsaWellnessService();

                    //this.webservice.Url = "http://pda.relaxsoftware.nl:4080/wsa/wsa1/wsdl?targeturi=urn:relaxsoftware";
                }
                return this.webservice;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has error; otherwise, <c>false</c>.
        /// </value>
        public bool HasError
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
