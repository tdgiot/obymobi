﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos;
using Dionysos.Diagnostics;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Integrations.POS.DataSources;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// PosDataSynchronizer class
    /// </summary>
    public class PosDataSynchronizer
    {
        #region Fields

        POSConnectorType posConnectorType = POSConnectorType.Unknown;
        CompanyEntity company = null;
        int companyId = 0;
        Transaction transaction = null;

        private readonly AlterationDataSource alterationDataSource = new AlterationDataSource();
        private readonly AlterationoptionDataSource alterationoptionDataSource = new AlterationoptionDataSource();

        private readonly int alterationVersion;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PosDataSynchronizer"/> class.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        public PosDataSynchronizer(int companyId, AlterationDialogMode alterationDialogMode)
        {
            // Company
            this.companyId = companyId;
            this.company = new CompanyEntity(this.companyId);

            // Set ConnectorType, because some settings / switches rely on the PosConnector
            this.posConnectorType = this.company.POSConnectorType;

            alterationVersion = alterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;
        }

        /// <summary>
        /// Synchronizes the existing products.
        /// </summary>
        public void SynchronizeExistingProducts(AlterationDialogMode alterationDialogMode)
        {
            ProductCollection products = ProductHelper.GetProductCollectionForCompany(this.companyId, null, null);

            var posProductIds = (from p in products
                                 where p.PosproductId.HasValue
                                 select p.PosproductId.Value).ToList();

            this.SynchronizeProducts(posProductIds, null, false, true, true, false, alterationDialogMode);
        }


        /// <summary>
        /// Synchronize PosProducts to actual Producs
        /// </summary>
        /// <param name="posProductIds">PosProducts to import</param>
        /// <param name="addToCategoryId">The add to category id.</param>
        /// <param name="overwriteNames">Overwrite the Names of existing Products / Alterations / Alterationoptions</param>
        /// <param name="overwritePrices">Overwrite the Prices of existing Products / Alterationoptions</param>
        /// <param name="overwriteVatTariff">Overwrite the Vat Tariff of existing Products</param>
        /// <param name="overwriteType">Overwrite the Type of existing Products</param>
        public void SynchronizeProducts(List<int> posProductIds, int? addToCategoryId, bool overwriteNames, bool overwritePrices,
                                            bool overwriteVatTariff, bool overwriteType, AlterationDialogMode alterationDialogMode)
        {
            if (posProductIds.Count == 0)
                return;

            this.transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "SynchronizeProducts");

            try
            {
                List<PosproductEntity> posProductsRequestedByUserToToImport;
                List<PosproductEntity> posProductsRequiredByAlterationsToImport;
                PosproductPosalterationCollection posproductPosalterationsToImport;
                PosalterationCollection posalterationsToImport;
                PosalterationitemCollection posalterationitemsToImport;
                PosalterationoptionCollection posalterationoptionsToImport;

                RetrievePosdataToImport(posProductIds, out posProductsRequestedByUserToToImport, out posProductsRequiredByAlterationsToImport,
                    out posproductPosalterationsToImport, out posalterationsToImport, out posalterationitemsToImport, out posalterationoptionsToImport);

                List<PosproductEntity> posProductsToImport = new List<PosproductEntity>();
                posProductsToImport.AddRange(posProductsRequestedByUserToToImport);
                posProductsToImport.AddRange(posProductsRequiredByAlterationsToImport);

                // Get the ProductCollection for the specified company
                // and get the default EntityView
                ProductCollection productCollection = new ProductCollection();
                productCollection.AddToTransaction(this.transaction);
                productCollection.GetMulti(new PredicateExpression(ProductFields.CompanyId == this.companyId));
                EntityView<ProductEntity> productView = productCollection.DefaultView;

                // Get the PoscategoryCollection for the specified company
                // and get the default EntityView
                PoscategoryCollection poscategoryCollection = new PoscategoryCollection();
                poscategoryCollection.GetMulti(new PredicateExpression(PoscategoryFields.CompanyId == this.companyId));
                poscategoryCollection.AddToTransaction(this.transaction);
                EntityView<PoscategoryEntity> poscategoryView = poscategoryCollection.DefaultView;

                // Get the CategoryCollection for the specified company
                // and get the default EntityView
                CategoryCollection categoryCollection = new CategoryCollection();
                categoryCollection.AddToTransaction(this.transaction);
                categoryCollection.GetMulti(new PredicateExpression(CategoryFields.CompanyId == this.companyId));
                EntityView<CategoryEntity> categoryView = categoryCollection.DefaultView;

                // Get the ProductCategoryCollection for the specified company
                // and get the default EntityView
                // Create the filter for the product categories
                PredicateExpression productCategoryFilter = new PredicateExpression();
                productCategoryFilter.Add(ProductFields.CompanyId == this.companyId);

                RelationCollection relations = new RelationCollection();
                relations.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId);

                // Initialize the product category collection
                ProductCategoryCollection productCategoryCollection = new ProductCategoryCollection();
                productCategoryCollection.AddToTransaction(this.transaction);
                productCategoryCollection.GetMulti(productCategoryFilter, relations);

                // Get the entity view
                EntityView<ProductCategoryEntity> productCategoryView = productCategoryCollection.DefaultView;

                // Walk through the POS products
                foreach (PosproductEntity posproduct in posProductsToImport)
                {
                    Debug.WriteLine("PosproductHelper.SynchronizeProducts - " + posproduct.Name);

                    // Get a product for the current POS product
                    ProductEntity product = PosproductHelper.GetProductByPosproductId(posproduct.PosproductId, productView);
                    product.AddToTransaction(this.transaction);

                    // Set a flag whether the product was new
                    bool wasNew = product.IsNew;

                    // Set the properties of the product
                    product.CompanyId = this.companyId;
                    product.PosproductId = posproduct.PosproductId;

                    if (wasNew || overwriteNames) // Only overwrite the name if the product was new or an update of the name was forced
                        product.Name = posproduct.Name; // .TurnFirstToUpper(); - Pivotal ID: 45382263

                    if (wasNew || overwritePrices) // Only overwrite the price if the product was new or an update of the price was forced
                        product.PriceIn = posproduct.PriceIn;

                    if (wasNew || overwriteVatTariff) // Only overwrite the vat tariff if the product was new or an update of the vat tariff was forced
                        product.VattariffId = posproduct.VatTariff;

                    if (wasNew || overwriteType) // Only overwrite the type if the product was new or an update of the type was forced
                        product.Type = (int)ProductType.Product;

                    // Save the product
                    if (product.Save(true))
                    {
                        if (wasNew)
                        {
                            product.Refetch();
                            productView.RelatedCollection.Add(product);
                        }

                        // Add product to category if required
                        if (addToCategoryId.HasValue && addToCategoryId.Value > 0)
                        {
                            // Check if this product wasn't added to that Category Earlier
                            if (!posProductsRequestedByUserToToImport.Any(pp => pp.PosproductId == posproduct.PosproductId))
                            {
                                // The import of this product was not requested by the user but is required because 
                                // it's related to an AlterationOption that belongs to this product.                                 
                            }
                            else if (!wasNew && product.ProductCategoryCollection.Any(pc => pc.CategoryId == addToCategoryId.Value))
                            {
                                // Not required to add, was already added earlier
                            }
                            else
                            {
                                ProductCategoryEntity pc = new ProductCategoryEntity();
                                pc.AddToTransaction(this.transaction);
                                pc.ProductId = product.ProductId;
                                pc.CategoryId = addToCategoryId.Value;
                                pc.Save();
                            }
                        }

                        // GK We stopped syncing categories, you define a structure and then import per category.
                        //// Get the POS category for the current POS product
                        //PoscategoryEntity poscategory = PosproductHelper.GetPoscategoryByExternalId(posproduct.ExternalPoscategoryId, poscategoryView);
                        //if (!poscategory.IsNew)
                        //{
                        //    // The related PosCategory was found, now check if an existing Category Exists
                        //    CategoryEntity category = PosproductHelper.GetCategoryByPoscategoryId(poscategory.PoscategoryId, categoryView);
                        //    if (!category.IsNew)
                        //    {
                        //        // The Category was found
                        //        ProductCategoryEntity productCategory = PosproductHelper.GetProductCategory(product.ProductId, category.CategoryId, productCategoryView);
                        //        if (productCategory.IsNew)
                        //        {
                        //            productCategory.ProductId = product.ProductId;
                        //            productCategory.CategoryId = category.CategoryId;
                        //            if (productCategory.Save())
                        //            {
                        //                productCategory.Refetch();
                        //                productCategoryView.RelatedCollection.Add(productCategory);
                        //            }
                        //        }
                        //    }
                        //}
                    }
                    else
                        throw new ObymobiException(ProductSaveResult.GenericSaveFalseResult);
                }

                // Synchronize the Alterations
                this.SynchronizeAlterations(posalterationsToImport, overwriteNames);
                this.SynchronizeAlterationoptions(posalterationoptionsToImport, overwriteNames, overwritePrices);
                this.SynchronizeAlterationitems(posalterationitemsToImport, alterationDialogMode);

                // For some reason it is possible (at least with untill) that the ProductAlterations get some sort of corrupt.
                // For a strange reason the ProductAlteration PosproductPosalteration which both link to other Alterations.
                // We need to automatically identify those cases and correct them.

                this.SynchronizeProductAlterations(posproductPosalterationsToImport, overwriteNames, alterationDialogMode);

                this.transaction.Commit();
            }
            catch
            {
                this.transaction.Rollback();

                throw;
            }
            finally
            {
                this.transaction.Dispose();
            }
        }

        /// <summary>
        /// Synchronize PosProducts special products (e.g. service items and payment methods)
        /// </summary>
        /// <param name="posProductIds">PosProducts to import</param>
        /// <param name="productType">The type of the special product</param>
        /// <param name="overwriteNames">Overwrite the Name of existing Products</param>
        public void SynchronizeSpecialProducts(List<int> posProductIds, ProductType productType, bool overwriteNames)
        {
            if (posProductIds.Count == 0)
                return;

            this.transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "SynchronizeServiceitems");

            try
            {
                // Get the PosproductCollection for the specified company
                // and get the default EntityView
                PosproductCollection posproductCollection = new PosproductCollection();
                var posproductFilter = new PredicateExpression(PosproductFields.CompanyId == this.companyId);
                posproductFilter.Add(PosproductFields.PosproductId == posProductIds);
                posproductCollection.GetMulti(posproductFilter);

                // Get the ProductCollection for the specified company
                // and get the default EntityView
                ProductCollection productCollection = new ProductCollection();
                productCollection.AddToTransaction(this.transaction);
                productCollection.GetMulti(new PredicateExpression(ProductFields.CompanyId == this.companyId));
                EntityView<ProductEntity> productView = productCollection.DefaultView;

                // Walk through the POS products
                foreach (PosproductEntity posproduct in posproductCollection)
                {
                    Debug.WriteLine("PosproductHelper.SynchronizeServiceitems - " + posproduct.Name);

                    // Get a product for the current POS product
                    ProductEntity product = PosproductHelper.GetProductByPosproductId(posproduct.PosproductId, productView);
                    product.AddToTransaction(this.transaction);

                    // Set a flag whether the product was new
                    bool wasNew = product.IsNew;

                    // Set the properties of the product
                    product.CompanyId = this.companyId;
                    product.PosproductId = posproduct.PosproductId;
                    product.Type = (int)productType;
                    product.PriceIn = posproduct.PriceIn;
                    product.VattariffId = posproduct.VatTariff;

                    if (wasNew || overwriteNames) // Only overwrite the name if the product was new or an update of the name was forced
                        product.Name = posproduct.Name; // .TurnFirstToUpper(); - Pivotal ID: 45382263

                    // Save the product
                    if (product.Save(true))
                    {
                        if (wasNew)
                        {
                            product.Refetch();
                            productView.RelatedCollection.Add(product);
                        }
                    }
                    else
                        throw new ObymobiException(ProductSaveResult.GenericSaveFalseResult);
                }
                this.transaction.Commit();
            }
            catch
            {
                this.transaction.Rollback();

                throw;
            }
            finally
            {
                this.transaction.Dispose();
            }
        }

        private void RetrievePosdataToImport(List<int> posProductIds, out List<PosproductEntity> posProductsToImport,
            out List<PosproductEntity> posProductsRequiredByAlterationsToImport,
            out PosproductPosalterationCollection posproductPosalterationsToImport,
            out PosalterationCollection posalterationsToImport,
            out PosalterationitemCollection posalterationitemsToImport,
            out PosalterationoptionCollection posalterationoptionsToImport)
        {
            posproductPosalterationsToImport = new PosproductPosalterationCollection();
            posalterationsToImport = new PosalterationCollection();
            posalterationitemsToImport = new PosalterationitemCollection();
            posalterationoptionsToImport = new PosalterationoptionCollection();
            posProductsRequiredByAlterationsToImport = new List<PosproductEntity>();

            // Get the PosproductCollection for the specified company
            // and get the default EntityView
            PosproductCollection posproductCollection = new PosproductCollection();
            posproductCollection.AddToTransaction(this.transaction);

            var posproductFilter = new PredicateExpression(PosproductFields.CompanyId == this.companyId);
            posproductFilter.Add(PosproductFields.PosproductId == posProductIds);
            posproductCollection.GetMulti(posproductFilter);

            posProductsToImport = new List<PosproductEntity>();
            posProductsToImport.AddRange(posproductCollection);

            //EntityView<PosproductEntity> posproductView = posproductCollection.DefaultView;

            // Before we can start we have to check if any of the to be imported products have alterationoptions that
            // are linked to products. If so, we also need to import those, and before the actual products are imported.
            posproductPosalterationsToImport = new PosproductPosalterationCollection();
            posproductPosalterationsToImport.AddToTransaction(this.transaction);

            PredicateExpression filterComplete = new PredicateExpression();
            filterComplete.Add(PosproductPosalterationFields.CompanyId == this.companyId);
            // Here we do NOT filter on PosproductPosalterationFields.DeletedFromCms, because we DO want to import it
            // so the user can later choose to include it. In the SynchronizeProductAlterations method these are skipped.
            PredicateExpression filterOrConditions = new PredicateExpression();

            foreach (var posproduct in posproductCollection)
            {
                filterOrConditions.AddWithOr(PosproductPosalterationFields.PosproductExternalId == posproduct.ExternalId);
            }

            filterComplete.Add(filterOrConditions);

            posproductPosalterationsToImport.GetMulti(filterComplete);

            // Don't have to continue, nothing related
            if (posproductPosalterationsToImport.Count == 0)
                return;

            // --

            posalterationitemsToImport = new PosalterationitemCollection();
            posalterationitemsToImport.AddToTransaction(this.transaction);

            filterComplete = new PredicateExpression();
            filterComplete.Add(PosalterationitemFields.CompanyId == this.companyId);
            filterOrConditions = new PredicateExpression();

            foreach (var posProductPosAlteration in posproductPosalterationsToImport)
            {
                filterOrConditions.AddWithOr(PosalterationitemFields.ExternalPosalterationId == posProductPosAlteration.PosalterationExternalId);
            }

            filterComplete.Add(filterOrConditions);

            posalterationitemsToImport.GetMulti(filterComplete);

            // Don't have to continue, nothing related
            if (posalterationitemsToImport.Count == 0)
                return;

            // --

            posalterationsToImport = new PosalterationCollection();
            posalterationsToImport.AddToTransaction(this.transaction);

            filterComplete = new PredicateExpression();
            filterComplete.Add(PosalterationFields.CompanyId == this.companyId);
            filterOrConditions = new PredicateExpression();

            foreach (var posProductPosAlteration in posproductPosalterationsToImport)
            {
                filterOrConditions.AddWithOr(PosalterationFields.ExternalId == posProductPosAlteration.PosalterationExternalId);
            }

            filterComplete.Add(filterOrConditions);

            posalterationsToImport.GetMulti(filterComplete);

            // --                       

            posalterationoptionsToImport = new PosalterationoptionCollection();
            posalterationoptionsToImport.AddToTransaction(this.transaction);

            filterComplete = new PredicateExpression();
            filterComplete.Add(PosalterationoptionFields.CompanyId == this.companyId);
            filterOrConditions = new PredicateExpression();

            foreach (var posAlterationitem in posalterationitemsToImport)
            {
                filterOrConditions.AddWithOr(PosalterationoptionFields.ExternalId == posAlterationitem.ExternalPosalterationoptionId);
            }

            filterComplete.Add(filterOrConditions);

            posalterationoptionsToImport.GetMulti(filterComplete);

            // Don't have to continue, nothing related
            if (posalterationoptionsToImport.Count == 0)
                return;

            //--

            filterComplete = new PredicateExpression();
            filterComplete.Add(PosproductFields.CompanyId == this.companyId);
            filterOrConditions = new PredicateExpression();

            foreach (var posAlterationoption in posalterationoptionsToImport)
            {
                if (!posAlterationoption.PosproductExternalId.IsNullOrWhiteSpace())
                {
                    filterOrConditions.AddWithOr(PosproductFields.ExternalId == posAlterationoption.PosproductExternalId);
                }
            }

            // If none of the PosAlterationOptions are linked to a PosProductExternalId, don't import.
            if (filterOrConditions.Count > 0)
            {
                filterComplete.Add(filterOrConditions);

                posproductCollection.GetMulti(filterComplete);

                posProductsRequiredByAlterationsToImport.InsertRange(0, posproductCollection);
            }
        }


        /// <summary>
        /// Synchronize all Alterations
        /// </summary>
        /// <param name="posalterationsToImport">The posalterations to import.</param>
        /// <param name="overwriteName">Overwrite the name of the existing Alterations</param>
        private void SynchronizeAlterations(PosalterationCollection posalterationsToImport, bool overwriteName)
        {
            // Get the EntityView for the PosalterationCollection
            EntityView<PosalterationEntity> posalterationView = posalterationsToImport.DefaultView;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationFields.CompanyId == this.companyId);
            filter.Add(AlterationFields.Version == alterationVersion);

            // Get the AlterationCollection for the specified company
            AlterationCollection alterationCollection = new AlterationCollection();
            alterationCollection.AddToTransaction(this.transaction);
            alterationCollection.GetMulti(filter);

            // Get the EntityView for the AlterationCollection
            EntityView<AlterationEntity> alterationView = alterationCollection.DefaultView;

            // Walk through the POS alterations
            foreach (PosalterationEntity posalteration in posalterationView)
            {
                // Get a alteration for the current POS alteration
                AlterationEntity alteration = PosalterationHelper.GetAlterationByPosalterationId(posalteration.PosalterationId, alterationView);
                alteration.AddToTransaction(transaction);

                // Set a flag whether the alteration was new
                bool wasNew = alteration.IsNew;

                // Set the properties of the alteration
                alteration.CompanyId = this.companyId;
                alteration.PosalterationId = posalteration.PosalterationId;
                alteration.Version = alterationVersion;

                if (wasNew || overwriteName) // Only overwrite the name if the alteration was new or an update of the name was forced
                    alteration.Name = posalteration.Name.TurnFirstToUpper();

                // Set the minimum option count
                if (posalteration.MinOptions.HasValue)
                    alteration.MinOptions = posalteration.MinOptions.Value;
                else if (wasNew)
                    alteration.MinOptions = 1;

                // Set the maximum option count
                if (posalteration.MaxOptions.HasValue)
                {
                    if (posalteration.MaxOptions.Value > 0)
                        alteration.MaxOptions = posalteration.MaxOptions.Value;
                    else
                        alteration.MaxOptions = 1;
                }
                else if (wasNew || alteration.MaxOptions < 1)
                {
                    alteration.MaxOptions = 1;
                }

                try
                {
                    // Save the alteration
                    if (alteration.Save(true) && wasNew)
                    {
                        alteration.Refetch();
                        alterationView.RelatedCollection.Add(alteration);
                    }
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericResult.EntitySaveException, ex);
                }
            }
        }

        /// <summary>
        /// Synchronize all Alterationoptions
        /// </summary>
        /// <param name="posalterationOptionsToImport">The posalteration options to import.</param>
        /// <param name="overwriteName">Overwrite the name of existing Alterationoptions</param>
        /// <param name="overwritePriceAddition">Overwerite the price of the existing Alterationoptions</param>
        private void SynchronizeAlterationoptions(PosalterationoptionCollection posalterationOptionsToImport, bool overwriteName, bool overwritePriceAddition)
        {
            // Get the EntityView for the PosalterationoptionCollection
            EntityView<PosalterationoptionEntity> posalterationoptionView = posalterationOptionsToImport.DefaultView;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationoptionFields.CompanyId == this.companyId);
            filter.Add(AlterationoptionFields.Version == alterationVersion);

            // Get the AlterationoptionCollection for the specified company
            AlterationoptionCollection alterationoptionCollection = new AlterationoptionCollection();
            alterationoptionCollection.AddToTransaction(this.transaction);
            alterationoptionCollection.GetMulti(filter);

            // Get the EntityView for the AlterationoptionCollection
            EntityView<AlterationoptionEntity> alterationoptionView = alterationoptionCollection.DefaultView;

            // Walk through the POS alterationoptions
            PosproductCollection posproductCollectionForRetrieval = new PosproductCollection();
            posproductCollectionForRetrieval.AddToTransaction(this.transaction);
            foreach (PosalterationoptionEntity posalterationoption in posalterationoptionView)
            {
                // Get a alterationoption for the current POS alterationoption
                AlterationoptionEntity alterationoption = PosalterationoptionHelper.GetAlterationoptionByPosalterationoptionId(posalterationoption.PosalterationoptionId, alterationoptionView);
                alterationoption.AddToTransaction(this.transaction);
                // Set a flag whether the alterationoption was new
                bool wasNew = alterationoption.IsNew;

                // Set the properties of the alterationoption
                alterationoption.CompanyId = this.companyId;
                alterationoption.PosalterationoptionId = posalterationoption.PosalterationoptionId;
                alterationoption.Version = alterationVersion;

                // If a Alterationoption is used as a Posproduct:
                if (!posalterationoption.PosproductExternalId.IsNullOrWhiteSpace())
                {
                    PredicateExpression filterPosproduct = new PredicateExpression();
                    filterPosproduct.Add(PosproductFields.ExternalId == posalterationoption.PosproductExternalId);
                    filterPosproduct.Add(PosproductFields.CompanyId == this.companyId);

                    var posproduct = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<PosproductEntity>(filterPosproduct, posproductCollectionForRetrieval);

                    if (posproduct == null)
                        throw new ObymobiException(SynchronizeAlterationoptionsResult.RelatedPosproductNotFound, "Posproduct not found for Posalterationoption: '{0}' - '{1}' searched for Posproduct.ExternalId {2}",
                            posalterationoption.Name, posalterationoption.ExternalId, posalterationoption.PosproductExternalId);
                    else if (posproduct.ProductCollection.Count == 0)
                    {
                        throw new ObymobiException(SynchronizeAlterationoptionsResult.PosproductForAlterationOptionHasNoProduct, "No Product found for Posproduct. Posalterationoption: {0} (ExternalId: {1}), Searched for PosexternalproductId: {2}.",
                                                    posalterationoption.Name, posalterationoption.ExternalId, posalterationoption.PosproductExternalId);
                    }
                    else if (posproduct.ProductCollection.Count > 1)
                    {
                        throw new ObymobiException(SynchronizeAlterationoptionsResult.PosproductForAlterationOptionHasMultipleProduct, "Multiple Products found for Posproduct '{0}'. Posalterationoption: {1} (ExternalId: {2}), Searched for PosexternalproductId: {3}. Check whether product '{4}' is set to invisible!",
                                                    posproduct.Name, posalterationoption.Name, posalterationoption.ExternalId, posalterationoption.PosproductExternalId, posproduct.ProductCollection[0].Name);
                    }

                    alterationoption.PosproductId = posproduct.PosproductId;
                }

                if (wasNew || overwriteName) // Only overwrite the name if the alterationoption was new or an update of the name was forced
                    alterationoption.Name = posalterationoption.Name.TurnFirstToUpper();

                if (wasNew || overwritePriceAddition) // Only overwrite the price addition if the alterationoption was new or an update of the name was forced
                    alterationoption.PriceIn = posalterationoption.PriceIn;

                try
                {
                    // Save the alterationoption
                    if (alterationoption.Save(true) && wasNew)
                    {
                        alterationoption.Refetch();
                        alterationoptionView.RelatedCollection.Add(alterationoption);
                    }
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericResult.EntitySaveException, ex);
                }
            }
        }

        /// <summary>
        /// Synchronize AlterationItems
        /// </summary>
        private void SynchronizeAlterationitems(PosalterationitemCollection posalterationitemsToImport, AlterationDialogMode alterationDialogMode)
        {
            // Get the EntityView for the PosalterationitemCollection
            EntityView<PosalterationitemEntity> posalterationitemView = posalterationitemsToImport.DefaultView;

            PredicateExpression filter = new PredicateExpression(AlterationFields.CompanyId == this.companyId);
            filter.Add(AlterationitemFields.Version == alterationVersion);

            RelationCollection relations = new RelationCollection();
            relations.Add(AlterationitemEntity.Relations.AlterationEntityUsingAlterationId);

            // Get the AlterationitemCollection for the specified company
            AlterationitemCollection alterationitemCollection = new AlterationitemCollection();
            alterationitemCollection.AddToTransaction(this.transaction);
            alterationitemCollection.GetMulti(filter, relations);

            // Get the EntityView for the AlterationitemCollection
            EntityView<AlterationitemEntity> alterationitemView = alterationitemCollection.DefaultView;

            // Walk through the POS alterationitems
            foreach (PosalterationitemEntity posalterationitem in posalterationitemView)
            {
                // Get a alterationitem for the current POS alterationitem
                AlterationitemEntity alterationitem = PosalterationitemHelper.GetAlterationitemByPosalterationitemId(posalterationitem.PosalterationitemId, alterationitemView);
                alterationitem.AddToTransaction(transaction);

                // Set a flag whether the alterationitem was new
                bool wasNew = alterationitem.IsNew;

                // Set the properties of the alterationitem
                alterationitem.PosalterationitemId = posalterationitem.PosalterationitemId;

                alterationitem.AlterationId = this.alterationDataSource.GetAlterationIdByPosalterationExternalId(this.companyId, posalterationitem.ExternalPosalterationId, this.transaction, alterationDialogMode);
                alterationitem.AlterationoptionId = this.alterationoptionDataSource.GetAlterationoptionIdByPosalterationoptionExternalId(this.companyId, posalterationitem.ExternalPosalterationoptionId, this.transaction, alterationDialogMode);
                alterationitem.SelectedOnDefault = posalterationitem.SelectedOnDefault;
                alterationitem.Version = alterationVersion;

                // Only set the sort order if it's currently 0, because we don't want to overwrite custom settings
                if (alterationitem.SortOrder == 0)
                    alterationitem.SortOrder = posalterationitem.SortOrder;

                try
                {
                    // Save the alterationitem
                    if (alterationitem.Save(true) && wasNew)
                    {
                        alterationitem.Refetch();
                        alterationitemView.RelatedCollection.Add(alterationitem);
                    }
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericResult.EntitySaveException, ex);
                }
            }
        }

        /// <summary>
        /// Synchronize all ProductAlterations
        /// </summary>
        /// <param name="posproductPosAlterationsToImport">The posproduct pos alterations to import.</param>
        /// <param name="overwriteName">if set to <c>true</c> [overwrite name].</param>
        private void SynchronizeProductAlterations(PosproductPosalterationCollection posproductPosAlterationsToImport, bool overwriteName, AlterationDialogMode alterationDialogMode)
        {
            // Get the EntityView for the PosproductPosalterationCollection
            EntityView<PosproductPosalterationEntity> posproductPosalterationView = posproductPosAlterationsToImport.DefaultView;

            PredicateExpression filter = new PredicateExpression(AlterationFields.CompanyId == this.companyId);
            RelationCollection relations = new RelationCollection();
            relations.Add(ProductAlterationEntity.Relations.AlterationEntityUsingAlterationId);

            // Get the ProductAlterationCollection for the specified company
            ProductAlterationCollection productAlterationCollection = new ProductAlterationCollection();
            productAlterationCollection.AddToTransaction(this.transaction);
            productAlterationCollection.GetMulti(filter, relations);

            // Get the EntityView for the ProductAlterationCollection
            EntityView<ProductAlterationEntity> productAlterationView = productAlterationCollection.DefaultView;

            // Walk through the Posproduct Posalterations
            foreach (PosproductPosalterationEntity posproductPosalteration in posproductPosalterationView)
            {
                // Skip user deleted
                if (posproductPosalteration.DeletedFromCms)
                    continue;

                // Get a product alteration for the current posproduct posalteration
                ProductAlterationEntity productAlteration = PosproductPosalterationHelper.GetProductAlterationByPosproductPosalterationId(posproductPosalteration.PosproductPosalterationId, productAlterationView);
                productAlteration.AddToTransaction(this.transaction);

                // Set a flag whether the alterationitem was new
                bool wasNew = productAlteration.IsNew;

                // Set the properties of the productAlteration
                productAlteration.PosproductPosalterationId = posproductPosalteration.PosproductPosalterationId;

                productAlteration.ProductId = ProductHelper.GetProductIdByPosproductExternalId(this.companyId, posproductPosalteration.PosproductExternalId, this.transaction);
                productAlteration.AlterationId = this.alterationDataSource.GetAlterationIdByPosalterationExternalId(this.companyId, posproductPosalteration.PosalterationExternalId, this.transaction, alterationDialogMode);

                try
                {
                    // Save the alterationitem
                    if (productAlteration.Save(true) && wasNew)
                    {
                        productAlteration.Refetch();
                        productAlterationView.RelatedCollection.Add(productAlteration);
                    }
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericResult.EntitySaveException, ex);
                }
            }
        }

        /// <summary>
        /// Synchronize Deliverypointgroups
        /// </summary>
        /// <param name="addNewDeliverypointgroups">Add Deliverypointgroups if not yet existing</param>
        /// <param name="updateExistingDeliverypointgroups">Update the details of existing Deliverypointgroups</param>
        public void SynchronizeDeliverypointgroups(bool addNewDeliverypointgroups, bool updateExistingDeliverypointgroups)
        {
            // Get the PosdeliverypointgroupCollection for the specified company
            PosdeliverypointgroupCollection posdeliverypointgroupCollection = new PosdeliverypointgroupCollection();
            posdeliverypointgroupCollection.AddToTransaction(this.transaction);
            posdeliverypointgroupCollection.GetMulti(new PredicateExpression(PosdeliverypointgroupFields.CompanyId == this.companyId));

            // Get the DeliverypointgroupCollection for the specified company
            DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
            deliverypointgroupCollection.AddToTransaction(transaction);
            deliverypointgroupCollection.GetMulti(new PredicateExpression(DeliverypointgroupFields.CompanyId == this.companyId));

            // Get the EntityView for the DeliverypointgroupCollection
            EntityView<DeliverypointgroupEntity> deliverypointgroupView = deliverypointgroupCollection.DefaultView;
            PredicateExpression filter = null;

            for (int i = 0; i < posdeliverypointgroupCollection.Count; i++)
            {
                PosdeliverypointgroupEntity posdeliverypointgroupEntity = posdeliverypointgroupCollection[i];

                filter = new PredicateExpression(DeliverypointgroupFields.PosdeliverypointgroupId == posdeliverypointgroupEntity.PosdeliverypointgroupId);
                deliverypointgroupView.Filter = filter;

                if (deliverypointgroupView.Count == 0 && addNewDeliverypointgroups)
                {
                    // No deliverypoint found for the specified posdeliverypoint
                    DeliverypointgroupEntity deliverypointgroupEntity = new DeliverypointgroupEntity();
                    deliverypointgroupEntity.AddToTransaction(this.transaction);
                    //deliverypointgroupEntity.TerminalId = terminalId;
                    deliverypointgroupEntity.CompanyId = this.companyId;
                    deliverypointgroupEntity.Name = posdeliverypointgroupEntity.Name;
                    deliverypointgroupEntity.Active = true;
                    deliverypointgroupEntity.PosdeliverypointgroupId = posdeliverypointgroupEntity.PosdeliverypointgroupId;
                    try
                    {
                        deliverypointgroupEntity.Save();
                    }
                    catch (Exception ex)
                    {
                        throw new ObymobiException(GenericResult.EntitySaveException, ex);
                    }
                }
                else if (deliverypointgroupView.Count > 0 && updateExistingDeliverypointgroups)
                {
                    for (int j = 0; j < deliverypointgroupView.Count; j++)
                    {
                        DeliverypointgroupEntity deliverypointgroupEntity = deliverypointgroupView[j];
                        deliverypointgroupEntity.AddToTransaction(this.transaction);

                        deliverypointgroupEntity.CompanyId = this.companyId;
                        deliverypointgroupEntity.Name = posdeliverypointgroupEntity.Name;
                        deliverypointgroupEntity.Active = true;
                        deliverypointgroupEntity.PosdeliverypointgroupId = posdeliverypointgroupEntity.PosdeliverypointgroupId;
                        try
                        {
                            deliverypointgroupEntity.Save();
                        }
                        catch (Exception ex)
                        {
                            throw new ObymobiException(GenericResult.EntitySaveException, ex);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Synchronize Deliverypoints
        /// </summary>
        /// <param name="addNewDeliverypoints">Add Deliverypoints if not yet existing</param>
        /// <param name="updateExistingDeliverypoints">Update the details of existing Deliverypoints</param>
        public void SynchronizeDeliverypoints(bool addNewDeliverypoints, bool updateExistingDeliverypoints)
        {
            PosdeliverypointCollection posdeliverypointCollection = new PosdeliverypointCollection();
            posdeliverypointCollection.AddToTransaction(this.transaction);
            posdeliverypointCollection.GetMulti(new PredicateExpression(PosdeliverypointFields.CompanyId == this.companyId));

            PredicateExpression filter2 = new PredicateExpression(DeliverypointgroupFields.CompanyId == this.companyId);
            RelationCollection relations = new RelationCollection(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.AddToTransaction(this.transaction);
            deliverypointCollection.GetMulti(filter2, relations);

            EntityView<DeliverypointEntity> deliverypointView = deliverypointCollection.DefaultView;
            PredicateExpression filter = null;

            for (int i = 0; i < posdeliverypointCollection.Count; i++)
            {
                PosdeliverypointEntity posdeliverypointEntity = posdeliverypointCollection[i];

                filter = new PredicateExpression(DeliverypointFields.PosdeliverypointId == posdeliverypointEntity.PosdeliverypointId);
                deliverypointView.Filter = filter;

                if (deliverypointView.Count == 0 && addNewDeliverypoints)
                {
                    // No deliverypoint found for the specified posdeliverypoint
                    DeliverypointEntity deliverypointEntity = new DeliverypointEntity();
                    deliverypointEntity.CompanyId = this.companyId;
                    deliverypointEntity.DeliverypointgroupId = PosdeliverypointHelper.GetDeliverypointgroupId(posdeliverypointEntity.ExternalPosdeliverypointgroupId, this.companyId);
                    deliverypointEntity.Name = posdeliverypointEntity.Name;

                    string number = string.Empty;
                    foreach (char c in posdeliverypointEntity.Name)
                    {
                        if (char.IsDigit(c))
                            number += c;
                    }
                    if (number.Length > 0)
                        deliverypointEntity.Number = number;
                    else
                    {
                        // There's no number in the name, different approach:
                        deliverypointEntity.Number = string.Format("{0} ({1})", posdeliverypointEntity.Name, posdeliverypointEntity.ExternalId);
                    }

                    if (deliverypointEntity.Name.IsNullOrWhiteSpace())
                        deliverypointEntity.Name = deliverypointEntity.Number;

                    deliverypointEntity.PosdeliverypointId = posdeliverypointEntity.PosdeliverypointId;
                    try
                    {
                        deliverypointEntity.Save();
                    }
                    catch (Exception ex)
                    {
                        throw new ObymobiException(GenericResult.EntitySaveException, ex);
                    }
                }
                else if (deliverypointView.Count > 0 && updateExistingDeliverypoints)
                {
                    for (int j = 0; j < deliverypointView.Count; j++)
                    {
                        DeliverypointEntity deliverypointEntity = deliverypointView[j];
                        deliverypointEntity.CompanyId = this.companyId;
                        deliverypointEntity.DeliverypointgroupId = PosdeliverypointHelper.GetDeliverypointgroupId(posdeliverypointEntity.ExternalPosdeliverypointgroupId, this.companyId);
                        deliverypointEntity.Name = posdeliverypointEntity.Name;
                        deliverypointEntity.Number = posdeliverypointEntity.ExternalId;
                        deliverypointEntity.PosdeliverypointId = posdeliverypointEntity.PosdeliverypointId;
                        deliverypointEntity.PosdeliverypointEntity.RevenueCenter = posdeliverypointEntity.RevenueCenter;
                        try
                        {
                            deliverypointEntity.Save();
                        }
                        catch (Exception ex)
                        {
                            throw new ObymobiException(GenericResult.EntitySaveException, ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Synchronize Deliverypoints
        /// </summary>
        /// <param name="posDeliverypointIds">PosdeliverypointIds to import</param>
        /// <param name="deliverypointgroupId">The deliverypointgroup id.</param>
        public void SynchronizeDeliverypoints(List<int> posDeliverypointIds, int? deliverypointgroupId)
        {
            PosdeliverypointCollection posdeliverypointCollection = new PosdeliverypointCollection();
            posdeliverypointCollection.AddToTransaction(this.transaction);
            PredicateExpression posdeliveryPointFilter = new PredicateExpression(PosdeliverypointFields.CompanyId == this.companyId);
            posdeliveryPointFilter.Add(PosdeliverypointFields.PosdeliverypointId == posDeliverypointIds);
            posdeliverypointCollection.GetMulti(posdeliveryPointFilter);

            PredicateExpression filter2 = new PredicateExpression(DeliverypointgroupFields.CompanyId == this.companyId);
            RelationCollection relations = new RelationCollection(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.AddToTransaction(this.transaction);
            deliverypointCollection.GetMulti(filter2, relations);

            DeliverypointgroupCollection deliverypointGroups = new DeliverypointgroupCollection();
            PrefetchPath deliverypointGroupsPath = new PrefetchPath(EntityType.DeliverypointgroupEntity);
            deliverypointGroupsPath.Add(DeliverypointgroupEntity.PrefetchPathPosdeliverypointgroupEntity);
            deliverypointGroups.GetMulti(filter2, deliverypointGroupsPath);

            EntityView<DeliverypointEntity> deliverypointView = deliverypointCollection.DefaultView;
            PredicateExpression filter = null;

            for (int i = 0; i < posdeliverypointCollection.Count; i++)
            {
                PosdeliverypointEntity posdeliverypointEntity = posdeliverypointCollection[i];

                filter = new PredicateExpression(DeliverypointFields.PosdeliverypointId == posdeliverypointEntity.PosdeliverypointId);
                deliverypointView.Filter = filter;

                if (deliverypointView.Count == 0)
                {
                    // No deliverypoint found for the specified posdeliverypoint
                    DeliverypointEntity deliverypointEntity = new DeliverypointEntity();
                    deliverypointEntity.CompanyId = this.companyId;
                    if (deliverypointgroupId.HasValue)
                    {
                        deliverypointEntity.DeliverypointgroupId = deliverypointgroupId.Value;
                    }
                    else
                    {
                        deliverypointEntity.DeliverypointgroupId = PosdeliverypointHelper.GetDeliverypointgroupId(posdeliverypointEntity.ExternalPosdeliverypointgroupId, this.companyId);
                    }

                    deliverypointEntity.Name = posdeliverypointEntity.Name;

                    // GK This is a bit of tricky with for example 'Bar 3' and 'Lounge 3', both would end up as '3' 
                    // GK WARING: Code duplicate - See line ~897
                    string number = string.Empty;
                    foreach (char c in posdeliverypointEntity.Name)
                    {
                        if (char.IsDigit(c))
                            number += c;
                    }
                    if (number.Length > 0)
                        deliverypointEntity.Number = number;
                    else
                    {
                        // There's no number in the name, different approach:
                        deliverypointEntity.Number = string.Format("{0} ({1})", posdeliverypointEntity.Name, posdeliverypointEntity.ExternalId);
                    }

                    if (deliverypointEntity.Name.IsNullOrWhiteSpace())
                        deliverypointEntity.Name = deliverypointEntity.Number;

                    deliverypointEntity.PosdeliverypointId = posdeliverypointEntity.PosdeliverypointId;
                    try
                    {
                        deliverypointEntity.Save();
                    }
                    catch (ObymobiException ex)
                    {
                        throw new ObymobiException(GenericResult.EntitySaveException, ex);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
        }
    }
}
