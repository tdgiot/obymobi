﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Integrations.POS.Interfaces;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// AlohaConfigurationAdapter class
    /// </summary>
    public class AlohaConfigurationAdapter : IConfigurationAdapter
    {
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Terminal terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.EmployeeNumber = terminal.PosValue2;
            this.EmployeePassword = terminal.PosValue3;
            this.JobCodeID = terminal.PosValue4;
            this.SessionToken = terminal.PosValue5;
            this.RequestID = terminal.PosValue6;
            this.Identifier = terminal.PosValue7;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.EmployeeNumber = terminal.PosValue2;
            this.EmployeePassword = terminal.PosValue3;
            this.JobCodeID = terminal.PosValue4;
            this.SessionToken = terminal.PosValue5;
            this.RequestID = terminal.PosValue6;
            this.Identifier = terminal.PosValue7;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.WebserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AlohaWebserviceUrl);
            this.EmployeeNumber = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AlohaEmployeeNumber);
            this.EmployeePassword = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AlohaEmployeePassword);
            this.JobCodeID = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AlohaJobCodeID);
            this.SessionToken = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AlohaSessionToken);
            this.RequestID = Dionysos.ConfigurationManager.GetInt(POSConfigurationConstants.AlohaRequestID).ToString();
            this.Identifier = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AlohaIdentifier);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AlohaWebserviceUrl, this.WebserviceUrl);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AlohaEmployeeNumber, this.EmployeeNumber);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AlohaEmployeePassword, this.EmployeePassword);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AlohaJobCodeID, this.JobCodeID);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AlohaSessionToken, this.SessionToken);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AlohaRequestID, this.RequestID);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AlohaIdentifier, this.Identifier);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.EmployeeNumber;
            terminal.PosValue3 = this.EmployeePassword;
            terminal.PosValue4 = this.JobCodeID;
            terminal.PosValue5 = this.SessionToken;
            terminal.PosValue6 = this.RequestID;
            terminal.PosValue7 = this.Identifier;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.EmployeeNumber;
            terminal.PosValue3 = this.EmployeePassword;
            terminal.PosValue4 = this.JobCodeID;
            terminal.PosValue5 = this.SessionToken;
            terminal.PosValue6 = this.RequestID;
            terminal.PosValue7 = this.Identifier;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the webservice URL.
        /// </summary>
        /// <value>
        /// The webservice URL.
        /// </value>
        public string WebserviceUrl { get; set; }
        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }
        /// <summary>
        /// Gets or sets the employee password.
        /// </summary>
        /// <value>
        /// The employee password.
        /// </value>
        public string EmployeePassword { get; set; }
        /// <summary>
        /// Gets or sets the job code ID.
        /// </summary>
        /// <value>
        /// The job code ID.
        /// </value>
        public string JobCodeID { get; set; }
        /// <summary>
        /// Gets or sets the session token.
        /// </summary>
        /// <value>
        /// The session token.
        /// </value>
        public string SessionToken { get; set; }
        /// <summary>
        /// Gets or sets the request ID.
        /// </summary>
        /// <value>
        /// The request ID.
        /// </value>
        public string RequestID { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Identifier { get; set; }

        #endregion
    }
}
