﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Logic.Model;
using System.Globalization;
using Obymobi.Logic.Loggers;
using Obymobi.Enums;
using System.Data;
using System.IO;
using Dionysos.XML;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// AlohaConnector class
    /// </summary>
    public class AlohaConnector : IPOSConnector
    {
        #region Fields

        private CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");

        private string url = string.Empty;
        private int employeeNumber = 101;
        private string employeePassword = string.Empty;
        private int jobCodeID = 10;
        private int requestID = 1234;
        private string identifier = "123456789";

        private bool localMode = false;
        private string localFile = string.Empty;

        private int markerID = 0;

        private string sessionToken = string.Empty;
        private List<int> freeslots = new List<int>();

        private bool isEmployeeLogin = false;
        private bool isTableOpen = false;
        private bool isCheckOpen = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AlohaConnector"/> class.
        /// </summary>
        public AlohaConnector()
        {
            this.Initialize();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the AlohaConnector by initializing the configuration  
        /// </summary>
        private void Initialize()
        {
            this.WriteToLogExtended("Initialize", "Start");

            try
            {
                this.url = ConfigurationManager.GetString(POSConfigurationConstants.AlohaWebserviceUrl);
                this.employeeNumber = ConfigurationManager.GetInt(POSConfigurationConstants.AlohaEmployeeNumber);
                this.employeePassword = ConfigurationManager.GetString(POSConfigurationConstants.AlohaEmployeePassword);
                this.jobCodeID = ConfigurationManager.GetInt(POSConfigurationConstants.AlohaJobCodeID);
                //this.requestID = ConfigurationManager.GetInt(POSConfigurationConstants.AlohaRequestID);
                //this.identifier = ConfigurationManager.GetString(POSConfigurationConstants.AlohaIdentifier);
                this.localMode = ConfigurationManager.GetBool(POSConfigurationConstants.AlohaLocalMode);
                this.localFile = ConfigurationManager.GetString(POSConfigurationConstants.AlohaLocalFile);
                this.markerID = ConfigurationManager.GetInt(POSConfigurationConstants.AlohaMarkerID);
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, "An exception was thrown while initializing the AlohaConnector. Exception: {0}", ex.Message);
            }

            this.WriteToLogExtended("Initialize", "End");
        }

        #region IPOSConnector methods

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public Model.Posproduct[] GetPosproducts()
        {
            List<Posproduct> posproducts = new List<Posproduct>();
            bool success = true;

            this.WriteToLogExtended("GetPosproducts", "Start");

            // Vectron culture for numbers
            var culture = CultureInfo.CreateSpecificCulture("en-GB");

            if (!this.localMode)
            {
                try
                {
                    // Try to get the profiles
                    this.WriteToLogExtended("GetPosproducts", "GetProfiles");
                    AlohaResponse getProfilesResponse = this.GetProfiles();
                    if (getProfilesResponse.ErrorCode == 0)
                    {
                        XmlDoc xmlDoc = new XmlDoc();
                        xmlDoc.ParseXml(getProfilesResponse.Data);

                        // ReturnData node                    
                        XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                        if (returnDataNode != null)
                        {
                            // FreeSlots node
                            XmlNode freeSlotsNode = returnDataNode.GetSingleChildNodeByName("FreeSlots");
                            if (freeSlotsNode != null)
                            {
                                // Slots nodes
                                List<XmlNode> slotNodes = freeSlotsNode.GetChildNodesByName("Slot");
                                if (slotNodes != null && slotNodes.Count > 0)
                                {
                                    foreach (XmlNode slotNode in slotNodes)
                                    {
                                        // SlotID
                                        XmlNode slotIDNode = slotNode.GetSingleChildNodeByName("SlotID");
                                        if (slotIDNode != null)
                                        {
                                            int slotID = 0;
                                            if (int.TryParse(slotIDNode.Value, out slotID))   // SlotID
                                            {
                                                this.freeslots.Add(slotID);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, "An exception was thrown while trying to get the products. Exception: {0}", ex.Message);
                }
            }

            this.WriteToLogExtended("GetPosproducts", "IfSuccess");
            if (success)
            {
                try
                {
                    this.WriteToLogExtended("GetPosproducts", "GetStaticFiles - LocalMode: {0}", this.localMode);

                    XmlDoc xmlDoc = new XmlDoc();

                    if (!this.localMode)
                    {
                        AlohaResponse getStaticFilesResponse = this.GetStaticFiles();
                        if (getStaticFilesResponse.ErrorCode == 0)
                        {
                            xmlDoc.ParseXml(getStaticFilesResponse.Data);
                        }
                        else
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts,
                                "An invalid response was returned while trying to get the static files. Error: {0}", getStaticFilesResponse.ErrorMessage);
                        }
                    }
                    else
                    {
                        if (this.localFile.IsNullOrWhiteSpace())
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts,
                                "The local file has not been configured in the configuration!");
                        }
                        else if (!File.Exists(this.localFile))
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, "The local static files file could not be located at '{0}'", this.localFile);
                        }
                        else
                        {
                            xmlDoc.ParseXml(File.ReadAllText(this.localFile));
                        }
                    }

                    this.WriteToLogExtended("GetPosproducts", "Is success: {0}", success);

                    if (success)
                    {
                        // Files node
                        XmlNode filesNode = null;

                        if (!this.localMode)
                        {
                            // ReturnData node
                            XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                            if (returnDataNode != null)
                            {
                                // Files node
                                filesNode = returnDataNode.GetSingleChildNodeByName("Files");
                            }
                        }

                        this.WriteToLogExtended("GetPosproducts", "Xml Files Node is null: {0}", (filesNode == null));

                        if (filesNode != null || this.localMode)
                        {
                            // Items node
                            XmlNode itemsNode = null;

                            if (!this.localMode)
                                itemsNode = filesNode.GetSingleChildNodeByName("Items");
                            else
                                itemsNode = xmlDoc.GetSingleChildNodeByName("Items");

                            if (itemsNode != null)
                            {
                                // Item nodes
                                List<XmlNode> itemNodes = itemsNode.GetChildNodesByName("Item");
                                if (itemNodes != null && itemNodes.Count > 0)
                                {
                                    foreach (XmlNode itemNode in itemNodes)
                                    {
                                        XmlNode f00Node = itemNode.GetSingleChildNodeByName("F00");
                                        XmlNode f01Node = itemNode.GetSingleChildNodeByName("F01");
                                        XmlNode f61Node = itemNode.GetSingleChildNodeByName("F61");

                                        if (f00Node != null && f01Node != null)
                                        {
                                            Posproduct posproduct = new Posproduct();

                                            posproduct.ExternalId = f00Node.Value;          // ProductId
                                            posproduct.Name = f01Node.Value.HtmlEncode();   // Name

                                            decimal price = 0;
                                            if (f61Node != null && decimal.TryParse(f61Node.Value, NumberStyles.Number, culture, out price))
                                                posproduct.PriceIn = price;
                                            else
                                                posproduct.PriceIn = 0;

                                            posproduct.ExternalPoscategoryId = "unknown";           // ExternalCategoryId                                                       
                                            posproduct.SortOrder = 0;                               // SortOrder
                                            posproduct.Description = string.Empty;                  // Description
                                            posproduct.VatTariff = 1;

                                            // Try to get the category id

                                            // Submenus node
                                            XmlNode submenusNode = null;

                                            if (!this.localMode)
                                                submenusNode = filesNode.GetSingleChildNodeByName("Submenus");
                                            else
                                                submenusNode = xmlDoc.GetSingleChildNodeByName("Submenus");

                                            if (submenusNode != null)
                                            {
                                                // Submenu nodes
                                                List<XmlNode> submenuNodes = submenusNode.GetChildNodesByName("Submenu");
                                                if (submenuNodes != null && submenuNodes.Count > 0)
                                                {
                                                    foreach (XmlNode submenuNode in submenuNodes)
                                                    {
                                                        XmlNode idNode = submenuNode.GetSingleChildNodeByName("F00");
                                                        if (idNode != null)
                                                        {
                                                            foreach (XmlNode node in submenuNode.ChildNodes)
                                                            {
                                                                if (node.Name.ToUpper().StartsWith("I") && node.Value.Equals(f00Node.Value, StringComparison.InvariantCultureIgnoreCase))
                                                                {
                                                                    posproduct.ExternalPoscategoryId = idNode.Value;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            // Try to get the alterations
                                            List<Posalteration> posalterations = new List<Posalteration>();
                                            foreach (XmlNode node in itemNode.ChildNodes)
                                            {
                                                if (node.Name.ToUpper().StartsWith("M"))
                                                {
                                                    string modifierId = node.Value;

                                                    // Modifiers node
                                                    XmlNode modifiersNode = null;

                                                    if (!this.localMode)
                                                        modifiersNode = filesNode.GetSingleChildNodeByName("Modifiers");
                                                    else
                                                        modifiersNode = xmlDoc.GetSingleChildNodeByName("Modifiers");

                                                    if (modifiersNode != null)
                                                    {
                                                        // Modifer nodes
                                                        List<XmlNode> modifierNodes = modifiersNode.GetChildNodesByName("Modifier");
                                                        if (modifierNodes != null && modifierNodes.Count > 0)
                                                        {
                                                            foreach (XmlNode modifierNode in modifierNodes)
                                                            {
                                                                XmlNode idNode = modifierNode.GetSingleChildNodeByName("F00");
                                                                XmlNode nameNode = modifierNode.GetSingleChildNodeByName("F01");
                                                                XmlNode minNode = modifierNode.GetSingleChildNodeByName("F06");
                                                                XmlNode maxNode = modifierNode.GetSingleChildNodeByName("F07");

                                                                if (idNode != null && idNode.Value.Equals(modifierId, StringComparison.InvariantCultureIgnoreCase) && nameNode != null && minNode != null && maxNode != null)
                                                                {
                                                                    // We have found the modifier
                                                                    Posalteration posalteration = new Posalteration();
                                                                    posalteration.Name = nameNode.Value;
                                                                    posalteration.ExternalId = idNode.Value;

                                                                    int min = 0;
                                                                    int.TryParse(minNode.Value, out min);
                                                                    posalteration.MinOptions = min;

                                                                    int max = 1;
                                                                    int.TryParse(maxNode.Value, out max);
                                                                    posalteration.MaxOptions = max;

                                                                    posalteration.FieldValue1 = max.ToString();        // Maximum alteration option count

                                                                    List<Posalterationoption> posalterationoptions = new List<Posalterationoption>();
                                                                    foreach (XmlNode temp in modifierNode.ChildNodes)
                                                                    {
                                                                        if (temp.Name.ToUpper().StartsWith("I"))
                                                                        {
                                                                            Posalterationoption posalterationoption = new Posalterationoption();

                                                                            foreach (XmlNode itemNode2 in itemNodes)
                                                                            {
                                                                                XmlNode itemIdNode = itemNode2.GetSingleChildNodeByName("F00");
                                                                                XmlNode itemNameNode = itemNode2.GetSingleChildNodeByName("F01");
                                                                                XmlNode itemPriceNode = itemNode2.GetSingleChildNodeByName("F61");

                                                                                if (itemIdNode != null && itemNameNode != null && itemIdNode.Value.Equals(temp.Value, StringComparison.InvariantCultureIgnoreCase))
                                                                                {
                                                                                    posalterationoption.Name = itemNameNode.Value;

                                                                                    decimal price2 = 0;
                                                                                    if (itemPriceNode != null && decimal.TryParse(itemPriceNode.Value, NumberStyles.Number, culture, out price2))
                                                                                        posalterationoption.PriceIn = price2;
                                                                                    else
                                                                                        posalterationoption.PriceIn = 0;

                                                                                    break;
                                                                                }
                                                                            }

                                                                            posalterationoption.ExternalId = temp.Value;
                                                                            posalterationoptions.Add(posalterationoption);
                                                                        }
                                                                    }

                                                                    posalteration.Posalterationoptions = posalterationoptions.ToArray();
                                                                    posalterations.Add(posalteration);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            posproduct.Posalterations = posalterations.ToArray();

                                            if (!posproduct.Name.IsNullOrWhiteSpace())
                                                posproducts.Add(posproduct);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    this.WriteToLogExtended("GetPosproducts", "Exception 1: {0}", ex.Message);
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, "An exception was thrown while trying to get the static files. Exception: {0}", ex.Message);
                }
            }

            this.WriteToLogExtended("GetPosproducts", "Products: {0}", posproducts.Count);

            this.WriteToLogExtended("GetPosproducts", "End");

            return posproducts.ToArray();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public Model.Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();
            bool success = true;

            this.WriteToLogExtended("GetPoscategories", "Start");

            if (!this.localMode)
            {
                try
                {
                    // Try to get the profiles
                    AlohaResponse getProfilesResponse = this.GetProfiles();
                    if (getProfilesResponse.ErrorCode == 0)
                    {
                        XmlDoc xmlDoc = new XmlDoc();
                        xmlDoc.ParseXml(getProfilesResponse.Data);

                        // ReturnData node
                        XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                        if (returnDataNode != null)
                        {
                            // FreeSlots node
                            XmlNode freeSlotsNode = returnDataNode.GetSingleChildNodeByName("FreeSlots");
                            if (freeSlotsNode != null)
                            {
                                // Slots nodes
                                List<XmlNode> slotNodes = freeSlotsNode.GetChildNodesByName("Slot");
                                if (slotNodes != null && slotNodes.Count > 0)
                                {
                                    foreach (XmlNode slotNode in slotNodes)
                                    {
                                        // SlotID
                                        XmlNode slotIDNode = slotNode.GetSingleChildNodeByName("SlotID");
                                        if (slotIDNode != null)
                                        {
                                            int slotID = 0;
                                            if (int.TryParse(slotIDNode.Value, out slotID))   // SlotID
                                            {
                                                this.freeslots.Add(slotID);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    this.WriteToLogExtended("GetPoscategories", "Exception: {0}", ex.Message);
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, "An exception was thrown while trying to get the profiles. Exception: {0}", ex.Message);
                }
            }

            if (success)
            {
                try
                {
                    this.WriteToLogExtended("GetPoscategories", "GetStaticFiles");

                    XmlDoc xmlDoc = new XmlDoc();

                    if (!this.localMode)
                    {
                        AlohaResponse getStaticFilesResponse = this.GetStaticFiles();
                        if (getStaticFilesResponse.ErrorCode == 0)
                        {
                            xmlDoc.ParseXml(getStaticFilesResponse.Data);
                        }
                        else
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories,
                                "An invalid response was returned while trying to get the static files. Error: {0}", getStaticFilesResponse.ErrorMessage);
                        }
                    }
                    else
                    {
                        if (this.localFile.IsNullOrWhiteSpace())
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, "The local file has not been configured in the configuration!");
                        }
                        else if (!File.Exists(this.localFile))
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, "The local static files file could not be located at '{0}'", this.localFile);
                        }
                        else
                        {
                            xmlDoc.ParseXml(File.ReadAllText(this.localFile));
                        }
                    }

                    if (success)
                    {
                        // Files node
                        XmlNode filesNode = null;

                        if (!this.localMode)
                        {
                            // ReturnData node
                            XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                            if (returnDataNode != null)
                            {
                                // Files node
                                filesNode = returnDataNode.GetSingleChildNodeByName("Files");
                            }
                        }

                        if (filesNode != null || this.localMode)
                        {
                            // Submenus node
                            XmlNode submenusNode = null;

                            if (!this.localMode)
                                submenusNode = filesNode.GetSingleChildNodeByName("Submenus");
                            else
                                submenusNode = xmlDoc.GetSingleChildNodeByName("Submenus");

                            if (submenusNode != null)
                            {
                                // Submenu nodes
                                List<XmlNode> submenuNodes = submenusNode.GetChildNodesByName("Submenu");
                                if (submenuNodes != null && submenuNodes.Count > 0)
                                {
                                    foreach (XmlNode submenuNode in submenuNodes)
                                    {
                                        XmlNode f00Node = submenuNode.GetSingleChildNodeByName("F00");
                                        XmlNode f01Node = submenuNode.GetSingleChildNodeByName("F01");

                                        if (f00Node != null && f01Node != null)
                                        {
                                            Poscategory poscategory = new Poscategory();

                                            poscategory.ExternalId = f00Node.Value;            // ExternalId
                                            poscategory.Name = f01Node.Value.HtmlEncode();     // Name

                                            if (!poscategory.Name.IsNullOrWhiteSpace())
                                                poscategories.Add(poscategory);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories,
                        "An exception was thrown while trying to get the static files. Exception: {0}", ex.Message);
                }
            }

            this.WriteToLogExtended("GetPoscategories", "End");

            return poscategories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();
            this.WriteToLogExtended("GetPosdeliverypointgroups", "Start");
            this.WriteToLogExtended("GetPosdeliverypointgroups", "End");
            return posdeliverypointgroups.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();
            bool success = true;

            this.WriteToLogExtended("GetPosdeliverypoints", "Start");

            if (!this.localMode)
            {
                try
                {
                    // Try to get the profiles
                    AlohaResponse getProfilesResponse = this.GetProfiles();

                    if (getProfilesResponse.ErrorCode == 0)
                    {
                        XmlDoc xmlDoc = new XmlDoc();
                        xmlDoc.ParseXml(getProfilesResponse.Data);

                        // ReturnData node
                        XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                        if (returnDataNode != null)
                        {
                            // FreeSlots node
                            XmlNode freeSlotsNode = returnDataNode.GetSingleChildNodeByName("FreeSlots");
                            if (freeSlotsNode != null)
                            {
                                // Slots nodes
                                List<XmlNode> slotNodes = freeSlotsNode.GetChildNodesByName("Slot");
                                if (slotNodes != null && slotNodes.Count > 0)
                                {
                                    foreach (XmlNode slotNode in slotNodes)
                                    {
                                        // SlotID
                                        XmlNode slotIDNode = slotNode.GetSingleChildNodeByName("SlotID");
                                        if (slotIDNode != null)
                                        {
                                            int slotID = 0;
                                            if (int.TryParse(slotIDNode.Value, out slotID))   // SlotID
                                            {
                                                this.freeslots.Add(slotID);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        success = false;
                        throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints,
                            "An invalid response was returned while trying to get the profiles. Error: {0}", getProfilesResponse.ErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints,
                        "An exception was thrown while trying to get the profiles. Exception: {0}", ex.Message);
                }
            }

            if (success)
            {
                try
                {
                    XmlDoc xmlDoc = new XmlDoc();

                    if (!this.localMode)
                    {
                        AlohaResponse getStaticFilesResponse = this.GetStaticFiles();
                        if (getStaticFilesResponse.ErrorCode == 0)
                        {
                            xmlDoc.ParseXml(getStaticFilesResponse.Data);
                        }
                        else
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints,
                                "An invalid response was returned while trying to get the static files. Error: {0}", getStaticFilesResponse.ErrorMessage);
                        }
                    }
                    else
                    {
                        if (this.localFile.IsNullOrWhiteSpace())
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints, "The local file has not been configured in the configuration!");
                        }
                        else if (!File.Exists(this.localFile))
                        {
                            success = false;
                            throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints,
                                "The local static files file could not be located at '{0}'", this.localFile);
                        }
                        else
                        {
                            xmlDoc.ParseXml(File.ReadAllText(this.localFile));
                        }
                    }

                    if (success)
                    {
                        // Files node
                        XmlNode filesNode = null;

                        if (!this.localMode)
                        {
                            // ReturnData node
                            XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                            if (returnDataNode != null)
                            {
                                // Files node
                                filesNode = returnDataNode.GetSingleChildNodeByName("Files");
                            }
                        }

                        if (filesNode != null || this.localMode)
                        {
                            // Submenus node
                            XmlNode submenusNode = null;

                            if (!this.localMode)
                                submenusNode = filesNode.GetSingleChildNodeByName("Tables");
                            else
                                submenusNode = xmlDoc.GetSingleChildNodeByName("Tables");

                            if (submenusNode != null)
                            {
                                // Submenu nodes
                                List<XmlNode> submenuNodes = submenusNode.GetChildNodesByName("Table");
                                if (submenuNodes != null && submenuNodes.Count > 0)
                                {
                                    foreach (XmlNode submenuNode in submenuNodes)
                                    {
                                        XmlNode f00Node = submenuNode.GetSingleChildNodeByName("F00");
                                        XmlNode f01Node = submenuNode.GetSingleChildNodeByName("F01");

                                        if (f00Node != null && f01Node != null)
                                        {
                                            Posdeliverypoint posdeliverypoint = new Posdeliverypoint();
                                            posdeliverypoint.ExternalId = f00Node.Value;

                                            string name = f01Node.Value.HtmlEncode();
                                            string number = f00Node.Value;

                                            if (!name.Contains(number))
                                                name = name + " " + number;

                                            posdeliverypoint.Name = name;
                                            posdeliverypoint.Number = number;

                                            if (!posdeliverypoint.Number.IsNullOrWhiteSpace())
                                                posdeliverypoints.Add(posdeliverypoint);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints,
                        "An exception was thrown while trying to get the static files. Exception: {0}", ex.Message);
                }
            }

            this.WriteToLogExtended("GetPosdeliverypoints", "End");
            return posdeliverypoints.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>An Obymobi.Logic.Model.Posorder instance</returns>
        public Model.Posorder GetPosorder(string deliverypointNumber)
        {
            Posorder posorder = null;
            OrderProcessingError proccesingError = OrderProcessingError.None;
            string errorMessage = string.Empty;

            this.WriteToLogExtended("GetPosOrder", "Start");

            int deliverypointId = -1;
            if (int.TryParse(deliverypointNumber, out deliverypointId))
            {
                if (this.freeslots.Count == 0)
                {
                    try
                    {
                        // Try to get the profiles
                        AlohaResponse getProfilesResponse = this.GetProfiles();

                        if (getProfilesResponse.ErrorCode == 0)
                        {
                            XmlDoc xmlDoc = new XmlDoc();
                            xmlDoc.ParseXml(getProfilesResponse.Data);

                            // ReturnData node
                            XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                            if (returnDataNode != null)
                            {
                                // FreeSlots node
                                XmlNode freeSlotsNode = returnDataNode.GetSingleChildNodeByName("FreeSlots");
                                if (freeSlotsNode != null)
                                {
                                    // Slots nodes
                                    List<XmlNode> slotNodes = freeSlotsNode.GetChildNodesByName("Slot");
                                    if (slotNodes != null && slotNodes.Count > 0)
                                    {
                                        foreach (XmlNode slotNode in slotNodes)
                                        {
                                            // SlotID
                                            XmlNode slotIDNode = slotNode.GetSingleChildNodeByName("SlotID");
                                            if (slotIDNode != null)
                                            {
                                                int slotID = 0;
                                                if (int.TryParse(slotIDNode.Value, out slotID))   // SlotID
                                                    this.freeslots.Add(slotID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            proccesingError = OrderProcessingError.PosErrorCouldNotGetPosOrders;
                            errorMessage = string.Format("An invalid response was returned while trying to get the profiles. Error: {0}", getProfilesResponse.ErrorMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        proccesingError = OrderProcessingError.PosErrorCouldNotGetPosOrders;
                        errorMessage = string.Format("An exception was thrown while trying to get the profiles. Exception: {0}", ex.Message);
                    }
                }

                if (this.freeslots.Count == 0)
                {
                    proccesingError = OrderProcessingError.PosErrorCouldNotGetPosOrders;
                    errorMessage = "There are no free slots!";
                }
                else
                {
                    if (proccesingError != OrderProcessingError.PosErrorCouldNotGetPosOrders)
                    {
                        // Try to set the profile
                        try
                        {
                            AlohaResponse setProfileResponse = this.SetProfile(this.freeslots[0]);
                            if (setProfileResponse.ErrorCode != 0)
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotGetProfiles;
                                errorMessage = string.Format("An invalid response was returned while trying to set the profiles. Error: {0}", setProfileResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotSetProfile;
                            errorMessage = string.Format("An exception was thrown while trying to set the profile with SlotID '{0}'. Exception: {1}", this.freeslots[0], ex.Message);
                        }

                    }

                    // GK Created but never used
                    //bool clockedIn = false;

                    if (proccesingError == OrderProcessingError.None)
                    {
                        // Try to login the employee
                        try
                        {
                            AlohaResponse logInResponse = this.LogInEmployee(this.employeeNumber, this.employeePassword);
                            if (logInResponse.ErrorCode != 0)
                            {
                                if (logInResponse.ErrorCode == 6) // The employee is already logged in at another terminal
                                {
                                    proccesingError = OrderProcessingError.AlohaCouldNotLoginEmployee;
                                    errorMessage = string.Format("An invalid response was returned while trying to log in the employee. Error: {0}", logInResponse.ErrorMessage);
                                }
                                else if (logInResponse.ErrorCode == 1000016) // We need to clock in
                                {
                                    if (proccesingError == OrderProcessingError.None)
                                    {
                                        // Try to clockin the employee
                                        try
                                        {
                                            AlohaResponse clockInResponse = this.ClockInEmployee(this.employeeNumber, string.Empty, this.jobCodeID);
                                            if (clockInResponse.ErrorCode != 0)
                                            {
                                                proccesingError = OrderProcessingError.AlohaCouldNotClockInEmployee;
                                                errorMessage = string.Format("An invalid response was returned while trying to clock in the employee. Error: {0}", clockInResponse.ErrorMessage);
                                            }
                                            // GK Created but never used
                                            //else
                                            //    clockedIn = true;
                                        }
                                        catch (Exception ex)
                                        {
                                            proccesingError = OrderProcessingError.AlohaCouldNotClockInEmployee;
                                            errorMessage = string.Format("An exception was thrown while trying to clockin the employee with number '{0}', password '{1}' and jobcode id '{2}'. Exception: {3}", this.employeeNumber, this.employeePassword, this.jobCodeID, ex.Message);
                                        }
                                    }
                                }
                                else
                                {
                                    proccesingError = OrderProcessingError.AlohaCouldNotLoginEmployee;
                                    errorMessage = string.Format("An invalid response was returned while trying to login the employee. Error: {0}", logInResponse.ErrorMessage);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotLoginEmployee;
                            errorMessage = string.Format("An exception was thrown while trying to login the employee with number '{0}' and password '{1}'. Exception: {2}", this.employeeNumber, this.employeePassword, ex.Message);
                        }
                    }

                    int tableId = 0;
                    if (proccesingError == OrderProcessingError.None)
                    {
                        // Try to get the open tables
                        try
                        {
                            AlohaResponse getOpenTablesResponse = this.GetOpenTables();

                            if (getOpenTablesResponse.ErrorCode == 0)
                            {
                                XmlDoc xmlDoc = new XmlDoc();
                                xmlDoc.ParseXml(getOpenTablesResponse.Data);

                                // ReturnData node
                                XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                                if (returnDataNode != null)
                                {
                                    // Tables node
                                    XmlNode tablesNode = returnDataNode.GetSingleChildNodeByName("Tables");
                                    if (tablesNode != null)
                                    {
                                        // Table nodes
                                        List<XmlNode> tableNodes = tablesNode.GetChildNodesByName("Table");
                                        foreach (XmlNode tableNode in tableNodes)
                                        {
                                            // ID node
                                            XmlNode slotNode = tableNode.GetSingleChildNodeByName("ID");
                                            if (slotNode != null)
                                                int.TryParse(slotNode.Value, out tableId);   // TableID
                                        }
                                    }
                                }
                            }
                            else
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotGetOpenTables;
                                errorMessage = string.Format("An invalid response was returned while trying to get the open tables. Error: {0}", getOpenTablesResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotGetOpenTables;
                            errorMessage = string.Format("An exception was thrown while trying to get the open tables'. Exception: {0}", ex.Message);
                        }
                    }

                    if (proccesingError == OrderProcessingError.None)
                    {
                        // Try the begin/open the table
                        try
                        {
                            AlohaResponse beginTableResponse = this.BeginTable(tableId, deliverypointId, 2);
                            if (beginTableResponse.ErrorCode == 50)
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotOpenTable;
                                errorMessage = string.Format("An invalid response was returned while trying to log in the employee. Error: {0}", beginTableResponse.ErrorMessage);
                            }
                            else if (beginTableResponse.ErrorCode != 0)
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotOpenTable;
                                errorMessage = string.Format("An invalid response was returned while trying to open the table with id '{0}' and number '{1}'. Error: {2}", tableId, deliverypointId, beginTableResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotOpenTable;
                            errorMessage = string.Format("An exception was thrown while trying to open the table with id '{0}' and number '{1}'. Exception: {2}", tableId, deliverypointId, ex.Message);
                        }
                    }

                    if (proccesingError == OrderProcessingError.None)
                    {
                        // Try the begin/open the table
                        try
                        {
                            AlohaResponse beginCheckResponse = this.BeginCheck(tableId);
                            if (beginCheckResponse.ErrorCode != 0)
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotOpenCheck;
                                errorMessage = string.Format("An invalid response was returned while trying to open the check with id '{0}' Error: {1}", tableId, deliverypointId, beginCheckResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotOpenCheck;
                            errorMessage = string.Format("An exception was thrown was thrown while trying to open the check with id '{0}' Error: {1}", tableId, deliverypointId, ex.Message);
                        }
                    }

                    if (proccesingError == OrderProcessingError.None)
                    {
                        // Try to add the items to the check
                        try
                        {
                            AlohaResponse getCheckDetailsResponse = this.GetCheckDetails();
                            if (getCheckDetailsResponse.ErrorCode != 0)
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotGetCheckDetails;
                                errorMessage = string.Format("An invalid response was returned while trying get the details of the check. Error: {0}", getCheckDetailsResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotGetCheckDetails;
                            errorMessage = string.Format("An exception was thrown while trying to get the details of the check. Exception: {0}", ex.Message);
                        }
                    }

                    if (proccesingError == OrderProcessingError.None)
                    {
                        // Try the end the check
                        try
                        {
                            AlohaResponse endCheckResponse = this.EndCheck(new List<int>());
                            if (endCheckResponse.ErrorCode != 0)
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotEndCheck;
                                errorMessage = string.Format("An invalid response was returned while trying to end the check. Error: {0}", endCheckResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotEndCheck;
                            errorMessage = string.Format("An exception was thrown while trying to end the check with id '{0}'. Exception: {1}", deliverypointId, ex.Message);
                        }
                    }

                    if (proccesingError == OrderProcessingError.None)
                    {
                        // Try the end the table
                        try
                        {
                            AlohaResponse endTableResponse = this.EndTable();
                            if (endTableResponse.ErrorCode != 0)
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotEndTable;
                                errorMessage = string.Format("An invalid response was returned while trying to end the table'. Error: {0}", endTableResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotEndTable;
                            errorMessage = string.Format("An exception was thrown while trying to end the table'. Exception: {0}", ex.Message);
                        }
                    }


                    if (proccesingError == OrderProcessingError.None)
                    {
                        // Try to logout the employee
                        try
                        {
                            AlohaResponse logoutResponse = this.LogOutEmployee();
                            if (logoutResponse.ErrorCode != 0)
                            {
                                proccesingError = OrderProcessingError.AlohaCouldNotLogOutEmployee;
                                errorMessage = string.Format("An invalid response was returned while trying to logout the employee'. Error: {0}", logoutResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            proccesingError = OrderProcessingError.AlohaCouldNotLogOutEmployee;
                            errorMessage = string.Format("An exception was thrown while trying to logout the employee'. Exception: {0}", ex.Message);
                        }
                    }
                }
            }

            if (proccesingError != OrderProcessingError.None)
                POSException.ThrowTyped(proccesingError, null, errorMessage, null);

            this.WriteToLogExtended("GetPosOrder", "Ended with status - " + proccesingError.ToString());

            return posorder;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public Enums.OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            OrderProcessingError processingError = OrderProcessingError.None;
            string errorMessage = string.Empty;

            this.WriteToLogExtended("SaveOrder", "Start");

            int deliverypointId = -1;
            if (int.TryParse(posorder.PosdeliverypointExternalId, out deliverypointId))
            {
                // Check whether we already have a free slot
                if (this.freeslots.Count == 0)
                {
                    try
                    {
                        // Try to get the profiles
                        AlohaResponse getProfilesResponse = this.GetProfiles();

                        if (getProfilesResponse.ErrorCode == 0)
                        {
                            XmlDoc xmlDoc = new XmlDoc();
                            xmlDoc.ParseXml(getProfilesResponse.Data);

                            //this.WriteToLogExtended("SaveOrder", "GetProfiles.Data:\r\n\r\n{0}\r\n\r\n", getProfilesResponse.Data);

                            // ReturnData node
                            XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                            if (returnDataNode != null)
                            {
                                // FreeSlots node
                                XmlNode freeSlotsNode = returnDataNode.GetSingleChildNodeByName("FreeSlots");
                                if (freeSlotsNode != null)
                                {
                                    // Slots nodes
                                    List<XmlNode> slotNodes = freeSlotsNode.GetChildNodesByName("Slot");
                                    if (slotNodes != null && slotNodes.Count > 0)
                                    {
                                        foreach (XmlNode slotNode in slotNodes)
                                        {
                                            // SlotID
                                            XmlNode slotIDNode = slotNode.GetSingleChildNodeByName("SlotID");
                                            if (slotIDNode != null)
                                            {
                                                int slotID = 0;
                                                if (int.TryParse(slotIDNode.Value, out slotID))   // SlotID
                                                    this.freeslots.Add(slotID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            processingError = OrderProcessingError.AlohaCouldNotGetProfiles;
                            errorMessage = string.Format("An invalid response was returned while trying to get the profiles. Error: {0}", getProfilesResponse.ErrorMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        processingError = OrderProcessingError.AlohaCouldNotGetProfiles;
                        errorMessage = string.Format("An exception was thrown while trying to get the profiles. Exception: {0}", ex.Message);
                    }

                    if (processingError == OrderProcessingError.None)
                    {
                        // Try to set the profile
                        try
                        {
                            AlohaResponse setProfileResponse = this.SetProfile(this.freeslots[0]);
                            if (setProfileResponse.ErrorCode != 0)
                            {
                                if (setProfileResponse.ErrorCode == 1000018)
                                {
                                    this.WriteToLogExtended("ExecuteRequest", "Invalid/expired session - Start a new session");

                                    // Start a new session
                                    this.StartSession();

                                    // And save the order again
                                    return this.SaveOrder(posorder);
                                }
                                else
                                {
                                    processingError = OrderProcessingError.AlohaCouldNotSetProfile;
                                    errorMessage = string.Format("An invalid response was returned while trying to set the profiles. Error: {0}", setProfileResponse.ErrorMessage);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotSetProfile;
                            errorMessage = string.Format("An exception was thrown while trying to set the profile with SlotID '{0}'. Exception: {1}", this.freeslots[0], ex.Message);
                        }
                    }
                }

                if (this.freeslots.Count == 0 && processingError == OrderProcessingError.None)
                {
                    processingError = OrderProcessingError.PosErrorDeliverypointLocked;
                    errorMessage = "There are no free slots!";
                }
                else
                {
                    // GK Created but never used
                    // bool clockedIn = false;

                    if (processingError == OrderProcessingError.None)
                    {
                        // Try to login the employee
                        try
                        {
                            AlohaResponse logInResponse = this.LogInEmployee(this.employeeNumber, this.employeePassword);
                            if (logInResponse.ErrorCode != 0)
                            {
                                if (logInResponse.ErrorCode == 6) // The employee is already logged in at another terminal
                                {
                                    processingError = OrderProcessingError.PosErrorDeliverypointLocked;
                                    errorMessage = string.Format("An invalid response was returned while trying to log in the employee. Error: {0}", logInResponse.ErrorMessage);
                                }
                                else if (logInResponse.ErrorCode == 1000018) // Session expired
                                {
                                    this.WriteToLogExtended("ExecuteRequest", "Invalid/expired session - Start a new session");

                                    // Start a new session
                                    this.StartSession();

                                    // And save the order again
                                    return this.SaveOrder(posorder);
                                }
                                else if (logInResponse.ErrorCode == 1000016) // We need to clock in
                                {
                                    if (processingError == OrderProcessingError.None)
                                    {
                                        // Try to clockin the employee
                                        try
                                        {
                                            AlohaResponse clockInResponse = this.ClockInEmployee(this.employeeNumber, string.Empty, this.jobCodeID);
                                            if (clockInResponse.ErrorCode != 0)
                                            {
                                                processingError = OrderProcessingError.AlohaCouldNotClockInEmployee;
                                                errorMessage = string.Format("An invalid response was returned while trying to clock in the employee. Error: {0}", clockInResponse.ErrorMessage);
                                            }
                                            // else
                                            //    clockedIn = true;
                                        }
                                        catch (Exception ex)
                                        {
                                            processingError = OrderProcessingError.AlohaCouldNotClockInEmployee;
                                            errorMessage = string.Format("An exception was thrown while trying to clockin the employee with number '{0}', password '{1}' and jobcode id '{2}'. Exception: {3}", this.employeeNumber, this.employeePassword, this.jobCodeID, ex.Message);
                                        }
                                    }
                                }
                                else
                                {
                                    processingError = OrderProcessingError.AlohaCouldNotLoginEmployee;
                                    errorMessage = string.Format("An invalid response was returned while trying to login the employee. Error: {0}", logInResponse.ErrorMessage);
                                }
                            }
                            else
                            {
                                this.isEmployeeLogin = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotLoginEmployee;
                            errorMessage = string.Format("An exception was thrown while trying to login the employee with number '{0}' and password '{1}'. Exception: {2}", this.employeeNumber, this.employeePassword, ex.Message);
                        }
                    }

                    int tableId = 0;
                    if (processingError == OrderProcessingError.None)
                    {
                        // Try to get the open tables
                        try
                        {
                            AlohaResponse getOpenTablesResponse = this.GetOpenTables();

                            if (getOpenTablesResponse.ErrorCode == 0)
                            {
                                XmlDoc xmlDoc = new XmlDoc();
                                xmlDoc.ParseXml(getOpenTablesResponse.Data);

                                // ReturnData node
                                XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                                if (returnDataNode != null)
                                {
                                    // Tables node
                                    XmlNode tablesNode = returnDataNode.GetSingleChildNodeByName("Tables");
                                    if (tablesNode != null)
                                    {
                                        int defId = -1;
                                        int srcId = -1;

                                        // Table nodes
                                        List<XmlNode> tableNodes = tablesNode.GetChildNodesByName("Table");
                                        foreach (XmlNode tableNode in tableNodes)
                                        {
                                            // ID node
                                            XmlNode idNode = tableNode.GetSingleChildNodeByName("ID");
                                            XmlNode defNode = tableNode.GetSingleChildNodeByName("TableDefId");

                                            if (idNode == null)
                                            {
                                                this.WriteToLogNormal("SaveOrder", "idNode == null");
                                            }
                                            else if (defNode == null)
                                            {
                                                this.WriteToLogNormal("SaveOrder", "defNode == null");
                                            }
                                            else if (!int.TryParse(defNode.Value, out defId))
                                            {
                                                this.WriteToLogNormal("SaveOrder", "defNode.value '{0}' could not be parsed to an int!", defNode.Value);
                                            }
                                            else if (defId != deliverypointId)
                                            {
                                                this.WriteToLogNormal("SaveOrder", "defId '{0}' != deliverypointId '{1}'!", defId, deliverypointId);
                                            }
                                            else if (!int.TryParse(idNode.Value, out srcId))
                                            {
                                                this.WriteToLogNormal("SaveOrder", "idNode.value '{0}' could not be parsed to an int!", idNode.Value);
                                            }
                                            else
                                            {
                                                tableId = srcId;
                                                this.WriteToLogNormal("SaveOrder", "tableId = '{0}'", tableId);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                processingError = OrderProcessingError.AlohaCouldNotGetOpenTables;
                                errorMessage = string.Format("An invalid response was returned while trying to get the open tables. Error: {0}", getOpenTablesResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotGetOpenTables;
                            errorMessage = string.Format("An exception was thrown while trying to get the open tables'. Exception: {0}", ex.Message);
                        }
                    }

                    if (processingError == OrderProcessingError.None)
                    {
                        // Try the begin/open the table
                        try
                        {
                            AlohaResponse beginTableResponse = this.BeginTable(tableId, deliverypointId, 2);
                            if (beginTableResponse.ErrorCode == 50)
                            {
                                processingError = OrderProcessingError.PosErrorDeliverypointLocked;
                                errorMessage = string.Format("An 'Table In Use' response was returned while trying to open the table with id '{0}' and number '{1}'. Error: {2}", tableId, deliverypointId, beginTableResponse.ErrorMessage);
                            }
                            else if (beginTableResponse.ErrorCode != 0)
                            {
                                processingError = OrderProcessingError.AlohaCouldNotOpenTable;
                                errorMessage = string.Format("An invalid response was returned while trying to open the table with id '{0}' and number '{1}'. Error: {2}", tableId, deliverypointId, beginTableResponse.ErrorMessage);
                            }
                            else
                            {
                                this.isTableOpen = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotOpenTable;
                            errorMessage = string.Format("An exception was thrown while trying to open the table with id '{0}' and number '{1}'. Exception: {2}", tableId, deliverypointId, ex.Message);
                        }
                    }

                    int checkId = 0;
                    if (processingError == OrderProcessingError.None)
                    {
                        // Try to get the table details
                        try
                        {
                            AlohaResponse getTableDetailsResponse = this.GetTableDetails();
                            if (getTableDetailsResponse.ErrorCode == 0)
                            {
                                XmlDoc xmlDoc = new XmlDoc();
                                xmlDoc.ParseXml(getTableDetailsResponse.Data);

                                // ReturnData node
                                XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                                if (returnDataNode != null)
                                {
                                    int temp = 0;

                                    // Table nodes
                                    List<XmlNode> tableNodes = returnDataNode.GetChildNodesByName("Table");
                                    foreach (XmlNode tableNode in tableNodes)
                                    {
                                        // ID node
                                        XmlNode idNode = tableNode.GetSingleChildNodeByName("ID");
                                        if (idNode != null)
                                        {
                                            if (int.TryParse(idNode.Value, out temp) && temp == tableId)   // TableID
                                            {
                                                XmlNode checksNode = tableNode.GetSingleChildNodeByName("Checks");
                                                if (checksNode != null)
                                                {
                                                    // Check nodes
                                                    List<XmlNode> checkNodes = checksNode.GetChildNodesByName("Check");
                                                    foreach (XmlNode node in checkNodes)
                                                    {
                                                        // ID node
                                                        XmlNode slotNode = node.GetSingleChildNodeByName("ID");
                                                        if (slotNode != null)
                                                        {
                                                            int.TryParse(slotNode.Value, out checkId);   // CheckID
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                processingError = OrderProcessingError.AlohaCouldNotGetTableDetails;
                                errorMessage = string.Format("An invalid response was returned while trying to get the table details. Error: {1}", tableId, deliverypointId, getTableDetailsResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotGetTableDetails;
                            errorMessage = string.Format("An exception was thrown was thrown while trying to get the table details. Error: {1}", tableId, deliverypointId, ex.Message);
                        }
                    }

                    if (processingError == OrderProcessingError.None)
                    {
                        // Try the begin/open the table
                        try
                        {
                            AlohaResponse beginCheckResponse = this.BeginCheck(checkId);
                            if (beginCheckResponse.ErrorCode != 0)
                            {
                                processingError = OrderProcessingError.AlohaCouldNotOpenCheck;
                                errorMessage = string.Format("An invalid response was returned while trying to open the check with id '{0}' Error: {1}", tableId, deliverypointId, beginCheckResponse.ErrorMessage);
                            }
                            else
                            {
                                this.isCheckOpen = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotOpenCheck;
                            errorMessage = string.Format("An exception was thrown was thrown while trying to open the check with id '{0}' Error: {1}", tableId, deliverypointId, ex.Message);
                        }
                    }

                    if (processingError == OrderProcessingError.None)
                    {
                        // Try to add the items to the check
                        try
                        {
                            AlohaResponse addItemsResponse = this.AddCheckItems(posorder);
                            if (addItemsResponse.ErrorCode == 0) // No error, but check if there's an error in the items
                            {
                                XmlDoc xmlDoc = new XmlDoc();
                                xmlDoc.ParseXml(addItemsResponse.Data);

                                // ReturnData node
                                XmlNode returnDataNode = xmlDoc.GetSingleChildNodeByName("ReturnData");
                                if (returnDataNode != null)
                                {
                                    // Items node
                                    XmlNode itemsNode = returnDataNode.GetSingleChildNodeByName("Items");
                                    if (itemsNode != null)
                                    {
                                        // Item nodes
                                        List<XmlNode> itemNodes = itemsNode.GetChildNodesByName("Item");
                                        foreach (XmlNode itemNode in itemNodes)
                                        {
                                            // ErrorCode node
                                            XmlNode errorCodeNode = itemNode.GetSingleChildNodeByName("ErrorCode");
                                            if (errorCodeNode != null)
                                            {
                                                int temp = -1;
                                                if (int.TryParse(errorCodeNode.Value, out temp) && temp > 0)
                                                {
                                                    string errorMsg = string.Empty;
                                                    XmlNode errorMsgNode = itemNode.GetSingleChildNodeByName("ErrorMsg");
                                                    if (errorMsgNode != null)
                                                        errorMsg = string.Format("ErrorMsg: {0}", errorMsgNode.Value);

                                                    string itemId = string.Empty;
                                                    XmlNode itemIdNode = itemNode.GetSingleChildNodeByName("ItemId");
                                                    if (itemIdNode != null)
                                                        itemId = string.Format("ItemId: {0}", itemIdNode.Value);

                                                    processingError = OrderProcessingError.AlohaCouldNotAddItemsToCheck;
                                                    errorMessage = string.Format("An invalid response was returned while trying to add the items to the check. ErrorCode: {0} {1} {2}", errorCodeNode.Value, errorMsg, itemId);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                processingError = OrderProcessingError.AlohaCouldNotAddItemsToCheck;
                                errorMessage = string.Format("An invalid response was returned while trying to add the items to the check. Error: {0}", addItemsResponse.ErrorMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotAddItemsToCheck;
                            errorMessage = string.Format("An exception was thrown while trying to add the items to the check. Exception: {0}", ex.Message);
                        }
                    }

                    if (processingError == OrderProcessingError.None)
                    {
                        // Try the end the check
                        try
                        {
                            AlohaResponse endCheckResponse = this.EndCheck(new List<int>());
                            if (endCheckResponse.ErrorCode != 0)
                            {
                                processingError = OrderProcessingError.AlohaCouldNotEndCheck;
                                errorMessage = string.Format("An invalid response was returned while trying to end the check. Error: {0}", endCheckResponse.ErrorMessage);
                            }
                            else
                            {
                                this.isCheckOpen = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotEndCheck;
                            errorMessage = string.Format("An exception was thrown while trying to end the check with id '{0}'. Exception: {1}", deliverypointId, ex.Message);
                        }
                    }

                    if (processingError == OrderProcessingError.None)
                    {
                        // Try the end the table
                        try
                        {
                            AlohaResponse endTableResponse = this.EndTable();
                            if (endTableResponse.ErrorCode != 0)
                            {
                                processingError = OrderProcessingError.AlohaCouldNotEndTable;
                                errorMessage = string.Format("An invalid response was returned while trying to end the table'. Error: {0}", endTableResponse.ErrorMessage);
                            }
                            else
                            {
                                this.isTableOpen = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotEndTable;
                            errorMessage = string.Format("An exception was thrown while trying to end the table'. Exception: {0}", ex.Message);
                        }
                    }


                    if (processingError == OrderProcessingError.None)
                    {
                        // Try to logout the employee
                        try
                        {
                            AlohaResponse logoutResponse = this.LogOutEmployee();
                            if (logoutResponse.ErrorCode != 0)
                            {
                                processingError = OrderProcessingError.AlohaCouldNotLogOutEmployee;
                                errorMessage = string.Format("An invalid response was returned while trying to logout the employee'. Error: {0}", logoutResponse.ErrorMessage);
                            }
                            else
                            {
                                this.isEmployeeLogin = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.AlohaCouldNotLogOutEmployee;
                            errorMessage = string.Format("An exception was thrown while trying to logout the employee'. Exception: {0}", ex.Message);
                        }
                    }
                }
            }

            if (this.isCheckOpen)
                this.EndCheck(new List<int>());
            if (this.isTableOpen)
                this.EndTable();
            if (this.isEmployeeLogin)
                this.LogOutEmployee();

            if (processingError != OrderProcessingError.None)
                POSException.ThrowTyped(processingError, null, this.ErrorMessage, null);

            this.WriteToLogExtended("SaveOrder", "End");

            return processingError;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            this.WriteToLogExtended("PrintConfirmationReceipt", "Start");
            this.WriteToLogExtended("PrintConfirmationReceipt", "End");
            return true;
        }

        ///// <summary>
        ///// Prints an Obymobi status report
        ///// </summary>
        ///// <param name="terminalStatus"></param>
        ///// <returns>
        ///// True if printing the status report was succesful, False if not
        ///// </returns>
        //public bool PrintStatusReport(Status.TerminalStatus terminalStatus)
        //{
        //    this.WriteToLogExtended("PrintStatusReport", "Start");
        //    this.WriteToLogExtended("PrintStatusReport", "End");
        //    return true;
        //}

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            this.WriteToLogExtended("PrintServiceRequestReceipt", "Start");
            this.WriteToLogExtended("PrintServiceRequestReceipt", "End");
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            this.WriteToLogExtended("PrintCheckoutRequestReceipt", "Start");
            this.WriteToLogExtended("PrintCheckoutRequestReceipt", "End");
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            this.WriteToLogExtended("NotifyLockedDeliverypoint", "Start");
            this.WriteToLogExtended("NotifyLockedDeliverypoint", "End");
            return true;
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Executes the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AlohaResponse ExecuteRequest(AlohaRequest request)
        {
            this.WriteToLogExtended(string.Format("ExecuteRequest '{0}'", request.Action), "Start");

            AlohaResponse response = new AlohaResponse();

            // Check whether we have a session token
            if (this.sessionToken.IsNullOrWhiteSpace() && request.Action != AlohaAction.StartSession)
            {
                this.WriteToLogExtended("ExecuteRequest", "No session token available - Start a new session");
                this.StartSession();
            }

            // Create and initialize the request
            AlohaWebservice.RequestMessage requestMessage = new AlohaWebservice.RequestMessage();
            requestMessage.RequestID = this.requestID;
            requestMessage.Identifier = this.identifier;
            requestMessage.Request = request.ToString();
            requestMessage.SessionToken = this.sessionToken;

            this.WriteToLogExtended("ExecuteRequest", "RequestMessage.SessionToken: {0}", requestMessage.SessionToken);
            this.WriteToLogExtended("ExecuteRequest", "RequestMessage.Request\r\n\r\n{0}", requestMessage.Request);


            try
            {
                //this.WriteToLogExtended("ExecuteRequest", "Request:\r\n{0}", request.ToString());
                AlohaWebservice.ReturnMessage returnMessage = this.Webservice.PostRequest(requestMessage);
                response.Data = returnMessage.Return;
                response.ErrorCode = returnMessage.ErrorCode;
                response.ErrorMessage = returnMessage.ErrorMessage;

                this.WriteToLogExtended("ExecuteRequest", "ReturnMessage.Data: {0}", response.Data);
                this.WriteToLogExtended("ExecuteRequest", "ReturnMessage.ErrorCode: {0}", returnMessage.ErrorCode);
                this.WriteToLogExtended("ExecuteRequest", "ReturnMessage.ErrorMessage: {0}", returnMessage.ErrorMessage);
                //this.WriteToLogExtended("ExecuteRequest", "ReturnMessage.ProcessorStamp: {0}", returnMessage.ProcessorStamp);
                //this.WriteToLogExtended("ExecuteRequest", "ReturnMessage.RequestID: {0}", returnMessage.RequestID);
                //this.WriteToLogExtended("ExecuteRequest", "ReturnMessage.Requests: {0}", returnMessage.Requests);
                //this.WriteToLogExtended("ExecuteRequest", "ReturnMessage.Return: {0}", returnMessage.Return);
                this.WriteToLogExtended("ExecuteRequest", "ReturnMessage.SessionToken: {0}", returnMessage.SessionToken);

                this.sessionToken = returnMessage.SessionToken;
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, ex,
                    "An exception was thrown while trying the execute request '{0}'. Exception: {1}", request.ToString(), ex.Message);
            }

            this.WriteToLogExtended(string.Format("ExecuteRequest '{0}'", request.Action), "End");

            return response;
        }

        #endregion

        #region TIM Webservice methods

        /// <summary>
        /// Gets the request queue snapshot.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse GetRequestQueueSnapshot()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.GetRequestQueueSnapShot);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Gets the device settings.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse GetDeviceSettings()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.GetDeviceSettings);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Sets the device settings.
        /// </summary>
        /// <param name="identifier">The identifier.</param>
        /// <param name="deviceSettings">The device settings.</param>
        /// <returns></returns>
        public AlohaResponse SetDeviceSettings(string identifier, Dictionary<string, object> deviceSettings)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.SetDeviceSettings);
            request.ActionData.Add("Identifier", identifier);
            request.ActionData.Add("DeviceSettings", deviceSettings);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Sets the profile.
        /// </summary>
        /// <param name="slotID">The slot ID.</param>
        /// <returns></returns>
        public AlohaResponse SetProfile(int slotID)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.SetProfile);
            request.ActionData.Add("SlotID", slotID);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Starts the session.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse StartSession()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.StartSession);
            request.ActionData.Add("DeviceName", "Otoucho");

            // Reset the free slots
            this.freeslots = new List<int>();

            // Reset the session token
            this.sessionToken = string.Empty;

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Gets the profiles.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse GetProfiles()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.GetProfiles);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Logs the in employee.
        /// </summary>
        /// <param name="employeeNumber">The employee number.</param>
        /// <param name="employeePassword">The employee password.</param>
        /// <returns></returns>
        public AlohaResponse LogInEmployee(int employeeNumber, string employeePassword)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.LogInEmployee);
            request.ActionData.Add("EmployeeNumber", employeeNumber);
            request.ActionData.Add("EmployeePsw", employeePassword);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Clocks the in employee.
        /// </summary>
        /// <param name="employeeNumber">The employee number.</param>
        /// <param name="employeePassword">The employee password.</param>
        /// <param name="jobCodeId">The job code id.</param>
        /// <returns></returns>
        public AlohaResponse ClockInEmployee(int employeeNumber, string employeePassword, int jobCodeId)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.ClockInEmployee);
            request.ActionData.Add("EmployeeNumber", employeeNumber);
            request.ActionData.Add("EmployeePsw", employeePassword);
            request.ActionData.Add("JobCodeId", jobCodeId);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Logs the out employee.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse LogOutEmployee()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.LogOutEmployee);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Clocks the out employee.
        /// </summary>
        /// <param name="declaredTips">The declared tips.</param>
        /// <returns></returns>
        public AlohaResponse ClockOutEmployee(decimal declaredTips)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.ClockOutEmployee);
            request.ActionData.Add("DeclaredTips", declaredTips);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Gets the open tables.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse GetOpenTables()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.GetOpenTables);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Gets the open tabs.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse GetOpenTabs()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.GetOpenTabs);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Gets the static files.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse GetStaticFiles()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.GetStaticFiles);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Is the get aloha terminals.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse iGetAlohaTerminals()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.iGetAlohaTerminals);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Begins the table.
        /// </summary>
        /// <param name="tableID">The table ID.</param>
        /// <param name="tableNumber">The table number.</param>
        /// <param name="numGuests">The num guests.</param>
        /// <returns></returns>
        public AlohaResponse BeginTable(int tableID, int tableNumber, int numGuests)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.BeginTable);
            //request.ProcessID = processID;
            //request.ProcessDate = processDate;

            if (tableID == 0)
                request.ActionData.Add("TableNumber", tableNumber);
            else
                request.ActionData.Add("TableID", tableID);

            //request.ActionData.Add("NumGuests", numGuests);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Ends the table.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse EndTable()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.EndTable);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Begins the tab.
        /// </summary>
        /// <param name="processID">The process ID.</param>
        /// <param name="processDate">The process date.</param>
        /// <param name="tableID">The table ID.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="numGuests">The num guests.</param>
        /// <returns></returns>
        public AlohaResponse BeginTab(int processID, DateTime processDate, int tableID, string tableName, int numGuests)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.BeginTab);
            request.ProcessID = processID;
            request.ProcessDate = processDate;
            request.ActionData.Add("TableID", tableID);
            request.ActionData.Add("TableName", tableName);
            request.ActionData.Add("NumGuests", numGuests);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Ends the tab.
        /// </summary>
        /// <param name="processID">The process ID.</param>
        /// <param name="processDate">The process date.</param>
        /// <returns></returns>
        public AlohaResponse EndTab(int processID, DateTime processDate)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.EndTab);
            request.ProcessID = processID;
            request.ProcessDate = processDate;

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Begins the check.
        /// </summary>
        /// <param name="checkID">The check ID.</param>
        /// <returns></returns>
        public AlohaResponse BeginCheck(int checkID)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.BeginCheck);
            request.ActionData.Add("CheckID", checkID);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Ends the check.
        /// </summary>
        /// <param name="holdItems">The hold items.</param>
        /// <returns></returns>
        public AlohaResponse EndCheck(List<int> holdItems)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.EndCheck);

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("EntryId", holdItems);

            request.ActionData.Add("HoldItems", dictionary);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Gets the table details.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse GetTableDetails()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.GetTableDetails);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Gets the check details.
        /// </summary>
        /// <returns></returns>
        public AlohaResponse GetCheckDetails()
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.GetCheckDetails);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Adds the check items.
        /// </summary>
        /// <param name="posorder">The posorder.</param>
        /// <returns></returns>
        public AlohaResponse AddCheckItems(Posorder posorder)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.AddCheckItems);
            //request.ProcessID = processID;
            //request.ProcessDate = processDate;

            StringBuilder items = new StringBuilder();
            if (posorder != null && posorder.Posorderitems != null && posorder.Posorderitems.Length > 0)
            {
                foreach (Posorderitem posorderitem in posorder.Posorderitems)
                {
                    items.AppendLine("<Item>".AddTabs(1));

                    items.AppendLine(string.Format("<ItemID>{0}</ItemID>", posorderitem.PosproductExternalId).AddTabs(2));
                    items.AppendLine(string.Format("<Qty>{0}</Qty>", posorderitem.Quantity).AddTabs(2));
                    //items.AppendLine(string.Format("<Name>{0}</Name>", posorderitem.Description).AddTabs(2));
                    //items.AppendLine(string.Format("<Price>{0}</Price>", posorderitem.Price.ToString(culture)).AddTabs(2));

                    if (posorderitem.Posalterationitems != null && posorderitem.Posalterationitems.Length > 0)
                    {
                        items.AppendLine("<Modifiers>".AddTabs(2));

                        foreach (Posalterationitem posalterationitem in posorderitem.Posalterationitems)
                        {
                            items.AppendLine("<Modifier>".AddTabs(3));

                            items.AppendLine(string.Format("<ItemID>{0}</ItemID>", posalterationitem.ExternalPosalterationoptionId).AddTabs(4));
                            items.AppendLine("<Name></Name>".AddTabs(4));
                            items.AppendLine("<Price></Price>".AddTabs(4));
                            items.AppendLine("<ModCod></ModCod>".AddTabs(4));

                            items.AppendLine("</Modifier>".AddTabs(3));
                        }

                        items.AppendLine("</Modifiers>".AddTabs(2));
                    }

                    items.AppendLine("</Item>".AddTabs(1));
                }

                if (this.markerID > 0)
                {
                    items.AppendLine("<Item>".AddTabs(1));

                    items.AppendLine(string.Format("<ItemID>{0}</ItemID>", this.markerID).AddTabs(2));
                    items.AppendLine("<Qty>1</Qty>".AddTabs(2));

                    items.AppendLine("</Item>".AddTabs(1));
                }
            }

            request.ActionData.Add("Items", items.ToString());

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Voids the check items.
        /// </summary>
        /// <param name="processID">The process ID.</param>
        /// <param name="processDate">The process date.</param>
        /// <param name="voidReason">The void reason.</param>
        /// <param name="voidItems">The void items.</param>
        /// <returns></returns>
        public AlohaResponse VoidCheckItems(int processID, DateTime processDate, int voidReason, List<int> voidItems)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.VoidCheckItems);
            request.ProcessID = processID;
            request.ProcessDate = processDate;
            request.ActionData.Add("VoidReason", voidReason);

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("EntryId", voidItems);

            request.ActionData.Add("VoidItems", dictionary);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Applies the cash payment.
        /// </summary>
        /// <param name="processID">The process ID.</param>
        /// <param name="processDate">The process date.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        public AlohaResponse ApplyCashPayment(int processID, DateTime processDate, decimal amount)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.ApplyCashPayment);
            request.ProcessID = processID;
            request.ProcessDate = processDate;
            request.ActionData.Add("Amount", amount.ToString(this.culture));

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Applies the payment.
        /// </summary>
        /// <param name="processID">The process ID.</param>
        /// <param name="processDate">The process date.</param>
        /// <param name="tenderID">The tender ID.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="tip">The tip.</param>
        /// <param name="cardID">The card ID.</param>
        /// <param name="expDate">The exp date.</param>
        /// <param name="trackInfo">The track info.</param>
        /// <param name="auth">The auth.</param>
        /// <returns></returns>
        public AlohaResponse ApplyPayment(int processID, DateTime processDate, int tenderID, decimal amount, decimal tip, string cardID, string expDate, string trackInfo, string auth)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.ApplyPayment);
            request.ProcessID = processID;
            request.ProcessDate = processDate;
            request.ActionData.Add("TenderID", tenderID);
            request.ActionData.Add("Amount", amount.ToString(this.culture));
            request.ActionData.Add("Tip", tip);
            request.ActionData.Add("CardID", cardID);
            request.ActionData.Add("ExpDate", expDate);
            request.ActionData.Add("TrackInfo", trackInfo);
            request.ActionData.Add("Auth", auth);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Deletes the payment.
        /// </summary>
        /// <param name="processID">The process ID.</param>
        /// <param name="processDate">The process date.</param>
        /// <param name="paymentID">The payment ID.</param>
        /// <returns></returns>
        public AlohaResponse DeletePayment(int processID, DateTime processDate, int paymentID)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.DeletePayment);
            request.ProcessID = processID;
            request.ProcessDate = processDate;
            request.ActionData.Add("PaymentID", paymentID);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Prints the check.
        /// </summary>
        /// <param name="processID">The process ID.</param>
        /// <param name="processDate">The process date.</param>
        /// <param name="checkID">The check ID.</param>
        /// <returns></returns>
        public AlohaResponse PrintCheck(int processID, DateTime processDate, int checkID)
        {
            // Create and initialize the request
            AlohaRequest request = new AlohaRequest(AlohaAction.PrintCheck);
            request.ProcessID = processID;
            request.ProcessDate = processDate;
            request.ActionData.Add("CheckID", checkID);

            // Execute the request
            AlohaResponse response = this.ExecuteRequest(request);
            return response;
        }

        /// <summary>
        /// Writes to log normal.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("AlohaConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes to log extended.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("AlohaConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Gets the webservice.
        /// </summary>
        public AlohaWebservice.TIMsoapConnectorservice Webservice
        {
            get
            {
                AlohaWebservice.TIMsoapConnectorservice webservice = new AlohaWebservice.TIMsoapConnectorservice();
                if (!this.url.IsNullOrWhiteSpace())
                    webservice.Url = this.url;
                return webservice;
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        /// <value></value>
        public string ErrorMessage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets or sets the employee number
        /// </summary>
        public int EmployeeNumber
        {
            get
            {
                return this.employeeNumber;
            }
            set
            {
                this.employeeNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the employee password
        /// </summary>
        public string EmployeePassword
        {
            get
            {
                return this.employeePassword;
            }
            set
            {
                this.employeePassword = value;
            }
        }

        /// <summary>
        /// Gets or sets the jobcode id
        /// </summary>
        public int JobCodeID
        {
            get
            {
                return this.jobCodeID;
            }
            set
            {
                this.jobCodeID = value;
            }
        }

        #endregion
    }
}
