﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using System.Xml;
using System.IO;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// AlohaResponse class
    /// </summary>
    public class AlohaResponse
    {
        #region Fields

        private string data = string.Empty;
        private int errorCode = -1;
        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the <see cref="AlohaResponse"/> type
        /// </summary>
        public AlohaResponse()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the data of the AlohaResponse
        /// </summary>
        public string Data
        {
            get
            {
                return this.data;
            }
            set
            {
                this.data = value;
            }
        }

        /// <summary>
        /// Gets or sets the error code
        /// </summary>
        public int ErrorCode
        {
            get
            {
                return this.errorCode;
            }
            set
            {
                this.errorCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion

    }
}
