﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// AlohaRequest class
    /// </summary>
    public class AlohaRequest
    {
        #region Fields

        private AlohaAction action;
        private int? processID = null;
        private DateTime? processDate = null;
        private AlohaActionData actionData = new AlohaActionData();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AlohaRequest"/> class.
        /// </summary>
        /// <param name="action">The action.</param>
        public AlohaRequest(AlohaAction action)
        {
            this.action = action;

            this.ActionData.Add("SoftwareToken", "FB06-62BB-B36D-4836-91BF-A323-9644-6F75");
            this.ActionData.Add("SoftwareVersion", "1.0.0.0");
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder request = new StringBuilder();

            int tabCounter = 0;

            // Open the Time Is Money tag
            request.AppendLine("<TIM>");
            tabCounter++;

            // Check whether the process tag whould be added
            if (this.processID.HasValue && this.processDate.HasValue)
            {
                request.AppendLine("<Process>".AddTabs(tabCounter));
                tabCounter++;

                request.AppendLine(string.Format("<ID>{0}</ID>", this.processID.Value).AddTabs(tabCounter));
                request.AppendLine(string.Format("<Date>{0:s}</Date>", this.processDate.Value).AddTabs(tabCounter));

                tabCounter--;
                request.AppendLine("</Process>".AddTabs(tabCounter));
            }

            // Add the Action tag
            tabCounter = 1;
            request.AppendLine(string.Format("<Action>{0}</Action>", this.action).AddTabs(tabCounter));

            // Check whether the ActionData tag should be added
            if (this.actionData.Count > 0)
            {
                request.AppendLine("<ActionData>".AddTabs(tabCounter));
                tabCounter++;

                foreach (string key in this.actionData.Keys)
                {
                    if (this.actionData[key] is Dictionary<string, object>)
                    {
                        request.AppendLine(string.Format("<{0}>", key).AddTabs(tabCounter));
                        tabCounter++;

                        Dictionary<string, object> dictionary = this.actionData[key] as Dictionary<string, object>;
                        foreach (string subKey in dictionary.Keys)
                        {
                            if (dictionary[subKey] is List<int>)
                            {
                                List<int> list = dictionary[subKey] as List<int>;
                                foreach (int item in list)
                                {
                                    request.AppendLine(string.Format("<{0}>{1}</{0}>", subKey, item).AddTabs(tabCounter));
                                }
                            }
                            else
                            {
                                request.AppendLine(string.Format("<{0}>{1}</{0}>", subKey, dictionary[subKey].ToString()).AddTabs(tabCounter));
                            }
                        }

                        tabCounter--;
                        request.AppendLine(string.Format("</{0}>", key).AddTabs(tabCounter));
                    }
                    else
                    {
                        request.AppendLine(string.Format("<{0}>{1}</{0}>", key, this.actionData[key].ToString()).AddTabs(tabCounter));
                    }
                }

                tabCounter--;
                request.AppendLine("</ActionData>".AddTabs(tabCounter));
            }

            // Close the Time Is Money tag
            tabCounter--;
            request.AppendLine("</TIM>".AddTabs(tabCounter));

            string blaatschaap = request.ToString();
            return request.ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>The action.</value>
        public AlohaAction Action
        {
            get
            {
                return this.action;
            }
            set
            {
                this.action = value;
            }
        }

        /// <summary>
        /// Gets or sets the process ID
        /// </summary>
        public int? ProcessID
        {
            get
            {
                return this.processID;
            }
            set
            {
                this.processID = value;
            }
        }

        /// <summary>
        /// Gets or sets the pricess date
        /// </summary>
        public DateTime? ProcessDate
        {
            get
            {
                return this.processDate;
            }
            set
            {
                this.processDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the action data.
        /// </summary>
        /// <value>The action data.</value>
        public AlohaActionData ActionData
        {
            get
            {
                return this.actionData;
            }
            set
            {
                this.actionData = value;
            }
        }

        #endregion
    }
}
