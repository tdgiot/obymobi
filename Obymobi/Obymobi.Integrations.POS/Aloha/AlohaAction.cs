﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// AlohaAction class
    /// </summary>
    public enum AlohaAction
    {
        /// <summary>
        /// GetRequestQueueSnapShot
        /// </summary>
        GetRequestQueueSnapShot,

        /// <summary>
        /// GetDeviceSettings
        /// </summary>
        GetDeviceSettings,

        /// <summary>
        /// SetDeviceSettings
        /// </summary>
        SetDeviceSettings,

        /// <summary>
        /// SetProfile
        /// </summary>
        SetProfile,

        /// <summary>
        /// GetProfiles
        /// </summary>
        GetProfiles,

        /// <summary>
        /// LogInEmployee
        /// </summary>
        LogInEmployee,

        /// <summary>
        /// ClockInEmployee
        /// </summary>
        ClockInEmployee,

        /// <summary>
        /// LogOutEmployee
        /// </summary>
        LogOutEmployee,

        /// <summary>
        /// ClockOutEmployee
        /// </summary>
        ClockOutEmployee,

        /// <summary>
        /// GetOpenTables
        /// </summary>
        GetOpenTables,

        /// <summary>
        /// GetOpenTabs
        /// </summary>
        GetOpenTabs,

        /// <summary>
        /// GetStaticFiles
        /// </summary>
        GetStaticFiles,

        /// <summary>
        /// iGetAlohaTerminals
        /// </summary>
        iGetAlohaTerminals,

        /// <summary>
        /// BeginTable
        /// </summary>
        BeginTable,

        /// <summary>
        /// EndTable
        /// </summary>
        EndTable,

        /// <summary>
        /// BeginTab
        /// </summary>
        BeginTab,

        /// <summary>
        /// EndTab
        /// </summary>
        EndTab,

        /// <summary>
        /// BeginCheck
        /// </summary>
        BeginCheck,

        /// <summary>
        /// EndCheck
        /// </summary>
        EndCheck,

        /// <summary>
        /// GetTableDetails
        /// </summary>
        GetTableDetails,

        /// <summary>
        /// GetCheckDetails
        /// </summary>
        GetCheckDetails,

        /// <summary>
        /// AddCheckItems
        /// </summary>
        AddCheckItems,

        /// <summary>
        /// VoidCheckItems
        /// </summary>
        VoidCheckItems,

        /// <summary>
        /// ApplyCashPayment
        /// </summary>
        ApplyCashPayment,

        /// <summary>
        /// ApplyPayment
        /// </summary>
        ApplyPayment,

        /// <summary>
        /// DeletePayment
        /// </summary>
        DeletePayment,

        /// <summary>
        /// PrintCheck
        /// </summary>
        PrintCheck,

        /// <summary>
        /// StartSession
        /// </summary>
        StartSession,

    }
}
