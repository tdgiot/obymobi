﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.POS.Interfaces;
using Dionysos;
using Adactus.RICO.UISupportServices.Contract.ServiceData.Enumerations;

namespace Obymobi.Integrations.POSLogic.POS.MicrosMcp
{
    /// <summary>
    /// CeniumConfigurationAdapter class
    /// </summary>
    public class MicrosMcpConfigurationAdapter : IConfigurationAdapter
    {
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.ApplicationId = terminal.PosValue1;
            this.CultureCode = terminal.PosValue2;
            this.WebserviceUrl = terminal.PosValue3;
            this.SetStoreId(terminal.PosValue4);

            this.DefaultCustomerEmail = terminal.PosValue5;
            this.DefaultCustomerPhonenumber = terminal.PosValue6;
            this.DefaultCustomerLastname = terminal.PosValue7;
            this.DefaultFulfillmentType = terminal.PosValue8;
            this.DefaultOrderClass = terminal.PosValue9;                                                           
            this.DefaultPaymentMethodType = terminal.PosValue10;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.ApplicationId = terminal.PosValue1;
            this.CultureCode = terminal.PosValue2;
            this.WebserviceUrl = terminal.PosValue3;
            this.SetStoreId(terminal.PosValue4);

            this.DefaultCustomerEmail = terminal.PosValue5;
            this.DefaultCustomerPhonenumber = terminal.PosValue6;
            this.DefaultCustomerLastname = terminal.PosValue7;
            this.DefaultFulfillmentType = terminal.PosValue8;
            this.DefaultOrderClass = terminal.PosValue9;
            this.DefaultPaymentMethodType = terminal.PosValue10;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.ApplicationId = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpApplicationId);
            this.CultureCode = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpCultureCode);
            this.WebserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpWebserviceUrl);
            this.StoreId = Dionysos.ConfigurationManager.GetInt(POSConfigurationConstants.MicrosMcpStoreId);

            this.DefaultOrderClass = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpDefaultOrderClass);
            this.DefaultFulfillmentType = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpFulfillmentType);
            this.DefaultCustomerEmail = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpCustomerEmail);
            this.DefaultCustomerPhonenumber = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpCustomerPhonenumber);
            this.DefaultCustomerLastname = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpCustomerLastname);
            this.DefaultPaymentMethodType = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpPaymentMethodType);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpApplicationId, this.ApplicationId);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpCultureCode, this.CultureCode);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpWebserviceUrl, this.WebserviceUrl);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpStoreId, this.StoreId);

            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpDefaultOrderClass, this.DefaultOrderClass);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpFulfillmentType, this.DefaultFulfillmentType);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpCustomerEmail, this.DefaultCustomerEmail);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpCustomerPhonenumber, this.DefaultCustomerPhonenumber);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpCustomerLastname, this.DefaultCustomerLastname);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.MicrosMcpPaymentMethodType, this.DefaultPaymentMethodType);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.ApplicationId;
            terminal.PosValue2 = this.CultureCode;
            terminal.PosValue3 = this.WebserviceUrl;
            terminal.PosValue4 = this.StoreId.ToString();

            terminal.PosValue5 = this.DefaultCustomerEmail;
            terminal.PosValue6 = this.DefaultCustomerPhonenumber;
            terminal.PosValue7 = this.DefaultCustomerLastname;
            terminal.PosValue8 = this.DefaultFulfillmentType;
            terminal.PosValue9 = this.DefaultOrderClass;
            terminal.PosValue10 = this.DefaultPaymentMethodType;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.ApplicationId;
            terminal.PosValue2 = this.CultureCode;
            terminal.PosValue3 = this.WebserviceUrl;
            terminal.PosValue4 = this.StoreId.ToString();

            terminal.PosValue5 = this.DefaultCustomerEmail;
            terminal.PosValue6 = this.DefaultCustomerPhonenumber;
            terminal.PosValue7 = this.DefaultCustomerLastname;
            terminal.PosValue8 = this.DefaultFulfillmentType;
            terminal.PosValue9 = this.DefaultOrderClass;
            terminal.PosValue10 = this.DefaultPaymentMethodType;
        }

        private void SetStoreId(string value)
        {
            int parsedInt;
            if (int.TryParse(value, out parsedInt))
            {
                this.StoreId = parsedInt;
            }
            else
            {
                throw new InvalidCastException(StringUtil.FormatSafe("Could not convert '{0}' to int for the MicroMcp StoreId.", parsedInt));
            }
        }        

        #region Properties

        public string ApplicationId { get; set; }
        public string CultureCode { get; set; }
        public string WebserviceUrl { get; set; }        
        public int StoreId { get; set; }
        public string DefaultOrderClass { get; set; }
        public string DefaultFulfillmentType { get; set; }
        public string DefaultCustomerEmail { get; set; }
        public string DefaultCustomerPhonenumber { get; set; }
        public string DefaultCustomerLastname { get; set; }
        public string DefaultPaymentMethodType { get; set; }

        #endregion
    }
}
