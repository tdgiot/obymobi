﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Integrations.POSLogic.POS.MicrosMcp
{
    public static class MicrosConfigurationAlias
    {
        public const string None = "None";
        public const string Container = "Container";
        public const string ComplexContainer = "ComplexContainer";

    }
}
