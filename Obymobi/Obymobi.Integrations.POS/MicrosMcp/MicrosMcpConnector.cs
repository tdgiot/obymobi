﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Logic.POS.Generic;
using Obymobi.Logic.Model;
using System.Net;
using Obymobi.Enums;
using Adactus.RICO.UISupportServices.Contract.ServiceData.Entities.DTO;
using Adactus.RICO.UISupportServices.Contract.ServiceData.Message.Request;
using Adactus.RICO.UISupportServices.Contract.ServiceData.Message.Response;
using Adactus.RICO.UISupportServices.Contract.ServiceData.Enumerations;
using UISupportServicesTestHarness.SecurityService;
using UISupportServicesTestHarness.PaymentService;
using UISupportServicesTestHarness.StoreService;
using UISupportServicesTestHarness.OrderService;
using System.ServiceModel;

// Design choices:
// It's indeed a bit strange to have TryGetStore and TryGetSession without the OUT parameter, but just accept that
// as I didn't think it would add a lot of more clearity to define the variable 
// as out every time you need it.

namespace Obymobi.Integrations.POSLogic.POS.MicrosMcp
{
    /// <summary>
    /// CeniumConnector class
    /// </summary>
    public class MicrosMcpConnector : PosConnectorBase, IPOSConnector
    {

        #region ResultEnum

        public enum MicrosMcpConnectorResult
        {
            ZeroAvailableTimes = 300
        }

        #endregion

        #region Fields

        // Retrieve the settings from the config file
        private string webserviceUrl;
        private string applicationId;
        private string cultureCode;
        private int storeId;

        private OrderClass defaultOrderClass;
        private FulfillmentTimeType defaultFulfillmentTimeType;
        private PaymentMethodType defaultPaymentmethod;
        private string defaultEmail;
        private string defaultPhonenumber;
        private string defaultLastname;

        // Handy to keep a sync report
        private StringBuilder syncReport = new StringBuilder();

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CeniumConnector"/> class.
        /// </summary>
        public MicrosMcpConnector()
        {
            this.ConnectorType = POSConnectorType.MicrosMcp;
        
            this.WriteToLogExtended("Constructor", "Start");

            // Init local vars
            this.webserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpWebserviceUrl);
            this.applicationId = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpApplicationId);
            this.cultureCode = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpCultureCode);
            this.storeId = Dionysos.ConfigurationManager.GetInt(POSConfigurationConstants.MicrosMcpStoreId);
            this.defaultPhonenumber = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpCustomerPhonenumber);
            this.defaultLastname = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpCustomerLastname);
            this.defaultEmail = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpCustomerEmail);

            OrderClass orderClass;
            if (!EnumUtil.TryParse(Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpDefaultOrderClass), out orderClass))
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, "'{0}' is not a valid value for enum 'OrderClass'", Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpDefaultOrderClass));
            this.defaultOrderClass = orderClass;

            FulfillmentTimeType fulfillmentType;
            if (!EnumUtil.TryParse(Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpFulfillmentType), out fulfillmentType))
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, "'{0}' is not a valid value for enum 'FulfillmentTimeType'", Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpFulfillmentType));
            this.defaultFulfillmentTimeType = fulfillmentType;

            PaymentMethodType paymentMethod;
            if (!EnumUtil.TryParse(Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpPaymentMethodType), out paymentMethod))
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, "'{0}' is not a valid value for enum 'PaymentMethodType'", Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.MicrosMcpPaymentMethodType));
            this.defaultPaymentmethod = paymentMethod;

            this.paymentService = null;
            this.securityService = null;
            this.storeService = null;
            this.orderService = null;
            this.sessionLastUsed = null;
            this.sessionDto = null;
            this.storeOrderOptions = null;
            this.menu = null;

            bool configurationComplete = false;

            this.WriteToLogExtended("Constructor", "Verify settings");

            // Verify if all settings are set
            if (this.webserviceUrl.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "Webservice url is empty");
            else if (this.applicationId.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "ApplicationId is empty");
            else if (this.cultureCode.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "CultureCode is empty");
            else if (this.storeId < 1)
                this.WriteToLogNormal("Constructor", "StoreId not set: " + this.storeId);
            else
                configurationComplete = true;

            if (!this.webserviceUrl.EndsWith("/"))
                this.webserviceUrl += "/";

            // Output the current config to the log:
            this.WriteToLogNormal("Constructor", "Webservice Url: {0}", this.webserviceUrl);
            this.WriteToLogNormal("Constructor", "Webservice ApplicationId: {0}", this.applicationId);
            this.WriteToLogNormal("Constructor", "Webservice CultureCode: {0}", this.cultureCode);
            this.WriteToLogNormal("Constructor", "Webservice Store Id: {0}", this.storeId);

            //this.ShowCompanies();

            if (!configurationComplete)
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, "Configuration Incomplete");

            this.WriteToLogExtended("Constructor", "End");
        }

        #region Session Management

        private DateTime? sessionLastUsed;
        private SessionDTO sessionDto;

        private SessionDTO BeginSession()
        {
            bool success = false;
            try
            {
                // GK Reusing/resuming sessions only seems to give us trouble
                //// If we have a session that in principle shouldn't have expired, try to reuse that.
                //if (this.sessionDto != null && this.sessionLastUsed.HasValue &&
                //    (DateTime.Now - this.sessionLastUsed.Value).TotalMinutes < 10)
                //{
                //    // Try to Resume Session
                //    this.ResumeSession();
                //}
                //else
                //{
                //    this.sessionDto = null;
                //}

                // GK If you re-enable the above, don't forget to comment this section :)
                if (this.sessionDto != null)
                {
                    this.WriteToPosConnectorLog("BeginSession", "There was still a session active which is now ended.");
                    this.sessionLastUsed = null;
                    this.sessionDto = null;
                    this.EndSession();
                }

                if (this.sessionDto == null)
                {
                    // Start Session
                    success = this._openSession();
                }
            }
            catch (POSException)
            {
                throw;
            }
            catch (Exception ex)
            {
                this.WriteToPosConnectorLog("BeginSession", "Failed to start session: {0}", ex.Message);
                this.WriteToPosConnectorLog("BeginSession", ex.GetAllMessages(true));
                throw new POSException(OrderProcessingError.MicrosMcpCouldntGetSession, ex, "Failed to get Session for ApplicationId: '{0}', CultureCode: '{1}'", this.applicationId, this.cultureCode);
            }
            finally
            {
                if (!success)
                {
                    this.sessionLastUsed = null;
                    this.sessionDto = null;
                }
            }

            return this.sessionDto;
        }

        /// <summary>
        /// This method should NOT be called by any claler but BeginSession()
        /// </summary>
        /// <returns></returns>
        private void ResumeSession()
        {
            GetSessionStateRequest statusRequest = new GetSessionStateRequest();
            GetSessionStateResponse statusResponse = this.SecurityService.GetSessionState(this.sessionDto, statusRequest);

            if (statusResponse.State != null && statusResponse.IsSuccess)
            {
                // We continue with the Session
                this.WriteToPosConnectorLog("ResumeSession", "Resuming with session: {0}", this.sessionDto.SessionId);
                this.sessionLastUsed = DateTime.Now;
            }
            else
            {
                this.sessionDto = null;
            };
        }

        /// <summary>
        /// This method should NOT be called by any claler but BeginSession()
        /// </summary>
        /// <returns></returns>
        private bool _openSession() // It's not our convention but wanted to ensure you don't use this method (use BeginSession)
        {
            bool success = false;

            // Start Session
            StartSessionRequest startRequest = new StartSessionRequest();
            startRequest.ApplicationId = this.applicationId;
            startRequest.CultureCode = this.cultureCode;
            StartSessionResponse startResponse = this.SecurityService.StartSession(startRequest);

            if (startResponse.IsSuccess &&
                startResponse.ResultType == StartSessionResultType.Success &&
                startResponse.Session != null)
            {
                // All good!
                this.WriteToPosConnectorLog("OpenSession", "Session started: {0}", startResponse.Session.SessionId);
                this.sessionLastUsed = DateTime.Now;
                this.sessionDto = startResponse.Session;
                success = true;
            }
            else
            {
                // Not good.
                this.WriteToPosConnectorLog("OpenSession", "Session not started.");
                this.WriteToPosConnectorLog("OpenSession", "Response.ResultType: {0}", startResponse.ResultType);
                string problemLog = this.LogResponse("Session not started", startResponse);
                throw new POSException(OrderProcessingError.MicrosMcpCouldntGetSession, "Failed to get Session for ApplicationId: '{0}', CultureCode: '{1}'. Problem log: {2}", this.applicationId, this.cultureCode, problemLog);
            }

            return success;
        }

        private void EndSession()
        {
            try
            {
                if (this.sessionDto != null)
                {
                    this.SecurityService.EndSession(this.sessionDto);
                }
            }
            catch (Exception ex)
            {
                this.WriteToPosConnectorLog("EndSession", "Failed to end session: {0}", ex.Message);
                this.WriteToPosConnectorLog("EndSession", ex.GetAllMessages(true));
            }
            finally
            {
                this.sessionDto = null;
                this.sessionLastUsed = null;
            }
        }

        #endregion

        #region Store Management

        private void LoadStore()
        {
            bool success = false;

            try
            {
                // Load the Store information
                this.LoadStoreDetail();
                this.LoadStoreOrderOptions();
                success = true;
            }
            catch (POSException)
            {
                throw;
            }
            catch (Exception ex)
            {
                this.WriteToPosConnectorLog("LoadStore", "Failed to get store: {0}", ex.Message);
                this.WriteToPosConnectorLog("LoadStore", ex.GetAllMessages(true));
                throw new POSException(OrderProcessingError.MicrosMcpCouldntGetStore, ex, "Failed to get Session for ApplicationId: '{0}', CultureCode: '{1}'", this.applicationId, this.cultureCode);
            }
            finally
            {
                if (!success)
                {
                    this.store = null;
                    this.storeOrderOptions = null;
                }
            }
        }

        StoreDTO store;
        GetStoreOrderOptionsResponse storeOrderOptions;
        private void LoadStoreDetail()
        {
            // No try/catch because that's arrange in the calling method LoadStore

            if (this.sessionDto == null)
                throw new POSException(OrderProcessingError.MicrosMcpSessionWasntInitialized, "Session is required for LoadStoreDetail");

            // Get Store
            StoreDetailRequest storeRequest = new StoreDetailRequest();
            storeRequest.StoreId = this.storeId;
            StoreDetailResponse storeResponse = this.StoreService.GetStoreDetail(this.sessionDto, storeRequest);

            if (storeResponse.IsSuccess)
            {
                // Good!
                this.store = storeResponse.Store;
            }
            else
            {
                // Not so good
                this.WriteToPosConnectorLog("LoadStoreDetail", "LoadStoreDetail failed.");
                string problemLog = this.LogResponse("LoadStoreDetail failed", storeResponse);
                throw new POSException(OrderProcessingError.MicrosMcpCouldntGetStore, "Failed to LoadStoreDetail for Session: '{0}', StoreId: '{1}'. Problem log: {2}", this.sessionDto.SessionId, this.storeId, problemLog);
            }
        }

        private void LoadStoreOrderOptions()
        {
            // No try/catch because that's arrange in the calling method LoadStore            
            if (this.sessionDto == null)
                throw new POSException(OrderProcessingError.MicrosMcpSessionWasntInitialized, "Session is required for LoadStoreOrderOptions");

            // Prepare & do GetStoreOrderOptions request
            GetStoreOrderOptionsRequest getStoreRequest = new GetStoreOrderOptionsRequest();
            getStoreRequest.StoreId = this.storeId;
            getStoreRequest.OrderTime = DateTime.Now;

            GetStoreOrderOptionsResponse getStoreResponse = this.StoreService.GetStoreOrderOptions(this.sessionDto, getStoreRequest);

            if (getStoreResponse.IsSuccess &&
                getStoreResponse.ResultType == GetStoreOrderOptionsResultType.Success)
            {
                // Good
                this.storeOrderOptions = getStoreResponse;                
            }
            else
            {
                // Not so good
                this.WriteToPosConnectorLog("LoadStoreOrderOptions", "GetStoreOrderOptionsRequest failed.");
                this.WriteToPosConnectorLog("LoadStoreOrderOptions", "Response.ResultType: {0}", getStoreResponse.ResultType.ToString());
                string problemLog = this.LogResponse("GetStoreOrderOptionsRequest failed", getStoreResponse);
                throw new POSException(OrderProcessingError.MicrosMcpCouldntGetStore, "Failed to GetStoreOrderOptionsRequest in Session: '{0}', StoreId: '{1}'. Problem log: {2}", this.sessionDto.SessionId, this.storeId, problemLog);
            }
        }

        private void LogStoreInformation()
        {
            if (this.store != null)
            {
                this.WriteToLogNormal("LogStoreInformation", "CateringDelayHoursOnOrderOption = {0}", this.store.CateringDelayHoursOnOrderOption);
                this.WriteToLogNormal("LogStoreInformation", "CateringOrderCancelHourLimit = {0}", this.store.CateringOrderCancelHourLimit);
                this.WriteToLogNormal("LogStoreInformation", "CateringOrderConfirmationHourLimit = {0}", this.store.CateringOrderConfirmationHourLimit);
                this.WriteToLogNormal("LogStoreInformation", "DistanceInKmsFromSearchOrigin = {0}", this.store.DistanceInKmsFromSearchOrigin);
                this.WriteToLogNormal("LogStoreInformation", "DistanceInMilesFromSearchOrigin = {0}", this.store.DistanceInMilesFromSearchOrigin);
                this.WriteToLogNormal("LogStoreInformation", "Email = {0}", this.store.Email);
                this.WriteToLogNormal("LogStoreInformation", "IsPrimary = {0}", this.store.IsPrimary);
                this.WriteToLogNormal("LogStoreInformation", "MaxFutureOrderDays = {0}", this.store.MaxFutureOrderDays);
                this.WriteToLogNormal("LogStoreInformation", "SpecialInstructionsMaxLength = {0}", this.store.SpecialInstructionsMaxLength);
                this.WriteToLogNormal("LogStoreInformation", "StoreName = {0}", this.store.StoreName);
                this.WriteToLogNormal("LogStoreInformation", "SupportsDeliveryToSearchAddress = {0}", this.store.SupportsDeliveryToSearchAddress);
                this.WriteToLogNormal("LogStoreInformation", "TelephoneNumber = {0}", this.store.TelephoneNumber);

                this.WriteToLogNormal("LogStoreInformation", "Address - AddressDescription = {0}", this.store.StoreAddress.AddressDescription);
                this.WriteToLogNormal("LogStoreInformation", "Address - AddressId = {0}", this.store.StoreAddress.AddressId);
                this.WriteToLogNormal("LogStoreInformation", "Address - AddressSearchId = {0}", this.store.StoreAddress.AddressSearchId);
                this.WriteToLogNormal("LogStoreInformation", "Address - AddressType = {0}", this.store.StoreAddress.AddressType);
                this.WriteToLogNormal("LogStoreInformation", "Address - BuildingLetter = {0}", this.store.StoreAddress.BuildingLetter);
                this.WriteToLogNormal("LogStoreInformation", "Address - BuildingName = {0}", this.store.StoreAddress.BuildingName);
                this.WriteToLogNormal("LogStoreInformation", "Address - BuildingNumber = {0}", this.store.StoreAddress.BuildingNumber);
                this.WriteToLogNormal("LogStoreInformation", "Address - BuildingNumberHigh = {0}", this.store.StoreAddress.BuildingNumberHigh);
                this.WriteToLogNormal("LogStoreInformation", "Address - BuildingNumberLow = {0}", this.store.StoreAddress.BuildingNumberLow);
                this.WriteToLogNormal("LogStoreInformation", "Address - BuildingNumberParity = {0}", this.store.StoreAddress.BuildingNumberParity);
                this.WriteToLogNormal("LogStoreInformation", "Address - CountryId = {0}", this.store.StoreAddress.CountryId);
                this.WriteToLogNormal("LogStoreInformation", "Address - CountryText = {0}", this.store.StoreAddress.CountryText);
                this.WriteToLogNormal("LogStoreInformation", "Address - District = {0}", this.store.StoreAddress.District);
                this.WriteToLogNormal("LogStoreInformation", "Address - FloorNumber = {0}", this.store.StoreAddress.FloorNumber);
                this.WriteToLogNormal("LogStoreInformation", "Address - Intersection = {0}", this.store.StoreAddress.Intersection);
                this.WriteToLogNormal("LogStoreInformation", "Address - IsFavourite = {0}", this.store.StoreAddress.IsFavourite);
                this.WriteToLogNormal("LogStoreInformation", "Address - Landmark = {0}", this.store.StoreAddress.Landmark);
                this.WriteToLogNormal("LogStoreInformation", "Address - Latitude = {0}", this.store.StoreAddress.Latitude);
                this.WriteToLogNormal("LogStoreInformation", "Address - Longitude = {0}", this.store.StoreAddress.Longitude);
                this.WriteToLogNormal("LogStoreInformation", "Address - OrganisationName = {0}", this.store.StoreAddress.OrganisationName);
                this.WriteToLogNormal("LogStoreInformation", "Address - PostCodeOrZip = {0}", this.store.StoreAddress.PostCodeOrZip);
                this.WriteToLogNormal("LogStoreInformation", "Address - ProviderAddressKey = {0}", this.store.StoreAddress.ProviderAddressKey);
                this.WriteToLogNormal("LogStoreInformation", "Address - RoomNumber = {0}", this.store.StoreAddress.RoomNumber);
                this.WriteToLogNormal("LogStoreInformation", "Address - RoomNumberHigh = {0}", this.store.StoreAddress.RoomNumberHigh);
                this.WriteToLogNormal("LogStoreInformation", "Address - RoomNumberLow = {0}", this.store.StoreAddress.RoomNumberLow);
                this.WriteToLogNormal("LogStoreInformation", "Address - RoomNumberParity = {0}", this.store.StoreAddress.RoomNumberParity);
                this.WriteToLogNormal("LogStoreInformation", "Address - StreetName = {0}", this.store.StoreAddress.StreetName);
                this.WriteToLogNormal("LogStoreInformation", "Address - TaxNumber = {0}", this.store.StoreAddress.TaxNumber);
                this.WriteToLogNormal("LogStoreInformation", "Address - Territory = {0}", this.store.StoreAddress.Territory);
                this.WriteToLogNormal("LogStoreInformation", "Address - TownCity = {0}", this.store.StoreAddress.TownCity);

                foreach (KeyValuePair<string, string> keyValue in this.store.StoreKeyValuePairList)
                {
                    this.WriteToLogNormal("LogStoreInformation", "KeyValuePair: {0} = {1}", keyValue.Key, keyValue.Value);
                }

                foreach (KeyValuePair<string, string> attribute in this.store.StoreKeyValuePairList)
                {
                    this.WriteToLogNormal("LogStoreInformation", "Attribute: {0} = {1}", attribute.Key, attribute.Value);
                }

                foreach (OpeningDayOfWeekDTO openingHour in this.store.OpeningHours)
                {
                    this.WriteToLogNormal("LogStoreInformation", "Opening Hour: Day of Week = {0}", openingHour.DayOfWeek);
                    this.WriteToLogNormal("LogStoreInformation", "Opening Hour: Localized Day of Week = {0}", openingHour.LocalizedDayOfWeek);
                    this.WriteToLogNormal("LogStoreInformation", "Opening Hour: Store Close Message = {0}", openingHour.StoreCloseMessge);
                    this.WriteToLogNormal("LogStoreInformation", "Opening Hour: Store Open = {0}", openingHour.StoreOpen);

                    foreach (OpeningTimePeriodDTO period in openingHour.OpeningPeriods)
                    {
                        this.WriteToLogNormal("LogStoreInformation", "Opening Hour - Open Time: {0}, Close Time: {1}", period.OpenTime, period.CloseTime);
                    }
                }
            }
            else
                this.WriteToLogNormal("LogStoreInformation", "Store == null");

            if (this.storeOrderOptions != null)
            {
                GetStoreOrderOptionsResponse options = this.storeOrderOptions;

                foreach (var option in options.StoreOrderOptions)
                {
                    this.WriteToLogNormal("LogStoreInformation", "Store Order Option: Store Id: {0}, OrderClass: {1}", option.StoreId, option.StoreOrderClass.ToString());

                    foreach (var slot in option.AvailableOrderSlots)
                    {
                        this.WriteToLogNormal("LogStoreInformation", "Available Order Slot: {0}", slot);
                    }

                    foreach (var slot in option.AvailableOrderDateSlots)
                    {
                        this.WriteToLogNormal("LogStoreInformation", "Available Order Slot Dates: {0}", slot);
                    }
                }
            }
            else
                this.WriteToLogNormal("LogStoreInformation", "Store Order Options == null");
        }

        private DateTime? GetFirstAvailableOrderSlot()
        {
            DateTime? toReturn = null;
            if (this.store == null)
                throw new POSException(OrderProcessingError.MicrosMcpStoreWasntInitialized, "Store is required for GetFirstAvailableOrderSlot");

            // Check if we have a StoreOrderOption with Dates and Slots.
            if (this.storeOrderOptions == null)
                this.WriteToPosConnectorLog("GetFirstAvailableOrderSlot", "Can't get a Available Order Slot: this.storeOrderOptions == null");
            else if (this.storeOrderOptions.StoreOrderOptions == null)
                this.WriteToPosConnectorLog("GetFirstAvailableOrderSlot", "Can't get a Available Order Slot: this.storeOrderOptions.StoreOrderOptions == null");
            else if (this.storeOrderOptions.StoreOrderOptions.Count <= 0)
                this.WriteToPosConnectorLog("GetFirstAvailableOrderSlot", "Can't get a Available Order Slot: this.storeOrderOptions.StoreOrderOptions == null");
            else
            {
                StoreOrderOptionDTO option = this.storeOrderOptions.StoreOrderOptions.FirstOrDefault(x => x.AvailableOrderDateSlots.Count > 0 && x.AvailableOrderSlots.Count > 0);
                if (option == null)
                {
                    this.WriteToPosConnectorLog("GetFirstAvailableOrderSlot", "Can't get a Available Order Slot: No StoreOrderOption with AvailableOrderDateSlots and AvailableOrderSlots");
                }
                else
                {                    
                    toReturn = DateTimeUtil.CombineDateAndTime(option.AvailableOrderSlots[0], option.AvailableOrderDateSlots[0]);
                    this.WriteToPosConnectorLog("GetFirstAvailableOrderSlot", "Found available slot: {0}", toReturn.Value);
                }
            }
            return toReturn;
        }

        #endregion

        #region Menu management

        CategoryDTO menu;
        int? menuId;

        public override void PrePosSynchronisation()
        {
            this.WriteToPosConnectorLog("PrePosSynchronisation", "Begin");
            base.PrePosSynchronisation();

            // Start Session
            this.WriteToPosConnectorLog("PrePosSynchronisation", "Begin Session");
            this.BeginSession();

            // Load Store
            this.WriteToPosConnectorLog("PrePosSynchronisation", "Load Store");
            this.LoadStore();

            // Log store info to log
            this.LogStoreInformation();

            // Load Menu
            this.WriteToPosConnectorLog("PrePosSynchronisation", "Load Menu");
            this.LoadMenu(this.defaultOrderClass, this.defaultFulfillmentTimeType);

            // Log menu info to log
            this.PrintMenuToLog(this.menu);

            this.WriteToPosConnectorLog("PrePosSynchronisation", "End");
        }

        public override void PostPosSynchronisation()
        {
            this.WriteToPosConnectorLog("PostPosSynchronisation", "Begin");
            base.PostPosSynchronisation();

            // End Session
            this.EndSession();

            this.WriteToPosConnectorLog("PostPosSynchronisation", "End");
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public override Model.Posproduct[] GetPosproducts()
        {
            this.WriteToLogExtended("GetPosproducts", "Begin");

            this.syncReport = new StringBuilder();            
            this.syncReport.AppendFormatLine("******** SYNC PRODUCTS START ********");
            this.syncReport.AppendFormatLine("Sync started at: {0}", DateTime.Now.ToString());

            // Log the settings where you sync with:
            List<Posproduct> posProducts = new List<Posproduct>();

            try
            {
                // Do the logic, log extensively.
                this.WriteToPosConnectorLog("GetPosproducts", "Extract Products from Menu");

                // Recuresively get all products
                this.LoadProductsFromMenu(syncReport, this.menu, posProducts);

                // Finish.
                this.syncReport.AppendFormatLine("Sync finished with success at: {0} with {1} products", DateTime.Now.ToString(), posProducts.Count);
                this.syncReport.AppendFormatLine("******** SYNC PRODUCTS FINISH ********");
                this.WriteToPosConnectorLog("GetPosproducts", this.syncReport.ToString());
            }
            catch (Exception ex)
            {
                this.WriteToLogExtended("GetPosproducts", "End - Failure");
                this.WriteToLogExtended("GetPosproducts", ex.Message);
                this.WriteToLogExtended("GetPosproducts", ex.StackTrace);
                this.syncReport.AppendFormatLine("Sync finished with failure at: {0}", DateTime.Now.ToString());
                this.syncReport.AppendFormatLine("******** SYNC PRODUCTS FINISH ********");
                this.WriteToPosConnectorLog("GetPosproducts", this.syncReport.ToString());

                throw;
            }

            this.WriteToLogExtended("GetPosproducts", "End - Success");
            return posProducts.ToArray();
        }

        private void LoadMenu(OrderClass orderClass, FulfillmentTimeType fullFillmentType)
        {
            bool success = false;

            try
            {
                // Get Store (also loads session)
                if (this.sessionDto == null)
                    throw new POSException(OrderProcessingError.MicrosMcpSessionWasntInitialized, "Session is required for LoadMenu");

                if (this.store == null)
                    throw new POSException(OrderProcessingError.MicrosMcpStoreWasntInitialized, "Store is required for LoadMenu");

                // Required to give an ordering time, so check if we have one.
                DateTime? orderTime = this.GetFirstAvailableOrderSlot();
                if (orderTime == null)
                {
                    this.WriteToPosConnectorLog("LoadMenu", "Unable to retrieve a valid order slot, falling back to DateTime.Now");
                    orderTime = DateTime.Now; // We fall back to this... 
                }

                // Get Menu
                GetMenuAndStartOrderRequest getMenuRequest = new GetMenuAndStartOrderRequest();
                getMenuRequest.StoreId = this.storeId;
                getMenuRequest.OrderClass = orderClass;
                getMenuRequest.FulfillmentTimeType = fullFillmentType;
                getMenuRequest.OrderTime = orderTime.Value; // GK Todo - logic to get a business hour + find out how to get the menu without an OrderTime
                getMenuRequest.RemoveEmptyCategory = false;

                if (this.menuId.HasValue)
                    getMenuRequest.CurrentMenuId = this.menuId.Value;

                GetMenuAndStartOrderResponse getMenuResponse = this.OrderService.GetMenuAndStartOrder(this.sessionDto, getMenuRequest);

                if (getMenuResponse.IsSuccess &&
                    getMenuResponse.ResultType == StoreStartOrderResultType.Success &&
                    // Menu will null if we requested it earlier with the same Id
                    getMenuResponse.MenuRootCategory != null || (getMenuResponse.MenuRootCategory == null && this.menuId.HasValue))
                {
                    // Good
                    if (getMenuResponse.MenuRootCategory != null) // We already had retrieved this menu
                    {
                        this.menu = getMenuResponse.MenuRootCategory;                                                    
                    }

                    this.menuId = getMenuResponse.MenuId;
                }
                else
                {
                    // Not so good
                    this.WriteToPosConnectorLog("LoadMenu", "GetMenuAndStartOrderRequest failed.");
                    this.WriteToPosConnectorLog("LoadMenu", "Response.ResultType: {0}", getMenuResponse.ResultType.ToString());
                    string problemLog = this.LogResponse("GetMenuAndStartOrderRequest failed", getMenuResponse);
                    throw new POSException(OrderProcessingError.MicrosMcpCouldntGetStore, "Failed to GetMenuAndStartOrderRequest in Session: '{0}', StoreId: '{1}', OrderClass: '{2}', Fullfillment: '{3}', OrderTime: '{4}', RemoveEmptyCategory: {5}, Problem log: {6}",
                        this.sessionDto.SessionId, this.storeId, getMenuRequest.OrderClass, getMenuRequest.FulfillmentTimeType, getMenuRequest.OrderTime, getMenuRequest.RemoveEmptyCategory, problemLog);
                }
                success = true;
            }
            catch (POSException)
            {
                throw;
            }
            catch (Exception ex)
            {
                this.WriteToPosConnectorLog("LoadMenu", "Failed to get store: {0}", ex.Message);
                this.WriteToPosConnectorLog("LoadMenu", ex.GetAllMessages(true));
                throw new POSException(OrderProcessingError.MicrosMcpCouldntGetStore, ex, "Failed to get Session for ApplicationId: '{0}', CultureCode: '{1}'", this.applicationId, this.cultureCode);
            }
            finally
            {
                if (!success)
                {
                    this.menu = null;
                    this.menuId = null;
                }
            }
        }

        private void PrintMenuToLog(CategoryDTO category, int level = 0)
        {
            this.WriteToPosConnectorLog("PrintMenuToLog", "{0}- {1} | {2}", "".PadLeft(level * 3), category.Title, category.Description);

            foreach (CategoryDTO cat in category.Categories)
            {
                level++;
                this.PrintMenuToLog(cat, level);
                level--;
            }

            foreach (ProductDTO product in category.Products)
            {
                this.WriteToPosConnectorLog("PrintMenuToLog", "{0}* {1} | {2} | {3}", "".PadLeft((level * 3) + 2), product.Title, product.ProductId, product.CustomConfigurationAlias);
                if (product.RelatedProducts != null)
                {
                    foreach (ProductRelationshipDTO productRelationship in product.RelatedProducts)
                    {
                        this.WriteToPosConnectorLog("PrintMenuToLog", "{0}  > {1} - {2}", "".PadLeft((level * 3) + 2), productRelationship.Description, productRelationship.ProductId);
                        foreach (ProductRelationshipDTO choiceProduct in productRelationship.RelatedProducts)
                        {
                            this.WriteToPosConnectorLog("PrintMenuToLog", "{0}    : {1} - {2}", "".PadLeft((level * 3) + 2), choiceProduct.Description, choiceProduct.ProductId);
                        }
                    }
                }
            }
        }

        private void LoadProductsFromMenu(StringBuilder importReport, CategoryDTO category, List<Posproduct> posProducts)
        {
            // Add products from this category to the list
            foreach (ProductDTO product in category.Products)
            {                
                // GK This might be sleasy but I think it might just work ;)
                // Since they use Products as Alterations some products are not relevant.     
                // So how do I decide? Each product with Alterations without Options gets skipped
                if (product.RelatedProducts.Count > 0 && product.RelatedProducts[0].RelatedProducts.Count == 0)
                {
                    importReport.AppendFormatLine("{0} is NOT imported as it's assumed to be an Alteration-product.", product.Title);
                    continue;
                }

                Posproduct posProduct = new Posproduct();
                posProduct.Name = product.Title;
                posProduct.Description = product.MarketingDescription.IsNullOrWhiteSpace() ? product.Description : product.MarketingDescription;
                posProduct.ExternalId = product.ProductId.ToString();
                posProduct.ExternalPoscategoryId = category.CategoryId.ToString();
                posProduct.PriceIn = product.Price;
                posProduct.VatTariff = 2;
                posProduct.SortOrder = product.SortOrder;

                // Check type of configuration (alterations)
                if (product.CustomConfigurationAlias.IsNullOrWhiteSpace() || product.CustomConfigurationAlias.Equals(MicrosConfigurationAlias.None, StringComparison.InvariantCultureIgnoreCase))
                {
                    // None
                }
                else if (product.CustomConfigurationAlias.Equals(MicrosConfigurationAlias.Container, StringComparison.InvariantCultureIgnoreCase))
                {
                    List<Posalteration> posalterations = new List<Posalteration>();
                    if (product.RelatedProducts != null)
                    {
                        foreach (ProductRelationshipDTO productRelationship in product.RelatedProducts)
                        {

                            // Create the Alteration
                            Posalteration alteration = new Posalteration();
                            alteration.ExternalId = productRelationship.ProductId.ToString();
                            alteration.Name = productRelationship.Description.IsNullOrWhiteSpace() ? productRelationship.Title : productRelationship.Description;                            

                            // Append it's options
                            List<Posalterationoption> alterationOptions = new List<Posalterationoption>();
                            foreach (ProductRelationshipDTO choiceProduct in productRelationship.RelatedProducts)
                            {
                                Posalterationoption option = new Posalterationoption();
                                option.ExternalId = choiceProduct.ProductId.ToString();
                                option.Name = choiceProduct.Description.IsNullOrWhiteSpace() ? choiceProduct.Title : choiceProduct.Description;
                                option.PriceIn = choiceProduct.Price;
                                alterationOptions.Add(option);
                            }

                            alteration.Posalterationoptions = alterationOptions.ToArray();

                            posalterations.Add(alteration);
                        }

                        // Add the Atleration to the product
                        posProduct.Posalterations = posalterations.ToArray();
                    }
                }
                else if (product.CustomConfigurationAlias.Equals(MicrosConfigurationAlias.ComplexContainer, StringComparison.InvariantCultureIgnoreCase))
                {
                    // TODO
                    this.WriteToPosConnectorLog("LoadProductsFromMenu", "Currently Complex Container is not supported: " + product.Title);
                    importReport.AppendFormatLine("Product: '{0}' not imported due to unsupported CustomConfigurationAlias '{1}'", product.Title, product.CustomConfigurationAlias);
                    continue; // Continue so it won't be synchronized.
                }
                else
                {
                    this.WriteToPosConnectorLog("LoadProductsFromMenu", "Non implemented CustomConfigurationAlias: " + product.CustomConfigurationAlias);
                    importReport.AppendFormatLine("Product: '{0}' not imported due to unsupported CustomConfigurationAlias '{1}'", product.Title, product.CustomConfigurationAlias);
                    continue; // Continue so it won't be synchronized.
                }

                if (!product.CustomConfigurationAlias.IsNullOrWhiteSpace())
                {
                    posProduct.Description += "Complex: " + product.CustomConfigurationAlias;
                }

                posProducts.Add(posProduct);
            }

            // Run this method for any child categories
            if (category.Categories != null)
            {
                foreach (CategoryDTO cat in category.Categories)
                {
                    this.LoadProductsFromMenu(importReport, cat, posProducts);
                }
            }
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public override Model.Poscategory[] GetPoscategories()
        {
            this.WriteToLogExtended("GetPoscategories", "Begin");

            this.syncReport = new StringBuilder();            
            this.syncReport.AppendFormatLine("******** SYNC CATEGORIES START ********");
            this.syncReport.AppendFormatLine("Sync started at: {0}", DateTime.Now.ToString());

            // Log the settings where you sync with:
            List<Poscategory> posCategories = new List<Poscategory>();

            try
            {                
                // Recuresively get all Cateogires
                this.WriteToPosConnectorLog("GetPoscategories", "Extract Categories from Menu");
                this.LoadCategoriesFromMenu(this.menu, posCategories);

                // Finish.
                this.syncReport.AppendFormatLine("Sync finished with success at: {0} with {1} categories", DateTime.Now.ToString(), posCategories.Count);
                this.syncReport.AppendFormatLine("******** SYNC CATEGORIES FINISH ********");
                this.WriteToPosConnectorLog("GetPoscategories", this.syncReport.ToString());
            }
            catch (Exception ex)
            {
                this.WriteToLogExtended("GetPoscategories", "End - Failure");
                this.WriteToLogExtended("GetPoscategories", ex.Message);
                this.WriteToLogExtended("GetPoscategories", ex.StackTrace);
                this.syncReport.AppendFormatLine("Sync finished with failure at: {0}", DateTime.Now.ToString());
                this.syncReport.AppendFormatLine("******** SYNC CATEGORIES FINISH ********");
                this.WriteToPosConnectorLog("GetPoscategories", this.syncReport.ToString());

                throw;
            }

            this.WriteToLogExtended("GetPoscategories", "End - Success");
            return posCategories.ToArray();
        }

        private void LoadCategoriesFromMenu(CategoryDTO category, List<Poscategory> posCategories)
        {
            Poscategory posCategory = new Poscategory();
            posCategory.ExternalId = category.CategoryId.ToString();
            posCategory.Name = category.Title.IsNullOrWhiteSpace() ? "NAME EMPTY - " + posCategory.ExternalId : category.Title;
            posCategories.Add(posCategory);

            // Run this method for any child categories
            if (category.Categories != null)
            {
                foreach (CategoryDTO cat in category.Categories)
                {
                    this.LoadCategoriesFromMenu(cat, posCategories);
                }
            }
        }

        #endregion

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public override Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();

            // Retrieve the DeliverypointGroups from the POS
            Posdeliverypointgroup group = new Posdeliverypointgroup();
            group.Name = "Default";
            group.ExternalId = "Default";
            posdeliverypointgroups.Add(group);

            return posdeliverypointgroups.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public override Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            //// Retrieve the Posdeliverypoints from the POS
            //CeniumWebservice.DeliveryPoints deliverpoints = new CeniumWebservice.DeliveryPoints();
            //this.Webservice.GetDeliverypoints(ref deliverpoints);

            //this.WriteToPosConnectorLog("GetPosdeliverypoints returned: '{0}' items", deliverpoints.Deliverypoint.Length);

            //foreach (CeniumWebservice.Deliverypoint deliverypoint in deliverpoints.Deliverypoint)
            //{
            //    if (!deliverypoint.Number.ToString().IsNullOrWhiteSpace())
            //    {
            //        this.WriteToPosConnectorLog("Sync Deliverypoint: Name: '{0}', Number: '{1}'",
            //            deliverypoint.Name, deliverypoint.Number);

            //        Posdeliverypoint posdeliverypoint = new Posdeliverypoint();
            //        posdeliverypoint.ExternalId = deliverypoint.DeliverypointId.ToString();
            //        posdeliverypoint.Name = deliverypoint.Name;
            //        posdeliverypoint.Number = deliverypoint.Number.ToString();
            //        posdeliverypoint.ExternalPosdeliverypointgroupId = "Default";

            //        posdeliverypoints.Add(posdeliverypoint);
            //    }
            //}

            return posdeliverypoints.ToArray();
        }

        #region Order Management

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public override Model.Posorder GetPosorder(string deliverypointNumber)
        {
            // Implement when available
            return null;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public override OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            this.WriteToLogExtended("SaveOrder", "Start");

            OrderProcessingError processingError = OrderProcessingError.None;

            try
            {
                FulfillmentTimeType fulfillmentType = this.defaultFulfillmentTimeType;
                OrderClass orderClass = this.defaultOrderClass;
                PaymentMethodType paymentMethod = this.defaultPaymentmethod;

                // Begin Session
                this.EndSession();
                this.BeginSession();

                // Select the Store (which will also create the Session and load the Store Order Options)
                this.WriteToPosConnectorLog("SaveOrder", "Load Store");
                this.LoadStore();

                // Call the clear down session, just to ensure that we start a new order
                this.WriteToLogExtended("SaveOrder", "Clear Session");
                ResetSessionOrderDetailsRequest resetRequest = new ResetSessionOrderDetailsRequest();
                this.OrderService.ResetSessionOrderDetails(this.sessionDto, resetRequest);

                // Get Menu and Start Order to be able to set the Options
                this.WriteToLogExtended("SaveOrder", "Load Menu");
                this.LoadMenu(this.defaultOrderClass, this.defaultFulfillmentTimeType);

                // Get Basket
                this.WriteToLogExtended("SaveOrder", "Get Basket");
                BasketDTO basket = this.GetBasket();

                // Add Products
                this.WriteToLogExtended("SaveOrder", "Append products");
                this.AddProductsToBasket(posorder);

                // Save Order
                this.PlaceOrder(posorder, fulfillmentType, paymentMethod, this.defaultLastname, this.defaultPhonenumber, this.defaultEmail);                    

                // Confirm Request
                this.ConfirmRequest(posorder);
            }
            catch (POSException pex)
            {
                this.WriteToLogNormal("SaveOrder", "Failed to Save Order: {0}", pex.BaseMessage);                
                throw;
            }
            catch (Exception ex)
            {
                var pex = new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, ex, "Exception while trying to save: '{0}'", ex.Message);
                this.WriteToLogNormal("SaveOrder", "Failed to Save Order: {0}", pex.BaseMessage);                
                throw pex;
            }

            return processingError;
        }

        private BasketDTO GetBasket()
        {
            // No try/catch because that's arrange in the calling method LoadStore
            BasketDTO toReturn = null;

            if (this.sessionDto == null)
                throw new POSException(OrderProcessingError.MicrosMcpSessionWasntInitialized, "Session is required for GetBasket");

            // Get Basket
            GetBasketRequest request = new GetBasketRequest();
            GetBasketResponse response = this.OrderService.GetBasket(this.sessionDto, request);

            if (response.IsSuccess)
            {
                // Good!
                toReturn = response.Basket;
            }
            else
            {
                // Not so good
                this.WriteToPosConnectorLog("GetBasket", "GetBasket failed.");
                string problemLog = this.LogResponse("GetBasket failed", response);
                throw new POSException(OrderProcessingError.MicrosMcpCouldntGetStore, "Failed to GetBasket for Session: '{0}', StoreId: '{1}'. Problem log: {2}", this.sessionDto.SessionId, this.storeId, problemLog);
            }

            return toReturn;
        }

        private void AddProductsToBasket(Posorder order)
        {            
            foreach (Posorderitem orderitem in order.Posorderitems)
            {
                GetBasketAddProductRequest request = new GetBasketAddProductRequest();
                int productId;

                if (!int.TryParse(orderitem.PosproductExternalId, out productId))
                    throw new POSException(OrderProcessingError.IncompatiblePosProductExternalId, "Couldn't parse to int: '{0}'", orderitem.PosproductExternalId);

                request.ProductTypeId = productId;
                request.Quantity = orderitem.Quantity;
                request.ProductType = Adactus.RICO.UISupportServices.Contract.ServiceData.Enumerations.BasketProductAddType.ProductId;
                // Non required: OrderTime, ContainerStateDTO

                // Alterations
                if (orderitem.Posalterationitems != null && orderitem.Posalterationitems.Length > 0)
                {
                    // Product Level Container
                    var productContainer = new ContainerStateDTO();
                    productContainer.ProductId = productId;
                    productContainer.Quantity = orderitem.Quantity;
                    productContainer.ProductModifiers = new List<ContainerStateDTO>();

                    foreach(var posalterationitem in orderitem.Posalterationitems)
                    {
                        // Alteration Level Container
                        int alterationId;

                        if (!int.TryParse(posalterationitem.ExternalPosalterationId, out alterationId))
                            throw new POSException(OrderProcessingError.IncompatiblePosAlterationExternalId, "Couldn't parse to int: '{0}'", posalterationitem.ExternalPosalterationId);

                        var alterationContainer = new ContainerStateDTO();
                        alterationContainer.Quantity = 1; // Copied hard coded 1 fron Test Harness
                        alterationContainer.ProductId = alterationId;
                        alterationContainer.ProductKey = "{0},{1}".FormatSafe(productId.ToString(), alterationId);
                        alterationContainer.ProductModifiers = new List<ContainerStateDTO>();

                        // Alteration Option Level Container (Micros supports recursive, we only have one left atm)
                        int alterationOptionId;
                        if (!int.TryParse(posalterationitem.ExternalPosalterationoptionId, out alterationOptionId))
                            throw new POSException(OrderProcessingError.IncompatiblePosAlterationOptionExternalId, "Couldn't parse to int: '{0}'", posalterationitem.ExternalPosalterationoptionId);

                        var optionContainer = new ContainerStateDTO();
                        optionContainer.Quantity = 1; // Copied hard coded 1 fron Test Harness
                        optionContainer.ProductId = alterationOptionId;
                        optionContainer.ProductKey = "{0},{1}".FormatSafe(alterationId, alterationOptionId);
                        
                        // Add Option to AlterationContainer
                        alterationContainer.ProductModifiers.Add(optionContainer);

                        // Add Alteration to ProductContainer
                        productContainer.ProductModifiers.Add(alterationContainer);
                    }

                    request.ContainerStateDTO = productContainer;
                }

                GetBasketResponse response;
                try
                {
                    response = this.OrderService.GetBasketAddProduct(this.sessionDto, request);
                }
                catch (Exception ex)
                {
                    var posException = new POSException(OrderProcessingError.PosErrorProductAndOrAlterationConfigurationInvalid, "AddProductsToBasket for product '{0}' (PosproductExternalId '{1}') failed for Order '{2}': {3}", 
                        orderitem.Description, orderitem.PosproductExternalId, order.PosorderId, ex.Message);
                    throw posException;
                }

                if (response.IsSuccess)
                {
                    // Good, keep loopin'                    
                }
                else
                {
                    // Not so good
                    this.WriteToPosConnectorLog("AddProductsToBasket", "AddProductsToBasket failed for Product '{0}' with ExternalId '{1}'.", orderitem.Description, orderitem.PosproductExternalId);
                    string problemLog = this.LogResponse("AddProductsToBasket failed", response);
                    throw new POSException(OrderProcessingError.PosErrorProductAndOrAlterationConfigurationInvalid, "AddProductsToBasket failed for Product '{0}' with ExternalId '{1}'. Problem log: {2}", orderitem.Description, orderitem.PosproductExternalId, problemLog);
                }
            }
        }

        private bool PlaceOrder(Posorder posorder, FulfillmentTimeType fulfillmentType, PaymentMethodType paymentMethod, string lastName, string phoneNumber, string email)        
        {
            bool toReturn = false;

            PlaceOrderRequest request = new PlaceOrderRequest();
            request.DeliveryAddress = null; // For now only RoomService / TableService
            request.PlaceOrderAttributes = new List<KeyValuePairDTO>();
            
            if (fulfillmentType == FulfillmentTimeType.ASAP) // Copied from Test Harness source
                request.PlaceOrderAttributes.Add(new KeyValuePairDTO(-1, "ASAP", "true"));

            // Handle Payment (for now Cash & Bill to Room only)
            if (paymentMethod != PaymentMethodType.Cash && paymentMethod != PaymentMethodType.BillToRoom)
                throw new POSException(OrderProcessingError.PosErrorPaymentmethodNotImplemented, "Paymentmethod '{0}' is not implemented.", this.defaultPaymentmethod);
                        
            request.OrderPayments = new PaymentDTO[1];
            request.OrderPayments[0] = new PaymentDTO();
            request.OrderPayments[0].PaymentMethodType = paymentMethod;
            request.OrderPayments[0].Amount = posorder.Posorderitems.Sum(x => x.PriceIn);

            // Place Order - Set Lastname and Phonenumber                 
            request.LastName = lastName;
            request.ContactTelephonePrimary = phoneNumber;
            request.ContactEmailPrimary = email;            
            
            //if (TestUtil.IsPcGabriel)
            //{ 
            //    // fail to set email on purpose
            //    request.LastName = null;
            //    request.ContactTelephonePrimary = null;
            //    request.ContactEmailPrimary = null;
            //}                            

            PlaceOrderResponse response = this.PaymentService.PlaceOrder(this.sessionDto, request);                        

            if (response.IsSuccess && response.OrderStatus == PlaceOrderStatus.Success)
            {
                // Good
                toReturn = true;
            }
            else
            {
                // Not so good                
                this.WriteToPosConnectorLog("PlaceOrder", "PlaceOrder failed for Order '{0}', Order Status: '{1}'", posorder.PosorderId, response.OrderStatus);
                
                // More information required?
                string problemLog = string.Empty;
                if (response.MoreInfoRequiredRequest != null)
                {
                    this.WriteToPosConnectorLog("PlaceOrder", "PlaceOrder failed - More information required: {0}\r\n", response.MoreInfoRequiredRequest.ProviderControlAlias);
                    problemLog += "PlaceOrder failed - More information required: {0}".FormatSafe(response.MoreInfoRequiredRequest.ProviderControlAlias);
                    foreach (KeyValuePairDTO moreInfo in response.MoreInfoRequiredRequest.ProviderAttributes)
                    {
                        problemLog += "PlaceOrder failed - More information required: {0} - {1} - {2}\r\n".FormatSafe(moreInfo.KeyValueId, moreInfo.Key, moreInfo.Value);
                        this.WriteToPosConnectorLog("PlaceOrder", "PlaceOrder failed - More information required: {0} - {1} - {2}", moreInfo.KeyValueId, moreInfo.Key, moreInfo.Value);
                    }
                }

                problemLog += this.LogResponse("PlaceOrder failed", response);
                var posException = new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, "PlaceOrder failed for Order '{0}', Order Status: '{1}'", posorder.PosorderId, response.OrderStatus);
                posException.AdditionalMessage = problemLog;
                throw posException;
            }

            return toReturn;
        }

        private void ConfirmRequest(Posorder order)
        {
            // No try/catch because that's arrange in the calling method LoadStore            
            if (this.sessionDto == null)
                throw new POSException(OrderProcessingError.MicrosMcpSessionWasntInitialized, "Session is required for ConfirmRequest");

            // Get Basket
            var request = new GetOrderConfirmationRequest();
            GetOrderConfirmationResponse response = this.OrderService.GetOrderConfirmation(this.sessionDto, request);

            if (response.IsSuccess)
            {
                // Good!                
                if (response.Order != null)
                    this.WriteToPosConnectorLog("ConfirmRequest", "Order with PosOrderId '{0}' confirmed with Micros Id '{1}' and reference '{2}', status: '{3}'",
                        order.PosorderId, response.Order.OrderId, response.Order.OrderReference, response.OrderStatusMessage);
            }
            else
            {
                // Not so good
                this.WriteToPosConnectorLog("ConfirmRequest", "ConfirmRequest failed.");                
                string responseLog = this.LogResponse("ConfirmRequest", response);
                POSException posException = new POSException(OrderProcessingError.MicrosMcpCouldntGetStore, "Failed to ConfirmRequest for Session: '{0}', StoreId: '{1}'", this.sessionDto.SessionId, this.storeId);
                posException.AdditionalMessage = responseLog;
                throw posException;
            }            
        }

        private void ClearOrder()
        {
            // No try/catch because that's arrange in the calling method LoadStore

            // Get Store
            ResetSessionOrderDetailsRequest resetRequest = new ResetSessionOrderDetailsRequest();
            ResetSessionOrderDetailsResponse resetResponse = this.OrderService.ResetSessionOrderDetails(this.sessionDto, resetRequest);

            if (resetResponse.IsSuccess)
            {
                // Good!                
            }
            else
            {
                // Not so good
                this.WriteToPosConnectorLog("ClearOrder", "Clear Order failed.");
                this.LogResponse("Clear Order failed", resetResponse);
                throw new POSException(OrderProcessingError.MicrosMcpCouldntClearOrder, "Failed to Clear Order for Session: '{0}'", this.sessionDto.SessionId);
            }
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public override bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            // Implement when available
            this.WriteToLogNormal("PrintConfirmationReceipt", "PrintConfirmationReceipt not supported (yet) by MicrosMcp");
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public override bool PrintServiceRequestReceipt(string serviceDescription)
        {
            // Implement when available
            this.WriteToLogNormal("PrintServiceRequestReceipt", "PrintServiceRequestReceipt not supported (yet) by MicrosMcp");
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public override bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            // Implement when available
            this.WriteToLogNormal("PrintCheckoutRequestReceipt", "PrintCheckoutRequestReceipt not supported (yet) by MicrosMcp");
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public override bool NotifyLockedDeliverypoint()
        {
            // Implement when available
            this.WriteToLogNormal("NotifyLockedDeliverypoint", "NotifyLockedDeliverypoint not supported (yet) by MicrosMcp");
            return true;
        }

        #endregion

        #region Utility methods

        private string LogResponse(string logPrefix, ServiceResponseBase response)
        {
            StringBuilder logText = new StringBuilder();

            this.WriteToPosConnectorLog(logPrefix, "IsSuccess: '{0}'", response.IsSuccess);
            this.WriteToPosConnectorLog(logPrefix, "ErrorType: '{0}'", response.ErrorType);
            this.WriteToPosConnectorLog(logPrefix, "ErrorMessage: '{0}'", response.ErrorMessage);

            logText.AppendFormatLine("IsSuccess: '{0}'\n", response.IsSuccess);
            logText.AppendFormatLine("ErrorType: '{0}'\n", response.ErrorType);
            logText.AppendFormatLine("ErrorMessage: '{0}'\n", response.ErrorMessage);

            if (response.ValidationErrors != null)
            {
                this.WriteToPosConnectorLog(logPrefix, "ValidationErrors Count: {0}", response.ValidationErrors.Length);
                logText.AppendFormatLine("ValidationErrors Count: {0}\n", response.ValidationErrors.Length);

                foreach (var error in response.ValidationErrors)
                {                                        
                    if (!error.ErrorCode.IsNullOrWhiteSpace())
                    {                        
                        this.WriteToPosConnectorLog(logPrefix, "Validation Error: '{0}' ('{1}')", error.ErrorDescription, error.ErrorCode);
                        logText.AppendFormatLine("Validation Error: '{0}' ('{1}')\n", error.ErrorDescription, error.ErrorCode);
                    }
                    else
                    {
                        this.WriteToPosConnectorLog(logPrefix, "Validation Error: '{0}'", error.ErrorDescription);
                        logText.AppendFormatLine("Validation Error: '{0}'\n", error.ErrorDescription);                        
                    }
                }
            }

            return logText.ToString();
        }

        #endregion

        #region Properties

        private StoreServiceClient storeService = null;
        private StoreServiceClient StoreService
        {
            get
            {
                if (this.storeService == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = int.MaxValue;
                    EndpointAddress endpoint = new EndpointAddress(this.webserviceUrl + "StoreService.svc/soap");
                    this.storeService = new StoreServiceClient(binding, endpoint);
                }

                return this.storeService;
            }
        }

        private OrderServiceClient orderService = null;
        private OrderServiceClient OrderService
        {
            get
            {
                if (this.orderService == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = int.MaxValue;
                    EndpointAddress endpoint = new EndpointAddress(this.webserviceUrl + "OrderService.svc/soap");
                    this.orderService = new OrderServiceClient(binding, endpoint);
                }

                return this.orderService;
            }
        }

        private PaymentServiceClient paymentService = null;
        private PaymentServiceClient PaymentService
        {
            get
            {
                if (this.paymentService == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = int.MaxValue;
                    EndpointAddress endpoint = new EndpointAddress(this.webserviceUrl + "PaymentService.svc/soap");
                    this.paymentService = new PaymentServiceClient(binding, endpoint);
                }

                return this.paymentService;
            }
        }

        private SecurityServiceClient securityService = null;
        private SecurityServiceClient SecurityService
        {
            get
            {
                if (this.securityService == null)
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = int.MaxValue;
                    EndpointAddress endpoint = new EndpointAddress(this.webserviceUrl + "SecurityService.svc/soap");
                    this.securityService = new SecurityServiceClient(binding, endpoint);
                    //if(TestUtil.IsPcDeveloper)
                    //    this.securityService.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.None;                    
                }

                return this.securityService;
            }
        }

        private bool hasError = false;
        /// <summary>
        /// Gets or sets the hasError
        /// </summary>
        public bool HasError
        {
            get
            {
                return this.hasError;
            }
            set
            {
                this.hasError = value;
            }
        }

        private string errorMessage = string.Empty;
        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public override string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
