﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys
{
    /// <summary>
    /// AgilysysHelper class
    /// </summary>
    public class AgilysysHelper : PosHelper
    {

        public static string GetReturnCodeString(int returnCode)
        {
            switch (returnCode)
            {
                case 30002:
                    return "Service not supported in OFFLINE mode";
                case 30003:
                    return "Invalid Employee ID";
                case 30004:
                    return "Config Data is not available";
                case 30005:
                    return "TLOG Build problem";
                case 30006:
                    return "Save/Tender Order LSR problem";
                case 30007:
                    return "Invalid Item ID (item not exist)";
                case 30008:
                    return "Open Priced Item problem";
                case 30009:
                    return "Weighted Item problem";
                case 30010:
                    return "Item Order internal processing problem";
                case 30011:
                    return "Kitchen Printing problem";
                case 30012:
                    return "Invalid Tender Payment ID";
                case 30013:
                    return "Internal Tender Processing problem";
                case 30014:
                    return "Insufficient Payment Amount";
                case 30016:
                    return "Get Clocked-in Employee List - LSR failure";
                case 30017:
                    return "Client Authentication - Internal problem";
                case 30018:
                    return "Client Authentication - Invalid Auth Code  ";
                case 30020:
                    return "User Auth - Internal problem";
                case 30021:
                    return "User Auth - Invalid Employee ID";
                case 30022:
                    return "User Auth - Invalid Swipe Card ID";
                case 30023:
                    return "User Auth - Invalid Password";
                case 30024:
                    return "User Auth - Employee NOT clocked-in";
                case 30025:
                    return "User Auth - Invalid Jobcode ID (not in terminal group)  ";
                case 30030:
                    return "Invalid Team ID";
                case 30031:
                    return "Server not on team  ";
                case 30051:
                    return "Credit Card - Service not available";
                case 30053:
                    return "Credit Card - Invalid Tender ID";
                case 30054:
                    return "Credit Card - Invalid Account# (LunMod10)";
                case 30056:
                    return "Credit Card - Invalid Expire Date";
                case 30057:
                    return "Credit Card - Card is Expired";
                case 30061:
                    return "Credit Card - DENIED (Auth is rejected)";
                case 30062:
                    return "Credit Card - REFERRAL (Call for Voice Auth)";
                case 30063:
                    return "Credit Card - OFFLINE (Service not available)";
                case 30064:
                    return "Credit Card - DEFAULT (Invalid auth status)";
                case 30066:
                    return "Credit Card - TIMEOUT (Try again?)";
                case 30067:
                    return "Credit Card - Internal UDP Socket Error";
                case 30068:
                    return "Credit Card - Invalid MSR swipe data (Track 1)";
                case 30069:
                    return "Credit Card - Invalid MSR swipe data (Track 2)";
                case 30070:
                    return "Credit Card - Token not allowed  ";
                case 30071:
                    return "Room Charge - Invalid Tender ID  ";
                case 30091:
                    return "Process Order - Over-payment not allowed for the tender type  ";
                case 30101:
                    return "Invalid Order ID - Order does not exist";
                case 30102:
                    return "Invalid Profit Center ID - does not exist";
                case 30103:
                    return "Invalid Employee ID - does not exist";
                case 30104:
                    return "Invalid Check Type ID - does not exist";
                case 30115:
                    return "Get List of Open Orders - Service failure  ";
                case 30121:
                    return "Recall Order - Order is assigned to different Profit Center";
                case 30122:
                    return "Recall Order - Not your order (assigned to different Server ID)  ";
                case 30130:
                    return "Recall Order - Recall order LSR error";
                case 30131:
                    return "Recall Order - Open order does not exist";
                case 30132:
                    return "Recall Order - Open order is locked by another terminal  ";
                case 30201:
                    return "Process Order - Negative Tip Amount  ";
                case 30305:
                    return "Ordered Item is out of stock  ";
                case 30400:
                    return "PMS Interface is not running  ";
                case 30421:
                    return "Room Charge - Not available in offline mode";
                case 30422:
                    return "Room Charge - LSR failure";
                case 30423:
                    return "Room Charge - Invalid room number";
                case 30424:
                    return "Room Charge - Service Timeout";
                case 30425:
                    return "Room Charge - Invalid tender ID";
                case 30426:
                    return "Room Charge - Invalid tender class";
                case 30427:
                    return "Room Charge - Denied (CASH ONLY)";
                case 30432:
                    return "Room Charge - DENIED (Auth is rejected)";
                case 30433:
                    return "Room Charge - OFFLINE (Service not available)";
                case 30434:
                    return "Room Charge - DEFAULT (Invalid auth status)";
                case 30436:
                    return "Room Charge - TIMEOUT (Try again?)";
                case 30437:
                    return "Room Charge - Internal UDP Socket Error  ";
                case 30441:
                    return "Folio Charge - Not available in offline mode";
                case 30442:
                    return "Folio Charge - LSR failure";
                case 30443:
                    return "Folio Charge - Invalid room number";
                case 30444:
                    return "Folio Charge - Service Timeout";
                case 30445:
                    return "Folio Charge - Invalid tender ID";
                case 30446:
                    return "Folio Charge - Invalid tender class";
                case 30447:
                    return "Folio Charge - Denied (CASH ONLY)";
                case 30448:
                    return "Folio Charge - Invalid payment type for this Account  ";
                case 30501:
                    return "UDP socket CREATE failure";
                case 30502:
                    return "UDP socket BIND failure";
                case 30503:
                    return "UDP socket SEND failure";
                case 30504:
                    return "UDP socket READ failure  ";
                case 30601:
                    return "TPG Internal Failure - Auth Data memory alloc";
                case 30602:
                    return "TPG Internal Failure - Auth Data invalid type";
                case 30603:
                    return "Payment Authorization - Invalid auth reference key";
                case 30604:
                    return "TPG Internal Failure - Auth Data bad parameter  ";
                case 30701:
                    return "TPG_ERR_GA_SERVICE_NOT_AVAILABLE";
                case 30702:
                    return "TPG_ERR_GA_CHARGE_NOT_WHEN_OFFLINE";
                case 30711:
                    return "TPG_ERR_GA_CHARGE_LSR_INVALID_PARMS";
                case 30712:
                    return "TPG_ERR_GA_CHARGE_LSR_TIMEOUT";
                case 30713:
                    return "TPG_ERR_GA_CHARGE_LSR_FAILURE";
                case 30714:
                    return "TPG_ERR_GA_CHARGE_DENIED";
                case 30715:
                    return "TPG_ERR_GA_CHARGE_INVALID_TENDER_ID";
                case 30716:
                    return "TPG_ERR_GA_CHARGE_INVALID_GA_TENDER_CLASS";
                case 30717:
                    return "TPG_ERR_GA_CHARGE_ACCOUNT_NOT_FOUND";
                case 30718:
                    return "TPG_ERR_GA_CHARGE_ACCOUNT_NOT_ACTIVE";
                case 30719:
                    return "TPG_ERR_GA_CHARGE_ACCOUNT_INVALID_TYPE";
                case 30720:
                    return "TPG_ERR_GA_CHARGE_ACCOUNT_CASH_ONLY";
                case 30721:
                    return "TPG_ERR_GA_CHARGE_CREDIT_NOT_ALLOWED";
                case 30722:
                    return "TPG_ERR_GA_CHARGE_ACCOUNT_BALANCE_EXCEEDED";
                case 30723:
                    return "TPG_ERR_GA_CHARGE_ACCOUNT_IS_EMPTY";
                case 30724:
                    return "TPG_ERR_GA_CHARGE_INVALID_GA_AMOUNT";
                case 30725:
                    return "TPG_ERR_GA_CHARGE_INVALID_PAYMENT_AMOUNT";
                case 30726:
                    return "TPG_ERR_GA_CHARGE_MISSING_INVALID_DATA";
                case 30727:
                    return "TPG_ERR_GA_CHARGE_MISSING_TENDER_DATA";
                case 30728:
                    return "TPG_ERR_GA_CHARGE_AUTH_TENDER_TYPE";
                case 30729:
                    return "TPG_ERR_GA_CHARGE_TOO_MANY_AUTHS_ON_CHK  ";
                case 30801:
                    return "TPG_ERR_TENDER_AUTH_ON_REFUND_CHECK";
                case 30802:
                    return "TPG_ERR_TENDER_AUTH_NOT_FOUND";
                case 30803:
                    return "TPG_ERR_TOO_MANY_TENDER_AUTHS";
                case 30804:
                    return "TPG_ERR_TENDER_AUTH_FULLY_AUTHD";
                case 30805:
                    return "TPG_ERR_NEGATIVE_TENDER_AUTH_AMT";
                case 30806:
                    return "TPG_ERR_TENDER_AUTH_AMT_EXCEEDS_BALANCE";
                case 30807:
                    return "TPG_ERR_TENDER_AUTH_AMT_BELOW_BALANCE";
                case 30808:
                    return "TPG_ERR_HOST_CAPTURE_TENDER_AUTH";
                case 30809:
                    return "TPG_ERR_CASH_ONLY_TENDER_AUTH";
                case 30810:
                    return "TPG_ERR_AUTH_TYPE_NOT_SUPPORTED";
                case 30811:
                    return "TPG_ERR_TENDER_AUTH_INVALID_XREF_KEY";
                case 30812:
                    return "TPG_ERR_TENDER_RESTRICTED  ";
                case 30901:
                    return "TPG_ERR_INQ_INVALID_TYPE";
                case 30902:
                    return "TPG_ERR_INQ_MISSING_INVALID_DATA";
                case 30903:
                    return "TPG_ERR_INQ_ZERO_RESPONSES";
                case 30904:
                    return "TPG_ERR_INQ_TOO_MANY_RESPONSES";
                case 30905:
                    return "TPG_ERR_INQ_INVALID_TENDER_TYPE";
                case 30912:
                    return "TPG_ERR_INQ_INVALID_TENDER_PAYMENT_ID  ";
                case 31011:
                    return "TPG_ERR_INVALID_ORDER_ATTRIBUTE";
                case 31100:
                    return "TPG_ERR_IA_SERVICE_INVALID_PARMS";
                case 31101:
                    return "TPG_ERR_IA_SERVICE_OFFLINE";
                case 31102:
                    return "TPG_ERR_IA_SERVICE_TIMEOUT";
                case 31103:
                    return "TPG_ERR_IA_SERVICE_FAILURE";
                case 31104:
                    return "TPG_ERR_IA_INVALID_IA_TENDER_ID";
                case 31105:
                    return "TPG_ERR_IA_ACCOUNT_NOT_FOUND";
                case 31106:
                    return "TPG_ERR_IA_ACCOUNT_NOT_ACTIVE";
                case 31107:
                    return "TPG_ERR_IA_ACCOUNT_IS_EMPTY";
                case 31108:
                    return "TPG_ERR_IA_ACCOUNT_REQ_LOWER_AMT";
                case 31109:
                    return "TPG_ERR_IA_ACCOUNT_CASH_ONLY";
                case 31110:
                    return "TPG_ERR_IA_ACCOUNT_INVALID_TYPE";
                case 31111:
                    return "TPG_ERR_IA_ACCOUNT_BALANCE_EXCEEDED";
                case 31112:
                    return "TPG_ERR_IA_CREDIT_NOT_ALLOWED";
                case 31113:
                    return "TPG_ERR_IA_MISSING_INVALID_DATA";
                case 31114:
                    return "TPG_ERR_IA_AUTH_ACCOUNT_TYPE";
                case 31115:
                    return "TPG_ERR_IA_INQ_ACCOUNT_TYPE";
                case 31116:
                    return "TPG_ERR_IA_TOO_MANY_AUTHS_ON_CHK";
                case 31117:
                    return "TPG_ERR_IA_DENIED";
                case 31118:
                    return "TPG_ERR_IA_DENIED_USE_LIMIT_AMT";
                case 31119:
                    return "TPG_ERR_IA_INVALID";
                case 31201:
                    return "Gift Card Invalid";  
                case 32001:
                    return "Check Full";
                case 32002:
                    return "Item Exceeds Max Check Total";
                case 32003:
                    return"Incomplete Config   90002 TPG Buffer Error - see Dr. Watson 90009 Order Total - Internal problem";
                default:
                    return "Unknown";
            }
        }
    }
}
