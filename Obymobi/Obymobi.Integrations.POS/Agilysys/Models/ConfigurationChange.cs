﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "config-change-time-request")]
    public class ConfigChangeTimeRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "config-change-time-request-Body")]
    public class ConfigChangeTimeRequestBody
    {
        [XmlElement(ElementName = "config-change-time-request")]
        public ConfigChangeTimeRequest Configchangetimerequest { get; set; }
    }

    [XmlRoot(ElementName = "config-change-time-response")]
    public class ConfigChangeTimeResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "config-change-time")]
        public string ConfigChangeTime { get; set; }
    }

    [XmlRoot(ElementName = "config-change-time-response-Body")]
    public class ConfigChangeTimeResponseBody
    {
        [XmlElement(ElementName = "config-change-time-response")]
        public ConfigChangeTimeResponse ConfigChangeTimeResponse { get; set; }
    }
}
