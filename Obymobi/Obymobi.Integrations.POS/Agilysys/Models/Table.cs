﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "table-data-request")]
    public class TableDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "table-data-request-Body")]
    public class TableDataRequestBody
    {
        [XmlElement(ElementName = "table-data-request")]
        public TableDataRequest TableDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "table")]
    public class Table
    {
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
    }

    [XmlRoot(ElementName = "table-data-response")]
    public class TableDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "table")]
        public List<Table> Tables { get; set; }
    }

    [XmlRoot(ElementName = "table-data-response-Body")]
    public class TableDataResponseBody
    {
        [XmlElement(ElementName = "table-data-response")]
        public TableDataResponse TableDataResponse { get; set; }
    }
}
