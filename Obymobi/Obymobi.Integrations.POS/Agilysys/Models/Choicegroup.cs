﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "choicegroup-data-request")]
    public class ChoicegroupDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "choicegroup-data-request-Body")]
    public class ChoicegroupDataRequestBody
    {
        [XmlElement(ElementName = "choicegroup-data-request")]
        public ChoicegroupDataRequest ChoicegroupDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "modifier")]
    public class ChoicegroupModifier
    {
        [XmlAttribute(AttributeName = "id")]
        public string ModifierId { get; set; }
    }

    [XmlRoot(ElementName = "choicegroup-data")]
    public class Choicegroup
    {
        [XmlElement(ElementName = "minimum-choices")]
        public string MinimumChoices { get; set; }
        [XmlElement(ElementName = "maximum-choices")]
        public string MaximumChoices { get; set; }
        [XmlElement(ElementName = "modifier")]
        public List<ChoicegroupModifier> Modifiers { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "button-text")]
        public string Buttontext { get; set; }
    }

    [XmlRoot(ElementName = "choicegroup-data-response")]
    public class ChoicegroupDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "choicegroup-data")]
        public List<Choicegroup> Choicegroups { get; set; }
    }

    [XmlRoot(ElementName = "choicegroup-data-response-Body")]
    public class ChoicegroupDataResponseBody
    {
        [XmlElement(ElementName = "choicegroup-data-response")]
        public ChoicegroupDataResponse ChoicegroupDataResponse { get; set; }
    }
}
