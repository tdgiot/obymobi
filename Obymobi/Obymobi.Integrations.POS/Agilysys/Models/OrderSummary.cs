﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "order-summary-header")]
    public class OrderSummaryHeader
    {
        [XmlElement(ElementName = "profitcenter-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
    }

    [XmlRoot(ElementName = "order-summary-request")]
    public class OrderSummaryRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "order-summary-header")]
        public OrderSummaryHeader OrderSummaryHeader { get; set; }
    }

    [XmlRoot(ElementName = "order-summary-request-Body")]
    public class OrderSummaryRequestBody
    {
        [XmlElement(ElementName = "order-summary-request")]
        public OrderSummaryRequest OrderSummaryRequest { get; set; }
    }

    [XmlRoot(ElementName = "order-summary-response-data")]
    public class OrderSummaryResponseData
    {
        [XmlElement(ElementName = "order-amount")]
        public string OrderAmount { get; set; }
        [XmlElement(ElementName = "early-payment-amount")]
        public string EarlyPaymentAmount { get; set; }
        [XmlElement(ElementName = "early-payment-allowed")]
        public string EarlyPaymentAllowed { get; set; }
        [XmlElement(ElementName = "order-completion-allowed")]
        public string OrderCompletionAllowed { get; set; }
    }

    [XmlRoot(ElementName = "payment-record")]
    public class PaymentRecord
    {
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "authorized-amount")]
        public string AuthorizedAmount { get; set; }
        [XmlElement(ElementName = "tender-description")]
        public string TenderDescription { get; set; }
        [XmlElement(ElementName = "authorization-name")]
        public string AuthorizationName { get; set; }
        [XmlElement(ElementName = "user-account")]
        public string UserAccount { get; set; }
        [XmlElement(ElementName = "modification-allowed")]
        public string ModificationAllowed { get; set; }
    }

    [XmlRoot(ElementName = "payment-summary-data-list")]
    public class PaymentSummaryDataList
    {
        [XmlElement(ElementName = "payment-record")]
        public List<PaymentRecord> PaymentRecords { get; set; }
    }

    [XmlRoot(ElementName = "order-summary-response")]
    public class OrderSummaryResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "order-summary-response-data")]
        public OrderSummaryResponseData OrderSummaryResponseData { get; set; }
        [XmlElement(ElementName = "payment-summary-data-list")]
        public PaymentSummaryDataList PaymentSummaryDataList { get; set; }
    }

    [XmlRoot(ElementName = "order-summary-response-Body")]
    public class OrderSummaryResponseBody
    {
        [XmlElement(ElementName = "order-summary-response")]
        public OrderSummaryResponse OrderSummaryResponse { get; set; }
    }
}
