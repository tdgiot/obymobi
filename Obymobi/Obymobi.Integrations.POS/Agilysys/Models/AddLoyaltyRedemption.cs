﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "add-loyalty-redemption-request-data")]
    public class AddLoyaltyRedemptionRequestData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "loyalty-id")]
        public string LoyaltyId { get; set; }
        [XmlElement(ElementName = "promotion-name")]
        public string PromotionName { get; set; }
        [XmlElement(ElementName = "payment-amount")]
        public string PaymentAmount { get; set; }
    }

    [XmlRoot(ElementName = "add-loyalty-redemption-request")]
    public class AddLoyaltyRedemptionRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "add-loyalty-redemption-request-data")]
        public AddLoyaltyRedemptionRequestData AddLoyaltyRedemptionRequestData { get; set; }
    }

    [XmlRoot(ElementName = "add-loyalty-redemption-request-Body")]
    public class AddLoyaltyRedemptionRequestBody
    {
        [XmlElement(ElementName = "add-loyalty-redemption-request")]
        public AddLoyaltyRedemptionRequest AddLoyaltyRedemptionRequest { get; set; }
    }

    [XmlRoot(ElementName = "loyalty-redemption-response-data")]
    public class AddLoyaltyRedemptionResponseData
    {
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "order-balance")]
        public string OrderBalance { get; set; }
    }

    [XmlRoot(ElementName = "add-loyalty-redemption-response")]
    public class AddLoyaltyRedemptionResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "loyalty-redemption-response-data")]
        public AddLoyaltyRedemptionResponseData AddLoyaltyRedemptionResponseData { get; set; }
    }

    [XmlRoot(ElementName = "add-loyalty-redemption-response-Body")]
    public class AddLoyaltyRedemptionResponseBody
    {
        [XmlElement(ElementName = "add-loyalty-redemption-response")]
        public AddLoyaltyRedemptionResponse AddLoyaltyRedemptionResponse { get; set; }
    }
}
