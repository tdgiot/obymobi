﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "clock-status-request")]
    public class ClockStatusRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "clock-status-request-Body")]
    public class ClockStatusRequestBody
    {
        [XmlElement(ElementName = "clock-status-request")]
        public ClockStatusRequest ClockStatusRequest { get; set; }
    }

    [XmlRoot(ElementName = "clock-status-data")]
    public class ClockStatus
    {
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "employee-name")]
        public string EmployeeName { get; set; }
        [XmlElement(ElementName = "jobcode-id")]
        public string JobcodeId { get; set; }
    }

    [XmlRoot(ElementName = "clock-status-response")]
    public class ClockStatusResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "clock-status-data")]
        public List<ClockStatus> ClockStatuses { get; set; }
    }

    [XmlRoot(ElementName = "clock-status-response-Body")]
    public class ClockStatusResponseBody
    {
        [XmlElement(ElementName = "clock-status-response")]
        public ClockStatusResponse ClockStatusResponse { get; set; }
    }
}
