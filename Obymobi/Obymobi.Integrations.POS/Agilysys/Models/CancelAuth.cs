﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "cancel-auth-request-data")]
    public class CancelAuthRequestData
    {
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
    }

    [XmlRoot(ElementName = "cancel-auth-request")]
    public class CancelAuthRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "cancel-auth-request-data")]
        public CancelAuthRequestData CancelAuthRequestData { get; set; }
    }

    [XmlRoot(ElementName = "cancel-auth-request-Body")]
    public class CancelauthrequestBody
    {
        [XmlElement(ElementName = "cancel-auth-request")]
        public CancelAuthRequest CancelAuthRequest { get; set; }
    }

    [XmlRoot(ElementName = "cancel-auth-response")]
    public class CancelAuthResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
    }

    [XmlRoot(ElementName = "cancel-auth-response-Body")]
    public class CancelAuthResponseBody
    {
        [XmlElement(ElementName = "cancel-auth-response")]
        public CancelAuthResponse CancelAuthResponse { get; set; }
    }
}
