﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "item-data-request")]
    public class ItemDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "item-data-request-Body")]
    public class ItemDataRequestBody
    {
        [XmlElement(ElementName = "item-data-request")]
        public ItemDataRequest ItemDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "choicegroup-selection-data")]
    public class ChoicegroupSelectionData
    {
        [XmlElement(ElementName = "choicegroup-id")]
        public List<string> ChoicegroupIds { get; set; }
    }

    [XmlRoot(ElementName = "item-data")]
    public class Item
    {
        [XmlElement(ElementName = "item-id")]
        public string ItemId { get; set; }
        [XmlElement(ElementName = "item-description")]
        public string ItemDescription { get; set; }
        [XmlElement(ElementName = "item-button-text")]
        public string ItemButtontext { get; set; }
        [XmlElement(ElementName = "item-base-price")]
        public string ItemBaseprice { get; set; }
        [XmlElement(ElementName = "item-product-class")]
        public string ItemProductClass { get; set; }
        [XmlElement(ElementName = "choicegroup-selection-data")]
        public ChoicegroupSelectionData ChoicegroupSelectionData { get; set; }
    }

    [XmlRoot(ElementName = "item-data-response")]
    public class ItemDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "item-data")]
        public List<Item> Items { get; set; }
    }

    [XmlRoot(ElementName = "item-data-response-Body")]
    public class ItemDataResponseBody
    {
        [XmlElement(ElementName = "item-data-response")]
        public ItemDataResponse ItemDataResponse { get; set; }
    }
}
