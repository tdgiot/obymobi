﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "calculate-order-amount-request")]
    public class CalculateOrderAmountRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "order-body")]
        public Order Order { get; set; }
    }

    [XmlRoot(ElementName = "calculate-order-amount-request-Body")]
    public class CalculateOrderAmountRequestBody
    {
        [XmlElement(ElementName = "calculate-order-amount-request")]
        public CalculateOrderAmountRequest CalculateOrderAmountRequest { get; set; }
    }

    [XmlRoot(ElementName = "detail-amounts")]
    public class DetailAmounts
    {
        [XmlElement(ElementName = "sales-net")]
        public string SalesNet { get; set; }
        [XmlElement(ElementName = "sales-gross")]
        public string SalesGross { get; set; }
        [XmlElement(ElementName = "discount")]
        public string Discount { get; set; }
        [XmlElement(ElementName = "tax")]
        public string Tax { get; set; }
        [XmlElement(ElementName = "gratuity")]
        public string Gratuity { get; set; }
        [XmlElement(ElementName = "service")]
        public string Service { get; set; }
        [XmlElement(ElementName = "tip")]
        public string Tip { get; set; }
        [XmlElement(ElementName = "tender")]
        public string Tender { get; set; }
        [XmlElement(ElementName = "balance")]
        public string Balance { get; set; }
        [XmlElement(ElementName = "total")]
        public string Total { get; set; }
    }

    [XmlRoot(ElementName = "calculate-order-amount-response")]
    public class CalculateOrderAmountResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "order-amount")]
        public string OrderAmount { get; set; }
        [XmlElement(ElementName = "detail-amounts")]
        public DetailAmounts DetailAmounts { get; set; }
    }

    [XmlRoot(ElementName = "calculate-order-amount-response-Body")]
    public class CalculateorderamountresponseBody
    {
        [XmlElement(ElementName = "calculate-order-amount-response")]
        public CalculateOrderAmountResponse CalculateOrderAmountResponse { get; set; }
    }

}
