﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "finalize-order-data")]
    public class FinalizeOrderData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "receipt-required")]
        public string ReceiptRequired { get; set; }
    }

    [XmlRoot(ElementName = "finalize-order-request")]
    public class FinalizeOrderRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "finalize-order-data")]
        public FinalizeOrderData FinalizeOrderData { get; set; }
    }

    [XmlRoot(ElementName = "finalize-order-request-Body")]
    public class FinalizeOrderRequestBody
    {
        [XmlElement(ElementName = "finalize-order-request")]
        public FinalizeOrderRequest FinalizeOrderRequest { get; set; }
    }

    [XmlRoot(ElementName = "finalize-order-response-data")]
    public class FinalizeOrderResponseData
    {
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
    }

    [XmlRoot(ElementName = "finalize-order-response")]
    public class FinalizeOrderResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "finalize-order-response-data")]
        public FinalizeOrderResponseData FinalizeOrderResponseData { get; set; }
    }

    [XmlRoot(ElementName = "finalize-order-response-Body")]
    public class FinalizeOrderResponseBody
    {
        [XmlElement(ElementName = "finalize-order-response")]
        public FinalizeOrderResponse FinalizeOrderResponse { get; set; }
    }

}
