﻿using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Integrations.POSLogic.POS.Agilysys
{
    /// <summary>
    /// AgilysysConfigurationAdapter class
    /// </summary>
    public class AgilysysConfigurationAdapter : IConfigurationAdapter
    {

        #region Methods
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.ClientId = terminal.PosValue2;
            this.AuthenticationCode = terminal.PosValue3;
            this.EmployeeId = terminal.PosValue4;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.ClientId;
            terminal.PosValue3 = this.AuthenticationCode;
            terminal.PosValue4 = this.EmployeeId;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.WebserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysWebserviceUrl);
            this.ClientId = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysClientId);
            this.AuthenticationCode = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysAuthenticationCode);
            this.EmployeeId = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysEmployeeId);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysWebserviceUrl, this.WebserviceUrl);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysClientId, this.ClientId);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysAuthenticationCode, this.AuthenticationCode);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysEmployeeId, this.EmployeeId);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.ClientId;
            terminal.PosValue3 = this.AuthenticationCode;
            terminal.PosValue4 = this.EmployeeId;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.ClientId = terminal.PosValue2;
            this.AuthenticationCode = terminal.PosValue3;
            this.EmployeeId = terminal.PosValue4;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets webservice url
        /// </summary>
        /// <value>
        /// The webservice url
        /// </value>
        public string WebserviceUrl { get; set; }
        /// <summary>
        /// Gets or sets the client id
        /// </summary>
        /// <value>
        /// The client id
        /// </value>
        public string ClientId { get; set; }
        /// <summary>
        /// Gets or sets the authentication code
        /// </summary>
        /// <value>
        /// The authentication code
        /// </value>
        public string AuthenticationCode { get; set; }
        /// <summary>
        /// Gets or sets the employee id
        /// </summary>
        /// <value>
        /// The employee id
        /// </value>
        public string EmployeeId { get; set; }

        #endregion
    }
}
