﻿using System.Collections.Generic;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// PosIntegrationInformationHelper class
    /// </summary>
    public class PosIntegrationInformationHelper
    {
        /// <summary>
        /// PosIntegrationInformationHelperResult enums
        /// </summary>
        public enum PosIntegrationInformationHelperResult
        {
            /// <summary>
            /// Terminal can't be found
            /// </summary>
            TerminalNotFound = 200,
            /// <summary>
            /// The company doesn't exists
            /// </summary>
            CompanyDoesNotExist = 201
        }

        PosIntegrationInformation posIntegrationInformation = null;
        TerminalEntity terminal = new TerminalEntity();
        private bool useCache = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="PosIntegrationInformationHelper"/> class.
        /// </summary>
        /// <param name="terminalId">The terminal id.</param>
        /// <param name="useCache">if set to <c>true</c> [use cache].</param>
        public PosIntegrationInformationHelper(int terminalId, bool useCache)
        {
            this.terminal = new TerminalEntity(terminalId);

            if (this.terminal.IsNew)
                throw new ObymobiException(PosIntegrationInformationHelperResult.TerminalNotFound, "TerminalId: {0}", terminalId);

            this.useCache = useCache;
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <returns></returns>
        public PosIntegrationInformation LoadData()
        {
            // Get the company
            CompanyEntity company = new CompanyEntity(this.terminal.CompanyId);
            if (company.IsNew)
                throw new ObymobiException(PosIntegrationInformationHelperResult.CompanyDoesNotExist, "Company with id '{0}' does not exist!", this.terminal.CompanyId);

            CacheHelper.CacheParameters cacheParams = new CacheHelper.CacheParameters("PosIntegrationInformationHelper.LoadData", company.PosIntegrationInformationLastModifiedAsTimeStamp, Global.ApplicationInfo.ApplicationVersion);
            cacheParams.Add(this.terminal.CompanyId);

            PosIntegrationInformation posInfoFromCache = null;

            if (this.useCache && !CacheHelper.TryGetValue(cacheParams, out posInfoFromCache))
            {
                posInfoFromCache = null;
            }

            if (posInfoFromCache != null)
                this.posIntegrationInformation = posInfoFromCache;
            else
            {
                // Initialize the PosIntegrationInformation instance
                this.posIntegrationInformation = new PosIntegrationInformation();

                // Get the categories
                this.posIntegrationInformation.Categories = CategoryHelper.GetCategories(this.terminal.CompanyId, null, true, true, null);

                // Get the products
                Product[] products = ProductHelper.GetProducts(this.terminal.CompanyId, null, false, true, null);
                Product[] posproducts = ProductHelper.GetPosProducts(this.terminal.CompanyId, true);
                Product[] serviceProducts = ProductHelper.GetServiceProducts(this.terminal.CompanyId, null, true);
                Product[] paymentmethodProducts = ProductHelper.GetPaymentmethodProducts(this.terminal.CompanyId, true);
                Product[] deliverypointProducts = ProductHelper.GetDeliverypointProducts(this.terminal.CompanyId, true);
                Product[] tabletProducts = ProductHelper.GetTabletProducts(this.terminal.CompanyId, true);

                List<int> productIds = new List<int>();

                List<Product> allProducts = new List<Product>();
                foreach (Product product in products)
                {
                    if (!productIds.Contains(product.ProductId))
                        allProducts.Add(product);
                    productIds.Add(product.ProductId);
                }

                foreach (Product posproduct in posproducts)
                {
                    if (!productIds.Contains(posproduct.ProductId))
                        allProducts.Add(posproduct);
                    productIds.Add(posproduct.ProductId);
                }

                foreach (Product serviceproduct in serviceProducts)
                {
                    if (!productIds.Contains(serviceproduct.ProductId))
                        allProducts.Add(serviceproduct);
                    productIds.Add(serviceproduct.ProductId);
                }

                foreach (Product paymentproduct in paymentmethodProducts)
                {
                    if (!productIds.Contains(paymentproduct.ProductId))
                        allProducts.Add(paymentproduct);
                    productIds.Add(paymentproduct.ProductId);
                }

                foreach (Product deliverypointProduct in deliverypointProducts)
                {
                    if (!productIds.Contains(deliverypointProduct.ProductId))
                        allProducts.Add(deliverypointProduct);
                    productIds.Add(deliverypointProduct.ProductId);
                }

                foreach (Product tabletProduct in tabletProducts)
                {
                    if (!productIds.Contains(tabletProduct.ProductId))
                        allProducts.Add(tabletProduct);
                    productIds.Add(tabletProduct.ProductId);
                }

                this.posIntegrationInformation.Products = allProducts.ToArray();

                // Get the delivery points
                this.posIntegrationInformation.Deliverypoints = DeliverypointHelper.GetDeliverypoints(this.terminal.CompanyId, this.terminal.TerminalId, true, 0);

                // Get the terminal
                this.posIntegrationInformation.Terminal = TerminalHelper.CreateTerminalModelFromEntity(this.terminal);

                this.LoadSeparateSettings();

                if (this.terminal.POSConnectorTypeEnum != POSConnectorType.Unknown)
                {
                    var posHelper = PosHelperWrapper.GetPosHelper(this.terminal.POSConnectorTypeEnum);

                    this.posIntegrationInformation.POSConnectorType = this.terminal.POSConnectorType;

                    posHelper.AddPosSpecificPosIntegrationInformation(this.terminal, this.posIntegrationInformation);
                }

                // Write the output to the cache
                if (useCache && this.posIntegrationInformation != null)
                    CacheHelper.CacheObject(cacheParams, this.posIntegrationInformation);
            }

            return this.posIntegrationInformation;
        }

        private void LoadSeparateSettings()
        {
            // System messages deliverypoint
            if (this.terminal.SystemMessagesDeliverypointId.HasValue && this.terminal.SystemMessagesDeliverypointEntity.PosdeliverypointId.HasValue)
                this.posIntegrationInformation.SystemMessagesDeliverypointId = this.terminal.SystemMessagesDeliverypointEntity.PosdeliverypointEntity.ExternalId;

            // Alternative system messages deliverypoint
            if (this.terminal.AltSystemMessagesDeliverypointId.HasValue && this.terminal.AltSystemMessagesDeliverypointEntity.PosdeliverypointId.HasValue)
                this.posIntegrationInformation.AltSystemMessagesDeliverypointId = this.terminal.AltSystemMessagesDeliverypointEntity.PosdeliverypointEntity.ExternalId;

            // Battery Low Product
            if (this.terminal.BatteryLowProductId.HasValue && this.terminal.BatteryLowProductEntity.PosproductId.HasValue)
                this.posIntegrationInformation.BatteryLowProductId = this.terminal.BatteryLowProductEntity.PosproductEntity.ExternalId;

            // Unlock table product
            if (this.terminal.UnlockDeliverypointProductId.HasValue && this.terminal.UnlockDeliverypointProductEntity.PosproductId.HasValue)
                this.posIntegrationInformation.UnlockDeliverypointProductId = this.terminal.UnlockDeliverypointProductEntity.PosproductEntity.ExternalId;

            // Client disconnected product
            if (this.terminal.ClientDisconnectedProductId.HasValue && this.terminal.ClientDisconnectedProductEntity.PosproductId.HasValue)
                this.posIntegrationInformation.ClientDisconnectedProductId = this.terminal.ClientDisconnectedProductEntity.PosproductEntity.ExternalId;
        }
    }
}
