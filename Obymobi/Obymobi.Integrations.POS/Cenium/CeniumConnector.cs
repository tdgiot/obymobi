﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Logic.POS.Generic;
using Obymobi.Logic.Model;
using System.Net;
using Obymobi.Enums;

namespace Obymobi.Logic.POS.Cenium
{
    /// <summary>
    /// CeniumConnector class
    /// </summary>
    public class CeniumConnector : PosConnectorBase, IPOSConnector
    {
        #region Fields

        // Retrieve the settings from the config file
        private string webserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumApiUrl);
        private string webserviceUsername = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumApiUsername);
        private string webservicePassword = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumApiPassword);
        private string webserviceDomain = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumApiDomain);
        private bool useDefaultCredentials = Dionysos.ConfigurationManager.GetBool(POSConfigurationConstants.CeniumUseDefaultCredentials);

        // Handy to keep a sync report
        private StringBuilder syncReport = new StringBuilder();

        // Variable to keep track of Category <> Products links during sync
        private Dictionary<string, List<string>> categoryProductLinks;

        #endregion

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="CeniumConnector"/> class.
        /// </summary>
        public CeniumConnector()
        {
            this.WriteToLogExtended("Constructor", "Start");

            bool configurationComplete = false;

            // Verify if all settings are set
            if (this.webserviceUrl.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "Webservice url is empty");
            else if (this.webserviceUsername.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "Webservice username is empty");
            else if (this.webservicePassword.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "Webservice password is empty");
            else
                configurationComplete = true;

            // Output the current config to the log:
            this.WriteToLogNormal("Constructor", "Webservice Url: {0}", this.webserviceUrl);
            this.WriteToLogNormal("Constructor", "Webservice Username: {0}", this.webserviceUsername);
            this.WriteToLogNormal("Constructor", "Webservice Password: {0}", this.webservicePassword);
            this.WriteToLogNormal("Constructor", "Webservice Domain: {0}", this.webserviceDomain);

            //this.ShowCompanies();

            if (!configurationComplete)
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, "Configuration Incomplete");

            this.WriteToLogExtended("Constructor", "End");
        }

        /// <summary>
        /// Shows the companies.
        /// </summary>
        private void ShowCompanies()
        {
            var companies = this.SystemWebservice.Companies();
            foreach (var company in companies)
            {
                this.WriteToLogExtended("ShowCompanies", "Company: {0}", company);
            }
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public override Model.Posproduct[] GetPosproducts()
        {
            this.WriteToLogExtended("GetPosproducts", "Start");

            this.syncReport = new StringBuilder();
            this.syncReport.AppendFormatLine("\r\n\r\n");
            this.syncReport.AppendFormatLine("******** SYNC START ********");
            this.syncReport.AppendFormatLine("Sync started at: {0}", DateTime.Now.ToString());

            // Log the settings where you sync with:
            List<Posproduct> posProducts = new List<Posproduct>();

            try
            {
                // Do the logic, log extensively.
                CeniumWebservice.Products products = new CeniumWebservice.Products();
                this.Webservice.GetProducts(ref products);

                this.WriteToPosConnectorLog("GetPosproducts", "GetProducts returned: '{0}' items", products.Product.Length);

                foreach (var product in products.Product)
                {
                    this.WriteToPosConnectorLog("GetPosproducts", "Sync Product: Name: '{0}', Id: '{1}'",
                        product.Name, product.ProductId);

                    // Get the product fields
                    Posproduct posProduct = new Posproduct();
                    posProduct.ExternalId = product.ProductId.ToString();
                    posProduct.Name = product.Name;
                    posProduct.Description = product.Description;
                    posProduct.PriceIn = product.Price;
                    posProduct.VatTariff = 2;

                    // Try to get a sort order
                    int sortOrder;
                    if (int.TryParse(product.SortOrder, out sortOrder))
                        posProduct.SortOrder = sortOrder;

                    // Check if it is part of a category (we can only add to one)                    

                    if (this.categoryProductLinks.Any(cp => cp.Value.Contains(posProduct.ExternalId)))
                    {
                        var categoryProductLink = this.categoryProductLinks.FirstOrDefault(cp => cp.Value.Contains(posProduct.ExternalId));
                        posProduct.ExternalPoscategoryId = categoryProductLink.Key;
                    }

                    // Parse all Alterations
                    List<Posalteration> posAlterations = new List<Posalteration>();
                    if (product.Alterations != null)
                    {
                        foreach (var alterationWrapper in product.Alterations)
                        {
                            // Get the Alteration Fields
                            Posalteration posAlteration = new Posalteration();
                            CeniumWebservice.Alteration alteration = alterationWrapper.Alteration[0];
                            posAlteration.ExternalId = alteration.AlterationId;
                            posAlteration.Name = alteration.Name;
                            posAlteration.MinOptions = alteration.Required ? 1 : 0;
                            posAlteration.MaxOptions = alteration.Multiselect ? alteration.AlterationOption.Length : 1;

                            // Get the Alteration Options
                            List<Posalterationoption> posalterationOptions = new List<Posalterationoption>();
                            if (alteration.AlterationOption != null)
                            {
                                foreach (var alterationOption in alteration.AlterationOption)
                                {
                                    // Get the Atleration Option fields
                                    Posalterationoption posOption = new Posalterationoption();
                                    posOption.ExternalId = alterationOption.AlterationOptionId;
                                    posOption.Name = alterationOption.Name;
                                    if (alterationOption.Description.Length > 0)
                                    {
                                        foreach (var desc in alterationOption.Description)
                                        {
                                            posOption.Description = StringUtil.CombineWithSpace(posOption.Description, desc);
                                        }
                                    }

                                    posOption.PriceIn = alterationOption.PriceAddition;

                                    // Add to options list
                                    posalterationOptions.Add(posOption);
                                }
                            }

                            // Add the options to the alteration
                            posAlteration.Posalterationoptions = posalterationOptions.ToArray();

                            // Add the alteration to the list 
                            posAlterations.Add(posAlteration);
                        }
                    }

                    // Add the alterations to the product
                    posProduct.Posalterations = posAlterations.ToArray();

                    // Add the product to the list
                    posProducts.Add(posProduct);
                }

                // Finish.
                this.syncReport.AppendFormatLine("Sync finished with success at: {0}", DateTime.Now.ToString());
                this.syncReport.AppendFormatLine("******** SYNC FINISH ********");
                this.WriteToPosConnectorLog("GetPosproducts", this.syncReport.ToString());

                this.WriteToLogExtended("GetPosproducts", "End - Success");

                return posProducts.ToArray();
            }
            catch (Exception ex)
            {
                this.WriteToLogExtended("GetPosproducts", "End - Failure");
                this.WriteToLogExtended("GetPosproducts", ex.Message);
                this.WriteToLogExtended("GetPosproducts", ex.StackTrace);
                this.syncReport.AppendFormatLine("Sync finished with failure at: {0}", DateTime.Now.ToString());
                this.syncReport.AppendFormatLine("******** SYNC FINISH ********");
                this.WriteToPosConnectorLog("GetPosproducts", this.syncReport.ToString());

                throw;
            }

        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public override Model.Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();
            this.categoryProductLinks = new Dictionary<string, List<string>>();

            var orderitems = new CeniumWebservice.GetOrderItems();

            // Retrieve the Categories from the POS
            CeniumWebservice.Categories categories = new CeniumWebservice.Categories();
            this.Webservice.GetCategories(ref categories);

            this.WriteToPosConnectorLog("GetPoscategories", "GetPoscategories returned: '{0}' items", categories.Category.Length);

            foreach (var category in categories.Category)
            {
                if (category.CategoryId.IsNullOrWhiteSpace() || category.CategoryId == "0")
                {
                    this.WriteToPosConnectorLog("GetPoscategorties", "Category Skipped to sync: Name: '{0}', Id: '{1}'",
                        category.Name, category.CategoryId);
                    continue;
                }
                else
                {
                    this.WriteToPosConnectorLog("GetPoscategorties", "Sync Category: Name: '{0}', Id: '{1}'",
                        category.Name, category.CategoryId);
                }

                Poscategory poscategory = new Poscategory();
                poscategory.ExternalId = category.CategoryId;
                poscategory.Name = category.Name;

                // Ensure the catogry is added once and only once.
                if (!this.categoryProductLinks.ContainsKey(poscategory.ExternalId))
                    this.categoryProductLinks.Add(poscategory.ExternalId, new List<string>());

                foreach (var catproduct in category.CatProducts)
                {
                    if (!this.categoryProductLinks[poscategory.ExternalId].Contains(catproduct.ProductId))
                        this.categoryProductLinks[poscategory.ExternalId].Add(catproduct.ProductId);
                }

                poscategories.Add(poscategory);
            }

            return poscategories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public override Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();

            // Retrieve the DeliverypointGroups from the POS
            Posdeliverypointgroup group = new Posdeliverypointgroup();
            group.Name = "Default";
            group.ExternalId = "Default";
            posdeliverypointgroups.Add(group);

            return posdeliverypointgroups.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public override Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            // Retrieve the Posdeliverypoints from the POS
            CeniumWebservice.DeliveryPoints deliverpoints = new CeniumWebservice.DeliveryPoints();
            this.Webservice.GetDeliverypoints(ref deliverpoints);

            this.WriteToPosConnectorLog("GetPosdeliverypoints", "GetPosdeliverypoints returned: '{0}' items", deliverpoints.Deliverypoint.Length);

            foreach (CeniumWebservice.Deliverypoint deliverypoint in deliverpoints.Deliverypoint)
            {
                if (!deliverypoint.Number.ToString().IsNullOrWhiteSpace())
                {
                    this.WriteToPosConnectorLog("GetPosdeliverypoints", "Sync Deliverypoint: Name: '{0}', Number: '{1}'",
                        deliverypoint.Name, deliverypoint.Number);

                    Posdeliverypoint posdeliverypoint = new Posdeliverypoint();
                    posdeliverypoint.ExternalId = deliverypoint.DeliverypointId.ToString();
                    posdeliverypoint.Name = deliverypoint.Name;
                    posdeliverypoint.Number = deliverypoint.Number.ToString();
                    posdeliverypoint.ExternalPosdeliverypointgroupId = "Default";

                    posdeliverypoints.Add(posdeliverypoint);
                }
            }

            return posdeliverypoints.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public override Model.Posorder GetPosorder(string deliverypointNumber)
        {
            // Implement when available
            return null;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public override OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            this.WriteToLogExtended("SaveOrder", "Start");

            OrderProcessingError processingError = OrderProcessingError.None;

            CeniumWebservice.MakeOrder order = new CeniumWebservice.MakeOrder();

            int deliverypointInt;
            if (!int.TryParse(posorder.PosdeliverypointExternalId, out deliverypointInt))
                throw new POSException(OrderProcessingError.PosErrorNonParseableOrConvertableDataType, "posorder.PosdeliverypointExternalId can't be converted to an int");

            order.DeliveryPoint = deliverypointInt;
            order.OrderId = posorder.PosorderId;
            List<CeniumWebservice.MakeOrderItems> orderitems = new List<CeniumWebservice.MakeOrderItems>();

            foreach (var posorderitem in posorder.Posorderitems)
            {
                var orderitem = new CeniumWebservice.MakeOrderItems();
                orderitem.OrderItemId = posorderitem.PosorderitemId;

                int productId;
                if (!int.TryParse(posorderitem.PosproductExternalId, out productId))
                {
                    throw new POSException(OrderProcessingError.PosErrorNonParseableOrConvertableDataType, "PosproductExternalId must be convertable to int, isn't valid for Product: '{0}' with id '{1}'",
                        posorderitem.Description, posorderitem.PosproductExternalId);
                }

                orderitem.ProductId = productId;
                orderitem.ProductName = posorderitem.Description;
                orderitem.Quantity = posorderitem.Quantity;

                List<CeniumWebservice.AlterationItems> alterationItems = new List<CeniumWebservice.AlterationItems>();
                if (posorderitem.Posalterationitems != null)
                {
                    foreach (var posalterationitem in posorderitem.Posalterationitems)
                    {
                        var alterationItem = new CeniumWebservice.AlterationItems();

                        int alterationItemId;
                        if (!int.TryParse(posalterationitem.ExternalPosalterationId, out alterationItemId))
                        {
                            throw new POSException(OrderProcessingError.PosErrorNonParseableOrConvertableDataType,
                                "ExternalId of Alteration must be convertable to int, isn't valid for Product: '{0}', ExternalPosAlterationId: '{1}'",
                                posorderitem.Description, posalterationitem.ExternalPosalterationId);
                        }

                        int alterationOptionId;
                        if (!int.TryParse(posalterationitem.ExternalPosalterationoptionId, out alterationOptionId))
                        {
                            throw new POSException(OrderProcessingError.PosErrorNonParseableOrConvertableDataType,
                                "ExternalId of Alterationoption must be convertable to int, isn't valid for Product: '{0}' with ExternalPosalterationoptionId: '{1}'",
                                posorderitem.Description, posalterationitem.ExternalPosalterationoptionId);
                        }

                        alterationItem.AlterationItemId = alterationItemId;
                        alterationItem.AlterationOptionId = alterationOptionId;
                        alterationItems.Add(alterationItem);
                    }
                    orderitem.AlterationItems = alterationItems.ToArray();
                }

                //orderitem.AlterationItems
                orderitems.Add(orderitem);
            }

            order.MakeOrderItems = orderitems.ToArray();

            var orderWrapper = new CeniumWebservice.MakeOrders();
            orderWrapper.MakeOrder = new CeniumWebservice.MakeOrder[] { order };

            string errorText = string.Empty;
            try
            {
                if (this.Webservice.ProcessOrder(orderWrapper, ref errorText))
                {
                    // Nothing to do
                }
                else
                {
                    this.WriteToLogNormal("SaveOrder: ProcessOrder returned FALSE: {0}", errorText);
                    processingError = OrderProcessingError.PosErrorUnspecifiedFatalPosProblem;
                }
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, ex, "Exception while trying to save: '{0}' - {1}", errorText, ex.Message);
            }

            if (processingError != OrderProcessingError.None)
                POSException.ThrowTyped(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, null, "No further details...", null);

            this.WriteToLogExtended("SaveOrder", "End: '{0}'", processingError);

            return processingError;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public override bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public override bool PrintServiceRequestReceipt(string serviceDescription)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public override bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public override bool NotifyLockedDeliverypoint()
        {
            // Implement when available
            return true;
        }

        #endregion

        #region Properties

        private CeniumWebservice.Crave webservice;
        /// <summary>
        /// Gets the Cenium webservice
        /// </summary>
        public CeniumWebservice.Crave Webservice
        {
            get
            {
                if (this.webservice == null)
                {
                    this.webservice = new CeniumWebservice.Crave();
                    this.webservice.Url = this.webserviceUrl;

                    if (this.useDefaultCredentials || this.webserviceUsername == "UseDefaultCredentials")
                    {
                        this.WriteToPosConnectorLog("SystemWebservice.GET", "UseDefaultCredentials");
                        this.WriteToLogNormal("SystemWebservice.GET", "UseDefaultCredentials");
                        this.webservice.UseDefaultCredentials = true;
                    }
                    else
                    {
                        this.WriteToPosConnectorLog("SystemWebservice.GET", "NetworkCredentials");
                        this.WriteToLogNormal("SystemWebservice.GET", "Use NetworkCredentials: {0}, {1}", this.webserviceUsername, this.webserviceDomain);
                        CredentialCache cc = new CredentialCache();
                        cc.Add(new Uri(this.Webservice.Url), "Negotiate", new NetworkCredential(this.webserviceUsername, this.webservicePassword, this.webserviceDomain));
                        this.webservice.Credentials = cc;
                    }

                    if (TestUtil.IsPcGabriel)
                    {
                        this.webservice.Timeout = 100000;
                    }
                }
                return this.webservice;
            }
        }

        private CeniumSystemWebservice.SystemService systemWebservice;
        /// <summary>
        /// Gets the Cenium webservice
        /// </summary>
        public CeniumSystemWebservice.SystemService SystemWebservice
        {
            get
            {
                if (this.systemWebservice == null)
                {
                    this.systemWebservice = new CeniumSystemWebservice.SystemService();
                    this.systemWebservice.Url = this.webserviceUrl;

                    if (this.useDefaultCredentials || this.webserviceUsername == "UseDefaultCredentials")
                    {
                        this.WriteToPosConnectorLog("SystemWebservice.GET", "UseDefaultCredentials");
                        this.WriteToLogNormal("SystemWebservice", "UseDefaultCredentials");
                        this.systemWebservice.UseDefaultCredentials = true;
                    }
                    else
                    {
                        this.WriteToPosConnectorLog("SystemWebservice.GET", "NetworkCredentials");
                        this.WriteToLogNormal("SystemWebservice", "Use NetworkCredentials: Username: '{0}', Domain: '{1}' (Password not shown)", this.webserviceUsername, this.webserviceDomain);
                        CredentialCache cc = new CredentialCache();
                        cc.Add(new Uri(this.Webservice.Url), "Negotiate", new NetworkCredential(this.webserviceUsername, this.webservicePassword, this.webserviceDomain));
                        this.systemWebservice.Credentials = cc;
                    }
                    if (TestUtil.IsPcGabriel)
                    {
                        this.systemWebservice.Timeout = 100000;
                    }
                }
                return this.systemWebservice;
            }
        }


        private bool hasError = false;
        /// <summary>
        /// Gets or sets the hasError
        /// </summary>
        public bool HasError
        {
            get
            {
                return this.hasError;
            }
            set
            {
                this.hasError = value;
            }
        }

        private string errorMessage = string.Empty;
        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public override string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
