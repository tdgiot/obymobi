﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Logic.POS.Cenium
{
    /// <summary>
    /// CeniumConfigurationAdapter class
    /// </summary>
    public class CeniumConfigurationAdapter : IConfigurationAdapter
    {
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.CeniumApiUrl = terminal.PosValue1;
            this.CeniumApiUsername = terminal.PosValue2;
            this.CeniumApiPassword = terminal.PosValue3;
            this.CeniumApiDomain = terminal.PosValue4;
            this.CeniumUseDefaultCredentials = terminal.PosValue5;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.CeniumApiUrl = terminal.PosValue1;
            this.CeniumApiUsername = terminal.PosValue2;
            this.CeniumApiPassword = terminal.PosValue3;
            this.CeniumApiDomain = terminal.PosValue4;
            this.CeniumUseDefaultCredentials = terminal.PosValue5;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.CeniumApiUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumApiUrl);
            this.CeniumApiUsername = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumApiUsername);
            this.CeniumApiPassword = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumApiPassword);
            this.CeniumApiDomain = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumApiDomain);
            this.CeniumUseDefaultCredentials = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.CeniumUseDefaultCredentials);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.CeniumApiUrl, this.CeniumApiUrl);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.CeniumApiUsername, this.CeniumApiUsername);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.CeniumApiPassword, this.CeniumApiPassword);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.CeniumApiDomain, this.CeniumApiDomain);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.CeniumUseDefaultCredentials, this.CeniumUseDefaultCredentials);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.CeniumApiUrl;
            terminal.PosValue2 = this.CeniumApiUsername;
            terminal.PosValue3 = this.CeniumApiPassword;
            terminal.PosValue4 = this.CeniumApiDomain;
            terminal.PosValue5 = this.CeniumUseDefaultCredentials;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.CeniumApiUrl;
            terminal.PosValue2 = this.CeniumApiUsername;
            terminal.PosValue3 = this.CeniumApiPassword;
            terminal.PosValue4 = this.CeniumApiDomain;
            terminal.PosValue5 = this.CeniumUseDefaultCredentials;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the cenium API URL.
        /// </summary>
        /// <value>
        /// The cenium API URL.
        /// </value>
        public string CeniumApiUrl { get; set; }
        /// <summary>
        /// Gets or sets the cenium API username.
        /// </summary>
        /// <value>
        /// The cenium API username.
        /// </value>
        public string CeniumApiUsername { get; set; }
        /// <summary>
        /// Gets or sets the cenium API password.
        /// </summary>
        /// <value>
        /// The cenium API password.
        /// </value>
        public string CeniumApiPassword { get; set; }
        /// <summary>
        /// Gets or sets the cenium API domain.
        /// </summary>
        /// <value>
        /// The cenium API domain.
        /// </value>
        public string CeniumApiDomain { get; set; }
        /// <summary>
        /// Gets or sets if we should use DefaultCredentials
        /// </summary>
        /// <value>
        /// The cenium API domain.
        /// </value>
        public string CeniumUseDefaultCredentials { get; set; }

        string ceniumUseDefaultCredentials;
        public bool TestMode
        {
            get
            {
                bool ceniumUseDefaultCredentialsBool = false;
                bool.TryParse(this.ceniumUseDefaultCredentials, out ceniumUseDefaultCredentialsBool);
                return ceniumUseDefaultCredentialsBool;
            }
            set
            {
                this.ceniumUseDefaultCredentials = value.ToString();
            }
        }

        #endregion
    }
}
