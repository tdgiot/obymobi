﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.235
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Obymobi.Logic.POS.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.57.108:3786/ECServer2CGI.exe/soap/IECServer2")]
        public string Obymobi_Logic_POS_TonitWebservice_IECServer2service {
            get {
                return ((string)(this["Obymobi_Logic_POS_TonitWebservice_IECServer2service"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.57.70:8083/wm/tpapi")]
        public string Obymobi_Logic_POS_UntillWebservice_TPApi {
            get {
                return ((string)(this["Obymobi_Logic_POS_UntillWebservice_TPApi"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://pda.relaxsoftware.nl:4080/wsa/wsa1")]
        public string Obymobi_Logic_POS_RelaxWebservice_wsaWellnessService {
            get {
                return ((string)(this["Obymobi_Logic_POS_RelaxWebservice_wsaWellnessService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.57.124/HCGrenosWS/HCGrenos.asmx")]
        public string Obymobi_Logic_POS_HoreCatWebservice_HCGrenosService {
            get {
                return ((string)(this["Obymobi_Logic_POS_HoreCatWebservice_HCGrenosService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:8083/soap/TIMsoapConnector")]
        public string Obymobi_Logic_POS_AlohaWebservice_TIMsoapConnectorservice {
            get {
                return ((string)(this["Obymobi_Logic_POS_AlohaWebservice_TIMsoapConnectorservice"]));
            }
        }
    }
}
