﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.POS.Generic;
using System.Net.Sockets;
using System.Net;
using Dionysos;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using System.Diagnostics;
using System.Globalization;

namespace Obymobi.Logic.POS.TorexAlpha
{
    /// <summary>
    /// TorexAlphaConnector class
    /// </summary>
    public class TorexAlphaConnector : PosConnectorBase
    {
        #region Socket logic

        private string ip = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.TorexAlphaIpAddress);
        private int port = 32101;
        private Socket socket = null;
        private IPAddress ipAddress = null;
        private IPEndPoint ipEndPoint = null;
        private string cashierName = string.Empty;
        private string cashierNumber = string.Empty;

        private int shortSleep = 100;
        //private int longSleep = 200;

        /// <summary>
        /// Connects the ICRTouch connector with the POS
        /// </summary>
        /// <returns>True if connecting with the POS was successful, False if not</returns>
        public string Connect()
        {
            string response = string.Empty;

            this.WriteToLogExtended("Connect", "Start");

            if (this.socket != null && this.socket.Connected)
            {
                this.WriteToLogExtended("Connect", "Already connected.");
                return "Already connected";
            }

            byte[] bytes = new byte[256];
            try
            {
                this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                this.socket.SendTimeout = 9000;
                this.socket.ReceiveTimeout = 16000;

                this.ipAddress = IPAddress.Parse(this.ip);
                this.ipEndPoint = new IPEndPoint(ipAddress, this.port);
                this.socket.Connect(ipEndPoint);
                if (!this.socket.Connected)
                    throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("The TorexAlphaConnector could not connect to the POS on '{0}:{1}'", this.ip, this.port));

                // GK From ICR, left here in case it was needed later
                //byte[] data = System.Text.Encoding.ASCII.GetBytes("<DEVICEINFO><PROTOCOLVERSION>1.0</PROTOCOLVERSION></DEVICEINFO>\r\n");
                //int result = this.socket.Send(data);
                //this.socket.Send(data);
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("An exception was thrown while connecting the TorexAlphaConnector. Exception: {0}", ex.Message));
            }

            this.WriteToLogExtended("Connect", "End");

            return response;
        }

        /// <summary>
        /// Converts to hex.
        /// </summary>
        /// <param name="asciiString">The ASCII string.</param>
        /// <returns></returns>
        public string ConvertToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        /// <summary>
        /// Does the command.
        /// </summary>
        /// <param name="commands">The command.</param>
        /// <param name="bufferSize">The buffer size.</param>
        /// <param name="sleep">The sleep.</param>
        /// <returns></returns>
        public string DoCommands(string[] commands, int bufferSize, int sleep)
        {
            string response = string.Empty;
            bool wasError = false;
            StringBuilder commandLog = new StringBuilder();

            commandLog.Append("\r\n------------------------ START COMMAND -------------------------\r\n");
            string commandsJoined = string.Join(", ", commands);
            commandLog.AppendFormat("Commands: {0}\r\n", commandsJoined);
            commandLog.AppendFormat("To server: {0}\r\n", this.ip);

            this.WriteToLogExtended("DoCommand - Start", commandsJoined); // Remove the last \n in the log
            try
            {
                this.Connect();

                // Execute the command string
                StringBuilder responsesOfAllCommands = new StringBuilder();
                try
                {
                    foreach (var command in commands)
                    {
                        this.WriteToLogExtended("DoCommand - Execute command:", command); // Remove the last \n in the log
                        byte[] sendData = System.Text.Encoding.ASCII.GetBytes(command);
                        int result = this.socket.Send(sendData);

                        //System.Threading.Thread.Sleep(750);

                        bool keepReceiving = true;
                        DateTime receivingStarted = DateTime.Now;
                        StringBuilder responseOfCurrentCommand;
                        bool receivedAck = false;
                        while (keepReceiving)
                        {
                            // Receive data from socket
                            // System.Threading.Thread.Sleep(this.shortSleep);
                            responseOfCurrentCommand = new StringBuilder();

                            byte[] receivedData = new byte[bufferSize];
                            int receivedSize = this.socket.Receive(receivedData, 0, bufferSize, 0);

                            // Resize array to length of content
                            Array.Resize(ref receivedData, receivedSize);

                            string encodedString = Encoding.UTF8.GetString(receivedData);

                            responsesOfAllCommands.Append(encodedString);
                            responseOfCurrentCommand.Append(encodedString);

                            //this.WriteToLogExtended("DoCommand", "Received from socket this iteration: '{0}'.", encodedString);
                            //this.WriteToLogExtended("DoCommand", "Received from socket totally: '{0}'", responsesOfAllCommands.ToString());

                            if (responseOfCurrentCommand.ToString().Equals("\x06"))
                            {
                                Debug.WriteLine("A - " + responseOfCurrentCommand.ToString() + " - " + this.ConvertToHex(responseOfCurrentCommand.ToString()));
                                receivedAck = true;
                                if (command.Contains("**"))
                                {
                                    // This was an order 'FINALIZE' command, we need to wait for the result of the order
                                    // They want u to respond with <ACK>
                                    //int i = 9;
                                }
                                else
                                    keepReceiving = false;
                            }
                            else if ((DateTime.Now - receivingStarted).TotalSeconds > 30)
                            {
                                Debug.WriteLine("B - " + responseOfCurrentCommand.ToString() + " - " + this.ConvertToHex(responseOfCurrentCommand.ToString()));
                                keepReceiving = false;
                                this.WriteToPosConnectorLog("DoCommands", "Receiving data from socket timed out (10s).");
                            }
                            else if (command.Contains("**") && receivedAck && responseOfCurrentCommand.ToString().Contains("\x03"))
                            {
                                Debug.WriteLine("C - " + responseOfCurrentCommand.ToString() + " - " + this.ConvertToHex(responseOfCurrentCommand.ToString()));
                                // GK I know this is crappy, but I didn't receive a termintor in the documentation 
                                // when I was writing this.
                                string confirmationCommand = "\x06";
                                this.WriteToLogExtended("DoCommand - Execute command:", confirmationCommand);
                                sendData = System.Text.Encoding.ASCII.GetBytes(confirmationCommand);
                                result = this.socket.Send(sendData);
                                response = responseOfCurrentCommand.ToString();
                                // Strip first and last character (they are just begin / terminators)
                                response = response.Substring(1, response.Length - 1);
                                keepReceiving = false;
                            }
                            else
                            {
                                Debug.WriteLine("D - " + responseOfCurrentCommand.ToString() + " - " + this.ConvertToHex(responseOfCurrentCommand.ToString()));
                            }

                            // GK There is no terminator line
                            //if (encodedString.Contains("\n"))
                            //{
                            //    keepReceiving = false;
                            //    //Debug.WriteLine("Terminator Found");
                            //}
                            //else
                            //Debug.WriteLine("Terminator Not Yet Found");
                        }

                        commandLog.AppendFormat("Result: {0}\r\n", response);
                    }
                }
                catch (Exception ex)
                {
                    wasError = true;
                    commandLog.AppendFormat("Exception with POS: {0}", ex.Message);
                    throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("An exception was thrown while trying the execute command '{0}'. Exception: {1}", commandsJoined, ex.Message));
                }
            }
            finally
            {
                if (this.socket != null && this.socket.Connected)
                {
                    this.socket.Disconnect(false);
                }
                this.socket = null;

                commandLog.Append("------------------------ END COMMAND -------------------------\r\n");
                this.WriteToLogExtended("DoCommand", commandLog.ToString());

                if (wasError)
                    this.WriteToPosConnectorLog("DoCommands", commandLog.ToString());
            }

            return response;
        }

        #endregion

        #region Save Order logic

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public override Enums.OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            OrderProcessingError processingError = OrderProcessingError.None;
            List<string> commands = new List<string>();

            decimal orderValue = 0;
            foreach (Posorderitem orderItem in posorder.Posorderitems)
            {
                //int quantity = orderItem.Quantity;

                //for (int i = 0; i < quantity; i++)
                //{
                // STX = \x02, ETC = \x03, ACK=\x06
                string orderItemCommand = string.Format("\x02\"SI\",{0},{1}\x03", orderItem.PosproductExternalId, orderItem.PriceIn.ToString("N2", CultureInfo.InvariantCulture));
                orderValue += orderItem.PriceIn;
                commands.Add(orderItemCommand);

                if (orderItem.Posalterationitems != null)
                {
                    foreach (var alterationItem in orderItem.Posalterationitems)
                    {
                        string sipCode = alterationItem.ExternalPosalterationoptionId.Replace("SIP-", string.Empty);
                        string alterationItemCommand = string.Format("\x02\"PR\",{0}\x03", sipCode);
                        commands.Add(alterationItemCommand);
                    }
                }
                //}
            }

            // Customer Name
            commands.Add(string.Format("\x02\"CN\",Crave\x03"));

            // Payment
            commands.Add(string.Format("\x02\"PY\",{0},1\x03", orderValue.ToString("N2", CultureInfo.InvariantCulture)));

            // OrderType
            commands.Add(string.Format("\x02\"OT\",TA\x03"));

            // Order DONE
            commands.Add(string.Format("\x02**\x03"));

            var result = this.DoCommands(commands.ToArray(), 32000, this.shortSleep);

            if (result.StartsWith("0"))
            {
                // GOOD                 
            }
            else
            {
                // BAD!                
                this.WriteToPosConnectorLog("Failed Order: {0}", result);
                throw new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, result);
            }

            this.WriteToLogExtended("SaveOrder", "End {0} - Status: {1}", TimeStamp.CreateTimeStamp(), processingError.ToString());

            return processingError;
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public override string ErrorMessage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Not implemented methods (It's a quick fix connector only)

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public override Model.Posproduct[] GetPosproducts()
        {
            // GK On purpose, we don't want a sync to happen.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public override Model.Poscategory[] GetPoscategories()
        {
            // GK On purpose, we don't want a sync to happen.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public override Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            // GK On purpose, we don't want a sync to happen.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public override Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            // GK On purpose, we don't want a sync to happen.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public override Model.Posorder GetPosorder(string deliverypointNumber)
        {
            // GK On purpose, we don't want a sync to happen.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public override bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            // GK Not used
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public override bool PrintServiceRequestReceipt(string serviceDescription)
        {
            // GK Not used
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public override bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            // GK Not used
            throw new NotImplementedException();
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public override bool NotifyLockedDeliverypoint()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
