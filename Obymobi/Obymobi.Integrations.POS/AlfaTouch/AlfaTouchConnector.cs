﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Enums;

namespace Obymobi.Logic.POS.AlfaTouch
{
    /// <summary>
    /// AlfaTouchConnector class
    /// </summary>
    public class AlfaTouchConnector : IPOSConnector
    {
        #region Fields

        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AlfaTouchConnector"/> class.
        /// </summary>
        public AlfaTouchConnector()
        {
        }

        #endregion

        #region Methods

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public Model.Posproduct[] GetPosproducts()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public Model.Poscategory[] GetPoscategories()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>An Obymobi.Logic.Model.Posorder instance</returns>
        public Model.Posorder GetPosorder(string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        /// <value></value>
        public string ErrorMessage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
