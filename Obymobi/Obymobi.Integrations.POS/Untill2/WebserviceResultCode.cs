﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// WebserviceResultCode enums
    /// </summary>
    public enum WebserviceResultCode : int
    {
        /// <summary>
        /// Ok
        /// </summary>
        Ok = 0,
        /// <summary>
        /// Internal server error occured
        /// </summary>
        InternalServerError = 1,
        /// <summary>
        /// Authentication error occured
        /// </summary>
        AuthenticationError = 2,
        /// <summary>
        /// Illegal request argument
        /// </summary>
        IllegalRequestArgument = 3,
        /// <summary>
        /// Database is unavailable
        /// </summary>
        DatabaseUnavaiable = 4,
        /// <summary>
        /// TPApieDisabled
        /// </summary>
        TPApiDisabled = 5,
        /// <summary>
        /// TPApiNotAllowed
        /// </summary>
        TPApiNotAllowed = 6,
        /// <summary>
        /// ConnectivityFailure
        /// </summary>
        ConnectivityFailure = 900,
        /// <summary>
        /// NoResult
        /// </summary>
        NoResult = 1000
    }
}
