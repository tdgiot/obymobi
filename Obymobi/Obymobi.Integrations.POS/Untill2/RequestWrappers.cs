﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Untill2Webservice;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// Based on http://stackoverflow.com/questions/7719236/cast-to-not-explicitly-implemented-interface
    /// </summary>
    public static class RequestWrappers
    {
        #region TGetSalesAreasInfoRequest

        private sealed class TGetSalesAreasInfoRequestWrapper : IRequest
        {
            private readonly TGetSalesAreasInfoRequest tGetSalesAreasInfoRequest;

            internal TGetSalesAreasInfoRequestWrapper(TGetSalesAreasInfoRequest instance)
            {
                this.tGetSalesAreasInfoRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.tGetSalesAreasInfoRequest.UserName;
                }
                set
                {
                    this.tGetSalesAreasInfoRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.tGetSalesAreasInfoRequest.Password;
                }
                set
                {
                    this.tGetSalesAreasInfoRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetSalesAreasInfoRequest instance)
        {
            return new TGetSalesAreasInfoRequestWrapper(instance);
        }

        #endregion

        #region TGetArticlesInfoRequest

        private sealed class TGetArticlesInfoRequestWrapper : IRequest
        {
            private readonly TGetArticlesInfoRequest tGetArticlesInfoRequest;

            internal TGetArticlesInfoRequestWrapper(TGetArticlesInfoRequest instance)
            {
                this.tGetArticlesInfoRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.tGetArticlesInfoRequest.UserName;
                }
                set
                {
                    this.tGetArticlesInfoRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.tGetArticlesInfoRequest.Password;
                }
                set
                {
                    this.tGetArticlesInfoRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetArticlesInfoRequest instance)
        {
            return new TGetArticlesInfoRequestWrapper(instance);
        }

        #endregion

        #region TGetOptionsInfoRequest

        private sealed class TGetOptionsInfoRequestWrapper : IRequest
        {
            private readonly TGetOptionsInfoRequest TGetOptionsInfoRequest;

            internal TGetOptionsInfoRequestWrapper(TGetOptionsInfoRequest instance)
            {
                this.TGetOptionsInfoRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TGetOptionsInfoRequest.UserName;
                }
                set
                {
                    this.TGetOptionsInfoRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TGetOptionsInfoRequest.Password;
                }
                set
                {
                    this.TGetOptionsInfoRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetOptionsInfoRequest instance)
        {
            return new TGetOptionsInfoRequestWrapper(instance);
        }

        #endregion


        #region TGetDepartmentsInfoRequest

        private sealed class TGetDepartmentsInfoRequestWrapper : IRequest
        {
            private readonly TGetDepartmentsInfoRequest TGetDepartmentsInfoRequest;

            internal TGetDepartmentsInfoRequestWrapper(TGetDepartmentsInfoRequest instance)
            {
                this.TGetDepartmentsInfoRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TGetDepartmentsInfoRequest.UserName;
                }
                set
                {
                    this.TGetDepartmentsInfoRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TGetDepartmentsInfoRequest.Password;
                }
                set
                {
                    this.TGetDepartmentsInfoRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetDepartmentsInfoRequest instance)
        {
            return new TGetDepartmentsInfoRequestWrapper(instance);
        }

        #endregion

        #region TGetGroupsInfoRequest

        private sealed class TGetGroupsInfoRequestWrapper : IRequest
        {
            private readonly TGetGroupsInfoRequest TGetGroupsInfoRequest;

            internal TGetGroupsInfoRequestWrapper(TGetGroupsInfoRequest instance)
            {
                this.TGetGroupsInfoRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TGetGroupsInfoRequest.UserName;
                }
                set
                {
                    this.TGetGroupsInfoRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TGetGroupsInfoRequest.Password;
                }
                set
                {
                    this.TGetGroupsInfoRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetGroupsInfoRequest instance)
        {
            return new TGetGroupsInfoRequestWrapper(instance);
        }

        #endregion


        #region TGetCategoriesInfoRequest

        private sealed class TGetCategoriesInfoRequestWrapper : IRequest
        {
            private readonly TGetCategoriesInfoRequest TGetCategoriesInfoRequest;

            internal TGetCategoriesInfoRequestWrapper(TGetCategoriesInfoRequest instance)
            {
                this.TGetCategoriesInfoRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TGetCategoriesInfoRequest.UserName;
                }
                set
                {
                    this.TGetCategoriesInfoRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TGetCategoriesInfoRequest.Password;
                }
                set
                {
                    this.TGetCategoriesInfoRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetCategoriesInfoRequest instance)
        {
            return new TGetCategoriesInfoRequestWrapper(instance);
        }

        #endregion


        #region TGetPricesInfoRequest

        private sealed class TGetPricesInfoRequestWrapper : IRequest
        {
            private readonly TGetPricesInfoRequest TGetPricesInfoRequest;

            internal TGetPricesInfoRequestWrapper(TGetPricesInfoRequest instance)
            {
                this.TGetPricesInfoRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TGetPricesInfoRequest.UserName;
                }
                set
                {
                    this.TGetPricesInfoRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TGetPricesInfoRequest.Password;
                }
                set
                {
                    this.TGetPricesInfoRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetPricesInfoRequest instance)
        {
            return new TGetPricesInfoRequestWrapper(instance);
        }

        #endregion


        #region TGetPaymentsInfoRequest

        private sealed class TGetPaymentsInfoRequestWrapper : IRequest
        {
            private readonly TGetPaymentsInfoRequest TGetPaymentsInfoRequest;

            internal TGetPaymentsInfoRequestWrapper(TGetPaymentsInfoRequest instance)
            {
                this.TGetPaymentsInfoRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TGetPaymentsInfoRequest.UserName;
                }
                set
                {
                    this.TGetPaymentsInfoRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TGetPaymentsInfoRequest.Password;
                }
                set
                {
                    this.TGetPaymentsInfoRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetPaymentsInfoRequest instance)
        {
            return new TGetPaymentsInfoRequestWrapper(instance);
        }

        #endregion


        #region TGetActiveOrdersRequest

        private sealed class TGetActiveOrdersRequestWrapper : IRequest
        {
            private readonly TGetActiveOrdersRequest TGetActiveOrdersRequest;

            internal TGetActiveOrdersRequestWrapper(TGetActiveOrdersRequest instance)
            {
                this.TGetActiveOrdersRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TGetActiveOrdersRequest.UserName;
                }
                set
                {
                    this.TGetActiveOrdersRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TGetActiveOrdersRequest.Password;
                }
                set
                {
                    this.TGetActiveOrdersRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IRequest
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TGetActiveOrdersRequest instance)
        {
            return new TGetActiveOrdersRequestWrapper(instance);
        }

        #endregion

        #region TCreateOrderRequest

        private sealed class TCreateOrderRequestWrapper : IRequest
        {
            private readonly TCreateOrderRequest TCreateOrderRequest;

            internal TCreateOrderRequestWrapper(TCreateOrderRequest instance)
            {
                this.TCreateOrderRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TCreateOrderRequest.UserName;
                }
                set
                {
                    this.TCreateOrderRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TCreateOrderRequest.Password;
                }
                set
                {
                    this.TCreateOrderRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Ases the I request.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TCreateOrderRequest instance)
        {
            return new TCreateOrderRequestWrapper(instance);
        }

        #endregion

        #region TCloseOrderRequest

        private sealed class TCloseOrderRequestWrapper : IRequest
        {
            private readonly TCloseOrderRequest TCloseOrderRequest;

            internal TCloseOrderRequestWrapper(TCloseOrderRequest instance)
            {
                this.TCloseOrderRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TCloseOrderRequest.UserName;
                }
                set
                {
                    this.TCloseOrderRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TCloseOrderRequest.Password;
                }
                set
                {
                    this.TCloseOrderRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Ases the I request.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TCloseOrderRequest instance)
        {
            return new TCloseOrderRequestWrapper(instance);
        }

        #endregion

        #region TPosRestartRequest

        private sealed class TPosRestartRequestWrapper : IRequest
        {
            private readonly TPosRestartRequest TPosRestartRequest;

            internal TPosRestartRequestWrapper(TPosRestartRequest instance)
            {
                this.TPosRestartRequest = instance;
            }

            public string UserName
            {
                get
                {
                    return this.TPosRestartRequest.UserName;
                }
                set
                {
                    this.TPosRestartRequest.UserName = value;
                }
            }

            public string Password
            {
                get
                {
                    return this.TPosRestartRequest.Password;
                }
                set
                {
                    this.TPosRestartRequest.Password = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IRequest AsIRequest(this TPosRestartRequest instance)
        {
            return new TPosRestartRequestWrapper(instance);
        }

        #endregion

    }
}
