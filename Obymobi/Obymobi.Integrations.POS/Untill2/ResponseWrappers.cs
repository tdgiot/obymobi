﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Untill2Webservice;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// Based on http://stackoverflow.com/questions/7719236/cast-to-not-explicitly-implemented-interface
    /// </summary>
    public static class ResponseWrappers
    {
        #region TGetSalesAreasInfoResponse

        private sealed class TGetSalesAreasInfoResponseWrapper : IResponse
        {
            private readonly TGetSalesAreasInfoResponse tGetSalesAreasInfoResponse;

            internal TGetSalesAreasInfoResponseWrapper(TGetSalesAreasInfoResponse instance)
            {
                this.tGetSalesAreasInfoResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.tGetSalesAreasInfoResponse.ReturnCode;
                }
                set
                {
                    this.tGetSalesAreasInfoResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.tGetSalesAreasInfoResponse.ReturnMessage;
                }
                set
                {
                    this.tGetSalesAreasInfoResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetSalesAreasInfoResponse instance)
        {
            return new TGetSalesAreasInfoResponseWrapper(instance);
        }

        #endregion

        #region TGetArticlesInfoResponse

        private sealed class TGetArticlesInfoResponseWrapper : IResponse
        {
            private readonly TGetArticlesInfoResponse tGetArticlesInfoResponse;

            internal TGetArticlesInfoResponseWrapper(TGetArticlesInfoResponse instance)
            {
                this.tGetArticlesInfoResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.tGetArticlesInfoResponse.ReturnCode;
                }
                set
                {
                    this.tGetArticlesInfoResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.tGetArticlesInfoResponse.ReturnMessage;
                }
                set
                {
                    this.tGetArticlesInfoResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetArticlesInfoResponse instance)
        {
            return new TGetArticlesInfoResponseWrapper(instance);
        }

        #endregion

        #region TGetOptionsInfoResponse

        private sealed class TGetOptionsInfoResponseWrapper : IResponse
        {
            private readonly TGetOptionsInfoResponse TGetOptionsInfoResponse;

            internal TGetOptionsInfoResponseWrapper(TGetOptionsInfoResponse instance)
            {
                this.TGetOptionsInfoResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TGetOptionsInfoResponse.ReturnCode;
                }
                set
                {
                    this.TGetOptionsInfoResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TGetOptionsInfoResponse.ReturnMessage;
                }
                set
                {
                    this.TGetOptionsInfoResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetOptionsInfoResponse instance)
        {
            return new TGetOptionsInfoResponseWrapper(instance);
        }

        #endregion

        #region TGetDepartmentsInfoResponse

        private sealed class TGetDepartmentsInfoResponseWrapper : IResponse
        {
            private readonly TGetDepartmentsInfoResponse TGetDepartmentsInfoResponse;

            internal TGetDepartmentsInfoResponseWrapper(TGetDepartmentsInfoResponse instance)
            {
                this.TGetDepartmentsInfoResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TGetDepartmentsInfoResponse.ReturnCode;
                }
                set
                {
                    this.TGetDepartmentsInfoResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TGetDepartmentsInfoResponse.ReturnMessage;
                }
                set
                {
                    this.TGetDepartmentsInfoResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetDepartmentsInfoResponse instance)
        {
            return new TGetDepartmentsInfoResponseWrapper(instance);
        }

        #endregion

        #region TGetGroupsInfoResponse

        private sealed class TGetGroupsInfoResponseWrapper : IResponse
        {
            private readonly TGetGroupsInfoResponse TGetGroupsInfoResponse;

            internal TGetGroupsInfoResponseWrapper(TGetGroupsInfoResponse instance)
            {
                this.TGetGroupsInfoResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TGetGroupsInfoResponse.ReturnCode;
                }
                set
                {
                    this.TGetGroupsInfoResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TGetGroupsInfoResponse.ReturnMessage;
                }
                set
                {
                    this.TGetGroupsInfoResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetGroupsInfoResponse instance)
        {
            return new TGetGroupsInfoResponseWrapper(instance);
        }

        #endregion

        #region TGetCategoriesInfoResponse

        private sealed class TGetCategoriesInfoResponseWrapper : IResponse
        {
            private readonly TGetCategoriesInfoResponse TGetCategoriesInfoResponse;

            internal TGetCategoriesInfoResponseWrapper(TGetCategoriesInfoResponse instance)
            {
                this.TGetCategoriesInfoResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TGetCategoriesInfoResponse.ReturnCode;
                }
                set
                {
                    this.TGetCategoriesInfoResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TGetCategoriesInfoResponse.ReturnMessage;
                }
                set
                {
                    this.TGetCategoriesInfoResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetCategoriesInfoResponse instance)
        {
            return new TGetCategoriesInfoResponseWrapper(instance);
        }

        #endregion

        #region TGetPricesInfoResponse

        private sealed class TGetPricesInfoResponseWrapper : IResponse
        {
            private readonly TGetPricesInfoResponse TGetPricesInfoResponse;

            internal TGetPricesInfoResponseWrapper(TGetPricesInfoResponse instance)
            {
                this.TGetPricesInfoResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TGetPricesInfoResponse.ReturnCode;
                }
                set
                {
                    this.TGetPricesInfoResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TGetPricesInfoResponse.ReturnMessage;
                }
                set
                {
                    this.TGetPricesInfoResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetPricesInfoResponse instance)
        {
            return new TGetPricesInfoResponseWrapper(instance);
        }

        #endregion

        #region TGetPaymentsInfoResponse

        private sealed class TGetPaymentsInfoResponseWrapper : IResponse
        {
            private readonly TGetPaymentsInfoResponse TGetPaymentsInfoResponse;

            internal TGetPaymentsInfoResponseWrapper(TGetPaymentsInfoResponse instance)
            {
                this.TGetPaymentsInfoResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TGetPaymentsInfoResponse.ReturnCode;
                }
                set
                {
                    this.TGetPaymentsInfoResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TGetPaymentsInfoResponse.ReturnMessage;
                }
                set
                {
                    this.TGetPaymentsInfoResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetPaymentsInfoResponse instance)
        {
            return new TGetPaymentsInfoResponseWrapper(instance);
        }

        #endregion

        #region TGetActiveOrdersResponse

        private sealed class TGetActiveOrdersResponseWrapper : IResponse
        {
            private readonly TGetActiveOrdersResponse TGetActiveOrdersResponse;

            internal TGetActiveOrdersResponseWrapper(TGetActiveOrdersResponse instance)
            {
                this.TGetActiveOrdersResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TGetActiveOrdersResponse.ReturnCode;
                }
                set
                {
                    this.TGetActiveOrdersResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TGetActiveOrdersResponse.ReturnMessage;
                }
                set
                {
                    this.TGetActiveOrdersResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TGetActiveOrdersResponse instance)
        {
            return new TGetActiveOrdersResponseWrapper(instance);
        }

        #endregion

        #region TCreateOrderResponse

        private sealed class TCreateOrderResponseWrapper : IResponse
        {
            private readonly TCreateOrderResponse TCreateOrderResponse;

            internal TCreateOrderResponseWrapper(TCreateOrderResponse instance)
            {
                this.TCreateOrderResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TCreateOrderResponse.ReturnCode;
                }
                set
                {
                    this.TCreateOrderResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TCreateOrderResponse.ReturnMessage;
                }
                set
                {
                    this.TCreateOrderResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TCreateOrderResponse instance)
        {
            return new TCreateOrderResponseWrapper(instance);
        }

        #endregion

        #region TCloseOrderResponse

        private sealed class TCloseOrderResponseWrapper : IResponse
        {
            private readonly TCloseOrderResponse TCloseOrderResponse;

            internal TCloseOrderResponseWrapper(TCloseOrderResponse instance)
            {
                this.TCloseOrderResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TCloseOrderResponse.ReturnCode;
                }
                set
                {
                    this.TCloseOrderResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TCloseOrderResponse.ReturnMessage;
                }
                set
                {
                    this.TCloseOrderResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TCloseOrderResponse instance)
        {
            return new TCloseOrderResponseWrapper(instance);
        }

        #endregion

        #region TPosRestartResponse

        private sealed class TPosRestartResponseWrapper : IResponse
        {
            private readonly TPosRestartResponse TPosRestartResponse;

            internal TPosRestartResponseWrapper(TPosRestartResponse instance)
            {
                this.TPosRestartResponse = instance;
            }

            public int ReturnCode
            {
                get
                {
                    return this.TPosRestartResponse.ReturnCode;
                }
                set
                {
                    this.TPosRestartResponse.ReturnCode = value;
                }
            }

            public string ReturnMessage
            {
                get
                {
                    return this.TPosRestartResponse.ReturnMessage;
                }
                set
                {
                    this.TPosRestartResponse.ReturnMessage = value;
                }
            }
        }

        /// <summary>
        /// Converts to IResponse
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static IResponse AsIResponse(this TPosRestartResponse instance)
        {
            return new TPosRestartResponseWrapper(instance);
        }

        #endregion

    }
}
