﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Logic.Loggers;
using Obymobi.Logic.Untill2Webservice;
using System.IO;
using Dionysos.Diagnostics;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos.Net;
using System.Threading;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// Untill2Connect is for Untill TP API version 2
    /// 
    /// Documentation: https://docs.google.com/document/pub?id=1l8SliLzAp8E_lf01WNS29lJrnvcW04DgzOFM-TpBEeM#id.tau4dvmt9yy6
    /// </summary>
    public class Untill2Connector : IPOSConnector
    {
        #region Fields

        private string webserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2WebserviceUrl);
        private string webserviceUsername = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2WebserviceUser);
        private string webservicePassword = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2WebservicePassword);
        private string pricegroupName = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2Pricegroup);
        private string salesArea = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2SalesArea);
        private bool testMode = Dionysos.ConfigurationManager.GetBool(POSConfigurationConstants.Untill2TestMode);
        private long testModepaymentMethodId = Dionysos.ConfigurationManager.GetLong(POSConfigurationConstants.Untill2TestModePaymentmethodId);
        private Untill2Webservice.ITPAPIPOSservice webservice;
        private bool hasError = false;
        private string errorMessage = string.Empty;

        // Testing / syncing stuff:
        private static Dictionary<string, int> TrackOrdersPerTableForTesting = new Dictionary<string, int>();
        // Long = the Id of the Entity, String contains a list of de products/options for which this was required.
        private Dictionary<long, string> notFoundOptions = new Dictionary<long, string>();
        private Dictionary<long, string> notFoundArticles = new Dictionary<long, string>();
        private Dictionary<long, string> notFoundDepartments = new Dictionary<long, string>();

        private StringBuilder syncReport = new StringBuilder();

        private TSalesAreaInfo[] salesAreaInfo = null;
        private TPriceInfo[] priceInfo = null;
        private TOptionInfo[] optionsInfo = null;
        private TArticleInfo[] articleInfo = null;
        private TDepartmentInfo[] departmentInfo = null;

        #endregion

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="Untill2Connector"/> class.
        /// </summary>
        public Untill2Connector()
        {
            this.WriteToLogExtended("Constructor", "Start");

            if (this.webserviceUrl.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "Webservice url is empty");
            if (this.webserviceUsername.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "WebserviceUsername url is empty");
            if (this.webservicePassword.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "WebservicePassword url is empty");
            if (this.pricegroupName.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "PricegroupName is empty");

            this.WriteToLogNormal("Constructor", "WebserivceUrl: {0}", this.webserviceUrl);
            this.WriteToLogNormal("Constructor", "Username: {0}", this.webserviceUsername);
            this.WriteToLogNormal("Constructor", "Password: {0}", this.webservicePassword);
            this.WriteToLogNormal("Constructor", "Pricegroup: {0}", this.pricegroupName);
            this.WriteToLogNormal("Constructor", "SalesArea: {0}", this.salesArea);
            this.WriteToLogNormal("Constructor", "TestMode: {0}", this.testMode);
            this.WriteToLogNormal("Constructor", "TestModePaymentId: {0}", this.testModepaymentMethodId);

            this.WriteToLogExtended("Constructor", "End");
        }

        private TTPApiPosResponse CallWebservice(TTPApiPosRequest request)
        {
            return this.CallWebservice(request, string.Empty);
        }

        private TTPApiPosResponse CallWebservice(TTPApiPosRequest request, string additionalLogInformation)
        {
            this.WriteToLogExtended("CallWebservice", "Start Request of Type: " + request.GetType().Name);
            StringBuilder fullErrorMessageBuilder = new StringBuilder();
            fullErrorMessageBuilder.AppendLine("\r\n-------------------------------------------------------------------");
            fullErrorMessageBuilder.AppendFormatLine("\r\nStarted: {0}", DateTime.Now);


            TTPApiPosResponse response = null;
            bool success = false;

            DateTime commandStart = DateTime.MinValue;
            TimeSpan commandDuration = TimeSpan.MinValue;

            try
            {
                // Complete Request with Generic Logic
                request.UserName = this.webserviceUsername;
                request.Password = this.webservicePassword;

                // The is a bool so we can keep the old logic in place, might it nog work using the external logic.
                bool internalReattempts = false;

                // Get NOW for timeing of request                
                // Do the request, max 3 times to be 'tolerant' for time-outs       
                // GK This all because Untill pos seems to be behaving crappy.
                for (int i = 0; i < 3; i++)
                {
                    try
                    {
                        // Reset values for duration
                        commandStart = DateTime.Now;
                        commandDuration = TimeSpan.MinValue;

                        // Increase time out.
                        if (TestUtil.IsPcMathieu)
                        {
                            this.WriteToLogNormal("CallWebservice", "THROWING EXCEPTION ON PURPOSE FOR TESTING!!!!");
                            this.WriteToLogNormal("CallWebservice", "THROWING EXCEPTION ON PURPOSE FOR TESTING!!!!");
                            this.WriteToLogNormal("CallWebservice", "THROWING EXCEPTION ON PURPOSE FOR TESTING!!!!");
                            this.WriteToLogNormal("CallWebservice", "THROWING EXCEPTION ON PURPOSE FOR TESTING!!!!");
                            this.Webservice.Timeout = 3;
                            throw new Exception("Unable to connect to the remote server");
                        }
                        else
                        {
                            if (this.testMode)
                                this.Webservice.Timeout = (120 * 1000);
                            else
                                this.Webservice.Timeout = (i + 1) * 30000;
                        }

                        // Perform request.
                        response = PerformRequest(request);

                        // We have completed the request without networking / timeout issues.
                        break;
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Equals("Unable to connect to the remote server") || ex.Message.Equals("The operation has timed out"))
                        {
                            fullErrorMessageBuilder.AppendFormatLine("[Verify POS connection] Test connection to POS on Host: {0} at Time: {1}", this.Webservice.Url, DateTime.Now);

                            fullErrorMessageBuilder.AppendLine("[Verify POS connection] Verify connection using a GetSalesAreasInfo call");
                            bool testCallSuccess = false;
                            bool pingSuccess = false;
                            this.WriteToLogNormal("CallWebservice", "[Verify POS connection] Verify connection using a GetSalesAreasInfo call");
                            try
                            {
                                Untill2Webservice.TGetSalesAreasInfoRequest testRequest = new Untill2Webservice.TGetSalesAreasInfoRequest();
                                TGetSalesAreasInfoResponse testResponse = null;
                                testResponse = this.PerformRequest(testRequest) as TGetSalesAreasInfoResponse;
                                fullErrorMessageBuilder.AppendLine("[Verify POS connection] GetSalesAreasInfo call completed.");
                                this.WriteToLogNormal("CallWebservice", "[Verify POS connection] GetSalesAreasInfo call completed.");
                                testCallSuccess = true;
                            }
                            catch (Exception ex2)
                            {
                                fullErrorMessageBuilder.AppendFormatLine("[Verify POS connection] GetSalesAreasInfo call failed: {0}.", ex2.Message);
                                this.WriteToLogNormal("CallWebservice", "[Verify POS connection] GetSalesAreasInfo call failed: {0}.", ex2.Message);

                                Uri webserviceUri = new Uri(this.Webservice.Url);

                                long responseTime;
                                if (PingUtil.Ping(webserviceUri.Host, out responseTime))
                                {
                                    fullErrorMessageBuilder.AppendFormatLine("[Verify POS connection] Ping succeeded to '{0}' in {1}ms.", webserviceUri.Host, responseTime);
                                    this.WriteToLogNormal("CallWebservice", "[Verify POS connection] Ping succeeded to '{0}' in {1}ms.", webserviceUri.Host, responseTime);
                                    pingSuccess = true;
                                }
                                else
                                {
                                    fullErrorMessageBuilder.AppendFormatLine("[Verify POS connection] Ping to POS at '{0}' failed.", webserviceUri.Host);
                                    this.WriteToLogNormal("CallWebservice", "[Verify POS connection] Ping to POS at '{0}' failed.", webserviceUri.Host);
                                }
                            }

                            if (internalReattempts)
                            {
                                // Wait for a bit
                                this.WriteToLogNormal("CallWebservice", "[Verify POS connection] Wait {0} seconds before next attempt.", ((i + 1) * 5));
                                Thread.Sleep((i + 1) * 5000);
                            }

                            // Prepare a response, since we have none.
                            response = new TTPApiPosResponse();
                            response.ReturnCode = (int)WebserviceResultCode.ConnectivityFailure;
                            response.ReturnMessage = string.Format("Connectivity failed: Test Webservice call: '{0}', Ping call: '{0}'", testCallSuccess, pingSuccess);
                        }
                        else
                            throw;
                    }

                    if (!internalReattempts)
                        break;
                }

                commandDuration = DateTime.Now - commandStart;

                // Check if the request failed
                WebserviceResultCode result;
                if (response != null && EnumUtil.TryParse(response.ReturnCode, out result))
                {
                    // We could parse the result
                }
                else
                    result = WebserviceResultCode.NoResult;

                if (result != WebserviceResultCode.Ok)
                {
                    // Quick error for normal log
                    if (response != null)
                    {
                        this.errorMessage = string.Format("Command Failed: {0} - {1} ({2})\r\n{3}",
                            request.GetType(), result.ToString(), response.ReturnCode, response.ReturnMessage);
                    }
                    else
                    {
                        this.errorMessage = string.Format("Command Failed: {0} - {1} - Result == null\r\n",
                            request.GetType(), result.ToString());
                    }
                    this.WriteToLogNormal("CallWebservice", this.errorMessage);

                    // Extensive Error for Untill Log                    
                    fullErrorMessageBuilder.AppendFormatLine("\r\n");
                    fullErrorMessageBuilder.AppendFormatLine("Request Type:   {0}", request.GetType());
                    fullErrorMessageBuilder.AppendFormatLine("Username:       {0}", request.UserName);
                    fullErrorMessageBuilder.AppendFormatLine("Password:       {0}", request.Password);

                    if (response != null)
                    {
                        fullErrorMessageBuilder.AppendFormatLine("Return Code:    {0} ({1})", response.ReturnCode, result.ToString());
                        fullErrorMessageBuilder.AppendFormatLine("Return Message: {0}", response.ReturnMessage);
                    }
                    fullErrorMessageBuilder.AppendFormatLine("Process time:   {0}ms", commandDuration.TotalMilliseconds);

                    if (request is TCreateOrderRequest)
                    {
                        OrderSpecificWebserviceLogging(request, fullErrorMessageBuilder);
                    }

                    fullErrorMessageBuilder.AppendFormatLine("Additional Info:\r\n{0}", additionalLogInformation);
                    fullErrorMessageBuilder.AppendFormatLine("\r\nEnded: {0}", DateTime.Now);
                    fullErrorMessageBuilder.AppendLine("\r\n-------------------------------------------------------------------\r\n");
                    string fullError = fullErrorMessageBuilder.ToString();

                    this.WriteToUntillLog(fullError);

                    this.HasError = true;
                    this.ErrorMessage = fullError;

                    // Only throw exception of a 'Valid' response when it's not an CreateOrderRequest/
                    // because that has it's own handling.                     
                    if (!(request is TCreateOrderRequest))
                        throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, fullError);
                }
                else
                    success = true;
            }
            catch (Exception ex)
            {
                commandDuration = DateTime.Now - commandStart;

                this.errorMessage = string.Format("Command Failed: {0} - {1}", request.GetType(), ex.Message);

                fullErrorMessageBuilder.AppendFormatLine("Response Type: {0}", request.GetType());
                fullErrorMessageBuilder.AppendFormatLine("Process time:  {0}ms", commandDuration.TotalMilliseconds);
                fullErrorMessageBuilder.AppendFormatLine("Message:       {0}", ex.Message);
                fullErrorMessageBuilder.AppendFormatLine("Stack:         {0}", ex.StackTrace);

                if (ex.InnerException != null)
                {
                    var ex2 = ex.InnerException;
                    fullErrorMessageBuilder.AppendFormatLine("Inner Message: {0}", ex2.Message);
                    fullErrorMessageBuilder.AppendFormatLine("Inner Stack: {0}", ex2.StackTrace);

                    if (ex2.InnerException != null)
                    {
                        var ex3 = ex2.InnerException;
                        fullErrorMessageBuilder.AppendFormatLine("Inner Inner Message: {0}", ex3.Message);
                        fullErrorMessageBuilder.AppendFormatLine("Inner Inner Stack: {0}", ex3.StackTrace);
                    }
                }

                fullErrorMessageBuilder.AppendLine("\r\n-------------------------------------------------------------------\r\n");

                string fullError = fullErrorMessageBuilder.ToString();

                this.WriteToUntillLog(fullError);

                this.HasError = true;
                this.ErrorMessage = fullError;

                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, fullError);
            }
            finally
            {
                if (response == null)
                {
                    response = new TTPApiPosResponse();
                    response.ReturnCode = (int)WebserviceResultCode.NoResult;
                    response.ReturnMessage = "No Result Available - Failure";
                }

                if (success)
                    this.WriteToLogExtended("CallWebservice", "End Request of Type: {0} (Success - {1}ms)", request.GetType().Name, commandDuration.TotalMilliseconds);
                else
                    this.WriteToLogExtended("CallWebservice", "End Request of Type: {0} (Failure - {1}ms)", request.GetType().Name, commandDuration.TotalMilliseconds);
            }

            return response;
        }

        private TTPApiPosResponse PerformRequest(TTPApiPosRequest request)
        {
            TTPApiPosResponse response;
            if (request is TGetSalesAreasInfoRequest)
                response = this.Webservice.GetSalesAreasInfo(request as TGetSalesAreasInfoRequest);
            else if (request is TGetPricesInfoRequest)
                response = this.Webservice.GetPricesInfo(request as TGetPricesInfoRequest);
            else if (request is TGetOptionsInfoRequest)
                response = this.webservice.GetOptionsInfo(request as TGetOptionsInfoRequest);
            else if (request is TGetDepartmentsInfoRequest)
                response = this.webservice.GetDepartmentsInfo(request as TGetDepartmentsInfoRequest);
            else if (request is TGetArticlesInfoRequest)
                response = this.webservice.GetArticlesInfo(request as TGetArticlesInfoRequest);
            else if (request is TCreateOrderRequest)
                response = this.webservice.CreateOrder(request as TCreateOrderRequest);
            else if (request is TCloseOrderRequest)
                response = this.webservice.CloseOrder(request as TCloseOrderRequest);
            else
            {
                this.WriteToLogNormal("CallWebservice", "Request Type not yet implemented: " + request.GetType().Name);
                throw new NotImplementedException(string.Format("NOT IMPLEMENTED: Callwebservice Request Type: {0}", request.GetType().Name));
            }
            return response;
        }

        private static void OrderSpecificWebserviceLogging(TTPApiPosRequest request, StringBuilder fullErrorMessageBuilder)
        {
            var requestTyped = ((TCreateOrderRequest)request);
            StringBuilder soapRequest = new StringBuilder();

            soapRequest.AppendFormatLine("<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:TPAPIPosIntfU-ITPAPIPOS\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">");
            soapRequest.AppendFormatLine("   <soapenv:Header/>");
            soapRequest.AppendFormatLine("   <soapenv:Body>");
            soapRequest.AppendFormatLine("      <urn:CreateOrder soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
            soapRequest.AppendFormatLine("         <Request xsi:type=\"urn:TCreateOrderRequest\" xmlns:urn=\"urn:TPAPIPosIntfU\">");
            soapRequest.AppendFormatLine("            <Password xsi:type=\"xsd:string\">{0}</Password>", requestTyped.Password);
            soapRequest.AppendFormatLine("            <UserName xsi:type=\"xsd:string\">{0}</UserName>", requestTyped.UserName);
            soapRequest.AppendFormatLine("            <TableNumber xsi:type=\"xsd:int\">{0}</TableNumber>", requestTyped.TableNumber);
            soapRequest.AppendFormatLine("            <TablePart xsi:type=\"xsd:string\">a</TablePart>");
            soapRequest.AppendFormatLine("            <ClientName xsi:type=\"xsd:string\"></ClientName>");
            soapRequest.AppendFormatLine("            <OrderName xsi:type=\"xsd:string\"></OrderName>");
            soapRequest.AppendFormatLine("            <OrderDescr xsi:type=\"xsd:string\"></OrderDescr>");
            soapRequest.AppendFormatLine("            <Items xsi:type=\"urn:TOrderItemArray\" soapenc:arrayType=\"urn:TOrderItem[{0}]\">", requestTyped.Items.Count());

            int itemNumber = 1;
            foreach (var item in requestTyped.Items)
            {
                fullErrorMessageBuilder.AppendFormatLine("Orderitem: ArticleId: {0}, ItemNumber: {1}, ManualPrice: {2}, OrderItemType: {3}, Quantity: {4}, Text: {5}",
                    item.ArticleId, item.ItemNumber, item.ManualPrice, item.OrderItemType, item.Quantity, item.Text);

                soapRequest.AppendFormatLine("               <item xsi:type=\"urn:TOrderItem\">");
                soapRequest.AppendFormatLine("                        <ItemNumber xsi:type=\"xsd:int\">{0}</ItemNumber>", itemNumber);
                soapRequest.AppendFormatLine("                        <ArticleId xsi:type=\"xsd:long\">{0}</ArticleId>", item.ArticleId);
                soapRequest.AppendFormatLine("                        <OrderItemType xsi:type=\"xsd:int\">{0}</OrderItemType>", item.OrderItemType);
                soapRequest.AppendFormatLine("                        <Text xsi:type=\"xsd:string\"></Text>");
                soapRequest.AppendFormatLine("                        <ManualPrice xsi:type=\"xsd:double\">0</ManualPrice>");
                soapRequest.AppendFormatLine("                        <Quantity xsi:type=\"xsd:int\">{0}</Quantity>", item.Quantity);
                soapRequest.AppendFormatLine("                </item>");

                itemNumber++;
            }

            soapRequest.AppendFormatLine("             </Items>");
            soapRequest.AppendFormatLine("         </Request>");
            soapRequest.AppendFormatLine("      </urn:CreateOrder>");
            soapRequest.AppendFormatLine("   </soapenv:Body>");
            soapRequest.AppendFormatLine("</soapenv:Envelope>");

            fullErrorMessageBuilder.AppendFormatLine("Test Server Request (THIS IS A RENDERED VERSION, MIGHT NOT BE 100% ACCURATE WITH WHAT WAS SENT TO THE POS!)");
            fullErrorMessageBuilder.AppendFormatLine(soapRequest.ToString());
        }

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        Model.Posproduct[] IPOSConnector.GetPosproducts()
        {
            this.WriteToLogExtended("GetPosproducts", "Start");

            this.syncReport = new StringBuilder();
            this.syncReport.AppendFormatLine("\r\n\r\n");
            this.syncReport.AppendFormatLine("******** SYNC START ********");
            this.syncReport.AppendFormatLine("Sync started at: {0}", DateTime.Now.ToString());
            this.syncReport.AppendFormatLine("SalesArea: {0}", this.salesArea);
            this.syncReport.AppendFormatLine("PriceGroup: {0}", this.pricegroupName);

            try
            {
                // Get SalesAreas (to know which price level to use
                this.GetSalesAreasInfo();

                // GetPricesInfo (to retrieve the prices for the products)
                this.GetPricesInfo();

                // Options (to later link them based on  the ID's)
                this.GetOptionsInfo();

                // Get Departments
                this.GetDepartmentInfo();

                // Articles to glue it all together
                this.GetArticlesInfo();

                this.WriteToLogExtended("GetPosproducts", "Convert XML to Posproducts");
                List<Posproduct> products = new List<Posproduct>();

                foreach (var article in this.articleInfo)
                {
                    try
                    {
                        products.Add(this.ConvertArticleInfoToPosProduct(article));
                    }
                    catch (POSException pex)
                    {
                        // Wrong product, don't fail whole batch on that
                        syncReport.AppendFormatLine("Article '{0}' ('{1}') couldn't be imported: '{2}'.", article.ArticleName, article.ArticleId, pex.Message);
                    }
                }

                // If we have missing stuff, try to find it anyway so it's easier to solve afterwards
                if (this.notFoundArticles.Count > 0)
                    this.HandleMissingArticles();
                if (this.notFoundOptions.Count > 0)
                    this.HandleMissingOptions();

                this.syncReport.AppendFormatLine("Sync finished with success at: {0}", DateTime.Now.ToString());
                this.syncReport.AppendFormatLine("******** SYNC FINISH ********");
                this.WriteToUntillLog(this.syncReport.ToString());

                this.WriteToLogExtended("GetPosproducts", "End - Success");

                return products.ToArray();
            }
            catch
            {
                this.WriteToLogExtended("GetPosproducts", "End - Failure");
                this.syncReport.AppendFormatLine("Sync finished with failure at: {0}", DateTime.Now.ToString());
                this.WriteToUntillLog(this.syncReport.ToString());

                throw;
            }
        }

        private void HandleMissingArticles()
        {
            var salesAreas = this.GetAllSalesAreas();
            this.syncReport.AppendFormatLine("** Missing Articles **");
            foreach (var missingArticle in this.notFoundArticles)
            {
                // Write the ArticleId
                this.syncReport.AppendFormatLine("ArticleId: {0}", missingArticle.Key);

                // Try to find it any way
                var response = this.GetArticleById(missingArticle.Key, false);
                if (response.Articles.Length > 0)
                {
                    this.syncReport.AppendFormatLine("ArticleName: {0}", response.Articles[0].ArticleName);
                    this.syncReport.AppendFormatLine("ArticleNumber: {0}", response.Articles[0].ArticleNumber);
                    this.syncReport.AppendFormatLine("SalesAreas: {0}", this.GetSalesAreaNames(salesAreas.SalesAreas, response.Articles[0].Available));
                }
                else
                {
                    response = this.GetArticleById(missingArticle.Key, true);
                    if (response.Articles.Length > 0)
                    {
                        this.syncReport.AppendFormatLine("Article INACTIVE");
                        this.syncReport.AppendFormatLine("ArticleName: {0}", response.Articles[0].ArticleName);
                        this.syncReport.AppendFormatLine("ArticleNumber: {0}", response.Articles[0].ArticleNumber);
                        this.syncReport.AppendFormatLine("SalesAreas: {0}", this.GetSalesAreaNames(salesAreas.SalesAreas, response.Articles[0].Available));
                    }
                    else
                    {
                        this.syncReport.AppendFormatLine("Article NOT FOUND BY GetArticleById");
                    }
                }

                this.syncReport.AppendFormatLine("Used for: {0}", missingArticle.Value.Replace(",", "\r\n"));
            }
        }

        private void HandleMissingOptions()
        {
            var salesAreas = this.GetAllSalesAreas();
            this.syncReport.AppendFormatLine("** Missing Options **");

            foreach (var missingOption in this.notFoundOptions)
            {
                // Write the ArticleId
                this.syncReport.AppendFormatLine("\r\n\r\n");
                this.syncReport.AppendFormatLine("OptionId: {0}", missingOption.Key);

                // Try to find it any way
                var response = this.GetOptionById(missingOption.Key);
                if (response.Options.Length > 0)
                {
                    this.syncReport.AppendFormatLine("OptionName: {0}", response.Options[0].OptionName);
                    this.syncReport.AppendFormatLine("SalesAreas: {0}", this.GetSalesAreaNames(salesAreas.SalesAreas, response.Options[0].Available));
                }
                else
                {
                    this.syncReport.AppendFormatLine("Option NOT FOUND BY GetOptionById");
                }

                this.syncReport.AppendFormatLine("Used for: {0}", missingOption.Value.Replace(",", "\r\n"));
            }
        }

        private string GetSalesAreaNames(TSalesAreaInfo[] salesAreas, long[] salesAreaIds)
        {
            string result = "";
            foreach (var salesAreaId in salesAreaIds)
            {
                var salesArea = salesAreas.FirstOrDefault(sa => sa.SalesAreaId == salesAreaId);
                if (salesArea != null)
                    result = StringUtil.CombineWithComma(result, salesArea.SalesAreaName);
                else
                    result = StringUtil.CombineWithComma(result, "Not found: " + salesAreaId);
            }
            return result;
        }

        private Posproduct ConvertArticleInfoToPosProduct(TArticleInfo article)
        {
            Posproduct posproduct = new Posproduct();
            posproduct.ExternalId = article.ArticleId.ToString();
            posproduct.ExternalPoscategoryId = article.DepartmentId > 0 ? article.DepartmentId.ToString() : "-9999";
            posproduct.Name = article.ArticleName;

            // GK Seems to be of no use, since it's reset later by article.IsMenu
            //posproduct.FieldValue1 = article.ArticleNumber.ToString();

            // Find price for our price group
            var priceToUse = this.priceInfo.FirstOrDefault(p => p.PriceName == this.pricegroupName);

            // Try to find the price belonging to the chosen pricegroup.
            bool specificPriceFound = false;
            posproduct.PriceIn = 0;
            if (priceToUse == null)
            {
                this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Article: {0} ({1} - Pricegroup not found: {2}", article.ArticleName, article.ArticleId, this.pricegroupName);
            }
            else
            {
                var price = article.Prices.FirstOrDefault(p => p.PriceId == priceToUse.PriceId);
                if (price == null)
                    this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Article: {0} ({1} - Price not found for PriceGroup: {2}", article.ArticleName, article.ArticleId, priceToUse.PriceName);
                else
                {
                    posproduct.PriceIn = (decimal)price.Amount;
                    specificPriceFound = true;
                    this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Article: {0} ({1} - Price IS found for PriceGroup: {2} - Price: {3}", article.ArticleName, article.ArticleId, priceToUse.PriceName, price.Amount);
                }
            }

            // We did not find a price for the configured price group
            // Better something than nothing, take first price if none was found.
            if (!specificPriceFound)
            {
                if (article.Prices != null && article.Prices.LongLength > 0)
                    posproduct.PriceIn = (decimal)article.Prices[0].Amount;
            }

            // Default:
            posproduct.VatTariff = 2;

            // Keep track of stuff we need when converting orders to posorders later.
            posproduct.FieldValue1 = article.IsMenu.ToString();
            posproduct.FieldValue2 = article.IsManualPrice.ToString();
            posproduct.FieldValue10 = article.ArticleNumber.ToString();

            // Alterations, this is quite a gathering:
            // supplements for an article (articles from an option specified in BO: Department / Supplement)
            // condiments for an article (articles from an option specified in BO: Department / Condiment)
            // must-have options for an article (articles from an options listed in BO: Articles / tab “Options”)
            // free-option for an article (articles from an option specified in BO: Articles / tab “Options” / field “Free Option”)
            // menu items. If certain article is a menu (Checkbox “Menu” in BO: Articles), the list of “must have” options for this article acts as a list of article-groups. Client may order one article per each option, this article will be included into order as “menu item”
            List<KeyValuePair<long, OrderitemType>> options = new List<KeyValuePair<long, OrderitemType>>();

            // Must have options
            if (article.Options != null)
            {
                foreach (var optionId in article.Options)
                    options.Add(new KeyValuePair<long, OrderitemType>(optionId, OrderitemType.MustHaveOption));
            }

            // Free options
            if (article.FreeOption > 0)
                options.Add(new KeyValuePair<long, OrderitemType>(article.FreeOption, OrderitemType.FreeOption));

            // Find department, required for supplements & condiments
            var department = this.departmentInfo.FirstOrDefault(dep => dep.DepartmentId == article.DepartmentId);

            if (department != null)
            {
                // Supplements
                if (department.Supplement > 0)
                    options.Add(new KeyValuePair<long, OrderitemType>(department.Supplement, OrderitemType.Supplement));

                // Condiments
                if (department.Condiment > 0)
                    options.Add(new KeyValuePair<long, OrderitemType>(department.Condiment, OrderitemType.Condiment));
            }
            else
            {
                this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Article: {0} ({1} - Department not found: {2}", article.ArticleName, article.ArticleId, department);
            }

            // Now add all of the gatered options to the product
            List<Posalteration> alterations = new List<Posalteration>();
            foreach (KeyValuePair<long, OrderitemType> optionId in options)
            {
                // GK Options can be added twice if they are added in 2 different type 
                // (i.e. Must Have & Free, or Must Have and Supplement)
                var option = this.optionsInfo.FirstOrDefault(o => o.OptionId == optionId.Key);

                if (option != null)
                {
                    Posalteration pa = new Posalteration();
                    pa.ExternalId = option.OptionId.ToString() + "_" + optionId.Value;
                    pa.Name = option.OptionName;

                    // This is must have, so:
                    if (optionId.Value == OrderitemType.MustHaveOption)
                        pa.MinOptions = 1;
                    else
                        pa.MinOptions = 0;

                    // By default we allow for ONE option
                    pa.MaxOptions = 1;

                    pa.FieldValue1 = ((int)optionId.Value).ToString();

                    this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Article: {0} ({1}) - Option: {2} ({3})", article.ArticleName, article.ArticleId, pa.Name, optionId.Key);

                    // Now add the Options vlaues
                    List<Posalterationoption> paos = new List<Posalterationoption>();
                    foreach (var optionChoice in option.Items)
                    {
                        if (priceToUse == null || optionChoice.PriceId == priceToUse.PriceId)
                        {
                            var pao = new Posalterationoption();

                            // Find Article
                            var optionChoiceArticle = this.articleInfo.FirstOrDefault(a => a.ArticleId == optionChoice.ArticleId);

                            if (optionChoiceArticle == null)
                            {
                                if (!this.notFoundArticles.ContainsKey(optionChoice.ArticleId))
                                {
                                    this.notFoundArticles.Add(optionChoice.ArticleId, string.Format("Optionitem-Article for Option: {0} (for Article: {1}),", pa.Name, article.ArticleName));
                                }
                                else
                                {
                                    this.notFoundArticles[optionChoice.ArticleId] += string.Format("Optionitem-Article for Option: {0} (for Article: {1}),", pa.Name, article.ArticleName);
                                }
                                this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Article: {0} ({1}) - OptionChoice article not found: {2},", article.ArticleName, article.ArticleId, optionChoice.ArticleId);
                            }
                            else
                            {
                                // Set simple fields
                                pao.Name = optionChoiceArticle.ArticleName;
                                pao.ExternalId = option.OptionId + "" + optionChoice.ArticleId;
                                pao.PosproductExternalId = optionChoice.ArticleId.ToString();

                                this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Article: {0} ({1}) - Option: {2} ({3}) - Choice: {4} - Amount: {5} ({6})", article.ArticleName, article.ArticleId, pa.Name, optionId.Key, pao.Name, optionChoice.Amount, optionChoice.PriceId);

                                // Find Price
                                if (optionChoice.Amount > 0)
                                {
                                    pao.PriceIn = (decimal)optionChoice.Amount;
                                }
                                /*else if (priceToUse != null)
                                {
                                    var price = this.priceInfo.FirstOrDefault(pi => pi.PriceId == priceToUse.PriceId);
                                    if (price != null && priceToUse.PriceId == optionChoice.PriceId)
                                    {
                                        pao.PriceIn = 0;
                                        this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Optionchoice-Price for OptionChoice: {0} - Price != NULL - Price Id: {1}", pao.Name, price.PriceId);
                                    }
                                }*/

                                paos.Add(pao);
                            }
                        }
                    }

                    pa.Posalterationoptions = paos.ToArray();

                    // Only add if there's one or more options, if not, just ignore (bad POS config)
                    // but NOT when it's a required option, in that case we exception the thing.
                    if (pa.Posalterationoptions.Length > 0)
                    {
                        alterations.Add(pa);
                    }
                    else if (pa.MinOptions.HasValue && pa.MinOptions.Value > 0)
                    {
                        // We've got NO options and the Alteration requires a choice - Will fail so we shouldn't add this products.
                        string message = String.Format("Product: '{0}' has a required alteration: '{1}' that has no zero options. Therefore can't be added", posproduct.Name, pa.Name);
                        this.WriteToLogNormal("ConvertARticleInforToPosProduct", message);
                        this.WriteToUntillLog(message);

                        throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, "Inconsitent data found: {0}", message);
                    }
                    else
                    {
                        this.syncReport.AppendFormatLine("Product: '{0}' has an alteration: '{1}' that has no zero options. The Alteration will be ignored.",
                            posproduct.Name, pa.Name);
                    }
                }
                else
                {
                    if (!this.notFoundOptions.ContainsKey(optionId.Key))
                    {
                        this.notFoundOptions.Add(optionId.Key, "Option for: " + article.ArticleName + ",");
                    }
                    else
                    {
                        this.notFoundOptions[optionId.Key] += "Option for: " + article.ArticleName + ",";
                    }
                    this.WriteToLogNormal("ConvertArticleInfoToPosProduct", "Article: {0} ({1}) - Option not found: {2}", article.ArticleName, article.ArticleId, optionId.Key);
                }
            }

            posproduct.Posalterations = alterations.ToArray();

            return posproduct;
        }

        private void GetSalesAreasInfo()
        {
            Untill2Webservice.TGetSalesAreasInfoRequest request = new Untill2Webservice.TGetSalesAreasInfoRequest();
            var response = this.CallWebservice(request) as TGetSalesAreasInfoResponse;

            // Select the Correct Sales Area (prepared for multiple areas, but currently only 1 is supported)
            Dictionary<long, TSalesAreaInfo> salesAreas = new Dictionary<long, TSalesAreaInfo>();
            foreach (var sa in response.SalesAreas)
            {
                if (sa.SalesAreaName.Equals(this.salesArea, StringComparison.InvariantCultureIgnoreCase))
                {
                    salesAreas.Add(sa.SalesAreaId, sa);
                }
            }

            // If non are found, throw Exception
            if (salesAreas.Count == 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormatLine("Untill2Connector requires that at least one sales Area is available");
                sb.AppendFormatLine("Configured in the CMS is SalesArea: '{0}'", this.salesArea);
                sb.AppendFormatLine("Available SalesAreas:");

                foreach (var sa in salesAreas)
                {
                    sb.AppendFormatLine("'{0}'", sa.Value.SalesAreaName);
                }

                this.syncReport.AppendFormatLine(sb.ToString());

                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypointGroups, sb.ToString());
            }

            this.salesAreaInfo = salesAreas.Values.ToArray();
        }

        private void GetPricesInfo()
        {
            Untill2Webservice.TGetPricesInfoRequest request = new Untill2Webservice.TGetPricesInfoRequest();
            var response = this.CallWebservice(request) as TGetPricesInfoResponse;
            this.priceInfo = response.Prices;
        }

        private void GetOptionsInfo()
        {
            Dictionary<long, TOptionInfo> options = new Dictionary<long, TOptionInfo>();
            foreach (var salesArea in this.salesAreaInfo)
            {
                Untill2Webservice.TGetOptionsInfoRequest request = new Untill2Webservice.TGetOptionsInfoRequest();

                request.SalesAreaId = salesArea.SalesAreaId;

                var response = this.CallWebservice(request) as TGetOptionsInfoResponse;

                foreach (var option in response.Options)
                {
                    if (!options.ContainsKey(option.OptionId))
                    {
                        options.Add(option.OptionId, option);
                    }
                }
            }

            this.optionsInfo = options.Values.ToArray();
        }

        private void GetArticlesInfo()
        {
            // GK To prevent that we load products that are not available for any sales areas (and therefore 
            // not orderable) we request the articles info per sale area.  
            Dictionary<long, TArticleInfo> articles = new Dictionary<long, TArticleInfo>();

            foreach (var salesArea in this.salesAreaInfo)
            {
                Untill2Webservice.TGetArticlesInfoRequest request = new Untill2Webservice.TGetArticlesInfoRequest();

                request.GetInactive = false;
                request.SalesAreaId = salesArea.SalesAreaId;

                var response = this.CallWebservice(request) as TGetArticlesInfoResponse;

                foreach (var article in response.Articles)
                {
                    if (!articles.ContainsKey(article.ArticleId))
                    {
                        articles.Add(article.ArticleId, article);
                    }
                }

            }

            this.articleInfo = articles.Values.ToArray();
        }

        private TGetArticlesInfoResponse GetArticleById(long articleId, bool getInactive)
        {
            Untill2Webservice.TGetArticlesInfoRequest request = new Untill2Webservice.TGetArticlesInfoRequest();
            request.ArticleId = articleId;

            var response = this.CallWebservice(request) as TGetArticlesInfoResponse;
            return response;
        }

        private TGetOptionsInfoResponse GetOptionById(long optionId)
        {
            Untill2Webservice.TGetOptionsInfoRequest request = new Untill2Webservice.TGetOptionsInfoRequest();
            request.OptionId = optionId;

            var response = this.CallWebservice(request) as TGetOptionsInfoResponse;
            return response;
        }

        private TGetSalesAreasInfoResponse GetAllSalesAreas()
        {
            Untill2Webservice.TGetSalesAreasInfoRequest request = new Untill2Webservice.TGetSalesAreasInfoRequest();
            var response = this.CallWebservice(request) as TGetSalesAreasInfoResponse;
            return response;
        }

        private TCloseOrderResponse CloseOrderForTesting(int tableNumber)
        {
            Untill2Webservice.TCloseOrderRequest request = new Untill2Webservice.TCloseOrderRequest();
            request.TableNumber = tableNumber;
            request.TablePart = "a";
            request.PaymentId = this.testModepaymentMethodId;

            var response = this.CallWebservice(request) as TCloseOrderResponse;
            return response;
        }

        private void GetDepartmentInfo()
        {
            Untill2Webservice.TGetDepartmentsInfoRequest request = new Untill2Webservice.TGetDepartmentsInfoRequest();
            var response = this.CallWebservice(request) as TGetDepartmentsInfoResponse;
            this.departmentInfo = response.Departments;
        }

        Model.Poscategory[] IPOSConnector.GetPoscategories()
        {
            if (this.departmentInfo == null)
                this.GetDepartmentInfo();

            List<Poscategory> poscategories = new List<Poscategory>();
            foreach (var department in this.departmentInfo)
            {
                Poscategory poscategory = new Poscategory();
                poscategory.ExternalId = department.DepartmentId.ToString();
                poscategory.Name = department.DepartmentName;
                poscategories.Add(poscategory);
            }

            return poscategories.ToArray();
        }

        Model.Posdeliverypointgroup[] IPOSConnector.GetPosdeliverypointgroups()
        {
            if (this.salesAreaInfo == null)
                this.GetSalesAreasInfo();

            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();
            foreach (var salesArea in this.salesAreaInfo)
            {
                Posdeliverypointgroup pdpg = new Posdeliverypointgroup();
                pdpg.ExternalId = salesArea.SalesAreaId.ToString();
                pdpg.Name = salesArea.SalesAreaName;
                posdeliverypointgroups.Add(pdpg);
            }

            return posdeliverypointgroups.ToArray();
        }

        Model.Posdeliverypoint[] IPOSConnector.GetPosdeliverypoints()
        {
            if (this.salesAreaInfo == null)
                this.GetSalesAreasInfo();

            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();
            foreach (var salesArea in this.salesAreaInfo)
            {
                foreach (var tablerange in salesArea.Tables)
                {
                    for (int i = tablerange.FromTable; i <= tablerange.ToTable; i++)
                    {
                        Posdeliverypoint pd = new Posdeliverypoint();
                        pd.ExternalPosdeliverypointgroupId = salesArea.SalesAreaId.ToString();
                        pd.ExternalId = i.ToString();
                        pd.Name = i.ToString() + " - " + salesArea.SalesAreaName;
                        pd.Number = i.ToString();
                        posdeliverypoints.Add(pd);
                    }
                }
            }

            return posdeliverypoints.ToArray();
        }

        Model.Posorder IPOSConnector.GetPosorder(string deliverypointNumber)
        {
            return null;
        }

        Enums.OrderProcessingError IPOSConnector.SaveOrder(Model.Posorder posorder)
        {
            if (TestUtil.IsPcGabriel)
            {
                this.WriteToLogNormal("SaveOrder", "GK OVERWRITE - SAVE ORDER IS FAKED!!!!!!!!");
                this.WriteToLogNormal("SaveOrder", "GK OVERWRITE - SAVE ORDER IS FAKED!!!!!!!!");
                this.WriteToLogNormal("SaveOrder", "GK OVERWRITE - SAVE ORDER IS FAKED!!!!!!!!");
                //throw new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, "FAKE!");
                return OrderProcessingError.None;
            }

            Obymobi.Enums.OrderProcessingError status = OrderProcessingError.None;

            this.WriteToLogExtended("SaveOrder", "Start");

            //	ItemNumber	Integer	Order item sequential number
            //	ArticleId	Long	Article id (Zero for OrderItemType=6)
            //	OrderItemType	Integer	Order item type, ref. App3: Constants
            //	Text	String	Text for order_item_type=6
            //	ManualPrice	Decimal	Specifies price for articles with IsManualPrice=true
            //	Quantity	Integer	Quantity of ordered items. Valid only for OrderItemType=0 and 5

            TCreateOrderRequest request = new TCreateOrderRequest();
            request.TableNumber = Convert.ToInt32(posorder.PosdeliverypointExternalId);
            request.TablePart = "a";

            List<TOrderItem> orderItems = new List<TOrderItem>();
            int itemNumber = 1;
            foreach (var posorderitem in posorder.Posorderitems)
            {
                TOrderItem item = new TOrderItem();

                item.ItemNumber = itemNumber;
                itemNumber++;

                item.ArticleId = Convert.ToInt64(posorderitem.PosproductExternalId);
                item.Quantity = posorderitem.Quantity;

                // GK This is it:
                item.OrderItemType = Convert.ToInt32(posorderitem.FieldValue1);

                orderItems.Add(item);
            }

            request.Items = orderItems.ToArray();

            // Prepare additional log information
            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("\r\nOrder info");
            sb.AppendFormatLine("Table Number:   {0}", request.TableNumber);
            sb.AppendFormatLine("Table Part:     {0}", request.TablePart);
            sb.AppendFormatLine("PosorderId:     {0}", posorder.PosorderId);

            foreach (var orderItem in posorder.Posorderitems)
            {
                sb.AppendFormatLine("Order");
            }

            sb.AppendFormatLine(XmlHelper.Serialize(posorder));

            var response = this.CallWebservice(request, sb.ToString()) as TTPApiPosResponse;

            if (response != null && response.ReturnCode == (int)WebserviceResultCode.Ok)
            {
                // Success!                
            }
            else if (response != null)
            {
                string returnMessageLower = response.ReturnMessage.ToLower();
                // Exception: Table %d already opened by waiter "%s" on PC "%s
                // 'Can not open table part: "a"'
                if (returnMessageLower.Contains("already opened by waiter") || returnMessageLower.Contains("can not open table"))
                    throw new DeliverypointLockedException(response.ReturnMessage);
                else if (returnMessageLower.Contains("option can't be ordered") || 
                    returnMessageLower.Contains("not avaiable in sales area") ||
                    response.ReturnMessage.Contains("TBLRGetOption", StringComparison.OrdinalIgnoreCase))
                    throw new InvalidProductsOrAlterationsException(response.ReturnMessage);
                else if (response.ReturnCode == (int)WebserviceResultCode.ConnectivityFailure)
                {
                    throw new ConnectivityException(response.ReturnMessage);
                }
                else
                    status = OrderProcessingError.PosErrorUnspecifiedFatalPosProblem;
            }
            else
            {
                status = OrderProcessingError.PosErrorUnspecifiedFatalPosProblem;
            }

            // Count orders when testing
            if (this.testMode)
            {
                bool newTable = false;
                if (!Untill2Connector.TrackOrdersPerTableForTesting.ContainsKey(posorder.PosdeliverypointExternalId))
                {
                    Untill2Connector.TrackOrdersPerTableForTesting.Add(posorder.PosdeliverypointExternalId, 1);
                    newTable = true; // Always close a new table, might be filled from a previous session.
                }
                else
                {
                    Untill2Connector.TrackOrdersPerTableForTesting[posorder.PosdeliverypointExternalId] += 1;
                }

                // If testing verify amount of order placed, when more than 15 pay bill
                // this to improve testing speed and reducre memory load
                if (Untill2Connector.TrackOrdersPerTableForTesting[posorder.PosdeliverypointExternalId] > 25 || newTable)
                {
                    if (this.testModepaymentMethodId == 0)
                    {
                        this.WriteToLogExtended("SaveOrder", "Can't reset orders in TestMode - No PaymentmethodId specified");
                    }
                    else
                    {
                        int timeoutWas = this.Webservice.Timeout;
                        this.Webservice.Timeout = (300 * 1000); // Can take VERY long.
                        var closeOrderResponse = this.CloseOrderForTesting(Convert.ToInt32(posorder.PosdeliverypointExternalId));
                        if (closeOrderResponse.ReturnCode == (int)WebserviceResultCode.Ok)
                        {
                            this.WriteToLogNormal("SaveOrder", "Closed Order on Table: '{0}'", posorder.PosdeliverypointExternalId);
                            Untill2Connector.TrackOrdersPerTableForTesting[posorder.PosdeliverypointExternalId] = 0;
                        }
                        else
                        {
                            this.WriteToLogNormal("SaveOrder", "Close Order Failed for Table: '{0}'", posorder.PosdeliverypointExternalId);
                        }
                        this.Webservice.Timeout = timeoutWas;
                    }
                }
            }

            this.WriteToLogExtended("SaveOrder", "End: '{0}'", status);

            return status;
        }

        bool IPOSConnector.PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            return true;
        }

        bool IPOSConnector.PrintServiceRequestReceipt(string serviceDescription)
        {
            return true;
        }

        bool IPOSConnector.PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            return true;
        }

        bool IPOSConnector.NotifyLockedDeliverypoint()
        {
            return true;
        }

        private void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Debug("Untill2Connector - {0} - {1}", methodName, string.Format(message, args));
        }

        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("Untill2Connector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public void WriteToUntillLog(string contents, params object[] args)
        {
            Debug.WriteLine(string.Format(contents, args));

            // Create the Logs directory if it doesn't exists yet
            string dir = Untill2Connector.CreateLogDirectory("Logs");

            DateTime now = DateTime.Now;

            // Set up a filestream
            FileStream fs = new FileStream(Path.Combine(dir, string.Format("{0}{1}{2}-Untill2.log", now.Year.ToString("0000"), now.Month.ToString("00"), now.Day.ToString("00"))), FileMode.OpenOrCreate, FileAccess.Write);

            // Set up a streamwriter for adding text
            StreamWriter sw = new StreamWriter(fs);

            // Find the end of the underlying filestream
            sw.BaseStream.Seek(0, SeekOrigin.End);

            // Add the text
            try
            {
                sw.Write(string.Format(contents, args));
            }
            catch
            {
                sw.WriteLine("String.Format failed");
                sw.WriteLine(string.Format("{0}:{1}:{2}", now.Hour.ToString("00"), now.Minute.ToString("00"), now.Second.ToString("00")));
                sw.WriteLine(contents);
                sw.WriteLine("Args: ", StringUtil.CombineWithSeperator(", ", args));
            }
            // Add the text to the underlying filestream
            sw.Flush();
            // Close the writer
            sw.Close();
        }

        /// <summary>
        /// Creates a directory relative to the application directory
        /// </summary>
        /// <param name="relativeDir">The directory relative to the application directory</param>
        /// <returns>The full path to the relative directory</returns>
        private static string CreateLogDirectory(string relativeDir)
        {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dir = Path.Combine(directory, relativeDir);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Untill webservice
        /// </summary>
        public Untill2Webservice.ITPAPIPOSservice Webservice
        {
            get
            {
                if (this.webservice == null)
                {
                    this.webservice = new Untill2Webservice.ITPAPIPOSservice();
                    this.webservice.Url = this.webserviceUrl;

                    if (TestUtil.IsPcGabriel)
                    {
                        this.Webservice.Timeout = 100000;
                    }
                }
                return this.webservice;
            }
        }

        /// <summary>
        /// Gets or sets the hasError
        /// </summary>
        public bool HasError
        {
            get
            {
                return this.hasError;
            }
            set
            {
                this.hasError = value;
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
