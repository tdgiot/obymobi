﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// OrderitemType enums
    /// </summary>
    public enum OrderitemType : int
    {
        /// <summary>
        /// Normal
        /// </summary>
        Normal = 0,
        /// <summary>
        /// Must have options
        /// </summary>
        MustHaveOption = 1,
        /// <summary>
        /// Free options
        /// </summary>
        FreeOption = 2,
        /// <summary>
        /// Supplement
        /// </summary>
        Supplement = 3,
        /// <summary>
        /// Condiment
        /// </summary>
        Condiment = 4,
        /// <summary>
        /// Menu item
        /// </summary>
        MenuItem = 5,
        /// <summary>
        /// Article message
        /// </summary>
        ArticleMessage = 6
    }
}
