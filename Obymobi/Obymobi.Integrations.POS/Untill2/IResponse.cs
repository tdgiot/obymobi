﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// IResponse interface
    /// </summary>
    public interface IResponse
    {
        /// <summary>
        /// Gets or sets the return code.
        /// </summary>
        /// <value>
        /// The return code.
        /// </value>
        int ReturnCode { get; set; }
        /// <summary>
        /// Gets or sets the return message.
        /// </summary>
        /// <value>
        /// The return message.
        /// </value>
        string ReturnMessage { get; set; }
    }
}
