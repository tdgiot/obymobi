﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// PaymentKind enums
    /// </summary>
    public enum PaymentKind : int
    {
        /// <summary>
        /// Cash
        /// </summary>
        Cash = 0,
        /// <summary>
        /// Card
        /// </summary>
        Card = 1,
        /// <summary>
        /// OnAccount
        /// </summary>
        OnAccount = 2,
        /// <summary>
        /// Room
        /// </summary>
        Room = 3,
        /// <summary>
        /// Cheque
        /// </summary>
        Cheque = 4,
        /// <summary>
        /// Hash
        /// </summary>
        Hash = 5,
        /// <summary>
        /// ServiceCharge
        /// </summary>
        ServiceCharge = 6,
        /// <summary>
        /// Discount
        /// </summary>
        Discount = 7,
        /// <summary>
        /// SmartCard
        /// </summary>
        SmartCard = 8
    }
}
