﻿using Dionysos;
using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// Untill2ConfigurationAdapter class
    /// </summary>
    public class Untill2ConfigurationAdapter : IConfigurationAdapter
    {

        #region Methods
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.User = terminal.PosValue2;
            this.Password = terminal.PosValue3;
            this.Pricegroup = terminal.PosValue4;
            this.TestMode = terminal.PosValue5.IsNullOrWhiteSpace() ? false : bool.Parse(terminal.PosValue5);
            this.SalesArea = terminal.PosValue6;
            this.TestModePaymentmethodId = terminal.PosValue7.IsNullOrWhiteSpace() ? 0 : long.Parse(terminal.PosValue7);
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.User = terminal.PosValue2;
            this.Password = terminal.PosValue3;
            this.Pricegroup = terminal.PosValue4;
            this.TestMode = terminal.PosValue5.IsNullOrWhiteSpace() ? false : bool.Parse(terminal.PosValue5);
            this.SalesArea = terminal.PosValue6;
            this.TestModePaymentmethodId = terminal.PosValue7.IsNullOrWhiteSpace() ? 0 : long.Parse(terminal.PosValue7);
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.WebserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2WebserviceUrl);
            this.User = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2WebserviceUser);
            this.Password = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2WebservicePassword);
            this.Pricegroup = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2Pricegroup);
            this.TestMode = Dionysos.ConfigurationManager.GetBool(POSConfigurationConstants.Untill2TestMode);
            this.SalesArea = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2SalesArea);
            this.TestModePaymentmethodId = Dionysos.ConfigurationManager.GetLong(POSConfigurationConstants.Untill2TestModePaymentmethodId);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.Untill2WebserviceUrl, this.WebserviceUrl);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.Untill2WebserviceUser, this.User);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.Untill2WebservicePassword, this.Password);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.Untill2Pricegroup, this.Pricegroup);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.Untill2TestMode, this.TestMode);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.Untill2SalesArea, this.SalesArea);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.Untill2TestModePaymentmethodId, this.TestModePaymentmethodId);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.User;
            terminal.PosValue3 = this.Password;
            terminal.PosValue4 = this.Pricegroup;
            terminal.PosValue5 = this.TestMode.ToString();
            terminal.PosValue6 = this.SalesArea;
            terminal.PosValue7 = this.TestModePaymentmethodId.ToString();
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.User;
            terminal.PosValue3 = this.Password;
            terminal.PosValue4 = this.Pricegroup;
            terminal.PosValue5 = this.TestMode.ToString();
            terminal.PosValue6 = this.SalesArea;
            terminal.PosValue7 = this.TestModePaymentmethodId.ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the webservice URL.
        /// </summary>
        /// <value>
        /// The webservice URL.
        /// </value>
        public string WebserviceUrl { get; set; }
        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public string User { get; set; }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }
        /// <summary>
        /// Gets or sets the pricegroup.
        /// </summary>
        /// <value>
        /// The pricegroup.
        /// </value>
        public string Pricegroup { get; set; }
        /// <summary>
        /// Gets or sets the sales area.
        /// </summary>
        /// <value>
        /// The sales area.
        /// </value>
        public string SalesArea { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [test mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [test mode]; otherwise, <c>false</c>.
        /// </value>
        public bool TestMode { get; set; }
        /// <summary>
        /// Gets or sets the test mode paymentmethod id.
        /// </summary>
        /// <value>
        /// The test mode paymentmethod id.
        /// </value>
        public long TestModePaymentmethodId { get; set; }

        #endregion
    }
}
