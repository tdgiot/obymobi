﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.POS.Interfaces;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data;
using Dionysos;

namespace Obymobi.Logic.POS.Untill2
{
    /// <summary>
    /// Untill2Helper class
    /// </summary>
    public class Untill2Helper : PosHelper
    {
        /// <summary>
        /// Untill2HelperErrors enums
        /// </summary>
        public enum Untill2HelperErrors : int
        {
            /// <summary>
            /// Menu product aren't supported yet
            /// </summary>
            IsMenuProductsAreNotYetSupport = 200,
            /// <summary>
            /// Manual price products aren't supported yet
            /// </summary>
            IsManualPriceProductsAreNotYetSupport = 201,
            /// <summary>
            /// POS products can't be imported as a product
            /// </summary>
            PosproductIsMenuCantBeImportedAsAProduct = 202,
            /// <summary>
            /// POS product with manual price can't be imported as a product
            /// </summary>
            PosproductIsManualPriceCantBeImportedAsAProduct = 203,
            /// <summary>
            /// The field value has an invalid format
            /// </summary>
            FieldValueIsInvalidFormatted = 204,
            /// <summary>
            /// Multiple terminals aren't supported
            /// </summary>
            MultipleTerminalsNotSupported = 205,
            /// <summary>
            /// Configuration is incomplete
            /// </summary>
            ConfigurationIncomplete = 206,
            /// <summary>
            /// Alterations aren't found in POS integration information
            /// </summary>
            AlterationNotFoundInPosIntegrationInformation = 207
        }

        /// <summary>
        /// Get's called in the Product Validator OnSave for product's that are linked to PosProduct and only when
        /// the PosproductId is changed.
        /// </summary>
        public override void ProductValidateEntityBeforeSave(ProductEntity product)
        {
            if (product.PosproductId.HasValue)
            {
                bool isMenu, isManualPrice;

                if (!bool.TryParse(product.PosproductEntity.FieldValue1, out isMenu))
                {
                    throw new ObymobiException(Untill2HelperErrors.FieldValueIsInvalidFormatted, "FieldValue1: '{0}', PosproductId: '{1}', PosproductName: '{2}'",
                        product.PosproductEntity.FieldValue1, product.PosproductId, product.PosproductEntity.Name);
                }
                else if (!bool.TryParse(product.PosproductEntity.FieldValue2, out isManualPrice))
                {
                    throw new ObymobiException(Untill2HelperErrors.FieldValueIsInvalidFormatted, "FieldValue2: '{0}', PosproductId: '{1}', PosproductName: '{2}'",
                        product.PosproductEntity.FieldValue1, product.PosproductId, product.PosproductEntity.Name);
                }
                else if (isMenu)
                {
                    throw new ObymobiException(Untill2HelperErrors.IsMenuProductsAreNotYetSupport, "ProductId: '{0}', ProductName: '{1}' PosproductId: '{2}', PosproductName: '{3}'",
                        product.ProductId, product.Name, product.PosproductId, product.PosproductEntity.Name);
                }
                else if (isManualPrice)
                {
                    throw new ObymobiException(Untill2HelperErrors.IsManualPriceProductsAreNotYetSupport, "ProductId: '{0}', ProductName: '{1}' PosproductId: '{2}', PosproductName: '{3}'",
                        product.ProductId, product.Name, product.PosproductId, product.PosproductEntity.Name);
                }
            }
            return;
        }

        /// <summary>
        /// Get's called in the Posproduct Validator
        /// </summary>
        public override void PosproductValidateEntityBeforeSave(PosproductEntity posproduct)
        {
            // Some posproduct types are not support, therefore can't be saved when linked to an imported product
            if (posproduct.ProductCollection.Count > 0)
            {
                bool isMenu, isManualPrice;

                if (!bool.TryParse(posproduct.FieldValue1, out isMenu))
                {
                    throw new ObymobiException(Untill2HelperErrors.FieldValueIsInvalidFormatted, "FieldValue1: '{0}', PosproductId: '{1}', PosproductName: '{2}'",
                        posproduct.FieldValue1, posproduct.PosproductId, posproduct.Name);
                }
                else if (!bool.TryParse(posproduct.FieldValue2, out isManualPrice))
                {
                    throw new ObymobiException(Untill2HelperErrors.FieldValueIsInvalidFormatted, "FieldValue2: '{0}', PosproductId: '{1}', PosproductName: '{2}'",
                        posproduct.FieldValue1, posproduct.PosproductId, posproduct.Name);
                }
                else if (isMenu)
                {
                    throw new ObymobiException(Untill2HelperErrors.IsMenuProductsAreNotYetSupport, "PosproductId: '{0}', PosproductName: '{1}'",
                        posproduct.PosproductId, posproduct.Name);
                }
                else if (isManualPrice)
                {
                    throw new ObymobiException(Untill2HelperErrors.IsManualPriceProductsAreNotYetSupport, "PosproductId: '{0}', PosproductName: '{1}'",
                        posproduct.PosproductId, posproduct.Name);
                }
            }
        }

        /// <summary>
        /// Retrieve all importable posproducts (this might exclude some Posproducts that are not
        /// supported)
        /// </summary>
        /// <param name="company">The company.</param>
        /// <param name="prefetchProducts">if set to <c>true</c> prefetch products.</param>
        /// <param name="hideImportedProducts">if set to <c>true</c> [hide imported products].</param>
        /// <returns></returns>
        public override Data.CollectionClasses.PosproductCollection RetrieveAllImportablePosproducts(CompanyEntity company, bool prefetchProducts, bool hideImportedProducts)
        {
            PosproductCollection posproducts = new PosproductCollection();
            RelationCollection joins = new RelationCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductFields.CompanyId == company.CompanyId);
            filter.Add(PosproductFields.FieldValue1 % "false");
            filter.Add(PosproductFields.FieldValue2 % "false");

            if (hideImportedProducts)
            {
                joins.Add(PosproductEntity.Relations.ProductEntityUsingPosproductId, JoinHint.Left);
                filter.Add(ProductFields.ProductId == DBNull.Value);
            }

            PrefetchPath pathPosproducts = new PrefetchPath(EntityType.PosproductEntity);
            pathPosproducts.Add(PosproductEntity.PrefetchPathProductCollection);

            posproducts.GetMulti(filter, 0, null, joins, pathPosproducts);

            return posproducts;
        }

        /// <summary>
        /// Verify if the Settings for the POS integration are complete and ready for an export to a config file
        /// </summary>
        /// <param name="terminal"></param>
        public override void VerifySettings(TerminalEntity terminal)
        {
            base.VerifySettings(terminal);

            Untill2ConfigurationAdapter config = new Untill2ConfigurationAdapter();

            config.ReadFromTerminalEntity(terminal);

            if (config.User.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(Untill2HelperErrors.ConfigurationIncomplete, "Missing Field: User");
            }
            else if (config.Password.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(Untill2HelperErrors.ConfigurationIncomplete, "Missing Field: Password");
            }
            else if (config.WebserviceUrl.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(Untill2HelperErrors.ConfigurationIncomplete, "Missing Field: WebserviceUrl");
            }
            else if (config.SalesArea.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(Untill2HelperErrors.ConfigurationIncomplete, "Missing Field: SalesArea");
            }
        }

        /// <summary>
        /// Prepares the orderitem created for a Alterationitem for pos by the supplied posIntegrationInformation.
        /// </summary>
        /// <param name="orderitem">The orderitem.</param>
        /// <param name="alteration">The alteration.</param>
        /// <param name="posIntegrationInformation">The pos integration information.</param>
        public override void PrepareOrderitemFromAlterationitemForPos(Orderitem orderitem, Alteration alteration, PosIntegrationInformation posIntegrationInformation)
        {
            var product = posIntegrationInformation.Products.FirstOrDefault(p => p.Alterations.FirstOrDefault(a => a.AlterationId == alteration.AlterationId) != null);
            if (product != null)
            {
                var alterationPosinformation = product.Alterations.FirstOrDefault(a => a.AlterationId == alteration.AlterationId);
                orderitem.FieldValue1 = alterationPosinformation.Posalteration.FieldValue1;
            }
            else
            {
                throw new ObymobiException(Untill2HelperErrors.AlterationNotFoundInPosIntegrationInformation, "Alteration id: {0} name: {1}", alteration.AlterationId, alteration.Name);
            }
        }
    }
}
