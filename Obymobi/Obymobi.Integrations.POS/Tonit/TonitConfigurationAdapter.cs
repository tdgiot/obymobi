﻿using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Logic.POS.Tonit
{
    /// <summary>
    /// TonitConfigurationAdapter class
    /// </summary>
    public class TonitConfigurationAdapter : IConfigurationAdapter
    {
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.WebserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.TonitWebserviceUrl);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.TonitWebserviceUrl, this.WebserviceUrl);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the webservice URL.
        /// </summary>
        /// <value>
        /// The webservice URL.
        /// </value>
        public string WebserviceUrl { get; set; }

        #endregion
    }
}
