﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.Logic.POS
{
    /// <summary>
    /// TonitConnector class
    /// </summary>
    public class TonitConnector : IPOSConnector
    {
        #region Fields

        private TonitWebservice.IECServer2service webservice = null;
        private string webserviceUrl = string.Empty;

        private string posproductsString = string.Empty;
        private string posdeliverypointsString = string.Empty;

        private bool hasError = false;
        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.POS.TonitConnector
        /// </summary>
        public TonitConnector()
        {
            this.webserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.TonitWebserviceUrl);
        }

        #endregion

        #region Methods

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posproduct instances</returns>
        public Posproduct[] GetPosproducts()
        {
            List<Posproduct> posproducts = new List<Posproduct>();

            string posproductsString = string.Empty;
            if (this.posproductsString.Length == 0)
            {
                try
                {
                    posproductsString = this.Webservice.GetAlleArtikelen();
                }
                catch
                {
                }
            }
            else
                posproductsString = this.posproductsString;

            if (posproductsString.Length > 0)
            {
                posproductsString = posproductsString.Replace("<OK>", "");
                posproductsString = posproductsString.Replace("</OK>", "");

                string[] poscategoryArray = posproductsString.Split(new string[] { "<G>" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < poscategoryArray.Length; i++)
                {
                    string[] posproductArray = poscategoryArray[i].Split(new string[] { "<A>" }, StringSplitOptions.RemoveEmptyEntries);

                    if (posproductArray.Length > 0)
                    {
                        string externalPoscategoryId = posproductArray[0];

                        for (int j = 1; j < posproductArray.Length; j++)
                        {
                            Posproduct posproduct = new Posproduct();
                            posproduct.ExternalPoscategoryId = externalPoscategoryId;

                            string[] posproductElements = posproductArray[j].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                            for (int k = 0; k < posproductElements.Length; k++)
                            {
                                string posproductElement = posproductElements[k];

                                switch (k)
                                {
                                    case 0:
                                        // id
                                        posproduct.ExternalId = posproductElement;
                                        break;
                                    case 1:
                                        // naam
                                        posproduct.Name = posproductElement;
                                        break;
                                    case 2:
                                        // prijs
                                        decimal priceDecimal = 0;
                                        if (Decimal.TryParse(posproductElement, out priceDecimal))
                                            posproduct.PriceIn = (int)(priceDecimal * 100m);
                                        break;
                                    case 3:
                                        // isOptie
                                        break;
                                    case 4:
                                        // artikelCode
                                        break;
                                    case 5:
                                        // openPrijs
                                        break;
                                    case 6:
                                        // keuzes
                                        break;
                                }
                            }

                            posproducts.Add(posproduct);
                        }
                    }
                }
            }

            return posproducts.ToArray();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Poscategory instances</returns>
        public Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();

            string posproductsString = string.Empty;
            if (this.posproductsString.Length == 0)
            {
                try
                {
                    posproductsString = this.Webservice.GetAlleArtikelen();
                }
                catch
                {
                }
            }
            else
                posproductsString = this.posproductsString;

            if (posproductsString.Length > 0)
            {
                posproductsString = posproductsString.Replace("<OK>", "");
                posproductsString = posproductsString.Replace("</OK>", "");

                string[] poscategoryArray = posproductsString.Split(new string[] { "<G>" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < poscategoryArray.Length; i++)
                {
                    string[] posproductArray = poscategoryArray[i].Split(new string[] { "<A>" }, StringSplitOptions.RemoveEmptyEntries);

                    if (posproductArray.Length > 0)
                    {
                        Poscategory poscategory = new Poscategory();
                        poscategory.ExternalId = posproductArray[0];
                        poscategory.Name = posproductArray[0];

                        poscategories.Add(poscategory);
                    }
                }
            }

            return poscategories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances</returns>
        public Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            string posdeliverypointsString = string.Empty;
            if (this.posdeliverypointsString.Length == 0)
            {
                try
                {
                    posdeliverypointsString = this.Webservice.GetAlleTafels();
                }
                catch
                {
                }
            }
            else
                posdeliverypointsString = this.posdeliverypointsString;

            if (posdeliverypointsString.Length > 0)
            {
                posdeliverypointsString = posdeliverypointsString.Replace("<OK>", "");
                posdeliverypointsString = posdeliverypointsString.Replace("</OK>", "");

                string[] posdeliverypointGroupArray = posdeliverypointsString.Split(new string[] { "<L>" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < posdeliverypointGroupArray.Length; i++)
                {
                    string[] posdeliverypointArray = posdeliverypointGroupArray[i].Split(new string[] { "<T>" }, StringSplitOptions.RemoveEmptyEntries);

                    if (posdeliverypointArray.Length > 0)
                    {
                        string posdeliverypointgroup = posdeliverypointArray[0];

                        for (int j = 1; j < posdeliverypointArray.Length; j++)
                        {
                            Posdeliverypoint posdeliverypoint = new Posdeliverypoint();
                            posdeliverypoint.ExternalPosdeliverypointgroupId = posdeliverypointgroup;

                            string[] posdeliverypointElements = posdeliverypointArray[j].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                            for (int k = 0; k < posdeliverypointElements.Length; k++)
                            {
                                string posdeliverypointElement = posdeliverypointElements[k];

                                switch (k)
                                {
                                    case 0:
                                        // id
                                        posdeliverypoint.ExternalId = posdeliverypointElement;
                                        break;
                                    case 1:
                                        // naam
                                        posdeliverypoint.Name = posdeliverypointElement;
                                        break;
                                }
                            }

                            posdeliverypoints.Add(posdeliverypoint);
                        }
                    }
                }
            }


            return posdeliverypoints.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances</returns>
        public Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();

            string posdeliverypointsString = string.Empty;
            if (this.posdeliverypointsString.Length == 0)
            {
                try
                {
                    posdeliverypointsString = this.Webservice.GetAlleTafels();
                }
                catch
                {
                }
            }
            else
                posdeliverypointsString = this.posdeliverypointsString;

            if (posdeliverypointsString.Length > 0)
            {
                posdeliverypointsString = posdeliverypointsString.Replace("<OK>", "");
                posdeliverypointsString = posdeliverypointsString.Replace("</OK>", "");

                string[] posdeliverypointGroupArray = posdeliverypointsString.Split(new string[] { "<L>" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < posdeliverypointGroupArray.Length; i++)
                {
                    string[] posdeliverypointArray = posdeliverypointGroupArray[i].Split(new string[] { "<T>" }, StringSplitOptions.RemoveEmptyEntries);

                    if (posdeliverypointArray.Length > 0)
                    {
                        Posdeliverypointgroup posdeliverypointgroup = new Posdeliverypointgroup();
                        posdeliverypointgroup.ExternalId = posdeliverypointArray[0];
                        posdeliverypointgroup.Name = posdeliverypointArray[0];

                        posdeliverypointgroups.Add(posdeliverypointgroup);
                    }
                }
            }

            return posdeliverypointgroups.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>An Obymobi.Logic.Model.Posorder instance</returns>
        public Posorder GetPosorder(string deliverypointNumber)
        {
            Posorder posorder = null;

            string posorderString = string.Empty;
            int deliveryPointNumberInt = -1;

            // Check whether the specified deliverypoint number is an integer
            // If so, get the current order for the specified deliverypoint number
            if (Int32.TryParse(deliverypointNumber, out deliveryPointNumberInt))
            {
                try
                {
                    posorderString = this.Webservice.GetBestelling(deliveryPointNumberInt);
                }
                catch
                {
                }
            }

            if (posorderString.Length > 0)
            {
                // Parse the posorder from the string

            }

            return posorder;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>True if the save was succesful, False if not</returns>
        public OrderProcessingError SaveOrder(Posorder posorder)
        {
            OrderProcessingError status = OrderProcessingError.None;

            StringBuilder orderString = new StringBuilder();

            // <T>1;1<P>-1<A>1;2;120;Patat<L>

            // Tafel
            orderString.Append("<T>");
            orderString.AppendFormat("{0};1", posorder.PosdeliverypointExternalId);

            // Personeelslid
            orderString.Append("<P>");
            orderString.Append("-1");

            // Orderitems
            for (int i = 0; i < posorder.Posorderitems.Length; i++)
            {
                Posorderitem posorderitem = posorder.Posorderitems[i];
                orderString.AppendFormat("<A>{0};{1};{2};{3}", posorderitem.PosproductExternalId, posorderitem.Quantity, posorderitem.ProductPriceIn, posorderitem.Description);
            }

            int length = orderString.Length;
            orderString.Append("<L>" + length);

            string result = string.Empty;

            try
            {
                result = this.Webservice.VerwerkBestelling(orderString.ToString());
            }
            catch
            {
            }

            if (result.Equals("<OK></OK>", StringComparison.OrdinalIgnoreCase))
            {
                // Good
            }
            else
                throw new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, "No further details...");

            return status;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <returns>True if printing the confirmation receipt was succesful, False if not</returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            bool success = true;

            try
            {
                this.Webservice.ObyMobi_BevestigingsBon(ref confirmationCode, ref name, ref phonenumber, ref deliverypointNumber);
            }
            catch
            {
                success = false;
            }

            return success;
        }

        ///// <summary>
        ///// Print an Obymobi status report
        ///// </summary>
        ///// <returns>True if printing the status report was succesful, False if not</returns>
        //public bool PrintStatusReport(Obymobi.Logic.Status.TerminalStatus terminalStatus)
        //{
        //    bool success = true;

        //    bool connectedToInternet = terminalStatus.Internet;
        //    bool connectedToWebservice = terminalStatus.Webservice;
        //    bool connectedToDatabase = terminalStatus.Database;
        //    bool canPrint = terminalStatus.Printer;
        //    bool configurationSpecified = terminalStatus.ConfigurationSpecified;
        //    DateTime checkDate = DateTime.Now;
        //    DateTime printDate = DateTime.Now;
        //    string notes = string.Empty;

        //    try
        //    {
        //        this.Webservice.ObyMobi_StatusRapport(ref connectedToInternet, ref connectedToWebservice, ref connectedToDatabase, ref canPrint, ref configurationSpecified, ref checkDate, ref printDate, ref notes);
        //    }
        //    catch
        //    {
        //        success = false;
        //    }

        //    return success;
        //}

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>True if printing the service request receipt was successful, False if not</returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>True if printing the checkout request was sucessful, False if not</returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            return true;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Tonit webservice
        /// </summary>
        public TonitWebservice.IECServer2service Webservice
        {
            get
            {
                if (this.webservice == null)
                {
                    this.webservice = new Obymobi.Logic.TonitWebservice.IECServer2service();
                    this.webservice.Url = this.webserviceUrl;
                }
                return this.webservice;
            }
        }

        /// <summary>
        /// Gets or sets the url of the Tonit webservice
        /// </summary>
        public string WebserviceUrl
        {
            get
            {
                return this.webserviceUrl;
            }
            set
            {
                this.webserviceUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether an error occurred
        /// </summary>
        public bool HasError
        {
            get
            {
                return this.hasError;
            }
            set
            {
                this.hasError = value;
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
