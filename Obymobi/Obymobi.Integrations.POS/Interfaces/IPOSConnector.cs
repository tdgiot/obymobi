﻿using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS.Interfaces
{
    /// <summary>
    /// IPOSConnector interface
    /// </summary>
    public interface IPOSConnector
    {
        /// <summary>
        /// Logic custom to the POS Connector required to prepare for a synchronisation
        /// </summary>
        void PrePosSynchronisation();

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posproduct instances</returns>
        Posproduct[] GetPosproducts();

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Poscategory instances</returns>
        Poscategory[] GetPoscategories();

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances</returns>
        Posdeliverypointgroup[] GetPosdeliverypointgroups();

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances</returns>
        Posdeliverypoint[] GetPosdeliverypoints();

        /// <summary>
        /// Logic custom to the POS Connector required after the synchronisation
        /// </summary>
        void PostPosSynchronisation();

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>An Obymobi.Logic.Model.Posorder instance</returns>
        Posorder GetPosorder(string deliverypointNumber);

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>True if the save was succesful, False if not</returns>
        OrderProcessingError SaveOrder(Posorder posorder);

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <returns>True if printing the confirmation receipt was succesful, False if not</returns>
        bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber);

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>True if printing the service request receipt was successful, False if not</returns>
        bool PrintServiceRequestReceipt(string serviceDescription);

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>True if printing the checkout request was sucessful, False if not</returns>
        bool PrintCheckoutRequestReceipt(string checkoutDescription);

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>True if notifying the locked deliverypoint was successful, False if not</returns>
        bool NotifyLockedDeliverypoint();

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        string ErrorMessage { get; set; }
    }
}
