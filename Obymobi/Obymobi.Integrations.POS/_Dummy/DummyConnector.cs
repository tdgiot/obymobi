﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Integrations.POS.Generic;
using Obymobi.Integrations.POS.Interfaces;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS._Dummy
{
    /// <summary>
    /// DummyConnector
    /// </summary>
    public class DummyConnector : PosConnectorBase, IPOSConnector
    {
        #region Fields

        // Retrieve the settings from the config file
        // private string webserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.Untill2WebserviceUrl);

        // Handy to keep a sync report
        private StringBuilder syncReport = new StringBuilder();

        #endregion

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="DummyConnector"/> class.
        /// </summary>
        public DummyConnector()
        {
            this.WriteToLogExtended("Constructor", "Start");

            // Verify if all settings are set
            //if (this.webserviceUrl.IsNullOrWhiteSpace())
            //    this.WriteToLogNormal("Constructor", "Webservice url is empty");

            // Output the current config to the log:
            //this.WriteToLogNormal("Constructor", "WebserivceUrl: {0}", this.webserviceUrl);

            this.WriteToLogExtended("Constructor", "End");
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public override Posproduct[] GetPosproducts()
        {
            this.WriteToLogExtended("GetPosproducts", "Start");

            this.syncReport = new StringBuilder();
            this.syncReport.AppendFormatLine("\r\n\r\n");
            this.syncReport.AppendFormatLine("******** SYNC START ********");
            this.syncReport.AppendFormatLine("Sync started at: {0}", DateTime.Now.ToString());

            // Log the settings where you sync with:
            //this.syncReport.AppendFormatLine("SalesArea: {0}", this.salesArea);
            //this.syncReport.AppendFormatLine("PriceGroup: {0}", this.pricegroupName);
            List<Posproduct> products = new List<Posproduct>();

            try
            {
                // Do the logic, log extensively.

                this.syncReport.AppendFormatLine("Sync finished with success at: {0}", DateTime.Now.ToString());
                this.syncReport.AppendFormatLine("******** SYNC FINISH ********");
                this.WriteToPosConnectorLog("GetPosProducts", this.syncReport.ToString());

                this.WriteToLogExtended("GetPosproducts", "End - Success");

                return products.ToArray();
            }
            catch
            {
                this.WriteToLogExtended("GetPosproducts", "End - Failure");
                this.syncReport.AppendFormatLine("Sync finished with failure at: {0}", DateTime.Now.ToString());
                this.WriteToPosConnectorLog("GetPosProducts", this.syncReport.ToString());

                throw;
            }
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public override Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();

            // Retrieve the Categories from the POS

            return poscategories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public override Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();

            // Retrieve the DeliverypointGroups from the POS

            return posdeliverypointgroups.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public override Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            // Retrieve the Posdeliverypoints from the POS

            return posdeliverypoints.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public override Posorder GetPosorder(string deliverypointNumber)
        {
            // Implement when available
            return null;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public override OrderProcessingError SaveOrder(Posorder posorder)
        {
            this.WriteToLogExtended("SaveOrder", "Start");

            OrderProcessingError status = OrderProcessingError.None;

            this.WriteToLogExtended("SaveOrder", "End: '{0}'", status);

            return status;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public override bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public override bool PrintServiceRequestReceipt(string serviceDescription)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public override bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public override bool NotifyLockedDeliverypoint()
        {
            // Implement when available
            return true;
        }

        #endregion

        #region Properties


        /// <summary>
        /// Has error
        /// </summary>
        protected bool hasError = false;
        /// <summary>
        /// Gets or sets the hasError
        /// </summary>
        public bool HasError
        {
            get
            {
                return this.hasError;
            }
            set
            {
                this.hasError = value;
            }
        }

        /// <summary>
        /// errorMessage
        /// </summary>
        protected string errorMessage = string.Empty;
        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public override string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
