﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Integrations.Interfaces;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS._Dummy
{
    /// <summary>
    /// DummyConfigurationAdapter class
    /// </summary>
    public class DummyConfigurationAdapter : IConfigurationAdapter
    {
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Terminal terminal)
        {
            // this.WebserviceUrl = terminal.PosValue1;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            // this.WebserviceUrl = terminal.PosValue1;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            // this.WebserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.HoreCatWebserviceUrl);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            // Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.HoreCatWebserviceUrl, this.WebserviceUrl);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            // terminal.PosValue1 = this.WebserviceUrl;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Terminal terminal)
        {
            // terminal.PosValue1 = this.WebserviceUrl;
        }

        #region Properties

        // public string WebserviceUrl { get; set; }

        #endregion
    }
}
