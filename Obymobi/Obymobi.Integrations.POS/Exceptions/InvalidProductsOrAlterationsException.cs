﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Integrations.POSLogic.POS
{
    /// <summary>
    /// Exception class for when an order cannot be processed
    /// </summary>
    public class InvalidProductsOrAlterationsException : NonFatalSaveOrderException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="message"></param>
        public InvalidProductsOrAlterationsException(string message)
            : base(OrderProcessingError.PosErrorProductAndOrAlterationConfigurationInvalid, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public InvalidProductsOrAlterationsException(string message, params object[] args)
            : base(OrderProcessingError.PosErrorProductAndOrAlterationConfigurationInvalid, message, args)
        {
        }

        #endregion
    }
}
