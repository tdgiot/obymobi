﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Integrations.POSLogic.POS
{
    /// <summary>
    /// Exception class for when an order cannot be processed
    /// </summary>
    public class UnprocessableException : TechnicalException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnprocessableException"/> class.
        /// </summary>
        /// <param name="message"></param>
        public UnprocessableException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public UnprocessableException(string message, params object[] args)
            : base(message, args)
        {
        }

        #endregion
    }
}
