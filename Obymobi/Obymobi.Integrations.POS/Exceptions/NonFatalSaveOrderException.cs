﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Integrations.POSLogic.POS
{
    /// <summary>
    /// Exception class for when an order cannot be processed due to a connectivity issue that could be temporary
    /// </summary>
    public class NonFatalSaveOrderException : POSException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectivityException"/> class.
        /// </summary>
        /// <param name="errorType"></param>
        /// <param name="message"></param>
        public NonFatalSaveOrderException(OrderProcessingError errorType, string message)
            : base(errorType, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectivityException"/> class.
        /// </summary>
        /// <param name="errorType">Type of the error.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public NonFatalSaveOrderException(OrderProcessingError errorType, string message, params object[] args)
            : base(errorType, message, args)
        {
        }

        #endregion
    }
}
