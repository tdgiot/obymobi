﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Integrations.POSLogic.POS
{
    /// <summary>
    /// Exception class for when an order cannot be processed
    /// </summary>
    public class DeliverypointLockedException : NonFatalSaveOrderException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="message"></param>
        public DeliverypointLockedException(string message)
            : base(OrderProcessingError.PosErrorDeliverypointLocked, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public DeliverypointLockedException(string message, params object[] args)
            : base(OrderProcessingError.PosErrorDeliverypointLocked, message, args)
        {
        }

        #endregion
    }
}
