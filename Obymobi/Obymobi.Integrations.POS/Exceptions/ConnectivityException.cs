﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Integrations.POSLogic.POS
{
    /// <summary>
    /// Exception class for when an order cannot be processed due to a connectivity issue that could be temporary
    /// </summary>
    public class ConnectivityException : NonFatalSaveOrderException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectivityException"/> class.
        /// </summary>
        /// <param name="message"></param>
        public ConnectivityException(string message)
            : base(OrderProcessingError.PosErrorConnectivityProblem, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectivityException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public ConnectivityException(string message, params object[] args)
            : base(OrderProcessingError.PosErrorConnectivityProblem, message, args)
        {
        }

        #endregion
    }
}
