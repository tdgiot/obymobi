﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Dionysos;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.POS.Untill
{
    /// <summary>
    /// ArticleInfo class for the Untill Connector
    /// </summary>
    public class ArticleInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleInfo"/> class.
        /// </summary>
        public ArticleInfo()
        {
            this.OptionGroupCollection = new List<OptionGroup>();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="articleString">Single article string (without questionmarks)</param>
        public ArticleInfo(string articleString)
        {
            // Parse the contents
            this.OptionGroupCollection = new List<OptionGroup>();
            this.Parse(articleString);
        }

        /// <summary>
        /// Parses the specified article string.
        /// </summary>
        /// <param name="articleString">The article string.</param>
        /// <returns></returns>
        private bool Parse(string articleString)
        {
            string[] elements = articleString.Split(';');

            StringReader sr = new StringReader(articleString);

            // First get the number, price and name
            // 45;Jenever;Normaal;2,5;
            this.Nr = elements[0];
            this.Name = elements[1];
            this.Pricegroup = elements[2];

            // Parse price to decimal
            decimal priceHolder;
            elements[3] = elements[3].Replace(',', '.');
            if (Decimal.TryParse(elements[3], out priceHolder))
                this.Price = Decimal.Parse(elements[3], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            else
                throw new FunctionalException("ArticleInfo parsing error of price: '{0}' as a decimal value for article '{1}'.", elements[3], this.Name);

            OptionGroup.OptionGroupType groupType = OptionGroup.OptionGroupType.Supplement;
            int optionGroupToCreateForType = 0;
            int optionGroupCreatedForType = 0;
            int optionGroupOptionCount = 0;
            int parsingLevel = 0;
            int parsingElement = 4;
            if (elements.Length > 4)
            {
                // Parse groups:
                //0;1; // TYPE & AMOUNT				
                //    Optie Mixdrankjes;11; // OptionGroup Info
                //       29;Cola;2; // OPTIONS >>
                //       30;Cola light;2;
                // Level 0 = Type & Amount
                // Level 1 = OptionGroupInfo
                // Level 2 = OptionInfo
                try
                {
                    bool done = false;
                    while (!done)
                    {
                        if (parsingLevel == 0)
                        {
                            // Start a new Type & Amount group
                            // Parse the Group Type							
                            groupType = Convert.ToInt32(elements[parsingElement]).ToEnum<OptionGroup.OptionGroupType>();
                            parsingElement++;

                            // Parse the amount of groups of this type
                            optionGroupToCreateForType = Convert.ToInt32(elements[parsingElement]);
                            optionGroupCreatedForType = 0;
                            parsingElement++;

                            // Increaes parsing level to OptionGroupInfo level
                            parsingLevel++;
                        }
                        else if (parsingLevel == 1)
                        {
                            // Start a new OptionGroup
                            OptionGroup optionGroup = new OptionGroup();

                            // Name
                            optionGroup.Name = elements[parsingElement];
                            optionGroup.GroupType = groupType;
                            parsingElement++;

                            // Number of options to parse
                            optionGroupOptionCount = Convert.ToInt32(elements[parsingElement]);
                            parsingElement++;

                            this.OptionGroupCollection.Add(optionGroup);

                            // update the optionGroupCountForType
                            optionGroupCreatedForType++;

                            // Increase 
                            parsingLevel++;
                        }
                        else if (parsingLevel == 2)
                        {
                            // Create an option
                            Option option = new Option();

                            // Number
                            option.Nr = elements[parsingElement];
                            parsingElement++;

                            // Name
                            option.Name = elements[parsingElement];
                            parsingElement++;

                            // Price                            
                            elements[parsingElement] = elements[parsingElement].Replace(',', '.');
                            if (Decimal.TryParse(elements[parsingElement], out priceHolder))
                                option.Price = Decimal.Parse(elements[parsingElement], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                            else
                                throw new FunctionalException("ArticleInfo parsing error of Option: '{0}' as a decimal value for Option '{1}'.",
                                    elements[parsingElement], option.Name);

                            parsingElement++;

                            // Add the Option to the latest OptionGroup
                            var optionGroup = this.OptionGroupCollection[this.OptionGroupCollection.Count - 1];
                            optionGroup.OptionCollection.Add(option);

                            if (optionGroup.OptionCollection.Count == optionGroupOptionCount)
                            {
                                // We had all options, move up one parsing level
                                parsingLevel--;

                                // If we also had all groups for this Type, go up another level
                                if (optionGroupCreatedForType == optionGroupToCreateForType)
                                    parsingLevel--;
                            }
                        }

                        // Check if we're finished
                        if (parsingElement >= elements.Length)
                        {
                            done = true;
                        }
                    }
                }
                catch
                {
                    throw new FunctionalException("Parsing failed of Untill Article with input: {0}", articleString);
                }
            }

            return true;
        }

        /// <summary>
        /// Convert to posproduct
        /// </summary>
        /// <returns>Posproduct</returns>
        public Posproduct ToPosproduct()
        {
            // Generic product info
            Posproduct posproduct = new Posproduct();
            posproduct.ExternalId = this.Nr;
            posproduct.ExternalPoscategoryId = "-9999";
            posproduct.Name = this.Name;
            posproduct.PriceIn = this.Price;
            posproduct.VatTariff = 2;

            // Alterations
            List<Posalteration> alterations = new List<Posalteration>();
            foreach (var optionGroup in this.OptionGroupCollection)
            {
                Posalteration pa = new Posalteration();
                pa.Name = optionGroup.Name;
                //pa.ExternalId = string.Format("{0}-{1}-{2}", this.Nr, ((int)optionGroup.GroupType).ToString(), optionGroup.Name);
                pa.ExternalId = optionGroup.Name + "-" + ((int)optionGroup.GroupType).ToString();

                if (optionGroup.GroupType == OptionGroup.OptionGroupType.MustHave)
                    pa.MinOptions = 1;
                else
                    pa.MinOptions = 0;

                pa.FieldValue1 = ((int)optionGroup.GroupType).ToString();

                List<Posalterationoption> paos = new List<Posalterationoption>();
                foreach (var option in optionGroup.OptionCollection)
                {
                    var pao = new Posalterationoption();
                    pao.Name = option.Name;
                    pao.PriceIn = option.Price;
                    pao.ExternalId = pa.ExternalId + option.Nr;
                    pao.PosproductExternalId = option.Nr;
                    paos.Add(pao);
                }

                pa.Posalterationoptions = paos.ToArray();

                alterations.Add(pa);
            }

            posproduct.Posalterations = alterations.ToArray();

            return posproduct;
        }

        /// <summary>
        /// Gets or sets the nr.
        /// </summary>
        /// <value>The nr.</value>
        public string Nr { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price.</value>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the pricegroup.
        /// </summary>
        /// <value>The pricegroup.</value>
        public string Pricegroup { get; set; }

        /// <summary>
        /// Gets or sets the option group collection.
        /// </summary>
        /// <value>The option group collection.</value>
        public List<OptionGroup> OptionGroupCollection { get; set; }
    }
}
