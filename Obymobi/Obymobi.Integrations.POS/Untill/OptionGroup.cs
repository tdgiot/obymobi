﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Untill
{
	/// <summary>
	/// OptionGroup class
	/// </summary>
	public class OptionGroup
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="OptionGroup"/> class.
        /// </summary>
		public OptionGroup()
		{
			this.OptionCollection = new List<Option>();
		}

		/// <summary>
		/// Option Group Type
		/// </summary>
		public enum OptionGroupType : int
		{
			/// <summary>
			/// Must have options
			/// </summary>
			MustHave = 0,
			/// <summary>
			/// Supplement
			/// </summary>
			Supplement = 1,
			/// <summary>
			/// Condiment
			/// </summary>
			Condiment = 2
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the type of the option group.
		/// </summary>
		/// <value>The type of the option group.</value>
		public OptionGroupType GroupType { get; set; }

		/// <summary>
		/// Gets or sets the option collection.
		/// </summary>
		/// <value>The option collection.</value>
		public List<Option> OptionCollection { get; set; }
	}
}
