﻿using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Logic.POS.Untill
{
    /// <summary>
    /// UntillConfigurationAdapter class
    /// </summary>
    public class UntillConfigurationAdapter : IConfigurationAdapter
    {

        #region Methods
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.User = terminal.PosValue2;
            this.Password = terminal.PosValue3;
            this.Pricegroup = terminal.PosValue4;
            this.TestMode = bool.Parse(terminal.PosValue5);
            this.OtouchoTableNumber = terminal.PosValue6;
            this.DeliverypointProductPLU = terminal.PosValue7;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.User = terminal.PosValue2;
            this.Password = terminal.PosValue3;
            this.Pricegroup = terminal.PosValue4;
            this.TestMode = bool.Parse(terminal.PosValue5);
            this.OtouchoTableNumber = terminal.PosValue6;
            this.DeliverypointProductPLU = terminal.PosValue7;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.WebserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillWebserviceUrl);
            this.User = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillWebserviceUser);
            this.Password = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillWebservicePassword);
            this.Pricegroup = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillPricegroup);
            this.TestMode = Dionysos.ConfigurationManager.GetBool(POSConfigurationConstants.UntillTestMode);
            this.OtouchoTableNumber = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillOtouchoTableNumber);
            this.DeliverypointProductPLU = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillDeliverypointProductPLU);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UntillWebserviceUrl, this.WebserviceUrl);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UntillWebserviceUser, this.User);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UntillWebservicePassword, this.Password);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UntillPricegroup, this.Pricegroup);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UntillTestMode, this.TestMode);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UntillOtouchoTableNumber, this.OtouchoTableNumber);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UntillDeliverypointProductPLU, this.DeliverypointProductPLU);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.User;
            terminal.PosValue3 = this.Password;
            terminal.PosValue4 = this.Pricegroup;
            terminal.PosValue5 = this.TestMode.ToString();
            terminal.PosValue6 = this.OtouchoTableNumber;
            terminal.PosValue7 = this.DeliverypointProductPLU;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.User;
            terminal.PosValue3 = this.Password;
            terminal.PosValue4 = this.Pricegroup;
            terminal.PosValue5 = this.TestMode.ToString();
            terminal.PosValue6 = this.OtouchoTableNumber;
            terminal.PosValue7 = this.DeliverypointProductPLU;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the webservice URL.
        /// </summary>
        /// <value>
        /// The webservice URL.
        /// </value>
        public string WebserviceUrl { get; set; }
        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public string User { get; set; }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }
        /// <summary>
        /// Gets or sets the pricegroup.
        /// </summary>
        /// <value>
        /// The pricegroup.
        /// </value>
        public string Pricegroup { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [test mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [test mode]; otherwise, <c>false</c>.
        /// </value>
        public bool TestMode { get; set; }
        /// <summary>
        /// Gets or sets the otoucho table number.
        /// </summary>
        /// <value>
        /// The otoucho table number.
        /// </value>
        public string OtouchoTableNumber { get; set; }
        /// <summary>
        /// Gets or sets the deliverypoint product PLU.
        /// </summary>
        /// <value>
        /// The deliverypoint product PLU.
        /// </value>
        public string DeliverypointProductPLU { get; set; }

        #endregion
    }
}
