﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Loggers;
using Obymobi.Logic.Model;
using Dionysos;
using Obymobi.Enums;
using System.Diagnostics;
using System.IO;

namespace Obymobi.Logic.POS.Untill
{
    /// <summary>
    /// UntillConnector class
    /// </summary>
    public class UntillConnector : IPOSConnector
    {
        #region Fields

        private string webserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillWebserviceUrl);
        private string webserviceUsername = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillWebserviceUser);
        private string webservicePassword = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UntillWebservicePassword);
        private UntillWebservice.TPApi webservice;
        private static int saveOrderThreads = 0;
        private static Dictionary<string, int> TrackOrdersPerTableForTesting = new Dictionary<string, int>();
        private bool hasError = false;
        private string errorMessage = string.Empty;
        private int otouchoTableNumber = Dionysos.ConfigurationManager.GetInt(POSConfigurationConstants.UntillOtouchoTableNumber);
        private int deliverypointProductPLU = Dionysos.ConfigurationManager.GetInt(POSConfigurationConstants.UntillDeliverypointProductPLU);

        #endregion

        #region Methods

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.POS.TonitConnector
        /// </summary>
        public UntillConnector()
        {
            this.WriteToLogExtended("Constructor", "Start");

            if (this.webserviceUrl.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "Webservice url is empty");
            if (this.webserviceUsername.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "WebserviceUsername url is empty");
            if (this.webservicePassword.IsNullOrWhiteSpace())
                this.WriteToLogNormal("Constructor", "WebservicePassword url is empty");

            this.WriteToLogExtended("Constructor", "End");
        }

        #region Implemented Interface methods

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public Model.Posproduct[] GetPosproducts()
        {
            this.WriteToLogExtended("GetPosproducts", "Start");
            List<Posproduct> products = new List<Posproduct>();

            string result;
            //if (this.DoCommand("GetArticlesInfo", out result))
            //{
            //    this.WriteToLogExtended("GetArticlesInfo", result);
            //}

            // Retrieve the articles string			
            if (this.DoCommand("GetArticlesInfo", out result))
            {
                string[] articles = result.Split('?');

                // Start at 1 because first is product count
                for (int i = 1; i < articles.Length; i++)
                {
                    // Create an ArticleInfo (which parses the string to objects, incl. Alterations + Options)
                    ArticleInfo ai = new ArticleInfo(articles[i]);

                    // Skip wrong pricegroups
                    if (!ai.Pricegroup.Equals(ConfigurationManager.GetString(POSConfigurationConstants.UntillPricegroup), StringComparison.OrdinalIgnoreCase))
                        continue;

                    // Add the ArticleInfo as a PosProduct
                    products.Add(ai.ToPosproduct());
                }
            }

            this.WriteToLogExtended("GetPosproducts", "End");

            return products.ToArray();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public Model.Poscategory[] GetPoscategories()
        {
            // TODO: Not available in Webservice
            this.WriteToLogExtended("GetPoscategories", "Start");

            var array = new Poscategory[1];
            array[0] = new Poscategory();
            array[0].ExternalId = "-9999";
            array[0].Name = "Categorie onbekend";

            this.WriteToLogExtended("GetPoscategories", "End");
            return array;
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            this.WriteToLogExtended("GetPosdeliverypointgroups", "Start");
            // TODO: Not available in Webservice

            this.WriteToLogExtended("GetPosdeliverypointgroups", "End");
            return new Posdeliverypointgroup[0];
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            // TODO: Not available in Webservice
            this.WriteToLogExtended("GetPosdeliverypoints", "Start");
            this.WriteToLogExtended("GetPosdeliverypoints", "End");
            return new Posdeliverypoint[0];
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public Model.Posorder GetPosorder(string deliverypointNumber)
        {
            this.WriteToLogExtended("GetPosorder", "Start");
            this.WriteToLogExtended("GetPosorder", "End");
            throw new NotImplementedException();
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            OrderProcessingError processingError = OrderProcessingError.None;
            UntillConnector.saveOrderThreads++;

            this.WriteToLogExtended("SaveOrder", "Start - {0}", UntillConnector.saveOrderThreads);
            // Request format: [table number]?[table part]?[client name]?[Articles count]?[Article string 1]?[Article string 2]?....?[Article string X]

            StringBuilder orderString = new StringBuilder();
            orderString.Append("MakeOrder?");

            // Table number
            orderString.AppendFormat("{0}?", posorder.PosdeliverypointExternalId);

            // Table part (A)
            orderString.AppendFormat("{0}?", "a");

            // Client Name
            orderString.AppendFormat("{0}?", "0");

            orderString.AppendFormat("??");

            // Articles count
            orderString.AppendFormat("{0}?", posorder.Posorderitems.Count());

            // Articles per orderitem Format: [Article number];[Article namer];[Article price];[quantity];[Article type]
            for (int i = 0; i < posorder.Posorderitems.Count(); i++)
            {
                var orderItem = posorder.Posorderitems[i];

                // Set Article Number
                orderString.AppendFormat("{0};", orderItem.PosproductExternalId);

                // Set Article Name              
                orderItem.Description = orderItem.Description.Replace("&", " n ");
                orderItem.Description = orderItem.Description.Replace("\'", " ");
                orderItem.Description = orderItem.Description.Replace("\"", " ");
                orderItem.Description = orderItem.Description.Replace("`", " ");

                if (orderItem.Description.IsNumeric())
                    orderItem.Description = "Baan " + orderItem.Description;

                orderString.AppendFormat("{0};", orderItem.Description);

                // Set Article Price
                orderString.AppendFormat("0.0;");

                // Set Quantity
                orderString.AppendFormat("{0};", orderItem.Quantity);

                // Set Article Type
                if (orderItem.FieldValue1.IsNullOrWhiteSpace())
                    orderString.AppendFormat("0");
                else
                {
                    // Untill Requires a different type when you return
                    if (orderItem.FieldValue1 == "0")
                        // Normal Article
                        orderString.AppendFormat("1");
                    else if (orderItem.FieldValue1 == "1")
                        // Supplement
                        orderString.AppendFormat("3");
                    else if (orderItem.FieldValue1 == "2")
                        // Condiment
                        orderString.AppendFormat("4");
                    else if (orderItem.FieldValue1 == "3")
                        // Menu option
                        orderString.AppendFormat("5");
                    else
                        throw new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, string.Format("Unsupported OptionType '{0}' for OrderItem '{1}'", orderItem.FieldValue1, orderItem.PosorderitemId));
                }

                // Only append question markt when not last order item
                if (i < posorder.Posorderitems.Count() - 1)
                    orderString.Append("?");
            }

            // Write order to webservice
            string result;
            this.WriteToLogNormal("SaveOrder", "Send order to POS: {0}", orderString.ToString());
            this.WriteToLogExtended("SaveOrder", "End - {0}", UntillConnector.saveOrderThreads);
            UntillConnector.saveOrderThreads--;

            this.Webservice.Timeout = 60000;
            if (this.DoCommand(orderString.ToString(), out result))
            {
                //processingError = OrderStatus.Processed;
            }
            else
                processingError = OrderProcessingError.PosErrorUnspecifiedFatalPosProblem;

            // Count orders when testing
            if (ConfigurationManager.GetBool(POSConfigurationConstants.UntillTestMode))
            {
                if (!UntillConnector.TrackOrdersPerTableForTesting.ContainsKey(posorder.PosdeliverypointExternalId))
                    UntillConnector.TrackOrdersPerTableForTesting.Add(posorder.PosdeliverypointExternalId, 1);
                else
                    UntillConnector.TrackOrdersPerTableForTesting[posorder.PosdeliverypointExternalId] += 1;

                // If testing verify amount of order placed, when more than 15 pay bill
                // this to improve testing speed and reducre memory load
                if (UntillConnector.TrackOrdersPerTableForTesting[posorder.PosdeliverypointExternalId] > 25)
                {
                    this.Webservice.Timeout = 10000;
                    string command = string.Format("CloseOrder?{0}?a?1", posorder.PosdeliverypointExternalId);
                    if (this.DoCommand(command, out result))
                    {

                        UntillConnector.TrackOrdersPerTableForTesting[posorder.PosdeliverypointExternalId] = 0;
                    }

                    /*
                    else
                    {
                        command += "?1";
                        if(this.DoCommand(command, out result))
                            UntillConnector.TrackOrdersPerTableForTesting[posorder.PosdeliverypointExternalId] = 0;
                    }*/
                }
            }


            return processingError;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            DesktopLogger.Verbose("UntillConnector - NotifyLockedDeliverypoint - Begin");

            if (this.deliverypointProductPLU > 0 && this.otouchoTableNumber > 0)
            {
                // Request format: [table number]?[table part]?[client name]?[Articles count]?[Article string 1]?[Article string 2]?....?[Article string X]
                // Articles per orderitem Format: [Article number];[Article namer];[Article price];[quantity];[Article type]
                string orderFormat = string.Format("{0}?a?0?1?{0};Tafels sluiten s.v.p.;0;1;0", this.otouchoTableNumber, this.deliverypointProductPLU);
                string result;
                this.DoCommand(orderFormat, out result);
            }
            else
                DesktopLogger.Verbose("UntillConnector - NotifyLockedDeliverypoint - DeliverypointProductPLU and/or OtouchoTableNumber not available.");

            DesktopLogger.Verbose("UntillConnector - NotifyLockedDeliverypoint - End");

            return true;
        }

        /// <summary>
        /// Writes to log normal.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Debug("UntillConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes to log extended.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("UntillConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public void WriteToUntillLog(string contents, params object[] args)
        {
            Debug.WriteLine(string.Format(contents, args));

            // Create the Logs directory if it doesn't exists yet
            string dir = UntillConnector.CreateLogDirectory("Logs");

            DateTime now = DateTime.Now;

            // Set up a filestream
            FileStream fs = new FileStream(Path.Combine(dir, string.Format("{0}{1}{2}-Untill.log", now.Year.ToString("0000"), now.Month.ToString("00"), now.Day.ToString("00"))), FileMode.OpenOrCreate, FileAccess.Write);

            // Set up a streamwriter for adding text
            StreamWriter sw = new StreamWriter(fs);

            // Find the end of the underlying filestream
            sw.BaseStream.Seek(0, SeekOrigin.End);

            // Add the text
            try
            {
                sw.Write(string.Format(contents, args));
            }
            catch
            {
                sw.WriteLine("String.Format failed");
                sw.WriteLine(string.Format("{0}:{1}:{2}", now.Hour.ToString("00"), now.Minute.ToString("00"), now.Second.ToString("00")));
                sw.WriteLine(contents);
                sw.WriteLine("Args: ", StringUtil.CombineWithSeperator(", ", args));
            }
            // Add the text to the underlying filestream
            sw.Flush();
            // Close the writer
            sw.Close();
        }

        /// <summary>
        /// Creates a directory relative to the application directory
        /// </summary>
        /// <param name="relativeDir">The directory relative to the application directory</param>
        /// <returns>The full path to the relative directory</returns>
        private static string CreateLogDirectory(string relativeDir)
        {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dir = Path.Combine(directory, relativeDir);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        #endregion

        #region Webservice Helper methods

        /// <summary>
        /// Does the command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        private bool DoCommand(string command, out string result)
        {
            if (this.Webservice.Url.Contains("?wsdl", StringComparison.InvariantCultureIgnoreCase))
                throw new POSException(OrderProcessingError.PosErrorConfigurationIncorrect, "Untill url should never end with ?wsdl");

            bool toReturn = false;

            // Append authentication to the command
            command = string.Format("{0}?{1}?{2}", this.webserviceUsername, this.webservicePassword, command);

            // Query the webservice
            DateTime commandStart = DateTime.Now;
            TimeSpan commandDuration = TimeSpan.MinValue;
            try
            {

                this.WriteToLogExtended("DoCommand", command);

                // Perform command			
                if (TestUtil.IsPcGabriel)
                    this.Webservice.Timeout = 100000;

                commandStart = DateTime.Now;
                result = this.Webservice.doCmd(command);
                commandDuration = DateTime.Now - commandStart;
                toReturn = true;
            }
            catch (Exception ex)
            {
                commandDuration = DateTime.Now - commandStart;

                result = string.Empty;

                // Log error                
                this.errorMessage = string.Format("Exception with command '{0}' - Exception: {1} - (Exec time: {2}ms)", command, ex.Message, commandDuration.TotalMilliseconds.ToString());
                this.WriteToLogNormal("DoCommand", this.errorMessage);

                // POS Error Log
                string untillError = "\r\n\r\n" + DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "\r\n";
                untillError += ex.Message.Replace("eu.untill.rs.rscore.UntillServer$EUntillException:", string.Empty);
                untillError.Replace("(TTPAPIManager", "\r\n(TTPAPIManager");
                untillError += "\r\n " + command;
                untillError += "\r\n Execution time: " + commandDuration.TotalMilliseconds;
                untillError += "\r\n";

                this.WriteToUntillLog(untillError);

                // Set error info
                this.HasError = true;
                this.ErrorMessage = ex.Message;

                if (ex.Message.ToLower().Contains("already opened by waiter"))
                    throw new DeliverypointLockedException(this.errorMessage);
                else
                    throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, untillError);
            }

            // Log result			
            this.WriteToLogExtended("DoCommand", "Result: {0} - Exec.Time: {1}ms", result, commandDuration.TotalMilliseconds.ToString());

            // Return the result
            return toReturn;
        }

        #endregion

        #endregion

        #region Event Handlers



        #endregion

        #region Properties

        /// <summary>
        /// Gets the Untill webservice
        /// </summary>
        public UntillWebservice.TPApi Webservice
        {
            get
            {
                if (this.webservice == null)
                {
                    this.webservice = new Obymobi.Logic.UntillWebservice.TPApi();
                    this.webservice.Url = this.webserviceUrl;
                }
                return this.webservice;
            }
        }

        /// <summary>
        /// Gets or sets the hasError
        /// </summary>
        public bool HasError
        {
            get
            {
                return this.hasError;
            }
            set
            {
                this.hasError = value;
            }
        }

        /// <summary>
        /// Gets or sets the errorMessage
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
