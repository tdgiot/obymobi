﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Untill
{
	/// <summary>
	/// Option class, is part of a OptionGroup
	/// Holds ArticleInfo like information
	/// </summary>
	public class Option : ArticleInfo
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="Option"/> class.
		/// </summary>
		public Option()
		{ 

		}
	}
}
