﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Integrations.POS
{
    /// <summary>
    /// DummyPosHelper class
    /// </summary>
    public class DummyPosHelper : PosHelper
    {
        #region Generic

        /// <summary>
        /// Get's called in the Product Validator OnSave for product's that are linked to PosProduct and only when
        /// the PosproductId is changed.
        /// </summary>
        public override void ProductValidateEntityBeforeSave(ProductEntity product)
        {
        }

        /// <summary>
        /// Get's called in the Posproduct Validator
        /// </summary>
        public override void PosproductValidateEntityBeforeSave(PosproductEntity product)
        {
        }

        /// <summary>
        /// Retrieve all importable posproducts (this might exclude some Posproducts that are not
        /// supported)
        /// </summary>
        /// <param name="company">The company.</param>
        /// <param name="prefetchProducts">if set to <c>true</c> prefetch products.</param>
        /// <param name="hideImportedProducts"></param>
        /// <returns></returns>
        public override PosproductCollection RetrieveAllImportablePosproducts(CompanyEntity company, bool prefetchProducts, bool hideImportedProducts)
        {
            return PosproductHelper.GetPosproductCollection(company.CompanyId, prefetchProducts, hideImportedProducts);
        }

        /// <summary>
        /// Verify if the Settings for the POS integration are complete and ready for an export to a config file
        /// </summary>
        public override void VerifySettings(TerminalEntity terminal)
        {

        }

        #endregion

        #region Request Service Specific

        /// <summary>
        /// Perform Pos Specific aciton on an Order to convert it to a Pos orders
        /// </summary>
        /// <param name="orderEntity">Order Entity</param>
        /// <param name="orderModel">Order Model</param>
        public override void PrepareOrderForRequestService(OrderEntity orderEntity, Order orderModel)
        {
        }

        #endregion

        #region On-site Server Specfic

        /// <summary>
        /// Prepares the order for pos by the supplied posIntegrationInformation.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="posIntegrationInformation">The pos integration information.</param>
        public override void PrepareOrderForPos(Order order, PosIntegrationInformation posIntegrationInformation)
        {
        }

        /// <summary>
        /// Prepares the orderitem for pos by the supplied posIntegrationInformation.
        /// </summary>
        /// <param name="sourceOrderitem">The sorce orderitem</param>
        /// <param name="resultOrderitems"></param>
        /// <param name="posIntegrationInformation">The pos integration information.</param>
        public override void PrepareOrderitemForPos(Orderitem sourceOrderitem, List<Orderitem> resultOrderitems, PosIntegrationInformation posIntegrationInformation)
        {
        }

        /// <summary>
        /// Prepares the orderitem created for a Alterationitem for pos by the supplied posIntegrationInformation.
        /// </summary>
        /// <param name="orderitem">The orderitem.</param>
        /// <param name="alteration">The alteration.</param>
        /// <param name="posIntegrationInformation">The pos integration information.</param>
        public override void PrepareOrderitemFromAlterationitemForPos(Orderitem orderitem, Alteration alteration, PosIntegrationInformation posIntegrationInformation)
        {
        }

        /// <summary>
        /// Adds the pos specific pos integration information to be used by the On-Site server
        /// </summary>
        /// <param name="terminalEntity">The terminal entity.</param>
        /// <param name="posIntegrationInformation">The pos integration information.</param>
        public override void AddPosSpecificPosIntegrationInformation(TerminalEntity terminalEntity, PosIntegrationInformation posIntegrationInformation)
        {
        }

        #endregion
    }
}
