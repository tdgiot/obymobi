﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Unitouch
{
    /// <summary>
    /// UnitouchAccountType enums
    /// </summary>
    public enum UnitouchAccountType
    {
        /// <summary>
        /// Table
        /// </summary>
        Table = 1,

        /// <summary>
        /// Room
        /// </summary>
        Room = 2,

        /// <summary>
        /// Customer
        /// </summary>
        Customer = 4,

        /// <summary>
        /// Cash
        /// </summary>
        Cash = 8,

        /// <summary>
        /// Order
        /// </summary>
        Order = 16
    }
}
