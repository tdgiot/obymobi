﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Obymobi.Logic.Loggers;
using Dionysos;
using Obymobi.Logic.POS.Unitouch.Enums;
using Obymobi.Logic.Model;
using System.Globalization;
using Obymobi.Enums;

namespace Obymobi.Logic.POS.Unitouch
{
    /// <summary>
    /// UnitouchConnector class
    /// </summary>
    public class UnitouchConnector : IPOSConnector
    {
        #region Fields

        private string ip = "192.168.0.66";
        private int port = 1026;
        private Socket socket = null;
        private IPAddress ipAddress = null;
        private IPEndPoint ipEndPoint = null;
        private int waiterId = 1;
        private int currentUser = 1;
        private int currentCashReg = 1;

        private int shortSleep = 100;
        private int longSleep = 500;

        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitouchConnector"/> class.
        /// </summary>
        public UnitouchConnector()
        {
            this.Initialize();
        }

        #endregion

        #region Methods

        #region IPOSConnector methods

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public Model.Posproduct[] GetPosproducts()
        {
            List<Posproduct> posproducts = new List<Posproduct>();

            this.WriteToLogExtended("GetPosproducts", "Start {0}", TimeStamp.CreateTimeStamp());

            // Unitouch culture for numbers
            var culture = CultureInfo.CreateSpecificCulture("en-GB");

            bool hasError = false;

            // Connect
            try
            {
                this.Connect();
            }
            catch (Exception ex)
            {
                hasError = true;
                this.errorMessage = string.Format("An exception was thrown while trying to connect to the POS on '{0}:{1}'. Exception: {2}", this.ip, this.port, ex.Message);
            }

            if (!hasError)
            {
                // Get the products
                try
                {
                    UnitouchResponse response = this.DataGet("ART.txt");
                    if (response.Data.Length > 0)
                    {
                        string[] lines = response.Data.Split("\n", StringSplitOptions.RemoveEmptyEntries);
                        foreach (string line in lines)
                        {
                            if (!line.IsNullOrWhiteSpace())
                            {
                                string[] fields = line.Split("\t", StringSplitOptions.None);
                                if (fields.Length == 20)
                                {
                                    Posproduct posproduct = new Posproduct();
                                    posproduct.ExternalId = fields[0];
                                    posproduct.Name = fields[1];

                                    int externalPoscategoryId = -1;
                                    if (int.TryParse(fields[2], out externalPoscategoryId))
                                    {
                                        externalPoscategoryId = (int)System.Math.Floor(System.Math.Log(externalPoscategoryId, 2));
                                        externalPoscategoryId++;
                                        posproduct.ExternalPoscategoryId = externalPoscategoryId.ToString();
                                    }

                                    decimal price = 0;
                                    if (decimal.TryParse(fields[3], NumberStyles.Number, culture, out price))
                                        posproduct.PriceIn = price;

                                    posproduct.VatTariff = 2;

                                    posproducts.Add(posproduct);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    hasError = true;
                    this.errorMessage = string.Format("An exception was thrown while trying to get the products from the POS. Exception: {0}", ex.Message);
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, ex, errorMessage);
                }
            }

            if (hasError)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, this.errorMessage);

            this.WriteToLogExtended("GetPosproducts", "End {0}", TimeStamp.CreateTimeStamp());

            return posproducts.ToArray();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public Model.Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();

            this.WriteToLogExtended("GetPoscategories", "Start {0}", TimeStamp.CreateTimeStamp());

            bool hasError = false;

            // Connect
            try
            {
                this.Connect();
            }
            catch (Exception ex)
            {
                hasError = true;
                this.errorMessage = string.Format("An exception was thrown while trying to connect to the POS on '{0}:{1}'. Exception: {2}", this.ip, this.port, ex.Message);
            }

            if (!hasError)
            {
                // Get the categories
                try
                {
                    UnitouchResponse response = this.DataGet("TYPES.txt");
                    if (response.Data.Length > 0)
                    {
                        string[] lines = response.Data.Split("\n", StringSplitOptions.RemoveEmptyEntries);
                        foreach (string line in lines)
                        {
                            if (!line.IsNullOrWhiteSpace())
                            {
                                string[] fields = line.Split("\t", StringSplitOptions.None);
                                if (fields.Length == 2)
                                {
                                    Poscategory poscategory = new Poscategory();
                                    poscategory.ExternalId = fields[0];
                                    poscategory.Name = fields[1];

                                    poscategories.Add(poscategory);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    hasError = true;
                    this.errorMessage = string.Format("An exception was thrown while trying to get the categories from the POS. Exception: {0}", ex.Message);
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, this.errorMessage);
                }
            }

            if (hasError)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, this.errorMessage);

            this.WriteToLogExtended("GetPoscategories", "End {0}", TimeStamp.CreateTimeStamp());

            return poscategories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            return null;
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            return null;
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public Model.Posorder GetPosorder(string deliverypointNumber)
        {
            return null;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            OrderProcessingError processingError = OrderProcessingError.None;

            this.WriteToLogExtended("SaveOrder", "Start {0}", TimeStamp.CreateTimeStamp());

            // Connect
            try
            {
                this.Connect();
            }
            catch (Exception ex)
            {
                processingError = OrderProcessingError.PosErrorApiInterfaceException;
                this.errorMessage = string.Format("An exception was thrown while trying to connect to the POS on '{0}:{1}'. Exception: {2}", this.ip, this.port, ex.Message);
            }

            int deliverypoint = -1;

            if (processingError == OrderProcessingError.None)
            {
                // Get the open tables
                try
                {
                    UnitouchResponse response = this.PlstOpen(UnitouchAccountType.Table);
                    if (response.Data.Length > 0)
                    {
                        string[] lines = response.Data.Split("\n", StringSplitOptions.RemoveEmptyEntries);
                        foreach (string line in lines)
                        {
                            if (!line.IsNullOrWhiteSpace())
                            {
                                string[] fields = line.Split("\t", StringSplitOptions.None);
                                if (fields.Length >= 6 && (fields[5].StartsWith(posorder.PosdeliverypointExternalId) || fields[5].EndsWith(posorder.PosdeliverypointExternalId)))
                                {
                                    if (!int.TryParse(fields[0], out deliverypoint))
                                        throw new POSException(OrderProcessingError.PosErrorNonParseableOrConvertableDataType,
                                            string.Format("The external delivery point '{0}' could not be parsed to an integer", fields[0]));
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    processingError = OrderProcessingError.AlohaCouldNotOpenTable;
                    this.errorMessage = string.Format("An exception was thrown while trying to get the open tables from the POS on '{0}:{1}'. Exception: {2}", this.ip, this.port, ex.Message);
                }
            }

            if (deliverypoint == -1)
            {
                processingError = OrderProcessingError.UnitouchCouldNotGetBill;
                this.errorMessage = string.Format("Deliverypoint '{0}' was not yet opened the POS", posorder.PosdeliverypointExternalId);
            }

            if (processingError == OrderProcessingError.None && deliverypoint != -1)
            {
                // Set the user
                try
                {
                    this.SetUsr(this.currentUser, this.currentCashReg);
                }
                catch (Exception ex)
                {
                    processingError = OrderProcessingError.UnitouchCouldNotSetUser;
                    this.errorMessage = string.Format("An exception was thrown while trying to set the user. Exception: {0}", ex.Message);
                }
            }

            if (processingError == OrderProcessingError.None && deliverypoint != -1)
            {
                // Get the current items on the bill to check whether the table is locked
                try
                {
                    this.ACCGetAll(UnitouchAccountType.Table, deliverypoint);
                }
                catch (Exception ex)
                {
                    processingError = OrderProcessingError.UnitouchCouldNotGetBill;
                    this.errorMessage = string.Format("An exception was thrown while trying to get the current items on the bill. Exception: {0}", ex.Message);
                }
            }

            if (processingError == OrderProcessingError.None && deliverypoint != -1)
            {
                // Put the order
                try
                {
                    this.ACCPut(UnitouchAccountType.Table, deliverypoint);
                }
                catch (Exception ex)
                {
                    processingError = OrderProcessingError.UnitouchCouldNotPutOrder;
                    this.errorMessage = string.Format("An exception was thrown while trying to put the order. Exception: {0}", ex.Message);
                }
            }

            //if (status != OrderStatus.FailedToProcess && deliverypoint != -1)
            //{
            //    // Get the locked PLUs in order to check 
            //    // whether all orderitems can be ordered
            //    try
            //    {
            //        UnitouchResponse response = this.GetLockPlu();
            //        if (response.Data.Length > 0)
            //        {
            //            // Get the locked plus
            //            string[] plu = response.Data.Split("/", StringSplitOptions.RemoveEmptyEntries);
            //            List<string> pluErrors = new List<string>();

            //            foreach (Posorderitem orderitem in posorder.Posorderitems)
            //            {
            //                if (plu.Contains(orderitem.PosproductExternalId))
            //                    pluErrors.Add(string.Format("{0} - {1}", orderitem.PosproductExternalId, orderitem.Description)); 
            //            }

            //            if (pluErrors.Count > 0)
            //            {
            //                status = OrderStatus.FailedToProcess;
            //                this.errorMessage = string.Format("The following PLUs cannot be ordered because they are locked: {0}", StringUtil.CombineWithComma(pluErrors.ToArray()));
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        status = OrderStatus.FailedToProcess;
            //        this.errorMessage = string.Format("An exception was thrown while trying to get the locked PLUs. Exception: {0}", ex.Message);
            //    }
            //}

            if (processingError == OrderProcessingError.None && deliverypoint != -1)
            {
                // Put the orderitems
                try
                {
                    this.ACCPutOrderitems(posorder.Posorderitems);
                }
                catch (Exception ex)
                {
                    processingError = OrderProcessingError.UnitouchCouldNotPutOrderitems;
                    this.errorMessage = string.Format("An exception was thrown while trying to put the orderitems. Exception: {0}", ex.Message);
                }
            }

            if (processingError != OrderProcessingError.None)
                POSException.ThrowTyped(processingError, null, this.errorMessage, null);

            this.WriteToLogExtended("SaveOrder", "End {0}", TimeStamp.CreateTimeStamp());

            return processingError;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            return true;
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Initializes the Unitouch connector
        /// </summary>
        private void Initialize()
        {
            try
            {
                this.ip = ConfigurationManager.GetString(POSConfigurationConstants.UnitouchIPAddress);
                this.port = ConfigurationManager.GetInt(POSConfigurationConstants.UnitouchPort);
                this.waiterId = ConfigurationManager.GetInt(POSConfigurationConstants.UnitouchWaiterId);
                this.currentUser = ConfigurationManager.GetInt(POSConfigurationConstants.UnitouchCurrentUser);
                this.currentCashReg = ConfigurationManager.GetInt(POSConfigurationConstants.UnitouchCurrentCashReg);
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, string.Format("An exception was thrown while initializing the UnitouchConnector. Exception: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Connects the Unitouch connector with the POS
        /// </summary>
        /// <returns>True if connecting with the POS was successful, False if not</returns>
        public UnitouchResponse Connect()
        {
            UnitouchResponse unitouchResponse = null;

            this.WriteToLogExtended("Connect", "Start");

            byte[] bytes = new byte[256];
            string response = string.Empty;
            try
            {
                this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                this.socket.SendTimeout = 9000;
                this.socket.ReceiveTimeout = 16000;

                this.ipAddress = IPAddress.Parse(this.ip);
                this.ipEndPoint = new IPEndPoint(ipAddress, this.port);
                this.socket.Connect(ipEndPoint);
                if (!this.socket.Connected)
                    throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("The UnitouchConnector could not connect to the POS on '{0}:{1}'", this.ip, this.port));

                int result = this.socket.Receive(bytes, bytes.Length, 0);
                if (result > 0)
                    response = Encoding.UTF8.GetString(bytes);
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("An exception was thrown while connecting the UnitouchConnector. Exception: {0}", ex.Message));
            }

            if (response.Length > 0)
            {
                unitouchResponse = new UnitouchResponse(response);
                if (unitouchResponse.Code != 100)
                    throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("An invalid response '{0} {1}' was returned while trying to connect to the POS on '{0} {1}'", unitouchResponse.Code, unitouchResponse.Message, this.ip, this.port));
            }

            this.WriteToLogExtended("Connect", "End");

            return unitouchResponse;
        }

        /// <summary>
        /// Does the command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="bufferSize">Size of the buffer.</param>
        /// <param name="sleep">The sleep.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        public string DoCommand(Commands command, int bufferSize, int sleep, params object[] args)
        {
            // Create the command string
            string commandString = this.CreateCommandString(command, args);
            return DoCommand(commandString, bufferSize, sleep);
        }

        /// <summary>
        /// Does the command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="bufferSize">The bufferSize.</param>
        /// <param name="sleep">The sleep.</param>
        /// <returns></returns>
        public string DoCommand(string command, int bufferSize, int sleep)
        {
            string response = string.Empty;

            this.WriteToLogExtended("DoCommand - Start", command.Substring(0, command.Length - 1)); // Remove the last \n in the log

            // Execute the command string
            byte[] bytes = new byte[bufferSize];

            try
            {
                byte[] data = System.Text.Encoding.ASCII.GetBytes(command);
                int result = this.socket.Send(data);

                System.Threading.Thread.Sleep(sleep);

                this.socket.Receive(bytes, 0, bufferSize, 0);
                response += Encoding.UTF8.GetString(bytes);
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("An exception was thrown while trying the execute command '{0}'. Exception: {1}", command, ex.Message));
            }

            this.WriteToLogExtended("DoCommand", "End");

            return response;
        }

        /// <summary>
        /// Creates the command string.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="args">The args.</param>
        /// <returns>A <see cref="String"/> instance containing the command string.</returns>
        private string CreateCommandString(Commands command, params object[] args)
        {
            string commandString = command.ToString();
            foreach (object arg in args)
            {
                commandString += string.Format(" {0}", arg);
            }
            commandString += "\n";
            return commandString;
        }

        /// <summary>
        /// Writes to log normal.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Debug("UnitouchConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes to log extended.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("UnitouchConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        #endregion

        #region Wrapper methods

        /// <summary>
        /// Aborts scheduled cron script
        /// </summary>
        public void AbortClean()
        {
        }

        /// <summary>
        /// Opens specified account and sends total amount due, account remains open
        /// </summary>
        public void ACCBill()
        {
        }

        /// <summary>
        /// Saves current open account
        /// </summary>
        public void ACCClose()
        {
        }

        /// <summary>
        /// Opens specified account and sends accountdata. LAST 20 ENTERED ITEMS ONLY. Account remains open.
        /// </summary>
        public void ACCGet()
        {
        }

        /// <summary>
        /// Identical to ACCGET, but all transaction items are sent
        /// </summary>
        public UnitouchResponse ACCGetAll(UnitouchAccountType accountType, int accountNumber)
        {
            UnitouchResponse unitouchResponse = null;

            string response = this.DoCommand(Commands.ACCGETALL, 8192, this.longSleep, (int)accountType, accountNumber);
            if (response.Length > 0)
                unitouchResponse = new UnitouchResponse(response);

            if (unitouchResponse.Code != 200 && unitouchResponse.Code != 201)
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("Command ACCGetAll returned an invalid response '{0} {1}'", unitouchResponse.Code, unitouchResponse.Message));

            return unitouchResponse;
        }

        /// <summary>
        /// Moves items on source account to destination account, source account will be closed
        /// </summary>
        public void ACCMove()
        {
        }

        /// <summary>
        /// Pays specified account, account will be closed
        /// </summary>
        public void ACCPay()
        {
        }

        /// <summary>
        /// Prints current open account that is cached by the daemon. Uses RepBillSmall as
        /// default unless another report is specified.
        /// Since there is no update cycle involved with this function, the report
        /// uses the data that the cash register had before the 'open'
        /// </summary>
        public void ACCPrint()
        {
        }

        /// <summary>
        /// Updates and saves specified account, account will be closed
        /// </summary>
        public UnitouchResponse ACCPut(UnitouchAccountType accountType, int accountNumber)
        {
            UnitouchResponse unitouchResponse = null;

            string response = this.DoCommand(Commands.ACCPUT, 256, this.shortSleep, (int)accountType, accountNumber);

            //if (response.Contains("END"))
            //    response = this.DoCommand(Commands.ACCPUT, 8192, (int)accountType, accountNumber);

            if (response.Length > 0)
                unitouchResponse = new UnitouchResponse(response);

            if (unitouchResponse.Code != 200 && unitouchResponse.Code != 201)
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("Command ACCPut returned an invalid response '{0} {1}'", unitouchResponse.Code, unitouchResponse.Message));

            return unitouchResponse;
        }

        /// <summary>
        /// Puts the orderitems
        /// </summary>
        /// <param name="orderitems">The orderitems.</param>
        public UnitouchResponse ACCPutOrderitems(Posorderitem[] orderitems)
        {
            UnitouchResponse unitouchResponse = null;

            StringBuilder sb = new StringBuilder();

            foreach (Posorderitem orderitem in orderitems)
            {
                sb.AppendFormat("{0}	{1}	{2}	{3}	7	F	{4}	F	*	0		0	0	0\n", this.waiterId, orderitem.PosproductExternalId, orderitem.Description, orderitem.Quantity, orderitem.ProductPriceIn.ToString(CultureInfo.InvariantCulture));
            }
            sb.Append("//END\n");

            string response = this.DoCommand(sb.ToString(), 8192, this.shortSleep);
            if (response.Length > 0)
                unitouchResponse = new UnitouchResponse(response);

            if (unitouchResponse.Code != 200 && unitouchResponse.Code != 201)
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("An invalid response '{0} {1}' was returned while adding the orderitems", unitouchResponse.Code, unitouchResponse.Message));

            return unitouchResponse;
        }

        /// <summary>
        /// Moves items on source account to destination account, source account will be closed
        /// </summary>
        public void ACCSplit()
        {
        }

        /// <summary>
        /// Moves items to the destination account specified by the item's DAT (Destination Account
        /// Type) and DAN (Destination Account Number) fields, account will be closed.
        /// Due to the complexity of this command (several destinations must be processed) the daemon
        /// can not respond with detailed error information such as locked destinations or insufficient
        /// space in one of the destination accounts. If some items can not be moved, it's up to the
        /// client to take appropriate action (notify user etc...)
        /// </summary>
        public void ACCSplitDst()
        {
        }

        /// <summary>
        /// Retreives state of specified account
        /// </summary>
        public void ACCState()
        {
        }

        /// <summary>
        /// Sends account items in 'view' format, this operation has no influence on the current. Open account
        /// </summary>
        public void ACCView()
        {
        }

        /// <summary>
        /// Adds customer
        /// </summary>
        public void ACCAddCust()
        {
        }

        /// <summary>
        /// Adds customer. New in version V5.30
        /// </summary>
        public void ACCAddCust_NS()
        {
        }

        /// <summary>
        /// Checks all open transactions of type AccountTable for items linked to the specified
        /// customer. Performs a split of these items to the customer account (if possible) an
        /// reports remaining item lines and remaining total when finished.
        /// Items can not be moved if table account and/or customer account are in use (locked)
        /// </summary>
        public void AutoSplit()
        {
        }

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data in BINARY format
        /// </summary>
        public void BinDataGet()
        {
        }

        /// <summary>
        /// Ends conversation with daemon.
        /// </summary>
        public void Bye()
        {
        }

        /// <summary>
        /// Changes transaction Reference counter
        /// </summary>
        public void ChangeRef()
        {
        }

        /// <summary>
        /// Changes Seq  Nr counter TSQ records in Count.txt
        /// </summary>
        public void ChangeSeqNr()
        {
        }

        /// <summary>
        /// Changes report Z-Counter
        /// </summary>
        public void ChangeZ()
        {
        }

        /// <summary>
        /// Saves CST table and rebuilds CustIndexByName, must be used after adding, updating, deleting customer records with the _NS commands
        /// </summary>
        public void Commit_Cst()
        {
        }

        /// <summary>
        /// Cleans CardLog file
        /// </summary>
        public void CrlClean()
        {
        }

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data
        /// </summary>
        public UnitouchResponse DataGet(string fileName)
        {
            UnitouchResponse unitouchResponse = null;

            string response = this.DoCommand(Commands.DATAGET, 16384, this.longSleep, fileName);
            if (response.Length > 0)
                unitouchResponse = new UnitouchResponse(response);

            if (unitouchResponse.Code != 200 && unitouchResponse.Code != 201)
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("Command DATAGET returned an invalid response '{0} {1}'", unitouchResponse.Code, unitouchResponse.Message));

            return unitouchResponse;
        }

        /// <summary>
        /// Deletes specified customer
        /// </summary>
        public void DelCust()
        {
        }

        /// <summary>
        /// Deletes specified customer
        /// </summary>
        public void DelCust_NS()
        {
        }

        /// <summary>
        /// Starts /usr/symbol/exportdata script with specified NUMERIC parameter
        /// </summary>
        public void Export()
        {
        }

        /// <summary>
        /// Gets customer data
        /// </summary>
        public void GetCust()
        {
        }

        /// <summary>
        /// Gets locked PLu's
        /// </summary>
        public UnitouchResponse GetLockPlu()
        {
            UnitouchResponse unitouchResponse = null;

            string response = this.DoCommand(Commands.GETLOCKPLU, 8192, this.longSleep);
            if (response.Length > 0)
                unitouchResponse = new UnitouchResponse(response);

            if (unitouchResponse.Code != 200 && unitouchResponse.Code != 201)
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("Command GetLockPLU returned an invalid response '{0} {1}'", unitouchResponse.Code, unitouchResponse.Message));

            return unitouchResponse;
        }

        /// <summary>
        /// Gets timestamp of plu locks so the Palm knows when these must be updated
        /// </summary>
        public void GetLockTs()
        {
        }

        /// <summary>
        /// Get NrPersons on specified account, account remains open
        /// </summary>
        public void GetNrp()
        {
        }

        /// <summary>
        /// Gets remark text of specified account, account remains open
        /// </summary>
        public void GetRmk()
        {
        }

        /// <summary>
        /// Gets specified SeqNr from TSQ
        /// </summary>
        public void GetSeqNr()
        {
        }

        /// <summary>
        /// Resolves roomnumber to guest name (Hotel Concepts)
        /// </summary>
        public void HC_CheckRoom()
        {
        }

        /// <summary>
        /// Initiates graceful BBKassa shutdown
        /// </summary>
        public void KillServer()
        {
        }

        /// <summary>
        /// Links Tag (alfanumeric) to specified account type/nr
        /// </summary>
        public void LinkTag()
        {
        }

        /// <summary>
        /// Clean BBKassa event log
        /// </summary>
        public void LogClean()
        {
        }

        /// <summary>
        /// Get directory listing
        /// </summary>
        public void Ls()
        {
        }

        /// <summary>
        /// Lists open accounts of specified type
        /// </summary>
        public void LstOpen()
        {
        }

        /// <summary>
        /// Cleans Stock Mutation file
        /// </summary>
        public void MutClean()
        {
        }

        /// <summary>
        /// OPENS a NEW trs of type AccountOrder, returns account number
        /// </summary>
        public void NewOrder()
        {
        }

        /// <summary>
        /// Retrieves creation date of a Palm OS .PRC or .PDB file
        /// </summary>
        public void PalmCrDate()
        {
        }

        /// <summary>
        /// Lists open accounts of specified type
        /// </summary>
        public UnitouchResponse PlstOpen(UnitouchAccountType accountType)
        {
            UnitouchResponse unitouchResponse = null;

            string response = this.DoCommand(Commands.PLSTOPEN, 8192, this.longSleep, (int)accountType);
            if (response.Length > 0)
                unitouchResponse = new UnitouchResponse(response);

            if (unitouchResponse.Code != 200 && unitouchResponse.Code != 201)
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("Command PlstOpen returned an invalid response '{0} {1}'", unitouchResponse.Code, unitouchResponse.Message));

            return unitouchResponse;
        }

        /// <summary>
        /// Resolves tag data to account type/nr
        /// </summary>
        public void ResvlTag()
        {
        }

        /// <summary>
        /// Schedule cron cleanup
        /// </summary>
        public void SchedClean()
        {
        }

        /// <summary>
        /// Clean sepsrvr transaction log
        /// </summary>
        public void SepClean()
        {
        }

        /// <summary>
        /// lock specified sepcard - 'reason' string MAX 49 chars!!!!
        /// </summary>
        public void SepCrdLock()
        {
        }

        /// <summary>
        /// Unlock specified sepcard
        /// </summary>
        public void SepCrdUnlock()
        {
        }

        /// <summary>
        /// Sets NrPersons on specified account, account ramains open
        /// </summary>
        public void SetNrp()
        {
        }

        /// <summary>
        /// Sets transaction remark on specified account, account ramains open
        /// </summary>
        public void SetRmk()
        {
        }

        /// <summary>
        /// Sets CurrentRanking on specified account, account ramains open
        /// </summary>
        public void SetRnk()
        {
        }

        /// <summary>
        /// Sets system date/time
        /// </summary>
        public void SetTime()
        {
        }

        /// <summary>
        /// Sets current user/cashreg. should be right sent after connecting to the daemon
        /// </summary>
        public UnitouchResponse SetUsr(int currentUser, int currentCashReg)
        {
            UnitouchResponse unitouchResponse = null;

            string response = this.DoCommand(Obymobi.Logic.POS.Unitouch.Enums.Commands.SETUSR, 256, this.shortSleep, currentUser, currentCashReg);
            if (response.Length > 0)
                unitouchResponse = new UnitouchResponse(response);

            if (unitouchResponse.Code != 200 && unitouchResponse.Code != 201)
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("Command SETUSR returned an invalid response '{0} {1}'", unitouchResponse.Code, unitouchResponse.Message));

            return unitouchResponse;
        }

        /// <summary>
        /// Cleans 'Totalized' file
        /// </summary>
        public void TotClean()
        {
        }

        /// <summary>
        /// Cleans Trs/Itm files
        /// </summary>
        public void TrsClean()
        {
        }

        /// <summary>
        /// Unlinks Tag (alfanumeric)
        /// </summary>
        public void UnlinkTag()
        {
        }

        /// <summary>
        /// Updates existing customer
        /// </summary>
        public void UpdCust()
        {
        }

        /// <summary>
        /// Updates existing customer
        /// </summary>
        public void UpdCust_NS()
        {
        }

        /// <summary>
        /// Checks if customer exists
        /// </summary>
        public void VerifyCust()
        {
        }

        /// <summary>
        /// Waiter cash cleanup
        /// </summary>
        public void WcaClean()
        {
        }

        /// <summary>
        /// Waiter timelog cleanup
        /// </summary>
        public void WtrClean()
        {
        }

        /// <summary>
        /// Pays specified account, similar to ACCPAY, but it will automatically close any
        /// open account that the daemon caches and returns the TRS/ITM data in 'standard'
        /// format. account will be closed
        /// </summary>
        public void WinACCPay()
        {
        }

        /// <summary>
        /// Views account data regardless of lock state
        /// </summary>
        public void WinACCView()
        {
        }

        /// <summary>
        /// Lists open accounts of specified type in 'standard' format
        /// </summary>
        public void WinLstOpen()
        {
        }

        /// <summary>
        /// Changes OrdState value of specified order
        /// </summary>
        public void ChOrdState()
        {
        }

        /// <summary>
        /// Removes specified order by returning all items and paying the account
        /// </summary>
        public void RemoveOrd()
        {
        }

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        /// <summary>
        /// Gets or sets the IP-address 
        /// </summary>
        public string IP
        {
            get
            {
                return this.ip;
            }
            set
            {
                this.ip = value;
            }
        }

        /// <summary>
        /// Gets or sets the port
        /// </summary>
        public int Port
        {
            get
            {
                return this.port;
            }
            set
            {
                this.port = value;
            }
        }

        /// <summary>
        /// Gets or sets the waiter ID
        /// </summary>
        public int WaiterID
        {
            get
            {
                return this.waiterId;
            }
            set
            {
                this.waiterId = value;
            }
        }

        /// <summary>
        /// Gets or sets the current user
        /// </summary>
        public int CurrentUser
        {
            get
            {
                return this.currentUser;
            }
            set
            {
                this.currentUser = value;
            }
        }

        /// <summary>
        /// Gets or sets the current cash register
        /// </summary>
        public int CurrentCashReg
        {
            get
            {
                return this.currentCashReg;
            }
            set
            {
                this.currentCashReg = value;
            }
        }

        #endregion
    }
}
