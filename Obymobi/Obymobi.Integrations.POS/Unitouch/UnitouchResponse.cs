﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Unitouch
{
    /// <summary>
    /// UnitouchResponse class
    /// </summary>
    public class UnitouchResponse
    {
        #region Fields

        private int code = -1;
        private string message = string.Empty;
        private string data = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitouchResponse"/> class.
        /// </summary>
        public UnitouchResponse(string response)
        {
            int code = -1;
            if (response.Length >= 3)
            {
                string codeString = response.Substring(0, 3);
                if (int.TryParse(codeString, out code))
                    this.code = code;
            }

            if (response.Length > 4)
            {
                this.message = response.Substring(4);
                if (this.message.Contains("\n"))
                {
                    int index = this.message.IndexOf("\n");
                    this.data = this.message.Substring(index+1);
                    this.message = this.message.Substring(0, index);
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public int Code
        {
            get
            {
                return this.code;
            }
            set
            {
                this.code = value;
            }
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get
            {
                return this.message;
            }
            set
            {
                this.message = value;
            }
        }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
        public string Data
        {
            get
            {
                return this.data;
            }
            set
            {
                this.data = value;
            }
        }

        #endregion

    }
}
