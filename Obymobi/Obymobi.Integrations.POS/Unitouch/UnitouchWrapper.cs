﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.POS.Unitouch.Enums;
using System.Net.Sockets;
using System.Net;

namespace Obymobi.Logic.POS.Unitouch
{
    /// <summary>
    /// UnitouchWrapper class
    /// </summary>
    public class UnitouchWrapper
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitouchWrapper"/> class.
        /// </summary>
        public UnitouchWrapper()
        {
        }

        #endregion

        #region Methods

        #region Helper methods

        private void DoCommand(Commands command, params object[] args)
        {
            // Create the command string
            string commandString = this.CreateCommandString(command, args);

            // Execute the command string
            try
            {
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                IPAddress ipAddress = IPAddress.Parse("192.168.0.66");
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, 1026);
                socket.Connect(ipEndPoint);
            }
            catch
            {
                // GK Nothing is performed here, I only delete the available line because of the warning 'Variable ex is never used'
            }

            // Process the result
        }

        /// <summary>
        /// Creates the command string.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="args">The args.</param>
        /// <returns>A <see cref="String"/> instance containing the command string.</returns>
        private string CreateCommandString(Commands command, params object[] args)
        {
            string commandString = command.ToString();
            foreach (object arg in args)
            {
                commandString += string.Format(" {0}", arg);
            }
            return commandString;
        }

        #endregion

        #endregion
    }
}
