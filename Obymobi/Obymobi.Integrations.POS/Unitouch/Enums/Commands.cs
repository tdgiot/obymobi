﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Unitouch.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum Commands
    {
        /// <summary>
        /// Aborts scheduled cron script
        /// </summary>
        ABORTCLEAN,

        /// <summary>
        /// Opens specified account and sends total amount due, account remains open
        /// </summary>
        ACCBILL,

        /// <summary>
        /// Saves current open account
        /// </summary>
        ACCCLOSE,

        /// <summary>
        /// Opens specified account and sends accountdata. LAST 20 ENTERED ITEMS ONLY. Account remains open.
        /// </summary>
        ACCGET,

        /// <summary>
        /// Identical to ACCGET, but all transaction items are sent
        /// </summary>
        ACCGETALL,

        /// <summary>
        /// Moves items on source account to destination account, source account will be closed
        /// </summary>
        ACCMOVE,

        /// <summary>
        /// Pays specified account, account will be closed
        /// </summary>
        ACCPAY,

        /// <summary>
        /// Prints current open account that is cached by the daemon. Uses RepBillSmall as
        /// default unless another report is specified.
        /// Since there is no update cycle involved with this function, the report
        /// uses the data that the cash register had before the 'open'
        /// </summary>
        ACCPRINT,

        /// <summary>
        /// Updates and saves specified account, account will be closed
        /// </summary>
        ACCPUT,

        /// <summary>
        /// Moves items on source account to destination account, source account will be closed
        /// </summary>
        ACCSPLIT,

        /// <summary>
        /// Moves items to the destination account specified by the item's DAT (Destination Account
        /// Type) and DAN (Destination Account Number) fields, account will be closed.
        /// Due to the complexity of this command (several destinations must be processed) the daemon
        /// can not respond with detailed error information such as locked destinations or insufficient
        /// space in one of the destination accounts. If some items can not be moved, it's up to the
        /// client to take appropriate action (notify user etc...)
        /// </summary>
        ACCSPLITDST,

        /// <summary>
        /// Retreives state of specified account
        /// </summary>
        ACCSTATE,

        /// <summary>
        /// Sends account items in 'view' format, this operation has no influence on the current. Open account
        /// </summary>
        ACCVIEW,

        /// <summary>
        /// Adds customer
        /// </summary>
        ADDCUST,

        /// <summary>
        /// Adds customer. New in version V5.30
        /// </summary>
        ADDCUST_NS,

        /// <summary>
        /// Checks all open transactions of type AccountTable for items linked to the specified
        /// customer. Performs a split of these items to the customer account (if possible) an
        /// reports remaining item lines and remaining total when finished.
        /// Items can not be moved if table account and/or customer account are in use (locked)
        /// </summary>
        AUTOSPLIT,

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data in BYNARY format
        /// </summary>
        BINDATAGET,

        /// <summary>
        /// Ends conversation with daemon.
        /// </summary>
        BYE,

        /// <summary>
        /// Changes transaction Reference counter
        /// </summary>
        CHANGEREF,

        /// <summary>
        /// Changes Seq  Nr counter TSQ records in Count.txt
        /// </summary>
        CHANGESEQNR,

        /// <summary>
        /// Changes report Z-Counter
        /// </summary>
        CHANGEZ,

        /// <summary>
        /// Saves CST table and rebuilds CustIndexByName, must be used after adding, updating, deleting customer records with the _NS commands
        /// </summary>
        COMMIT_CST,

        /// <summary>
        /// Cleans CardLog file
        /// </summary>
        CRLCLEAN,

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data
        /// </summary>
        DATAGET,

        /// <summary>
        /// Deletes specified customer
        /// </summary>
        DELCUST,

        /// <summary>
        /// Deletes specified customer
        /// </summary>
        DELCUST_NS,

        /// <summary>
        /// Starts /usr/symbol/exportdata script with specified NUMERIC parameter
        /// </summary>
        EXPORT,

        /// <summary>
        /// Gets customer data
        /// </summary>
        GETCUST,

        /// <summary>
        /// Gets locked PLu's
        /// </summary>
        GETLOCKPLU,

        /// <summary>
        /// Gets timestamp of plu locks so the Palm knows when these must be updated
        /// </summary>
        GETLOCKTS,

        /// <summary>
        /// Get NrPersons on specified account, account remains open
        /// </summary>
        GETNRP,

        /// <summary>
        /// Gets remark text of specified account, account remains open
        /// </summary>
        GETRMK,

        /// <summary>
        /// Gets specified SeqNr from TSQ
        /// </summary>
        GETSEQNR,

        /// <summary>
        /// Resolves roomnumber to guest name (Hotel Concepts)
        /// </summary>
        HC_CHECKROOM,

        /// <summary>
        /// Initiates graceful BBKassa shutdown
        /// </summary>
        KILLSERVER,

        /// <summary>
        /// Links Tag (alfanumeric) to specified account type/nr
        /// </summary>
        LINKTAG,

        /// <summary>
        /// Clean BBKassa event log
        /// </summary>
        LOGCLEAN,

        /// <summary>
        /// Get directory listing
        /// </summary>
        LS,

        /// <summary>
        /// Lists open accounts of specified type
        /// </summary>
        LSTOPEN,

        /// <summary>
        /// Cleans Stock Mutation file
        /// </summary>
        MUTCLEAN,

        /// <summary>
        /// OPENS a NEW trs of type AccountOrder, returns account number
        /// </summary>
        NEWORDER,

        /// <summary>
        /// Retrieves creation date of a Palm OS .PRC or .PDB file
        /// </summary>
        PALMCRDATE,

        /// <summary>
        /// Lists open accounts of specified type
        /// </summary>
        PLSTOPEN,

        /// <summary>
        /// Resolves tag data to account type/nr
        /// </summary>
        RESLVTAG,

        /// <summary>
        /// Schedule cron cleanup
        /// </summary>
        SCHEDCLEAN,

        /// <summary>
        /// Clean sepsrvr transaction log
        /// </summary>
        SEPCLEAN,

        /// <summary>
        /// lock specified sepcard - 'reason' string MAX 49 chars!!!!
        /// </summary>
        SEPCRDLOCK,

        /// <summary>
        /// Unlock specified sepcard
        /// </summary>
        SEPCRDUNLOCK,

        /// <summary>
        /// Sets NrPersons on specified account, account ramains open
        /// </summary>
        SETNRP,

        /// <summary>
        /// Sets transaction remark on specified account, account ramains open
        /// </summary>
        SETRMK,

        /// <summary>
        /// Sets CurrentRanking on specified account, account ramains open
        /// </summary>
        SETRNK,

        /// <summary>
        /// Sets system date/time
        /// </summary>
        SETTIME,

        /// <summary>
        /// Sets current user/cashreg. should be right sent after connecting to the daemon
        /// </summary>
        SETUSR,

        /// <summary>
        /// Cleans 'Totalized' file
        /// </summary>
        TOTCLEAN,

        /// <summary>
        /// Cleans Trs/Itm files
        /// </summary>
        TRSCLEAN,

        /// <summary>
        /// Unlinks Tag (alfanumeric)
        /// </summary>
        UNLINKTAG,

        /// <summary>
        /// Updates existing customer
        /// </summary>
        UPDCUST,

        /// <summary>
        /// Updates existing customer
        /// </summary>
        UPDCUST_NS,

        /// <summary>
        /// Checks if customer exists
        /// </summary>
        VERIFYCUST,

        /// <summary>
        /// Waiter cash cleanup
        /// </summary>
        WCACLEAN,

        /// <summary>
        /// Waiter timelog cleanup
        /// </summary>
        WTRCLEAN,

        /// <summary>
        /// Pays specified account, similar to ACCPAY, but it will automatically close any
        /// open account that the daemon caches and returns the TRS/ITM data in 'standard'
        /// format. account will be closed
        /// </summary>
        WINACCPAY,

        /// <summary>
        /// Views account data regardless of lock state
        /// </summary>
        WINACCVIEW,

        /// <summary>
        /// Lists open accounts of specified type in 'standard' format
        /// </summary>
        WINLSTOPEN,

        /// <summary>
        /// Changes OrdState value of specified order
        /// </summary>
        CHORDSTATE,

        /// <summary>
        /// Removes specified order by returning all items and paying the account
        /// </summary>
        REMOVEORD
    }
}
