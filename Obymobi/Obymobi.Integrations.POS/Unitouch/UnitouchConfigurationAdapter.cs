﻿using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Logic.POS.Unitouch
{
    /// <summary>
    /// UnitouchConfigurationAdapter class
    /// </summary>
    public class UnitouchConfigurationAdapter : IConfigurationAdapter
    {
        #region Methods

        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.IPAddress = terminal.PosValue1;
            this.Port = terminal.PosValue2;
            this.WaiterId = terminal.PosValue3;
            this.CurrentUser = terminal.PosValue4;
            this.CurrentCashReg = terminal.PosValue5;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.IPAddress = terminal.PosValue1;
            this.Port = terminal.PosValue2;
            this.WaiterId = terminal.PosValue3;
            this.CurrentUser = terminal.PosValue4;
            this.CurrentCashReg = terminal.PosValue5;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.IPAddress = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UnitouchIPAddress);
            this.Port = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UnitouchPort);
            this.WaiterId = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UnitouchWaiterId);
            this.CurrentUser = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UnitouchCurrentUser);
            this.CurrentCashReg = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.UnitouchCurrentCashReg);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UnitouchIPAddress, this.IPAddress);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UnitouchPort, this.Port);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UnitouchWaiterId, this.WaiterId);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UnitouchCurrentUser, this.CurrentUser);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.UnitouchCurrentCashReg, this.CurrentCashReg);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.IPAddress;
            terminal.PosValue2 = this.Port;
            terminal.PosValue3 = this.WaiterId;
            terminal.PosValue4 = this.CurrentUser;
            terminal.PosValue5 = this.CurrentCashReg;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.IPAddress;
            terminal.PosValue2 = this.Port;
            terminal.PosValue3 = this.WaiterId;
            terminal.PosValue4 = this.CurrentUser;
            terminal.PosValue5 = this.CurrentCashReg;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the IP address.
        /// </summary>
        /// <value>
        /// The IP address.
        /// </value>
        public string IPAddress { get; set; }
        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public string Port { get; set; }
        /// <summary>
        /// Gets or sets the waiter id.
        /// </summary>
        /// <value>
        /// The waiter id.
        /// </value>
        public string WaiterId { get; set; }
        /// <summary>
        /// Gets or sets the current user.
        /// </summary>
        /// <value>
        /// The current user.
        /// </value>
        public string CurrentUser { get; set; }
        /// <summary>
        /// Gets or sets the current cash reg.
        /// </summary>
        /// <value>
        /// The current cash reg.
        /// </value>
        public string CurrentCashReg { get; set; }

        #endregion
    }
}
