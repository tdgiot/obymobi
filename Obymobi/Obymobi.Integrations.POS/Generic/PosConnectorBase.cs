﻿using System;
using System.IO;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Integrations.POS.Interfaces;
using Obymobi.Logging;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS.Generic
{
    /// <summary>
    /// PosConnectorBase class
    /// </summary>
    public abstract class PosConnectorBase : IPOSConnector
    {
        /// <summary>
        /// POSConnectorType
        /// </summary>
        public POSConnectorType ConnectorType = POSConnectorType.Unknown;

        public virtual void PrePosSynchronisation()
        {
            // Nothing - Ready to be overwritten
        }

        public virtual void PostPosSynchronisation()
        {
            // Nothing - Ready to be overwritten
        }

        /// <summary>
        /// Writes to log normal.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        protected void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Debug("{0} - {1} - {2}", this.ConnectorType.ToString(), methodName, string.Format(message, args));
            this.WriteToPosConnectorLog(methodName, message, args);
        }

        /// <summary>
        /// Writes to log extended.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        protected void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("{0} - {1} - {2}", this.ConnectorType.ToString(), methodName, string.Format(message, args));
            this.WriteToPosConnectorLog(methodName, message, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file specific for the POS integration
        /// also writes it via WriteToLogExtended
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public void WriteToPosConnectorLog(string method, string contents, params object[] args)
        {
            string formattedText = contents.FormatSafe(args);            

            // Create the Logs directory if it doesn't exists yet
            string dir = PosConnectorBase.CreateLogDirectory("Logs");

            DateTime now = DateTime.Now;

            // Set up a filestream
            FileStream fs = new FileStream(Path.Combine(dir, string.Format("{0}{1}{2}-{3}.log", now.Year.ToString("0000"), now.Month.ToString("00"), now.Day.ToString("00"), this.ConnectorType.ToString())),
                FileMode.OpenOrCreate, FileAccess.Write);

            // Set up a streamwriter for adding text
            StreamWriter sw = new StreamWriter(fs);

            // Find the end of the underlying filestream
            sw.BaseStream.Seek(0, SeekOrigin.End);

            // Add the text
            try
            {                
                sw.WriteLine(string.Format("{0}:{1}:{2}\t{3} - {4}", now.Hour.ToString("00"), now.Minute.ToString("00"), now.Second.ToString("00"), method, formattedText));
            }
            catch
            {
                sw.WriteLine("String.Format failed");
                sw.WriteLine(string.Format("{0}:{1}:{2}", now.Hour.ToString("00"), now.Minute.ToString("00"), now.Second.ToString("00")));
                sw.WriteLine(contents);
                sw.WriteLine("Args: ", StringUtil.CombineWithSeperator(", ", args));
            }
            // Add the text to the underlying filestream
            sw.Flush();
            // Close the writer
            sw.Close();
        }

        /// <summary>
        /// Creates a directory relative to the application directory
        /// </summary>
        /// <param name="relativeDir">The directory relative to the application directory</param>
        /// <returns>The full path to the relative directory</returns>
        protected static string CreateLogDirectory(string relativeDir)
        {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dir = Path.Combine(directory, relativeDir);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public abstract Posproduct[] GetPosproducts();

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public abstract Poscategory[] GetPoscategories();

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public abstract Posdeliverypointgroup[] GetPosdeliverypointgroups();

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public abstract Posdeliverypoint[] GetPosdeliverypoints();

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public abstract Posorder GetPosorder(string deliverypointNumber);


        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public abstract Enums.OrderProcessingError SaveOrder(Posorder posorder);

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public abstract bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber);

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public abstract bool PrintServiceRequestReceipt(string serviceDescription);

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public abstract bool PrintCheckoutRequestReceipt(string checkoutDescription);

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public abstract bool NotifyLockedDeliverypoint();

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public abstract string ErrorMessage
        {
            get;
            set;
        }

    }
}
