﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// MultilevelPricingGroup class
    /// </summary>
    public class MultilevelPricingGroup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MultilevelPricingGroup"/> class.
        /// </summary>
        public MultilevelPricingGroup()
        {
            //Using a dictionary for fast lookups
            ProductPLUs = new Dictionary<int,bool>();
        }

        /// <summary>
        /// Adds the PLU.
        /// </summary>
        /// <param name="plu">The plu.</param>
        public void AddPLU(int plu)
        {
            if (!ProductPLUs.ContainsKey(plu))
            {
                ProductPLUs.Add(plu, true);
            }
        }

        /// <summary>
        /// Determines whether the specified plu has PLU.
        /// </summary>
        /// <param name="plu">The plu.</param>
        /// <returns>
        ///   <c>true</c> if the specified plu has PLU; otherwise, <c>false</c>.
        /// </returns>
        public bool HasPLU(int plu)
        {
            return ProductPLUs.ContainsKey(plu);
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text
        { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        /// <value>
        /// The level.
        /// </value>
        public int Level
        { get; set; }

        Dictionary<int, bool> ProductPLUs;
    }
}
