﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// FunctionkeyXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "DATA")]
    public class FunctionkeyXml
    {
        /// <summary>
        /// Gets or sets the record.
        /// </summary>
        /// <value>
        /// The record.
        /// </value>
        [XmlElementAttribute(ElementName = "RECORD")]
        public int Record { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlElementAttribute(ElementName = "NAME")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [XmlElementAttribute(ElementName = "VALUE")]
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets the keytype.
        /// </summary>
        /// <value>
        /// The keytype.
        /// </value>
        [XmlElementAttribute(ElementName = "KEYTYPE")]
        public int Keytype { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [XmlElementAttribute(ElementName = "STATUS")]
        public int Status { get; set; }
    }
}
