﻿using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// IcrtouchConfigurationAdapter class
    /// </summary>
    public class IcrtouchConfigurationAdapter : IConfigurationAdapter
    {

        #region Methods
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.IpAddress = terminal.PosValue1;
            this.CashierName = terminal.PosValue2;
            this.CashierNumber = terminal.PosValue3;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.IpAddress;
            terminal.PosValue2 = this.CashierName;
            terminal.PosValue3 = this.CashierNumber;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.IpAddress = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.IcrtouchIpAddress);
            this.CashierName = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.IcrtouchCashierName);
            this.CashierNumber = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.IcrtouchCashierNumber);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.IcrtouchIpAddress, this.IpAddress);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.IcrtouchCashierName, this.CashierName);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.IcrtouchCashierNumber, this.CashierNumber);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.IpAddress;
            terminal.PosValue2 = this.CashierName;
            terminal.PosValue3 = this.CashierNumber;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            this.IpAddress = terminal.PosValue1;
            this.CashierName = terminal.PosValue2;
            this.CashierNumber = terminal.PosValue3;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        public string IpAddress { get; set; }
        /// <summary>
        /// Gets or sets the name of the cashier.
        /// </summary>
        /// <value>
        /// The name of the cashier.
        /// </value>
        public string CashierName { get; set; }
        /// <summary>
        /// Gets or sets the cashier number.
        /// </summary>
        /// <value>
        /// The cashier number.
        /// </value>
        public string CashierNumber { get; set; }

        #endregion
    }
}
