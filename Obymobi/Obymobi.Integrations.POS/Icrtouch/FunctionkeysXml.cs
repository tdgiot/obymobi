﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// FunctionkeysXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PROGRAMDATA")]
    public class FunctionkeysXml
    {
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [XmlElementAttribute("DATA", IsNullable = true)]
        public FunctionkeyXml[] Data { get; set; }
    }

}
