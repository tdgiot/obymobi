﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// ButtonXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "DATA")]
    public class ButtonXml
    {
        /// <summary>
        /// Gets or sets the record.
        /// </summary>
        /// <value>
        /// The record.
        /// </value>
        [XmlElementAttribute(ElementName = "RECORD")]
        public int Record { get; set; }

        /// <summary>
        /// Gets or sets the fill colour.
        /// </summary>
        /// <value>
        /// The fill colour.
        /// </value>
        [XmlElementAttribute(ElementName = "FILLCOLOUR")]
        public int FillColour { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [XmlElementAttribute(ElementName = "TEXT")]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the item file.
        /// </summary>
        /// <value>
        /// The item file.
        /// </value>
        [XmlElementAttribute(ElementName = "ITEMFILE")]
        public string ItemFile { get; set; }

        /// <summary>
        /// Gets or sets the item record.
        /// </summary>
        /// <value>
        /// The item record.
        /// </value>
        [XmlElementAttribute(ElementName = "ITEMRECORD")]
        public int ItemRecord { get; set; }

        /// <summary>
        /// Gets or sets the left.
        /// </summary>
        /// <value>
        /// The left.
        /// </value>
        [XmlElementAttribute(ElementName = "LEFT")]
        public int Left { get; set; }

        /// <summary>
        /// Gets or sets the top.
        /// </summary>
        /// <value>
        /// The top.
        /// </value>
        [XmlElementAttribute(ElementName = "TOP")]
        public int Top { get; set; }

        /// <summary>
        /// Gets or sets the right.
        /// </summary>
        /// <value>
        /// The right.
        /// </value>
        [XmlElementAttribute(ElementName = "RIGHT")]
        public int Right { get; set; }

        /// <summary>
        /// Gets or sets the bottom.
        /// </summary>
        /// <value>
        /// The bottom.
        /// </value>
        [XmlElementAttribute(ElementName = "BOTTOM")]
        public int Bottom { get; set; }

        /// <summary>
        /// Gets or sets the font.
        /// </summary>
        /// <value>
        /// The font.
        /// </value>
        [XmlElementAttribute(ElementName = "FONT")]
        public int Font { get; set; }

        /// <summary>
        /// Gets or sets the text colour.
        /// </summary>
        /// <value>
        /// The text colour.
        /// </value>
        [XmlElementAttribute(ElementName = "TEXTCOLOUR")]
        public int TextColour { get; set; }
    }
}
