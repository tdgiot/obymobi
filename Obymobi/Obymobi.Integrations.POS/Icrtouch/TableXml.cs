﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// TableXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "TABLE")]
    public class TableXml
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        [XmlAttribute(AttributeName = "number")]
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the tablenumber.
        /// </summary>
        /// <value>
        /// The tablenumber.
        /// </value>
        [XmlAttribute(AttributeName = "tablenumber")]
        public string Tablenumber { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        /// <value>
        /// The level.
        /// </value>
        [XmlAttribute(AttributeName = "level")]
        public string Level { get; set; }
    }

}
