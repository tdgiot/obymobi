﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{

    /// <summary>
    /// ProductXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PRODUCT")]
    public class ProductXml
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        [XmlAttribute(AttributeName = "number")]
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the department.
        /// </summary>
        /// <value>
        /// The department.
        /// </value>
        [XmlAttribute(AttributeName = "debt")]
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets the plu group.
        /// </summary>
        /// <value>
        /// The plu group.
        /// </value>
        [XmlAttribute(AttributeName = "plugroup")]
        public string PluGroup { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        [XmlAttribute(AttributeName = "price1")]
        public string Price { get; set; }

        /// <summary>
        /// Gets or sets the price2.
        /// </summary>
        /// <value>
        /// The price2.
        /// </value>
        [XmlAttribute(AttributeName = "price2")]
        public string Price2 { get; set; }

        /// <summary>
        /// Gets or sets the price3.
        /// </summary>
        /// <value>
        /// The price3.
        /// </value>
        [XmlAttribute(AttributeName = "price3")]
        public string Price3 { get; set; }

        /// <summary>
        /// Gets or sets the condiment.
        /// </summary>
        /// <value>
        /// The condiment.
        /// </value>
        [XmlAttribute(AttributeName = "condiment")]
        public string Condiment { get; set; }

        /// <summary>
        /// Gets or sets the list plus.
        /// </summary>
        /// <value>
        /// The list plus.
        /// </value>
        [XmlElementAttribute("LISTPLU", IsNullable = true)]
        public ProductListpluXML[] ListPlus { get; set; }

        /// <summary>
        /// ProductListpluXML class
        /// </summary>
        [Serializable, XmlRootAttribute(ElementName = "LISTPLU")]
        public class ProductListpluXML
        {
            /// <summary>
            /// Gets or sets the number.
            /// </summary>
            /// <value>
            /// The number.
            /// </value>
            [XmlAttribute(AttributeName = "number")]
            public string Number { get; set; }
        }
    }
}
