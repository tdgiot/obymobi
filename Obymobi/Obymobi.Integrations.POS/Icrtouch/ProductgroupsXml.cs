﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// ProductgroupsXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PRODUCTGROUPS")]
    public class ProductgroupsXml
    {
        /// <summary>
        /// Gets or sets the productgroups.
        /// </summary>
        /// <value>
        /// The productgroups.
        /// </value>
        [XmlElementAttribute("PRODUCTGROUP", IsNullable = true)]
        public ProductgroupXml[] Productgroups { get; set; }
    }

}
