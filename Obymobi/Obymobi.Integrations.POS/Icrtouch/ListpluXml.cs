﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// ListpluXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "LISTPLU")]
    public class ListpluXml
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        [XmlAttribute(AttributeName = "number")]
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        /// <value>
        /// The products.
        /// </value>
        [XmlElementAttribute("PRODUCT", IsNullable = true)]
        public ListpluProductXml[] Products { get; set; }

        /// <summary>
        /// ListpluProductXml class
        /// </summary>
        [Serializable, XmlRootAttribute(ElementName = "PRODUCT")]
        public class ListpluProductXml
        {
            /// <summary>
            /// Gets or sets the number.
            /// </summary>
            /// <value>
            /// The number.
            /// </value>
            [XmlAttribute(AttributeName = "number")]
            public string Number { get; set; }
        }
    }

}
