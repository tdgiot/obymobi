﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// ProductsXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PRODUCTS")]
    public class ProductsXml
    {
        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        /// <value>
        /// The products.
        /// </value>
        [XmlElementAttribute("PRODUCT", IsNullable = true)]
        public ProductXml[] Products { get; set; }
    }

}
