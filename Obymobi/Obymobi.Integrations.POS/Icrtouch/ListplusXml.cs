﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// ListplusXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "LISTPLUS")]
    public class ListplusXml
    {
        /// <summary>
        /// Gets or sets the listplus.
        /// </summary>
        /// <value>
        /// The listplus.
        /// </value>
        [XmlElementAttribute("LISTPLU", IsNullable = true)]
        public ListpluXml[] Listplus { get; set; }
    }

}
