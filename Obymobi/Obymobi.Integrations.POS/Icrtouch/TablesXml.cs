﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// TablesXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "TABLES")]
    public class TablesXml
    {
        /// <summary>
        /// Gets or sets the tables.
        /// </summary>
        /// <value>
        /// The tables.
        /// </value>
        [XmlElementAttribute("TABLE", IsNullable = true)]
        public TableXml[] Tables { get; set; }
    }

}
