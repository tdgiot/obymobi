﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// ResponseMessageType enums
    /// </summary>
    public enum ResponseMessageType
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown,
        /// <summary>
        /// ResponseOk
        /// </summary>
        ResponseOk,
        /// <summary>
        /// ResponseCancel
        /// </summary>
        ResponseCancel,
        /// <summary>
        /// RetryError
        /// </summary>
        RetryError,
        /// <summary>
        /// FailedError
        /// </summary>
        FailedError,
        /// <summary>
        /// DisplayMessage
        /// </summary>
        DisplayMessage,
        /// <summary>
        /// MessageDialog
        /// </summary>
        MessageDialog,
        /// <summary>
        /// GetNumber
        /// </summary>
        GetNumber,
        /// <summary>
        /// GetText
        /// </summary>
        GetText,
        /// <summary>
        /// ItemList
        /// </summary>
        ItemList
    }
}
