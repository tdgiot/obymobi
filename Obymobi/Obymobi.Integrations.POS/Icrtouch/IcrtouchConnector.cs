﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Obymobi.Logic.Loggers;
using Dionysos;
using Obymobi.Logic.POS.Unitouch.Enums;
using Obymobi.Logic.Model;
using System.Globalization;
using Obymobi.Enums;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Text.RegularExpressions;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.POS.Untill;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// IcrtouchConnector class
    /// </summary>
    public class IcrtouchConnector : IPOSConnector
    {
        #region Fields

        private List<ProductgroupXml> productGroupXmls = new List<ProductgroupXml>();
        private List<MultilevelPricingGroup> multiLevelPricingGroups = new List<MultilevelPricingGroup>();
        private string ip = string.Empty;
        private int port = 5559;
        private Socket socket = null;
        private IPAddress ipAddress = null;
        private IPEndPoint ipEndPoint = null;
        private string cashierName = string.Empty;
        private string cashierNumber = string.Empty;

        private int shortSleep = 100;
        private int longSleep = 200;

        private string errorMessage = string.Empty;
        //private bool icrTouchVersionInfoPrinted = false;
        //private List<int> orderedForDeliverypointsDebugOnly = new List<int>();   

        private const int NUM_SCREENLEVELS = 65;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IcrtouchConnector"/> class.
        /// </summary>
        public IcrtouchConnector()
        {
            this.Initialize();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IcrtouchConnector"/> class.
        /// </summary>
        public IcrtouchConnector(PosIntegrationInformation posIntegrationInformation)
        {
            this.Initialize();
        }

        #endregion

        #region Methods

        #region IPOSConnector methods

        public void PrePosSynchronisation()
        {
            // Noshing to do here.
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public Model.Posproduct[] GetPosproducts()
        {
            List<Posproduct> posproducts = new List<Posproduct>();

            this.WriteToLogExtended("GetPosproducts", "Start {0}", TimeStamp.CreateTimeStamp());

            bool hasError = false;

            if (!hasError)
            {
                // Get the products
                try
                {
                    ProductsXml products = this.GetProducts("all", string.Empty, string.Empty);
                    ListplusXml listplus = this.GetListplus("all");

                    //Get/update all multilevel pricing groups
                    GetMultiLevelPricingData();

                    if (products.Products.Length > 0)
                    {
                        foreach (var product in products.Products)
                        {
                            Posproduct posproduct = SetupPosProduct(product, 1, products, listplus);
                            if (posproduct != null)
                                posproducts.Add(posproduct);

                            if (product.Price2 != null)
                            {
                                posproduct = SetupPosProduct(product, 2, products, listplus);
                                if (posproduct != null)
                                    posproducts.Add(posproduct);
                            }

                            if (product.Price3 != null)
                            {
                                posproduct = SetupPosProduct(product, 3, products, listplus);
                                if (posproduct != null)
                                    posproducts.Add(posproduct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    hasError = true;
                    this.errorMessage = string.Format("An exception was thrown while trying to get the products from the POS. Exception: {0}", ex.Message);
                }
            }

            if (hasError)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, this.errorMessage);

            this.WriteToLogExtended("GetPosproducts", "End {0}", TimeStamp.CreateTimeStamp());

            return posproducts.ToArray();
        }

        /// <summary>
        /// Setups the pos product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="priceLevel">The price level.</param>
        /// <param name="products">The products.</param>
        /// <param name="listplus">The listplus.</param>
        /// <returns></returns>
        public Posproduct SetupPosProduct(ProductXml product, int priceLevel, ProductsXml products, ListplusXml listplus)
        {
            Posproduct posproduct = new Posproduct();
            if (product.Name.IsNullOrWhiteSpace() || product.Name.StartsWith("PLU"))
            {
                // No name or (not a condiment & no price)
                return null;
            }

            MultilevelPricingGroup priceLevelGroup = null;
            if (priceLevel > 1)
            {
                priceLevelGroup = GetMultiLevelPricingGroup(priceLevel, int.Parse(product.Number));
                if (priceLevelGroup != null)
                {
                    //Products with more price levels will have a "+{pricelevel}" post-fix
                    posproduct.ExternalId = product.Number + "+" + priceLevel;
                    posproduct.Name = priceLevelGroup.Text + " " + product.Name;

                    //We will use FieldValue4 for the real PLU and FieldValue5 for the price level
                    posproduct.FieldValue4 = product.Number;
                    posproduct.FieldValue5 = "" + priceLevel;
                }
                else
                {
                    //Can't find the necessary information, bail out
                    return null;
                }
            }
            else
            {
                posproduct.ExternalId = product.Number;
                posproduct.Name = product.Name;
            }

            string strPrice = product.Price;
            if (priceLevel == 2)
                strPrice = product.Price2;
            else if (priceLevel == 3)
                strPrice = product.Price3;

            decimal price = 0;
            if (decimal.TryParse(strPrice, NumberStyles.Number, CultureInfo.InvariantCulture, out price))
                posproduct.PriceIn = price;


            posproduct.VatTariff = 2;
            posproduct.FieldValue1 = product.Condiment;
            posproduct.FieldValue2 = product.Department;
            posproduct.FieldValue3 = product.PluGroup;

            // Category
            var productGroup = this.productGroupXmls.FirstOrDefault(group => group.Products != null && group.Products.Any(productInGroup => productInGroup.Number == product.Number));
            if (productGroup != null)
                posproduct.ExternalPoscategoryId = productGroup.Number;

            // Alterations
            List<Posalteration> posAlterations = new List<Posalteration>();
            if (product.ListPlus != null)
            {
                foreach (var listpluId in product.ListPlus)
                {
                    // find the listplu
                    var listPlu = listplus.Listplus.SingleOrDefault(lp => lp.Number == listpluId.Number);

                    if (listPlu != null)
                    {

                        Posalteration pa = new Posalteration();
                        pa.ExternalId = listPlu.Number;
                        pa.Name = listPlu.Name;

                        List<Posalterationoption> paos = new List<Posalterationoption>();
                        foreach (var optionProduct in listPlu.Products)
                        {
                            Posalterationoption pao = new Posalterationoption();
                            pao.PosproductExternalId = optionProduct.Number;
                            pao.ExternalId = pa.ExternalId + "-" + optionProduct.Number;
                            // Find the product
                            var productForOption = products.Products.SingleOrDefault(p => p.Number == optionProduct.Number);

                            // If found set some additional values
                            if (productForOption != null)
                            {
                                pao.Name = productForOption.Name;

                                if (decimal.TryParse(productForOption.Price, NumberStyles.Number, CultureInfo.InvariantCulture, out price))
                                    pao.PriceIn = price;
                            }

                            // Add to collection
                            paos.Add(pao);
                        }

                        // Convert PosalterationOption collection to array and assign to the Alteration
                        pa.Posalterationoptions = paos.ToArray();

                        // Add the alteration to the List of alterations
                        posAlterations.Add(pa);
                    }
                }
            }

            // Assigned the PosAlterations to the Product
            posproduct.Posalterations = posAlterations.ToArray();
            return posproduct;
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public Model.Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();
            this.productGroupXmls = new List<ProductgroupXml>();

            this.WriteToLogExtended("GetPoscategories", "Start {0}", TimeStamp.CreateTimeStamp());

            bool hasError = false;


            if (!hasError)
            {
                // Get the categories
                try
                {
                    // Retrieve the productgroups
                    //ProductgroupXml t;
                    ProductgroupsXml productgroups = this.GetProductgroups();

                    if (productgroups != null && productgroups.Productgroups != null && productgroups.Productgroups.Length > 0)
                    {
                        foreach (var group in productgroups.Productgroups)
                        {
                            Poscategory poscategory = new Poscategory();
                            poscategory.ExternalId = group.Number;
                            poscategory.Name = group.Name;
                            poscategories.Add(poscategory);

                            // Retrieve the content of all the productgroups, which we later be used in GetProducts
                            ProductgroupXml groupXml = this.GetProductgroup(group.Number, string.Empty);
                            this.productGroupXmls.Add(groupXml);
                        }
                    }
                }
                catch (Exception ex)
                {
                    hasError = true;
                    this.errorMessage = string.Format("An exception was thrown while trying to get the categories from the POS. Exception: {0}", ex.Message);
                    throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, ex, errorMessage);
                }
            }

            if (hasError)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, this.errorMessage);

            this.WriteToLogExtended("GetPoscategories", "End {0}", TimeStamp.CreateTimeStamp());

            return poscategories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            return new Model.Posdeliverypointgroup[0];
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            this.WriteToLogExtended("GetPosdeliverypoints", "Start {0}", TimeStamp.CreateTimeStamp());

            bool hasError = false;

            if (!hasError)
            {
                // Get the categories
                try
                {
                    // Retrieve the productgroups
                    TablesXml tables = this.GetTables("all");

                    if (tables.Tables.Length > 0)
                    {
                        foreach (var table in tables.Tables)
                        {
                            Posdeliverypoint posdeliverypoint = new Posdeliverypoint();
                            posdeliverypoint.ExternalId = table.Tablenumber;
                            posdeliverypoint.Number = table.Tablenumber;
                            posdeliverypoint.Name = table.Tablenumber;
                            posdeliverypoints.Add(posdeliverypoint);
                        }
                    }
                }
                catch (Exception ex)
                {
                    hasError = true;
                    this.errorMessage = string.Format("An exception was thrown while trying to get the categories from the POS. Exception: {0}", ex.Message);
                }
            }

            if (hasError)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosDeliverypoints, this.errorMessage);

            this.WriteToLogExtended("GetPosdeliverypoints", "End {0}", TimeStamp.CreateTimeStamp());

            return posdeliverypoints.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public Model.Posorder GetPosorder(string deliverypointNumber)
        {
            return null;
        }

        /// <summary>
        /// Saves an order to the point-of-sale
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            OrderProcessingError processingError = OrderProcessingError.None;

            this.WriteToLogExtended("SaveOrder", "Start {0}", TimeStamp.CreateTimeStamp());

            if (processingError == OrderProcessingError.None)
            {
                SendSaleXml sendSaleXml = new SendSaleXml();

                // Header
                sendSaleXml.Header.Consec = posorder.PosorderId.ToString();
                sendSaleXml.Header.Mcid = "Otoucho";
                sendSaleXml.Header.Cashier.Name = this.cashierName;
                sendSaleXml.Header.Cashier.Number = this.cashierNumber;

                // KPREROUTE
                if (posorder.FieldValue1 != null && posorder.FieldValue1.Length > 0)
                {
                    string[] mappings = posorder.FieldValue1.Split(';', StringSplitOptions.RemoveEmptyEntries);

                    sendSaleXml.Header.Kpreroute = new SendSaleXml.SendSaleKprerouteXml();

                    foreach (var mappingText in mappings)
                    {
                        string[] mapping = mappingText.Split(',', StringSplitOptions.RemoveEmptyEntries);

                        if (mapping[0] == "1")
                            sendSaleXml.Header.Kpreroute.Route1 = mapping[1];

                        if (mapping[0] == "2")
                            sendSaleXml.Header.Kpreroute.Route2 = mapping[1];

                        if (mapping[0] == "3")
                            sendSaleXml.Header.Kpreroute.Route3 = mapping[1];

                        if (mapping[0] == "4")
                            sendSaleXml.Header.Kpreroute.Route4 = mapping[1];

                        if (mapping[0] == "5")
                            sendSaleXml.Header.Kpreroute.Route5 = mapping[1];

                        if (mapping[0] == "6")
                            sendSaleXml.Header.Kpreroute.Route6 = mapping[1];

                        if (mapping[0] == "7")
                            sendSaleXml.Header.Kpreroute.Route7 = mapping[1];

                        if (mapping[0] == "8")
                            sendSaleXml.Header.Kpreroute.Route8 = mapping[1];

                        if (mapping[0] == "9")
                            sendSaleXml.Header.Kpreroute.Route9 = mapping[1];

                        if (mapping[0] == "10")
                            sendSaleXml.Header.Kpreroute.Route10 = mapping[1];
                    }
                }


                // Add the products
                decimal totalValue = 0;
                int totalQuantity = 0;
                List<SendSaleXml.SendSaleProductXml> products = new List<SendSaleXml.SendSaleProductXml>();
                foreach (var orderitem in posorder.Posorderitems)
                {
                    SendSaleXml.SendSaleProductXml productXml = new SendSaleXml.SendSaleProductXml();
                    if (!StringUtil.IsNullOrWhiteSpace(orderitem.FieldValue4))
                    {
                        productXml.Number = orderitem.FieldValue4;
                        productXml.Priceshift = orderitem.FieldValue5;
                        productXml.Text = orderitem.Description;
                    }
                    else
                    {
                        productXml.Number = orderitem.PosproductExternalId;
                    }

                    productXml.Quantity = orderitem.Quantity.ToString("F2", CultureInfo.InvariantCulture.NumberFormat);

                    // Keep track of totals
                    decimal totalPrice = orderitem.PriceIn;
                    productXml.Value = totalPrice.ToString("F2", CultureInfo.InvariantCulture.NumberFormat);
                    totalValue += totalPrice;


                    totalQuantity += orderitem.Quantity;

                    // Add product to temp list
                    products.Add(productXml);
                }

                // Write list to content as products.
                sendSaleXml.Content.Products = products.ToArray();

                // Write an empty Table message
                sendSaleXml.Content.TextMessage = " ";

                // Final
                sendSaleXml.Final.SendToTable = posorder.PosdeliverypointExternalId;
                sendSaleXml.Final.Quantity = totalQuantity.ToString("F2", CultureInfo.InvariantCulture.NumberFormat);
                sendSaleXml.Final.TotalValue = totalValue.ToString("F2", CultureInfo.InvariantCulture.NumberFormat);
                //sendSaleXml.Final.PrintReceipt = string.Empty;

                string command = this.Serialize(sendSaleXml);

                // Because XmlSerializer doesnlt allow <1>value</1> (invalid XML required by ICR) we fix that here:
                command = command.Replace("X1X", "1");
                command = command.Replace("X2X", "2");
                command = command.Replace("X3X", "3");
                command = command.Replace("X4X", "4");
                command = command.Replace("X5X", "5");
                command = command.Replace("X6X", "6");
                command = command.Replace("X7X", "7");
                command = command.Replace("X8X", "8");
                command = command.Replace("X9X", "9");
                command = command.Replace("X10X", "10");

                string response = this.DoCommand(command, 32000, this.shortSleep);

                this.WriteToLogExtended("SaveOrder", "Result: {0}", response);

                string responseText;
                ResponseMessageType responseType = this.ParseResponse(response, out responseText);

                if (responseType == ResponseMessageType.ResponseOk)
                {
                    // DONE!
                    //string icrError = "\r\n\r\n ----------------------------- SUCCESS ---------------------------------";
                    //icrError += "r\n" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fffff") + "\r\n";
                    //icrError += "Table: " + posorder.PosdeliverypointExternalId + "\r\n";
                    //icrError += "Command: " + command + "\r\n";
                    //icrError += "Response Type: " + responseType.ToString() + "\r\n";
                    //icrError += "Response Text: " + responseText + "\r\n";
                    //icrError += "\r\n Full response: " + response;
                    //icrError += "\r\n\r\n --------------------------------------------------------------";
                    //this.WriteToIcrLog(icrError);                    
                }
                else if (responseType == ResponseMessageType.RetryError)
                {
                    // ERROR THAT CAN BE RETRIED
                    string icrError = "\r\n\r\n ------------------------ FAILURE: RETRYABLE ERROR ---------------------------------";
                    icrError += "\r\n" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fffff") + "\r\n";
                    icrError += "Table: " + posorder.PosdeliverypointExternalId + "\r\n";
                    icrError += "Command: " + command + "\r\n";
                    icrError += "Response Type: " + responseType.ToString() + "\r\n";
                    icrError += "Response Text: " + responseText + "\r\n";
                    icrError += "Full response: " + response;
                    icrError += "\r\n\r\n --------------------------------------------------------------\r\n";
                    this.WriteToIcrLog(icrError);

                    this.WriteToLogNormal("Order not processed Retry Error: {0}", response);

                    // We don't retry, we do that ourselves.
                    this.DoCommand("<RETRY>NO</RETRY>", 32000, this.shortSleep);

                    throw new DeliverypointLockedException(responseText);
                }
                else if (responseType == ResponseMessageType.DisplayMessage || responseText.Contains("EINUSE"))
                {
                    // MESSAGE DISPLAYED, RETRY
                    this.WriteToLogNormal("Order not processed - Display Message: {0}", response);

                    string icrError = "\r\n\r\n ------------------------ FAILURE: DISPLAY MESSAGE ---------------------------------";
                    icrError += "\r\n" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fffff") + "\r\n";
                    icrError += "Table: " + posorder.PosdeliverypointExternalId + "\r\n";
                    icrError += "Command: " + command + "\r\n";
                    icrError += "Response Type: " + responseType.ToString() + "\r\n";
                    icrError += "Response Text: " + responseText + "\r\n";
                    icrError += "Full response: " + response + "\r\n";
                    icrError += "Now throw DeliverypointLockedException\r\n";
                    icrError += "\r\n\r\n --------------------------------------------------------------\r\n";
                    this.WriteToIcrLog(icrError);

                    throw new DeliverypointLockedException(responseText);
                }
                else
                {
                    string icrError = "\r\n\r\n ------------------------ FAILURE: UNKOWN ERROR ---------------------------------";
                    icrError += "\r\n" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fffff") + "\r\n";
                    icrError += "Table: " + posorder.PosdeliverypointExternalId + "\r\n";
                    icrError += "Command: " + command + "\r\n";
                    icrError += "Response Type: " + responseType.ToString() + "\r\n";
                    icrError += "Response Text: " + responseText + "\r\n";
                    icrError += "Full response: " + response;
                    icrError += "\r\n\r\n --------------------------------------------------------------\r\n";
                    this.WriteToIcrLog(icrError);

                    throw new POSException(OrderProcessingError.PosErrorSaveOrderFailed, string.Format("Order Save Failed: {0}", response));
                }
            }

            this.WriteToLogExtended("SaveOrder", "End {0}", TimeStamp.CreateTimeStamp());

            return processingError;
        }

        // GK to be moved to generic helper class
        string Serialize(object source)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.Indent = false;
            settings.NewLineChars = string.Empty;
            settings.NewLineHandling = NewLineHandling.None;

            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            StringBuilder sb = new StringBuilder();

            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                XmlSerializer serializer = new XmlSerializer(source.GetType());
                serializer.Serialize(writer, source, namespaces);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            return true;
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Initializes the Unitouch connector
        /// </summary>
        private void Initialize()
        {
            try
            {
                this.ip = ConfigurationManager.GetString(POSConfigurationConstants.IcrtouchIpAddress);
                this.port = 5559;
                this.cashierName = ConfigurationManager.GetString(POSConfigurationConstants.IcrtouchCashierName);
                this.cashierNumber = ConfigurationManager.GetString(POSConfigurationConstants.IcrtouchCashierNumber);

                string versionInfo = this.DoCommand("<GETVERSIONINFO />", 32000, this.shortSleep);
                this.WriteToLogNormal("Initialize", "VersionInfo: {0}", versionInfo);
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector,
                    "An exception was thrown while initializing the IcrtouchConnector. Exception: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Connects the ICRTouch connector with the POS
        /// </summary>
        /// <returns>True if connecting with the POS was successful, False if not</returns>
        public string Connect()
        {
            string response = string.Empty;

            this.WriteToLogExtended("Connect", "Start");

            if (this.socket != null && this.socket.Connected)
            {
                this.WriteToLogExtended("Connect", "Already connected.");
                return "Already connected";
            }

            byte[] bytes = new byte[256];
            try
            {
                this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                this.socket.SendTimeout = 9000;
                this.socket.ReceiveTimeout = 16000;

                this.ipAddress = IPAddress.Parse(this.ip);
                this.ipEndPoint = new IPEndPoint(ipAddress, this.port);
                this.socket.Connect(ipEndPoint);
                if (!this.socket.Connected)
                    throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("The Icrtouchconnector could not connect to the POS on '{0}:{1}'", this.ip, this.port));

                byte[] data = System.Text.Encoding.ASCII.GetBytes("<DEVICEINFO><PROTOCOLVERSION>1.0</PROTOCOLVERSION></DEVICEINFO>\r\n");
                int result = this.socket.Send(data);
                this.socket.Send(data);
            }
            catch (Exception ex)
            {
                throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("An exception was thrown while connecting the Icrtouchconnector. Exception: {0}", ex.Message));
            }

            this.WriteToLogExtended("Connect", "End");

            return response;
        }


        /// <summary>
        /// Does the command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="bufferSize">Size of the buffer.</param>
        /// <param name="sleep">The sleep.</param>
        /// <returns></returns>
        public string DoCommand(string command, int bufferSize, int sleep)
        {
            string response = string.Empty;
            bool wasError = false;
            StringBuilder icrLog = new StringBuilder();

            icrLog.Append("\r\n------------------------ START COMMAND -------------------------\r\n");
            icrLog.AppendFormat("Command: {0}\r\n", command);

            this.WriteToLogExtended("DoCommand - Start", command); // Remove the last \n in the log
            try
            {
                this.Connect();

                // Execute the command string
                try
                {
                    command += "\r\n";
                    byte[] sendData = System.Text.Encoding.ASCII.GetBytes(command);
                    int result = this.socket.Send(sendData);

                    //System.Threading.Thread.Sleep(750);

                    bool keepReceiving = true;
                    StringBuilder sb = new StringBuilder();
                    while (keepReceiving)
                    {
                        // Receive data from socket
                        // System.Threading.Thread.Sleep(this.shortSleep);

                        byte[] receivedData = new byte[bufferSize];
                        int receivedSize = this.socket.Receive(receivedData, 0, bufferSize, 0);

                        // Resize array to length of content
                        Array.Resize(ref receivedData, receivedSize);

                        string encodedString = Encoding.UTF8.GetString(receivedData);

                        sb.Append(encodedString);

                        if (encodedString.Contains("\n"))
                        {
                            keepReceiving = false;
                            //Debug.WriteLine("Terminator Found");
                        }
                        //else
                        //Debug.WriteLine("Terminator Not Yet Found");
                    }

                    response = sb.ToString();

                    icrLog.AppendFormat("Result: {0}\r\n", response);

                }
                catch (Exception ex)
                {
                    wasError = true;
                    icrLog.AppendFormat("Exception with POS: {0}", ex.Message);
                    throw new POSException(OrderProcessingError.PosErrorApiInterfaceException, string.Format("An exception was thrown while trying the execute command '{0}'. Exception: {1}", command, ex.Message));
                }
            }
            finally
            {
                this.socket.Disconnect(false);
                this.socket = null;

                icrLog.Append("------------------------ END COMMAND -------------------------\r\n");
                this.WriteToLogExtended("DoCommand", icrLog.ToString());

                if (wasError)
                    this.WriteToIcrLog(icrLog.ToString());
            }

            return response;
        }

        /// <summary>
        /// Writes to log normal.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Debug("IcrtouchConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes to log extended.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("IcrtouchConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public void WriteToIcrLog(string contents, params object[] args)
        {
            Debug.WriteLine(string.Format(contents, args));

            // Create the Logs directory if it doesn't exists yet
            string dir = IcrtouchConnector.CreateLogDirectory("Logs");

            DateTime now = DateTime.Now;

            // Set up a filestream
            FileStream fs = new FileStream(Path.Combine(dir, string.Format("{0}{1}{2}-IcrTouch.log", now.Year.ToString("0000"), now.Month.ToString("00"), now.Day.ToString("00"))), FileMode.OpenOrCreate, FileAccess.Write);

            // Set up a streamwriter for adding text
            StreamWriter sw = new StreamWriter(fs);

            // Find the end of the underlying filestream
            sw.BaseStream.Seek(0, SeekOrigin.End);

            // Add the text
            try
            {
                sw.Write(string.Format(contents, args));
            }
            catch
            {
                sw.WriteLine("String.Format failed");
                sw.WriteLine(string.Format("{0}:{1}:{2}", now.Hour.ToString("00"), now.Minute.ToString("00"), now.Second.ToString("00")));
                sw.WriteLine(contents);
                sw.WriteLine("Args: ", StringUtil.CombineWithSeperator(", ", args));
            }
            // Add the text to the underlying filestream
            sw.Flush();
            // Close the writer
            sw.Close();
        }

        /// <summary>
        /// Creates a directory relative to the application directory
        /// </summary>
        /// <param name="relativeDir">The directory relative to the application directory</param>
        /// <returns>The full path to the relative directory</returns>
        private static string CreateLogDirectory(string relativeDir)
        {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dir = Path.Combine(directory, relativeDir);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        private static Regex ResponseRegex = new Regex(@"<RESPONSE>(?<response>.*?)</RESPONSE>", RegexOptions.IgnoreCase);
        private static Regex RetryErrorRegex = new Regex(@"<RETRYERROR>(?<response>.*?)</RETRYERROR>", RegexOptions.IgnoreCase);
        private static Regex FailedErrorRegex = new Regex(@"<FAILEDERROR error=""(?<error>.*?)"">(?<description>.*?)</FAILEDERROR>", RegexOptions.IgnoreCase);
        private static Regex DisplayMessageRegex = new Regex(@"<DISPLAYMESSAGE>(?<response>.*?)</DISPLAYMESSAGE>", RegexOptions.IgnoreCase);
        private static Regex MessageDialogRegex = new Regex(@"<MESSAGEDIALOG kind=""(?<kind>.*?)"">(?<question>.*?)</MESSAGEDIALOG>", RegexOptions.IgnoreCase);
        private static Regex GetNumberRegex = new Regex(@"<GETNUMBER maxlength=""(?<maxlength>.*?)"">(?<response>.*?)</GETNUMBER>", RegexOptions.IgnoreCase);
        private static Regex GetTextRegex = new Regex(@"<GETTEXT maxlength=""(?<maxlength>.*?)"">(?<response>.*?)</GETTEXT>", RegexOptions.IgnoreCase);
        private static Regex ItemListRegex = new Regex(@"<ITEMLIST>(?<response>.*?)</ITEMLIST>", RegexOptions.IgnoreCase);


        /// <summary>
        /// Parses the response.
        /// </summary>
        /// <param name="responseXml">The response XML.</param>
        /// <param name="description">The description.</param>
        /// <returns></returns>
        private ResponseMessageType ParseResponse(string responseXml, out string description)
        {
            ResponseMessageType toReturn = ResponseMessageType.Unknown;
            description = responseXml;
            MatchCollection matches;

            if (IcrtouchConnector.ResponseRegex.IsMatch(responseXml))
            {
                // RESPONSE
                matches = IcrtouchConnector.ResponseRegex.Matches(responseXml);
                description = matches[0].Groups["response"].Value;
                toReturn = description.Equals("OK", StringComparison.InvariantCultureIgnoreCase) ? ResponseMessageType.ResponseOk : ResponseMessageType.ResponseCancel;
            }
            else if (IcrtouchConnector.RetryErrorRegex.IsMatch(responseXml))
            {
                // RETRYERROR
                toReturn = ResponseMessageType.RetryError;
                matches = IcrtouchConnector.RetryErrorRegex.Matches(responseXml);
                description = matches[0].Groups["response"].Value;
            }
            else if (IcrtouchConnector.FailedErrorRegex.IsMatch(responseXml))
            {
                // FAILEDERROR
                toReturn = ResponseMessageType.FailedError;
                matches = IcrtouchConnector.FailedErrorRegex.Matches(responseXml);
                description = string.Format("Error: {0} - {1}", matches[0].Groups["error"].Value, matches[0].Groups["description"].Value);
            }
            else if (IcrtouchConnector.DisplayMessageRegex.IsMatch(responseXml))
            {
                // DISPLAYMESSAGE
                toReturn = ResponseMessageType.DisplayMessage;
                matches = IcrtouchConnector.DisplayMessageRegex.Matches(responseXml);
                description = matches[0].Groups["response"].Value;
            }
            else if (IcrtouchConnector.MessageDialogRegex.IsMatch(responseXml))
            {
                // MESSAGE DIALOG
                toReturn = ResponseMessageType.MessageDialog;
                matches = IcrtouchConnector.MessageDialogRegex.Matches(responseXml);
                description = string.Format("Message Dialog: {0} - {1}", matches[0].Groups["kind"].Value, matches[0].Groups["question"].Value);
            }
            else if (IcrtouchConnector.GetNumberRegex.IsMatch(responseXml))
            {
                // GET NUMBER
                toReturn = ResponseMessageType.GetNumber;
                matches = IcrtouchConnector.GetNumberRegex.Matches(responseXml);
                description = string.Format("Get Number: {0} - {1}", matches[0].Groups["maxlength"].Value, matches[0].Groups["response"].Value);
            }
            else if (IcrtouchConnector.GetTextRegex.IsMatch(responseXml))
            {
                // GET TEXT
                toReturn = ResponseMessageType.GetText;
                matches = IcrtouchConnector.GetTextRegex.Matches(responseXml);
                description = string.Format("Get Text: {0} - {1}", matches[0].Groups["maxlength"].Value, matches[0].Groups["response"].Value);
            }
            else if (IcrtouchConnector.ItemListRegex.IsMatch(responseXml))
            {
                // GET TEXT
                toReturn = ResponseMessageType.GetNumber;
                matches = IcrtouchConnector.ItemListRegex.Matches(responseXml);
                description = string.Format("Itemlist: {0}", matches[0].Groups["response"].Value);
            }

            return toReturn;
        }

        #endregion

        #region Wrapper methods

        /// <summary>
        /// Gets the raw data.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public T GetRawData<T>(string file)
        {
            T data = default(T);
            string command = "<GETPROGRAMDATA file=\"" + file + "\" />";
            string response = this.DoCommand(command, 500000, 3000);

            if (response.Contains("<FAILEDERROR error=\"ECMD\">"))
            {
                throw new NotImplementedException(string.Format("Command: '{0}' is not supported by your version of ICR Touch.", command));
            }

            if (response.Length > 0)
            {
                response = response.Substring(0, response.IndexOf("\r\n"));
                StringReader sr = new StringReader(response);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                data = (T)serializer.Deserialize(sr);
                sr.Close();
            }
            return data;
        }

        /// <summary>
        /// Gets the multi level pricing data.
        /// </summary>
        public void GetMultiLevelPricingData()
        {
            this.multiLevelPricingGroups = new List<MultilevelPricingGroup>();
            FunctionkeysXml functionkeys = null;

            // Get function keys
            try
            {
                functionkeys = GetRawData<FunctionkeysXml>("53");
            }
            catch 
            {
                // Ok, don't do MultiLevelPricing then
                this.WriteToLogNormal("GetMultiLevelPricingData", "The used version of ICR Touch doesn't support MultiLevelPricing.");
                return;
            }

            // Get buttons
            ButtonsXml buttons = GetRawData<ButtonsXml>("74");

            List<string> priceSwitchKeys = new List<string>();
            foreach (FunctionkeyXml functionkey in functionkeys.Data)
            {
                if (functionkey.Keytype == 70)
                {
                    priceSwitchKeys.Add(functionkey.Name);

                    MultilevelPricingGroup group = new MultilevelPricingGroup();
                    group.Level = functionkey.Value + 1; // level 2 = value 1 etc.
                    group.Text = functionkey.Name;
                    multiLevelPricingGroups.Add(group);
                }
            }

            List<string> pages = new List<string>();
            foreach (ButtonXml button in buttons.Data)
            {
                if (button.Text != null)
                {
                    for (int i = 0; i < multiLevelPricingGroups.Count; i++)
                    {
                        MultilevelPricingGroup group = multiLevelPricingGroups[i];
                        if (group.Text == button.Text)
                        {
                            //Found a price-shift button on this page

                            //Products start at record 2001 / page 10
                            //Every page has 200 records
                            int pageNumber = (int)System.Math.Floor(((double)button.Record - 2001.0) / 200.0);

                            //Get all products on this page
                            int startId = (pageNumber * 200) + 2001;
                            for (int id = startId; id < startId + 200; id++)
                            {
                                ButtonXml productButton = buttons.Data[id];
                                if (productButton.Text != null) //Very likely a product
                                {
                                    group.AddPLU(productButton.ItemRecord);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the multi level pricing group.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="plu">The plu.</param>
        /// <returns></returns>
        public MultilevelPricingGroup GetMultiLevelPricingGroup(int level, int plu)
        {
            MultilevelPricingGroup ret = null;
            if (multiLevelPricingGroups == null)
                GetMultiLevelPricingData();

            foreach (MultilevelPricingGroup group in multiLevelPricingGroups)
            {
                if (group.HasPLU(plu) && group.Level == level)
                {
                    ret = group;
                    break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="group">The group.</param>
        /// <param name="listplu">The listplu.</param>
        /// <returns></returns>
        public ProductsXml GetProducts(string number, string group, string listplu)
        {
            int parametersGiven = 0;
            string command = string.Empty;

            parametersGiven += number.IsNullOrWhiteSpace() ? 0 : 1;
            parametersGiven += group.IsNullOrWhiteSpace() ? 0 : 1;
            parametersGiven += listplu.IsNullOrWhiteSpace() ? 0 : 1;

            if (parametersGiven > 1)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, "Multiple parameters are supplied for GetProduct, this is not allowed.");
            else if (parametersGiven == 0)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosProducts, "No parameters are supplied for GetProduct, this is not allowed.");

            if (!number.IsNullOrWhiteSpace())
            {
                command = string.Format("<GETPRODUCT number=\"{0}\">", number);
            }
            else if (!group.IsNullOrWhiteSpace())
            {
                command = string.Format("<GETPRODUCT group=\"{0}\">", group);
            }
            else if (!listplu.IsNullOrWhiteSpace())
            {
                command = string.Format("<GETPRODUCT listplu=\"{0}\">", listplu);
            }

            string response = this.DoCommand(command, 500000, 3000);

            ProductsXml products = null;
            if (response.Length > 0)
            {
                response = response.Substring(0, response.IndexOf("\r\n"));
                StringReader sr = new StringReader(response);
                XmlSerializer serializer = new XmlSerializer(typeof(ProductsXml));
                products = (ProductsXml)serializer.Deserialize(sr);
                sr.Close();
            }

            return products;
        }


        /// <summary>
        /// Retrieves specified file from /usr/symbol/data
        /// </summary>
        public ProductgroupsXml GetProductgroups()
        {
            string response = this.DoCommand("<GETPRODUCTGROUPLIST />", 16384, this.longSleep);

            ProductgroupsXml productgroups = null;
            if (response.Length > 0)
            {
                response = response.Substring(0, response.IndexOf("\r\n"));
                StringReader sr = new StringReader(response);
                XmlSerializer serializer = new XmlSerializer(typeof(ProductgroupsXml));
                productgroups = (ProductgroupsXml)serializer.Deserialize(sr);
                sr.Close();
            }

            return productgroups;
        }

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data
        /// </summary>
        public ProductgroupXml GetProductgroup(string number, string name)
        {
            int parametersGiven = 0;
            string command = string.Empty;

            parametersGiven += number.IsNullOrWhiteSpace() ? 0 : 1;
            parametersGiven += name.IsNullOrWhiteSpace() ? 0 : 1;

            if (parametersGiven > 1)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, "Multiple parameters are supplied for GetProductgroup, this is not allowed.");
            else if (parametersGiven == 0)
                throw new POSException(OrderProcessingError.PosErrorCouldNotGetPosCategories, "No parameters are supplied for GetProductgroup, this is not allowed.");

            if (!number.IsNullOrWhiteSpace())
            {
                command = string.Format("<GETPRODUCTGROUP number=\"{0}\">", number);
            }
            else if (!name.IsNullOrWhiteSpace())
            {
                command = string.Format("<GETPRODUCTGROUP name=\"{0}\">", name);
            }

            string response = this.DoCommand(command, 16384, this.longSleep);

            ProductgroupXml productGroup = null;
            if (response.Length > 0)
            {
                response = response.Substring(0, response.IndexOf("\r\n"));
                StringReader sr = new StringReader(response);
                XmlSerializer serializer = new XmlSerializer(typeof(ProductgroupXml));

                this.WriteToLogExtended("GetProductgroup", response);
                productGroup = (ProductgroupXml)serializer.Deserialize(sr);

                sr.Close();
            }

            return productGroup;
        }

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data
        /// </summary>
        public TablesXml GetTables(string level)
        {
            string command = string.Empty;
            command = string.Format("<GETTABLE level=\"{0}\">", level);

            string response = this.DoCommand(command, 16384, this.longSleep);

            TablesXml tables = null;
            if (response.Length > 0)
            {
                response = response.Substring(0, response.IndexOf("\r\n"));
                StringReader sr = new StringReader(response);
                XmlSerializer serializer = new XmlSerializer(typeof(TablesXml));
                tables = (TablesXml)serializer.Deserialize(sr);
                sr.Close();
            }

            return tables;
        }

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data
        /// </summary>
        public ListplusXml GetListplus(string number)
        {
            string command = string.Empty;
            command = string.Format("<GETLISTPLU level=\"{0}\">", number);

            string response = this.DoCommand(command, 16384, this.longSleep);

            ListplusXml listplus = null;
            if (response.Length > 0)
            {
                response = response.Substring(0, response.IndexOf("\r\n"));
                StringReader sr = new StringReader(response);
                XmlSerializer serializer = new XmlSerializer(typeof(ListplusXml));
                listplus = (ListplusXml)serializer.Deserialize(sr);
                sr.Close();
            }

            return listplus;
        }

        /// <summary>
        /// Retrieves specified file from /usr/symbol/data
        /// </summary>
        public bool SendSale(SendSaleXml sendSale)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SendSaleXml));
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            serializer.Serialize(sw, sendSale);

            string response = this.DoCommand(sb.ToString(), 16384, this.longSleep);

            if (response.Equals("<RESPONSE>OK</RESPONSE>", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            else if (response.StartsWith("<RETRYERROR>"))
            {
                throw new DeliverypointLockedException(response);
            }
            else
            {
                this.errorMessage = string.Format("SendSale failed: '{0}'\r\n Xml: {1}", response, sb.ToString());
                return false;
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
        #endregion
    }
}
