﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.POS.Interfaces;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Logic.POS.Icrtouch
{
    internal class IcrtouchHelper : PosHelper
    {
        public override void PrepareOrderForRequestService(OrderEntity orderEntity, Order orderModel)
        {
            // Arrange Printer Rerouting if required

            // Check if we have a IcrtouchPrinterMapping available
            PredicateExpression filter = new PredicateExpression();
            filter.Add(IcrtouchprintermappingDeliverypointFields.DeliverypointId == orderEntity.DeliverypointId);

            var mapping = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<IcrtouchprintermappingDeliverypointEntity>(filter);

            if (mapping != null)
            {
                // We have a mapping
                string mappings = "";

                if (mapping.Printer1.HasValue)
                    mappings += string.Format("1,{0};", mapping.Printer1.Value);

                if (mapping.Printer2.HasValue)
                    mappings += string.Format("2,{0};", mapping.Printer2.Value);

                if (mapping.Printer3.HasValue)
                    mappings += string.Format("3,{0};", mapping.Printer3.Value);

                if (mapping.Printer4.HasValue)
                    mappings += string.Format("4,{0};", mapping.Printer4.Value);

                if (mapping.Printer5.HasValue)
                    mappings += string.Format("5{0};", mapping.Printer5.Value);

                if (mapping.Printer6.HasValue)
                    mappings += string.Format("6,{0};", mapping.Printer6.Value);

                if (mapping.Printer7.HasValue)
                    mappings += string.Format("7,{0};", mapping.Printer7.Value);

                if (mapping.Printer8.HasValue)
                    mappings += string.Format("8,{0};", mapping.Printer8.Value);

                if (mapping.Printer9.HasValue)
                    mappings += string.Format("9,{0};", mapping.Printer9.Value);

                if (mapping.Printer10.HasValue)
                    mappings += string.Format("10,{0};", mapping.Printer10.Value);

                orderModel.FieldValue1 = mappings;
            }
        }


        public override void AddPosSpecificPosIntegrationInformation(TerminalEntity terminalEntity, PosIntegrationInformation posIntegrationInformation)
        {
            // Add the Current IcrtouchPrinterMapping
            posIntegrationInformation.Icrtouchprintermapping = IcrtouchprintermappingHelper.CreateModelFromEntity(terminalEntity.IcrtouchprintermappingEntity);
        }

        /// <summary>
        /// Prepares the order for pos by the supplied posIntegrationInformation.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="posIntegrationInformation">The pos integration information.</param>
        public override void PrepareOrderForPos(Order order, PosIntegrationInformation posIntegrationInformation)
        {
            // Find the DeliverypointId
            var deliverypoint = posIntegrationInformation.Deliverypoints.FirstOrDefault(d => d.Number == order.DeliverypointNumber);

            // Arrange table mapping
            var mapping = posIntegrationInformation.Icrtouchprintermapping.IcrtouchprintermappingDeliverypoints.FirstOrDefault(id => id.DeliverypointId == deliverypoint.DeliverypointId);

            if (mapping != null)
            {
                // We have a mapping
                string mappings = "";

                if (mapping.Printer1 > 0)
                    mappings += string.Format("1,{0};", mapping.Printer1);

                if (mapping.Printer2 > 0)
                    mappings += string.Format("2,{0};", mapping.Printer2);

                if (mapping.Printer3 > 0)
                    mappings += string.Format("3,{0};", mapping.Printer3);

                if (mapping.Printer4 > 0)
                    mappings += string.Format("4,{0};", mapping.Printer4);

                if (mapping.Printer5 > 0)
                    mappings += string.Format("5{0};", mapping.Printer5);

                if (mapping.Printer6 > 0)
                    mappings += string.Format("6,{0};", mapping.Printer6);

                if (mapping.Printer7 > 0)
                    mappings += string.Format("7,{0};", mapping.Printer7);

                if (mapping.Printer8 > 0)
                    mappings += string.Format("8,{0};", mapping.Printer8);

                if (mapping.Printer9 > 0)
                    mappings += string.Format("9,{0};", mapping.Printer9);

                if (mapping.Printer10 > 0)
                    mappings += string.Format("10,{0};", mapping.Printer10);

                order.FieldValue1 = mappings;
            }
        }

        /// <summary>
        /// Prepares the orderitem created for a Alterationitem for pos by the supplied posIntegrationInformation.
        /// </summary>
        /// <param name="orderitem">The orderitem.</param>
        /// <param name="alteration">The alteration.</param>
        /// <param name="posIntegrationInformation">The pos integration information.</param>
        public override void PrepareOrderitemFromAlterationitemForPos(Orderitem orderitem, Alteration alteration, PosIntegrationInformation posIntegrationInformation)
        {
            // FieldValue1 Contains the GroupType
            orderitem.FieldValue1 = alteration.Posalteration.FieldValue1;
        }
    }
}
