﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// SendSaleXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SENDSALE")]
    public class SendSaleXml
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SendSaleXml"/> class.
        /// </summary>
        public SendSaleXml()
        {
            this.Header = new SendSaleHeaderXml();
            this.Content = new SendSaleContentXml();
            this.Final = new SendSaleFinalXml();
        }

        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        [XmlElementAttribute(ElementName = "HEADER")]
        public SendSaleHeaderXml Header { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        [XmlElementAttribute(ElementName = "CONTENT")]
        public SendSaleContentXml Content { get; set; }

        /// <summary>
        /// Gets or sets the final.
        /// </summary>
        /// <value>
        /// The final.
        /// </value>
        [XmlElementAttribute(ElementName = "FINAL")]
        public SendSaleFinalXml Final { get; set; }

        #region Classed used by SendSaleXml

        /// <summary>
        /// SendSaleHeaderXml class
        /// </summary>
        [Serializable, XmlRootAttribute(ElementName = "HEADER")]
        public class SendSaleHeaderXml
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="SendSaleHeaderXml"/> class.
            /// </summary>
            public SendSaleHeaderXml()
            {
                this.Cashier = new SendSaleCashierXml();
            }

            /// <summary>
            /// Gets or sets the consec.
            /// </summary>
            /// <value>
            /// The consec.
            /// </value>
            [XmlElementAttribute(ElementName = "CONSEC")]
            public string Consec { get; set; }

            /// <summary>
            /// Gets or sets the mcid.
            /// </summary>
            /// <value>
            /// The mcid.
            /// </value>
            [XmlElementAttribute(ElementName = "MCID")]
            public string Mcid { get; set; }

            /// <summary>
            /// Gets or sets the cashier.
            /// </summary>
            /// <value>
            /// The cashier.
            /// </value>
            [XmlElementAttribute(ElementName = "CASHIER")]
            public SendSaleCashierXml Cashier { get; set; }

            /// <summary>
            /// Gets or sets the kpreroute.
            /// </summary>
            /// <value>
            /// The kpreroute.
            /// </value>
            [XmlElementAttribute(ElementName = "KPREROUTE")]
            public SendSaleKprerouteXml Kpreroute { get; set; }
        }

        /// <summary>
        /// SendSaleFinalXml class
        /// </summary>
        [Serializable, XmlRootAttribute(ElementName = "FINAL")]
        public class SendSaleFinalXml
        {
            /// <summary>
            /// Gets or sets the quantity.
            /// </summary>
            /// <value>
            /// The quantity.
            /// </value>
            [XmlAttribute(AttributeName = "quantity")]
            public string Quantity { get; set; }

            /// <summary>
            /// Gets or sets the total value.
            /// </summary>
            /// <value>
            /// The total value.
            /// </value>
            [XmlAttribute(AttributeName = "total")]
            public string TotalValue { get; set; }

            /// <summary>
            /// Gets or sets the send to table.
            /// </summary>
            /// <value>
            /// The send to table.
            /// </value>
            [XmlElementAttribute(ElementName = "SENDTOTABLE")]
            public string SendToTable { get; set; }

            //[XmlElementAttribute(ElementName = "PRINTRECEIPT")]
            //public string PrintReceipt { get; set; }
        }

        /// <summary>
        /// SendSaleCashierXml class
        /// </summary>
        [Serializable, XmlRootAttribute(ElementName = "CASHIER")]
        public class SendSaleCashierXml
        {
            /// <summary>
            /// Gets or sets the number.
            /// </summary>
            /// <value>
            /// The number.
            /// </value>
            [XmlAttribute(AttributeName = "number")]
            public string Number { get; set; }

            /// <summary>
            /// Gets or sets the name.
            /// </summary>
            /// <value>
            /// The name.
            /// </value>
            [XmlTextAttribute()]
            public string Name { get; set; }
        }

        /// <summary>
        /// SendSaleKprerouteXml class
        /// </summary>
        [Serializable, XmlRootAttribute(ElementName = "KPREROUTE")]
        public class SendSaleKprerouteXml
        {
            /// <summary>
            /// Gets or sets the route1.
            /// </summary>
            /// <value>
            /// The route1.
            /// </value>
            [XmlElementAttribute(ElementName = "X1X")]
            public string Route1 { get; set; }

            /// <summary>
            /// Gets or sets the route2.
            /// </summary>
            /// <value>
            /// The route2.
            /// </value>
            [XmlElementAttribute(ElementName = "X2X")]
            public string Route2 { get; set; }

            /// <summary>
            /// Gets or sets the route3.
            /// </summary>
            /// <value>
            /// The route3.
            /// </value>
            [XmlElementAttribute(ElementName = "X3X")]
            public string Route3 { get; set; }

            /// <summary>
            /// Gets or sets the route4.
            /// </summary>
            /// <value>
            /// The route4.
            /// </value>
            [XmlElementAttribute(ElementName = "X4X")]
            public string Route4 { get; set; }

            /// <summary>
            /// Gets or sets the route5.
            /// </summary>
            /// <value>
            /// The route5.
            /// </value>
            [XmlElementAttribute(ElementName = "X5X")]
            public string Route5 { get; set; }

            /// <summary>
            /// Gets or sets the route6.
            /// </summary>
            /// <value>
            /// The route6.
            /// </value>
            [XmlElementAttribute(ElementName = "X6X")]
            public string Route6 { get; set; }

            /// <summary>
            /// Gets or sets the route7.
            /// </summary>
            /// <value>
            /// The route7.
            /// </value>
            [XmlElementAttribute(ElementName = "X7X")]
            public string Route7 { get; set; }

            /// <summary>
            /// Gets or sets the route8.
            /// </summary>
            /// <value>
            /// The route8.
            /// </value>
            [XmlElementAttribute(ElementName = "X8X")]
            public string Route8 { get; set; }

            /// <summary>
            /// Gets or sets the route9.
            /// </summary>
            /// <value>
            /// The route9.
            /// </value>
            [XmlElementAttribute(ElementName = "X9X")]
            public string Route9 { get; set; }

            /// <summary>
            /// Gets or sets the route10.
            /// </summary>
            /// <value>
            /// The route10.
            /// </value>
            [XmlElementAttribute(ElementName = "X10X")]
            public string Route10 { get; set; }
        }


        /// <summary>
        /// SendSaleContentXml class
        /// </summary>
        [Serializable, XmlRootAttribute(ElementName = "CONTENT")]
        public class SendSaleContentXml
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="SendSaleContentXml"/> class.
            /// </summary>
            public SendSaleContentXml()
            {
                this.Products = new SendSaleProductXml[0];
            }

            /// <summary>
            /// Gets or sets the products.
            /// </summary>
            /// <value>
            /// The products.
            /// </value>
            [XmlElementAttribute("PRODUCT", IsNullable = true)]
            public SendSaleProductXml[] Products { get; set; }

            /// <summary>
            /// Gets or sets the text message.
            /// </summary>
            /// <value>
            /// The text message.
            /// </value>
            [XmlElementAttribute(ElementName = "TEXTMESSAGE")]
            public string TextMessage { get; set; }
        }

        /// <summary>
        /// SendSaleProductXml class
        /// </summary>
        [Serializable, XmlRootAttribute(ElementName = "PRODUCT")]
        public class SendSaleProductXml
        {
            /// <summary>
            /// Gets or sets the number.
            /// </summary>
            /// <value>
            /// The number.
            /// </value>
            [XmlAttribute(AttributeName = "number")]
            public string Number { get; set; }

            /// <summary>
            /// Gets or sets the quantity.
            /// </summary>
            /// <value>
            /// The quantity.
            /// </value>
            [XmlAttribute(AttributeName = "quantity")]
            public string Quantity { get; set; }

            /// <summary>
            /// Gets or sets the value.
            /// </summary>
            /// <value>
            /// The value.
            /// </value>
            [XmlAttribute(AttributeName = "value")]
            public string Value { get; set; }

            /// <summary>
            /// Gets or sets the priceshift.
            /// </summary>
            /// <value>
            /// The priceshift.
            /// </value>
            [XmlAttribute(AttributeName = "priceshift")]
            public string Priceshift { get; set; }

            /// <summary>
            /// Gets or sets the text.
            /// </summary>
            /// <value>
            /// The text.
            /// </value>
            [XmlAttribute(AttributeName = "Text")]
            public string Text { get; set; }
        }

        #endregion

    }
}
