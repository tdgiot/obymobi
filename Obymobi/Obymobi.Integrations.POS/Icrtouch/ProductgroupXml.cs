﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.POS.Icrtouch
{
    /// <summary>
    /// ProductgroupXml class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PRODUCTGROUP")]
    public class ProductgroupXml
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductgroupXml"/> class.
        /// </summary>
        public ProductgroupXml()
        {
            this.Products = new ProductXml[0];
        }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        [XmlAttribute(AttributeName = "number")]
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        /// <value>
        /// The products.
        /// </value>
        [XmlElementAttribute("PRODUCT", IsNullable = true)]
        public ProductXml[] Products { get; set; }
    }

}
