﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Flexipos;
//using Obymobi.Logic.Model;
//using Dionysos;
//using System.IO.Ports;
//using System.Diagnostics;
//using Obymobi.Logic.Loggers;
//using Obymobi.Enums;
//using System.IO;
//using System.Globalization;

//namespace Obymobi.Logic.POS.Vectron
//{
//    public class VectronSynchronizer
//    {
//        #region Fields

//        private bool initialized = false;

//        private string categoriesFilePath = string.Empty;
//        private string productsFilePath = string.Empty;
        
//        private string errorMessage = string.Empty;

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Initializes a new instance of the <see cref="VectronSynchronizer"/> class.
//        /// </summary>
//        public VectronSynchronizer()
//        {
//        }

//        #endregion

//        #region Methods

//        #region IPOSConnector methods

//        /// <summary>
//        /// Gets the products from the point-of-service
//        /// </summary>
//        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posproduct instances</returns>
//        public Posproduct[] GetPosproducts()
//        {
//            List<Posproduct> posproducts = new List<Posproduct>();

//            if (this.productsFilePath.IsNullOrWhiteSpace())
//                throw new POSException("No file specified which contains the POS products!");
//            else if (!File.Exists(this.productsFilePath))
//                throw new POSException(string.Format("The POS products file '{0}' does not exist!", this.productsFilePath));
//            else
//            {
//                StringBuilder sb = new StringBuilder();

//                // Read file as lines
//                string[] lines = File.ReadAllLines(this.productsFilePath);

//                string lastProductId = string.Empty;
//                string errorForProductId = string.Empty;

//                Posproduct posproduct = null;

//                // Vectron culture for numbers
//                var culture = CultureInfo.CreateSpecificCulture("en-GB");

//                foreach (var line in lines)
//                {
//                    try
//                    {
//                        string[] lineElements = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
//                        if (lineElements.Length < 4)
//                        {
//                            this.WriteToLogNormal("GetPosproducts", "Cannot import line: {0} - Too little fields", line);
//                            continue;
//                        }

//                        string recordType, externalProductId, fieldName, value;
//                        recordType = lineElements[0];
//                        externalProductId = lineElements[1];
//                        fieldName = lineElements[2];
//                        value = lineElements[3];

//                        // Only use Product records
//                        if (recordType.Equals("101"))
//                        {
//                            if (errorForProductId.Length > 0 && externalProductId.Equals(errorForProductId))
//                                continue;

//                            // Check if we're in the same product, otherwise save previous and start a new one;
//                            if (!lastProductId.Equals(externalProductId))
//                            {
//                                // Save the current POS product
//                                // because we've reached the next POS product
//                                if (posproduct != null && posproduct.Name.Length > 2)
//                                {
//                                    posproducts.Add(posproduct);
//                                }

//                                // Get an existing or new PosproductEntity
//                                // using the external id
//                                posproduct = new Posproduct();

//                                // Set some default properties
//                                posproduct.PriceIn = 0;
//                                posproduct.VatTariff = 3;
//                                posproduct.ExternalId = externalProductId;
//                                posproduct.ExternalPoscategoryId = "unknown";

//                                // Remember the current id
//                                lastProductId = externalProductId;
//                            }

//                            if (fieldName.Equals("(12)"))        // Productname
//                            {
//                                if (value.StartsWith("'"))
//                                    value = value.Substring(1);
//                                if (value.EndsWith("'"))
//                                    value = value.Substring(0, value.Length - 1);

//                                value = value.Trim();

//                                posproduct.Name = value;
//                            }
//                            else if (fieldName.Equals("(13)"))  // Price
//                            {
//                                decimal priceDecimal;
//                                if (decimal.TryParse(value, NumberStyles.Number, culture, out priceDecimal))
//                                {
//                                    int priceInt = (int)(priceDecimal * 100);
//                                    posproduct.PriceIn = priceInt;
//                                }
//                                else
//                                    posproduct.PriceIn = 0;
//                            }
//                            else if (fieldName.Equals("(14)"))  // Department id
//                            {
//                                int externalDepartmentId = -1;
//                                if (int.TryParse(value, out externalDepartmentId))
//                                    posproduct.ExternalPoscategoryId = externalDepartmentId.ToString();
//                            }
//                            else if (fieldName.Equals("(16)"))  // Vat tariff
//                            {
//                                if (value.Equals("2"))
//                                    posproduct.VatTariff = 1;
//                                else if (value.Equals("1"))
//                                    posproduct.VatTariff = 2;
//                            }
//                        }
//                    }
//                    catch (Exception ex)
//                    {
//                        this.WriteToLogNormal("GetPosproducts", "Error for line: {0} - {1}<br>", line, ex.Message);
//                        posproduct = null;
//                        errorForProductId = lastProductId;
//                    }
//                }

//                if (posproduct != null && posproduct.Name != null && posproduct.Name.Length > 2)
//                {
//                    // Add the posproduct to the list
//                    posproducts.Add(posproduct);

//                    // Reset the posproduct
//                    posproduct = null;
//                }
//            }

//            return posproducts.ToArray();
//        }

//        /// <summary>
//        /// Gets the categories from the point-of-service
//        /// </summary>
//        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Poscategory instances</returns>
//        public Poscategory[] GetPoscategories()
//        {
//            List<Poscategory> poscategories = new List<Poscategory>();

//            if (this.categoriesFilePath.IsNullOrWhiteSpace())
//                throw new POSException("No file specified which contains the POS categories!");
//            else if (!File.Exists(this.categoriesFilePath))
//                throw new POSException(string.Format("The POS categories file '{0}' does not exist!", this.categoriesFilePath));
//            else
//            {
//                StringBuilder sb = new StringBuilder();

//                // Read file as lines
//                string[] lines = File.ReadAllLines(this.categoriesFilePath);
//                File.Delete(this.categoriesFilePath);

//                string errorForScreenId = string.Empty;

//                Poscategory poscategory = null;

//                foreach (var line in lines)
//                {
//                    try
//                    {
//                        string[] lineElements = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
//                        if (lineElements.Length < 4)
//                        {
//                            this.WriteToLogNormal("GetPoscategories", "Cannot import line: {0} - Too little fields", line);
//                            continue;
//                        }

//                        string recordType, externalScreenId, fieldName, value;
//                        recordType = lineElements[0];
//                        externalScreenId = lineElements[1];
//                        fieldName = lineElements[2];
//                        value = lineElements[3];

//                        if (recordType.Equals("102"))
//                        {
//                            if (errorForScreenId.Length > 0 && externalScreenId.Equals(errorForScreenId))
//                                continue;

//                            poscategory = new Poscategory();
//                            poscategory.ExternalId = externalScreenId;
//                            poscategory.Name = value;
//                            if (poscategory.Name.StartsWith("'"))
//                                poscategory.Name = poscategory.Name.Substring(1);
//                            if (poscategory.Name.EndsWith("'"))
//                                poscategory.Name = poscategory.Name.Substring(0, poscategory.Name.Length - 1);

//                            poscategories.Add(poscategory);
//                        }

//                    }
//                    catch (Exception ex)
//                    {
//                        this.WriteToLogNormal("GetPoscategories", "Error voor lijn: {0} - {1}<br>", line, ex.Message);
//                        poscategory = null;
//                    }
//                }
//            }

//            return poscategories.ToArray();
//        }

//        /// <summary>
//        /// Gets the deliverypoint groups from the point-of-service
//        /// </summary>
//        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances</returns>
//        public Posdeliverypoint[] GetPosdeliverypoints()
//        {
//            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

//            // TODO: implement
        
//            return posdeliverypoints.ToArray();
//        }

//        /// <summary>
//        /// Gets the deliverypoints from the point-of-service
//        /// </summary>
//        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances</returns>
//        public Posdeliverypointgroup[] GetPosdeliverypointgroups()
//        {
//            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();

//            // TODO: implement

//            return posdeliverypointgroups.ToArray();
//        }

//        /// <summary>
//        /// Get the order from the point-of-serivce
//        /// </summary>
//        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
//        /// <returns>An Obymobi.Logic.Model.Posorder instance</returns>
//        public Posorder GetPosorder(string deliverypointNumber)
//        {
//            Posorder posorder = null;

//            // TODO: implement

//            return posorder;
//        }

//        #endregion

//        #region Properties

//        /// <summary>
//        /// Gets or sets the categories file path.
//        /// </summary>
//        /// <value>The categories file path.</value>
//        public string CategoriesFilePath
//        {
//            get
//            {
//                return this.categoriesFilePath;
//            }
//            set
//            {
//                this.categoriesFilePath = value;
//            }
//        }

//        /// <summary>
//        /// Gets or sets the products file path.
//        /// </summary>
//        /// <value>The products file path.</value>
//        public string ProductsFilePath
//        {
//            get
//            {
//                return this.productsFilePath;
//            }
//            set
//            {
//                this.productsFilePath = value;
//            }
//        }

//        #region IPOSConnector properties

//        /// <summary>
//        /// Gets or sets the error message
//        /// </summary>
//        public string ErrorMessage
//        {
//            get
//            {
//                return this.errorMessage;
//            }
//            set
//            {
//                this.errorMessage = value;
//            }
//        }

//        #endregion
//    }
//}
