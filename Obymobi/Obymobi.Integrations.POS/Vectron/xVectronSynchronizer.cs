﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Dionysos;
//using System.IO;
//using Obymobi.Data.EntityClasses;
//using System.Globalization;

//namespace Obymobi.Logic.POS.Vectron
//{
//    public class VectronSynchronizer : POSSynchronizer
//    {
//        #region Fields

//        private string categoriesFilePath = string.Empty;
//        private string productsFilePath = string.Empty;

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Initializes a new instance of the <see cref="VectronSynchronizer"/> class.
//        /// </summary>
//        /// <param name="companyId">The company id.</param>
//        public VectronSynchronizer(int companyId) : base(companyId)
//        {
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        /// Synchronizes the POS categories.
//        /// </summary>
//        /// <returns>
//        /// True if synchronizing the POS categories was successful, False if not
//        /// </returns>
//        public override bool SynchronizePoscategories()
//        {
//            bool success = true;

//            if (this.categoriesFilePath.IsNullOrWhiteSpace())
//                throw new POSException("No file specified which contains the POS categories!");
//            else if (!File.Exists(this.categoriesFilePath))
//                throw new POSException(string.Format("The POS categories file '{0}' does not exist!", this.categoriesFilePath));
//            else
//            {
//                StringBuilder sb = new StringBuilder();

//                // Read file as lines
//                string[] lines = File.ReadAllLines(this.categoriesFilePath);
//                File.Delete(this.categoriesFilePath);

//                string errorForScreenId = string.Empty;

//                PoscategoryEntity poscategory = null;

//                foreach (var line in lines)
//                {
//                    try
//                    {
//                        string[] lineElements = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
//                        if (lineElements.Length < 4)
//                        {
//                            sb.AppendFormat("Kan lijn niet importeren: {0} - Te weinig velden", line);
//                            continue;
//                        }

//                        string recordType, externalScreenId, fieldName, value;
//                        recordType = lineElements[0];
//                        externalScreenId = lineElements[1];
//                        fieldName = lineElements[2];
//                        value = lineElements[3];

//                        if (recordType.Equals("102"))
//                        {
//                            if (errorForScreenId.Length > 0 && externalScreenId.Equals(errorForScreenId))
//                                continue;

//                            poscategory = this.GetPoscategoryByExternalId(externalScreenId);

//                            bool wasNew = poscategory.IsNew;

//                            poscategory.CompanyId = Convert.ToInt32(this.CompanyId);
//                            poscategory.ExternalId = externalScreenId;
//                            poscategory.Name = value;
//                            if (poscategory.Name.StartsWith("'"))
//                                poscategory.Name = poscategory.Name.Substring(1);
//                            if (poscategory.Name.EndsWith("'"))
//                                poscategory.Name = poscategory.Name.Substring(0, poscategory.Name.Length - 1);

//                            if (poscategory.Save(true))
//                            {
//                                poscategory.Refetch();

//                                if (wasNew)
//                                    this.PoscategoryView.RelatedCollection.Add(poscategory);
//                            }
//                        }

//                    }
//                    catch (Exception ex)
//                    {
//                        sb.AppendFormat("Error voor lijn: {0} - {1}<br>", line, ex.Message);
//                        poscategory = null;
//                    }
//                }
//            }

//            return success;
//        }

//        /// <summary>
//        /// Synchronizes the  POS products.
//        /// </summary>
//        /// <returns>
//        /// True if synchronizing the POS products was successful, False if not
//        /// </returns>
//        public override bool SynchronizePosproducts()
//        {
//            bool success = true;

//            if (this.productsFilePath.IsNullOrWhiteSpace())
//                throw new POSException("No file specified which contains the POS products!");
//            else if (!File.Exists(this.productsFilePath))
//                throw new POSException(string.Format("The POS products file '{0}' does not exist!", this.productsFilePath));
//            else
//            {
//                StringBuilder sb = new StringBuilder();

//                // Read file as lines
//                string[] lines = File.ReadAllLines(this.productsFilePath);

//                string lastProductId = string.Empty;
//                string errorForProductId = string.Empty;
//                bool wasNew = false;

//                PosproductEntity posproduct = null;

//                // Vectron culture for numbers
//                var culture = CultureInfo.CreateSpecificCulture("en-GB");

//                foreach (var line in lines)
//                {
//                    try
//                    {
//                        string[] lineElements = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
//                        if (lineElements.Length < 4)
//                        {
//                            sb.AppendFormat("Kan lijn niet importeren: {0} - Te weinig velden", line);
//                            continue;
//                        }

//                        string recordType, externalProductId, fieldName, value;
//                        recordType = lineElements[0];
//                        externalProductId = lineElements[1];
//                        fieldName = lineElements[2];
//                        value = lineElements[3];

//                        // Only use Product records
//                        if (recordType.Equals("101"))
//                        {
//                            if (errorForProductId.Length > 0 && externalProductId.Equals(errorForProductId))
//                                continue;

//                            // Check if we're in the same product, otherwise save previous and start a new one;
//                            if (!lastProductId.Equals(externalProductId))
//                            {
//                                if (posproduct != null)
//                                    wasNew = posproduct.IsNew;

//                                // Save the current POS product
//                                // because we've reached the next POS product
//                                if (posproduct != null && posproduct.Name.Length > 2)
//                                {
//                                    if (posproduct.Save(true) && wasNew)
//                                    {
//                                        posproduct.Refetch();
//                                        this.PosproductView.RelatedCollection.Add(posproduct);
//                                    }
//                                }

//                                // Get an existing or new PosproductEntity
//                                // using the external id
//                                posproduct = this.GetPosproductByExternalId(externalProductId);

//                                // Set some default properties
//                                posproduct.PriceIn = 0;
//                                posproduct.VatTariff = 3;
//                                posproduct.ExternalId = externalProductId;
//                                posproduct.CompanyId = this.CompanyId;
//                                posproduct.ExternalPoscategoryId = "unknown";

//                                // Remember the current id
//                                lastProductId = externalProductId;
//                            }

//                            if (fieldName.Equals("(12)"))        // Productname
//                            {
//                                if (value.StartsWith("'"))
//                                    value = value.Substring(1);
//                                if (value.EndsWith("'"))
//                                    value = value.Substring(0, value.Length - 1);

//                                value = value.Trim();

//                                posproduct.Name = value;
//                            }
//                            else if (fieldName.Equals("(13)"))  // Price
//                            {
//                                decimal price;
//                                if (decimal.TryParse(value, NumberStyles.Number, culture, out price))
//                                    posproduct.PriceIn = price;
//                                else
//                                    posproduct.PriceIn = 0;
//                            }
//                            else if (fieldName.Equals("(14)"))  // Department id
//                            {
//                                int externalDepartmentId = -1;
//                                if (int.TryParse(value, out externalDepartmentId))
//                                    posproduct.ExternalPoscategoryId = externalDepartmentId.ToString();
//                            }
//                            else if (fieldName.Equals("(16)"))  // Vat tariff
//                            {
//                                if (value.Equals("2"))
//                                    posproduct.VatTariff = 1;
//                                else if (value.Equals("1"))
//                                    posproduct.VatTariff = 2;
//                            }
//                        }
//                    }
//                    catch (Exception ex)
//                    {
//                        sb.AppendFormat("Error voor lijn: {0} - {1}<br>", line, ex.Message);
//                        posproduct = null;
//                        errorForProductId = lastProductId;
//                    }
//                }

//                if (posproduct != null && posproduct.Name.Length > 2)
//                {
//                    // Save the POS product
//                    if (posproduct.Save(true) && wasNew)
//                    {
//                        posproduct.Refetch();
//                        this.PosproductView.RelatedCollection.Add(posproduct);
//                    }

//                    // Reset the posproduct
//                    posproduct = null;
//                }

//                // Delete the product export file
//                File.Delete(this.productsFilePath);
//            }

//            return success;
//        }

//        #endregion

//        #region Properties

//        /// <summary>
//        /// Gets or sets the categories file path.
//        /// </summary>
//        /// <value>The categories file path.</value>
//        public string CategoriesFilePath
//        {
//            get
//            {
//                return this.categoriesFilePath;
//            }
//            set
//            {
//                this.categoriesFilePath = value;
//            }
//        }

//        /// <summary>
//        /// Gets or sets the products file path.
//        /// </summary>
//        /// <value>The products file path.</value>
//        public string ProductsFilePath
//        {
//            get
//            {
//                return this.productsFilePath;
//            }
//            set
//            {
//                this.productsFilePath = value;
//            }
//        }

//        #endregion
//    }
//}
