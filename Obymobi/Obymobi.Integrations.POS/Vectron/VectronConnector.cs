﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flexipos;
using Obymobi.Logic.Model;
using Dionysos;
using System.IO.Ports;
using System.Diagnostics;
using Obymobi.Logic.Loggers;
using Obymobi.Enums;
using System.IO;
using System.Globalization;

namespace Obymobi.Logic.POS.Vectron
{
    /// <summary>
    /// VectronConnector class
    /// </summary>
    public class VectronConnector : IPOSConnector
    {
        #region Fields

        private OrderClass vectron = null;
        private GlobalVarObjectClass globalVars = null;

        private short comPort = 0;
        private string baudrate = string.Empty;
        private string wordLength = string.Empty;
        private string parity = string.Empty;
        private string stopBits = string.Empty;
        private short lengthClerkNr = 3;
        private short lengthGuestCheckNr = 3;
        private short lengthPluNr = 3;
        private short timeOut = 1000;
        private string pathLogFile = @"C:\Vectron\FlexiPos\";
        private string nameLogfile = "FlexiPos.log";
        private short daysLogfile = 14;
        private short clerkNo = 888;
        private bool displayOutput = false;
        private short otouchoGuestCheckNr = 9999;
        private short deliverypointLockedPLU = -1;

        private bool initialized = false;

        private string categoriesFilePath = "VectronCategories.exp";
        private string productsFilePath = "VectronProducts.exp";

        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.POS.Vectron.VectronConnector type
        /// </summary>
        public VectronConnector()
        {
            this.Initialize();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the FlexiPos.OrderClass
        /// </summary>
        private void Initialize()
        {
            try
            {
                // Get the settings from the configuration file                                                                  // DEFAULT
                this.comPort = ConfigurationManager.GetShort(POSConfigurationConstants.VectronComPort);                          // 5
                this.baudrate = ConfigurationManager.GetString(POSConfigurationConstants.VectronBaudrate);                       // 9600
                this.wordLength = ConfigurationManager.GetString(POSConfigurationConstants.VectronWordLength);                   // 8
                this.parity = ConfigurationManager.GetString(POSConfigurationConstants.VectronParity);                           // Even
                this.stopBits = ConfigurationManager.GetString(POSConfigurationConstants.VectronStopbits);                       // 1
                this.lengthClerkNr = ConfigurationManager.GetShort(POSConfigurationConstants.VectronLengthClerkNr);              // 3
                this.lengthGuestCheckNr = ConfigurationManager.GetShort(POSConfigurationConstants.VectronLengthGuestCheckNr);    // 3
                this.lengthPluNr = ConfigurationManager.GetShort(POSConfigurationConstants.VectronLengthPluNr);                  // 3
                this.timeOut = ConfigurationManager.GetShort(POSConfigurationConstants.VectronTimeOut);                          // 1000
                this.pathLogFile = ConfigurationManager.GetString(POSConfigurationConstants.VectronPathLogFile);                 // C:\Vectron\FlexiPosOrder\
                this.nameLogfile = ConfigurationManager.GetString(POSConfigurationConstants.VectronNameLogfile);                 // FlexiPos.log
                this.daysLogfile = ConfigurationManager.GetShort(POSConfigurationConstants.VectronDaysLogfile);                  // 14
                this.clerkNo = ConfigurationManager.GetShort(POSConfigurationConstants.VectronClerkNumber);                      // 888
                this.displayOutput = ConfigurationManager.GetBool(POSConfigurationConstants.VectronDisplayOutput);               // false

                this.otouchoGuestCheckNr = ConfigurationManager.GetShort(POSConfigurationConstants.VectronOtouchoGuestCheckNr);         // 9999
                this.deliverypointLockedPLU = ConfigurationManager.GetShort(POSConfigurationConstants.VectronDeliverypointLockedPLU);   // -1

                // Kill FlexiPos if it is already running
                this.KillFlexiPos();

                // Initialize the GlobalVarObjectClass instance which contains the settings
                this.globalVars = new GlobalVarObjectClass();
                this.globalVars.ComPort = this.comPort;
                this.globalVars.Baudrate = this.baudrate;
                this.globalVars.WordLength = this.wordLength;
                this.globalVars.Parity = this.parity;
                this.globalVars.Stopbits = this.stopBits;
                this.globalVars.LengthClerkNr = this.lengthClerkNr;
                this.globalVars.LengthGuestCheckNr = this.lengthGuestCheckNr;
                this.globalVars.LengthPluNr = this.lengthPluNr;
                this.globalVars.TimeOut = this.timeOut;
                this.globalVars.PathLogFile = this.pathLogFile;
                this.globalVars.NameLogfile = this.nameLogfile;
                this.globalVars.DaysLogfile = this.daysLogfile;
                this.globalVars.SaveRegSettings();
            }
            catch (Exception ex)
            {
                string errorMessage = string.Format("An exception was thrown while initializing the VectronConnector. Exception: {0}", ex.Message);
                this.WriteToLogNormal("Initialize", "Vectron Connector failed: {0}", errorMessage);
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, errorMessage);
            }
        }

        /// <summary>
        /// Kills FlexiPos
        /// </summary>
        /// <returns>True if killing FlexiPos was successful, False if not</returns>
        private bool KillFlexiPos()
        {
            bool success = true;

            this.WriteToLogExtended("KillFlexiPos", "Start");

            try
            {
                this.WriteToLogExtended("KillFlexiPos", "Trying to kill the FlexiPos process");

                Process[] processes = Process.GetProcessesByName("Flexipos");
                if (processes.Length > 0)
                    processes[0].Kill();

                this.WriteToLogExtended("KillFlexiPos", "FlexiPos process killed");
            }
            catch (Exception ex)
            {
                success = false;
                string errorMessage = string.Format("An exception was thrown while trying to kill the Flexipos process. Exception: {0}", ex.Message);
                this.WriteToLogNormal("KillFlexiPos", "Vectron Connector failed: {0}", errorMessage);
                throw new POSException(OrderProcessingError.VectronFlexiPosCouldNotBeKilled, errorMessage);
            }

            // Sleep for one second in order to let the FlexiPos process be killed
            System.Threading.Thread.Sleep(1000);

            return success;
        }

        /// <summary>
        /// Initializes the FlexiPos COM-object
        /// </summary>
        /// <returns>True if initializing FlexiPos was successful, False if not</returns>
        public bool InitializeFlexiPos()
        {
            bool success = true;

            this.WriteToLogExtended("InitializeFlexiPos", "Start");

            success = this.KillFlexiPos();

            if (success)
            {
                try
                {
                    this.WriteToLogExtended("InitializeFlexiPos", "Trying to initialize the FlexiPos process");

                    this.vectron = new OrderClass();
                    this.initialized = true;

                    this.WriteToLogExtended("InitializeFlexiPos", "FlexiPos process initialized");
                }
                catch (Exception ex)
                {
                    success = false;
                    throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, string.Format("An exception was thrown while initializing the VectronConnector. Exception: {0}", ex.Message));
                }
            }

            this.WriteToLogExtended("InitializeFlexiPos", "Start");

            return success;
        }

        #region IPOSConnector methods

        public void PrePosSynchronisation()
        {            
            string dir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            if (dir != null && Directory.Exists(dir))
            {
                this.CategoriesFilePath = Path.Combine(dir, "VectronCategories.exp");
                this.ProductsFilePath = Path.Combine(dir, "VectronProducts.exp");
            }            
        }

        public void PostPosSynchronisation()
        {
            // Noshing to do here.
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posproduct instances</returns>
        public Posproduct[] GetPosproducts()
        {
            List<Posproduct> posproducts = new List<Posproduct>();

            if (this.productsFilePath.IsNullOrWhiteSpace())
                throw new POSException(OrderProcessingError.VectronPosProductsFileNotFound, "No file specified which contains the POS products!");
            else if (!File.Exists(this.productsFilePath))
                throw new POSException(OrderProcessingError.VectronPosProductsFileNotFound, "The POS products file '{0}' does not exist!", this.productsFilePath);
            else
            {
                StringBuilder sb = new StringBuilder();

                // Read file as lines
                string[] lines = File.ReadAllLines(this.productsFilePath);

                string lastProductId = string.Empty;
                string errorForProductId = string.Empty;

                Posproduct posproduct = null;

                // Vectron culture for numbers
                var culture = CultureInfo.CreateSpecificCulture("en-GB");

                foreach (var line in lines)
                {
                    try
                    {
                        string[] lineElements = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        if (lineElements.Length < 4)
                        {
                            this.WriteToLogNormal("GetPosproducts", "Cannot import line: {0} - Too little fields", line);
                            continue;
                        }

                        string recordType, externalProductId, fieldName, value;
                        recordType = lineElements[0];
                        externalProductId = lineElements[1];
                        fieldName = lineElements[2];
                        value = lineElements[3];

                        // Only use Product records
                        if (recordType.Equals("101"))
                        {
                            if (errorForProductId.Length > 0 && externalProductId.Equals(errorForProductId))
                                continue;

                            // Check if we're in the same product, otherwise save previous and start a new one;
                            if (!lastProductId.Equals(externalProductId))
                            {
                                // Save the current POS product
                                // because we've reached the next POS product
                                if (posproduct != null && posproduct.Name != null && posproduct.Name.Length > 2)
                                    posproducts.Add(posproduct);

                                // Get an existing or new PosproductEntity
                                // using the external id
                                posproduct = new Posproduct();

                                // Set some default properties
                                posproduct.PriceIn = 0;
                                posproduct.VatTariff = 3;
                                posproduct.ExternalId = externalProductId;
                                posproduct.ExternalPoscategoryId = "unknown";

                                // Remember the current id
                                lastProductId = externalProductId;
                            }

                            if (fieldName.Equals("(12)"))        // Productname
                            {
                                if (value.StartsWith("'"))
                                    value = value.Substring(1);
                                if (value.EndsWith("'"))
                                    value = value.Substring(0, value.Length - 1);

                                value = value.Trim();

                                posproduct.Name = value;
                            }
                            else if (fieldName.Equals("(13)"))  // Price
                            {
                                decimal priceDecimal;
                                if (decimal.TryParse(value, NumberStyles.Number, culture, out priceDecimal))
                                    posproduct.PriceIn = priceDecimal;
                                else
                                    posproduct.PriceIn = 0;
                            }
                            else if (fieldName.Equals("(14)"))  // Department id
                            {
                                int externalDepartmentId = -1;
                                if (int.TryParse(value, out externalDepartmentId))
                                    posproduct.ExternalPoscategoryId = externalDepartmentId.ToString();
                            }
                            else if (fieldName.Equals("(16)"))  // Vat tariff
                            {
                                if (value.Equals("2"))
                                    posproduct.VatTariff = 1;
                                else if (value.Equals("1"))
                                    posproduct.VatTariff = 2;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        this.WriteToLogNormal("GetPosproducts", "Error for line: {0} - {1}<br>", line, ex.Message);
                        posproduct = null;
                        errorForProductId = lastProductId;
                    }
                }

                if (posproduct != null && posproduct.Name != null && posproduct.Name.Length > 2)
                {
                    // Add the posproduct to the list
                    posproducts.Add(posproduct);

                    // Reset the posproduct
                    posproduct = null;
                }
            }

            return posproducts.ToArray();
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Poscategory instances</returns>
        public Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();

            if (this.categoriesFilePath.IsNullOrWhiteSpace())
                throw new POSException(OrderProcessingError.VectronPosCategoriesFileNotFound, "No file specified which contains the POS categories!");
            else if (!File.Exists(this.categoriesFilePath))
                throw new POSException(OrderProcessingError.VectronPosCategoriesFileNotFound, "The POS categories file '{0}' does not exist!", this.categoriesFilePath);
            else
            {
                StringBuilder sb = new StringBuilder();

                // Read file as lines
                string[] lines = File.ReadAllLines(this.categoriesFilePath);
                File.Delete(this.categoriesFilePath);

                string errorForScreenId = string.Empty;

                Poscategory poscategory = null;

                foreach (var line in lines)
                {
                    try
                    {
                        string[] lineElements = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        if (lineElements.Length < 4)
                        {
                            this.WriteToLogNormal("GetPoscategories", "Cannot import line: {0} - Too little fields", line);
                            continue;
                        }

                        string recordType, externalScreenId, fieldName, value;
                        recordType = lineElements[0];
                        externalScreenId = lineElements[1];
                        fieldName = lineElements[2];
                        value = lineElements[3];

                        if (recordType.Equals("102"))
                        {
                            if (errorForScreenId.Length > 0 && externalScreenId.Equals(errorForScreenId))
                                continue;

                            poscategory = new Poscategory();
                            poscategory.ExternalId = externalScreenId;
                            poscategory.Name = value;
                            if (poscategory.Name.StartsWith("'"))
                                poscategory.Name = poscategory.Name.Substring(1);
                            if (poscategory.Name.EndsWith("'"))
                                poscategory.Name = poscategory.Name.Substring(0, poscategory.Name.Length - 1);

                            poscategories.Add(poscategory);
                        }

                    }
                    catch (Exception ex)
                    {
                        this.WriteToLogNormal("GetPoscategories", "Error voor lijn: {0} - {1}<br>", line, ex.Message);
                        poscategory = null;
                    }
                }
            }

            return poscategories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances</returns>
        public Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            // TODO: implement

            return posdeliverypoints.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances</returns>
        public Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();

            // TODO: implement

            return posdeliverypointgroups.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>An Obymobi.Logic.Model.Posorder instance</returns>
        public Posorder GetPosorder(string deliverypointNumber)
        {
            Posorder posorder = null;

            // TODO: implement

            return posorder;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>True if the save was succesful, False if not</returns>
        public OrderProcessingError SaveOrder(Posorder posorder)
        {
            OrderProcessingError processingError = OrderProcessingError.None;

            this.WriteToLogExtended("SaveOrder", "Start {0}", TimeStamp.CreateTimeStamp());

            // Initialize FlexiPos
            this.InitializeFlexiPos();

            // Check whether the FlexiPos.OrderClass was initialized
            if (!this.initialized)
            {
                processingError = OrderProcessingError.PosErrorCouldNotInitializeConnector;
                this.errorMessage = "VectronConnector is not initialized.";
            }
            else
            {
                short posDeliverypointExternalId = 0;
                short pluNo = 0;
                short quantity = 0;
                bool clerkOpened = false;
                bool guestCheckOpened = false;

                // Try to display the output
                try
                {
                    // Check whether the output should be displayed
                    if (this.displayOutput)
                        this.vectron.Show();
                }
                catch (Exception ex)
                {
                    processingError = OrderProcessingError.PosErrorUnspecifiedNonFatalPosProblem;
                    this.errorMessage = string.Format("An exception was thrown while trying to display the output of the Flexipos ordering. Exception: {0}", ex.Message);
                }

                // Try to open the clerk
                try
                {
                    // Login the employee
                    if (this.vectron.ClerkOpen(ref this.clerkNo))
                    {
                        clerkOpened = true;
                        this.WriteToLogExtended("SaveOrder", "Clerk '{0}' opened", this.clerkNo);
                    }
                    else
                    {
                        processingError = OrderProcessingError.VectronClerkCouldNotBeOpened;
                        this.errorMessage = string.Format("The clerk with number '{0}' could not be opened!", this.clerkNo);
                    }
                }
                catch (Exception ex)
                {
                    processingError = OrderProcessingError.VectronClerkCouldNotBeOpened;
                    this.errorMessage = string.Format("An exception was thrown while trying to open clerk '{0}'. Exception: {1}", this.clerkNo, ex.Message);
                }

                // Try to open the guest check
                if (processingError == OrderProcessingError.None)
                {
                    try
                    {
                        // Open a guest check for the specified table no
                        if (short.TryParse(posorder.PosdeliverypointExternalId, out posDeliverypointExternalId))
                        {
                            if (this.vectron.GuestCOpen(ref posDeliverypointExternalId))
                            {
                                guestCheckOpened = true;
                                this.WriteToLogExtended("SaveOrder", "Guest check '{0}' opened", posDeliverypointExternalId);
                            }
                            else
                            {
                                processingError = OrderProcessingError.PosErrorDeliverypointLocked;
                                this.errorMessage = string.Format("The guest check for table '{0}' could not be opened!", posDeliverypointExternalId);
                            }
                        }
                        else
                        {
                            processingError = OrderProcessingError.PosErrorNonParseableOrConvertableDataType;
                            this.errorMessage = string.Format("The POS external deliverypoint number could not be parsed into a short! posorder.PosdeliverypointExternalId = '{0}'", posorder.PosdeliverypointExternalId);
                        }
                    }
                    catch (Exception ex)
                    {
                        processingError = OrderProcessingError.VectronGuestCheckCouldNotBeOpened;
                        this.errorMessage = string.Format("An exception was thrown while trying to open guest check '{0}'. Exception: {1}", posDeliverypointExternalId, ex.Message);
                    }
                }

                // Try to add the products to the guest check
                if (processingError == OrderProcessingError.None)
                {
                    // Walk through the POS orderitems
                    // and add them to the POS
                    for (int i = 0; i < posorder.Posorderitems.Length; i++)
                    {
                        try
                        {
                            Posorderitem posorderitem = posorder.Posorderitems[i];
                            quantity = (short)posorderitem.Quantity;
                            if (!short.TryParse(posorderitem.PosproductExternalId, out pluNo))
                            {
                                processingError = OrderProcessingError.PosErrorNonParseableOrConvertableDataType;
                                this.errorMessage = string.Format("The POS external product number could not be parsed into a short! posorderitem.PosproductExternalId = '{0}'", posorderitem.PosproductExternalId);
                                break;
                            }

                            // Add the product to the check
                            if (!this.vectron.PLU(ref quantity, ref pluNo))
                            {
                                processingError = OrderProcessingError.VectronProductCouldNotBeAddedToCheck;
                                this.errorMessage = string.Format("The product with PLU '{0}' and quantity '{1}' could not be added to the check!", pluNo, quantity);
                                break;
                            }
                            else
                                this.WriteToLogExtended("SaveOrder", "PLU '{0}' and quantity '{1}' added to the check", pluNo, quantity);
                        }
                        catch (Exception ex)
                        {
                            processingError = OrderProcessingError.VectronProductCouldNotBeAddedToCheck;
                            this.errorMessage = string.Format("An exception was thrown while trying to add PLU '{0}' with quantity '{1}'. Exception: {2}", pluNo, quantity, ex.Message);
                        }
                    }
                }

                // Try to close the guest check
                if (guestCheckOpened)
                {
                    try
                    {
                        // Close the guest check for the specified table no
                        if (!this.vectron.GuestCClose(ref posDeliverypointExternalId))
                        {
                            if (processingError == OrderProcessingError.None)
                            {
                                processingError = OrderProcessingError.VectronCouldNotCloseGuestCheck;
                                this.errorMessage = string.Format("The guest check for table '{0}' could not be closed!", posDeliverypointExternalId);
                            }
                        }
                        else
                            this.WriteToLogExtended("SaveOrder", "Guest check '{0}' closed", posDeliverypointExternalId);
                    }
                    catch (Exception ex)
                    {
                        if (processingError == OrderProcessingError.None)
                        {
                            processingError = OrderProcessingError.VectronCouldNotCloseGuestCheck;
                            this.errorMessage = string.Format("An exception was thrown while trying to close guest check '{0}'. Exception: {1}", posDeliverypointExternalId, ex.Message);
                        }
                    }
                }

                // Try the close the clerk
                if (clerkOpened)
                {
                    try
                    {
                        if (!this.vectron.ClerkClose(ref this.clerkNo))
                        {
                            if (processingError == OrderProcessingError.None)
                            {
                                processingError = OrderProcessingError.VectronCouldNotCloseClerk;
                                this.errorMessage = string.Format("The clerk with number '{0}' could not be closed!", this.clerkNo);
                            }
                        }
                        else
                            this.WriteToLogExtended("SaveOrder", "Clerk '{0}' closed", this.clerkNo);
                    }
                    catch (Exception ex)
                    {
                        if (processingError == OrderProcessingError.None)
                        {
                            processingError = OrderProcessingError.VectronCouldNotCloseClerk;
                            this.errorMessage = string.Format("An exception was thrown while trying to close clerk '{0}'. Exception: {1}", this.clerkNo, ex.Message);
                        }
                    }
                }

                // Try to hide the output
                try
                {
                    // Check whether the output is displayed
                    if (this.displayOutput)
                        this.vectron.Hide();
                }
                catch (Exception ex)
                {
                    if (processingError == OrderProcessingError.None)
                    {
                        processingError = OrderProcessingError.VectronCouldNotHideOutput;
                        this.errorMessage = string.Format("An exception was thrown while trying to hide the output of the Flexipos ordering. Exception: {0}", ex.Message);
                    }
                }
            }

            if (processingError != OrderProcessingError.None)
            {
                // Throw a typed POSException
                POSException.ThrowTyped(processingError, null, this.errorMessage, null);
            }

            this.WriteToLogExtended("SaveOrder", "End {0}", TimeStamp.CreateTimeStamp());

            return processingError;
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <returns>True if printing the confirmation receipt was succesful, False if not</returns>
        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>True if printing the service request receipt was successful, False if not</returns>
        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>True if printing the checkout request was sucessful, False if not</returns>
        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public bool NotifyLockedDeliverypoint()
        {
            bool success = true;

            short posDeliverypointExternalId = ConfigurationManager.GetShort(POSConfigurationConstants.VectronOtouchoGuestCheckNr);
            short pluNo = ConfigurationManager.GetShort(POSConfigurationConstants.VectronDeliverypointLockedPLU);
            short quantity = 1;
            bool clerkOpened = false;
            bool guestCheckOpened = false;

            this.WriteToLogExtended("NotifyLockedDeliverypoint", "Start {0} with Otoucho guest check nr '{1}' and PLU '{2}'", TimeStamp.CreateTimeStamp(), posDeliverypointExternalId, pluNo);

            if (pluNo == -1)
                this.WriteToLogExtended("NotifyLockedDeliverypoint", "No deliverypoint locked PLU configured");

            if (posDeliverypointExternalId > 0 && pluNo > 0)
            {
                // Initialize FlexiPos
                this.InitializeFlexiPos();

                // Check whether the FlexiPos.OrderClass was initialized
                if (!this.initialized)
                {
                    success = false;
                    this.errorMessage = "VectronConnector is not initialized.";
                }
                else
                {
                    // Try to display the output
                    try
                    {
                        // Check whether the output should be displayed
                        if (this.displayOutput)
                            this.vectron.Show();
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        this.errorMessage = string.Format("An exception was thrown while trying to display the output of the Flexipos ordering. Exception: {0}", ex.Message);
                    }

                    // Try to open the clerk
                    try
                    {
                        // Login the employee
                        if (this.vectron.ClerkOpen(ref this.clerkNo))
                        {
                            clerkOpened = true;
                            this.WriteToLogExtended("NotifyLockedDeliverypoint", "Clerk '{0}' opened", this.clerkNo);
                        }
                        else
                        {
                            success = false;
                            this.errorMessage = string.Format("The clerk with number '{0}' could not be opened!", this.clerkNo);
                        }
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        this.errorMessage = string.Format("An exception was thrown while trying to open clerk '{0}'. Exception: {1}", this.clerkNo, ex.Message);
                    }

                    // Try to open the guest check
                    if (success)
                    {
                        try
                        {
                            // Open a guest check for the specified table no
                            if (this.vectron.GuestCOpen(ref posDeliverypointExternalId))
                            {
                                guestCheckOpened = true;
                                this.WriteToLogExtended("NotifyLockedDeliverypoint", "Guest check '{0}' opened", posDeliverypointExternalId);
                            }
                            else
                            {
                                success = false;
                                this.errorMessage = string.Format("The guest check for table '{0}' could not be opened!", posDeliverypointExternalId);
                            }
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            this.errorMessage = string.Format("An exception was thrown while trying to open guest check '{0}'. Exception: {1}", posDeliverypointExternalId, ex.Message);
                        }
                    }

                    // Try to add the products to the guest check
                    if (success)
                    {
                        try
                        {
                            // Add the product to the check
                            if (!this.vectron.PLU(ref quantity, ref pluNo))
                            {
                                success = false;
                                this.errorMessage = string.Format("The product with PLU '{0}' and quantity '{1}' could not be added to the check!", pluNo, quantity);
                            }
                            else
                                this.WriteToLogExtended("NotifyLockedDeliverypoint", "PLU '{0}' and quantity '{1}' added to the check", pluNo, quantity);
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            this.errorMessage = string.Format("An exception was thrown while trying to add PLU '{0}' with quantity '{1}'. Exception: {2}", pluNo, quantity, ex.Message);
                        }
                    }

                    // Try to close the guest check
                    if (guestCheckOpened)
                    {
                        try
                        {
                            // Close the guest check for the specified table no
                            if (!this.vectron.GuestCClose(ref posDeliverypointExternalId))
                            {
                                success = false;
                                this.errorMessage = string.Format("The guest check for table '{0}' could not be closed!", posDeliverypointExternalId);
                            }
                            else
                                this.WriteToLogExtended("NotifyLockedDeliverypoint", "Guest check '{0}' closed", posDeliverypointExternalId);
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            this.errorMessage = string.Format("An exception was thrown while trying to close guest check '{0}'. Exception: {1}", posDeliverypointExternalId, ex.Message);
                        }
                    }

                    // Try the close the clerk
                    if (clerkOpened)
                    {
                        try
                        {
                            if (!this.vectron.ClerkClose(ref this.clerkNo))
                            {
                                success = false;
                                this.errorMessage = string.Format("The clerk with number '{0}' could not be closed!", this.clerkNo);
                            }
                            else
                                this.WriteToLogExtended("NotifyLockedDeliverypoint", "Clerk '{0}' closed", this.clerkNo);
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            this.errorMessage = string.Format("An exception was thrown while trying to close clerk '{0}'. Exception: {1}", this.clerkNo, ex.Message);
                        }
                    }

                    // Try to hide the output
                    try
                    {
                        // Check whether the output is displayed
                        if (this.displayOutput)
                            this.vectron.Hide();
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        this.errorMessage = string.Format("An exception was thrown while trying to hide the output of the Flexipos ordering. Exception: {0}", ex.Message);
                    }
                }
            }

            this.WriteToLogExtended("NotifyLockedDeliverypoint", "End {0}", TimeStamp.CreateTimeStamp());

            return success;
        }

        #endregion

        /// <summary>
        /// Writes to log normal.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogNormal(string methodName, string message, params object[] args)
        {
            DesktopLogger.Debug("VectronConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        /// <summary>
        /// Writes to log extended.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            DesktopLogger.Verbose("VectronConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the COM port
        /// </summary>        
        public short ComPort
        {
            get
            {
                return this.comPort;
            }
            set
            {
                this.comPort = value;
            }
        }

        /// <summary>
        /// Gets or sets the baudrate
        /// </summary>        
        public string Baudrate
        {
            get
            {
                return this.baudrate;
            }
            set
            {
                this.baudrate = value;
            }
        }

        /// <summary>
        /// Gets or sets the word length
        /// </summary>        
        public string WordLength
        {
            get
            {
                return this.wordLength;
            }
            set
            {
                this.wordLength = value;
            }
        }

        /// <summary>
        /// Gets or sets the parity
        /// </summary>        
        public string Parity
        {
            get
            {
                return this.parity;
            }
            set
            {
                this.parity = value;
            }
        }

        /// <summary>
        /// Gets or sets the stop bits
        /// </summary>        
        public string Stopbits
        {
            get
            {
                return this.stopBits;
            }
            set
            {
                this.stopBits = value;
            }
        }

        /// <summary>
        /// Gets or sets the length of the clerk number
        /// </summary>        
        public short LengthClerkNr
        {
            get
            {
                return this.lengthClerkNr;
            }
            set
            {
                this.lengthClerkNr = value;
            }
        }

        /// <summary>
        /// Gets or sets the length of the guest check number 
        /// </summary>        
        public short LengthGuestCheckNr
        {
            get
            {
                return this.lengthGuestCheckNr;
            }
            set
            {
                this.lengthGuestCheckNr = value;
            }
        }

        /// <summary>
        /// Gets or sets the length of the PLU number
        /// </summary>        
        public short LengthPluNr
        {
            get
            {
                return this.lengthPluNr;
            }
            set
            {
                this.lengthPluNr = value;
            }
        }

        /// <summary>
        /// Gets or sets the timeout
        /// </summary>        
        public short TimeOut
        {
            get
            {
                return this.timeOut;
            }
            set
            {
                this.timeOut = value;
            }
        }

        /// <summary>
        /// Gets or sets the path to the FlexiPos logfile
        /// </summary>        
        public string PathLogFile
        {
            get
            {
                return this.pathLogFile;
            }
            set
            {
                this.pathLogFile = value;
            }
        }

        /// <summary>
        /// Gets or sets the file of the FlexiPos logfile
        /// </summary>        
        public string NameLogfile
        {
            get
            {
                return this.nameLogfile;
            }
            set
            {
                this.nameLogfile = value;
            }
        }

        /// <summary>
        /// Gets or sets the amount of days to keep the FlexiPos logfile
        /// </summary>        
        public short DaysLogfile
        {
            get
            {
                return this.daysLogfile;
            }
            set
            {
                this.daysLogfile = value;
            }
        }

        /// <summary>
        /// Gets or sets number of the Otoucho clerk
        /// </summary>        
        public short ClerkNo
        {
            get
            {
                return this.clerkNo;
            }
            set
            {
                this.clerkNo = value;
            }
        }

        /// <summary>
        /// Gets or sets flag which indicates whether the output should be displayed
        /// </summary>        
        public bool DisplayOutput
        {
            get
            {
                return this.displayOutput;
            }
            set
            {
                this.displayOutput = value;
            }
        }

        /// <summary>
        /// Gets or sets the categories file path.
        /// </summary>
        /// <value>The categories file path.</value>
        public string CategoriesFilePath
        {
            get
            {
                return this.categoriesFilePath;
            }
            set
            {
                this.categoriesFilePath = value;
            }
        }

        /// <summary>
        /// Gets or sets the products file path.
        /// </summary>
        /// <value>The products file path.</value>
        public string ProductsFilePath
        {
            get
            {
                return this.productsFilePath;
            }
            set
            {
                this.productsFilePath = value;
            }
        }

        #region IPOSConnector properties

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion

        #endregion
    }
}
