﻿
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.POS.Interfaces;
namespace Obymobi.Logic.POS.Vectron
{
    /// <summary>
    /// VectronConfigurationAdapter class
    /// </summary>
    public class VectronConfigurationAdapter : IConfigurationAdapter
    {
        #region Fields

        // Pos value 1 
        private short comPort = 5;
        // Pos value 2 
        private string baudrate = "9600";
        // Pos value 3 
        private string wordLength = "8";
        // Pos value 4 
        private string parity = "Even";
        // Pos value 5 
        private string stopBits = "1";
        // Pos value 6 
        private short lengthClerkNr = 3;
        // Pos value 7 
        private short lengthPluNr = 3;
        // Pos value 8 
        private short timeOut = 1000;
        // Pos value 9 
        private string pathLogFile = @"c:\vectron\flexiposorder\";
        // Pos value 10 
        private string nameLogFile = "FlexiPos.log";
        // Pos value 11
        private short clerkNumber = 888;
        // Pos value 12 
        private bool displayOutput = false;
        /// <summary>
        /// Gets or sets the length guest check nr.
        /// Pos value 13 
        /// </summary>
        /// <value>
        /// The length guest check nr.
        /// </value>
        public short LengthGuestCheckNr { get; set; }
        /// <summary>
        /// Gets or sets the days log file.
        /// </summary>
        /// <value>
        /// The days log file.
        /// Pos value 14 
        /// </value>
        public short DaysLogFile { get; set; }
        /// <summary>
        /// Gets or sets the otoucho guest check nr.
        /// </summary>
        /// <value>
        /// The otoucho guest check nr.
        /// Pos value 15
        /// </value>
        public short OtouchoGuestCheckNr { get; set; }
        /// <summary>
        /// Gets or sets the deliverypoint locked plu.
        /// </summary>
        /// <value>
        /// The deliverypoint locked plu.
        /// Pos value 16
        /// </value>
        public short DeliverypointLockedPlu { get; set; }



        #endregion

        #region Methods

        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Terminal terminal)
        {
            this.ComPort = short.Parse(terminal.PosValue1);
            this.Baudrate = terminal.PosValue2;
            this.WordLength = terminal.PosValue3;
            this.Parity = terminal.PosValue4;
            this.StopBits = terminal.PosValue5;
            this.LengthClerkNr = short.Parse(terminal.PosValue6);
            this.LengthPluNr = short.Parse(terminal.PosValue7);
            this.TimeOut = short.Parse(terminal.PosValue8);
            this.PathLogFile = terminal.PosValue9;
            this.NameLogFile = terminal.PosValue10;
            this.ClerkNumber = short.Parse(terminal.PosValue11);
            this.DisplayOutput = bool.Parse(terminal.PosValue12);
            this.LengthGuestCheckNr = short.Parse(terminal.PosValue13);
            this.DaysLogFile = short.Parse(terminal.PosValue14);
            this.OtouchoGuestCheckNr = short.Parse(terminal.PosValue15);
            this.DeliverypointLockedPlu = short.Parse(terminal.PosValue16);
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(TerminalEntity terminal)
        {
            this.ComPort = short.Parse(terminal.PosValue1);
            this.Baudrate = terminal.PosValue2;
            this.WordLength = terminal.PosValue3;
            this.Parity = terminal.PosValue4;
            this.StopBits = terminal.PosValue5;
            this.LengthClerkNr = short.Parse(terminal.PosValue6);
            this.LengthPluNr = short.Parse(terminal.PosValue7);
            this.TimeOut = short.Parse(terminal.PosValue8);
            this.PathLogFile = terminal.PosValue9;
            this.NameLogFile = terminal.PosValue10;
            this.ClerkNumber = short.Parse(terminal.PosValue11);
            this.DisplayOutput = bool.Parse(terminal.PosValue12);
            this.LengthGuestCheckNr = short.Parse(terminal.PosValue13);
            this.DaysLogFile = short.Parse(terminal.PosValue14);
            this.OtouchoGuestCheckNr = short.Parse(terminal.PosValue15);
            this.DeliverypointLockedPlu = short.Parse(terminal.PosValue16);
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.ComPort = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronComPort);
            this.Baudrate = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.VectronBaudrate);
            this.WordLength = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.VectronWordLength);
            this.Parity = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.VectronParity);
            this.StopBits = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.VectronStopbits);
            this.LengthClerkNr = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronLengthClerkNr);
            this.LengthPluNr = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronLengthPluNr);
            this.TimeOut = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronTimeOut);
            this.PathLogFile = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.VectronPathLogFile);
            this.NameLogFile = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.VectronNameLogfile);
            this.ClerkNumber = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronClerkNumber);
            this.DisplayOutput = Dionysos.ConfigurationManager.GetBool(POSConfigurationConstants.VectronDisplayOutput);
            this.LengthGuestCheckNr = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronLengthGuestCheckNr);
            this.DaysLogFile = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronDaysLogfile);
            this.OtouchoGuestCheckNr = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronOtouchoGuestCheckNr);
            this.DeliverypointLockedPlu = Dionysos.ConfigurationManager.GetShort(POSConfigurationConstants.VectronDeliverypointLockedPLU);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronComPort, this.ComPort);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronBaudrate, this.Baudrate);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronWordLength, this.WordLength);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronParity, this.Parity);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronStopbits, this.StopBits);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronLengthClerkNr, this.LengthClerkNr);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronLengthPluNr, this.LengthPluNr);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronTimeOut, this.TimeOut);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronPathLogFile, this.PathLogFile);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronNameLogfile, this.NameLogFile);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronClerkNumber, this.ClerkNumber);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronDisplayOutput, this.DisplayOutput);

            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronLengthGuestCheckNr, this.LengthGuestCheckNr);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronDaysLogfile, this.DaysLogFile);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronOtouchoGuestCheckNr, this.OtouchoGuestCheckNr);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.VectronDeliverypointLockedPLU, this.DeliverypointLockedPlu);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(TerminalEntity terminal)
        {
            terminal.PosValue1 = this.ComPort.ToString();
            terminal.PosValue2 = this.Baudrate;
            terminal.PosValue3 = this.WordLength;
            terminal.PosValue4 = this.Parity;
            terminal.PosValue5 = this.StopBits;
            terminal.PosValue6 = this.LengthClerkNr.ToString();
            terminal.PosValue7 = this.LengthPluNr.ToString();
            terminal.PosValue8 = this.TimeOut.ToString();
            terminal.PosValue9 = this.PathLogFile;
            terminal.PosValue10 = this.NameLogFile;
            terminal.PosValue11 = this.ClerkNumber.ToString();
            terminal.PosValue12 = this.DisplayOutput.ToString();

            terminal.PosValue13 = this.LengthGuestCheckNr.ToString();
            terminal.PosValue14 = this.DaysLogFile.ToString();
            terminal.PosValue15 = this.OtouchoGuestCheckNr.ToString();
            terminal.PosValue16 = this.DeliverypointLockedPlu.ToString();
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Terminal terminal)
        {
            terminal.PosValue1 = this.ComPort.ToString();
            terminal.PosValue2 = this.Baudrate;
            terminal.PosValue3 = this.WordLength;
            terminal.PosValue4 = this.Parity;
            terminal.PosValue5 = this.StopBits;
            terminal.PosValue6 = this.LengthClerkNr.ToString();
            terminal.PosValue7 = this.LengthPluNr.ToString();
            terminal.PosValue8 = this.TimeOut.ToString();
            terminal.PosValue9 = this.PathLogFile;
            terminal.PosValue10 = this.NameLogFile;
            terminal.PosValue11 = this.ClerkNumber.ToString();
            terminal.PosValue12 = this.DisplayOutput.ToString();

            terminal.PosValue13 = this.LengthGuestCheckNr.ToString();
            terminal.PosValue14 = this.DaysLogFile.ToString();
            terminal.PosValue15 = this.OtouchoGuestCheckNr.ToString();
            terminal.PosValue16 = this.DeliverypointLockedPlu.ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the displayOutput
        /// </summary>
        public bool DisplayOutput
        {
            get
            {
                return this.displayOutput;
            }
            set
            {
                this.displayOutput = value;
            }
        }


        /// <summary>
        /// Gets or sets the clerkNumber
        /// </summary>
        public short ClerkNumber
        {
            get
            {
                return this.clerkNumber;
            }
            set
            {
                this.clerkNumber = value;
            }
        }


        /// <summary>
        /// Gets or sets the nameLogFile
        /// </summary>
        public string NameLogFile
        {
            get
            {
                return this.nameLogFile;
            }
            set
            {
                this.nameLogFile = value;
            }
        }


        /// <summary>
        /// Gets or sets the pathLogFile
        /// </summary>
        public string PathLogFile
        {
            get
            {
                return this.pathLogFile;
            }
            set
            {
                this.pathLogFile = value;
            }
        }


        /// <summary>
        /// Gets or sets the timeOut
        /// </summary>
        public short TimeOut
        {
            get
            {
                return this.timeOut;
            }
            set
            {
                this.timeOut = value;
            }
        }


        /// <summary>
        /// Gets or sets the lengthPluNr
        /// </summary>
        public short LengthPluNr
        {
            get
            {
                return this.lengthPluNr;
            }
            set
            {
                this.lengthPluNr = value;
            }
        }



        /// <summary>
        /// Gets or sets the clerkNr
        /// </summary>
        public short LengthClerkNr
        {
            get
            {
                return this.lengthClerkNr;
            }
            set
            {
                this.lengthClerkNr = value;
            }
        }


        /// <summary>
        /// Gets or sets the stopBits
        /// </summary>
        public string StopBits
        {
            get
            {
                return this.stopBits;
            }
            set
            {
                this.stopBits = value;
            }
        }


        /// <summary>
        /// Gets or sets the parity
        /// </summary>
        public string Parity
        {
            get
            {
                return this.parity;
            }
            set
            {
                this.parity = value;
            }
        }


        /// <summary>
        /// Gets or sets the wordLength
        /// </summary>
        public string WordLength
        {
            get
            {
                return this.wordLength;
            }
            set
            {
                this.wordLength = value;
            }
        }

        /// <summary>
        /// Gets or sets the Baudrate
        /// </summary>
        public string Baudrate
        {
            get
            {
                return this.baudrate;
            }
            set
            {
                this.baudrate = value;
            }
        }


        /// <summary>
        /// Gets or sets the comPort
        /// </summary>
        public short ComPort
        {
            get
            {
                return this.comPort;
            }
            set
            {
                this.comPort = value;
            }
        }

        #endregion
    }
}
