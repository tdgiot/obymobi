﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Logic.POS.FuturePos
{
    /// <summary>
    /// FuturePosConfigurationAdapter class
    /// </summary>
    public class FuturePosConfigurationAdapter : IConfigurationAdapter
    {
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.FuturePosConnectionString = terminal.PosValue1;
            this.FuturePosPriceLevel = terminal.PosValue2;
            this.FuturePosLayoutRooms = terminal.PosValue3;
            this.FuturePosApiUrl = terminal.PosValue4;
            this.FuturePosApiUsername = terminal.PosValue5;
            this.FuturePosApiPassword = terminal.PosValue6;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.FuturePosConnectionString = terminal.PosValue1;
            this.FuturePosPriceLevel = terminal.PosValue2;
            this.FuturePosLayoutRooms = terminal.PosValue3;
            this.FuturePosApiUrl = terminal.PosValue4;
            this.FuturePosApiUsername = terminal.PosValue5;
            this.FuturePosApiPassword = terminal.PosValue6;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.FuturePosConnectionString = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosConnectionString);
            this.FuturePosPriceLevel = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosPriceLevel);
            this.FuturePosLayoutRooms = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosLayoutRooms);
            this.FuturePosApiUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosApiUrl);
            this.FuturePosApiUsername = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosApiUsername);
            this.FuturePosApiPassword = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosApiPassword);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.FuturePosConnectionString, this.FuturePosConnectionString);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.FuturePosPriceLevel, this.FuturePosPriceLevel);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.FuturePosLayoutRooms, this.FuturePosLayoutRooms);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.FuturePosApiUrl, this.FuturePosApiUrl);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.FuturePosApiUsername, this.FuturePosApiUsername);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.FuturePosApiPassword, this.FuturePosApiPassword);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.FuturePosConnectionString;
            terminal.PosValue2 = this.FuturePosPriceLevel;
            terminal.PosValue3 = this.FuturePosLayoutRooms;
            terminal.PosValue4 = this.FuturePosApiUrl;
            terminal.PosValue5 = this.FuturePosApiUsername;
            terminal.PosValue6 = this.FuturePosApiPassword;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.FuturePosConnectionString;
            terminal.PosValue2 = this.FuturePosPriceLevel;
            terminal.PosValue3 = this.FuturePosLayoutRooms;
            terminal.PosValue4 = this.FuturePosApiUrl;
            terminal.PosValue5 = this.FuturePosApiUsername;
            terminal.PosValue6 = this.FuturePosApiPassword;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the future pos connection string.
        /// </summary>
        /// <value>
        /// The future pos connection string.
        /// </value>
        public string FuturePosConnectionString { get; set; }
        /// <summary>
        /// Gets or sets the future pos price level.
        /// </summary>
        /// <value>
        /// The future pos price level.
        /// </value>
        public string FuturePosPriceLevel { get; set; }
        /// <summary>
        /// Gets or sets the future pos layout rooms.
        /// </summary>
        /// <value>
        /// The future pos layout rooms.
        /// </value>
        public string FuturePosLayoutRooms { get; set; }
        /// <summary>
        /// Gets or sets the future pos API URL.
        /// </summary>
        /// <value>
        /// The future pos API URL.
        /// </value>
        public string FuturePosApiUrl { get; set; }
        /// <summary>
        /// Gets or sets the future pos API username.
        /// </summary>
        /// <value>
        /// The future pos API username.
        /// </value>
        public string FuturePosApiUsername { get; set; }
        /// <summary>
        /// Gets or sets the future pos API password.
        /// </summary>
        /// <value>
        /// The future pos API password.
        /// </value>
        public string FuturePosApiPassword { get; set; }

        #endregion
    }
}
