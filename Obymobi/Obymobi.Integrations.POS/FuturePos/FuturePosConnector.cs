﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Logic.Loggers;
using Obymobi.Logic.Untill2Webservice;
using System.IO;
using Dionysos.Diagnostics;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.POS.Generic;
using System.Text.RegularExpressions;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Pos.FuturePos.Data.HelperClasses;
using Obymobi.Logic.Pos.FuturePos.Data;
using Obymobi.Logic.Pos.FuturePos.Data.EntityClasses;
using Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses;
using Obymobi.Logic.FuturePosWebservice;
using System.Xml;
using Dionysos.Data.LLBLGen;

namespace Obymobi.Logic.POS.FuturePos
{
    /// <summary>
    /// DummyConnector
    /// </summary>
    public class FuturePosConnector : PosConnectorBase, IPOSConnector
    {
        #region Fields

        // Retrieve the settings from the config file
        private string connectionString = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosConnectionString);
        private string priceLevel = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosPriceLevel);
        private string layoutRooms = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosLayoutRooms);
        private string apiUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosApiUrl);
        private string apiUsername = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosApiUsername);
        private string apiPassword = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.FuturePosApiPassword);
        private string databaseCatalog = string.Empty;
        private TISSLMenu webservice = null;

        private LayoutRoomCollection layoutRoomCollection = null;
        private DepartmentCollection departmentCollection = null;

        // Handy to keep a sync report
        private StringBuilder syncReport = new StringBuilder();
        private List<string> reportedNonFoundItems = new List<string>();

        #endregion

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="FuturePosConnector"/> class.
        /// </summary>
        public FuturePosConnector()
        {
            this.WriteToLogExtended("Constructor", "Start");

            base.ConnectorType = POSConnectorType.FuturePos;

            bool configError = false;

            if (this.connectionString.IsNullOrWhiteSpace())
            {
                configError = true;
                this.WriteToLogNormal("Constructor", "FuturePosConnectionString is empty");
            }

            // That's ok.
            //if (this.priceLevel.IsNullOrWhiteSpace())
            //{
            //    configError = true;
            //    this.WriteToLogNormal("Constructor", "FuturePosPriceLevel is empty");
            //}

            // That's ok.
            //if (this.layoutSections.IsNullOrWhiteSpace())
            //{
            //    configError = true;
            //    this.WriteToLogNormal("Constructor", "FuturePosLayoutSections is empty");
            //}

            if (TestUtil.IsPcGabriel)
            {
                this.WriteToLogNormal("PC GABRIEL - DOESN'T VALIDATE API SETTINGS", "");
                this.WriteToLogNormal("PC GABRIEL - DOESN'T VALIDATE API SETTINGS", "");
                this.WriteToLogNormal("PC GABRIEL - DOESN'T VALIDATE API SETTINGS", "");
                this.WriteToLogNormal("PC GABRIEL - DOESN'T VALIDATE API SETTINGS", "");
            }
            else
            {
                if (this.apiUrl.IsNullOrWhiteSpace())
                {
                    configError = true;
                    this.WriteToLogNormal("Constructor", "FuturePosApiUrl is empty");
                }

                // GK Currently the webservice doesn't have credentials.
                //if (this.apiUsername.IsNullOrWhiteSpace())
                //{
                //    configError = true;
                //    this.WriteToLogNormal("Constructor", "FuturePosApiUsername is empty");
                //}

                //if (this.apiPassword.IsNullOrWhiteSpace())
                //{
                //    configError = true;
                //    this.WriteToLogNormal("Constructor", "FuturePosApiPassword is empty");
                //}
            }

            // Get Database from Connection STring
            Regex catalogRegEx = new Regex(@"Initial Catalog=(?<Catalog>.*?);", RegexOptions.IgnoreCase);

            if (catalogRegEx.IsMatch(this.connectionString))
            {
                this.databaseCatalog = catalogRegEx.Match(this.connectionString).Groups["Catalog"].Value;
            }
            else
            {
                configError = true;
                this.WriteToLogNormal("Constructor", "Initial Catalog (RegEx:'Initial Catalog=(?<Catalog>.*?);') could not be found in ConnectionString");
            }

            this.WriteToLogNormal("Constructor", "FuturePosConnectionString: {0}", this.connectionString);
            this.WriteToLogNormal("Constructor", "FuturePosPriceLevel: {0}", this.priceLevel);
            this.WriteToLogNormal("Constructor", "FuturePosLayoutSections: {0}", this.layoutRooms);
            this.WriteToLogNormal("Constructor", "FuturePosApiUrl: {0}", this.apiUrl);
            //this.WriteToLogNormal("Constructor", "FuturePosApiUsername: {0}", this.apiUsername);
            //this.WriteToLogNormal("Constructor", "FuturePosApiPassword: {0}", this.apiPassword);
            this.WriteToLogNormal("Constructor", "DatabaseCatalog: {0}", this.databaseCatalog);

            // Set the connection string for database access
            Obymobi.Logic.Pos.FuturePos.Data.DaoClasses.CommonDaoBase.ActualConnectionString = this.connectionString;

            // Verify Database Connection
            this.VerifySqlConnection(ref configError);
            if (configError)
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, "Sql Server connection not working.");

            // Verify Webservice Connection
            if (!this.VerifyWebserviceConnection())
                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, "Webservice connection not working.");

            this.WriteToLogExtended("Constructor", "End");
        }

        /// <summary>
        /// Verifies the webservice connection.
        /// </summary>
        /// <returns></returns>
        private bool VerifyWebserviceConnection()
        {
            try
            {
                string[] openTables = this.Webservice.GetOpenTables();
                return true;
            }
            catch (Exception ex)
            {
                this.WriteToLogNormal("Constructor", "Webservice connection failed: {0}", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Verifies the SQL connection.
        /// </summary>
        /// <param name="configError">if set to <c>true</c> [config error].</param>
        private void VerifySqlConnection(ref bool configError)
        {
            System.Data.SqlClient.SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new System.Data.SqlClient.SqlConnection(this.connectionString);

                sqlConnection.Open();

                // 4. Create the SQL Command and assign it it a string 
                string strSQLCommand = "SELECT COUNT(*) FROM Item";

                // 5. Execute the SQL Command 
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(strSQLCommand, sqlConnection);

                // 6. Use ExecuteScalar() to return the first result 
                string itemCount = command.ExecuteScalar().ToString();
                this.WriteToLogNormal("Constructor", "Database Connection Test: Success (Item count: {0})", itemCount);
            }
            catch (Exception ex)
            {
                this.WriteToLogNormal("Constructor", "Database connection failed: {0}", ex.Message);

                throw new POSException(OrderProcessingError.PosErrorCouldNotInitializeConnector, ex, "FuturePosConnector couldn't be initialized: Connection String is invalid");
            }
            finally
            {
                if (sqlConnection != null)
                    sqlConnection.Dispose();
            }
        }

        /// <summary>
        /// Gets the products from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posproduct instances
        /// </returns>
        public override Model.Posproduct[] GetPosproducts()
        {
            this.WriteToLogExtended("GetPosproducts", "Start");

            this.syncReport = new StringBuilder();
            this.syncReport.AppendFormatLine("\r\n\r\n");
            this.syncReport.AppendFormatLine("******** SYNC START ********");
            this.syncReport.AppendFormatLine("Sync started at: {0}", DateTime.Now.ToString());

            // Log the settings where you sync with:
            //this.syncReport.AppendFormatLine("SalesArea: {0}", this.salesArea);
            //this.syncReport.AppendFormatLine("PriceGroup: {0}", this.pricegroupName);
            List<Posproduct> products = new List<Posproduct>();

            try
            {
                // Do the logic, log extensively.

                // Retrieve the Items.
                Obymobi.Logic.Pos.FuturePos.Data.CollectionClasses.ItemCollection items = new Pos.FuturePos.Data.CollectionClasses.ItemCollection();

                PrefetchPath pathItems = new PrefetchPath(EntityType.ItemEntity);
                pathItems.Add(ItemEntity.PrefetchPathItemPrice);

                SortExpression sort = new SortExpression();
                sort.Add(ItemFields.ItemName | SortOperator.Ascending);

                items.GetMulti(null, 0, sort, null, pathItems);

                // Create a product for each item
                foreach (var item in items)
                {
                    Posproduct product = new Posproduct();
                    product.ExternalId = item.ItemName.ToString();
                    product.Name = item.ItemDescription;

                    if (item.ItemPrice.Count > 0)
                    {
                        // Always just take first
                        product.PriceIn = (Convert.ToDecimal(item.ItemPrice[0].DefaultPrice) / 100m);
                    }
                    else
                        product.PriceIn = 0;

                    var department = this.departmentCollection.FirstOrDefault(d => d.DepartmentName == item.Department);
                    if (department != null)
                    {
                        product.ExternalPoscategoryId = department.DepartmentId.ToString();
                    }

                    // Alterations
                    product.Posalterations = this.GetAlterationsForProduct(item).ToArray();

                    // Default:
                    product.VatTariff = 2;

                    products.Add(product);
                }

                // Reset from memory:
                if (departmentCollection != null)
                {
                    this.departmentCollection.Dispose();
                    this.departmentCollection = null;
                }

                this.syncReport.AppendFormatLine("Sync finished with success at: {0}", DateTime.Now.ToString());
                this.syncReport.AppendFormatLine("******** SYNC FINISH ********");
                this.WriteToPosConnectorLog("GetPosproducts", this.syncReport.ToString());

                this.WriteToLogExtended("GetPosproducts", "End - Success");

                return products.ToArray();
            }
            catch
            {
                this.WriteToLogExtended("GetPosproducts", "End - Failure");
                this.syncReport.AppendFormatLine("Sync finished with failure at: {0}", DateTime.Now.ToString());
                this.WriteToPosConnectorLog("GetPosproducts", this.syncReport.ToString());

                throw;
            }
        }

        /// <summary>
        /// Gets the alterations for product.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private List<Posalteration> GetAlterationsForProduct(ItemEntity item)
        {
            // First we have to determine the way in which the Alterations are bound:
            // 1. In this scenario only one Alteration is posibble. The information about the Alteration is stored in 
            // the Item record itself. De ItemModifier table contains the AlterationOptions for that Alteration. 
            // So in this scenario 
            // Item = Product & Alteration 
            // Item.ItemModifier = AlterationItem
            // Item.ItemModifier.Item = AlterationOption 
            // 2. In this scenario one or more Alterations are posibble. The structure is:
            // Item = Product
            // Item.ItemModifier = ProductAlteration
            // Item.ItemModifier.Item = Alteration
            // Item.ItemModifier.Item.ItemModifer = AlterationItem
            // Item.ItemModifier.Item.ItemModifer.Item = AlterationOption
            // We assume that it's either 1 or 2. So we retrieve the first Modifier for an article, test if it has it's own Items (= scenario 2)            
            List<Posalteration> alterations = new List<Posalteration>();

            // Get first level modifiers
            ItemModifierCollection levelOneModifiers = new ItemModifierCollection();
            PredicateExpression levelOneFilter = new PredicateExpression();
            levelOneFilter.Add(ItemModifierFields.ItemId == item.ItemId);
            levelOneModifiers.GetMulti(levelOneFilter);

            // Get first level modifiers for item
            if (levelOneModifiers.Count > 0)
            {
                // Get the Item for this Modifier
                bool nestedModifiers = this.HasNestedModifiers(levelOneModifiers);

                if (nestedModifiers)
                {
                    // Here the Item.ItemModifiers are equal to our Alterations and their
                    // linked  ItemModifier.Item - ItemModifiers.Item are the AlterationOptions
                    foreach (var modifier in levelOneModifiers)
                    {
                        // This is the ProductAlteration.Alteration level.
                        ItemEntity itemForAlteration = new ItemEntity();
                        if (this.IsValidModifier(modifier, out itemForAlteration))
                            alterations.Add(this.ConvertItemToAlteration(itemForAlteration));
                    }
                }
                else
                {
                    // Here the Item itself contains the information of the Alteration (Name, Max, Min)
                    // and AlterationOptions are the ItemModifiers
                    alterations.Add(this.ConvertItemToAlteration(item));
                }
            }
            else
            {
                // No alterations
            }

            return alterations;
        }

        /// <summary>
        /// Converts the item to alteration.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private Posalteration ConvertItemToAlteration(ItemEntity item)
        {
            Posalteration alteration = new Posalteration();
            alteration.ExternalId = item.ItemName;
            alteration.Name = item.ModifierDescription;

            // Set Min amount to select
            if (item.UseModifierMinSelect)
                alteration.MinOptions = item.ModifierMinSelect;
            else
                alteration.MinOptions = null;

            // Set Max amount to select
            if (item.UseModifierMaxSelect)
                alteration.MaxOptions = item.ModifierMaxSelect;
            else
                alteration.MaxOptions = null;

            // Convert to ItemModifier to Alteration options
            ItemModifierCollection modifiers = new ItemModifierCollection();
            PredicateExpression modifierFilter = new PredicateExpression();
            modifierFilter.Add(ItemModifierFields.ItemId == item.ItemId);
            modifiers.GetMulti(modifierFilter);

            List<Posalterationoption> options = new List<Posalterationoption>();
            foreach (var modifier in modifiers)
            {
                Posalterationoption option = this.ConvertModifierToAlterationoption(modifier);
                if (option != null)
                    options.Add(option);
            }

            alteration.Posalterationoptions = options.ToArray();

            return alteration;
        }

        /// <summary>
        /// Converts the modifier to alterationoption.
        /// </summary>
        /// <param name="modifier">The modifier.</param>
        /// <returns></returns>
        private Posalterationoption ConvertModifierToAlterationoption(ItemModifierEntity modifier)
        {
            // Set the basic value (name & id's)
            Posalterationoption option = null;
            ItemEntity item;
            if (this.IsValidModifier(modifier, out item))
            {
                option = new Posalterationoption();
                option.ExternalId = item.ItemName;//.ItemModifierId.ToString();
                // option.PosproductExternalId = item.ItemName; > They work with 'real' alterations, so not needed to link Posproduct
                option.Name = item.ItemDescription;

                // Get a price
                bool priceFound = false;
                if (modifier.Item.ModifierPriceLevel > 0)
                {
                    // Get it at the specified level for the modifier.
                    string levelFieldName = string.Format("Level{0}Price", item.ModifierPriceLevel);
                    if (item.ItemPrice.Count > 0 &&
                        item.ItemPrice[0].Fields[levelFieldName].CurrentValue != null)
                    {
                        int priceInCent = (int)item.ItemPrice[0].Fields[levelFieldName].CurrentValue;
                        option.PriceIn = (Convert.ToDecimal(priceInCent) / 100m);
                    }
                }

                // If no price was found, take the default.
                if (!priceFound && item.ItemPrice.Count > 0)
                {
                    option.PriceIn = (Convert.ToDecimal(item.ItemPrice[0].DefaultPrice) / 100m);
                }
            }
            return option;
        }

        /// <summary>
        /// Determines if the supplied modifier has a Item in the Item table linked.
        /// This should be the case, but the database can have incositencies
        /// </summary>        
        /// <param name="modifier">Modifier for which to check if an Item exists for it</param>        
        /// <param name="itemForAlteration">Returns the Item that's linked to the modifier</param>
        /// <returns></returns>
        private bool IsValidModifier(ItemModifierEntity modifier, out ItemEntity itemForAlteration)
        {
            itemForAlteration = null;

            ItemCollection items = new ItemCollection();

            // check if it was checked before and failed then.
            if (!this.reportedNonFoundItems.Contains(modifier.ModifierName))
            {
                // Retrieve the Item linked to the ItemModifier, which contains the Alteration information
                items.GetMulti(ItemFields.ItemName == modifier.ModifierName);

                if (items.Count == 0)
                {
                    this.syncReport.AppendFormatLine("Item with ItemName '{0}' is not found.", modifier.ModifierName);
                }
                else if (items.Count > 1)
                {
                    this.syncReport.AppendFormatLine("Item with ItemName '{0}' is found multiple Items.", modifier.ModifierName);
                }
                else
                    itemForAlteration = items[0];

                // Report for which this Modifier is used
                if (itemForAlteration == null)
                {
                    this.reportedNonFoundItems.Add(modifier.ModifierName);

                    ItemModifierCollection itemModifiers = new ItemModifierCollection();
                    itemModifiers.GetMulti(ItemModifierFields.ModifierName == modifier.ModifierName);

                    this.syncReport.AppendFormatLine("The following Items use this Item: ");
                    foreach (var itemModifier in itemModifiers)
                    {
                        items.GetMulti(ItemFields.ItemId == itemModifier.ItemId);
                        if (items.Count == 1)
                        {
                            this.syncReport.AppendFormatLine("- '{0}' (Item.ItemName: '{1}', Item.ItemId '{2}', via ItemModifer.ItemModifierId '{3}')",
                                items[0].ItemDescription, items[0].ItemName, items[0].ItemId, itemModifier.ItemModifierId);
                        }
                    }
                    // Add emptuy line
                    this.syncReport.AppendLine();
                }
            }
            return (itemForAlteration != null);
        }

        /// <summary>
        /// Determines whether [has nested modifiers] [the specified level one modifiers].
        /// </summary>
        /// <param name="levelOneModifiers">The level one modifiers.</param>
        /// <returns>
        ///   <c>true</c> if [has nested modifiers] [the specified level one modifiers]; otherwise, <c>false</c>.
        /// </returns>
        private bool HasNestedModifiers(ItemModifierCollection levelOneModifiers)
        {
            bool nestedModifiers = false;
            ItemCollection items = new ItemCollection();
            ItemModifierCollection levelTwoModifiers = new ItemModifierCollection();
            foreach (var modifier in levelOneModifiers)
            {
                items.GetMulti(ItemFields.ItemName == levelOneModifiers[0].ModifierName);

                if (items.Count == 1)
                {
                    // We found a Modifier with one linked Item. 
                    nestedModifiers = (levelTwoModifiers.GetDbCount(ItemModifierFields.ItemId == items[0].ItemId) > 0);
                    break;
                }
            }
            return nestedModifiers;
        }

        /// <summary>
        /// Gets the categories from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Poscategory instances
        /// </returns>
        public override Model.Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();

            // Retrieve the Categories from the POS
            DepartmentCollection departments = new DepartmentCollection();
            departments.GetMulti(null);

            foreach (var department in departments)
            {
                Poscategory category = new Poscategory();

                // Can't use DepartmentID because in Item.Department it's linked on Department.DepartmentName
                category.ExternalId = department.DepartmentId.ToString();
                category.Name = department.DepartmentDescription;

                poscategories.Add(category);
            }

            this.departmentCollection = departments;

            return poscategories.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoint groups from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypointgroup instances
        /// </returns>
        public override Model.Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();

            // Retrieve the DeliverypointGroups from the POS            
            PredicateExpression filterRooms = new PredicateExpression();
            if (!this.layoutRooms.IsNullOrWhiteSpace())
            {
                List<string> roomNames = new List<string>();
                List<int> roomIndexes = new List<int>();

                string[] settingElements;
                if (this.layoutRooms.Contains(","))
                {
                    settingElements = this.layoutRooms.Split(',', StringSplitOptions.RemoveEmptyEntries);
                }
                else
                {
                    settingElements = new string[] { this.layoutRooms };
                }

                foreach (var element in settingElements)
                {
                    int roomIndex;
                    if (int.TryParse(element, out roomIndex))
                        roomIndexes.Add(roomIndex);
                    else
                        roomNames.Add(element);
                }

                if (roomIndexes.Count > 0)
                    filterRooms.AddWithOr(LayoutRoomFields.RoomIndex == roomIndexes);
                if (roomNames.Count > 0)
                    filterRooms.AddWithOr(LayoutRoomFields.RoomName == roomNames);
            }

            LayoutRoomCollection rooms = new LayoutRoomCollection();
            rooms.GetMulti(filterRooms);

            // Keep on the instance to later use in GetPosdeliverypoints
            this.layoutRoomCollection = rooms;

            foreach (var room in rooms)
            {
                Posdeliverypointgroup group = new Posdeliverypointgroup();
                group.ExternalId = room.LayoutRoomId.ToString();
                group.Name = room.RoomName;

                posdeliverypointgroups.Add(group);
            }

            return posdeliverypointgroups.ToArray();
        }

        /// <summary>
        /// Gets the deliverypoints from the point-of-service
        /// </summary>
        /// <returns>
        /// An System.Array instance containing Obymobi.Logic.Model.Posdeliverypoint instances
        /// </returns>
        public override Model.Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            PredicateExpression filterTables = new PredicateExpression();

            if (this.layoutRoomCollection != null && this.layoutRoomCollection.Count > 0)
            {
                // Retrieve the Posdeliverypoints from the POS
                var sectionIndexes = from r in this.layoutRoomCollection select r.RoomIndex;
                filterTables.Add(LayoutTableFields.SectionIndex == sectionIndexes.ToList());
            }

            LayoutTableCollection tables = new LayoutTableCollection();
            tables.GetMulti(filterTables);

            // Create a Deliverypoint for each Table            
            List<int> retrievedTableNumbers = new List<int>();
            foreach (var table in tables)
            {
                Posdeliverypoint deliverypoint = new Posdeliverypoint();

                int tableNumber;
                if (!table.TableName.IsNumeric() || !int.TryParse(table.TableName, out tableNumber))
                {
                    this.syncReport.AppendFormatLine("Table '{0}' not supported: TableName must contain only numbers. (LayoutTableId: '{1}')", table.TableName, table.LayoutTableId);
                    continue;
                }
                else if (retrievedTableNumbers.Contains(tableNumber))
                {
                    this.syncReport.AppendFormatLine("Table '{0}' is skipped: A table with that number is already found. (LayoutTableId: '{1}')", table.TableName, table.LayoutTableId);
                    continue;
                }

                retrievedTableNumbers.Add(tableNumber);
                deliverypoint.ExternalId = table.TableName;
                deliverypoint.Number = table.TableName.ToString();
                deliverypoint.Name = table.TableName;

                // Check if we can find the Group
                var room = this.layoutRoomCollection.FirstOrDefault(r => r.RoomIndex == table.SectionIndex);
                deliverypoint.ExternalPosdeliverypointgroupId = room != null ? room.LayoutRoomId.ToString() : string.Empty;

                posdeliverypoints.Add(deliverypoint);
            }

            // Reset from memory, no longer needed
            if (this.layoutRoomCollection == null)
            {
                this.layoutRoomCollection.Dispose();
                this.layoutRoomCollection = null;
            }

            return posdeliverypoints.ToArray();
        }

        /// <summary>
        /// Get the order from the point-of-serivce
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the order for</param>
        /// <returns>
        /// An Obymobi.Logic.Model.Posorder instance
        /// </returns>
        public override Model.Posorder GetPosorder(string deliverypointNumber)
        {
            // Implement when available
            return null;
        }

        /// <summary>
        /// Saves an order to the point-of-service
        /// </summary>
        /// <param name="posorder">The Obymobi.Logic.Model.Posorder instance to save</param>
        /// <returns>
        /// True if the save was succesful, False if not
        /// </returns>
        public override OrderProcessingError SaveOrder(Model.Posorder posorder)
        {
            this.WriteToLogExtended("SaveOrder", "Start");

            OrderProcessingError processingError = OrderProcessingError.None;

            // Initialize required vars
            int errorCode = 0;
            string errorText = "";
            Exception catchedException = null;
            bool success = false;
            string orderXml = string.Empty;

            // Try to send to webservice
            try
            {
                orderXml = this.PosorderToFuturePosXml(posorder);
                success = this.Webservice.ProcessOrder(orderXml, ref errorCode, ref errorText);
            }
            catch (Exception ex)
            {
                catchedException = ex;
                success = false;
            }

            // Handle result
            if (success)
            {
                this.WriteToLogExtended("SaveOrder", "Order: '{0}'  processed: '{1}'", posorder.PosorderId, errorText);
            }
            else
            {
                // Report an extensive error to the POS connector log.
                this.ReportErrorToPosLog(posorder, errorCode, errorText, catchedException, orderXml);

                if (catchedException != null)
                    this.WriteToLogNormal("SaveOrder", "Exception during save: {0}", catchedException.Message);
                else
                    this.WriteToLogNormal("SaveOrder", "Order failed: {0}, {1}", errorCode, errorText);

                throw new POSException(OrderProcessingError.PosErrorUnspecifiedFatalPosProblem, catchedException, catchedException.Message);
            }

            this.WriteToLogExtended("SaveOrder", "End: '{0}'", processingError);

            return processingError;
        }

        /// <summary>
        /// Reports the error to pos log.
        /// </summary>
        /// <param name="posorder">The posorder.</param>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorText">The error text.</param>
        /// <param name="catchedException">The catched exception.</param>
        /// <param name="orderXml">The order XML.</param>
        private void ReportErrorToPosLog(Model.Posorder posorder, int errorCode, string errorText, Exception catchedException, string orderXml)
        {
            StringBuilder errorReport = new StringBuilder();
            errorReport.AppendFormatLine(string.Empty);
            errorReport.AppendFormatLine("------ ERROR REPORT {0} -------", DateTime.Now.ToString());
            errorReport.AppendFormatLine("SaveOrder Failed: {0}, {1}", errorText, errorCode);
            if (catchedException != null)
            {
                errorReport.AppendFormatLine("Exception: {0}", catchedException.Message);
                errorReport.AppendFormatLine("Stack:\r\n {0}", catchedException.StackTrace);
                if (catchedException.InnerException != null)
                {
                    errorReport.AppendFormatLine("Inner Exception: {0}", catchedException.InnerException.Message);
                    errorReport.AppendFormatLine("Inner Stack:\r\n {0}", catchedException.InnerException.StackTrace);

                    if (catchedException.InnerException.InnerException != null)
                    {
                        errorReport.AppendFormatLine("Inner-Inner Exception: {0}", catchedException.InnerException.InnerException.Message);
                        errorReport.AppendFormatLine("Inner-Inner Stack:\r\n {0}", catchedException.InnerException.InnerException.StackTrace);
                    }
                }

                errorReport.AppendFormatLine("Order XML:\r\n");
                errorReport.AppendFormatLine(orderXml);
                errorReport.AppendFormatLine("PosOrder:\r\n");
                errorReport.AppendFormatLine(Obymobi.Logic.HelperClasses.XmlHelper.Serialize(posorder));
            }
            errorReport.AppendFormatLine(string.Empty);
            errorReport.AppendFormatLine("-------------------------", errorText, errorCode);
            errorReport.AppendFormatLine(string.Empty);

            this.WriteToPosConnectorLog("ReportErrorToPosLog", errorReport.ToString());
        }

        /// <summary>
        /// Posorders to future pos XML.
        /// </summary>
        /// <param name="posorder">The posorder.</param>
        /// <returns></returns>
        private string PosorderToFuturePosXml(Model.Posorder posorder)
        {
            StringWriter xml = new StringWriter();
            XmlTextWriter writer = new XmlTextWriter(xml);

            // Order Start
            writer.WriteStartElement("Order");

            // Order.OrderId
            writer.WriteElementString("OrderId", posorder.PosorderId.ToString());

            // Order.Device
            writer.WriteElementString("Device", "tablet");

            // Order.DeliverpointNumber
            writer.WriteElementString("DeliverypointNumber", posorder.PosdeliverypointExternalId);

            // Order.CustomerNumber
            writer.WriteElementString("CustomerNumber", posorder.PosdeliverypointExternalId);

            // Order.OrderItems Start
            writer.WriteStartElement("Orderitems");

            int orderItemId = 0;
            foreach (var orderItem in posorder.Posorderitems)
            {
                // Order.OrderItems.OrderItem Start
                writer.WriteStartElement("Orderitem");

                // Order.OrderItems.OrderItem.OrderitemId
                // According to documentation we should supply sequence
                writer.WriteElementString("OrderitemId", orderItemId.ToString());
                orderItemId++;
                //writer.WriteElementString("OrderitemId", orderItem.PosorderitemId.ToString());

                // Order.OrderItems.OrderItem.ProductId
                writer.WriteElementString("ProductId", orderItem.PosproductExternalId);

                // Order.OrderItems.OrderItem.ProductName
                writer.WriteElementString("ProductName", orderItem.Description);

                // Order.OrderItems.OrderItem.Quantity
                writer.WriteElementString("Quantity", orderItem.Quantity.ToString());

                // Order.OrderItems.Orderitem.Alterationitems Start 
                writer.WriteStartElement("AlterationItems");

                int alterationItemId = 0;
                if (orderItem.Posalterationitems != null)
                {
                    foreach (var alterationItem in orderItem.Posalterationitems)
                    {
                        // Order.OrderItems.Orderitem.Alterationitems.AlterationItem Start 
                        writer.WriteStartElement("AlterationItem");

                        // Order.OrderItems.Orderitem.Alterationitems.AlterationItem.AlterationItemId
                        writer.WriteElementString("AlterationItemId", alterationItemId.ToString());
                        alterationItemId++;
                        //writer.WriteElementString("AlterationItemId", alterationItem.PosalterationitemId.ToString());

                        // Order.OrderItems.Orderitem.Alterationitems.AlterationItem.AlterationOptionId
                        writer.WriteElementString("AlterationOptionId", alterationItem.ExternalPosalterationoptionId);

                        // Order.OrderItems.Orderitem.Alterationitems.AlterationItem End
                        writer.WriteEndElement();
                    }
                }

                // Order.OrderItems.Orderitem.Alterationitems End
                writer.WriteEndElement();

                // Order.OrderItems.OrderItem End
                writer.WriteEndElement();
            }

            // Order.OrderItems.End
            writer.WriteEndElement();

            // Payment 
            writer.WriteStartElement("Payments");
            writer.WriteEndElement();

            // Order End
            writer.WriteEndElement();

            // GK Not implemented

            return xml.ToString();
        }

        /// <summary>
        /// Prints a confirmation receipt
        /// </summary>
        /// <param name="confirmationCode"></param>
        /// <param name="name"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deliverypointNumber"></param>
        /// <returns>
        /// True if printing the confirmation receipt was succesful, False if not
        /// </returns>
        public override bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for service
        /// </summary>
        /// <param name="serviceDescription">The description of the requested service</param>
        /// <returns>
        /// True if printing the service request receipt was successful, False if not
        /// </returns>
        public override bool PrintServiceRequestReceipt(string serviceDescription)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Prints a receipt which contains a request for checkout
        /// </summary>
        /// <param name="checkoutDescription">The description of the checkout</param>
        /// <returns>
        /// True if printing the checkout request was sucessful, False if not
        /// </returns>
        public override bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            // Implement when available
            return true;
        }

        /// <summary>
        /// Notifies the locked deliverypoint.
        /// </summary>
        /// <returns>
        /// True if notifying the locked deliverypoint was successful, False if not
        /// </returns>
        public override bool NotifyLockedDeliverypoint()
        {
            // Implement when available
            return true;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Untill webservice
        /// </summary>
        public TISSLMenu Webservice
        {
            get
            {
                if (this.webservice == null)
                {
                    this.webservice = new TISSLMenu();
                    this.webservice.Url = this.apiUrl;

                    if (TestUtil.IsPcGabriel)
                    {
                        this.Webservice.Timeout = 100000;
                    }
                }
                return this.webservice;
            }
        }

        /// <summary>
        /// Has error
        /// </summary>
        protected bool hasError = false;
        /// <summary>
        /// Gets or sets the hasError
        /// </summary>
        public bool HasError
        {
            get
            {
                return this.hasError;
            }
            set
            {
                this.hasError = value;
            }
        }

        /// <summary>
        /// Error message
        /// </summary>
        protected string errorMessage = string.Empty;
        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public override string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion



    }
}
