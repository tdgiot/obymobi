﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraReports.Web;
using DevExpress.XtraReports.UI;
using System.IO;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Class for pages displaying a report
	/// </summary>
	public abstract class PageXtraReportV2 : Dionysos.Web.UI.PageQueryStringDataBinding
	{
		#region Fields

		private ReportViewer reportViewer = null;
		protected XtraReport report = null;
		private ReportToolbar reportToolbar = null;
		private bool useCaching = true;

		#endregion

		#region Methods

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			this.HookUpEvents();
			this.Load += new EventHandler(PageXtraReportV2_Load);
		}

		void HookUpEvents()
		{
			this.ReportViewer.CacheReportDocument += new CacheReportDocumentEventHandler(ReportViewer_CacheReportDocument);

			// Prevent the report from being rendered ont he first request of the page
			if (QueryStringHelper.GetValue<bool>("generate") == true)
			{
				this.ReportViewer.RestoreReportDocumentFromCache += new RestoreReportDocumentFromCacheEventHandler(ReportViewer_RestoreReportDocumentFromCache);
				this.ReportViewer.Visible = true;
				this.ReportToolbar.Visible = true;
			}
		}

		/// <summary>
		/// Initialize the report including loading and setting it's data source
		/// </summary>
		/// <returns></returns>
		public abstract XtraReport InitializeReport();

		protected void ShowReport()
		{
			this.report = this.InitializeReport();
			this.ReportViewer.Report = this.report;
		}

		/// <summary>
		/// Can be overriden 
		/// </summary>
		protected override void SetDefaultValuesToControls()
		{

		}

		#endregion

		#region Event Handlers

		void ReportViewer_RestoreReportDocumentFromCache(object sender, RestoreReportDocumentFromCacheEventArgs e)
		{
		    Stream s;
			if (this.UseCaching && CacheHelper.TryGetValue(e.Key, true, out s))
			{
				e.RestoreDocumentFromStream(s);
			}
			else
				this.ShowReport();
		}

		void ReportViewer_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
		{
			if (this.UseCaching)
			{
				e.Key = Guid.NewGuid().ToString();
				CacheHelper.AddAbsoluteExpire(true, e.Key, e.SaveDocumentToMemoryStream(), 3);
			}
		}

		void PageXtraReportV2_Load(object sender, EventArgs e)
		{
			if (QueryStringHelper.GetValue<bool>("generate") == true)
			{
				this.ShowReport();
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the reportViewer
		/// </summary>
		public ReportViewer ReportViewer
		{
			get
			{
				if (this.reportViewer == null)
				{
					for (int i = 0; i < this.ControlList.Count; i++)
					{
						ReportViewer rv = this.ControlList[i] as ReportViewer;

						if (rv != null)
						{
							this.reportViewer = rv;
							break;
						}
					}
				}
				return this.reportViewer;
			}
			set
			{
				this.reportViewer = value;
			}
		}

		/// <summary>
		/// Gets or sets the reportToolbar
		/// </summary>
		public ReportToolbar ReportToolbar
		{
			get
			{
				if (this.reportToolbar == null)
				{
					for (int i = 0; i < this.ControlList.Count; i++)
					{
						ReportToolbar rt = this.ControlList[i] as ReportToolbar;

						if (rt != null)
						{
							this.reportToolbar = rt;
							break;
						}
					}
				}
				return this.reportToolbar;
			}
			set
			{
				this.reportToolbar = value;
			}
		}

		/// <summary>
		/// Gets or sets the reportViewer
		/// </summary>
		public XtraReport Report
		{
			get
			{
				return this.report;
			}
			set
			{
				this.report = value;
			}
		}

		/// <summary>
		/// Gets or sets the useCaching
		/// </summary>
		public bool UseCaching
		{
			get
			{
				return this.useCaching;
			}
			set
			{
				this.useCaching = value;
			}
		}

		#endregion

	}
}
