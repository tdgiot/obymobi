﻿using Dionysos.Interfaces.Data;
using Dionysos.Web.UI.DevExControls;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Interface for Pages/Subpanels that can work with the GridViewEntityCollection
    /// </summary>
    public interface IGridViewEntityCollectionReady
	{
		IView SelectedView { get; set; }
		string EntityName { get; set; }
		string EntityPageUrl { get; set; }
		CollectionPageMode PageMode { get; }
		GridViewEntityCollection MainGridView { get; }
	}
}

