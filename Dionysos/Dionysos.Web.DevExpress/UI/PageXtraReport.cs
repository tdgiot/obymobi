using System;
using System.IO;
using System.Web.UI;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web;
using XtraReports = DevExpress.XtraReports;
using Dionysos.IO;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Class for Displaying XtraReports in ASP.NET Applications
	/// </summary>
	public abstract class PageXtraReport : PageReport
	{
		#region Fields

		private bool cacheToFile = false;

		#endregion

		#region Methods

		protected override void OnInitComplete(EventArgs e)
		{
			base.OnInitComplete(e);
			this.ReportViewer.CacheReportDocument += new XtraReports.Web.CacheReportDocumentEventHandler(ReportViewer_CacheReportDocument);
			this.ReportViewer.RestoreReportDocumentFromCache += new XtraReports.Web.RestoreReportDocumentFromCacheEventHandler(ReportViewer_RestoreReportDocumentFromCache);
		}


		/// <summary>
		/// Bind the Report of the page to the ReportViewer
		/// </summary>
		protected override void BindReportToViewer()
		{
			this.ReportViewer.Report = this.Report;
			this.ReportViewer.DataBind();
		}

		bool CanCreateFile()
		{
			try
			{
				using (File.Open(FullFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
			}
			catch
			{
				return false;
			}
			return true;
		}

		bool CanReadFile()
		{
			try
			{
				using (File.Open(FullFileName, FileMode.Open, FileAccess.Read, FileShare.None)) { }
			}
			catch
			{
				return false;
			}
			return true;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Cache the document to a File instead of the WebCache. For example when report is to big for the provider.
		/// </summary>
		public bool CacheToFile
		{
			get
			{
				return this.cacheToFile;
			}
			set
			{
				this.cacheToFile = value;
			}
		}

		/// <summary>
		/// Report Viewer Control on the Page
		/// </summary>
		public new XtraReports.Web.ReportViewer ReportViewer
		{
			get
			{
				if (base.ReportViewer == null)
				{
					for (int i = 0; i < this.ControlList.Count; i++)
					{
						// Get a control from the control collection
						// and check whether the control is a data grid
						Control control = this.ControlList[i];
						if (control is XtraReports.Web.ReportViewer)
						{
							return control as XtraReports.Web.ReportViewer;
						}
					}
				}

				if (base.ReportViewer == null)
					throw new Dionysos.TechnicalException("No XtraReportViewer available on XtraReportPage.");

				return base.ReportViewer as XtraReports.Web.ReportViewer;
			}
		}

		/// <summary>
		/// Report to be displayed in the ReportViewer
		/// </summary>
		public new XtraReport Report
		{
			get
			{
				return base.Report as XtraReport;
			}
			set
			{
				base.Report = value;
			}
		}

		string FullFileName
		{
			get
			{
				return this.Request.PhysicalApplicationPath + "\\" + this.FileName;
			}
		}

		string FileName
		{
			get
			{
				// GK Dangerous I would think, if you change QuerySTring parameters (or something else) it won't refresh.
				string queryString = FileUtil.SanitizeFilename(this.Request.QueryString.ToString());
				return string.Format(this.ReportName + "_{0}_{1}_{2}_{3}.prnx", DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, queryString);
			}
		}

		#endregion

		#region Event Handlers

		protected void ReportViewer_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
		{
			if (this.UseCaching && CanCreateFile())
				e.SaveDocumentToFile(FullFileName);
		}

		protected void ReportViewer_RestoreReportDocumentFromCache(object sender, RestoreReportDocumentFromCacheEventArgs e)
		{
			if (this.UseCaching && CanReadFile())
				e.RestoreDocumentFromFile(FullFileName);
			else
			{
				if (this.Report == null)
					this.CreateReport();

				this.BindReportToViewer();
			}
			foreach (string fileName in Directory.GetFiles(this.Request.PhysicalApplicationPath, this.ReportName + "*.prnx"))
			{
				try
				{
					string[] dateParts = System.IO.Path.GetFileNameWithoutExtension(fileName).Replace(this.ReportName, string.Empty).Split('_');
					DateTime date = new DateTime(int.Parse(dateParts[0]), int.Parse(dateParts[1]), int.Parse(dateParts[2]));
					if (date != DateTime.Today)
						File.Delete(fileName);
				}
				catch
				{
				}
			}
		}

		#endregion




	}
}

