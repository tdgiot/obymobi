﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Data;
using Dionysos.Data.LLBLGen;

namespace Dionysos.Web.UI
{
	public class PageLLBLOverviewDataSourceCollectionGeneric<TEntity, TEntityCollection> : PageOverviewDataSourceCollection 
        where TEntity : IEntity, new() 
        where TEntityCollection : IEntityCollection, new()
    {
        /// <summary>
        /// Extra filter applied to the datasource
        /// </summary>
        protected PredicateExpression Filter = null;

		/// <summary>
		/// Handles the PerformSelect event of the DataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="SD.LLBLGen.Pro.ORMSupportClasses.PerformSelectEventArgs" /> instance containing the event data.</param>
		private void DataSource_PerformSelect(object sender, PerformSelectEventArgs e)
		{
			e.ContainedCollection.GetMulti(e.Filter, e.MaxNumberOfItemsToReturn, e.Sorter, e.Relations, e.PrefetchPath, e.PageNumber, e.PageSize);
		}

		/// <summary>
		/// Handles the PerformGetDbCount event of the DataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="SD.LLBLGen.Pro.ORMSupportClasses.PerformGetDbCountEventArgs" /> instance containing the event data.</param>
		private void DataSource_PerformGetDbCount(object sender, PerformGetDbCountEventArgs e)
		{
			e.DbCount = e.ContainedCollection.GetDbCount(e.Filter, e.Relations);
		}

		/// <summary>
		/// Handles the PerformWork event of the DataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="PerformWorkEventArgs"/> instance containing the event data.</param>
		private void DataSource_PerformWork(object sender, PerformWorkEventArgs e)
		{
			ITransaction transaction = null;
			try
			{
				// Perform the work passed in via the PerformWorkEventArgs object. Start a new transaction with the passed in unit of work.
				transaction = LLBLGenUtil.GetTransaction(IsolationLevel.ReadCommitted, this.UniqueID);

				// Pass the transaction to the Commit routine and tell it to autocommit when the work is done.
				// If an exception is thrown, the transaction will be rolled back by the Dispose call of the using statement.
				e.Uow.Commit(transaction, true);

				LLBLGenProDataSource ds = this.DataSource as LLBLGenProDataSource;
				if (ds != null) ds.Refetch = true;
			}
			catch
			{
				if (transaction != null)
				{
					transaction.Rollback();
				}
				throw;
			}
			finally
			{
				if (transaction != null)
				{
					transaction.Dispose();
				}
			}
		}

		/// <summary>
		/// Initializes the data source.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the data source is initialized; otherwise, <c>false</c>.
		/// </returns>
		public override bool InitializeDataSource()
		{
			// Set data source id
			this.DataSourceId = this.ClientID + "_DataSource";

			// Initialize the DataSource object
			LLBLGenProDataSource ds = new LLBLGenProDataSource()
			{
				ID = this.DataSourceId,
				LivePersistence = false,
				CacheLocation = DataSourceCacheLocation.None,
				DataContainerType = DataSourceDataContainerType.EntityCollection,
				EnablePaging = true
			};

			// Retrieve the full type, assembly name of the collection to load
            IEntityCollection collection = new TEntityCollection();
			Type collectionType = collection.GetType();
			ds.EntityCollectionTypeName = String.Format("{0}, {1}", collectionType.FullName, collectionType.Assembly);
            ds.FilterToUse = new PredicateExpression();

            // Filter for archived
            IEntity entity = new TEntity();
			if (!PageDataSourceCollection.ShowArchivedRecords &&
				entity.Fields["Archived"] != null)
			{
				ds.FilterToUse = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression(this.EntityName, "Archived", false);
			}

            if (this.Filter != null)
            {
                ds.FilterToUse.Add(this.Filter);
            }

            // Set PK fieldname
            this.MainGridView.KeyFieldName = entity.PrimaryKeyFields[0].Name;

			// Set events for the data source object
			ds.PerformSelect += new EventHandler<PerformSelectEventArgs>(DataSource_PerformSelect);
			ds.PerformGetDbCount += new EventHandler<PerformGetDbCountEventArgs>(DataSource_PerformGetDbCount);
			ds.PerformWork += new EventHandler<PerformWorkEventArgs>(DataSource_PerformWork);

			// Add DataSource object and use as page DataSource
			this.Controls.Add(ds);
			this.DataSource = ds;

			return true;
		}
	}
}
