﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which detects the user's authentication state and toggles the state of a link to log in or to log out 
    /// </summary>
    public class SignOnStatus : LoginStatus
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.SignOnStatus type
        /// </summary>
        public SignOnStatus()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text used for the sign on link
        /// </summary>
        public string SignOnText
        {
            get
            {
                return this.LoginText;
            }
            set
            {
                this.LoginText = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines the action taken when a user logs out
        /// </summary>
        public LogoutAction SignOutAction
        {
            get
            {
                return this.LogoutAction;
            }
            set
            {
                this.LogoutAction = value;
            }
        }

        /// <summary>
        /// Gets or sets the text used for the logout link
        /// </summary>
        public string SignOutText
        {
            get
            {
                return this.LogoutText;
            }
            set
            {
                this.LogoutText = value;
            }
        }

        /// <summary>
        /// Gets or sets the URL of the sign out page
        /// </summary>
        public string SignOutPageUrl
        {
            get
            {
                return this.LogoutPageUrl;
            }
            set
            {
                this.LogoutPageUrl = value;
            }
        }

        #endregion
    }
}
