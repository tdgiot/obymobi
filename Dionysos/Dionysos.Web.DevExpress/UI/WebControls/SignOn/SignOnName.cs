﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Web.Security;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which displays the value of the Dionysos.Web.Security.UserManager.CurrentUser.ShortDisplayName property
    /// </summary>
    public class SignOnName : LoginName
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.SignOnName type
        /// </summary>
        public SignOnName()
        {
        }

        #endregion

        #region Methods

        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            if (this.FormatString.Length > 0)
                writer.Write(string.Format(this.FormatString, UserManager.CurrentUser.ShortDisplayName));
            else
                writer.Write(UserManager.CurrentUser.ShortDisplayName);
        }

        #endregion
    }
}
