﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.Security;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which can be used to retrieve lost passwords
    /// </summary>
    public class PasswordRetriever : UserControl
    {
        #region Fields

        private string passwordRetrievalPageRelativeUrl = "~/RetrievePassword.aspx";

        private string errorMessage = string.Empty;
        private string password = string.Empty;
        private PasswordRetrievalResult result;

        private TextBoxString tbInput = null;
        private Button btnSubmit = null;
        private LinkButton lbSubmit = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.PasswordRetriever class
        /// </summary>
        public PasswordRetriever()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the password retrieval controls
        /// </summary>
        private void SetPasswordRetrievalControls()
        {
            this.tbInput = ControlHelper.FindControlRecursively(this, "tbInput") as TextBoxString;
            this.btnSubmit = ControlHelper.FindControlRecursively(this, "btnSubmit") as Button;
            this.lbSubmit = ControlHelper.FindControlRecursively(this, "btnSubmit") as LinkButton;
        }

        /// <summary>
        /// Hookup the events to the corresponding event handlers
        /// </summary>
        private void HookupEvents()
        {
            if (!Instance.Empty(this.btnSubmit))
                this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            if (!Instance.Empty(this.lbSubmit))
                this.lbSubmit.Click += new EventHandler(btnSubmit_Click);  
        }

        /// <summary>
        /// Retrieves the password using the input specified by the user 
        /// and processes the result
        /// </summary>
        public virtual void RetrievePassword()
        {
            this.result = UserManager.Instance.GetPassword(this.tbInput.Value, out this.password);
            if (this.result == PasswordRetrievalResult.Succes)
            {
                // Send the password by mail
                if (!UserManager.Instance.SendPassword(this.tbInput.Value))
                {
                    this.errorMessage = "Er is een fout opgetreden bij het verzenden van het wachtwoord.";
                }
            }

            this.ProcessPasswordRetrieval();
            this.AfterPasswordRetrieval();
        }

        /// <summary>
        /// Processes the password retrieval result
        /// </summary>
        public virtual void ProcessPasswordRetrieval()
        {
            this.errorMessage = string.Empty;
            switch (this.result)
            {
                case PasswordRetrievalResult.Succes:
                    break;
                case PasswordRetrievalResult.LoginUnkown:
                    this.errorMessage = "Gebruikersnaam is onbekend";
                    break;
                case PasswordRetrievalResult.UserIsLockedOut:
                    this.errorMessage = "Gebruiker is geblokkeerd";
                    break;
                case PasswordRetrievalResult.UserIsNotActivated:
                    this.errorMessage = "Gebruiker is niet geactiveerd";
                    break;
                case PasswordRetrievalResult.TechnicalFailure:
                    this.errorMessage = "Technische fout, probeer later opnieuw of neem contact op.";
                    break;
                case PasswordRetrievalResult.UsernameEmpty:
                    this.errorMessage = "Er is geen gebruikersnaam ingegeven.";
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Checks whether an error message occurred and redirects to the password retrieval page using a query string
        /// </summary>
        public virtual void AfterPasswordRetrieval()
        {
            if (this.ErrorMessage.Length == 0)
                HttpContext.Current.Response.Redirect(ResolveUrl(this.passwordRetrievalPageRelativeUrl + "?infomsg=Uw wachtwoord is naar uw e-mailadres verzonden."));
            else
                HttpContext.Current.Response.Redirect(ResolveUrl(this.passwordRetrievalPageRelativeUrl + "?msg=" + this.ErrorMessage));
        }

        #endregion

        #region Event handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.SetPasswordRetrievalControls();
            this.HookupEvents();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            this.RetrievePassword();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the password retrieval error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        /// <summary>
        /// Gets or sets the relative url to the password retrieval page
        /// </summary>
        public string PasswordRetrievalPageRelativeUrl
        {
            get
            {
                return this.passwordRetrievalPageRelativeUrl;
            }
            set
            {
                this.passwordRetrievalPageRelativeUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the result of the password retrieval
        /// </summary>
        public PasswordRetrievalResult Result
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        /// <summary>
        /// Gets or sets the password
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }

        #endregion
    }
}
