﻿namespace Dionysos.Web.DevExpress.UI.DevExControls.TokenBox.Models
{
    public class Token
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Color { get; set; } = string.Empty;

        public int SortOrder { get; set; }
    }
}
