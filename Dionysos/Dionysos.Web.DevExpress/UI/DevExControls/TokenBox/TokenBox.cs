﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos.Interfaces;
using Dionysos.Web.DevExpress.UI.DevExControls.TokenBox.Models;

namespace Dionysos.Web.UI.DevExControls
{
    public class TokenBox : ASPxTokenBox, IBindable, IValidator
    {
        public TokenBox()
        {
            Properties.Width = Unit.Percentage(100);
            Properties.TextField = "Name";
            Properties.ValueField = "Id";

            Properties.AllowCustomTokens = false;
            Properties.ShowDropDownOnFocus = ShowDropDownOnFocusMode.Always;
            Properties.IncrementalFilteringMode = IncrementalFilteringMode.Contains;
            Properties.TokenRemoveButtonStyle.CssClass = "removeButton";

            base.Init += OnInit;
            base.Load += OnLoad;
            base.ItemRowPrepared += OnItemRowPrepared;
            base.TokensChanged += OnTokensChanged;
        }

        [Browsable(true)]
        public bool IsRequired { get; set; }

        [Browsable(true)]
        public bool UseDataBinding { get; set; }

        public bool IsDirty { get; protected set; }

        /// <summary>
        /// The selected tokens.
        /// </summary>
        [Browsable(true)]
        protected List<Token> SelectedTokens { get; } = new List<Token>();

        protected List<Token> SelectedHiddenTokens { get; } = new List<Token>();

        /// <summary>
        /// The full token collection selected and unselected.
        /// </summary>
        protected List<Token> TokenCollection { get; } = new List<Token>();

        string IValidator.ErrorMessage { get; set; } = string.Empty;

        bool IValidator.IsValid { get; set; } = true;

        public void SetSelectedTokens(IEnumerable<Token> tokens)
        {
            SelectedTokens.Clear();
            SelectedTokens.AddRange(tokens);
            Refresh();
        }

        public void SetReadOnlyTokens(IEnumerable<string> readOnlyTokens)
        {
            this.JSProperties["cpReadOnlyTokens"] = readOnlyTokens;
        }

        public void HideTokens(IEnumerable<Token> tokensToHide)
        {
            foreach (var tokenToHide in tokensToHide)
            {
                var token = TokenCollection.FirstOrDefault(t => t.Id == tokenToHide.Id);
                if (token != null)
                {
                    TokenCollection.Remove(token);
                }

                token = SelectedTokens.FirstOrDefault(t => t.Id == tokenToHide.Id);
                if (token != null)
                {
                    SelectedHiddenTokens.Add(token);
                    SelectedTokens.Remove(token);
                }
            }

            Refresh();
        }

        protected void Refresh()
        {
            Tokens.Clear();
            Tokens.AddRange(SelectedTokens.Select(token => token.Name));
            
            JSProperties["cpInitialTokens"] = SelectedTokens;
        }

        protected void OnLoad(object sender, EventArgs args)
        {
            if (ReadOnly || !ClientEnabled)
            {
                ControlStyle.CssClass = "disable";
                Properties.ShowDropDownOnFocus = ShowDropDownOnFocusMode.Never;
            }
            
            DataSource = TokenCollection;
            DataBindItems();
        }

        private void OnItemRowPrepared(object sender, ListBoxItemRowPreparedEventArgs args)
        {
            Token token = args.Item.GetDataItem() as Token;
            args.Row.CssClass += $"token {token?.Color}";
        }

        private void OnTokensChanged(object sender, EventArgs args)
        {
            IsDirty = true;
            SelectedTokens.Clear();
            foreach (string tokenName in Tokens)
            {
                // Existing token
                Token token = TokenCollection.FirstOrDefault(t => t.Name.Equals(tokenName, StringComparison.InvariantCultureIgnoreCase));
                
                // New token
                if (token == null)
                {
                    token = new Token
                    {
                        Name = tokenName
                    };
                }

                SelectedTokens.Add(token);
            }
            Refresh();
        }

        private void OnInit(object sender, EventArgs args)
        {
            if (IsRequired)
            {
                Page.Validators.Add(this);
            }
        }
    }
}
