﻿using System;
using Dionysos.Interfaces;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;
using System.ComponentModel;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Dionysos implementation of the ASPxTextBox
	/// </summary>
	public class TextBox : ASPxTextBox, IBindable
	{
		#region Fields
		private bool useDataBinding = true;
		private bool isRequired = false;
		#endregion

		#region Methods

		void HookUpEvents()
		{
			this.PreRender += new EventHandler(TextBox_PreRender);
			this.Init += new EventHandler(TextBox_Init);
		}

		public TextBox()
		{
			this.Width = Unit.Percentage(100);			
			this.HookUpEvents();
		}

		#endregion

		#region Event Handlers	

		void TextBox_PreRender(object sender, EventArgs e)
		{

		}

		void TextBox_Init(object sender, EventArgs e)
		{			
			// Set validation
			if (this.IsRequired)
			{
				// Set required settings				
				this.ValidationSettings.RequiredField.IsRequired = true;
				this.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
				this.ValidationSettings.ErrorText = Dionysos.ConfigurationManager.GetString(DevExConfigConstants.DefaultRequiredFieldErrorText);
				this.ValidationSettings.ErrorTextPosition = ErrorTextPosition.Right;
				this.ValidationSettings.SetFocusOnError = true;
				this.ValidationSettings.CausesValidation = true;

				// Set Css 
				this.CssClass += " required ";
			}

			
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the flag indicating whether databinding should be used
		/// </summary>
		[Browsable(true)]
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		/// <summary>
		/// Gets or sets if the control is required to have a value
		/// </summary>
		public bool IsRequired
		{
			get
			{ 
				return this.isRequired;
			}
			set
			{
				this.isRequired = value;
			}
		}

		#endregion

	}
}
