﻿using Dionysos.Interfaces;
using Dionysos.Web.DevExpress.Resources;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.DevExControls
{
    /// <summary>
    /// Customized control using ASPxDateEdit and ASPxTimeEdit.
    /// </summary>
    [ToolboxData("<{0}:DateTimeEdit runat=\"server\" />")]
	public class DateTimeEdit : CompositeControl, IBindable, IExtendedValidator
	{
		#region Fields

		/// <summary>
		/// Event fires when the value is changed (may fire twice, if date and time are changed).
		/// </summary>
		public event EventHandler ValueChanged;

		private DateEdit dateEdit;
		private TimeEdit timeEdit;

		private string friendlyName = String.Empty;

		private bool useDataBinding = true;
		private bool isValid = true;
		private string errorMessage = String.Empty;
		private bool useValidation = true;
		private string validationGroup = String.Empty;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value that indicates whether an automatic postback to the server occurs when the <see cref="DateTimeEdit"/> control loses focus.
		/// </summary>
		/// <value>
		/// <c>true</c> if an automatic postback occurs when the <see cref="DateTimeEdit"/> control loses focus; otherwise, <c>false</c>. The default is <c>false</c>.
		/// </value>
		public bool AutoPostBack
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.AutoPostBack && this.timeEdit.AutoPostBack;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.AutoPostBack = this.timeEdit.AutoPostBack = value;
			}
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		public DateTime? Value
		{
			get
			{
				this.EnsureChildControls();

				if (this.dateEdit.Value.HasValue && this.timeEdit.Value.HasValue)
				{
					return DateTimeUtil.CombineDateAndTime(this.timeEdit.Value.Value, this.dateEdit.Value.Value);
				}

                return dateEdit.Value;
            }
			set
			{
				this.EnsureChildControls();

				// Set value
				this.dateEdit.Value = this.timeEdit.Value = value;

				// Enable or disable time
				this.timeEdit.ClientEnabled = this.dateEdit.Value.HasValue;
				if (!this.timeEdit.ClientEnabled)
				{
					// Set value to empty (0:00)
					this.timeEdit.Value = new DateTime();
				}
			}
		}

        public DateTime? ValueDate
        {
            get => this.dateEdit.Value;
            set => this.dateEdit.Value = value;
        }

        public DateTime? ValueTime
        {
            get => this.timeEdit.Value;
            set => this.timeEdit.Value = value;
        }

        /// <summary>
		/// Gets the text of the date and time.
		/// </summary>
		public string Text
		{
			get
			{
				this.EnsureChildControls();

				return String.Concat(this.dateEdit.Text, " ", this.timeEdit.Text);
			}
		}

		/// <summary>
		/// Gets or sets the friendly name.
		/// </summary>
		public string FriendlyName
		{
			get
			{
				if (String.IsNullOrEmpty(this.friendlyName))
				{
					// Try to find a friendly name if not set
					string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					ITextControl textControl = this.Parent.FindControl("lbl" + strippedId) as ITextControl;
					if (textControl != null)
					{
						this.friendlyName = textControl.Text;
					}
					else
					{
						this.friendlyName = strippedId;
					}
				}

				return this.friendlyName;
			}
			set
			{
				this.friendlyName = value;
			}
		}

		/// <summary>
		/// Indicates whether null is allowed.
		/// </summary>
		public bool AllowNull
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.AllowNull;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.AllowNull = value;
			}
		}

		/// <summary>
		/// The display format used for the date.
		/// </summary>
		public string DateDisplayFormatString
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.DisplayFormatString;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.DisplayFormatString = value;
			}
		}

		/// <summary>
		/// The display format used for the time.
		/// </summary>
		public string TimeDisplayFormatString
		{
			get
			{
				this.EnsureChildControls();

				return this.timeEdit.DisplayFormatString;
			}
			set
			{
				this.EnsureChildControls();

				this.timeEdit.DisplayFormatString = value;
			}
		}

		/// <summary>
		/// The edit format used for the date.
		/// </summary>
		public string DateEditFormatString
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.EditFormatString;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.EditFormatString = value;
			}
		}

		/// <summary>
		/// The edit format used for the time.
		/// </summary>
		public string TimeEditFormatString
		{
			get
			{
				this.EnsureChildControls();

				return this.timeEdit.EditFormatString;
			}
			set
			{
				this.EnsureChildControls();

				this.timeEdit.EditFormatString = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum date allowed to be entered into the editor.
		/// </summary>
		public DateTime MaxDateTime
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.MaxDate;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.MaxDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the minumum date allowed to be entered into the editor.
		/// </summary>
		public DateTime MinDateTime
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.MinDate;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.MinDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the prompt text displayed within the editor's input box when the editor is not focused and its value is null.
		/// </summary>
		public string NullText
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.NullText;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.NullText = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether an end-user can change the editor's contents at runtime.
		/// </summary>
		public bool ReadOnly
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.ReadOnly && this.timeEdit.ReadOnly;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.ReadOnly = this.timeEdit.ReadOnly = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Web server control is enabled.
		/// </summary>
		/// <value><c>true</c> if control is enabled; otherwise, <c>false</c>. The default is <c>true</c>.</value>
		public override bool Enabled
		{
			get
			{
				this.EnsureChildControls();

				return this.dateEdit.Enabled;
			}
			set
			{
				this.EnsureChildControls();

				this.dateEdit.Enabled = value;
			}
		}

        public bool EnabledTime
        {
            get
            {
                this.EnsureChildControls();

                return this.timeEdit.Enabled;
            }
            set
            {
                this.EnsureChildControls();

                this.timeEdit.Enabled = value;
            }
        }

        public bool VisibleTime
        {
            get
            {
                this.EnsureChildControls();

                return this.timeEdit.Visible;
            }
            set
            {
                this.EnsureChildControls();

                this.timeEdit.Visible = value;
            }
        }

        /// <summary>
        /// Gets or sets whether a value is required.
        /// </summary>
        public bool IsRequired { get; set; }

		#endregion

		#region Event Handlers

		protected void dateEdit_ValueChanged(object sender, EventArgs e)
		{
			// Enable or disable time
			this.timeEdit.ClientEnabled = this.dateEdit.Value.HasValue;
			if (!this.timeEdit.ClientEnabled)
			{
				// Set value to empty (0:00)
				this.timeEdit.Value = new DateTime();
			}

			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

		protected void timeEdit_ValueChanged(object sender, EventArgs e)
		{
			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

		#endregion

		#region Methods

		protected override void RecreateChildControls()
		{
			this.EnsureChildControls();
		}

		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			string timeEditClientInstanceName = String.Format("{0}{1}teTime", this.UniqueID, this.IdSeparator);
			this.dateEdit = new DateEdit()
			{
				ID = $"dateEdit_{this.ID}",
				Width = 100,
				UseValidation = false
			};
			this.dateEdit.ValueChanged += new EventHandler(dateEdit_ValueChanged);
			this.dateEdit.ClientSideEvents.ValueChanged = String.Format("function (s, e) {{ var timeEdit = window['{0}'], enable = s.GetValue() != null; timeEdit.SetEnabled(enable); if (!enable) {{ timeEdit.SetValue(); }} }}", timeEditClientInstanceName);
			if (this.UseValidation && this.IsRequired)
			{
				this.dateEdit.CssClass += " required";
			}
			this.Controls.Add(this.dateEdit);

			this.timeEdit = new TimeEdit()
			{
                ID = $"timeEdit_{this.ID}",
				ClientInstanceName = timeEditClientInstanceName,
				Width = 66,
				UseValidation = false,
				ClientEnabled = false
			};
			this.timeEdit.ValueChanged += new EventHandler(timeEdit_ValueChanged);
			this.Controls.Add(this.timeEdit);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			this.AddAttributesToRender(writer);
			writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
			writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
			writer.RenderBeginTag(HtmlTextWriterTag.Table);

			writer.RenderBeginTag(HtmlTextWriterTag.Tr);

			writer.RenderBeginTag(HtmlTextWriterTag.Td);
			this.dateEdit.RenderControl(writer);
			writer.RenderEndTag(); // Td

			writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "4px");
			writer.RenderBeginTag(HtmlTextWriterTag.Td);
			this.timeEdit.RenderControl(writer);
			writer.RenderEndTag(); // Td

			writer.RenderEndTag(); // Tr

			writer.RenderEndTag(); // Table
		}

		#endregion

		#region IBindable Members

		/// <summary>
		/// Gets or sets whether databinding is used.
		/// </summary>
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		#endregion

		#region IExtendedValidator Members

		/// <summary>
		/// Validates this control.
		/// </summary>
		public void Validate()
		{
            if (UseValidation && Visible && Enabled && IsRequired && !Value.HasValue)
            {
                ErrorMessage = string.Format(Dionysos_Web_UI_DevExControls.ErrorMessage_Required, FriendlyName);
                IsValid = false;
            }
        }

		/// <summary>
		/// Gets or sets whether this control is valid.
		/// </summary>
		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
			}
		}

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value ?? String.Empty;
			}
		}

		/// <summary>
		/// Gets or sets whether this control is validated.
		/// </summary>
		public bool UseValidation
		{
			get
			{
				return this.useValidation;
			}
			set
			{
				this.useValidation = value;
			}
		}

		/// <summary>
		/// Gets or sets the validation group.
		/// </summary>
		public string ValidationGroup
		{
			get
			{
				return this.validationGroup;
			}
			set
			{
				this.validationGroup = value ?? String.Empty;
			}
		}

		#endregion
	}
}
