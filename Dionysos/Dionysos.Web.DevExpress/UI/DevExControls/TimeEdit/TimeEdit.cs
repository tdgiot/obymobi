﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Interfaces;
using System.Web.UI;
using System.ComponentModel;
using DevExpress.Web;
using Dionysos.Web.DevExpress.Resources;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Customized ASPxTimeEdit control.
	/// </summary>
	public class TimeEdit : ASPxTimeEdit, IBindable, IExtendedValidator
	{
		#region Fields

		private string friendlyName = String.Empty;
		private bool useDataBinding = true;
		private bool isValid = true;
		private string errorMessage = String.Empty;
		private bool useValidation = true;
		private string validationGroup = String.Empty;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		public new DateTime? Value
		{
			get
			{
				return base.Value as DateTime?;
			}
			set
			{
				base.Value = value;
			}
		}

		/// <summary>
		/// Gets or sets the friendly name.
		/// </summary>
		public string FriendlyName
		{
			get
			{
				if (String.IsNullOrEmpty(this.friendlyName))
				{
					// Try to find a friendly name if not set
					string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					ITextControl textControl = this.Parent.FindControl("lbl" + strippedId) as ITextControl;
					if (textControl != null)
					{
						this.friendlyName = textControl.Text;
					}
					else
					{
						this.friendlyName = strippedId;
					}
				}

				return this.friendlyName;
			}
			set
			{
				this.friendlyName = value;
			}
		}

		/// <summary>
		/// Gets or sets whether a value is required.
		/// </summary>
		public bool IsRequired { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TimeEdit class.
		/// </summary>
		public TimeEdit()
		{
			this.Load += new EventHandler(TimeEdit_Load);
		}

		#endregion

		#region Event Handlers

		protected void TimeEdit_Load(object sender, EventArgs e)
		{
			if (this.UseValidation)
			{
				if (this.IsRequired)
				{
					this.Page.Validators.Add(this);
					this.CssClass += " required";
				}
			}
		}

		#endregion

		#region IBindable Members

		/// <summary>
		/// Gets or sets whether databinding is used.
		/// </summary>
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		#endregion

		#region IExtendedValidator Members

		/// <summary>
		/// Validates this control.
		/// </summary>
		public new void Validate()
		{
            if (UseValidation && Visible && Enabled && IsRequired && !Value.HasValue)
            {
                ErrorMessage = string.Format(Dionysos_Web_UI_DevExControls.ErrorMessage_Required, FriendlyName);
                IsValid = false;
            }
        }

		/// <summary>
		/// Gets or sets whether this control is valid.
		/// </summary>
		public new bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
			}
		}

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		/// <summary>
		/// Gets or sets whether this control is validated.
		/// </summary>
		public bool UseValidation
		{
			get
			{
				return this.useValidation;
			}
			set
			{
				this.useValidation = value;
			}
		}

		/// <summary>
		/// Gets or sets the validation group.
		/// </summary>
		public string ValidationGroup
		{
			get
			{
				return this.validationGroup;
			}
			set
			{
				this.validationGroup = value;
			}
		}

		#endregion
	}
}
