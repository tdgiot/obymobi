﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
    public class PopupControl : ASPxPopupControl
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.DevExControls.PopupControl type
        /// </summary>
        public PopupControl()
        {
            // Set the defaults
            this.SetDefaults();
        }

        #endregion

        #region Methods

        private void SetDefaults()
        {
        }

        #endregion

        #region Event handlers

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            string javascript = string.Empty;
            javascript += "function SetPCVisible(value) {";
            javascript += "var popupControl = GetPopupControl();";
            javascript += "if(value)";
            javascript += "   popupControl.Show();";
            javascript += "else";
            javascript += "   popupControl.Hide();";
            javascript += "}";
            javascript += "function GetPopupControl() {";
            javascript += string.Format("return {0};", this.ClientInstanceName);
            javascript += "}";

            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("popupControl"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(GetType(), "popupControl", javascript, true);
            }
        }

        #endregion
    }
}
