﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Interfaces;
using Dionysos.Interfaces.Presentation;
using ASPxTabControl = DevExpress.Web.ASPxTabControl;
using System.ComponentModel;
using DevExpressTabPage = DevExpress.Web.TabPage;

namespace Dionysos.Web.UI.DevExControls
{
    public class TabPage : DevExpressTabPage, IUielement
    {
        #region Fields

        private string translationTagText = string.Empty;

        #endregion


        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.DevExControls.TabPage type
        /// </summary>
        public TabPage()
        {
            TranslationTagText = string.Empty;
        }

        /// <summary>
        /// Localize Text when the Parent page has localization
        /// </summary>
        [Category("Localization"), DefaultValue(true)]
        public bool LocalizeText
        {
            get
            {
                if (ViewState["LocalizeText"] != null)
                    return (bool)ViewState["LocalizeText"];
                else
                    return true;
            }
            set { ViewState["LocalizeText"] = value; }
        }

        public string TranslationTagText
        {

            get
            {
                if (!this.translationTagText.IsNullOrWhiteSpace())
                {
                    return this.translationTagText;
                }
                else if (this.Name == "Generic")
                    return Dionysos.Globalization.GenericTranslatables.Generic.TranslationKey;
                else
                    return string.Empty;
            }
            set
            {
                this.translationTagText = string.Empty;
            }

        }

        #endregion
    }
}
