﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;
using System.ComponentModel;
using DevExpress.Web;
using Dionysos.Globalization;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Dionysos inheritance of DevExpress.AspxPageControl (TabControl)
	/// </summary>
	public class PageControl : ASPxPageControl, System.Web.UI.IValidator, ILocalizableControl
	{
		#region Fields

		/// <summary>
		/// Name of QueryString to activate a tab.
		/// </summary>
		public const string queryStringElementToActiveTab = "tabToActivate";

		/// <summary>
		/// The validation of all tabs.
		/// </summary>
		private bool validationOfAllTabs = true;

		/// <summary>
		/// The controls added to validators on page.
		/// </summary>
		private bool controlsAddedToValidatorsOnPage = false;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the ValidationOfAllTabs, which means if the Tabs should all be validated (also non visible/non active) or only active (DevExpress behaviour).
		/// </summary>
		/// <value>
		///   <c>true</c> if all tabs are validated; otherwise, <c>false</c>.
		/// </value>
		public bool ValidationOfAllTabs
		{
			get
			{
				return this.validationOfAllTabs;
			}
			set
			{
				this.validationOfAllTabs = value;
			}
		}

		/// <summary>
		/// Gets the unique session id for this control.
		/// </summary>
		/// <value>
		/// The unique session id.
		/// </value>
		public string UniqueSessionId
		{
			get
			{
				return String.Concat(this.Page.GetType(), this.ClientID);
			}
		}

		/// <summary>
		/// Gets whether active tabs must be saved across pages.
		/// </summary>
		/// <value>
		/// <c>true</c> if the active tabs are saved across pages; otherwise, <c>false</c>.
		/// </value>
		public bool SavePageControlActiveTabs
		{
			get
			{
				return Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.SavePageControlActiveTabs);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PageControl"/> class.
		/// </summary>
		public PageControl()
		{
			// Add events
			this.Load += new EventHandler(PageControl_Load);
			this.ActiveTabChanged += new TabControlEventHandler(PageControl_ActiveTabChanged);
			this.PreRender += new EventHandler(PageControl_PreRender);
            this.Theme = DevExpressTheme.Theme;

            // GK Added this since the DevExpress boys suggested that as a work around - But I almost can't imagine this giving other issues, since it's so simple.
            // http://www.devexpress.com/support/center/Question/Details/T108883
            // Thank you for informing us. I have reproduced this issue and forwarded this ticket to our developers for further processing.
            // We will update this report once any news regarding this subject is available.
            // As a temporary solution, set the ASPxPageControl.EnableViewState property to False.
            //this.EnableViewState = false;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Raises the <see cref="E:Init" /> event.
		/// </summary>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			// Add itself to the Validators collection of the page.
			this.Page.Validators.Add(this);
		}

		/// <summary>
		/// Handles the Load event of the PageControl control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void PageControl_Load(object sender, EventArgs e)
		{
			PageLLBLGenEntity pageLLBLGenEntity = this.Page as PageLLBLGenEntity;
			PageEntity pageEntity = this.Page as PageEntity;

			if (!this.Page.IsPostBack &&
				!this.Page.IsCallback && // Not on postback or callback
				(pageEntity == null || pageEntity.PageMode != PageMode.Add)) // Not on entity add
			{
				// Only set ActiveTabIndex on first request
				int activeTabIndex;
				if ((this.SavePageControlActiveTabs || this.Request.Url.Equals(this.Request.UrlReferrer)) &&
					SessionHelper.TryGetValue<int>(this.UniqueSessionId, out activeTabIndex))
				{
					// Use saved ActiveTabIndex
					this.ActiveTabIndex = activeTabIndex;
				}
				else
				{
					// Set ActiveTabIndex to first tab
					this.ActiveTabIndex = 0;

					// Remove session value (to reduce size)
					SessionHelper.RemoveValue(this.UniqueSessionId);
				}
			}
		}

		/// <summary>
		/// Handles the ActiveTabChanged event of the PageControl control.
		/// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="e">The <see cref="TabControlEventArgs"/> instance containing the event data.</param>
		protected void PageControl_ActiveTabChanged(object source, TabControlEventArgs e)
		{
		    if (e.Tab != null)
		    {
		        // Save ActiveTabIndex to session
		        SessionHelper.SetValue(this.UniqueSessionId, e.Tab.Index);
		    }
		}

		/// <summary>
		/// Handles the PreRender event of the PageControl control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void PageControl_PreRender(object sender, EventArgs e)
		{
			// Set active tab using QueryString parameter
			string tabName;
			if (!this.Page.IsPostBack && QueryStringHelper.TryGetValue(queryStringElementToActiveTab, out tabName))
			{
				var tabPage = this.TabPages.FindByName(tabName);
				if (tabPage != null)
				{
					this.ActiveTabIndex = tabPage.Index;
				}
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds the control to a new tab page.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="systemName">The system name.</param>
		/// <param name="relativeControlPath">The relative control path.</param>
		/// <returns>
		/// The control added to the new tab page.
		/// </returns>
		public Control AddTabPage(string text, string systemName, string relativeControlPath, bool localizeTabText = true)
		{
			return this.AddTabPage(text, systemName, relativeControlPath, null, false, localizeTabText);
		}

		/// <summary>
		/// Adds the control to a new tab page.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="systemName">The system name.</param>
		/// <param name="relativeControlPath">The relative control path.</param>
		/// <param name="placeAfterControlSystemName">The system name of the tab page to place the new tab page after.</param>
		/// <returns>
		/// The control added to the new tab page.
		/// </returns>
		public Control AddTabPage(string text, string systemName, string relativeControlPath, string placeAfterControlSystemName)
		{
			return this.AddTabPage(text, systemName, relativeControlPath, placeAfterControlSystemName, false);
		}

		/// <summary>
		/// Adds the control to a new tab page.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="systemName">The system name.</param>
		/// <param name="relativeControlPath">The relative control path.</param>
		/// <param name="placeAfterControlSystemName">The system name of the tab page to place the new tab page after.</param>
		/// <param name="placeBeforeControl">If set to <c>true</c> places the control before the specified tab page.</param>
		/// <returns>
		/// The control added to the new tab page.
		/// </returns>
        public Control AddTabPage(string text, string systemName, string relativeControlPath, string placeAfterControlSystemName, bool placeBeforeControl, bool localizeTabText = true)
		{
			// Load the control
			Control controlOnTabPage = this.Page.LoadControl(relativeControlPath);
			if (controlOnTabPage is Dionysos.Web.UI.WebControls.SubPanelOnGuiEntitySimple)
			{
				((Dionysos.Web.UI.WebControls.SubPanelOnGuiEntitySimple)controlOnTabPage).Parent = this.Page as IGuiEntitySimple;
			}

			return this.AddTabPage(text, systemName, controlOnTabPage, placeAfterControlSystemName, placeBeforeControl, localizeTabText);
		}

		/// <summary>
		/// Adds the control to a new tab page.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="systemName">The system name.</param>
		/// <param name="controlOnTabPage">The control to add on the new tab page.</param>
		/// <returns>
		/// The control added to the new tab page.
		/// </returns>
		public Control AddTabPage(string text, string systemName, Control controlOnTabPage)
		{
			return this.AddTabPage(text, systemName, controlOnTabPage, null, false);
		}

		/// <summary>
		/// Adds the control to a new tab page.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="systemName">The system name.</param>
		/// <param name="controlOnTabPage">The control to add on the new tab page.</param>
		/// <param name="placeAfterControlSystemName">The system name of the tab page to place the new tab page after.</param>
		/// <returns>
		/// The control added to the new tab page.
		/// </returns>
		public Control AddTabPage(string text, string systemName, Control controlOnTabPage, string placeAfterControlSystemName)
		{
			return this.AddTabPage(text, systemName, controlOnTabPage, placeAfterControlSystemName, false);
		}

		/// <summary>
		/// Adds the control to a new tab page.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="systemName">The system name.</param>
		/// <param name="controlOnTabPage">The control to add on the new tab page.</param>
		/// <param name="placeAfterControlSystemName">The system name of the tab page to place the new tab page after (or before).</param>
		/// <param name="placeBeforeControl">If set to <c>true</c> places the control before the specified tab page.</param>
		/// <returns>
		/// The control added to the new tab page.
		/// </returns>
		private Control AddTabPage(string text, string systemName, Control controlOnTabPage, string placeAfterControlSystemName, bool placeBeforeControl, bool localizeTabText = false)
		{
			// Create tab
			TabPage tabPage = new TabPage();
			tabPage.Name = systemName;
			tabPage.Text = text;
            tabPage.LocalizeText = localizeTabText;

            // Add control to tab page
            tabPage.Controls.Add(controlOnTabPage);

            // Add tab page
            this.TabPages.Add(tabPage);

            // Change index
            // DK (2020-09-09) Disabled for now as it breaks pages after DevExpress upgrade. If this absolutely needs to work... good luck boi
            //                  A better solution is to add the tabs in your aspx/ascx file with a place holder and dynamically load the control in there
            /*if (!String.IsNullOrEmpty(placeAfterControlSystemName))
            {
                var tabToPlaceControlAfter = this.TabPages.FindByName(placeAfterControlSystemName);
                if (tabToPlaceControlAfter != null &&
                    tabToPlaceControlAfter.Index >= 0)
                {
                    if (placeBeforeControl)
                    {
                        tabPage.Index = tabToPlaceControlAfter.Index;
                    }
                    else
                    {
                        tabPage.Index = tabToPlaceControlAfter.Index + 1;
                    }
                }
            }*/

            return controlOnTabPage;
		}

		/// <summary>
		/// Add a control to the Controls collection of a Tab
		/// </summary>
		/// <param name="relativeControlPath">Path of the Control to Add</param>
		/// <param name="tabPageName">Tabname to add the controls to</param>
		/// <returns></returns>
		public Control AddToTabPage(string relativeControlPath, string tabPageName)
		{
			// Load control into tab
			Control loadedControl = this.Page.LoadControl(relativeControlPath);

			// Append control to tab
			TabPage tabPage = this.GetTabPageByName(tabPageName);
			if (tabPage == null) throw new TechnicalException("Er is geprobeerd een control op niet bestaande tabpage ('{0}') te plaatsen. (control: {1})", tabPageName, relativeControlPath);

			tabPage.Controls.Add(loadedControl);

			return loadedControl;
		}

		/// <summary>
		/// Find a tabpage
		/// </summary>
		/// <param name="tabPageName">TabPageName</param>
		/// <returns>Tabpage or null if not found</returns>
		public TabPage GetTabPageByName(string tabPageName)
		{
			TabPage tab = this.TabPages.FindByName(tabPageName) as TabPage;
			return tab;
		}

		/// <summary>
		/// Indicates whether the tab page exists.
		/// </summary>
		/// <param name="tabPageName">The name of the tab page</param>
		/// <returns>True, when a tab page was found with the input tabPageName</returns>
        public bool HasTabPage(string tabPageName)
        {
            return this.TabPages.FindByName(tabPageName) != null;
        }

	    public void ToggleAllControls(bool enabled)
	    {
	        foreach (global::DevExpress.Web.TabPage tabPage in this.TabPages)
	        {
	            foreach (ContentControl contentControl in tabPage.ContentCollection)
	            {
	                contentControl.Enabled = enabled;
	            }
	        }
	    }

		#endregion

		#region IValidator

		public string ErrorMessage
		{
			get
			{
				return String.Empty;
			}
			set
			{
				// Nothing
			}
		}

		public bool IsValid
		{
			get
			{
				return true;
			}
			set
			{
				// nothing
			}
		}

		public void Validate()
		{
			// Validate each tab & add the controls to the validation collection			
			if (ValidationOfAllTabs && !this.controlsAddedToValidatorsOnPage)
			{
				if (this.TabPages.Count > 0)
				{
                    global::DevExpress.Web.TabPage currentTabPage = this.TabPages[0];

					ControlCollection controls = new ControlCollection();
					controls.CollectControls(currentTabPage.TabControl.Controls);

					for (int j = 0; j < controls.Count; j++)
					{
						Control currentControlOnTab = controls[j];
						System.Web.UI.IValidator validator = currentControlOnTab as System.Web.UI.IValidator;
						BaseValidator basevalidator = currentControlOnTab as BaseValidator;

						if (validator != null)
						{
							this.Page.Validators.Add(validator);
							validator.Validate();
							//if (!validator.IsValid && !currentTabPage.Text.StartsWith("! "))
							//{
							//    currentTabPage.Text = "! " + currentTabPage.Text;
							//    currentTabPage.TabStyle.CssClass += " error";
							//}
						}
						else if (basevalidator != null)
						{
							this.Page.Validators.Add(basevalidator);
							basevalidator.Validate();
							//if (!basevalidator.IsValid && !currentTabPage.Text.StartsWith("! "))
							//{
							//    currentTabPage.Text = "! " + currentTabPage.Text;
							//    currentTabPage.TabStyle.CssClass += " error";
							//}
						}
					}
				}

				// Mark that we have done this, to prevent double loads
				this.controlsAddedToValidatorsOnPage = true;
			}
		}

		#endregion

		#region Localization Logic

		private bool localizeTabPageTitles = true;
		private Dictionary<string, Translatable> localizableProperties = null;

		/// <summary>
		/// Gets / Sets if the control should be localized
		/// </summary>
		[Category("Localization"), DefaultValue(true)]
		public bool LocalizeTabPageTitles
		{
			get { return this.localizeTabPageTitles; }
			set { this.localizeTabPageTitles = value; }
		}

		public Dictionary<string, Translatable> TranslatableProperties
		{
			get
			{
				if (this.localizableProperties == null)
				{
					this.localizableProperties = new Dictionary<string, Translatable>();

					// TranslationKey Prefix
					string translationKey = string.Empty;
					if (this.ID != null && this.ID.Length > 0)
						translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID;

					if (translationKey.Length == 0)
						throw new Dionysos.TechnicalException("An .ID property or TranslationTag is required for controls to be localized!, failed for control : {1}", this.ClientID);

					// TabPage Titles
					if (this.LocalizeTabPageTitles)
					{
						for (int i = 0; i < this.TabPages.Count; i++)
						{                           
							TabPage currentTabPage = this.TabPages[i] as TabPage;

                            if (!currentTabPage.LocalizeText)
                                continue;

							Translatable tabPageTitle = new Translatable();
							tabPageTitle.TranslationValue = currentTabPage.Text;

							if (currentTabPage.Name.Length == 0)
								throw new Dionysos.TechnicalException("An TabPage.Name is required for Localization of TabPages, missing for TabPage {0} on PageControl {1}", currentTabPage.Text, this.ID);

							if (currentTabPage.TranslationTagText.IsNullOrWhiteSpace())
								tabPageTitle.TranslationKey = translationKey + "." + currentTabPage.Name + ".Text";
							else
								tabPageTitle.TranslationKey = currentTabPage.TranslationTagText;

							try
							{
								this.localizableProperties.Add(currentTabPage.Name, tabPageTitle);
							}
							catch (Exception ex)
							{
								throw new Dionysos.TechnicalException(ex, "Indexing of translatable options found a double key: {0} for the tabpage", currentTabPage.Name);
							}
						}
					}
				}

				return this.localizableProperties;
			}
			set
			{
				this.localizableProperties = value;

				// Set the translated values
				foreach (KeyValuePair<string, Translatable> pair in this.localizableProperties)
				{
                    global::DevExpress.Web.TabPage tabPage = this.TabPages.FindByName(pair.Key);
					tabPage.Text = pair.Value.TranslationValue;
				}
			}
		}

		#endregion
	}
}
