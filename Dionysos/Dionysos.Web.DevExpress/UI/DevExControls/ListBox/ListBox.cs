﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
    [ToolboxData("<{0}:ListBox runat=server></{0}:ListBox>")]
    public class ListBox : ASPxListBox
    {
        #region Fields

        /// <summary>
        /// Indicates whether to display an empty item.
        /// </summary>
        private bool displayEmptyItem = true;

        /// <summary>
        /// Indicates whether to disable once selected from the query string.
        /// </summary>
        private bool disabledOnceSelectedFromQueryString = true;

        #endregion

        #region Constructors

        public ListBox()
        {
            // Set default values
            this.SetDefaults();

            // Add event handlers
            this.PreRender += new EventHandler(ListBox_PreRender);
        }

        #endregion

        #region Methods

        private void SetDefaults()
        {            
            this.EnableCheckAll = false;            
            this.CheckAllNoneText = "Check all / none";
            this.SelectionMode = ListEditSelectionMode.Multiple;
            this.Width = Unit.Percentage(100);
            this.CssClass = "listbox";
        }

        /// <summary>
        /// Gets the selected values as a list.
        /// </summary>
        /// <returns>The list of selected values.</returns>
        public List<int> SelectedItemsValuesAsIntList
        {
            get
            {
                List<int> toReturn = new List<int>();

                for (int i = 0; i < this.Items.Count; i++)
                {
                    if (this.Items[i].Selected)
                    {
                        toReturn.Add(Convert.ToInt32(this.Items[i].Value));
                    }
                }

                return toReturn;
            }
        }

        /// <summary>
        /// Mark the input values as selected.
        /// </summary>
        /// <param name="values">Values to mark as selected.</param>
        public void SelectValues(IEnumerable<int> values)
        {
            foreach (int selectedId in values)
            {
                foreach (ListEditItem listItem in this.Items)
                {
                    if (int.TryParse(listItem.Value.ToString(), out int listItemId) && listItemId == selectedId)
                    {
                        listItem.Selected = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the selected values as a list.
        /// </summary>
        /// <returns>The list of selected values.</returns>
        public List<string> SelectedValuesAsStringList
        {
            get
            {
                List<string> toReturn = new List<string>();

                for (int i = 0; i < this.Items.Count; i++)
                {
                    if (this.Items[i].Selected)
                    {
                        toReturn.Add(this.Items[i].Value.ToString());
                    }
                }

                return toReturn;
            }
        }

        /// <summary>
        /// Selects the item based on query string.
        /// </summary>
        public void SelectItemBasedOnQueryString()
        {
            // Check if the QueryString Contains a value for this DropDownlist if it's not selected yet
            string fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);

            // Changed: if (this.pageMode == PageMode.Add && this.ValidId <= 0 && QueryStringHelper.HasParameter(fieldName) && !this.Page.IsPostBack)
            // Removed: this.pageMode == PageMode.Add &&
            if ((this.Value == null || this.Value.ToString() == "0") && QueryStringHelper.HasValue(fieldName)) // GK This seemed invalid: && !this.Page.IsPostBack)
            {
                int valueToSelect = QueryStringHelper.GetValue<int>(fieldName).GetValueOrDefault();
                if (this.Items.IndexOfValue(valueToSelect) >= 0)
                {
                    //this.SelectedIndex = this.Items.IndexOfValue(valueToSelect);
                    this.Value = valueToSelect;
                    this.ToolTip = "Deze waarde is vanaf een andere pagina meegenomen.";
                    if (this.DisabledOnceSelectedAndSaved)
                    {
                        this.SetStateForDisabledOnceSelectedAndSaved();
                    }
                }
            }
        }

        /// <summary>
        /// Sets the state for disabled once selected and saved.
        /// </summary>
        protected virtual void SetStateForDisabledOnceSelectedAndSaved()
        {
            if (this.DisabledOnceSelectedAndSaved && this.Value != null)
            {
                StringBuilder trace = new StringBuilder();
                try
                {
                    trace.Append("a");

                    var item = this.Items[this.Items.IndexOfValue(this.Value)];
                    trace.Append("b");

                    this.Items.Clear();
                    trace.Append("c");

                    this.Items.Add(item);
                    trace.Append("d");

                    this.SelectedIndex = 0;
                    trace.Append("e");

                    this.Enabled = false;
                    trace.Append("f");
                }
                catch (Exception ex)
                {
                    throw new Dionysos.TechnicalException(ex, "Cache-probleem voor ComboBox {0} - Items.Count {1} - Gezochte value {2} - DateTime.UtcNow {3} - Trace {4} - Gelieve deze melding te mailen.",
                        this.ID, this.Items.Count, this.Value, DateTime.UtcNow, trace.ToString());
                }
            }
        }

        #endregion

        #region IBindable Members

        /// <summary>
        /// Gets or sets the flag which indicated whether databinding should be used
        /// </summary>
        [Browsable(true)]
        public bool UseDataBinding { get; set; }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the PreRender event of the ComboBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public virtual void ListBox_PreRender(object sender, EventArgs e)
        {         
            if (this.EnableCheckAll)
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "ListBoxToggleScript", "function listBoxToggle(e, listBox) { if(e.checked) { listBox.SelectAll(); } else { listBox.UnselectAll(); } listBox.SelectedIndexChanged.FireEvent(this, -1); }", true);
            }

            this.SetStateForDisabledOnceSelectedAndSaved();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.EnableCheckAll)
            {
                this.EnableClientSideAPI = true;
                if (this.ClientInstanceName.IsNullOrWhiteSpace()) this.ClientInstanceName = "aspxListBox" + this.ID;                                                
                
                string checkboxChecked = (this.SelectedItems.Count > 0) ? "checked=\"checked\"" : string.Empty;
                writer.Write("<span class=\"listboxToggle\"><input type=\"checkbox\" id=\"{0}_toggle\" {1} onclick='listBoxToggle(this, {0})'><label for=\"{0}_toggle\" >Check all / none</label></span>".FormatSafe(this.ClientInstanceName, checkboxChecked));         
            }
            
            base.Render(writer);
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when data bound items are ready to be cached.
        /// </summary>
        public event EventHandler ItemsDataBoundReadyToBeCached;

        /// <summary>
        /// Called when data bound items are ready to be cached.
        /// </summary>
        protected void OnItemsDataBoundReadyToBeCached()
        {
            if (this.ItemsDataBoundReadyToBeCached != null)
            {
                this.ItemsDataBoundReadyToBeCached(this, null);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the selected value string.
        /// </summary>
        public string SelectedValueString
        {
            get
            {
                return this.Value == null ? String.Empty : this.Value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display an empty item.
        /// </summary>
        /// <value>
        ///   <c>true</c> if an empty item is displayed; otherwise, <c>false</c>.
        /// </value>
        public bool DisplayEmptyItem
        {
            get
            {
                return this.displayEmptyItem;
            }
            set
            {
                this.displayEmptyItem = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to disable once selected from query string.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if disabled once selected from query string; otherwise, <c>false</c>.
        /// </value>
        public bool DisabledOnceSelectedFromQueryString
        {
            get
            {
                return this.disabledOnceSelectedFromQueryString;
            }
            set
            {
                this.disabledOnceSelectedFromQueryString = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to disable once selected and saved.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if disableded once selected and saved; otherwise, <c>false</c>.
        /// </value>
        public bool DisabledOnceSelectedAndSaved { get; set; }

        /// <summary>
        /// Gets or sets the empty item text.
        /// </summary>
        /// <value>
        /// The empty item text.
        /// </value>
        public string CheckAllNoneText { get; set;}

        /// <summary>
        /// Gets or sets the check all option.
        /// </summary>
        public bool EnableCheckAll {get;set;}

        public int ItemsCount
        {
            get
            {
                int count = 0;
                for (int i = 0; i < this.Items.Count; i++)
                {                    
                    count++;                    
                }
                return count;
            }
        }

        public int SelectedItemsCount
        {
            get
            {
                int count = 0;
                for (int i = 0; i < this.Items.Count; i++)
                {                    
                    if (this.Items[i].Selected)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        #endregion

        
    }
}
