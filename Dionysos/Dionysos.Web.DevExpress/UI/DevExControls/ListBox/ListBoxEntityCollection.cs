﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
    /// <summary>
    /// Represents an editor which displays a list of items with entity values within its listbox.
    /// </summary>
    public abstract class ListBoxEntityCollection : ListBoxInt
    {
        #region Fields

        /// <summary>
        /// The data sort field.
        /// </summary>
        private string dataSortField = String.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ListBoxEntityCollection"/> class.
        /// </summary>
        public ListBoxEntityCollection()
        { 
            this.Init += new EventHandler(ListBoxEntityCollection_Init);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the Init event of the ListBoxEntityCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ListBoxEntityCollection_Init(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.ValueField) &&
                !String.IsNullOrEmpty(this.EntityName))
            {
                this.ValueField = this.EntityName + "Id";
            }

            this.BindDataSource();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the properly sorted entity collection of the dropdownlist. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <returns>
        ///   <c>true</c> is succesfull; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool InitializeEntityCollection();

        /// <summary>
        /// Loads the ListItems from the cache, returns TRUE when could be retrieved, otherwise false
        /// </summary>
        /// <returns>
        ///   <c>true</c> is succesfull; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool LoadListItemsFromCache();

        /// <summary>
        /// Binds the collection based on the EntityName to the DropDownList. On default the DataValueField = EntityName + "Id" and DataTextField "Name" (only loaded with empty DataValueField and DataTextField properties.
        /// </summary>
        public void BindDataSource()
        {
            try
            {
                this.RetrieveTextField();
                if (!this.PreventEntityCollectionInitialization &&
                    this.LoadListItemsFromCache())
                {
                    // Nothing to do, loaded from Cache

                    // Perform logic that is normally invoked from OnDataBound:
                    this.SelectItemBasedOnQueryString();
                }
                else
                {
                    if (!this.PreventEntityCollectionInitialization)
                        this.InitializeEntityCollection();

                    if (this.EntityCollection != null)
                    {
                        this.DataSource = this.EntityCollection;
                        this.DataBind();
                    }
                    else if (this.PreventEntityCollectionInitialization)
                    {
                        // Nothing todo, manual entity collection
                    }
                    else
                    {
                        this.Items.Clear();
                        this.Items.Add(" - Databinding Failed - ");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Dionysos.TechnicalException(ex, "Problem Databinding ComboBox '{0}', TextField: '{1}', ValueField: '{2}', message: {3}", this.ID, this.TextField, this.ValueField, ex.Message);
            }
        }

        /// <summary>
        /// Retrieves the text field.
        /// </summary>
        private void RetrieveTextField()
        {
            // This retrieves the TextField (ValueField could be done in inherited class of this)
            if (base.TextField.Length == 0)
            {
                base.TextField = Dionysos.Data.EntityInformationUtil.GetEntityInformation(this.EntityName).ShowFieldName;
            }
        }

        /// <summary>
        /// Selects the item by value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if the value is selected; otherwise, <c>false</c>.
        /// </returns>
        public bool SelectByItemValue(object value)
        {
            bool isSelected = false;

            ListEditItem li = this.Items.FindByValue(value);
            if (li != null)
            {
                // Set selected item and index
                li.Selected = true;
                this.SelectedIndex = this.Items.IndexOf(li);

                isSelected = true;
            }

            return isSelected;
        }
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the entity.
        /// </summary>
        /// <value>
        /// The name of the entity.
        /// </value>
        public string EntityName { get; set; }

        /// <summary>
        /// Gets or sets the entity collection.
        /// </summary>
        /// <value>
        /// The entity collection.
        /// </value>
        public object EntityCollection { get; set; }

        /// <summary>
        /// Gets or sets the data sort field.
        /// </summary>
        /// <value>
        /// The data sort field.
        /// </value>
        public string DataSortField
        {
            get
            {
                if (!String.IsNullOrEmpty(this.dataSortField))
                {
                    return this.dataSortField;
                }
                else
                {
                    return this.TextField;
                }
            }
            set
            {
                this.dataSortField = value;
            }
        }

        /// <summary>
        /// Gets or sets the data source field that provides display texts for the editor's items.
        /// </summary>
        /// <value>
        /// A string value that specifies the name of the data source field from which the editor obtains item texts.
        /// </value>
        public new string TextField
        {
            get
            {
                if (String.IsNullOrEmpty(base.TextField))
                {
                    this.RetrieveTextField();
                }

                if (String.IsNullOrEmpty(base.TextField))
                {
                    throw new TechnicalException("De TextField property voor '{0}' is leeg. Vul hem of controleer of er wel een ShowField bestaat in de entityinformation", this.ID);
                }

                return base.TextField;
            }
            set
            {
                base.TextField = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to prevent entity collection initialization.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if entity collection initialization is prevented; otherwise, <c>false</c>.
        /// </value>
        public bool PreventEntityCollectionInitialization { get; set; }

        #endregion
    }
}
