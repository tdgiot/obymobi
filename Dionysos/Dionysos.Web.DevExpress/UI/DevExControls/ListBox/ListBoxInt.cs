﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI.DevExControls
{
    public class ListBoxInt : ListBox
    {
        #region Methods

		/// <summary>
		/// Data binds the enum using the default string value as text.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		public void DataBindEnum<T>()
			where T : struct, IComparable, IFormattable, IConvertible
		{
			this.DataBindEnum<T>(e => EnumUtil.GetStringValue(e as Enum, null), false);            
		}

		/// <summary>
		/// Data binds the enum using the default string value as text.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="orderByText">If set to <c>true</c> orders the items by text.</param>
		public void DataBindEnum<T>(bool orderByText)
			where T : struct, IComparable, IFormattable, IConvertible
		{
			this.DataBindEnum<T>(e => EnumUtil.GetStringValue(e as Enum, null), orderByText);
		}

		/// <summary>
		/// Data binds the enum using the specified string value key as text.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="stringValueKey">The string value key.</param>
		public void DataBindEnum<T>(string stringValueKey)
			where T : struct, IComparable, IFormattable, IConvertible
		{
			this.DataBindEnum<T>(e => EnumUtil.GetStringValue(e as Enum, stringValueKey), false);
		}

		/// <summary>
		/// Data binds the enum.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="stringValueKey">The string value key.</param>
		/// <param name="orderByText">If set to <c>true</c> orders the items by text.</param>
		public void DataBindEnum<T>(string stringValueKey, bool orderByText)
			where T : struct, IComparable, IFormattable, IConvertible
		{
			this.DataBindEnum<T>(e => EnumUtil.GetStringValue(e as Enum, stringValueKey), orderByText);
		}

		/// <summary>
		/// Data binds the enum.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="textSelector">The text selector.</param>
		public void DataBindEnum<T>(Func<T, string> textSelector)
			where T : struct, IComparable, IFormattable, IConvertible
		{
			this.DataBindEnum<T>(textSelector, false);
		}

		/// <summary>
		/// Data binds the enum.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="textSelector">The text selector.</param>
		/// <param name="orderByText">If set to <c>true</c> orders the items by text.</param>
		public void DataBindEnum<T>(Func<T, string> textSelector, bool orderByText)
			where T : struct, IComparable, IFormattable, IConvertible
		{
			Dictionary<int, string> dataSource = EnumUtil.ToArray<T>().ToDictionary(e => e.ToInt32(CultureInfo.InvariantCulture), textSelector);
			if (orderByText)
			{
				dataSource = dataSource.OrderBy(kv => kv.Value).ToDictionary(kv => kv.Key, kv => kv.Value);
			}

			this.DataSource = dataSource;
			this.Properties.ValueField = "Key";
			this.Properties.ValueType = typeof(int);
			this.Properties.TextField = "Value";
			this.DataBind();
		}

		/// <summary>
		/// Datas the bind enum string values as data source.
		/// </summary>
		/// <param name="enumeration">The enumeration.</param>
		// [Obsolete] - GK Removed obsolete, because it doesn't tell which method to use as the alternative.
		public void DataBindEnumStringValuesAsDataSource(Type enumeration)
		{
			string[] names = Enum.GetNames(enumeration);
			Array values = Enum.GetValues(enumeration);

			for (int i = 0; i < names.Length; i++)
			{
				string text = EnumUtil.GetStringValue((Enum)values.GetValue(i));
				int value = Convert.ToInt32(values.GetValue(i));
				this.Items.Add(text, value);
			}

			this.OnDataBound(null);
		}

		/// <summary>
		/// Datas the bind enum string values as data source.
		/// </summary>
		/// <param name="enumeration">The enumeration.</param>
        // [Obsolete] - GK Removed obsolete, because it doesn't tell which method to use as the alternative.
		public void DataBindEnumStringValuesAsDataSource(Type enumeration, int rangeFrom, int rangeTo)
		{
			string[] names = Enum.GetNames(enumeration);
			Array values = Enum.GetValues(enumeration);

			for (int i = 0; i < names.Length; i++)
			{
				string text = EnumUtil.GetStringValue((Enum)values.GetValue(i));
				int value = Convert.ToInt32(values.GetValue(i));

				if (value >= rangeFrom && value < rangeTo)
					this.Items.Add(text, value);
			}

			this.OnDataBound(null);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the editor's edited value.
		/// </summary>
		/// <value>
		/// A nullable <see cref="T:System.Int32"/> value representing the editor's value.
		/// </value>
		[Browsable(false)]
		public new int? Value
		{
			get
			{
				int value;
				if (base.Value != null && Int32.TryParse(base.Value.ToString(), out value))
				{
					return value;
				}
				else
				{
					return null;
				}
			}
			set
			{
				base.Value = value;
			}
		}

		/// <summary>
		/// Returns an int of the selected item (if ValueGreaterThanZero == true), or -1 if it can't be converted to int.
		/// </summary>
		public int ValidId
		{
			get
			{
				if (this.ValueGreaterThanZero)
				{
					return this.Value.Value;
				}
				else
				{
					return -1;
				}
			}
		}

		/// <summary>
		/// Gets a value indicating whether the value is greater than zero.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the value is greater than zero; otherwise, <c>false</c>.
		/// </value>
		public bool ValueGreaterThanZero
		{
			get
			{
				return this.Value.HasValue && this.Value.Value > 0;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ListBoxInt"/> class.
		/// </summary>
		public ListBoxInt()
		{
			this.ValueType = typeof(int);
			this.UseDataBinding = true;
		}

		#endregion
    }
}
