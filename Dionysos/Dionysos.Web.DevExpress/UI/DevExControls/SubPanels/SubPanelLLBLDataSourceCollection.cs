using System;
using System.Data;
using System.Web.UI.WebControls;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Gui class which represents a subpanel which contains a LLBLGen entity collection
	/// </summary>
	public abstract class SubPanelLLBLDataSourceCollection : SubPanelCollection
	{
        #region Properties

        /// <summary>
        /// Extra filter applied to the datasource
        /// </summary>
        protected Predicate Filter = null;

        /// <summary>
        /// Gets the foreign key field parameter.
        /// </summary>
        /// <value>
        /// The foreign key field parameter.
        /// </value>
        protected Parameter ForeignKeyFieldParameter
		{
			get
			{
				return new Parameter(this.ForeignKeyFieldName, TypeCode.Int32, this.ParentPrimaryKeyFieldValue.ToString());
			}
		}

		/// <summary>
		/// Gets or sets the URL of the entity page (if not explicitly set, uses the current request URL and entity name).
		/// </summary>
		public override string EntityPageUrl
		{
			get
			{
				if (String.IsNullOrEmpty(base.EntityPageUrl))
				{
					// Get path from entity information
					IEntityInformation entityInformation = EntityInformationUtil.GetEmptyEntityInformationInstance(this.EntityName);

					if (entityInformation != null &&
						!String.IsNullOrEmpty(entityInformation.DefaultEntityEditPage))
					{
						return entityInformation.DefaultEntityEditPage;
					}
					else
					{
						// Get path of the Request
						int lastSlash = this.Request.Path.LastIndexOf('/');
						return this.Request.Path.Substring(0, lastSlash + 1) + this.EntityName + ".aspx";
					}
				}
				else
				{
					return base.EntityPageUrl;
				}
			}
			set
			{
				base.EntityPageUrl = value;
			}
		}

		/// <summary>
		/// Gets or sets the DataSourceId
		/// </summary>
		public string DataSourceId { get; set; }

		/// <summary>
		/// Gets or sets the data source.
		/// </summary>
		public new LLBLGenProDataSource DataSource
		{
			get
			{
				return this.FindControl(this.DataSourceId) as LLBLGenProDataSource;
			}
		}

		/// <summary>
		/// Gets the parent data source.
		/// </summary>
		/// <value>
		/// The parent data source.
		/// </value>
		public new IEntity ParentDataSource
		{
			get
			{
				return (IEntity)this.Parent.DataSource;
			}
		}

		/// <summary>
		/// Gets or sets the name of the parent primary key field.
		/// </summary>
		/// <value>
		/// The name of the parent primary key field.
		/// </value>
		public override string ParentPrimaryKeyFieldName
		{
			get
			{
				if (!String.IsNullOrEmpty(base.ParentPrimaryKeyFieldName))
				{
					return base.ParentPrimaryKeyFieldName;
				}
				else
				{
					// RelatedField would be PK field on Parent
					if (this.ParentDataSource == null)
					{
						throw new TechnicalException("DataSource is (nog) null op de Parent Page");
					}
					else if (this.ParentDataSource.PrimaryKeyFields.Count != 1)
					{
						throw new TechnicalException("Het aantal PK velden op de datasource is != 1");
					}
					else
					{
						return this.ParentDataSource.PrimaryKeyFields[0].Name;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the name of the foreign key field.
		/// </summary>
		/// <value>
		/// The name of the foreign key field.
		/// </value>
		public override string ForeignKeyFieldName
		{
			get
			{
				if (!String.IsNullOrEmpty(base.ForeignKeyFieldName))
				{
					return base.ForeignKeyFieldName;
				}
				else
				{
					// Normally ForeignKeyField would be same as PK
					return this.ParentPrimaryKeyFieldName;
				}
			}
		}

		/// <summary>
		/// Gets or sets the value of the parent primary key field.
		/// </summary>
		/// <value>
		/// The value of the parent primary key field.
		/// </value>
		public new int ParentPrimaryKeyFieldValue
		{
			get
			{
				int primaryKeyFieldValue;
				if (base.ParentPrimaryKeyFieldValue != null &&
					Int32.TryParse(base.ParentPrimaryKeyFieldValue.ToString(), out primaryKeyFieldValue))
				{
					return primaryKeyFieldValue;
				}
				else
				{
					// Related Field Value would be PK value on Parent
					if (this.ParentDataSource == null)
					{
						throw new TechnicalException("DataSource is (nog) null op de Parent Page");
					}
					else if (this.ParentDataSource.PrimaryKeyFields.Count != 1)
					{
						throw new TechnicalException("Het aantal PK velden op de datasource is != 1");
					}
					else
					{
						if (this.ParentDataSource.Fields[this.ParentPrimaryKeyFieldName].CurrentValue != null &&
							Int32.TryParse(this.ParentDataSource.Fields[this.ParentPrimaryKeyFieldName].CurrentValue.ToString(), out primaryKeyFieldValue))
						{
							return primaryKeyFieldValue;
						}
						else
						{
							return -1;
						}
					}
				}
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the entity collection of the subpanel
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public override bool InitializeDataSource()
		{
			// Set data source id
			this.DataSourceId = this.ClientID + "_DataSource";

			// Initialize the data source object
			LLBLGenProDataSource ds = new LLBLGenProDataSource()
			{
				ID = this.DataSourceId,
				LivePersistence = false,
				CacheLocation = DataSourceCacheLocation.None,
				DataContainerType = DataSourceDataContainerType.EntityCollection,
				EnablePaging = true
			};

			// Retrieve the full type, assembly name of the collection to load
            IEntityCollection collection = this.GetEntityCollection();
			Type collectionType = collection.GetType();
			ds.EntityCollectionTypeName = String.Format("{0}, {1}", collectionType.FullName, collectionType.Assembly);

            ds.FilterToUse = new PredicateExpression();
            if (this.Filter != null)
            {
                ds.FilterToUse.Add(this.Filter);
            }

            // Set PK fieldname
            IEntity entity = this.GetEntity();
			this.MainGridView.KeyFieldName = entity.PrimaryKeyFields[0].Name;

			// It's a subpanel so we need to filter the data for the parent
			ds.SelectParameters.Add(this.ForeignKeyFieldParameter);

			// Set events for the data source object
			ds.PerformSelect += new EventHandler<PerformSelectEventArgs>(DataSource_PerformSelect);
			ds.PerformGetDbCount += new EventHandler<PerformGetDbCountEventArgs>(DataSource_PerformGetDbCount);
			ds.PerformWork += new EventHandler<PerformWorkEventArgs>(DataSource_PerformWork);

			// Add data source control
			this.Controls.Add(ds);

			return true;
		}

        public virtual IEntityCollection GetEntityCollection()
        {
            return DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as IEntityCollection;
        }

        public virtual IEntity GetEntity()
        {
            return DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
        }

		/// <summary>
		/// Initializes the data data bindings of the controls on the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the initialization was successful; otherwise, <c>false</c>.
		/// </returns>
		public override bool InitializeDataBindings()
		{
			this.MainGridView.DataSourceID = this.DataSourceId;

			return true;
		}

		#endregion

		#region Events

		/// <summary>
		/// Handles the PerformSelect event of the DataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="PerformSelectEventArgs" /> instance containing the event data.</param>
		private void DataSource_PerformSelect(object sender, PerformSelectEventArgs e)
		{
			e.ContainedCollection.GetMulti(e.Filter, e.MaxNumberOfItemsToReturn, e.Sorter, e.Relations, e.PrefetchPath, e.PageNumber, e.PageSize);
		}

		/// <summary>
		/// Handles the PerformGetDbCount event of the DataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="PerformGetDbCountEventArgs" /> instance containing the event data.</param>
		private void DataSource_PerformGetDbCount(object sender, PerformGetDbCountEventArgs e)
		{
			e.DbCount = e.ContainedCollection.GetDbCount(e.Filter, e.Relations);
		}

		/// <summary>
		/// Handles the PerformWork event of the DataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="PerformWorkEventArgs" /> instance containing the event data.</param>
		private void DataSource_PerformWork(object sender, PerformWorkEventArgs e)
		{
			ITransaction transaction = null;
			try
			{
				// Perform the work passed in via the PerformWorkEventArgs object. Start a new transaction with the passed in unit of work.
				transaction = LLBLGenUtil.GetTransaction(IsolationLevel.ReadCommitted, this.UniqueID);

				// Pass the transaction to the Commit routine and tell it to autocommit when the work is done.
				// If an exception is thrown, the transaction will be rolled back by the Dispose call of the using statement.
				e.Uow.Commit(transaction, true);
			}
			catch
			{
				if (transaction != null)
				{
					transaction.Rollback();
				}
				throw;
			}
			finally
			{
				if (transaction != null)
				{
					transaction.Dispose();
				}
			}
		}

		#endregion
	}
}
