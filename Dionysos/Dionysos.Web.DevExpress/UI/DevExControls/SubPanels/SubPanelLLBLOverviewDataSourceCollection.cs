﻿using System;
using System.Collections.Generic;
using Dionysos.Data;
using Dionysos.Interfaces.Data;
using Dionysos.Web.Security;

namespace Dionysos.Web.UI.DevExControls
{
    public class SubPanelLLBLOverviewDataSourceCollection : SubPanelLLBLDataSourceCollection, IGridViewEntityCollectionReady
    {
        #region Fields

        /// <summary>
        /// The selected view.
        /// </summary>
        private IView selectedView;

        /// <summary>
        /// The main grid view column selector.
        /// </summary>
        private GridViewColumnSelector mainGridViewColumnSelector;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the selected view.
        /// </summary>
        /// <value>
        /// The selected view.
        /// </value>
        public IView SelectedView
        {
            get
            {
                if (this.selectedView == null)
                {
                    var view = ViewUtil.GetView(this.GetType().BaseType.FullName, this.EntityName, UserManager.CurrentUser.UserId);

                    // Remove parent primary key/entity columns from selected view
                    string parentPrimaryKey = this.ParentPrimaryKeyFieldName;
                    view.RemoveViewItem(parentPrimaryKey);

                    string parentEntityName = this.Parent.EntityName;
                    IEntityInformation parentEntityInformation = EntityInformationUtil.GetEntityInformation(parentEntityName);
                    if (parentEntityInformation != null)
                    {
                        foreach (KeyValuePair<string, IEntityFieldInformation> parentEntityFieldInformation in parentEntityInformation.AllFields)
                        {
                            view.RemoveViewItem(parentEntityName + "." + parentEntityFieldInformation.Key);
                        }
                    }

                    this.selectedView = view;
                }

                return this.selectedView;
            }
            set
            {
                this.selectedView = value;
            }
        }

        /// <summary>
        /// Gets the page mode.
        /// </summary>
        /// <value>
        /// The page mode.
        /// </value>
        public CollectionPageMode PageMode
        {
            get
            {
                return CollectionPageMode.View;
            }
        }

        /// <summary>
        /// Gets or sets the main grid view.
        /// </summary>
        /// <value>
        /// The main grid view.
        /// </value>
        public new GridViewEntityCollection MainGridView
        {
            get
            {
                GridViewEntityCollection gridView;

                GridViewColumnSelector gridViewColumnSelector = this.MainGridViewColumnSelector;
                if (gridViewColumnSelector != null)
                {
                    gridView = gridViewColumnSelector.GridView;
                }
                else
                {
                    gridView = base.MainGridView as GridViewEntityCollection;
                }

                return gridView;
            }
        }

        /// <summary>
        /// Gets the main grid view column selector.
        /// </summary>
        /// <value>
        /// The main grid view column selector.
        /// </value>
        public virtual GridViewColumnSelector MainGridViewColumnSelector
        {
            get
            {
                return this.mainGridViewColumnSelector ?? (this.mainGridViewColumnSelector = ControlHelper.FindControlRecursively<GridViewColumnSelector>(this));
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            GridViewEntityCollection gridView = this.MainGridView;
            gridView.GenerateColumnsFromEntityInformation = true;
            gridView.ShowDeleteColumn = false;
            gridView.ShowEditColumn = false;
            gridView.ShowDeleteHyperlinkColumn = true;
            gridView.ShowEditHyperlinkColumn = true;
            //gridView.SettingsBehavior.AllowDragDrop = false;

            if (UserManager.CurrentUser != null &&
                UserManager.CurrentUser.PageSize > 0)
            {
                gridView.SettingsPager.PageSize = UserManager.CurrentUser.PageSize;
            }
            else
            {
                gridView.SettingsPager.PageSize = Dionysos.ConfigurationManager.GetInt(DionysosWebConfigurationConstants.PageSize);
            }

            GridViewColumnSelector gridViewColumnSelector = this.MainGridViewColumnSelector;
            if (gridViewColumnSelector != null)
            {
                gridViewColumnSelector.Load += new EventHandler(MainGridViewColumnSelector_Load);
            }

            base.OnInit(e);
        }

        /// <summary>
        /// Handles the Load event of the MainGridViewColumnSelector control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void MainGridViewColumnSelector_Load(object sender, EventArgs e)
        {
            GridViewColumnSelector gridViewColumnSelector = this.MainGridViewColumnSelector;

            if (gridViewColumnSelector != null)
            {
                if (ConfigurationManager.GetBool(DevExConfigConstants.AllowSubPanelCustomViews))
                {
                    // Select correct column nodes
                    gridViewColumnSelector.ColumnSelector.SelectNodesBasedOnGridView(this.MainGridViewColumnSelector.ColumnSelector.Nodes);
                }
                else
                {
                    // Hide column selector
                    gridViewColumnSelector.ShowColumnSelector = false;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an data item to the data collection of the subpanel
        /// </summary>
        /// <returns>
        /// True if adding was succesfull, False if not
        /// </returns>
        public virtual bool Add()
        {
            return this.Add(null);
        }

        /// <summary>
        /// Adds an data item to the data collection of the subpanel
        /// </summary>
        /// <param name="queryStringParameters">The query string parameters.</param>
        /// <returns>
        /// True if adding was succesfull, False if not
        /// </returns>
        /// <exception cref="EmptyException">The 'EntityPageUrl' property is not set for the SubPanelLLBLGenEntityCollection instance.</exception>
        public virtual bool Add(QueryStringHelper queryStringParameters)
        {
            // First save
            this.Page.Validate();

            if (this.Page.IsValid && this.PageAsPageLLBLGenEntity.Save())
            {
                QueryStringHelper queryString;
                if (queryStringParameters != null)
                    queryString = queryStringParameters;
                else
                    queryString = new QueryStringHelper();

                queryString.AddItem("entity", this.EntityName);
                queryString.AddItem("mode", "add");

                // Add RelationalData if possible
                if (this.PageAsPageLLBLGenEntity.DataSource != null &&
                    this.PageAsPageLLBLGenEntity.DataSource.PrimaryKeyFields.Count == 1)
                {
                    queryString.AddItem(this.ParentPrimaryKeyFieldName, this.ParentPrimaryKeyFieldValue);
                }

                if (Instance.Empty(this.EntityPageUrl))
                {
                    throw new EmptyException("The 'EntityPageUrl' property is not set for the SubPanelLLBLGenEntityCollection instance.");
                }
                else
                {
                    this.Response.Redirect(this.ResolveUrl(this.EntityPageUrl + queryString.Value), true);
                }
            }

            return true;
        }

        #endregion
    }
}
