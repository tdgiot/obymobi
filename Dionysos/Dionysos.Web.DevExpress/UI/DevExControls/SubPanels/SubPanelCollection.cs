using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Delegates.Web;
using Dionysos.Interfaces;
using Dionysos.Web.UI.WebControls;
using ASPxGridView = DevExpress.Web.ASPxGridView;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Represents a sub panel control with a grid view using the parent data source.
	/// </summary>
	public abstract class SubPanelCollection : SubPanelOnGuiEntitySimple, ISubPanelEntityCollection, IGuiEntityCollectionSimple
	{
		#region Fields

		/// <summary>
		/// The main grid view.
		/// </summary>
		private ASPxGridView mainGridView;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the entity name.
		/// </summary>
		public virtual string EntityName { get; set; }

		/// <summary>
		/// Gets or sets the data source.
		/// </summary>
		public object DataSource { get; set; }

		/// <summary>
		/// Gets or sets the parent data source.
		/// </summary>
		/// <value>
		/// The parent data source.
		/// </value>
		public object ParentDataSource { get; set; }

		/// <summary>
		/// Gets or sets the value of the parent primary key field.
		/// </summary>
		/// <value>
		/// The value of the parent primary key field.
		/// </value>
		public virtual object ParentPrimaryKeyFieldValue { get; set; }

		/// <summary>
		/// Gets or sets the name of the foreign key field.
		/// </summary>
		/// <value>
		/// The name of the foreign key field.
		/// </value>
		public virtual string ForeignKeyFieldName { get; set; }

		/// <summary>
		/// Gets or sets the name of the parent primary key field.
		/// </summary>
		/// <value>
		/// The name of the parent primary key field.
		/// </value>
		public virtual string ParentPrimaryKeyFieldName { get; set; }

		/// <summary>
		/// Gets or sets the URL of the entity page.
		/// </summary>
		public virtual string EntityPageUrl { get; set; }

		/// <summary>
		/// Gets or sets the main grid view.
		/// </summary>
		/// <value>
		/// The main grid view.
		/// </value>
		public ASPxGridView MainGridView
		{
			get
			{
				if (this.mainGridView == null)
				{
					// On first call set the ClientInstanceName
					this.mainGridView = this.GetMainGridView();
					this.mainGridView.ClientInstanceName = "gridView" + this.UniqueID;
				}

				return this.mainGridView;
			}
			set
			{
				this.mainGridView = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SubPanelCollection"/> class.
		/// </summary>
		public SubPanelCollection()
		{
			this.PreRender += new EventHandler(SubPanelCollection_PreRender);
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the PreRender event of the SubPanelCollection control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		/// <exception cref="Dionysos.TechnicalException">Parent has not not been set</exception>
		private void SubPanelCollection_PreRender(object sender, EventArgs e)
		{
			// Verify if ReadyToRockAndRoll has been called & the parent is not null
			if (this.Parent == null)
			{
				throw new TechnicalException("Parent has not not been set");
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Get the first GridView of the subpanel and use it as MainGridView
		/// </summary>
		/// <returns></returns>
		private ASPxGridView GetMainGridView()
		{
			ASPxGridView mainGridView = null;

			// Walk through the control collection of this page
			foreach (Control control in this.ControlList)
			{
				mainGridView = control as ASPxGridView;
				if (mainGridView != null) break;
			}

			return mainGridView;
		}

		/// <summary>
		/// Initializes the data source and data bindings.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the initialization was successful; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool InitializeDataSourceAndDataBindings()
		{
			// Initialize the entity collection
			bool preventLoad = false;
			this.OnDataSourceLoad(ref preventLoad);
			if (!preventLoad)
			{
				this.InitializeDataSource();
			}

			this.OnDataSourceLoaded();

			// Initialize the databindings
			this.InitializeDataBindings();

			return true;
		}

		/// <summary>
		/// Called when the parent data source is loaded.
		/// </summary>
		protected override void OnParentDataSourceLoaded()
		{
			base.OnParentDataSourceLoaded();

			// If the parent datasource is loaded we can load it too
			this.InitializeDataSourceAndDataBindings();
		}

		/// <summary>
		/// Initializes the entity collection of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the initialization was successful; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool InitializeDataSource();

		/// <summary>
		/// Initializes the data data bindings of the controls on the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the initialization was successful; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool InitializeDataBindings();

		/// <summary>
		/// Sets the cell value of a row in the grid view.
		/// </summary>
		/// <param name="row">The row to set the value for.</param>
		/// <param name="fieldName">The field name of the cell.</param>
		/// <param name="value">The value to set.</param>
        protected void SetCellValue(TableRow row, string fieldName, string value)
        {
            if (this.MainGridView.Columns[fieldName] != null)
            {
                int index = this.MainGridView.Columns[fieldName].Index;
                if (index < row.Cells.Count)
                {
                    row.Cells[index].Text = value;
                }
            }
        }

		#endregion

		#region Events

		/// <summary>
		/// Event fired before loading the entity
		/// </summary>
		public event DataSourceLoadHandler DataSourceLoad;

		/// <summary>
		/// Event fired after loading the entity
		/// </summary>
		public event DataSourceLoadedHandler DataSourceLoaded;

		/// <summary>
		/// Called when the data source loads.
		/// </summary>
		/// <param name="preventLoad">If set to <c>true</c> prevents loading the data source.</param>
		protected void OnDataSourceLoad(ref bool preventLoad)
		{
			if (this.DataSourceLoad != null)
				this.DataSourceLoad(this, ref preventLoad);
		}

		/// <summary>
		/// Called when the data source is loaded.
		/// </summary>
		protected void OnDataSourceLoaded()
		{
			if (this.DataSourceLoaded != null)
				this.DataSourceLoaded(this);
		}

		#endregion
	}
}
