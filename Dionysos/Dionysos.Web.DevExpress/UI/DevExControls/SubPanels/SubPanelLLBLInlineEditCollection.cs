using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Gui class which represents a subpanel which contains a LLBLGen entity collection
	/// </summary>
	public abstract class SubPanelLLBLInlineEditCollection : SubPanelLLBLDataSourceCollection
	{
		#region Fields

		private bool allowNew = true;

		#endregion

		#region Event Handlers

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
		/// </summary>
		/// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnLoad(EventArgs e)
		{
			this.ConfigureGridView();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Configures the grid view.
		/// </summary>
		public virtual void ConfigureGridView()
		{
			// Add command column if not already exists
			if (this.MainGridView.Columns[0].GetType() != typeof(GridViewCommandColumn))
			{
				GridViewCommandColumn commandColumn = new GridViewCommandColumn();
				commandColumn.VisibleIndex = 0;
				commandColumn.ButtonRenderMode = GridCommandButtonRenderMode.Image;
				commandColumn.Width = Unit.Pixel(75);

				// Edit
                commandColumn.ShowEditButton = true;
                this.MainGridView.SettingsCommandButton.EditButton.Image.Url = "~/images/icons/table_edit.png";

				// Delete
				commandColumn.ShowDeleteButton = true;
                this.MainGridView.SettingsCommandButton.EditButton.Image.Url = "~/images/icons/delete.png";

				// Cancel
				commandColumn.ShowCancelButton = true;
                this.MainGridView.SettingsCommandButton.EditButton.Image.Url = "~/images/icons/cross_grey.png";

				// Update
				commandColumn.ShowUpdateButton = true;
                this.MainGridView.SettingsCommandButton.EditButton.Image.Url = "~/images/icons/disk.png";

				// Add column
				this.MainGridView.Columns.Insert(0, commandColumn);
			}

			// Set default settings
			if (this.allowNew)
				this.MainGridView.Columns[0].HeaderTemplate = new AddNewRowColumnHeaderTemplate(this.MainGridView);
			else
				this.MainGridView.Columns[0].Caption = string.Empty;

			this.MainGridView.ClientSideEvents.RowDblClick = "function(s, e) { s.StartEditRow(e.visibleIndex); }";
            this.MainGridView.SettingsResizing.ColumnResizeMode = ColumnResizeMode.NextColumn;
			this.MainGridView.SettingsBehavior.ConfirmDelete = true;
			this.MainGridView.SettingsText.ConfirmDelete = "Are you sure you want to delete this line?";
			this.MainGridView.Styles.CommandColumnItem.Spacing = Unit.Pixel(5);

			// If the page is in Add mode, disable, in Edit show a new empty row
			if (((IEntity)this.Parent.DataSource).IsNew)
			{
				this.MainGridView.Enabled = false;
				this.MainGridView.ToolTip = "Sla de pagina eerst op om items toe te kunnen voegen";
			}
			else
			{
				this.MainGridView.Enabled = true;
			}

			// If the collection is emtpy we need to a a empty item to make it function
			this.MainGridView.KeyFieldName = ((IEntity)Dionysos.DataFactory.EntityFactory.GetEntity(this.EntityName)).PrimaryKeyFields[0].Name;
		}

		/// <summary>
		/// Sets the data source insert parameter.
		/// </summary>
		public virtual void SetDataSourceInsertParameter()
		{
			// Set a insert paramater which add's the related field value to newly inserted entities
			this.DataSource.InsertParameters.Add(this.ForeignKeyFieldParameter);
		}

		/// <summary>
		/// Initializes the page. This is a virtual method and can be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if initialization was succesful, False if not
		/// </returns>
		public override bool InitializeDataSourceAndDataBindings()
		{
			bool initialized = base.InitializeDataSourceAndDataBindings();

			if (initialized)
			{
				this.ConfigureGridView();
				this.SetDataSourceInsertParameter();

				// For inline views, cache to view state
				this.DataSource.CacheLocation = DataSourceCacheLocation.ViewState;
			}

			return initialized;
		}

		#endregion

		#region Properties

		public bool AllowNew
		{
			get
			{
				return this.allowNew;
			}
			set
			{
				this.allowNew = value;
			}
		}

		#endregion
	}
}
