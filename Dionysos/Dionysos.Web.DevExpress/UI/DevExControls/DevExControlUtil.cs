﻿using System;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web;
using System.Web.UI;
using Dionysos.Interfaces.Data;
using System.Collections;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Class for performing generic functions on DevEx controls
	/// </summary>
	public class DevExControlUtil
	{
		public static string GetStandardRequiredEmtpyError(string friendlyName)
		{
			return string.Format(Dionysos.ConfigurationManager.GetString(DevExConfigConstants.DefaultRequiredButEmptyError), friendlyName);
		}

		private const string ClientInitHandlerFormat = "function(s, e) {{ {0} }}";
		private const string ClientInitHandlerContent =
			@"var editor = arguments[0]; " +
			@"editor.widthCorrectionRequired = false; " +
			@"var inputElement = editor.GetInputElement(); " +
			@"inputElement.style.width = ""100%"";";
		private static readonly Regex AnonymEventHandlerRegex = new Regex(@"^\s*?(?<FUNCTION_SIGNATURE>function\s*?\([^\)]*?\))\s*?\{(?<HANDLER_CONTENT>.*)\}\s*?$",
			RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.CultureInvariant);
		private static readonly Regex EventHandlerNameRegex = new Regex(@"^(?<HANDLER_NAME>\w+)$",
			RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant);

		// Page controls processing proc
		public static void ProcessControlsOnThePage(Page page)
		{
			if (IsIE)
				ProcessControlsInTheContainer(page);
		}

		private static bool IsIE
		{
			get { return (Browser == null || string.Compare(Browser.Browser, "ie", true, CultureInfo.InvariantCulture) == 0); }
		}

		private static HttpBrowserCapabilities Browser
		{
			get { return (HttpContext.Current != null && HttpContext.Current.Request != null) ? HttpContext.Current.Request.Browser : null; }
		}

		private static void ProcessControlsInTheContainer(Control container)
		{
			foreach (Control child in container.Controls)
			{
				if (child is ASPxTextBox)
				{
					ASPxTextBox textBox = child as ASPxTextBox;
					textBox.ClientSideEvents.Init = GetUpdatedClientInitHandler(textBox.ClientSideEvents.Init);
				}
				else if (child is ASPxButtonEdit)
				{
					ASPxButtonEdit buttonEdit = child as ASPxButtonEdit;
					buttonEdit.ClientSideEvents.Init = GetUpdatedClientInitHandler(buttonEdit.ClientSideEvents.Init);
				}
				else if (child is ASPxDateEdit)
				{
					ASPxDateEdit dataEdit = child as ASPxDateEdit;
					dataEdit.ClientSideEvents.Init = GetUpdatedClientInitHandler(dataEdit.ClientSideEvents.Init);
				}
				else if (child is ASPxComboBox)
				{
					ASPxComboBox comboBox = child as ASPxComboBox;
					comboBox.ClientSideEvents.Init = GetUpdatedClientInitHandler(comboBox.ClientSideEvents.Init);
				}
				else if (child is ASPxSpinEdit)
				{
					ASPxSpinEdit spinEdit = child as ASPxSpinEdit;
					spinEdit.ClientSideEvents.Init = GetUpdatedClientInitHandler(spinEdit.ClientSideEvents.Init);
				}
				else if (child is ASPxMemo)
				{
					ASPxMemo memo = child as ASPxMemo;
					memo.ClientSideEvents.Init = GetUpdatedClientInitHandler(memo.ClientSideEvents.Init);
				}
				ProcessControlsInTheContainer(child);
			}
		}

		private static string GetUpdatedClientInitHandler(string initHandler)
		{
			if (string.IsNullOrEmpty(initHandler))
				return string.Format(ClientInitHandlerFormat, ClientInitHandlerContent);
			else
			{
				Match match;
				if ((match = EventHandlerNameRegex.Match(initHandler)).Success)
					return string.Format("function(s, e) {{ {0} {1}(s, e); }}", ClientInitHandlerContent, match.Groups["HANDLER_NAME"]);
				else if ((match = AnonymEventHandlerRegex.Match(initHandler)).Success)
					return string.Format("{0} {{ {1} {2} }}", match.Groups["FUNCTION_SIGNATURE"], ClientInitHandlerContent, match.Groups["HANDLER_CONTENT"]);
				else
					throw new ArgumentException("initHandler");
			}
		}


	}
}
