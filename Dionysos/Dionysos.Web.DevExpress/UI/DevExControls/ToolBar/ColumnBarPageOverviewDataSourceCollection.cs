﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
    /// <summary>
    /// Gui class which is being used as a ColumnBar control on a entity collection page
    /// </summary>
    public class ColumnBarPageOverviewDataSourceCollectionDesigner : System.Web.UI.Design.ControlDesigner
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ColumnBarPageOverviewDataSourceCollectionDesigner class
        /// </summary>
        public ColumnBarPageOverviewDataSourceCollectionDesigner()
            : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions
        /// </summary>
        /// <returns></returns>
        public override string GetDesignTimeHtml()
        {
            return "[ColumnBarPageOverviewDataSourceCollection]";
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [DesignerAttribute(typeof(ColumnBarPageOverviewDataSourceCollectionDesigner), typeof(IDesigner))]
    public class ColumnBarPageOverviewDataSourceCollection : CompositeControl
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ColumnBarPageOverviewDataSourceCollection class
        /// </summary>
        public ColumnBarPageOverviewDataSourceCollection()
        {
            this.CssClass = "columnbar";
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            Control parent = ControlHelper.GetParentUIElement(this);
            if (Instance.Empty(parent))
            {
                parent = this.Page;
            }

            if (parent is PageOverviewDataSourceCollection)
            {
                ColumnBarButton btnColumns = new ColumnBarButton("Kolommen...", "Columns", ResolveUrl("~/Images/Icons/expand.png"));
                btnColumns.ClientInstanceName = "btnColumns";
                btnColumns.AutoPostBack = false;
                this.Controls.Add(btnColumns);

                //CommandButton btnColumns = new CommandButton();
                //btnColumns.ID = "btnColumns";
                //btnColumns.ClientInstanceName = "btnColumns";
                //btnColumns.CssClass = "ToolbarButtonLeft";
                //btnColumns.ImagePosition = DevExpress.Web.ASPxClasses.ImagePosition.Right;
                //btnColumns.ImageUrl = ResolveUrl("~/Images/Icons/expand.gif");
                //btnColumns.CommandName = "Columns";
                //btnColumns.ToolTip = "Kolommen selecteren";
                //btnColumns.Text = "Kolommen...";
                //btnColumns.AutoPostBack = false;
                //this.Controls.Add(btnColumns);

                PopupControl popupControl = new PopupControl();
                popupControl.ClientInstanceName = "ASPxPopupClientControl";
                popupControl.Width = new Unit("260px");
                popupControl.Height = new Unit("100%");
                popupControl.ID = "pcColumns";
                popupControl.PopupElementID = btnColumns.ClientID;
                popupControl.HeaderText = "Kolommen selecteren";
                popupControl.PopupHorizontalAlign = PopupHorizontalAlign.LeftSides;
                popupControl.PopupVerticalAlign = PopupVerticalAlign.Below;
                popupControl.EnableHierarchyRecreation = true;
                popupControl.Style.Add("z-index", "99");

                Panel panel = new Panel();
                panel.ID = "pnlColumns";

                TreeListColumnSelector treelistColumnSelector = new TreeListColumnSelector();
                treelistColumnSelector.Width = new Unit("100%");

                //LiteralControl div = new LiteralControl("<div id=\"columnDiv\" style=\"position: absolute;\""

                panel.Controls.Add(treelistColumnSelector);
                popupControl.Controls.Add(panel);
                this.Controls.Add(popupControl);
            }
            else
            {
                throw new TechnicalException("A ColumnBarPageOverviewDataSourceCollection can only be placed on a page which inherits PageOverviewDataSourceCollection.");
            }
        }

        #endregion
    }
}
