﻿using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using DevExpress.Web;
using Dionysos;
using Dionysos.Globalization;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Represents the button control.
	/// </summary>
	public class ToolBarButton : CommandButton
	{
		#region Fields

		/// <summary>
		/// The keyboard shortcut.
		/// </summary>
		private string keyboardShortcut = string.Empty;

		/// <summary>
		/// The pre submit warning.
		/// </summary>
		private string preSubmitWarning = string.Empty;

		#endregion

		#region Properies

		/// <summary>
		/// Represents the alignment of the button control (by setting the CssClass)
		/// </summary>
		public enum ButtonPosition
		{
			/// <summary>
			/// Align the button to the left by setting the CssClass to "ToolbarButtonLeft"
			/// </summary>
			Left,
			/// <summary>
			/// Align the button to the right by setting the CssClass to "ToolbarButtonRight"
			/// </summary>
			Right
		}

		/// <summary>
		/// Gets or sets the KeyboardShortcut for the button (For example: Ctrl+S, Alt+S, Esc)
		/// </summary>
		/// <value>
		/// The keyboard shortcut.
		/// </value>
		public string KeyboardShortcut
		{
			get
			{
				return this.keyboardShortcut;
			}
			set
			{
				this.keyboardShortcut = value;
			}
		}

		/// <summary>
		/// Gets or sets the pre submit warning.
		/// </summary>
		/// <value>
		/// The pre submit warning.
		/// </value>
		public string PreSubmitWarning
		{
			get
			{
				return this.preSubmitWarning;
			}
			set
			{
				this.preSubmitWarning = value;

				if (!String.IsNullOrEmpty(this.preSubmitWarning))
				{
					this.ClientSideEvents.Click = String.Format("function(s, e) {{ e.processOnServer = confirm('{0}'); }}", this.preSubmitWarning.HtmlEncode());
				}
				else
				{
					this.ClientSideEvents.Click = null;
				}
			}
		}

		/// <summary>
		/// Localization information per property
		/// </summary>
		/// <exception cref="TechnicalException">A TranslationTag or ID is required for controls to be localized! Failed for control with PreSubmitWarning: '{0}' - {1}</exception>
		public override Dictionary<string, Translatable> TranslatableProperties
		{
			get
			{
				Dictionary<string, Translatable> translatableProperties = base.TranslatableProperties;

				if (this.LocalizeText &&
					!String.IsNullOrEmpty(this.PreSubmitWarning) &&
					!translatableProperties.ContainsKey("PreSubmitWarning"))
				{
					Translatable preSubmitWarningProperty = new Translatable();
					preSubmitWarningProperty.TranslationValue = this.PreSubmitWarning;

					// Get translation key
					if (!String.IsNullOrEmpty(this.TranslationTagText))
					{
						preSubmitWarningProperty.TranslationKey = this.TranslationTagText.RemoveRight(".Text") + ".PreSubmitWarning";
					}
					else if (!String.IsNullOrEmpty(this.ID))
					{
						preSubmitWarningProperty.TranslationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID + ".PreSubmitWarning";
					}
					else
					{
						throw new TechnicalException("A TranslationTag or ID is required for controls to be localized! Failed for control with PreSubmitWarning: '{0}' - {1}", this.PreSubmitWarning, this.ClientID);
					}

					translatableProperties.Add("PreSubmitWarning", preSubmitWarningProperty);
				}

				return base.TranslatableProperties;
			}
			set
			{
				base.TranslatableProperties = value;

				// Set the translated values
				if (this.TranslatableProperties != null)
				{
					Translatable preSubmitWarningProperty;
					if (this.TranslatableProperties.TryGetValue("PreSubmitWarning", out preSubmitWarningProperty))
					{
						this.PreSubmitWarning = preSubmitWarningProperty.TranslationValue;
					}
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ToolBarButton class with default settings.
		/// </summary>
		public ToolBarButton()
		{
			this.SetDefaultSettings();
		}

		/// <summary>
		/// Initializes a new instance of the ToolBarButton class with default settings.
		/// </summary>
		/// <param name="text">Sets the text displayed within the button control.</param>
		/// <param name="commandName">Sets the command name associated with the ToolBarButton control that is passed to the ToolBarButton.Command event.</param>
		public ToolBarButton(string text, string commandName)
		{
			this.SetDefaultSettings();

			this.Text = text;
			this.CommandName = commandName;
		}

		/// <summary>
		/// Initializes a new instance of the ToolBarButton class with default settings.
		/// </summary>
		/// <param name="text">Sets the text displayed within the button control.</param>
		/// <param name="commandName">Sets the command name associated with the ToolBarButton control that is passed to the ToolBarButton.Command event.</param>
		/// <param name="buttonPosition">Sets the alignment of the button control.</param>
		public ToolBarButton(string text, string commandName, ButtonPosition buttonPosition)
		{
			this.SetDefaultSettings();

			this.Text = text;
			this.CommandName = commandName;
			this.SetButtonPosition(buttonPosition);
		}

		/// <summary>
		/// Initializes a new instance of the ToolBarButton class with default settings.
		/// </summary>
		/// <param name="text">Sets the text displayed within the button control.</param>
		/// <param name="commandName">Sets the command name associated with the ToolBarButton control that is passed to the ToolBarButton.Command event.</param>
		/// <param name="imageUrl">Sets the path to the image displayed within the button control.</param>
		public ToolBarButton(string text, string commandName, string imageUrl)
		{
			this.SetDefaultSettings();

			this.Text = text;
			this.CommandName = commandName;
			this.ImageUrl = imageUrl;
		}

		/// <summary>
		/// Initializes a new instance of the ToolBarButton class with default settings.
		/// </summary>
		/// <param name="text">Sets the text displayed within the button control.</param>
		/// <param name="commandName">Sets the command name associated with the ToolBarButton control that is passed to the ToolBarButton.Command event.</param>
		/// <param name="imageUrl">Sets the path to the image displayed within the button control.</param>
		/// <param name="translationTagText">The translation tag text.</param>
		public ToolBarButton(string text, string commandName, string imageUrl, string translationTagText)
		{
			this.SetDefaultSettings();

			this.Text = text;
			this.CommandName = commandName;
			this.ImageUrl = imageUrl;
			this.TranslationTagText = translationTagText;
		}

		/// <summary>
		/// Initializes a new instance of the ToolBarButton class with default settings.
		/// </summary>
		/// <param name="text">Sets the text displayed within the button control.</param>
		/// <param name="commandName">Sets the command name associated with the ToolBarButton control that is passed to the ToolBarButton.Command event.</param>
		/// <param name="imageUrl">Sets the path to the image displayed within the button control.</param>
		/// <param name="buttonPosition">Sets the alignment of the button control.</param>
		public ToolBarButton(string text, string commandName, string imageUrl, ButtonPosition buttonPosition)
		{
			this.SetDefaultSettings();

			this.Text = text;
			this.CommandName = commandName;
			this.ImageUrl = imageUrl;
			this.SetButtonPosition(buttonPosition);
		}

		/// <summary>
		/// Initializes a new instance of the ToolBarButton class with default settings.
		/// </summary>
		/// <param name="text">Sets the text displayed within the button control.</param>
		/// <param name="commandName">Sets the command name associated with the ToolBarButton control that is passed to the ToolBarButton.Command event.</param>
		/// <param name="imageUrl">Sets the path to the image displayed within the button control.</param>
		/// <param name="keyboardShortcut">Sets the keyboard shortcut for the button, for example Ctrl+S.</param>
		/// <param name="translationTagText">The translation tag text.</param>
		public ToolBarButton(string text, string commandName, string imageUrl, string keyboardShortcut, string translationTagText)
		{
			this.SetDefaultSettings();

			this.Text = text;
			this.CommandName = commandName;
			this.ImageUrl = imageUrl;
			this.KeyboardShortcut = keyboardShortcut;
			this.TranslationTagText = translationTagText;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the default settings.
		/// </summary>
		private void SetDefaultSettings()
		{
			this.ImagePosition = ImagePosition.Left;
			this.ImageSpacing = new Unit("7px");
			this.Image.Height = new Unit("16px");
			this.Image.Width = new Unit("16px");
			this.SetButtonPosition(ButtonPosition.Left);
		}

		/// <summary>
		/// Sets the button position.
		/// </summary>
		/// <param name="bp">The button position.</param>
		private void SetButtonPosition(ButtonPosition bp)
		{
			switch (bp)
			{
				case ButtonPosition.Right:
					this.CssClass = "ToolbarButtonRight";
					break;
				case ButtonPosition.Left:
					this.CssClass = "ToolbarButtonLeft";
					break;
			}
		}

		#endregion
	}
}
