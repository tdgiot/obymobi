﻿using System.Web.UI.WebControls;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
    /// <summary>
    /// Represents the button control.
    /// </summary>
    public class ColumnBarButton : CommandButton
	{
		#region Fields

		private string keyboardShortcut = string.Empty;
		private string preSubmitWarning = string.Empty;

		#endregion

		#region Constructors

		/// <summary>
        /// Initializes a new instance of the ColumnBarButton class with default settings.
        /// </summary>
        public ColumnBarButton()
        {
            this.SetDefaultSettings();			
        }		

        /// <summary>
        /// Initializes a new instance of the ColumnBarButton class with default settings.
        /// </summary>
        /// <param name="Text">Sets the text displayed within the button control.</param>
        /// <param name="CommandName">Sets the command name associated with the ColumnBarButton control that is passed to the ColumnBarButton.Command event.</param>
        public ColumnBarButton(string Text, string CommandName)
        {
            this.SetDefaultSettings();

            this.Text = Text;
            this.CommandName = CommandName;			
        }

        /// <summary>
        /// Initializes a new instance of the ColumnBarButton class with default settings.
        /// </summary>
        /// <param name="Text">Sets the text displayed within the button control.</param>
        /// <param name="CommandName">Sets the command name associated with the ColumnBarButton control that is passed to the ColumnBarButton.Command event.</param>
        /// <param name="ButtonPosition">Sets the alignment of the button control.</param>
        public ColumnBarButton(string Text, string CommandName, ButtonPosition ButtonPosition)
        {
            this.SetDefaultSettings();			
            this.Text = Text;
            this.CommandName = CommandName;
            this.SetButtonPosition(ButtonPosition);
        }

        /// <summary>
        /// Initializes a new instance of the ColumnBarButton class with default settings.
        /// </summary>
        /// <param name="Text">Sets the text displayed within the button control.</param>
        /// <param name="CommandName">Sets the command name associated with the ColumnBarButton control that is passed to the ColumnBarButton.Command event.</param>
        /// <param name="ImageUrl">Sets the path to the image displayed within the button control.</param>
        public ColumnBarButton(string Text, string CommandName, string ImageUrl)
        {
            this.SetDefaultSettings();

            this.Text = Text;
            this.CommandName = CommandName;
            this.ImageUrl = ImageUrl;
        }

        /// <summary>
        /// Initializes a new instance of the ColumnBarButton class with default settings.
        /// </summary>
        /// <param name="Text">Sets the text displayed within the button control.</param>
        /// <param name="CommandName">Sets the command name associated with the ColumnBarButton control that is passed to the ColumnBarButton.Command event.</param>
        /// <param name="ImageUrl">Sets the path to the image displayed within the button control.</param>
        /// <param name="ButtonPosition">Sets the alignment of the button control.</param>
        public ColumnBarButton(string Text, string CommandName, string ImageUrl, ButtonPosition ButtonPosition)
        {
            this.SetDefaultSettings();

            this.Text = Text;
            this.CommandName = CommandName;
            this.SetButtonPosition(ButtonPosition);
            this.ImageUrl = ImageUrl;
        }

		/// <summary>
		/// Initializes a new instance of the ColumnBarButton class with default settings.
		/// </summary>
		/// <param name="Text">Sets the text displayed within the button control.</param>
		/// <param name="CommandName">Sets the command name associated with the ColumnBarButton control that is passed to the ColumnBarButton.Command event.</param>
		/// <param name="ImageUrl">Sets the path to the image displayed within the button control.</param>
		/// <param name="keyboardShortcut">Sets the keyboard shortcut for the button, for example Ctrl+S.</param>
		public ColumnBarButton(string Text, string CommandName, string ImageUrl, string keyboardShortcut)
		{
			this.SetDefaultSettings();
			this.KeyboardShortcut = keyboardShortcut;
			this.Text = Text;
			this.CommandName = CommandName;			
			this.ImageUrl = ImageUrl;
		}

        #endregion

        #region Properies

        /// <summary>
        /// Represents the alignment of the button control (by setting the CssClass) 
        /// </summary>
        public enum ButtonPosition
        {
            /// <summary>
            /// Align the button to the left by setting the CssClass to "ToolbarButtonLeft"
            /// </summary>
            Left,
            /// <summary>
            /// Align the button to the right by setting the CssClass to "ToolbarButtonRight"
            /// </summary>
            Right
        }		

		/// <summary>
		/// Gets or sets the KeyboardShortcut for the button (For example: Ctrl+S, Alt+S, Esc)
		/// </summary>
		public string KeyboardShortcut
		{
			get
			{
				return this.keyboardShortcut;
			}
			set
			{
				this.keyboardShortcut = value;
			}
		}	

		/// <summary>
		/// Gets or sets the preSubmitWarning
		/// </summary>
		public string PreSubmitWarning
		{
			get
			{
				return this.preSubmitWarning;
			}
			set
			{
				this.preSubmitWarning = value;
			}
		}

        #endregion

        #region Methods

        private void SetDefaultSettings()
        {
            this.Style.Add("z-index", "100000");
            this.ImagePosition = ImagePosition.Right;
            this.ImageSpacing = new Unit("7px");
            this.Image.Height = new Unit("16px");
            this.Image.Width = new Unit("16px");
            this.SetButtonPosition(ButtonPosition.Right);
			this.PreRender += new System.EventHandler(ColumnBarButton_PreRender);
        }

        private void SetButtonPosition(ButtonPosition bp)
        {
            switch (bp)
            {
                case ButtonPosition.Right:
                    this.CssClass = "ToolbarButtonRight";
                    break;
                case ButtonPosition.Left:
                    this.CssClass = "ToolbarButtonLeft";
                    break;
            }
        }

		void ColumnBarButton_PreRender(object sender, System.EventArgs e)
		{
			if (this.PreSubmitWarning != string.Empty)
			{
				this.ClientSideEvents.Click = string.Format("return confirm('{0}');", this.PreSubmitWarning);
			}
		}

        #endregion
    }
}
