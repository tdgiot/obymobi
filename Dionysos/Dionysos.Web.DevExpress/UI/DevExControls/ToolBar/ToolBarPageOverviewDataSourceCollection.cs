﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.UI.WebControls;

namespace Dionysos.Web.UI.DevExControls
{
    /// <summary>
    /// Gui class which is being used as a ToolBar control on a entity collection page
    /// </summary>
    public class ToolBarPageOverviewDataSourceCollectionDesigner : System.Web.UI.Design.ControlDesigner
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarPageOverviewDataSourceCollectionDesigner class
        /// </summary>
        public ToolBarPageOverviewDataSourceCollectionDesigner()
            : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions
        /// </summary>
        /// <returns></returns>
        public override string GetDesignTimeHtml()
        {
            return "[ToolBarPageOverviewDataSourceCollection]";
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [DesignerAttribute(typeof(ToolBarPageOverviewDataSourceCollectionDesigner), typeof(IDesigner))]
    public class ToolBarPageOverviewDataSourceCollection : CompositeControl
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarPageOverviewDataSourceCollection class
        /// </summary>
        public ToolBarPageOverviewDataSourceCollection()
        {
            this.CssClass = "toolbar";
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            Control parent = ControlHelper.GetParentUIElement(this);
            if (Instance.Empty(parent))
            {
                parent = this.Page;
            }

            if (parent is Dionysos.Interfaces.IGuiEntityCollectionSimple)
            {
                this.Controls.Add(new ToolBarButton{
										ID = "btAdd",
										TranslationTagText = "ToolBarPageOverviewDataSourceCollection.Add",
										Text = "Nieuw", 
										CommandName = "Add", 
										ImageUrl = ResolveUrl("~/Images/Icons/add.png")
									});

				if (this.PageAsPageDataSourceCollection != null)
				{
					if (this.PageAsPageDataSourceCollection.PageMode == CollectionPageMode.View)
					{
						this.Controls.Add(new ToolBarButton
						{
							ID = "btSetPageModePrint",
							TranslationTagText = "ToolBarPageOverviewDataSourceCollection.SetPageModePrint",
							Text = "Toon afdruk weergave",
							CommandName = "SetPageMode",
							CommandArgument = "print",
							ImageUrl = this.ResolveUrl("~/Images/Icons/printer.png")
						});

						if (this.PageAsPageOverviewDataSourceCollection != null)
						{
							this.Controls.Add(new ToolBarButton
							{
								ID = "btResetAutoFilter",
								TranslationTagText = "ToolBarPageOverviewDataSourceCollection.ResetAutoFilter",
								Text = "Filter(s) resetten",
								CommandName = "ResetAutoFilter",
								CommandArgument = string.Empty,
								ImageUrl = this.ResolveUrl("~/Images/Icons/filter_delete.png")
							});
						}
					}
					else
					{
						this.Controls.Add(new ToolBarButton
						{
							ID = "btSetPageModeStandard",
							TranslationTagText = "ToolBarPageOverviewDataSourceCollection.SetPageModeStandard",
							Text = "Toon standaard weergave",
							CommandName = "SetPageMode",
							CommandArgument = "view",
							ImageUrl = this.ResolveUrl("~/Images/Icons/table.png")
						});
					}
				}

				#region Old Meuk
				//ToolBarButton btnAdd = new ToolBarButton();
                //btnAdd.Text = "Nieuw";
                //btnAdd.CommandName = "Add";
                //btnAdd.ImageUrl = ResolveUrl("~/Images/Icons/add.png");
                //this.Controls.Add(btnAdd);

                //ToolBarButton btnCancel = new ToolBarButton("Annuleren", "Cancel", ResolveUrl("~/Images/Icons/cross_grey.png"));
                //this.Controls.Add(btnCancel);

                //CommandButton btnColumns = new CommandButton();
                //btnColumns.CssClass = "ToolbarButtonLeft";
                //btnColumns.ImagePosition = DevExpress.Web.ASPxClasses.ImagePosition.Right;
                //btnColumns.ImageUrl = ResolveUrl("~/Images/Icons/expand.gif");
                //btnColumns.CommandName = "Columns";
                //btnColumns.ToolTip = "Kolommen selecteren";
                //btnColumns.Text = "Kolommen...";
                //this.Controls.Add(btnColumns);

                //ColumnBarButton btnColumns = new ColumnBarButton("Kolommen...", "Columns", ResolveUrl("~/Images/Icons/expand.png"));
                //btnColumns.ClientInstanceName = "btnColumns";
                //btnColumns.AutoPostBack = false;
                //this.Controls.Add(btnColumns);

                //PopupControl popupControl = new PopupControl();
                //popupControl.ClientInstanceName = "ASPxPopupClientControl";
                //popupControl.Width = new Unit("260px");
                //popupControl.ID = "pcColumns";
                //popupControl.PopupElementID = btnColumns.ClientID;
                //popupControl.HeaderText = "Kolommen selecteren";
                //popupControl.PopupHorizontalAlign = DevExpress.Web.ASPxClasses.PopupHorizontalAlign.LeftSides;
                //popupControl.PopupVerticalAlign = DevExpress.Web.ASPxClasses.PopupVerticalAlign.Below;
                //popupControl.EnableHierarchyRecreation = true;
                //popupControl.Style.Add("z-index", "100000");

                //System.Web.UI.WebControls.Panel panel = new System.Web.UI.WebControls.Panel();
                //panel.ID = "pnlColumns";

                //TreeListColumnSelector treelistColumnSelector = new TreeListColumnSelector();
                //treelistColumnSelector.Width = new Unit("100%");

                //panel.Controls.Add(treelistColumnSelector);
                //popupControl.Controls.Add(panel);
				//this.Controls.Add(popupControl);
				#endregion
			}
            else
            {
				throw new TechnicalException("A ToolBarPageOverviewDataSourceCollection can only be placed on a page which implements IGuiEntityCollectionSimple.");
            }
        }

        #endregion

		public ToolBarButton AddButton
		{
			get
			{
				return this.FindControl("btAdd") as ToolBarButton;
			}
		}

		private PageDataSourceCollection PageAsPageDataSourceCollection
		{ 
			get
			{
				return this.Page as PageDataSourceCollection;
			}
		}

		private PageOverviewDataSourceCollection PageAsPageOverviewDataSourceCollection
		{
			get
			{
				return this.Page as PageOverviewDataSourceCollection;
			}
		}
    }
}
