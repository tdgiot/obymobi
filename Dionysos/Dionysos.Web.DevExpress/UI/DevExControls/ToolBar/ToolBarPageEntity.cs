﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Globalization;
using Dionysos.Data.LLBLGen;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI.DevExControls
{
	public class ToolBarPageEntityDesigner : System.Web.UI.Design.ControlDesigner
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ToolBarPageEntityDesigner"/> class.
		/// </summary>
		public ToolBarPageEntityDesigner()
			: base()
		{ }

		#endregion

		#region Methods

		/// <summary>
		/// Retrieves the HTML markup that is used to represent the control at design time.
		/// </summary>
		/// <returns>
		/// The HTML markup used to represent the control at design time.
		/// </returns>
		public override string GetDesignTimeHtml()
		{
			return "[ToolBarPageEntity]";
		}

		#endregion
	}

	[DesignerAttribute(typeof(ToolBarPageEntityDesigner), typeof(IDesigner))]
	public class ToolBarPageEntity : CompositeControl
	{
		#region Properties

		/// <summary>
		/// Gets the edit button.
		/// </summary>
		public ToolBarButton EditButton { get; private set; }

		/// <summary>
		/// Gets the save button.
		/// </summary>
		public ToolBarButton SaveButton { get; private set; }

		/// <summary>
		/// Gets the save and go button.
		/// </summary>
		public ToolBarButton SaveAndGoButton { get; private set; }

		/// <summary>
		/// Gets the save and new button.
		/// </summary>
		public ToolBarButton SaveAndNewButton { get; private set; }

		/// <summary>
		/// Gets the save and clone button.
		/// </summary>
		public ToolBarButton SaveAndCloneButton { get; private set; }

		/// <summary>
		/// Gets the delete button.
		/// </summary>
		public ToolBarButton DeleteButton { get; private set; }

		/// <summary>
		/// Gets the cancel button.
		/// </summary>
		public ToolBarButton CancelButton { get; private set; }

		/// <summary>
		/// Gets or sets a value indicating whether the edit, save and delete buttons must be hiddenin the edit page mode.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the edit, save and delete buttons are hidden in the edit page mode; otherwise, <c>false</c>.
		/// </value>
		public bool IsReadOnly { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ToolBarPageEntity"/> class.
		/// </summary>
		public ToolBarPageEntity()
		{
			this.CssClass = "toolbar";
			this.PreRender += new EventHandler(ToolBarPageEntity_PreRender);
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the PreRender event of the ToolBarPageEntity control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ToolBarPageEntity_PreRender(object sender, EventArgs e)
		{
            // Hide edit, save and delete buttons if read only
			if (this.IsReadOnly)
			{
				if (this.EditButton != null) this.EditButton.Visible = false;
				if (this.SaveButton != null) this.SaveButton.Visible = false;
				if (this.SaveAndGoButton != null) this.SaveAndGoButton.Visible = false;
				if (this.SaveAndNewButton != null) this.SaveAndNewButton.Visible = false;
				if (this.SaveAndCloneButton != null) this.SaveAndCloneButton.Visible = false;
				if (this.DeleteButton != null) this.DeleteButton.Visible = false;
			}

			// Hide save and clone if not ICloneable
			bool isCloneable = ((PageEntity)this.Page).DataSource is ICloneable;
			if (!isCloneable && this.SaveAndCloneButton != null) this.SaveAndCloneButton.Visible = false;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			PageEntity entityPage = this.Page as PageEntity;
			if (entityPage == null) throw new TechnicalException("A ToolBarPageEntity can only be placed on a page which inherits PageEntity.");

            string clientSideEventJs = "function(s, e){ OnToolBarClick(s,e); dirtyPageReset(s, e); }";

            switch (entityPage.PageMode)
			{
				case PageMode.View:
					this.EditButton = new ToolBarButton("Wijzigen", "Edit", "~/Images/Icons/ico_edit.gif", "Ctrl+E", "ToolBarPageEntity.Edit");
					this.Controls.Add(this.EditButton);
					break;
				case PageMode.Add:
					this.SaveButton = new ToolBarButton("Opslaan", "SaveCommand", "~/Images/Icons/disk.png", "Ctrl+S", "ToolBarPageEntity.Save");
					this.SaveButton.ID = "saveButton";
					this.SaveButton.CssClass = "ToolbarSave";
                    this.SaveButton.ClientSideEvents.Click = clientSideEventJs;
                    this.Controls.Add(this.SaveButton);

					this.SaveAndGoButton = new ToolBarButton("Opslaan en vorige", "SaveAndGo", "~/Images/Icons/disk_go.png", "Ctrl+Shift+S", "ToolBarPageEntity.SaveAndGo");
					this.SaveAndGoButton.ID = "saveAndGoButton";
					this.SaveAndGoButton.CssClass = "ToolbarSave";
                    this.SaveAndGoButton.ClientSideEvents.Click = clientSideEventJs;
                    this.Controls.Add(this.SaveAndGoButton);

					this.SaveAndNewButton = new ToolBarButton("Opslaan en nieuw", "SaveAndNew", "~/Images/Icons/disk_add.png", "Ctrl+Alt+S", "ToolBarPageEntity.SaveAndNew");
					this.SaveAndNewButton.ID = "saveAndNewButton";
					this.SaveAndNewButton.CssClass = "ToolbarSave";
                    this.SaveAndNewButton.ClientSideEvents.Click = clientSideEventJs;
                    this.Controls.Add(this.SaveAndNewButton);
					break;
				case PageMode.Edit:
				default:
					this.SaveButton = new ToolBarButton("Opslaan", "SaveCommand", "~/Images/Icons/disk.png", "Ctrl+S", "ToolBarPageEntity.Save");
					this.SaveButton.ID = "saveButton";
					this.SaveButton.CssClass = "ToolbarSave";
                    this.SaveButton.ClientSideEvents.Click = clientSideEventJs;
                    this.Controls.Add(this.SaveButton);

					this.SaveAndGoButton = new ToolBarButton("Opslaan en vorige", "SaveAndGo", "~/Images/Icons/disk_go.png", "Ctrl+Shift+S", "ToolBarPageEntity.SaveAndGo");
					this.SaveAndGoButton.ID = "saveAndGoButton";
					this.SaveAndGoButton.CssClass = "ToolbarSave";
                    this.SaveAndGoButton.ClientSideEvents.Click = clientSideEventJs;
                    this.Controls.Add(this.SaveAndGoButton);

					this.SaveAndNewButton = new ToolBarButton("Opslaan en nieuw", "SaveAndNew", "~/Images/Icons/disk_add.png", "Ctrl+Alt+S", "ToolBarPageEntity.SaveAndNew");
					this.SaveAndNewButton.ID = "saveAndNewButton";
					this.SaveAndNewButton.CssClass = "ToolbarSave";
                    this.SaveAndNewButton.ClientSideEvents.Click = clientSideEventJs;
                    this.Controls.Add(this.SaveAndNewButton);

					this.SaveAndCloneButton = new ToolBarButton("Opslaan en dupliceren", "SaveAndClone", "~/Images/Icons/disk_add.png", "Ctrl+Alt+D", "ToolBarPageEntity.SaveAndClone");
					this.SaveAndCloneButton.ID = "saveAndCloneButton";
					this.SaveAndCloneButton.CssClass = "ToolbarSave";
					this.SaveAndCloneButton.PreSubmitWarning = "Weet u zeker dat u dit item wilt opslaan en dupliceren?";
					this.Controls.Add(this.SaveAndCloneButton);

					this.DeleteButton = new ToolBarButton("Verwijderen", "Delete", "~/Images/Icons/delete.png", "ToolBarPageEntity.Delete");
					this.DeleteButton.ID = "deleteButton";
					this.DeleteButton.CssClass = "ToolbarDelete";
					this.DeleteButton.PreSubmitWarning = "Weet u zeker dat u dit item wilt verwijderen?";
					this.Controls.Add(this.DeleteButton);
					break;
			}

			this.CancelButton = new ToolBarButton("Cancel", "Cancel", "~/Images/Icons/cross_grey.png", "ToolBarPageEntity.Cancel");
			this.Controls.Add(this.CancelButton);
		}

		#endregion
	}
}
