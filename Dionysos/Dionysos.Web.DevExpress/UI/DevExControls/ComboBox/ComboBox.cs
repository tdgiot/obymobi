﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos.Globalization;
using Dionysos.Interfaces;
using Dionysos.Reflection;
using Dionysos.Web.DevExpress.Resources;

namespace Dionysos.Web.UI.DevExControls
{
    /// <summary>
    /// Represents an editor which displays a list of items within its dropdown window.
    /// </summary>
    public class ComboBox : ASPxComboBox, IBindable, System.Web.UI.IValidator, ILocalizableControl
    {
        #region Fields

        /// <summary>
        /// The page mode.
        /// </summary>
        private PageMode pageMode = PageMode.None;

        /// <summary>
        /// The friendly name.
        /// </summary>
        private string friendlyName = String.Empty;

        /// <summary>
        /// The pre-submit warning.
        /// </summary>
        private string preSubmitWarning = String.Empty;

        /// <summary>
        /// Indicates whether to display an empty item.
        /// </summary>
        private bool displayEmptyItem = true;

        /// <summary>
        /// The empty item text.
        /// </summary>
        private string emptyItemText = String.Empty;

        /// <summary>
        /// Indicates whether to disable once selected from the query string.
        /// </summary>
        private bool disabledOnceSelectedFromQueryString = true;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage = String.Empty;

        /// <summary>
        /// Indicates whether this instance is valid.
        /// </summary>
        private bool isValid = true;

        /// <summary>
        /// Indicates whether the empty item text must be localized.
        /// </summary>
        private bool localizeEmptyItemText = true;

        /// <summary>
        /// The localizeble properties.
        /// </summary>
        private Dictionary<string, Translatable> localizableProperties = null;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the translation tag.
        /// </summary>
        /// <value>
        /// The translation tag.
        /// </value>
        [Category("Localization")]
        public string TranslationTag
        {
            get
            {
                return (this.ViewState["TranslationTag"] as string) ?? String.Empty;
            }
            set
            {
                this.ViewState["TranslationTag"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to disable once selected from query string.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if disabled once selected from query string; otherwise, <c>false</c>.
        /// </value>
        public bool DisabledOnceSelectedFromQueryString
        {
            get
            {
                return this.disabledOnceSelectedFromQueryString;
            }
            set
            {
                this.disabledOnceSelectedFromQueryString = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to localize all items.
        /// </summary>
        /// <value>
        ///   <c>true</c> if all items are localized; otherwise, <c>false</c>.
        /// </value>
        [Category("Localization"), DefaultValue(false)]
        public bool LocalizeAllItems
        {
            get
            {
                return (this.ViewState["LocalizeAllItems"] as bool?) ?? false;
            }
            set
            {
                this.ViewState["LocalizeAllItems"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the page mode.
        /// </summary>
        /// <value>
        /// The page mode.
        /// </value>
        [Browsable(false)]
        public PageMode PageMode
        {
            get
            {
                if (this.pageMode == PageMode.None)
                {
                    // Set the page mode for this control
                    if (Member.HasMember(Page, "PageMode"))
                    {
                        PageMode pageMode = (PageMode)Member.InvokeProperty(Page, "PageMode");
                        if (Instance.Empty(pageMode))
                        {
                            throw new EmptyException("Variable 'pageMode' is empty.");
                        }
                        else
                        {
                            this.pageMode = pageMode;
                        }
                    }
                }

                return this.pageMode;
            }
            set
            {
                this.pageMode = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a value is required.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this a value is required; otherwise, <c>false</c>.
        /// </value>
        [Browsable(true)]
        public bool IsRequired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to disable once selected and saved.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if disableded once selected and saved; otherwise, <c>false</c>.
        /// </value>
        public bool DisabledOnceSelectedAndSaved { get; set; }

        /// <summary>
        /// Gets or sets the friendly name.
        /// </summary>
        /// <value>
        /// The friendly name.
        /// </value>
        [Browsable(false)]
        public string FriendlyName
        {
            get
            {
                if (Instance.Empty(this.friendlyName))
                {
                    // Try to find a friendly name if not available, via a corresponding lbl
                    string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
                    Label textLabel = this.Parent.FindControl("lbl" + strippedId) as Label;
                    if (textLabel != null)
                    {
                        this.friendlyName = textLabel.Text;
                    }
                    else
                    {
                        this.friendlyName = strippedId;
                    }
                }
                return this.friendlyName;
            }
            set
            {
                this.friendlyName = value;
            }
        }

        /// <summary>
        /// Gets the selected value string.
        /// </summary>
        public string SelectedValueString
        {
            get
            {
                return this.Value == null ? String.Empty : this.Value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display an empty item.
        /// </summary>
        /// <value>
        ///   <c>true</c> if an empty item is displayed; otherwise, <c>false</c>.
        /// </value>
        public bool DisplayEmptyItem
        {
            get
            {
                return this.displayEmptyItem;
            }
            set
            {
                this.displayEmptyItem = value;
            }
        }

        /// <summary>
        /// Gets or sets the empty item text.
        /// </summary>
        /// <value>
        /// The empty item text.
        /// </value>
        public string EmptyItemText
        {
            get
            {
                return this.emptyItemText;
            }
            set
            {
                this.emptyItemText = value;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboBox"/> class.
        /// </summary>
        public ComboBox()
        {
            // Set default values
            this.Width = Unit.Percentage(100);
            this.IncrementalFilteringMode = IncrementalFilteringMode.Contains;
            this.Native = false;

            // Set values from configuration
            if (GlobalLight.ConfigurationProvider != null)
            {
                this.EnableCallbackMode = ConfigurationManager.GetBool(DevExConfigConstants.ComboBoxEnableCallbackMode);
                this.CallbackPageSize = ConfigurationManager.GetInt(DevExConfigConstants.ComboBoxCallbackPageSize);
            }

            this.IncrementalFilteringDelay = 1000;

            // Add event handlers
            this.Init += new EventHandler(ComboBox_Init);
            this.PreRender += new EventHandler(ComboBox_PreRender);

            // Add client side events
            this.ClientSideEvents.GotFocus = "function(sender, e) { sender.SelectAll(); }";
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the Init event of the ComboBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ComboBox_Init(object sender, EventArgs e)
        {
            // Set validation
            if (this.IsRequired)
            {
                this.Page.Validators.Add(this);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:DataBound"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnDataBound(EventArgs e)
        {
            // Add empty item (Check that it has not been added before, because the ComboBoxLLBLEntityCollection add's it it self because of caching stuff
            if (this.DisplayEmptyItem)
            {
                this.Items.Insert(0, new ListEditItem(this.EmptyItemText, null));
                this.Value = null;
            }

            // Notify that the items are now "raw" ready to be cached
            // Raw = Including Empty item & Before the items get stripped by the method below.
            this.OnItemsDataBoundReadyToBeCached();

            // Select item from query string
            this.SelectItemBasedOnQueryString();

            base.OnDataBound(e);
        }

        /// <summary>
        /// Handles the PreRender event of the ComboBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public virtual void ComboBox_PreRender(object sender, EventArgs e)
        {
            // GK Fall back when a control was not databound
            if (this.DisplayEmptyItem && !this.EmptyItemText.IsNullOrWhiteSpace())
            {
                if (this.Items.Count == 0 || this.Items[0].Value != null)
                {
                    // Insert an empty item
                    this.Items.Insert(0, new ListEditItem(this.EmptyItemText, null));
                }

                if (this.Items.Count >= 1 &&
                    this.Items[0].Value == null &&
                    this.Items[0].Text != this.EmptyItemText)
                {
                    this.Items[0].Text = this.EmptyItemText;
                }
            }

            this.SetStateForDisabledOnceSelectedAndSaved();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Selects the item based on query string.
        /// </summary>
        public void SelectItemBasedOnQueryString()
        {
            // Check if the QueryString Contains a value for this DropDownlist if it's not selected yet
            string fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);

            // Changed: if (this.pageMode == PageMode.Add && this.ValidId <= 0 && QueryStringHelper.HasParameter(fieldName) && !this.Page.IsPostBack)
            // Removed: this.pageMode == PageMode.Add &&
            if ((this.Value == null || this.Value.ToString() == "0" || this.Value.Equals(this.EmptyItemText)) && QueryStringHelper.HasValue(fieldName)) // GK This seemed invalid: && !this.Page.IsPostBack)
            {
                int valueToSelect = QueryStringHelper.GetValue<int>(fieldName).GetValueOrDefault();
                if (this.Items.IndexOfValue(valueToSelect) >= 0)
                {
                    //this.SelectedIndex = this.Items.IndexOfValue(valueToSelect);
                    this.Value = valueToSelect;
                    this.ToolTip = "Deze waarde is vanaf een andere pagina meegenomen.";
                    if (this.DisabledOnceSelectedAndSaved)
                    {
                        this.SetStateForDisabledOnceSelectedAndSaved();
                    }
                }
            }
        }

        /// <summary>
        /// Sets the state for disabled once selected and saved.
        /// </summary>
        protected virtual void SetStateForDisabledOnceSelectedAndSaved()
        {
            if (this.DisabledOnceSelectedAndSaved && this.Value != null)
            {
                StringBuilder trace = new StringBuilder();
                try
                {
                    trace.Append("a");

                    var item = this.Items[this.Items.IndexOfValue(this.Value)];
                    trace.Append("b");

                    this.Items.Clear();
                    trace.Append("c");

                    this.Items.Add(item);
                    trace.Append("d");

                    this.SelectedIndex = 0;
                    trace.Append("e");

                    this.Enabled = false;
                    trace.Append("f");
                }
                catch (Exception ex)
                {
                    throw new Dionysos.TechnicalException(ex, "Cache-probleem voor ComboBox {0} - Items.Count {1} - Gezochte value {2} - DateTime.UtcNow {3} - Trace {4} - Gelieve deze melding te mailen.",
                        this.ID, this.Items.Count, this.Value, DateTime.UtcNow, trace.ToString());
                }
            }
        }

        /// <summary>
        /// Renders the specified writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            bool renderReadOnly = false;
            if (this.Page.GetType().GetProperty("RenderReadOnly") != null)
            {
                object val = this.Page.GetType().GetProperty("RenderReadOnly").GetValue(this.Page, null);
                if (Convert.ToBoolean(val))
                {
                    renderReadOnly = true;
                }
            }

            // If Disabled, make sure the item is free of HTML
            if (!this.Enabled)
            {
                this.Text = StringUtil.StripHtml(this.Text);
            }

            if (this.IsRequired)
            {
                this.CssClass += " required ";
            }

            //Perform the normal rendering
            if (this.isValid)
            {
                this.CssClass.Replace(" ivld ", "");
            }
            else
            {
                this.CssClass += " ivld ";
            }

            // Render the control as read only when the associated flag is set
            // or the pagemode is set to view
            if ((renderReadOnly || this.PageMode == PageMode.View))
            {
                Dionysos.Web.UI.WebControls.Label lbl = new Dionysos.Web.UI.WebControls.Label();
                lbl.LocalizeText = false;
                lbl.Text = (this.Text.Length > 0 ? this.Text : string.Empty);
                lbl.RenderControl(writer);
            }
            else
            {
                //Perform the normal rendering

                // Add the required CssClass if needed
                if (this.IsRequired)
                {
                    this.CssClass += " " + ConfigurationManager.GetString(DevExConfigConstants.RequiredControlCssClass);
                }

                base.Render(writer);
            }
        }

        #endregion

        #region IValidator Members

        /// <summary>
        /// When implemented by a class, gets or sets the error message text generated when the condition being validated fails.
        /// </summary>
        /// <returns>
        /// The error message to generate.
        ///   </returns>
        string System.Web.UI.IValidator.ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether the editor's value passes validation.
        /// </summary>
        /// <value>
        /// 	<b>true</b> if the editor's value passes validation; otherwise, <b>false</b>.
        /// </value>
        bool System.Web.UI.IValidator.IsValid
        {
            get
            {
                return this.isValid;
            }
            set
            {
                this.isValid = value;
            }
        }

        /// <summary>
        /// Performs the editor's validation.
        /// </summary>
        public new void Validate()
        {
            if (this.pageMode == PageMode.View)
            {
                // No validation in view page mode
            }
            else
            {
                // Check whether input is required
                // and if so, check whether input is entered

                // First Check Entity Validation
                if (this.Page is Dionysos.Web.UI.PageLLBLGenEntity)
                {
                    IDataErrorInfo errorInfo = ((Dionysos.Web.UI.PageLLBLGenEntity)this.Page).DataErrorInfo;
                    string fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
                    if (errorInfo[fieldName] != null && errorInfo[fieldName].Length > 0)
                    {
                        this.errorMessage = "<strong>" + this.FriendlyName + "</strong>: " + errorInfo[fieldName].Replace(fieldName, this.FriendlyName);
                        this.isValid = false;
                    }
                }

                // Only check if no LLBL errors are found
                if (isValid && Visible && Enabled && IsRequired && (Instance.Empty(Value) || SelectedItem == null || EmptyItemText == SelectedItem.Text))
                {
                    isValid = false;
                    errorMessage = string.Format(Dionysos_Web_UI_DevExControls.ErrorMessage_Required, FriendlyName);
                }
            }
        }

        #endregion

        #region IBindable Members

        /// <summary>
        /// Gets or sets the flag which indicated whether databinding should be used
        /// </summary>
        [Browsable(true)]
        public bool UseDataBinding { get; set; }

        #endregion

        #region ILocalizableControl Members

        /// <summary>
        /// Gets or sets the translation tag empty item text.
        /// </summary>
        /// <value>
        /// The translation tag empty item text.
        /// </value>
        public string TranslationTagEmptyItemText { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to localize the empty item text.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the empty item text is localized; otherwise, <c>false</c>.
        /// </value>
        public bool LocalizeEmptyItemText
        {
            get
            {
                return this.localizeEmptyItemText;
            }
            set
            {
                this.localizeEmptyItemText = value;
            }
        }

        /// <summary>
        /// Localization information per property
        /// </summary>
        public Dictionary<string, Translatable> TranslatableProperties
        {
            get
            {
                if (this.localizableProperties == null)
                {
                    this.localizableProperties = new Dictionary<string, Translatable>();

                    // TranslationKey Prefix
                    string translationKey = String.Empty;
                    if (!String.IsNullOrEmpty(this.ID))
                    {
                        translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID;
                    }

                    // EmptyItemText
                    if (this.LocalizeEmptyItemText)
                    {
                        Translatable textProperty = new Translatable();
                        textProperty.TranslationValue = this.EmptyItemText;

                        if (!this.TranslationTagEmptyItemText.IsNullOrWhiteSpace())
                        {
                            textProperty.TranslationKey = this.TranslationTagEmptyItemText;
                        }
                        else
                        {
                            // It's a fixed value, not per page.
                            textProperty.TranslationKey = "ComboBox.EmptyItemText";
                        }

                        this.localizableProperties.Add("EmptyItemText", textProperty);
                    }
                }

                return this.localizableProperties;
            }
            set
            {
                this.localizableProperties = value;

                // Set the translated values
                if (this.localizableProperties.ContainsKey("EmptyItemText") &&
                    this.Items.Count > 0)
                {
                    this.EmptyItemText = this.localizableProperties["EmptyItemText"].TranslationValue;

                    // Update the EmptyItem ListItem
                    if (this.DisplayEmptyItem &&
                        !this.EmptyItemText.IsNullOrWhiteSpace())
                    {
                        if (this.Items[0].Value == null &&
                            this.Items[0].Text != this.EmptyItemText)
                        {
                            this.Items[0].Text = this.EmptyItemText;
                        }
                    }
                }
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Occurs when data bound items are ready to be cached.
        /// </summary>
        public event EventHandler ItemsDataBoundReadyToBeCached;

        /// <summary>
        /// Called when data bound items are ready to be cached.
        /// </summary>
        protected void OnItemsDataBoundReadyToBeCached()
        {
            if (this.ItemsDataBoundReadyToBeCached != null)
            {
                this.ItemsDataBoundReadyToBeCached(this, null);
            }
        }

        #endregion
    }
}
