﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces.Data;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Represents an editor which displays a list of items with LLBLGen entity values within its dropdown window.
	/// </summary>
	public class ComboBoxLLBLGenEntityCollection : ComboBoxEntityCollection
	{

        public static bool DatabindingMagic = false;

		#region Fields

		/// <summary>
		/// Indicates whether items are cached on the DataBound event.
		/// </summary>
		private bool cacheItemsOnDataBound = false;

		/// <summary>
		/// The fetched collection count.
		/// </summary>
		private int? fetchedCollectionCount = null;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the cache key items.
		/// </summary>
		private string CacheKeyItems
		{
			get
			{
				return StringUtil.CombineWithSeperator("-", this.GetType().FullName, this.EntityName, "Items", this.ValueField, this.TextField);
			}
		}

		/// <summary>
		/// Gets the cache key last retrieval.
		/// </summary>
		private string CacheKeyLastRetrieval
		{
			get
			{
				return StringUtil.CombineWithSeperator("-", this.GetType().FullName, this.EntityName, "LastRetrieval");
			}
		}

		/// <summary>
		/// Gets the cache key count.
		/// </summary>
		private string CacheKeyCount
		{
			get
			{
				return StringUtil.CombineWithSeperator("-", this.GetType().FullName, this.EntityName, "Count");
			}
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public new int? Value
		{
			get
			{
				return base.Value;
			}
			set
			{
				base.Value = value;

				if (value.GetValueOrDefault() == 0 || value.GetValueOrDefault() == -1) // GK It's a 'Gamble', is just don't expect lower than 0 or -1 to be used
				{
					base.Value = null;
					this.Text = this.EmptyItemText;
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ComboBoxLLBLGenEntityCollection"/> class.
		/// </summary>
		public ComboBoxLLBLGenEntityCollection()
		{
			this.ItemsDataBoundReadyToBeCached += new EventHandler(ComboBoxLLBLGenEntityCollection_ItemsDataBoundReadyToBeCached);
		}

		#endregion

		#region Event Handlers

		public override void ComboBox_PreRender(object sender, EventArgs e)
		{
			// GK Fall back when a control was not databound
			if (this.DisplayEmptyItem && !this.EmptyItemText.IsNullOrWhiteSpace())
			{
				if (this.Items.Count == 0 || this.Items[0].Value != null)
				{
					// Insert an empty item
					this.Items.Insert(0, new ListEditItem(this.EmptyItemText, null));
				}

				if (this.Items.Count >= 1 &&
					this.Items[0].Value == null &&
					this.Items[0].Text != this.EmptyItemText)
				{
					this.Items[0].Text = this.EmptyItemText;
				}
			}

			// Set the correct link for the selected entity
			if (ConfigurationManager.GetBool(DevExConfigConstants.ComboBoxEnableEntityLink))
			{
				IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(this.EntityName);
				if (entityInformation != null &&
					!String.IsNullOrEmpty(entityInformation.DefaultEntityEditPage) &&
					!String.IsNullOrEmpty(this.SelectedValueString))
				{
					// TODO Should also check if page is allowed for the current user
					string url = entityInformation.DefaultEntityEditPage + "?id=" + this.SelectedValueString;

					// Link image
					HyperLink imgLink = new HyperLink();
					imgLink.ID = this.ID + "-combobox";
					imgLink.ImageUrl = this.ResolveUrl("~/Images/Icons/navigate.png");
					imgLink.CssClass = "comboboxLinkImg";
					imgLink.NavigateUrl = url;
					imgLink.Target = "_blank";

					this.Width = Unit.Percentage(96);
					this.Controls.Add(imgLink);
				}
			}

			this.SetStateForDisabledOnceSelectedAndSaved();
		}

		/// <summary>
		/// Handles the ItemsDataBoundReadyToBeCached event of the ComboBoxLLBLGenEntityCollection control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ComboBoxLLBLGenEntityCollection_ItemsDataBoundReadyToBeCached(object sender, EventArgs e)
		{
			// Cache Items only when retrieved within the combobox
			int? expectedItemCount = this.fetchedCollectionCount;

			if (expectedItemCount.HasValue &&
				this.DisplayEmptyItem)
			{
				expectedItemCount++;
			}

			if (ConfigurationManager.GetBool(DevExConfigConstants.CacheComboBoxDataSources) &&
				this.cacheItemsOnDataBound &&
				expectedItemCount.HasValue &&
				expectedItemCount == this.Items.Count)
			{
				List<string[]> items = new List<string[]>();
				for (int i = 0; i < this.Items.Count; i++)
				{
					string value = null;
					if (this.Items[i].Value != null)
					{
						value = this.Items[i].Value.ToString();
					}

					items.Add(new string[] { this.Items[i].Text, value });
				}

				CacheHelper.AddSlidingExpire(false, this.CacheKeyItems, items, 600);
				this.cacheItemsOnDataBound = false;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the state for disabled once selected and saved.
		/// </summary>
		protected override void SetStateForDisabledOnceSelectedAndSaved()
		{
			if (this.DisabledOnceSelectedAndSaved &&
				this.ValidId > 0)
			{
				StringBuilder trace = new StringBuilder();
				try
				{
					trace.Append("a");
					var index = this.Items.IndexOfValue(this.Value);
					trace.AppendFormat("b - index: '{0}' - ", index);
					var item = this.Items[index];
					trace.Append("c");
					this.Items.Clear();
					trace.Append("d");
					this.Items.Add(item);
					trace.Append("e");
					this.SelectedIndex = 0;
					trace.Append("f");
					this.Enabled = false;
					trace.Append("g");
				}
				catch (Exception ex)
				{
					StringBuilder itemsText = new StringBuilder();
					if (this.Items != null)
					{
						foreach (ListEditItem item in this.Items)
							itemsText.AppendFormatLine("Item: '{0}' - '{1}'", item.Text, item.Value);
					}

					DateTime lastRetrieval;
					if (!CacheHelper.TryGetValue(CacheKeyLastRetrieval, false, out lastRetrieval))
						lastRetrieval = DateTime.MinValue;

					throw new TechnicalException(ex, "Cache-probleem voor ComboboxEntityCollection {0} - Items.Count {1} - Gezochte value {2} - Valid Id {5} - DateTime.UtcNow {3} - Cached {4} - Trace {6} - Items {7} Gelieve deze melding te mailen.",
						this.ID, this.Items.Count, this.Value, DateTime.UtcNow, lastRetrieval, this.ValidId, trace.ToString(), itemsText.ToString());
				}
			}
		}

		/// <summary>
		/// Loads the ListItems from the cache, returns TRUE when could be retrieved, otherwise false
		/// </summary>
		/// <returns>
		/// returns TRUE when could be retrieved, otherwise false
		/// </returns>
		public override bool LoadListItemsFromCache()
		{
			bool useFromCache = false;

			// Check if the underlying collection has changed
			DateTime lastRetrieval;
			if (ConfigurationManager.GetBool(DevExConfigConstants.CacheComboBoxDataSources) &&
				CacheHelper.TryGetValue(CacheKeyLastRetrieval, false, out lastRetrieval))
			{
				IEntity entity = (IEntity)DataFactory.EntityFactory.GetEntity(this.EntityName);
				if (entity.Fields["Created"] != null &&
					entity.Fields["Updated"] != null)
				{
					PredicateExpression filter = LLBLGenFilterUtil.GetFieldCompareValuePredicateExpression(this.EntityName, new string[] { "Created", "Updated" }, new ComparisonOperator[] { ComparisonOperator.GreaterThan, ComparisonOperator.GreaterThan }, new object[] { lastRetrieval, lastRetrieval }, true);
					IEntityCollection entities = (IEntityCollection)DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName);
					int count;
					if (entities.GetDbCount(filter) == 0 &&
						(!CacheHelper.TryGetValue(CacheKeyCount, false, out count) || count == entities.GetDbCount()))
					{
						// No created or updated items and the same count (if available for comparison), so use from cache
						useFromCache = true;
					}
				}
			}

			// Append the cache items, if can be loaded from cache
			List<string[]> items;
			if (useFromCache &&
				CacheHelper.TryGetValue(this.CacheKeyItems, false, out items))
			{
				// Items found in cache
				foreach (string[] item in items)
				{
					this.Items.Add(item[0], item[1]);
				}

				if (this.DisplayEmptyItem)
				{
					this.Value = 0;
				}
			}
			else
			{
				// No items found in cache
				useFromCache = false;
			}

			// If can't use from cache, make sure we save them on next prerender.
			this.cacheItemsOnDataBound = !useFromCache;

			return useFromCache;
		}

		/// <summary>
		/// Initializes the properly sorted entity collection of the dropdownlist. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		///   <c>true</c> is succesfull; otherwise, <c>false</c>.
		/// </returns>
		public override bool InitializeEntityCollection()
		{
			// Retrieve
			IEntityCollection entities = (IEntityCollection)DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName);
			IEntity entity = (IEntity)DataFactory.EntityFactory.GetEntity(this.EntityName);

			ExcludeIncludeFieldsList includedFields = new ExcludeIncludeFieldsList(false);
			if (!String.IsNullOrEmpty(this.ValueField) &&
				!String.IsNullOrEmpty(this.TextField))
			{
				// Get distinct non-null fields
				IEnumerable<string> fields = new string[] {
					this.ValueField, this.TextField, this.DataSortField
				}.Distinct().Where(field => !String.IsNullOrEmpty(field));

				if (fields.All(field => entity.Fields[field] != null))
				{
					// All database fields, so no problem fetching only these
					foreach (string field in fields)
					{
						includedFields.Add(entity.Fields[field]);
					}
				}
				else
				{
					// Get DatabaseFieldsAttribute from all fields
					IEnumerable<PropertyInfo> propertyInfos = fields.Select(field => entity.GetType().GetProperty(field));
					IEnumerable<DatabaseFieldsAttribute> databaseFieldAttributes = propertyInfos.Select(propertyInfo => propertyInfo == null ? null : propertyInfo.GetCustomAttributes(typeof(DatabaseFieldsAttribute), false).Cast<DatabaseFieldsAttribute>().SingleOrDefault());
					if (databaseFieldAttributes.All(databaseFieldAttribute => databaseFieldAttribute != null))
					{
						// All decorated with DatabaseFieldsAttribute, so fetch only specified database fields
						foreach (DatabaseFieldsAttribute databaseFieldAttribute in databaseFieldAttributes)
						{
							foreach (string databaseField in databaseFieldAttribute.DatabaseFields)
							{
								includedFields.Add(entity.Fields[databaseField]);
							}
						}
					}
				}
			}

			entities.GetMulti(null, includedFields, null);

			// Used for caching check in DataBound event:
			if (ConfigurationManager.GetBool(DevExConfigConstants.CacheComboBoxDataSources))
			{
				// Save last retrieval to cache
				this.fetchedCollectionCount = entities.Count;
				if (ConfigurationManager.GetBool(DevExConfigConstants.CacheComboBoxDataSources))
				{
					CacheHelper.AddSlidingExpire(false, this.CacheKeyLastRetrieval, DateTime.Now, 1440);
					CacheHelper.AddSlidingExpire(false, this.CacheKeyCount, entities.Count, 1440);
				}
			}

			if (!String.IsNullOrEmpty(this.DataSortField))
			{
				entities.Sort(this.DataSortField, ListSortDirection.Ascending, null);
			}

			this.EntityCollection = entities;

			return !Instance.Empty(this.EntityCollection);
		}

		#endregion

        #region Optimized Databinding (Phase 1, to be reviewed / updated by FO)

        // This logic must be extend with:
        // It should work for every existing Combobox, by checking the required fields, only
        // loading those using includefieldlists and databinding more optimized, but below 
        // doesn't support Properties instead of DBFields.
        // Be sure to test pre and post performance.

        public int? TextFieldIndex { get; set; }

        public int? ValueFieldIndex { get; set; }

        public override void DataBind()
        {
            if (!ComboBoxLLBLGenEntityCollection.DatabindingMagic)
            {
                base.DataBind();
                return;
            }

            bool databound = false;
            if (this.DataSource is IEntityCollection)
            {
                IEntityCollection entityCollection = (IEntityCollection)this.DataSource;
                if (!this.TextFieldIndex.HasValue || !this.ValueFieldIndex.HasValue)
                { 
                    // Simple version
                    if (entityCollection.Count > 0)                    
                    {
                        if (entityCollection[0].Fields[this.ValueField] != null &&
                            entityCollection[0].Fields[this.TextField] != null)
                        {
                            this.TextFieldIndex = entityCollection[0].Fields[this.TextField].FieldIndex;
                            this.ValueFieldIndex = entityCollection[0].Fields[this.ValueField].FieldIndex;
                        }
                    }
                }

                if (this.TextFieldIndex.HasValue && this.ValueFieldIndex.HasValue)
                {
                    bool displayFieldIsString = false;
                    foreach (IEntity entity in entityCollection)
                    {
                        if (entity.Fields[this.TextFieldIndex.Value].CurrentValue is string)
                            displayFieldIsString = true;

                        if (displayFieldIsString)
                            this.Items.Add((string)entity.Fields[this.TextFieldIndex.Value].CurrentValue, (int)entity.Fields[this.ValueFieldIndex.Value].CurrentValue);
                        else
                            this.Items.Add(entity.Fields[this.TextFieldIndex.Value].CurrentValue.ToString(), (int)entity.Fields[this.ValueFieldIndex.Value].CurrentValue);
                    }
                    databound = true;
                }
            }
            
            if(!databound)
                base.DataBind();
        }

        #endregion
    }
}
