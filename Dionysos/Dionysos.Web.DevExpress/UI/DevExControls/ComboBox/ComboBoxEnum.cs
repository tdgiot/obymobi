﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using Dionysos.Interfaces;
using System.Reflection;
using System.Diagnostics;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Represents an editor which displays a list of items with enum values within its dropdown window.
	/// </summary>
	public class ComboBoxEnum : ComboBox, IBindable
    {
        #region Fields

        private Type type = null;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            this.GetEnumType();
            if (this.type != null)
                this.DataBindEnumStringValuesAsDataSource(this.type);
        }

        public void SetEnumType(Type type)
        {
            this.type = type;            
        }

        /// <summary>
        /// Gets the enumeration type from the assembly qualified type string.
        /// </summary>
        /// <returns>The enumeration type if the type was found, null if not.</returns>
        private void GetEnumType()
        {
            Type type = null;

            if (!this.Type.IsNullOrWhiteSpace())
                type = System.Type.GetType(this.Type);

            if (type == null)
            {
                if (this.type == null)
                    throw new TechnicalException("No enumeration type could be determined for this ComboBoxEnum instance because the assembly qualified type name was not defined in the markup and no type was set during databinding.");
                else if (!this.type.IsEnum)
                    throw new TechnicalException("The type set during databinding is not an enumeration type. Set the assembly qualified type name (property Type in the markup) if you want to bind this control to an integer field.");
            }
            else
                this.type = type;
        }

		/// <summary>
		/// Datas the bind enum string values as data source.
		/// </summary>
		/// <param name="enumeration">The enumeration.</param>
		public void DataBindEnumStringValuesAsDataSource(Type enumeration)
		{
			string[] names = Enum.GetNames(enumeration);
			Array values = Enum.GetValues(enumeration);

			for (int i = 0; i < names.Length; i++)
			{
				string text = EnumUtil.GetStringValue((Enum)values.GetValue(i));
				int value = Convert.ToInt32(values.GetValue(i));
				this.Items.Add(text, value);
			}

			this.OnDataBound(null);
		}

		/// <summary>
		/// Datas the bind enum string values as data source.
		/// </summary>
		/// <param name="enumeration">The enumeration.</param>
		public void DataBindEnumStringValuesAsDataSource(Type enumeration, int rangeFrom, int rangeTo)
		{
			string[] names = Enum.GetNames(enumeration);
			Array values = Enum.GetValues(enumeration);

			for (int i = 0; i < names.Length; i++)
			{
				string text = EnumUtil.GetStringValue((Enum)values.GetValue(i));
				int value = Convert.ToInt32(values.GetValue(i));

				if (value >= rangeFrom && value < rangeTo)
					this.Items.Add(text, value);
			}

			this.OnDataBound(null);
		}

		#endregion

		#region Properties

        /// <summary>
        /// Gets or sets the value that represents the fully qualified type.
        /// </summary>
        public string Type { get; set; }

		/// <summary>
		/// Gets or sets the editor's edited value.
		/// </summary>
		/// <value>
		/// A nullable <see cref="T:System.Int32"/> value representing the editor's value.
		/// </value>
		[Browsable(false)]
		public new int? Value
		{
			get
			{
				int value;
				if (base.Value != null && Int32.TryParse(base.Value.ToString(), out value))
				{
					return value;
				}
				else
				{
					return null;
				}
			}
			set
			{
				base.Value = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ComboBoxEnum"/> class.
		/// </summary>
		public ComboBoxEnum()
		{
			this.ValueType = typeof(int);
			this.UseDataBinding = true;
            this.DisplayEmptyItem = false;
		}

		#endregion
	}
}
