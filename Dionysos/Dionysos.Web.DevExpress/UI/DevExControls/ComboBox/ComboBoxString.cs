﻿using System;
using System.Text;
using System.Web;
using System.Collections;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Dionysos.Reflection;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.DevExControls
{
	public class ComboBoxString : ComboBox, IBindable
	{
		#region Fields
		#endregion

		#region Methods

		public ComboBoxString()
		{
			this.ValueType = typeof(string);
			this.UseDataBinding = true;
		}

		#endregion

		#region Event Handlers	
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value of this TextBox
		/// </summary>
		[Browsable(false)]
		public new string Value
		{
			get
			{
				string toReturn = string.Empty;
				if (base.Value != null)
				{
					return base.Value.ToString();
				}
				else
				{
					return string.Empty;
				}				
			}
			set
			{
				// If the value is set and the box is a "" 				
				base.Value = value;
			}
		}	

		#endregion

	}
}
