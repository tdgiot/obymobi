﻿using System;
using System.Collections.Generic;
using DevExpress.Web.ASPxTreeList;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Interfaces.Data;
using Dionysos.Data;
using System.Drawing;
using System.Web.UI;
using Dionysos.Web.Security;
using System.Diagnostics;
using System.Web.UI.WebControls;
using System.Collections;
using System.ComponentModel;
using System.Linq;

namespace Dionysos.Web.UI.DevExControls
{
    /// <summary>
    /// Gui class which represents a treelist control for selecting gridview columns
    /// </summary>
    public class TreeListColumnSelector : TreeList
    {
        #region Fields

        private string entityName = string.Empty;
        private IGridViewEntityCollectionReady parentAsGridViewEntityReady = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.DevExControls.TreeListColumnSelector type
        /// </summary>
        public TreeListColumnSelector()
        {
            // Set the defaults
            this.SetDefaults();

            // Hookup the events
            this.HookupEvents();
        }

        #endregion

        #region Methods

        private void SetDefaults()
        {
            TreeListDataColumn fieldName = new TreeListDataColumn("FieldName");
            fieldName.Visible = false;
            TreeListDataColumn friendlyName = new TreeListDataColumn("FriendlyName");
            TreeListDataColumn type = new TreeListDataColumn("Type");
            type.Visible = false;

            this.Columns.Add(fieldName);
            this.Columns.Add(friendlyName);
            this.Columns.Add(type);

            this.SettingsSelection.Enabled = true;
            this.SettingsSelection.AllowSelectAll = true;

            this.Styles.Indent.BackColor = Color.Transparent;
            this.Styles.IndentWithButton.BackColor = Color.Transparent;

            this.EnableCallbacks = Dionysos.ConfigurationManager.GetBool(DevExConfigConstants.EnableCallbacksForControls);
        }

        private void HookupEvents()
        {
            this.PreRender += new EventHandler(TreeListColumnSelector_PreRender);
        }

        /// <summary>
        /// Creates the nodes for the treelist based on the column of the entity and related entities
        /// </summary>
        public void CreateColumnNodes()
        {
            IEntity entity = DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
            Trace.Assert(!Instance.Empty(entity), string.Format("Entity could not be loaded for entity name '{0}'", this.EntityName));

            IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(this.EntityName);
            Trace.Assert(!Instance.Empty(entityInformation), string.Format("Entity information could not be retrieved for entity '{0}'", this.EntityName));

            int fieldCount = 0;
            SortedList keys = new SortedList();

            // Check whether field exist
            foreach (IEntityFieldInformation entityFieldInformation in entityInformation.AllFields.Values)
            {
                if (!Instance.Empty(entityFieldInformation) && entityFieldInformation.AllowShowOnGridView && !entityInformation.ForeignKeyFields.ContainsKey(entityFieldInformation.FieldName))
                {
                    fieldCount++;

                    if (keys.ContainsKey(entityFieldInformation.FriendlyName))
                        keys.Add(string.Format("{0}{1}", entityFieldInformation.FriendlyName, fieldCount), entityFieldInformation.FieldName);
                    else
                        keys.Add(entityFieldInformation.FriendlyName, entityFieldInformation.FieldName);
                }
            }

            if (fieldCount > 0)
            {
                // Add the entity name of the root entity
                // to the list of entity names


                // Create a node for the entity
                TreeListNode root = this.CreateNode(this.EntityName,
                    "Entity",
                    this.EntityName,
                    (entityInformation.FriendlyName.Length > 0 ? entityInformation.FriendlyName : entityInformation.EntityName),
                    this.EntityName,
                    string.Empty,
                    null,
                    false);

                // Always expand root
                root.AllowSelect = false;
                root.Expanded = true;

                // Add the fields of the root entity to the treelist
                foreach (string key in keys.Values)
                {
                    IEntityFieldInformation entityFieldInformation = entityInformation.AllFields[key];
                    if (!Instance.Empty(entityFieldInformation) &&
                        entityFieldInformation.AllowShowOnGridView)
                    {
                        this.CreateNode(entityFieldInformation.FieldName,
                            "EntityField",
                            entityFieldInformation.FieldName,
                            (entityFieldInformation.FriendlyName.Length > 0 ? entityFieldInformation.FriendlyName : entityFieldInformation.FieldName),
                            this.EntityName,
                            entityFieldInformation.Type,
                            root,
                            false);
                    }
                }

                foreach (IEntityRelationInformation relationInformation in entityInformation.RelationInformation.Values)
                {
                    List<string> entityNames = new List<string>();
                    entityNames.Add(this.EntityName);
                    if (relationInformation.RelationType == RelationType.OneToMany)
                    {
                        // Invalid entity information?
                    }
                    else if (!Instance.Empty(relationInformation)) // GK Deleted:  && !entityNames.Contains(relationInformation.RelatedEntityName))
                    {
                        // Recursively walk through the related entities of this entity
                        this.CreateRelatedNodes(relationInformation, string.Empty, ref entityNames, root);
                    }
                }
            }
        }

        /// <summary>
        /// Creates the nodes for the treelist based on the columns of related entities
        /// </summary>
        /// <param name="relationInformation">The relation information instance to create the nodes for</param>
        /// <param name="parentMappedFieldName">This contains the names of the parrent MappedFieldNames, for example OrderEntity.CustomerEntity.CountryEntity </param>
        /// <param name="entityNames">The list of entity names which already have been added to the treelist</param>
        private void CreateRelatedNodes(IEntityRelationInformation relationInformation, string parentMappedFieldName, ref List<string> entityNames, TreeListNode root)
        {
            IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(relationInformation.RelatedEntityName);

            int fieldCount = 0;
            SortedList keys = new SortedList();

            // Check whether field exist
            foreach (IEntityFieldInformation entityFieldInformation in entityInformation.AllFields.Values)
            {
                if (!Instance.Empty(entityFieldInformation) &&
                    entityFieldInformation.AllowShowOnGridView &&
                    !entityInformation.ForeignKeyFields.ContainsKey(entityFieldInformation.FieldName))
                {
                    fieldCount++;

                    if (keys.ContainsKey(entityFieldInformation.FriendlyName))
                        keys.Add(string.Format("{0}{1}", entityFieldInformation.FriendlyName, fieldCount), entityFieldInformation.FieldName);
                    else
                        keys.Add(entityFieldInformation.FriendlyName, entityFieldInformation.FieldName);
                }
            }

            if (fieldCount > 0)
            {
                // Add the entity name of the related entity
                // to the list of entity names

                // GK Deleted:
                entityNames.Add(relationInformation.RelatedEntityName);

                string mappedFieldNameIncludingParentMappedFieldName = StringUtil.CombineWithSeperator(".", parentMappedFieldName, relationInformation.MappedFieldName);

                // Create a treelist node for the related entity
                TreeListNode parentNode = this.CreateNode(mappedFieldNameIncludingParentMappedFieldName,
                                                        "EntityRelation",
                                                        mappedFieldNameIncludingParentMappedFieldName,
                                                        (relationInformation.RelatedEntityFriendlyName.Length > 0 ? relationInformation.RelatedEntityFriendlyName : relationInformation.RelatedEntityName),
                                                        relationInformation.RelatedEntityName,
                                                        string.Empty,
                                                        root,
                                                        false);

                parentNode.AllowSelect = false;

                // Add the fields
                foreach (string key in keys.Values)
                {
                    IEntityFieldInformation entityFieldInformation = entityInformation.AllFields[key];

                    if (!Instance.Empty(entityFieldInformation) &&
                        entityFieldInformation.AllowShowOnGridView)
                    {
                        string fullMappedFieldName = StringUtil.CombineWithSeperator(".", mappedFieldNameIncludingParentMappedFieldName, entityFieldInformation.FieldName);
                        string friendlyName = entityFieldInformation.FriendlyName.Length > 0 ? entityFieldInformation.FriendlyName : entityFieldInformation.FieldName;

                        this.CreateNode(fullMappedFieldName,
                            "EntityField",
                            fullMappedFieldName,
                            friendlyName,
                            relationInformation.RelatedEntityName,
                            entityFieldInformation.Type,
                            parentNode,
                            false);
                    }
                }

                // Add the related entities
                foreach (IEntityRelationInformation relationInformation2 in entityInformation.RelationInformation.Values)
                {
                    if (!Instance.Empty(relationInformation2) && !entityNames.Contains(relationInformation2.RelatedEntityName))
                    {
                        this.CreateRelatedNodes(relationInformation2, mappedFieldNameIncludingParentMappedFieldName, ref entityNames, parentNode);
                    }
                }
            }
        }

        public void SelectNodesBasedOnGridView(TreeListNodeCollection nodes)
        {
            GridViewEntityCollection gv = this.PageAsGridViewEntityCollectionReady.MainGridView;
            foreach (TreeListNode node in nodes)
            {
                // Mark as selected (first select this item (the parent) then the childs. Otherwise non-active parent would deact
                node.Selected = gv.Columns[node.Key] != null;

                if (node.ChildNodes.Count > 0)
                    SelectNodesBasedOnGridView(node.ChildNodes);
            }
        }

        /// <summary>
        /// Creates an treelist node
        /// </summary>
        /// <param name="key">The key of the node</param>
        /// <param name="iconName">The name of the icon</param>
        /// <param name="fieldName">The name of the field</param>
        /// <param name="friendlyName">The text to display</param>
        /// <param name="type">The type</param>
        /// <param name="parentNode">The parent node</param>
        /// <returns>A TreeListNode instance</returns>
        public TreeListNode CreateNode(object key, string iconName, string fieldName, string friendlyName, string entityName, string type, TreeListNode parentNode, bool selected)
        {
            TreeListNode node = this.AppendNode(key, parentNode);
            node["IconName"] = iconName;
            node["FieldName"] = fieldName;
            node["FriendlyName"] = friendlyName;
            node["Type"] = type;
            node["EntityName"] = entityName;

            return node;
        }

        private void SaveVisibleColumns(List<TreeListNode> selectedNodes)
        {
            // Ensure the selected view is a custom view before changing the definition
            this.PageAsGridViewEntityCollectionReady.SelectedView = ViewUtil.GetOrCreateCustomView(this.PageAsGridViewEntityCollectionReady.SelectedView, UserManager.CurrentUser.UserId);

            // Remove deselected
            for (int i = this.PageAsGridViewEntityCollectionReady.SelectedView.ViewItems.Count - 1; i >= 0; i--)
            {
                IViewItem viewItem = this.PageAsGridViewEntityCollectionReady.SelectedView.ViewItems[i] as IViewItem;
                Trace.Assert(viewItem != null);

                string fieldName = viewItem.FieldName;

                TreeListNode node = this.FindNodeByKeyValue(fieldName);
                if (node == null || !node.Selected)
                    this.PageAsGridViewEntityCollectionReady.SelectedView.RemoveViewItem(fieldName);
            }

            foreach (TreeListNode selectedNode in selectedNodes.Where(node => !node.HasChildren))
            {
                string fieldName = selectedNode["FieldName"] as string;
                string type = selectedNode["Type"] as string;                

                if (this.PageAsGridViewEntityCollectionReady.SelectedView.ContainsViewItem(fieldName))
                {
                    // Already part of the viewItem, no need to add.
                    continue;
                }

                IEntityFieldInformation fieldInformation;

                // Check for related fieldnames, identify their Entity
                if (fieldName.Contains("."))
                {
                    // Get entityname
                    string[] fieldNameElements = fieldName.Split(new char[] { '.' });
                    string relatedFieldName = fieldNameElements[fieldNameElements.Length - 1];

                    string nodeEntityName = selectedNode["EntityName"] as string;
                    var entityInfo = EntityInformationUtil.GetEntityInformation(nodeEntityName);
                    fieldInformation = entityInfo.AllFields[relatedFieldName];
                }
                else
                {
                    var entityInfo = EntityInformationUtil.GetEntityInformation(this.EntityName);
                    fieldInformation = entityInfo.AllFields[fieldName];
                }

                this.PageAsGridViewEntityCollectionReady.SelectedView.AddViewItem(fieldName, type, fieldInformation.DisplayFormatString, this.PageAsGridViewEntityCollectionReady.GetType().BaseType.FullName);
            }
        }

        protected override void CreateChildControls()
        {
            LinkButton hlSaveTop = new LinkButton();
            hlSaveTop.Text = Dionysos.Web.DevExpress.Resources.Dionysos_Web_UI_DevExControls.ColumnSelectorSave;
            hlSaveTop.Click += new EventHandler(hlSaveTop_Click);
            this.Controls.Add(hlSaveTop);
            this.Controls.Add(new LiteralControl("<br /><br />"));

            base.CreateChildControls();

            this.Controls.Add(new LiteralControl("&nbsp;<br />"));
            if (this.Page is PageDataSourceCollection)
            {
                LinkButton hlShowArchivedSwitch = new LinkButton();
                hlShowArchivedSwitch.Text = PageDataSourceCollection.ShowArchivedRecords ? Dionysos.Web.DevExpress.Resources.Dionysos_Web_UI_DevExControls.ColumnSelectorHideArchivedRecords : Dionysos.Web.DevExpress.Resources.Dionysos_Web_UI_DevExControls.ColumnSelectorShowArchivedRecords;
                hlShowArchivedSwitch.Click += new EventHandler(hlShowArchivedSwitch_Click);
                this.Controls.Add(hlShowArchivedSwitch);
            }
        }

        #endregion

        #region Event Handlers

        protected void hlShowArchivedSwitch_Click(object sender, EventArgs e)
        {
            PageDataSourceCollection.ShowArchivedRecords = !PageDataSourceCollection.ShowArchivedRecords;
            WebShortcuts.RedirectUsingRawUrl();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the entity
        /// </summary>
        public string EntityName
        {
            get
            {
                if (this.entityName.Length == 0)
                    this.entityName = this.PageAsGridViewEntityCollectionReady.EntityName;
                return this.entityName;
            }
            set
            {
                this.entityName = value;
            }
        }

        private IGridViewEntityCollectionReady GetParentAsIGridViewEntityCollectionReady(Control c)
        {
            IGridViewEntityCollectionReady toReturn = c as IGridViewEntityCollectionReady;
            if (toReturn != null)
                return toReturn;

            if (c.Parent != null)
                return this.GetParentAsIGridViewEntityCollectionReady(c.Parent);
            else
                return null;
        }

        /// <summary>
        /// Gets the current page/parent control casted as an PageLLBLOverviewDataSourceCollection
        /// </summary>
        public IGridViewEntityCollectionReady PageAsGridViewEntityCollectionReady
        {
            get
            {
                // First try parent control
                if (parentAsGridViewEntityReady == null)
                {
                    parentAsGridViewEntityReady = this.GetParentAsIGridViewEntityCollectionReady(this);
                    if (Instance.Empty(parentAsGridViewEntityReady))
                    {
                        // Try Page is not parent control
                        parentAsGridViewEntityReady = this.Page as IGridViewEntityCollectionReady;
                    }
                }
                if (Instance.Empty(parentAsGridViewEntityReady))
                {
                    throw new EmptyException("The page or parent control is not of type 'IGridViewEntityCollectionReady'.");
                }

                return parentAsGridViewEntityReady;
            }
        }

        #endregion

        #region Event handlers

        protected void hlSaveTop_Click(object sender, EventArgs e)
        {
            List<TreeListNode> selectedNodes = this.GetSelectedNodes(false);
            if (selectedNodes.Any())
            {
                this.SaveVisibleColumns(selectedNodes);
            }

            WebShortcuts.RedirectUsingRawUrl();
        }

        protected void TreeListColumnSelector_PreRender(object sender, EventArgs e)
        {
            this.SelectNodesBasedOnGridView(this.Nodes);
        }

        #endregion

        /// <summary>
        /// Provides data for a cancelable event.
        /// </summary>
        public class CancelCreateRelatedNodesEventArgs : CancelEventArgs
        {
            #region Properties

            /// <summary>
            /// Gets the relation information.
            /// </summary>
            /// <value>
            /// The relation information.
            /// </value>
            public IEntityRelationInformation RelationInformation { get; private set; }

            #endregion

            #region Constructors

            /// <summary>
            /// Initializes a new instance of the <see cref="CancelExceptionEventArgs" /> class with the specified exception and the <see cref="Cancel" /> property set to <c>false</c>.
            /// </summary>
            /// <param name="relationInformation">The relation information.</param>
            /// <exception cref="ArgumentNullException">exception is null.</exception>
            public CancelCreateRelatedNodesEventArgs(IEntityRelationInformation relationInformation)
                : this(relationInformation, false)
            { }

            /// <summary>
            /// Initializes a new instance of the <see cref="CancelExceptionEventArgs" /> class with the specified exception and the <see cref="Cancel" /> property set to the given value.
            /// </summary>
            /// <param name="relationInformation">The relation information.</param>
            /// <param name="cancel"><c>true</c> to cancel the event; otherwise, <c>false</c>.</param>
            /// <exception cref="System.ArgumentNullException">relationInformation</exception>
            /// <exception cref="ArgumentNullException">exception is null.</exception>
            public CancelCreateRelatedNodesEventArgs(IEntityRelationInformation relationInformation, bool cancel)
                : base(cancel)
            {
                if (relationInformation == null) throw new ArgumentNullException("relationInformation");

                this.RelationInformation = relationInformation;
            }

            #endregion
        }
    }
}
