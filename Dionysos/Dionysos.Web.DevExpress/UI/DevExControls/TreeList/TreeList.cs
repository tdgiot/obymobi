﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web.ASPxTreeList;

namespace Dionysos.Web.UI.DevExControls
{
    public class TreeList : ASPxTreeList
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.DevExControls
        /// </summary>
        public TreeList()
        {
            // Set the default
            this.SetDefaults();
        }

        #endregion

        #region Methods

        private void SetDefaults()
        {
            this.SettingsBehavior.ExpandCollapseAction = TreeListExpandCollapseAction.NodeDblClick;
            this.Settings.ShowColumnHeaders = false;
			this.EnableCallbacks = Dionysos.ConfigurationManager.GetBool(DevExConfigConstants.EnableCallbacksForControls);
        }

        /// <summary>
        /// Creates an treelist node
        /// </summary>
        /// <param name="key">The key of the node</param>
        /// <param name="iconName">The name of the icon</param>
        /// <param name="text">The text to display</param>
        /// <param name="parentNode">The parent node</param>
        /// <param name="selected">Flag which indicates whether the node is selected</param>
        /// <returns>A TreeListNode instance</returns>
        public TreeListNode CreateNode(object key, string iconName, string text, TreeListNode parentNode, bool selected)
        {
            TreeListNode node = this.AppendNode(key, parentNode);
            node["IconName"] = iconName;
            node["Name"] = text;
            node.Selected = selected;

            return node;
        }

        #endregion
    }
}
