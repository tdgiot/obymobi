﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using DevExpress.Web;
using Dionysos.Web.DevExpress.Resources;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Header Template Containing a Link with "Add"
	/// </summary>
	public class AddNewRowColumnHeaderTemplate : ITemplate
	{
		#region Fields

		ASPxGridView gridView = null;
		private string imageUrl = string.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Dionysos.Web.UI.DevExControls.AddNewRowColumnHeaderTemplate type
		/// </summary>
		/// <param name="gridView">The ASPxGridView instance which contains the header</param>
		public AddNewRowColumnHeaderTemplate(ASPxGridView gridView)
		{
			this.gridView = gridView;
		}

		#endregion

		#region Methods

		public void InstantiateIn(Control container)
		{
			HyperLink hl = new HyperLink();
			hl.Attributes.Add("onclick", string.Format("{0}.AddNewRow()", this.gridView.ClientInstanceName));
			hl.NavigateUrl = "javascript:;";
			hl.Text = Dionysos_Web_UI_DevExControls.AddNewRow;
			hl.ImageUrl = this.ImageUrl;
			container.Controls.Add(hl);
		}

		#endregion

		#region Event Handlers
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the text
		/// </summary>
		public string Text { get; set; }

		/// <summary>
		/// Gets or sets the imageUrl
		/// </summary>
		public string ImageUrl
		{
			get
			{
				return this.imageUrl;
			}
			set
			{
				this.imageUrl = value;
			}
		}


		#endregion
	}
}
