﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraPrinting;
using Dionysos.Interfaces.Data;
using Dionysos.Data;
using System.Web;
using System.IO;
using DevExpress.Web;
using Dionysos.Web.DevExpress.Resources;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI.DevExControls
{
	public class GridViewColumnSelector : Control, INamingContainer
	{
		#region Fields

		private GridViewEntityCollection gridView = null;
		private TreeListColumnSelector columnSelector = null;
		private PopupControl popupControl;
		private HyperLink popupHyperLink;
		private Panel panel;
		private Dionysos.Web.UI.WebControls.Button buttonExportToPdf = null;
		private Dionysos.Web.UI.WebControls.Button buttonExportToExcel = null;
		private ASPxGridViewExporter exporter = null;
		public const string QueryStringParameterPageMode = "mode";
		private Dionysos.Web.UI.WebControls.Button buttonPrintViaPdf = null;

		#endregion

		#region Methods

		public GridViewColumnSelector()
		{
			this.EnsureChildControls();
		}

		protected override void OnLoad(EventArgs e)
		{
			this.EnsureChildControls();

			base.OnLoad(e);

			this.HookUpEvents();
		}

		protected override void CreateChildControls()
		{
			if (!this.ChildControlsCreated)
			{
				// Add controls for reporting mode
				if (this.PageModePrint)
					this.CreateControlsForReportingMode();

				// Create default controls
				this.CreateStandardControls();

				// Create on LoadComplete to perform tasks on GridViewentity
				this.gridView.LoadComplete += new EventHandler(gridView_LoadComplete);

				this.ChildControlsCreated = true;

				this.HookUpEvents();
			}
		}

		protected void CreateStandardControls()
		{

			// Initialize and append GridView
			if (this.gridView == null)
			{
				this.gridView = new GridViewEntityCollection();
				this.gridView.AutoGenerateColumns = false;
				this.gridView.GenerateColumnsFromEntityInformation = true;
				this.gridView.EnableCallBacks = Dionysos.ConfigurationManager.GetBool(DevExConfigConstants.EnableCallbacksForControls);
				this.gridView.ID = "gvMainGridView";

				if (this.PageModePrint)
				{
					this.GridView.Settings.ShowGroupPanel = true;
				}
			}

			this.Controls.Add(gridView);

			if (this.PageModePrint)
			{
				this.Exporter = new ASPxGridViewExporter();
				this.Exporter.Landscape = true;
				this.Exporter.ID = "Exporter";
				this.Exporter.SetDefaultSettings();
				//this.Exporter.GridViewID = this.GridView.ID;
				this.Controls.Add(this.Exporter);
			}

			// Initialize Popup Control, Panel and Column Selector
			if (this.popupHyperLink == null)
			{
				this.popupHyperLink = new HyperLink();
				this.popupHyperLink.ID = this.ID + "hlPopup";
				this.popupHyperLink.ImageUrl = "~/Images/Icons/expand.png";
			}

			if (this.panel == null)
			{
				this.panel = new Panel();
				this.panel.ID = this.ID + "pnlColumns";
			}

			if (this.columnSelector == null)
			{
				this.columnSelector = new TreeListColumnSelector();
				this.columnSelector.Width = new Unit("100%");
				this.columnSelector.ID = this.ID + "csColumns";
			}

			// Initialize and append column selector in a popupcontrol
			if (this.popupControl == null)
			{
				this.popupControl = new PopupControl();
				this.popupControl.Width = new Unit("400px");
				this.popupControl.MaxHeight = new Unit("400px");
				//this.popupControl.Style.Add("border-top", "100px solid transparent");
				this.popupControl.ID = this.ID + "pcColumns";
				this.popupControl.ClientInstanceName = this.ID + "pcColumns";
				this.popupControl.PopupVerticalAlign = PopupVerticalAlign.Below;
				this.popupControl.PopupHorizontalAlign = PopupHorizontalAlign.RightSides;
				this.popupControl.AutoUpdatePosition = true;
				this.popupControl.ScrollBars = ScrollBars.Auto;
				this.popupControl.EnableAnimation = false;
				this.popupControl.HeaderText = String.Empty;
				this.popupControl.EnableClientSideAPI = true;
				this.popupControl.EnableHierarchyRecreation = true;
			}

			// Create hiearchy: PopupControl > Panel > ColumnSelector
			this.panel.Controls.Add(this.columnSelector);
			this.popupControl.Controls.Add(this.panel);
			this.Controls.Add(this.popupControl);
		}

		protected void CreateControlsForReportingMode()
		{
			this.buttonPrintViaPdf = new Dionysos.Web.UI.WebControls.Button();
			this.buttonPrintViaPdf.ID = "btPrintViaPdf";
			this.buttonPrintViaPdf.TranslationTagText = "GridViewColumnSelector.btPrintViaPdf";
			this.buttonPrintViaPdf.Text = Dionysos_Web_UI_DevExControls.PrintPDF;

			this.buttonExportToPdf = new Dionysos.Web.UI.WebControls.Button();
			this.buttonExportToPdf.ID = "btExportToPdf";
			this.buttonExportToPdf.TranslationTagText = "GridViewColumnSelector.btExportToPdf";
			this.buttonExportToPdf.Text = Dionysos_Web_UI_DevExControls.ExportToPDF;

			this.buttonExportToExcel = new Dionysos.Web.UI.WebControls.Button();
			this.buttonExportToExcel.ID = "btExportToExcel";
			this.buttonExportToExcel.TranslationTagText = "GridViewColumnSelector.btExportToExcel";
			this.buttonExportToExcel.Text = Dionysos_Web_UI_DevExControls.ExportToExcel;

			LiteralControl startDiv = new LiteralControl("<div class=\"exportbuttons\">");
			LiteralControl endDiv = new LiteralControl("</div>");

			this.Controls.Add(startDiv);
			this.Controls.Add(this.ButtonExportToExcel);
			this.Controls.Add(this.ButtonExportToPdf);
			this.Controls.Add(this.ButtonPrintViaPdf);
			this.Controls.Add(endDiv);
		}

		protected void HookUpEvents()
		{
			if (this.PageModePrint)
			{
				this.buttonExportToExcel.Click += new EventHandler(buttonExportToExcel_Click);
				this.buttonExportToPdf.Click += new EventHandler(buttonExportToPdf_Click);
				this.ButtonPrintViaPdf.Click += new EventHandler(ButtonPrintViaPdf_Click);
			}
		}

		protected void gridView_LoadComplete(object sender, EventArgs e)
		{
            if (this.gridView.ShowColumnSelectorButton)
            {
                this.columnSelector.EntityName = this.PageAsIGridViewEntityCollectionReady.EntityName;
                this.columnSelector.CreateColumnNodes();

                // Only add column selector if set to do so
                if (Dionysos.ConfigurationManager.GetBool(DevExConfigConstants.AllowCustomViews))
                {
                    this.popupHyperLink.Attributes.Add("onclick", string.Format("{0}.ShowAtElement(this)", this.popupControl.ClientInstanceName));

                    // Try to get the very last columns
                    GridViewColumn column = this.GridView.Columns[this.GridView.Columns.Count - 1];
                    if (this.GridView.Columns["HyperLinkDeleteColumn"] != null)
                        column = this.GridView.Columns["HyperLinkDeleteColumn"];
                    else if (this.GridView.Columns["HyperLinkEditColumn"] != null)
                        column = this.GridView.Columns["HyperLinkEditColumn"];

                    if (this.GridView.Columns.Count > 0)
                    {
                        var gvHead = new ColumnSelectorColumnHeaderTemplate(this.GridView, this.popupHyperLink);
                        column.HeaderTemplate = gvHead;
                    }
                }
            }
		}

		#endregion

		#region Event Handlers

		protected void ButtonPrintViaPdf_Click(object sender, EventArgs e)
		{
			this.SetEditDeleteColumnsVisibility(false);

			PdfExportOptions options = new PdfExportOptions();
			options.Compressed = true;
			options.DocumentOptions.Title = this.EntityInformation.FriendlyNamePlural;
			options.ShowPrintDialogOnOpen = true;

			this.Page.Response.Clear();
			this.Exporter.WritePdfToResponse(this.GetFileName(), true, options);

			this.SetEditDeleteColumnsVisibility(true);
		}

		protected void buttonExportToPdf_Click(object sender, EventArgs e)
		{
			this.SetEditDeleteColumnsVisibility(false);

			PdfExportOptions options = new PdfExportOptions();
			options.Compressed = true;
			options.DocumentOptions.Title = this.EntityInformation.FriendlyNamePlural;

			this.Page.Response.Clear();
			this.Exporter.WritePdfToResponse(this.GetFileName(), true, options);

			this.SetEditDeleteColumnsVisibility(true);
		}

		protected void buttonExportToExcel_Click(object sender, EventArgs e)
		{
			this.SetEditDeleteColumnsVisibility(false);

			XlsExportOptions options = new XlsExportOptions(TextExportMode.Value, true, true);
			options.SheetName = this.EntityInformation.FriendlyNamePlural;

			this.Page.Response.Clear();
			this.Exporter.WriteXlsToResponse(this.GetFileName(), true, options);

			this.SetEditDeleteColumnsVisibility(true);
		}

		private void SetEditDeleteColumnsVisibility(bool visible)
		{
			if (this.GridView.Columns[GridViewEntityCollection.HyperLinkDeleteColumnName] != null)
				this.GridView.Columns[GridViewEntityCollection.HyperLinkDeleteColumnName].Visible = visible;
			if (this.GridView.Columns[GridViewEntityCollection.HyperLinkEditColumnName] != null)
				this.GridView.Columns[GridViewEntityCollection.HyperLinkEditColumnName].Visible = visible;
		}

		#endregion

		#region Properties

		private bool? reportingMode = null;

		/// <summary>
		/// Gets or sets the PageMode print from the QueryString because this control is build earlier than
		/// the Page is available
		/// </summary>
		public bool PageModePrint
		{
			get
			{
				bool toReturn = false;
				if (this.reportingMode.HasValue)
					toReturn = this.reportingMode.Value;
				else
				{
					if (QueryStringHelper.HasValueAndEquals(QueryStringParameterPageMode, "print"))
					{
						toReturn = true;
					}
				}

				return toReturn;
			}
			set
			{
				this.reportingMode = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the column selector is shown.
		/// </summary>
		/// <value>
		///   <c>true</c> if the column selector is shown; otherwise, <c>false</c>.
		/// </value>
		public bool ShowColumnSelector
		{
			get
			{
				return this.popupHyperLink.Visible;
			}
			set
			{
				this.popupHyperLink.Visible = false;
			}
		}

		/// <summary>
		/// Gets or sets the columnSelector
		/// </summary>
		public TreeListColumnSelector ColumnSelector
		{
			get
			{
				return this.columnSelector;
			}
			set
			{
				this.columnSelector = value;
			}
		}

		/// <summary>
		/// Gets or sets the PropertyName
		/// </summary>
		public GridViewEntityCollection GridView
		{
			get
			{
				return this.gridView;
			}
			set
			{
				this.gridView = value;
			}
		}

		/// <summary>
		/// Gets or sets the buttonExportToExcel
		/// </summary>
		public Dionysos.Web.UI.WebControls.Button ButtonExportToExcel
		{
			get
			{
				return this.buttonExportToExcel;
			}
			set
			{
				this.buttonExportToExcel = value;
			}
		}


		/// <summary>
		/// Gets or sets the buttonExportToPdf
		/// </summary>
		public Dionysos.Web.UI.WebControls.Button ButtonExportToPdf
		{
			get
			{
				return this.buttonExportToPdf;
			}
			set
			{
				this.buttonExportToPdf = value;
			}
		}

		/// <summary>
		/// Gets or sets the exporter
		/// </summary>
		public ASPxGridViewExporter Exporter
		{
			get
			{
				return this.exporter;
			}
			set
			{
				this.exporter = value;
			}
		}

		public override string ID
		{
			get
			{
				return base.ID;
			}
			set
			{
				base.ID = value;

				//if (HttpContext.Current.Items.Contains("GridViewColumSelectorId" + this.ID))
				//	throw new Dionysos.TechnicalException("Multiple GridViewColumnSelectors found with Id '{0}'. ID's must be unique.", this.ID);
				//else
				//	HttpContext.Current.Items.Add("GridViewColumSelectorId" + this.ID, "ok");

				this.popupControl.ID = this.ID + "pcColumns";
				this.popupControl.ClientInstanceName = this.ID + "pcColumns";
			}
		}

		private string GetFileName()
		{
			return StringUtil.RemoveAllNoneSafeUrlCharacters(this.EntityInformation.FriendlyNamePlural);
		}

		/// <summary>
		/// Gets or sets the buttonPrintViaPdf
		/// </summary>
		public Dionysos.Web.UI.WebControls.Button ButtonPrintViaPdf
		{
			get
			{
				return this.buttonPrintViaPdf;
			}
			set
			{
				this.buttonPrintViaPdf = value;
			}
		}

		public IEntityInformation EntityInformation
		{
			get
			{
				return EntityInformationUtil.GetEntityInformation(this.PageAsIGridViewEntityCollectionReady.EntityName);
			}
		}

		public IGridViewEntityCollectionReady PageAsIGridViewEntityCollectionReady
		{
			get
			{
				// First try parent control
				Control parent = this.Parent;
				while (parent != null && !(parent is IGridViewEntityCollectionReady))
				{
					parent = parent.Parent;
				}

				IGridViewEntityCollectionReady pageGridViewEntityReady = parent as IGridViewEntityCollectionReady;
				if (Instance.Empty(pageGridViewEntityReady))
				{
					// Try Page is not parent control
					pageGridViewEntityReady = this.Page as IGridViewEntityCollectionReady;
				}

				if (Instance.Empty(pageGridViewEntityReady))
				{
					throw new EmptyException("The page or parent control is not of type 'IGridViewEntityCollectionReady'.");
				}

				return pageGridViewEntityReady;
			}
		}

		#endregion

	}
}
