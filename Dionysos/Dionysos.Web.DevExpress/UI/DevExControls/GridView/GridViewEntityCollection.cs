﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Web;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces;
using Dionysos.Interfaces.Data;
using Dionysos.Web.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI.DevExControls
{
    public class GridViewEntityCollection : GridView
    {
        #region Fields

        public const string CommandsColumnName = "CommandsColumn";
        public const string HyperLinkDeleteColumnName = "HyperLinkDeleteColumn";
        public const string HyperLinkEditColumnName = "HyperLinkEditColumn";
        private IEntity emptyEntityOfTypeToDisplay = null;
        private bool? hasArchivedColumn = null;
        private IGuiEntityCollectionSimple parentAsEntityCollectionSimple = null;
        private IGridViewEntityCollectionReady parentAsGridViewEntityReady = null;
        private bool showColumnSelectorButton = true;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the set AutoFilter for the current grid instance. Key,Value = ColumnName,FilterValue
        /// </summary>
        public Dictionary<string, string> AutoFilters
        {
            get
            {
                string keyName = this.Page.GetType().BaseType.FullName + "-" + this.ID;

                Dictionary<string, string> filters;
                if (!SessionHelper.TryGetValue(keyName, out filters))
                {
                    filters = new Dictionary<string, string>();
                    SessionHelper.SetValue(keyName, filters);
                }

                return filters;
            }
        }

        /// <summary>
        /// Gets or sets the enableRowDoubleClick
        /// </summary>
        public bool EnableRowDoubleClick { get; set; } = true;

        /// <summary>
        /// Gets or sets the generateColumnsFromEntityInformation
        /// </summary>
        public bool GenerateColumnsFromEntityInformation { get; set; } = false;

        public bool HasArchivedColumn
        {
            get
            {
                if (!this.hasArchivedColumn.HasValue)
                {
                    if (this.EmptyEntityOfTypeToDisplay.Fields["Archived"] != null)
                        hasArchivedColumn = true;
                    else
                        hasArchivedColumn = false;
                }

                return this.hasArchivedColumn.Value;
            }
        }

        /// <summary>
        /// Gets the parent as an IGuiEntityCollectionSimple
        /// </summary>
        public IGuiEntityCollectionSimple ParentAsEntityCollectionSimple
        {
            get
            {
                if (this.parentAsEntityCollectionSimple == null)
                    this.parentAsEntityCollectionSimple = this.GetParentGuiEntityCollectionSimple(this);

                return this.parentAsEntityCollectionSimple;
            }
        }

        /// <summary>
        /// Gets the current page/parent control casted as an PageLLBLOverviewDataSourceCollection
        /// </summary>
        public IGridViewEntityCollectionReady ParentAsGridViewEntityCollectionReady
        {
            get
            {
                // First try parent control
                if (this.parentAsGridViewEntityReady == null)
                {
                    this.parentAsGridViewEntityReady = this.GetParentAsIGridViewEntityCollectionReady(this);
                    if (Instance.Empty(parentAsGridViewEntityReady))
                    {
                        // Try Page is not parent control
                        this.parentAsGridViewEntityReady = this.Page as IGridViewEntityCollectionReady;
                    }
                }

                if (this.parentAsGridViewEntityReady == null)
                {
                    throw new EmptyException("The page or parent control is not of type 'IGridViewEntityCollectionReady'.");
                }

                return this.parentAsGridViewEntityReady;
            }
        }

        /// <summary>
        /// Gets or sets the showAddNewButton
        /// </summary>
        public bool ShowAddNewButton { get; set; } = false;

        /// <summary>
        /// Gets or sets the flag which indicates whether to show the column selector button
        /// </summary>
        public bool ShowColumnSelectorButton
        {
            get
            {
                if (this.Page is PageInlineEditDataSourceCollection)
                    return false;
                else
                    return this.showColumnSelectorButton;
            }
            set
            {
                this.showColumnSelectorButton = value;
            }
        }

        /// <summary>
        /// Gets or sets the ShowDeleteColumn
        /// </summary>
        public bool ShowDeleteColumn { get; set; } = true;

        /// <summary>
        /// Gets or sets the ShowDeleteHyperlinkColumn
        /// </summary>
        public bool ShowDeleteHyperlinkColumn { get; set; } = false;

        /// <summary>
        /// Gets or sets the ShowEditColumn
        /// </summary>
        public bool ShowEditColumn { get; set; } = true;

        /// <summary>
        /// Gets or sets the ShowEditHyperlinkColumn
        /// </summary>
        public bool ShowEditHyperlinkColumn { get; set; } = false;

        /// <summary>
        /// Create a Empty Entity of the type to display
        /// </summary>
        public IEntity EmptyEntityOfTypeToDisplay
        {
            get
            {
                if (this.emptyEntityOfTypeToDisplay == null)
                    this.emptyEntityOfTypeToDisplay = Dionysos.DataFactory.EntityFactory.GetEntity(this.ParentAsEntityCollectionSimple.EntityName) as IEntity;

                return this.emptyEntityOfTypeToDisplay;
            }
            set
            {
                this.emptyEntityOfTypeToDisplay = value;
            }
        }

        private bool PersistAutoFiltersToSession
        {
            get
            {
                if (this.ParentAsGridViewEntityCollectionReady is UserControl)
                {
                    // SubPanel
                    return Dionysos.ConfigurationManager.GetBool(DevExConfigConstants.PersistAutoFiltersToSessionForUserControls);
                }
                else if (this.ParentAsGridViewEntityCollectionReady is PageLLBLOverviewDataSourceCollection)
                {
                    // Overview page
                    return Dionysos.ConfigurationManager.GetBool(DevExConfigConstants.PersistAutoFiltersToSessionForCollectionPages);
                }
                else
                {
                    return false;
                }
            }
        }

        private IGridViewEntityCollectionReady GetParentAsIGridViewEntityCollectionReady(Control c)
        {
            IGridViewEntityCollectionReady toReturn = c as IGridViewEntityCollectionReady;
            if (toReturn != null)
                return toReturn;

            if (c.Parent != null)
                return this.GetParentAsIGridViewEntityCollectionReady(c.Parent);
            else
                return null;
        }

        public bool PreventSaveColumnOrder { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when loading is completed.
        /// </summary>
        public event EventHandler LoadComplete;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GridViewEntityCollection"/> class.
        /// </summary>
        public GridViewEntityCollection()
        {
            this.HookUpEvents();
            this.InitializeSettings();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Grids the view entity collection_ client layout.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        protected void GridViewEntityCollection_ClientLayout(object sender, ASPxClientLayoutArgs e)
        {
            if ((this.Page.IsCallback || this.Page.IsPostBack) && e.LayoutMode == ClientLayoutMode.Saving && !this.PreventSaveColumnOrder)
            {
                this.SaveColumnOrder();
            }
        }

        /// <summary>
        /// Handles the CustomButtonCallback event of the GridViewEntityCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ASPxGridViewCustomButtonCallbackEventArgs"/> instance containing the event data.</param>
        protected void GridViewEntityCollection_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            string mode = "";
            if (e.ButtonID.Contains("editButton"))
            {
                mode = "edit";
            }
            else if (e.ButtonID.Contains("deleteButton"))
            {
                mode = "delete";
            }

            string url = string.Format("{0}?mode={1}&entity={2}&id=", this.ParentAsEntityCollectionSimple.EntityPageUrl, mode, this.ParentAsEntityCollectionSimple.EntityName);
            string id = this.GetRowValues(e.VisibleIndex, this.EmptyEntityOfTypeToDisplay.PrimaryKeyFields[0].Name).ToString();
            url = url + id;

            ASPxWebControl.RedirectOnCallback(this.Page.ResolveUrl(url));
        }

        /// <summary>
        /// Handles the CustomColumnDisplayText event of the GridViewEntityCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ASPxGridViewColumnDisplayTextEventArgs"/> instance containing the event data.</param>
        protected void GridViewEntityCollection_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            // DELETE AS HYPERLINK, WORKS BUT IS TRICKY BECAUSE CONFIRMATION DOES NOT SHOW WHEN CLICKED WITH SCROLLWHEEL (NEW TAB)
            if (e.Column.Name == GridViewEntityCollection.HyperLinkDeleteColumnName)
            {
                string resolvedEntityUrl = this.Page.ResolveUrl(this.ParentAsEntityCollectionSimple.EntityPageUrl);
                e.EncodeHtml = false;
                e.DisplayText = String.Format("<a class=\"dxeHyperlink_gridviewdeletebutton\" href=\"{0}?mode=delete&id={1}&ReturnUrl={2}\" onclick=\"return confirm('Are you sure you want to delete this line (and related details)?')\"> </a>",
                    resolvedEntityUrl, e.Value, this.Page.Server.UrlEncode(ResolveUrl(this.Request.Url.ToString().Replace("http", "https"))));
            }
        }

        /// <summary>
        /// Handles the HtmlRowCreated event of the GridViewEntityCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ASPxGridViewTableRowEventArgs"/> instance containing the event data.</param>
        protected void GridViewEntityCollection_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (this.EnableRowDoubleClick && e.RowType == GridViewRowType.Data && this.SettingsEditing.Mode != GridViewEditingMode.Inline)
            {
                string url = this.ResolveUrl(string.Format("{0}?id={1}", this.ParentAsEntityCollectionSimple.EntityPageUrl, e.GetValue(this.EmptyEntityOfTypeToDisplay.PrimaryKeyFields[0].Name)));
                e.Row.Attributes.Add("onDblclick", string.Format("location.href='{0}';", url));
            }
        }

        /// <summary>
        /// Handles the HtmlRowPrepared event of the GridViewEntityCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ASPxGridViewTableRowEventArgs"/> instance containing the event data.</param>
        protected void GridViewEntityCollection_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (this.HasArchivedColumn && e.GetValue("Archived") != null && (bool)e.GetValue("Archived"))
            {
                e.Row.CssClass += " archived";
            }
        }

        /// <summary>
        /// Handles the Load event of the GridViewEntityCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void GridViewEntityCollection_Load(object sender, EventArgs e)
        {
            this.ConfigureGrid();
            this.AddColumns();
            this.ConfigureColumns();
            this.AddAutoFilteringFromSession();

            LoadComplete?.Invoke(this, e);
        }

        /// <summary>
        /// Handles the PreRender event of the GridViewEntityCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void GridViewEntityCollection_PreRender(object sender, EventArgs e)
        {
            if (!this.Page.IsCallback && !this.Page.IsPostBack && this.SortCount == 0)
            {
                try
                {
                    var items = this.ParentAsGridViewEntityCollectionReady.SelectedView.ViewItems as IEnumerable<IViewItem>;

                    var itemWithLowestDisplayPosition = items.OrderBy(i => i.DisplayPosition).FirstOrDefault();
                    var matchingColumn = Columns[itemWithLowestDisplayPosition.FieldName];

                    if (matchingColumn != null)
                    {
                        this.SortBy(matchingColumn, ColumnSortOrder.Ascending);
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Handles the ProcessColumnAutoFilter event of the GridViewEntityCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ASPxGridViewAutoFilterEventArgs"/> instance containing the event data.</param>
        protected void GridViewEntityCollection_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
        {
            if (this.PersistAutoFiltersToSession)
            {
                if (e.Kind == GridViewAutoFilterEventKind.ExtractDisplayText)
                {
                    if (e.Value.Length > 0)
                    {
                        if (this.AutoFilters.ContainsKey(e.Column.Name))
                            this.AutoFilters[e.Column.Name] = e.Value;
                        else
                            this.AutoFilters.Add(e.Column.Name, e.Value);
                    }
                    else
                    {
                        this.AutoFilters.Remove(e.Column.Name);
                    }
                }
                else if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria &&
                    e.Value.Length == 0)
                {
                    this.AutoFilters.Remove(e.Column.Name);
                }
            }
        }

        protected void GridViewEntityCollection_DataBound(object sender, EventArgs e)
        {
            this.Settings.ShowFilterBar = this.PageCount > 1
                ? GridViewStatusBarMode.Visible
                : GridViewStatusBarMode.Auto;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a column to the gridview for the specified fieldname
        /// </summary>
        /// <param name="viewItem">The IViewItem instance to create a column for</param>
        /// <exception cref="Dionysos.TechnicalException">Er moet nog een datasource op de pagina worden aangemaakt van type: {0} met ID: {1} voor het gerelateerde veld. Dit is een beperking in de de GridViewentityCollection die deze datasource nog niet zelf aan kan maken!</exception>
        public void AddColumn(IViewItem viewItem)
        {
            if (this.Columns[viewItem.FieldName] != null)
            {
                // Already existing, don't add
                return;
            }

            if (viewItem.FieldName.Contains(".") && this.SettingsEditing.Mode == GridViewEditingMode.Inline)
            {
                // Related field with ComboBox for InlineEdit

                GridViewDataComboBoxColumn comboBoxColumn = new GridViewDataComboBoxColumn();

                // Zet de standaard waarden
                this.ConfigureColumnByViewItem(viewItem, comboBoxColumn);

                // GK TODO: In ViewItems opnemen wat het echte veld is waar gelinkt naar moet worden, nu gok ik gewoon op FK
                string relatedEntityName = StringUtil.GetAllBeforeFirstOccurenceOf(viewItem.FieldName, '.', true);
                IEntity relatedEntity = Dionysos.DataFactory.EntityFactory.GetEntity(relatedEntityName) as IEntity;
                IEntityField relatedEntityPKField = LLBLGenEntityUtil.GetPkFieldInfo(relatedEntity);

                // change fieldname, since it will contain something like: relatedentity.fieldname
                comboBoxColumn.FieldName = relatedEntityPKField.Name;

                // Set DataSource and Configure Combobox
                string dataSourceId = this.ID + "." + viewItem.FieldName;

                if (this.Page.FindControl(dataSourceId) == null)
                {
                    throw new Dionysos.TechnicalException("Er moet nog een datasource op de pagina worden aangemaakt van type: {0} met ID: {1} voor het gerelateerde veld. Dit is een beperking in de de GridViewentityCollection die deze datasource nog niet zelf aan kan maken!",
                        relatedEntityName, dataSourceId);
                }

                // Set the ComboBox
                comboBoxColumn.PropertiesComboBox.DataSourceID = dataSourceId;
                comboBoxColumn.PropertiesComboBox.TextField = StringUtil.GetAllAfterFirstOccurenceOf(viewItem.FieldName, '.');
                comboBoxColumn.PropertiesComboBox.ValueField = relatedEntityPKField.Name;
                comboBoxColumn.PropertiesComboBox.ValueType = typeof(int);

                this.Columns.Add(comboBoxColumn);
            }
            else if (viewItem.Type.Length > 0)
            {
                switch (viewItem.Type)
                {
                    case "System.Boolean":
                        GridViewDataCheckColumn checkCol = new GridViewDataCheckColumn();
                        this.ConfigureColumnByViewItem(viewItem, checkCol);
                        this.Columns.Add(checkCol);
                        break;

                    case "System.DateTime":
                        GridViewDataDateColumn dateCol = new GridViewDataDateColumn();
                        this.ConfigureColumnByViewItem(viewItem, dateCol);
                        this.Columns.Add(dateCol);
                        break;

                    case "System.Int32":
                        GridViewDataTextColumn intCol = new GridViewDataTextColumn();
                        this.ConfigureColumnByViewItem(viewItem, intCol);
                        if (intCol.PropertiesEdit.DisplayFormatString.Length == 0)
                            intCol.PropertiesEdit.DisplayFormatString = "N0";
                        intCol.PropertiesEdit.Style.HorizontalAlign = HorizontalAlign.Right;
                        this.Columns.Add(intCol);
                        break;

                    case "System.Decimal":
                        GridViewDataTextColumn decimalCol = new GridViewDataTextColumn();
                        this.ConfigureColumnByViewItem(viewItem, decimalCol);
                        if (decimalCol.PropertiesEdit.DisplayFormatString.Length == 0)
                            decimalCol.PropertiesEdit.DisplayFormatString = "N2";
                        decimalCol.PropertiesEdit.Style.HorizontalAlign = HorizontalAlign.Right;
                        this.Columns.Add(decimalCol);
                        break;

                    default:
                        GridViewDataTextColumn textCol = new GridViewDataTextColumn();
                        this.ConfigureColumnByViewItem(viewItem, textCol);
                        this.Columns.Add(textCol);
                        break;
                }
            }
            else
            {
                GridViewDataTextColumn textCol = new GridViewDataTextColumn();
                this.ConfigureColumnByViewItem(viewItem, textCol);

                this.Columns.Add(textCol);
            }
        }

        /// <summary>
        /// Add the to be shown columns of Entity to the Gridview
        /// </summary>
        /// <exception cref="TechnicalException">There are no view items available to show on the gridview. Check whether the field 'AllowShowOnGridView' is specified in the entity field information!</exception>
        /// <exception cref="EmptyException">Variable 'viewItem' is empty.</exception>
        public void AddEntitySpecificColumnsToGrid()
        {
            if (this.ParentAsGridViewEntityCollectionReady.SelectedView == null)
            {
                return;
            }

            if (this.ViewState == null)
            {
                return;
            }

            IEntityCollection viewItems = this.ParentAsGridViewEntityCollectionReady.SelectedView.ViewItems;
            if (viewItems.Count == 0)
            {
                throw new TechnicalException("There are no view items available to show on the gridview. Check whether the field 'AllowShowOnGridView' is specified in the entity field information!");
            }

            SortExpression sortViewItems = new SortExpression(new SortClause(viewItems[0].Fields["DisplayPosition"], SortOperator.Ascending));
            var viewItemsView = viewItems.DefaultView;
            viewItemsView.Sorter = sortViewItems;
            for (int i = 0; i < viewItemsView.Count; i++)
            {
                IViewItem viewItem = viewItemsView[i] as IViewItem;
                if (Instance.Empty(viewItem))
                {
                    throw new EmptyException("Variable 'viewItem' is empty.");
                }
                else
                {
                    if (viewItem.ShowOnGridView)
                    {
                        this.AddColumn(viewItem);
                    }
                }
            }
        }

        /// <summary>
        /// Resets the auto filter.
        /// </summary>
        public void ResetAutoFilter()
        {
            this.AutoFilters.Clear();
            for (int i = 0; i < this.Columns.Count; i++)
            {
                GridViewColumn current = this.Columns[i];
                GridViewDataColumn currentDataCol = current as GridViewDataColumn;
                if (currentDataCol != null)
                {
                    currentDataCol.AutoFilterBy(string.Empty);
                }
            }
        }

        /// <summary>
        /// Saves the current column order to the database
        /// </summary>
        public void SaveColumnOrder()
        {
            foreach (IViewItem viewItem in this.ParentAsGridViewEntityCollectionReady.SelectedView.ViewItems)
            {
                GridViewDataColumn column = this.Columns[viewItem.FieldName] as GridViewDataColumn;
                if (column != null)
                {
                    viewItem.DisplayPosition = column.VisibleIndex;
                    viewItem.SortOperator = (int)column.SortOrder;
                    viewItem.SortPosition = (column.SortOrder == ColumnSortOrder.None) ? 0 : column.SortIndex;
                }
            }

            this.ParentAsGridViewEntityCollectionReady.SelectedView = ViewUtil.SaveCustomViewUpdates(this.ParentAsGridViewEntityCollectionReady.SelectedView, UserManager.CurrentUser.UserId);
        }

        /// <summary>
        /// Adds the auto filtering from session.
        /// </summary>
        private void AddAutoFilteringFromSession()
        {
            if (!this.PersistAutoFiltersToSession)
            {
                return;
            }

            // Set auto filter values from session
            if (!this.Page.IsPostBack && !this.Page.IsCallback)
            {
                // Copy to list, because operations might be performend on the AutoFilters dict. and that doesn't work using foreach
                var tempList = this.AutoFilters.ToList();
                foreach (KeyValuePair<string, string> pair in tempList)
                {
                    if (this.Columns[pair.Key] != null)
                        this.AutoFilterByColumn(this.Columns[pair.Key], pair.Value);
                }
            }
        }

        /// <summary>
        /// Adds the columns to the gridview
        /// </summary>
        private void AddColumns()
        {
            // The command colums is used by inline
            // because it's the 0 VisibleIndex it should be added first
            // if not, other columns won't get a higher visibleindex than 0 unless there are columsn
            this.AddCommandColumns();

            if (this.Columns.Count == 0 ||
                (this.Columns.Count == 1 && this.SettingsEditing.Mode == GridViewEditingMode.Inline))
            {
                if (this.GenerateColumnsFromEntityInformation)
                {
                    this.AddEntitySpecificColumnsToGrid();
                }

                // These are always last, and they need the hightest VisibleIndex, therefor they
                // are rendered after the entity columns
                this.AddHyperlinkColumns();
            }

            // With inline we always have more than 0 columns and require to add this on postback:
            if (this.ShowAddNewButton)
            {
                // Append to command column
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    object current = this.Columns[i];
                    if (current.GetType() == typeof(GridViewCommandColumn))
                    {
                        ((GridViewCommandColumn)current).HeaderTemplate = new AddNewRowColumnHeaderTemplate(this);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the command columns to the gridview for editing and deleting
        /// </summary>
        private void AddCommandColumns()
        {
            bool commandsNeeded = false;
            bool commandColumnAlreadyExisting = false;

            GridViewCommandColumn commands;
            if (this.Columns[CommandsColumnName] == null)
            {
                commands = new GridViewCommandColumn("&nbsp;");
                commands.Name = GridViewEntityCollection.CommandsColumnName;
                commands.VisibleIndex = 0;
                commands.AllowDragDrop = DefaultBoolean.False;
                commands.ButtonRenderMode = GridCommandButtonRenderMode.Image;
                commands.Width = Unit.Pixel(35);
                this.Styles.CommandColumn.Spacing = Unit.Pixel(10);
            }
            else
            {
                commands = this.Columns[CommandsColumnName] as GridViewCommandColumn;
                commandColumnAlreadyExisting = true;
            }

            if (this.SettingsEditing.Mode == GridViewEditingMode.Inline)
            {
                if (ShowEditColumn)
                {
                    commands.ShowEditButton = true;
                    this.SettingsCommandButton.EditButton.Image.Url = "~/images/icons/table_edit.png";
                    commandsNeeded = true;
                }

                if (ShowDeleteColumn)
                {
                    commands.ShowDeleteButton = true;
                    this.SettingsCommandButton.DeleteButton.Image.Url = "~/images/icons/delete.png";
                    commandsNeeded = true;
                }

                this.SettingsBehavior.ConfirmDelete = true;
                this.SettingsText.ConfirmDelete = string.Format("Are you sure you want to delete this line?");

                commands.ShowCancelButton = true;
                this.SettingsCommandButton.CancelButton.Image.Url = "~/images/icons/cross_grey.png";

                commands.ShowUpdateButton = true;
                this.SettingsCommandButton.UpdateButton.Image.Url = "~/images/icons/disk.png";

                commands.Width = Unit.Pixel(75);
            }
            else
            {
                if (ShowEditColumn)
                {
                    var editButton = new GridViewCommandColumnCustomButton();
                    editButton.ID = this.ID + "editButton";
                    editButton.Text = "Bewerken";
                    editButton.Visibility = GridViewCustomButtonVisibility.BrowsableRow;
                    editButton.Image.Url = "~/images/icons/ico_edit.gif";
                    editButton.Image.AlternateText = "Bewerken";
                    commands.CustomButtons.Add(editButton);
                    commandsNeeded = true;
                    /*commands.EditButton.Visible = true;
					commands.EditButton.Text = "";
					commands.EditButton.Image.Url = "~/images/icons/ico_edit.gif";*/
                }

                if (ShowDeleteColumn)
                {
                    var deleteButton = new GridViewCommandColumnCustomButton();
                    deleteButton.ID = this.ID + "deleteButton";
                    deleteButton.Text = "Verwijderen";
                    deleteButton.Visibility = GridViewCustomButtonVisibility.BrowsableRow;
                    deleteButton.Image.Url = "~/images/icons/delete.png";
                    deleteButton.Image.AlternateText = "Verwijderen";
                    commands.CustomButtons.Add(deleteButton);
                    commandsNeeded = true;
                    /*commands.DeleteButton.Text = "";
					commands.DeleteButton.Visible = true;
					commands.DeleteButton.Image.Url = "~/images/icons/ico_del.gif";*/
                }

                if (ShowDeleteColumn && ShowEditColumn)
                {
                    commands.Width = Unit.Pixel(60);
                }
            }

            if (commandsNeeded && !commandColumnAlreadyExisting)
            {
                this.Columns.Add(commands);
            }
        }

        /// <summary>
        /// Adds the hyperlink columns.
        /// </summary>
        private void AddHyperlinkColumns()
        {
            if (this.ShowEditHyperlinkColumn)
            {
                string colName = GridViewEntityCollection.HyperLinkEditColumnName;
                if (this.Columns[colName] == null)
                {
                    GridViewDataHyperLinkColumn col = this.CreateLinkColumn(PageMode.Edit);
                    col.Name = colName;
                    col.Settings.AllowSort = DefaultBoolean.False;
                    col.Settings.AllowDragDrop = DefaultBoolean.False;
                    col.PropertiesHyperLinkEdit.TextFormatString = "&nbsp;";

                    //col.PropertiesHyperLinkEdit.NavigateUrlFormatString = string.Format("javascript:{0}redirectToEditPage({{0}});", col.FieldName);
                    col.PropertiesHyperLinkEdit.NavigateUrlFormatString = string.Format("{0}?id={{0}}", this.ParentAsEntityCollectionSimple.EntityPageUrl, col.FieldName);
                    col.PropertiesHyperLinkEdit.CssPostfix = "gridvieweditbutton";

                    col.Caption = string.Empty;
                    col.Caption = "&nbsp;";
                    col.VisibleIndex = this.GetHighestVisibleIndex();
                    col.Width = Unit.Pixel(16);
                    this.Columns.Add(col);
                }
            }

            if (this.ShowDeleteHyperlinkColumn)
            {
                string colName = GridViewEntityCollection.HyperLinkDeleteColumnName;
                if (this.Columns[colName] == null)
                {
                    GridViewDataColumn col = new GridViewDataColumn();
                    col.Name = colName;
                    col.FieldName = this.EmptyEntityOfTypeToDisplay.PrimaryKeyFields[0].Name;
                    col.UnboundType = UnboundColumnType.Bound;
                    col.Caption = "&nbsp;";
                    col.Settings.AllowSort = DefaultBoolean.False;
                    col.Settings.AllowDragDrop = DefaultBoolean.False;
                    col.VisibleIndex = this.GetHighestVisibleIndex();
                    col.Width = Unit.Pixel(16);
                    this.Columns.Add(col);

                    //GridViewDataHyperLinkColumn col = this.CreateLinkColumn(PageMode.Delete);
                    //col.Name = colName;
                    //col.Settings.AllowSort = DefaultBoolean.False;
                    //col.PropertiesHyperLinkEdit.TextFormatString = "&nbsp;";
                    //col.PropertiesHyperLinkEdit.NavigateUrlFormatString = string.Format("javascript:{0}redirectToDeletePage({{0}});", col.FieldName);
                    //col.PropertiesHyperLinkEdit.CssPostfix = "gridviewdeletebutton";
                    //col.Caption = string.Empty;
                    //col.Caption = "&nbsp;";
                    //col.VisibleIndex = this.GetHighestVisibleIndex();
                    //col.Width = Unit.Pixel(16);
                    //this.Columns.Add(col);
                }
            }
        }

        /// <summary>
        /// Configures the column by view item.
        /// </summary>
        /// <param name="viewItem">The view item.</param>
        /// <param name="column">The column.</param>
        /// <exception cref="FunctionalException">Field / property '{0}' could not be found on Entity '{1}'. Do you have the latest version of the Data project and has it been recompiled?</exception>
        /// <exception cref="TechnicalException">An error occured when trying to configure the column for '{0}' based on the entity information, error: {1}</exception>
        private void ConfigureColumnByViewItem(IViewItem viewItem, GridViewDataColumn column)
        {
            try
            {
                column.FieldName = viewItem.FieldName;
                column.Caption = GetColumnCaption(viewItem);
                column.Name = viewItem.FieldName;
                column.VisibleIndex = viewItem.DisplayPosition;

                ColumnSortOrder sortOrder = (ColumnSortOrder)viewItem.SortOperator;
                if (sortOrder != ColumnSortOrder.None)
                {
                    column.SortIndex = viewItem.SortPosition;
                    column.SortOrder = sortOrder;
                }

                if (viewItem.Width.Length > 0)
                {
                    column.Width = new Unit(viewItem.Width);
                }

                column.Visible = true;

                if (viewItem.DisplayFormatString.Length > 0)
                {
                    column.PropertiesEdit.DisplayFormatString = viewItem.DisplayFormatString;
                }
            }
            catch (Exception ex)
            {
                throw new TechnicalException(ex, "An error occured when trying to configure the column for '{0}' based on the entity information, error: {1}", viewItem.FieldName, ex.Message);
            }
        }

        private static string GetColumnCaption(IViewItem viewItem)
        {
            IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(viewItem.EntityName);

            if (entityInformation.AllFields.TryGetValue(viewItem.FieldName, out IEntityFieldInformation entityFieldInformation))
            {
                // Use entity information, so even related enities can have a custom name
                return entityFieldInformation.FriendlyName;
            }
            else
            {
                // No entity information found, related field or invalid/outdated entity information
                if (viewItem.FieldName.Contains('.'))
                {
                    return viewItem.GetFriendlyNameForRelatedField();
                }
                else
                {
                    if (!entityInformation.AllFields.ContainsKey(viewItem.FieldName))
                    {
                        throw new FunctionalException("Field / property '{0}' could not be found on the entity field information of entity '{1}'. Do you have the latest version of the Data project and has it been recompiled and is the entity information up-to-date?", viewItem.FieldName, viewItem.EntityName);
                    }

                    return viewItem.FieldName;
                }
            }
        }

        /// <summary>
        /// Configures the columns by setting the default settings like filtering
        /// </summary>
        private void ConfigureColumns()
        {
            int columnCount = this.Columns.Count;
            if (this.ShowEditHyperlinkColumn)
                columnCount--;
            if (this.ShowDeleteHyperlinkColumn)
                columnCount--;

            for (int i = 0; i < this.Columns.Count; i++)
            {
                GridViewColumn current = this.Columns[i];

                //current.VisibleIndex = current.VisibleIndex + 10; // ? maybe for the button columns ?
                GridViewDataColumn currentDataCol = current as GridViewDataColumn;
                if (currentDataCol != null)
                {
                    currentDataCol.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                    currentDataCol.Settings.FilterMode = ColumnFilterMode.DisplayText;

                    if (this.IsChrome)
                    {
                        if (currentDataCol.Name.Equals(GridViewEntityCollection.HyperLinkDeleteColumnName))
                            currentDataCol.Width = Unit.Pixel(29);
                        else if (currentDataCol.Name.Equals(GridViewEntityCollection.HyperLinkEditColumnName))
                            currentDataCol.Width = Unit.Pixel(29);
                        else if (columnCount > 0)
                            currentDataCol.Width = Unit.Percentage(100 / columnCount);
                    }
                }
            }
        }

        /// <summary>
        /// Configures the grid by settings the defaults settings
        /// </summary>
        private void ConfigureGrid()
        {
            this.Width = Unit.Percentage(100);

            this.SettingsBehavior.AutoFilterRowInputDelay = Dionysos.ConfigurationManager.GetInt(DevExConfigConstants.FilterRowInputDelay);
            this.Settings.ShowFilterRow = true;
            if (this.SettingsText.Title.Length > 0)
                this.Settings.ShowTitlePanel = true;
        }

        /// <summary>
        /// Creates a GridView column to link to an edit page in a certain pageMode
        /// </summary>
        /// <param name="pageMode">PageMode to link to</param>
        /// <returns>
        /// GridviewColumn
        /// </returns>
        public GridViewDataHyperLinkColumn CreateLinkColumn(PageMode pageMode)
        {
            GridViewDataHyperLinkColumn toReturn = new GridViewDataHyperLinkColumn();

            toReturn.Name = this.ID + pageMode;
            toReturn.FieldName = this.EmptyEntityOfTypeToDisplay.PrimaryKeyFields[0].Name;
            toReturn.UnboundType = UnboundColumnType.Bound;
            string pageModeText = "";
            switch (pageMode)
            {
                case PageMode.None:
                    pageModeText = "";
                    break;
                case PageMode.Add:
                    pageModeText = "add";
                    break;
                case PageMode.View:
                    pageModeText = "view";
                    break;
                case PageMode.Edit:
                    pageModeText = "edit";
                    break;
                case PageMode.Delete:
                    pageModeText = "delete";
                    break;
                default:
                    break;
            }

            string url = string.Format("{0}?mode={1}&entity={2}&id=", this.ParentAsEntityCollectionSimple.EntityPageUrl, pageModeText, this.ParentAsEntityCollectionSimple.EntityName);
            toReturn.PropertiesHyperLinkEdit.NavigateUrlFormatString = url + "{0}";

            return toReturn;
        }

        /// <summary>
        /// Gets the index of the highest visible.
        /// </summary>
        /// <returns></returns>
        private int GetHighestVisibleIndex()
        {
            int highest = 0;
            foreach (GridViewColumn col in this.Columns)
            {
                if (col.VisibleIndex > highest)
                    highest = col.VisibleIndex;
            }
            return highest;
        }

        /// <summary>
        /// Recursivley find the first parent of the gridview that's a ParentAsEntityCollectionOverview
        /// </summary>
        /// <param name="control">Control to test being a ParentAsEntityCollectionOverview</param>
        /// <returns>
        /// First found ParentAsEntityCollectionOverview
        /// </returns>
        /// <exception cref="TechnicalException">GridViewentityCollection (Id:{0}) moet een parent hebben van het type IGuiEntityCollectionSimple, deze is niet gevonden</exception>
        private IGuiEntityCollectionSimple GetParentGuiEntityCollectionSimple(Control control)
        {
            // First try to cast as the interface
            IGuiEntityCollectionSimple controlAsOverview = control as IGuiEntityCollectionSimple;

            // If null try to find it's parent
            if (controlAsOverview == null)
            {
                if (control.Parent == null)
                {
                    // No parent, error!
                    throw new TechnicalException("GridViewentityCollection (Id:{0}) moet een parent hebben van het type IGuiEntityCollectionSimple, deze is niet gevonden", this.ID);
                }
                else
                {
                    // Try the parent
                    controlAsOverview = this.GetParentGuiEntityCollectionSimple(control.Parent);
                }
            }

            // Return the parent
            return controlAsOverview;
        }

        /// <summary>
        /// Hooks up events.
        /// </summary>
        private void HookUpEvents()
        {
            this.Load += new EventHandler(GridViewEntityCollection_Load);
            this.CustomButtonCallback += new ASPxGridViewCustomButtonCallbackEventHandler(GridViewEntityCollection_CustomButtonCallback);
            this.ClientLayout += new ASPxClientLayoutHandler(GridViewEntityCollection_ClientLayout);
            this.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(GridViewEntityCollection_CustomColumnDisplayText);
            this.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(GridViewEntityCollection_HtmlRowPrepared);
            this.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(GridViewEntityCollection_HtmlRowCreated);
            this.ProcessColumnAutoFilter += new ASPxGridViewAutoFilterEventHandler(GridViewEntityCollection_ProcessColumnAutoFilter);
            this.PreRender += new EventHandler(GridViewEntityCollection_PreRender);
            this.DataBound += GridViewEntityCollection_DataBound;
        }

        /// <summary>
        /// Initializes the settings.
        /// </summary>
        private void InitializeSettings()
        {
            int pageSize = ConfigurationManager.GetInt(DionysosWebConfigurationConstants.PageSize);
            if (pageSize <= 0)
            {
                this.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
            }
            else
            {
                this.SettingsPager.PageSize = pageSize;
            }

            this.SettingsFilterControl.ViewMode = FilterControlViewMode.VisualAndText;
            this.SettingsFilterControl.ShowAllDataSourceColumns = false;

            this.SettingsBehavior.EnableRowHotTrack = true;
            this.ClientSideEvents.ColumnResized = "function(s, e) { e.processOnServer = true; }";
        }

        #endregion
    }

    public class PaddedNumberComparer : System.Collections.Generic.IComparer<IViewItem>
    {
        #region IComparer<IViewItem> Members

        /// <summary>
        /// Compares the specified x.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        int IComparer<IViewItem>.Compare(IViewItem x, IViewItem y)
        {
            string paddedx = x.DisplayPosition.ToString().PadLeft(25, '0');
            string paddedy = y.DisplayPosition.ToString().PadLeft(25, '0');
            return paddedx.CompareTo(paddedy);
        }

        #endregion
    }
}