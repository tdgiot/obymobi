﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
	public static class GridViewExporterUtil
	{
		public static void SetDefaultSettings(this ASPxGridViewExporter exporter)
		{
			exporter.Styles.Default.Font.Names = new string[]{"Arial", "Verdana", "SansSerif"};
			exporter.Styles.Default.Font.Size = new System.Web.UI.WebControls.FontUnit(System.Web.UI.WebControls.Unit.Point(11));			
		}
	}
}
