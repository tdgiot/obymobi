﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Interfaces.Data;
using System.Collections;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Dionysos.Data;
using Dionysos.Web.Security;
using System.Web.UI;
using System.Threading;
using ASPxGridView = DevExpress.Web.ASPxGridView;
using System.Web;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Gridview object, inheritance of ASPxGridView.
	/// </summary>
	public class GridView : ASPxGridView
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="GridView"/> class.
		/// </summary>
		public GridView()
		{
			this.SetDefaults();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the defaults.
		/// </summary>
		private void SetDefaults()
		{
			if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.Equals("nl", StringComparison.OrdinalIgnoreCase))
			{                
				this.SettingsText.CommandCancel = "Cancel";
				this.SettingsText.CommandClearFilter = "Clear filter";
				this.SettingsText.CommandDelete = "Delete";
				this.SettingsText.CommandEdit = "Edit";
				this.SettingsText.CommandNew = "Add";
				this.SettingsText.CommandSelect = "Select";
				this.SettingsText.CommandUpdate = "Save";
				this.SettingsText.GroupPanel = "Drag column titles to regroup";
				this.SettingsText.GroupContinuedOnNextPage = "Continues on the next page";

				this.SettingsText.EmptyDataRow = "Empty results";

				this.SettingsPager.Summary.Text = "Page {0} from {1} ({2} items)";
				this.SettingsPager.Summary.AllPagesText = "Pages: {0} - {1} ({2} items)";
			}

            // Fix for Chrome browser, see http://blog.zerosharp.com/how-to-improve-xaf-grid-layout-for-chrome/
            this.Settings.UseFixedTableLayout = this.IsChrome;

			if (ConfigurationManager.GetBool(DevExConfigConstants.GridViewUsePaging)) this.SettingsPager.Mode = GridViewPagerMode.ShowPager;
			else this.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;

			this.EnableCallBacks = ConfigurationManager.GetBool(DevExConfigConstants.EnableCallbacksForControls);
		}

		#endregion

        #region Properties

        public bool IsChrome
        {
            get
            {
                return (HttpContext.Current != null && HttpContext.Current.Request.Browser.Browser == "Chrome");
            }
        }

        #endregion
    }
}
