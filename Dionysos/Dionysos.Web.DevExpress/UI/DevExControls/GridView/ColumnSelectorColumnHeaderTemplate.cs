﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Header Template Containing a button for selecting columns
	/// </summary>
	public class ColumnSelectorColumnHeaderTemplate : ITemplate
	{
		#region Fields

		ASPxGridView gridView = null;
        HyperLink hyperLink = null;

		#endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.DevExControls.ColumnSelectorColumnHeaderTemplate type
        /// </summary>
        /// <param name="gridView">The ASPxGridView instance which contains the header</param>
        public ColumnSelectorColumnHeaderTemplate(ASPxGridView gridView, HyperLink hyperLink)
        {
            this.gridView = gridView;
            this.hyperLink = hyperLink;			
        }		

        #endregion

        #region Methods

        public void InstantiateIn(Control container)
		{
			container.Controls.Add(this.hyperLink);
		}

		#endregion

		#region Properties

		#endregion
	}
}
