﻿using System;
using Dionysos.Interfaces;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;
using System.ComponentModel;
using DevExpress.Web;
using Dionysos.Globalization;

namespace Dionysos.Web.UI.DevExControls
{
	/// <summary>
	/// Dionysos implementation of the ASPxTextBox
	/// </summary>
	public class Button : ASPxButton, ILocalizableControl
	{
		#region Localization Logic

		private bool localizeText = true;

		/// <summary>
		/// Gets / Sets if the control should be localized
		/// </summary>
		[Category("Localization"), DefaultValue(true)]
		public bool LocalizeText
		{
			get { return this.localizeText; }
			set { this.localizeText = value; }
		}

		private string translationTagText = String.Empty;

		/// <summary>
		/// Gets / sets the control's TranslationTag for the Text property, when set this will be used a the TranslationKey instead of the Page's Namespace + Control.ID
		/// 
		/// </summary>
		[Category("Localization"), DefaultValue("")]
		public string TranslationTagText
		{
			get { return this.translationTagText; }
			set { this.translationTagText = value; }
		}

		private Dictionary<string, Translatable> localizableProperties = null;

		/// <summary>
		/// Localization information per property
		/// </summary>
		/// <value></value>
		public virtual Dictionary<string, Translatable> TranslatableProperties
		{
			get
			{
				if (this.localizableProperties == null)
				{
					this.localizableProperties = new Dictionary<string, Translatable>();

					if (this.LocalizeText)
					{
						Translatable textProperty = new Translatable();
						textProperty.TranslationValue = this.Text;

						// Get translation key
						if (!String.IsNullOrEmpty(this.TranslationTagText))
						{
							textProperty.TranslationKey = this.TranslationTagText;
						}
						else if (!String.IsNullOrEmpty(this.ID))
						{
							textProperty.TranslationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID + ".Text";
						}
						else
						{
							throw new TechnicalException("A TranslationTag or ID is required for controls to be localized! Failed for control with Text: '{0}' - {1}", this.Text, this.ClientID);
						}

						this.localizableProperties.Add("Text", textProperty);
					}
				}

				return this.localizableProperties;
			}
			set
			{
				this.localizableProperties = value;

				// Set the translated values
				if (this.TranslatableProperties != null)
				{
					Translatable textProperty;
					if (this.TranslatableProperties.TryGetValue("Text", out textProperty))
					{
						this.Text = textProperty.TranslationValue;
					}
				}
			}
		}

		#endregion

	}
}
