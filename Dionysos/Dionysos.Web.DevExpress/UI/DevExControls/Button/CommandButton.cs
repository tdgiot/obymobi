﻿using System;
using Dionysos.Reflection;
using System.Web.UI;

namespace Dionysos.Web.UI.DevExControls
{
    /// <summary>
    /// Represents the button control.
    /// </summary>
    public class CommandButton : Button
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the CommandButton class with default settings.
        /// </summary>
        public CommandButton()
        {
            this.HookupEvents();
        }

        #endregion

        #region Methods

        private void HookupEvents()
        {
            this.Command += new System.Web.UI.WebControls.CommandEventHandler(CommandButton_Command);
        }

        #endregion

        #region Event handlers

        private bool ExecuteCommand(Control parent)
        {
            bool toReturn = false;
            if (Member.HasMember(parent, this.CommandName))
            {
                toReturn = true;
                if (this.CommandArgument.Length > 0)
                    Member.InvokeMethod(parent, this.CommandName, new object[] { this.CommandArgument });
                else
                    Member.InvokeMethod(parent, this.CommandName, new object[] { });
            }
            else if (parent.Parent != null)
                toReturn = ExecuteCommand(parent.Parent);
            else
                toReturn = false;
            return toReturn;
        }

        /// <summary>
        /// Raises the System.Web.UI.WebControls.Button.Command event from the System.Web.UI.WebControls.Button instance
        /// </summary>
        /// <param name="e">The System.Web.UI.WebControls.CommandEventArgs instance</param>
        private void CommandButton_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (Instance.Empty(this.CommandName))
            {
                // No command name specified
                throw new Dionysos.TechnicalException("No CommandName defined for '{0}'", this.ID);
            }
            else
            {
                if (!ExecuteCommand(this.Parent))
                {
                    throw new Dionysos.TechnicalException("CommandButton '{0}' fired unhandled Command '{1}'. Is the CommandName available as a Method? Is the Method modifier accessible (not private)?", this.ID, this.CommandName);
                }
            }
        }

        //private void CommandButton_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        //{
        //    if (Instance.Empty(this.CommandName))
        //    {
        //        // No command name specified
        //    }
        //    else
        //    {
        //        if (Member.HasMember(this.Page, this.CommandName))
        //        {
        //            if (e.CommandArgument.ToString().Length > 0)					
        //                Member.InvokeMethod(this.Page, this.CommandName, new object[] { e.CommandArgument });					
        //            else
        //                Member.InvokeMethod(this.Page, this.CommandName, new object[] { });
        //        }
        //    }
        //}

        #endregion
    }
}
