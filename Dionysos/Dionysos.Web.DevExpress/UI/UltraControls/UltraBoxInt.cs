﻿using Dionysos.Interfaces;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Dionysos.Web.UI.UltraControls
{
    /// <summary>
    /// Summary description for UltraBoxInt
    /// </summary>
    public class UltraBoxInt : Control, IBindable, INamingContainer
    {
        private ComboBoxInt devExControl;
        private DropDownListInt2 aspControl;
        private IUltraControl control;
        private UltraControlRenderer renderer;
        private System.Web.UI.WebControls.HiddenField valueField;
        private bool valueSet;

        public UltraBoxInt()
	    {
            //this.Renderer = UltraControlRenderer.NotSet;            
            this.EmptyItemValue = -1;
            this.UseDataBinding = true;
            this.valueSet = false;
	    }

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);
            this.EnsureChildControls();
            this.Page.Validators.Add(this.control);
            this.EnableViewState = false;
            this.Page.PreLoad += Page_PreLoad;                   
        }

        protected override void OnUnload(EventArgs e)
        {
            if (Page != null)
            {                
                Page.Validators.Remove(this.control);                                
            }
            base.OnUnload(e);
        }

        void Page_PreLoad(object sender, EventArgs e)
        {
            // Hack for DropDownListInt
            if (this.Renderer == UltraControlRenderer.AspNet)
            {
                if (this.aspControl.Items.Count == 0 && !this.aspControl.DefaultListItemText.IsNullOrWhiteSpace())
                {
                    this.aspControl.Items.Add(new System.Web.UI.WebControls.ListItem(this.aspControl.DefaultListItemText, "-1"));
                }
            }

            string uniqueId = this.aspControl != null ? this.aspControl.UniqueID : this.devExControl.UniqueID;
            // Directly get the value to return it in the case of a postback with a disabled control (since it won't be filled then)
            if (this.Page.IsPostBack &&
                /* If it doesn't contain the control for postback, that means it was disabled at postback and we'll reset it's value */
                !this.Page.Request.Form.AllKeys.Contains(this.aspControl.UniqueID) &&
                /* Ensure it wasn't set by some other logic */
                !this.valueSet &&
                this.Page.Request.Form[this.valueField.UniqueID] != null)
            {
                // Not using this.Value as it would give a rescursive loop
                int valueInt;
                if (int.TryParse(this.Page.Request.Form[this.valueField.UniqueID], out valueInt))
                {
                    if (this.aspControl != null)
                        this.aspControl.Value = valueInt;
                    else if (this.devExControl != null)
                        this.devExControl.Value = valueInt;
                }
            }
        }

        protected override void CreateChildControls()
        {
            int intValue;
            if(this.Renderer != UltraControlRenderer.NotSet)
            {
                intValue = (int)this.Renderer;
            }
            else
                intValue = Dionysos.ConfigurationManager.GetInt(Dionysos.Web.DionysosWebConfigurationConstants.UltraControlRenderer);           

            if (intValue == (int)UltraControlRenderer.AspNet)
            {
                this.aspControl = new DropDownListInt2();
                this.aspControl.ID = "sub" + this.ID;
                this.aspControl.ValueChanged += control_ValueChanged;
                this.aspControl.UseDataBinding = false;
                this.control = this.aspControl;
                this.renderer = UltraControlRenderer.AspNet;                     
            }
            else
            {
                this.devExControl = new ComboBoxInt();
                this.devExControl.ID = "sub" + this.ID;
                this.devExControl.ValueChanged += control_ValueChanged;
                this.devExControl.UseDataBinding = false;
                this.control = this.devExControl;                
                this.renderer = UltraControlRenderer.DevEx;
            }            

            this.Controls.Add((Control)this.control);

            this.valueField = new System.Web.UI.WebControls.HiddenField();            
            this.valueField.ID = "hf" + this.ID;
            this.Controls.Add(this.valueField);
        }

        public void InsertItem(int index, string text, int value)
        {         
            if (this.aspControl != null)
                this.aspControl.Items.Insert(index, new System.Web.UI.WebControls.ListItem(text, value.ToString()));
            else
                this.devExControl.Items.Insert(0, new global::DevExpress.Web.ListEditItem(text, value));
        }

        public void AddItem(string text, int value)
        {
            if (this.aspControl != null)
            {
                // Hack for DropDownListInt
                if (this.aspControl.Items.Count == 0 && !this.aspControl.DefaultListItemText.IsNullOrWhiteSpace())
                {
                    this.aspControl.Items.Add(new System.Web.UI.WebControls.ListItem(this.aspControl.DefaultListItemText, "-1"));
                }

                this.aspControl.Items.Add(new System.Web.UI.WebControls.ListItem(text, value.ToString()));
            }
            else
                this.devExControl.Items.Add(text, value);
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            if(!this.EmptyItemText.IsNullOrWhiteSpace())            
                this.InsertItem(0, this.EmptyItemText, this.EmptyItemValue);                            

            base.RenderChildren(writer);
        }

        public bool UseDataBinding { get; set; }

        public UltraControlRenderer Renderer { get; set; }

        public string EmptyItemText { get; set; }
        public int EmptyItemValue { get; set; }

        /// <summary>
        /// Bind an Entity Collection if you only want to add 'standard' database fields, not custom properties
        /// It's on purpose that there's is NO method do work with a PropertyName / Refelction because in such cases,
        /// for performance reasons it's better to manually iterate over the collection and add the items.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="textField"></param>
        /// <param name="valueField"></param>
        public void BindEntityCollection(IEntityCollection entities, EntityField textField, EntityField valueField)
        {
            int textIndex = (int)textField.FieldIndex;
            int valueIndex = (int)valueField.FieldIndex;
            bool toString = false;
            foreach (IEntity entity in entities)
            {
                try
                {
                    if(toString)
                        this.AddItem(entity.Fields[textIndex].CurrentValue.ToString(), (int)entity.Fields[valueIndex].CurrentValue);
                    else
                        this.AddItem((string)entity.Fields[textIndex].CurrentValue, (int)entity.Fields[valueIndex].CurrentValue);
                }
                catch
                {
                    // Cheapest, because we don't have to check it, unless it's actually needed.
                    // Otherwise I would have had to check the first entity for the type of it's textField.
                    if (!toString)
                        toString = true;
                    else
                        throw;
                }
            }

            this.SelectItemBasedOnQueryString();   
        }

        public void SelectItemBasedOnQueryString()
        {
            string fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
            // Changed: if (this.pageMode == PageMode.Add && this.ValidId <= 0 && QueryStringHelper.HasParameter(fieldName) && !this.Page.IsPostBack)
            // Removed: this.pageMode == PageMode.Add &&
            string fieldValue;
            if (this.ValidId <= 0 &&
                QueryStringHelper.TryGetValue(fieldName, out fieldValue) &&
                !this.Page.IsPostBack)
            {
                int fieldValueInt;
                if (Int32.TryParse(fieldValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out fieldValueInt))
                {
                    // Found a value
                    this.Value = fieldValueInt;
                }
            }
        }

        public int SelectedIndex
        {
            get
            {
                return this.control.SelectedIndex;
            }
            set
            {
                this.control.SelectedIndex = value;
            }
        }

        public bool IsRequired
        {
            get
            {
                this.EnsureChildControls();
                return this.control.IsRequired;
            }
            set
            {
                this.EnsureChildControls();
                this.control.IsRequired = value;
            }
        }

        public int ValidId
        {
            get
            {
                return this.Value ?? 0;
            }
        }

        public bool Enabled
        {
            get
            {                
                return this.control.Enabled;                
            }
            set
            {
                this.control.Enabled = value;
            }
        }

        public string SelectedItemText
        {
            get
            {
                if (this.aspControl != null)
                    return this.aspControl.SelectedItem.Text;
                else
                    return this.devExControl.SelectedItem.Text;
            }
        }

        public int? Value
        {
            get
            {
                this.EnsureChildControls();
                return this.control.Value;
            }
            set
            {
                this.EnsureChildControls();
                this.valueSet = true;
                if (this.aspControl != null)
                    this.aspControl.Value = value ?? -1;
                else
                    this.devExControl.Value = value;

                this.valueField.Value = (value != null) ? value.ToString() : string.Empty;
            }
        }

        /// <summary>
        /// Helper method to fire up the value changed event
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The EventArgs</param>
        public void OnValueChanged(object sender, EventArgs e)
        {
            if (this.ValueChanged != null)
            {
                this.ValueChanged(sender, e);
            }
        }

        void control_ValueChanged(object sender, EventArgs e)
        {
            this.OnValueChanged(sender, e);
        }

        /// <summary>
        /// Occurs when the value changed between posts to the server
        /// </summary>
        public event EventHandler ValueChanged;

    }
}