﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI.DevExControls
{
    public enum UltraControlRenderer : int
    { 
        NotSet = 0,
        AspNet = 1,
        DevEx = 2
    }
}
