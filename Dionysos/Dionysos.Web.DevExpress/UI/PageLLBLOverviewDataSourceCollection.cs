﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Data;
using DevExpress.Data.Linq;
using System.Reflection;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces.Data;
using Dionysos.Data;

namespace Dionysos.Web.UI
{
    public class PageLLBLOverviewDataSourceCollection : PageOverviewDataSourceCollection
    {
        #region Fields

        /// <summary>
        /// Includes or excludes certains properties for the GetMulti call (DataSource_PerformSelect)
        /// </summary>
        private ExcludeIncludeFieldsList excludeIncludeFieldsList = null;

        #endregion

        #region Properties

        /// <summary>
        /// Includes certains properties for the GetMulti call (DataSource_PerformSelect)
        /// </summary>
        protected IncludeFieldsList IncludeFieldsList = null;

        /// <summary>
        /// PrefetchPath for prefetching of related entity fields
        /// </summary>
        protected IPrefetchPath PrefetchPath = null;

        /// <summary>
        /// Extra filter applied to the datasource
        /// </summary>
        protected PredicateExpression Filter = null;

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the PerformSelect event of the DataSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SD.LLBLGen.Pro.ORMSupportClasses.PerformSelectEventArgs" /> instance containing the event data.</param>
        private void DataSource_PerformSelect(object sender, PerformSelectEventArgs e)
        {
            if (this.PrefetchPath == null)
                this.PrefetchPath = e.PrefetchPath;

            e.ContainedCollection.GetMulti(e.Filter, e.MaxNumberOfItemsToReturn, e.Sorter, e.Relations, this.PrefetchPath, this.excludeIncludeFieldsList, e.PageNumber, e.PageSize);
        }

        /// <summary>
        /// Handles the PerformGetDbCount event of the DataSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SD.LLBLGen.Pro.ORMSupportClasses.PerformGetDbCountEventArgs" /> instance containing the event data.</param>
        private void DataSource_PerformGetDbCount(object sender, PerformGetDbCountEventArgs e)
        {
            e.DbCount = e.ContainedCollection.GetDbCount(e.Filter, e.Relations);
        }

        /// <summary>
        /// Handles the PerformWork event of the DataSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="PerformWorkEventArgs"/> instance containing the event data.</param>
        private void DataSource_PerformWork(object sender, PerformWorkEventArgs e)
        {
            ITransaction transaction = null;
            try
            {
                // Perform the work passed in via the PerformWorkEventArgs object. Start a new transaction with the passed in unit of work.
                transaction = LLBLGenUtil.GetTransaction(IsolationLevel.ReadCommitted, this.UniqueID);

                // Pass the transaction to the Commit routine and tell it to autocommit when the work is done.
                // If an exception is thrown, the transaction will be rolled back by the Dispose call of the using statement.
                e.Uow.Commit(transaction, true);

                LLBLGenProDataSource ds = this.DataSource as LLBLGenProDataSource;
                if (ds != null) ds.Refetch = true;
            }
            catch
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw;
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the data source.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if the data source is initialized; otherwise, <c>false</c>.
        /// </returns>
        public override bool InitializeDataSource()
        {
            // Set data source id
            this.DataSourceId = this.ClientID + "_DataSource";

            // Initialize the DataSource object
            LLBLGenProDataSource ds = new LLBLGenProDataSource()
            {
                ID = this.DataSourceId,
                LivePersistence = false,
                CacheLocation = DataSourceCacheLocation.None,
                DataContainerType = DataSourceDataContainerType.EntityCollection,
                EnablePaging = true
            };

            // Retrieve the full type, assembly name of the collection to load
            IEntityCollection collection = this.GetEntityCollection();
            Type collectionType = collection.GetType();
            ds.EntityCollectionTypeName = String.Format("{0}, {1}", collectionType.FullName, collectionType.Assembly);
            ds.FilterToUse = new PredicateExpression();

            // Filter for archived
            IEntity entity = this.GetEntity();
            if (!PageDataSourceCollection.ShowArchivedRecords &&
                entity.Fields["Archived"] != null)
            {
                ds.FilterToUse.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression(this.EntityName, "Archived", false));
            }

            if (this.Filter != null)
            {
                ds.FilterToUse.Add(this.Filter);
            }

            // Set PK fieldname
            this.MainGridView.KeyFieldName = entity.PrimaryKeyFields[0].Name;

            // Get the DefaultGridView Includefields
            this.excludeIncludeFieldsList = this.GetDefaultIncludeFields(entity);

            if (this.IncludeFieldsList != null)
            {
                this.excludeIncludeFieldsList.AddRange(this.IncludeFieldsList);
            }

            // Set events for the data source object
            ds.PerformSelect += this.DataSource_PerformSelect;
            ds.PerformGetDbCount += this.DataSource_PerformGetDbCount;
            ds.PerformWork += this.DataSource_PerformWork;

            // Add DataSource object and use as page DataSource
            this.Controls.Add(ds);
            this.DataSource = ds;

            return true;
        }

        public IncludeFieldsList GetDefaultIncludeFields(IEntity entity)
        {
            IncludeFieldsList includes = new IncludeFieldsList();

            if (this.MainGridView.ParentAsGridViewEntityCollectionReady != null)
            {
                IEntityCollection viewItems = this.MainGridView.ParentAsGridViewEntityCollectionReady.SelectedView.ViewItems;
                List<string> prefetchedEntities = new List<string>();

                PropertyInfo[] propertyInfoCollection = null;
                for (int i = 0; i < viewItems.Count; i++)
                {
                    IViewItem viewItem = viewItems[i] as IViewItem;
                    if (!Instance.Empty(viewItem) &&
                        viewItem.ShowOnGridView)
                    {
                        if (entity.Fields[viewItem.FieldName] != null)
                        {
                            // EntityField
                            includes.Add(entity.Fields[viewItem.FieldName]);
                        }
                        else if (viewItem.FieldName.Contains(".", StringComparison.InvariantCultureIgnoreCase))
                        {
                            // Related EntityField
                            string[] items = viewItem.FieldName.Split('.');
                            if (items.Length > 1 && !prefetchedEntities.Contains(items[0], StringComparison.InvariantCultureIgnoreCase))
                            {
                                prefetchedEntities.Add(items[0]);

                                string relatedEntityName = items[0];
                                IEntity relatedEntity = null;

                                try
                                {
                                    relatedEntity = this.GetEntity(relatedEntityName);
                                }
                                catch (Exception ex)
                                {
                                    // No worries, we can't get the entity. Just execute the query the oldskool way
                                }

                                if (relatedEntity != null && this.PrefetchPath == null)
                                {
                                    this.PrefetchPath = this.CreatePrefetchPath();

                                    // Create the PrefetchPathElement
                                    object prefetchPathElementObj = this.CreatePrefetchPathElement(relatedEntityName);
                                    if (prefetchPathElementObj != null)
                                    {
                                        IncludeFieldsList relatedEntityIncludes = new IncludeFieldsList();

                                        List<IViewItem> relatedViewItems = this.GetViewItemsWithFieldNamePrefix(viewItems, relatedEntityName + ".");
                                        foreach (IViewItem relatedViewItem in relatedViewItems)
                                        {
                                            string[] relatedValues = relatedViewItem.FieldName.Split('.');
                                            if (relatedValues.Length > 1 && relatedEntity.Fields[relatedValues[1]] != null)
                                            {
                                                relatedEntityIncludes.Add(relatedEntity.Fields[relatedValues[1]]);
                                            }
                                        }
                                        this.PrefetchPath.Add((IPrefetchPathElement)prefetchPathElementObj, relatedEntityIncludes);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Custom property                            
                            if (propertyInfoCollection == null)
                                propertyInfoCollection = Dionysos.Reflection.Member.GetPropertyInfo(entity);

                            if (propertyInfoCollection != null)
                            {
                                var propertyInfo = propertyInfoCollection.SingleOrDefault(pi => pi.Name.Equals(viewItem.FieldName, StringComparison.InvariantCultureIgnoreCase));
                                if (propertyInfo != null)
                                {
                                    // Get the the EntityFieldDependencyAttribute
                                    object[] attribute = propertyInfo.GetCustomAttributes(typeof(EntityFieldDependencyAttribute), false);
                                    if (attribute.Length > 0)
                                    {
                                        EntityFieldDependencyAttribute entityFieldDependencyAttr = (EntityFieldDependencyAttribute)attribute[0];
                                        for (int j = 0; j < entityFieldDependencyAttr.FieldIndexes.Count; j++)
                                        {
                                            int fieldIndex = entityFieldDependencyAttr.FieldIndexes[j];

                                            if (entity.Fields[fieldIndex] != null)
                                            {
                                                // EntityField                                        
                                                includes.Add(entity.Fields[fieldIndex]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return includes;
        }

        private List<IViewItem> GetViewItemsWithFieldNamePrefix(IEntityCollection viewItems, string fieldNamePrefix)
        {
            List<IViewItem> toReturn = new List<IViewItem>();
            for (int i = 0; i < viewItems.Count; i++)
            {
                IViewItem viewItem = viewItems[i] as IViewItem;
                if (!Instance.Empty(viewItem) && viewItem.ShowOnGridView && viewItem.FieldName.StartsWith(fieldNamePrefix, StringComparison.InvariantCultureIgnoreCase))
                {
                    toReturn.Add(viewItem);
                }
            }
            return toReturn;
        }

        public virtual IEntityCollection GetEntityCollection()
        {
            return DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as IEntityCollection;
        }

        public virtual IEntity GetEntity()
        {
            return DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
        }

        public virtual IEntity GetEntity(string entityName)
        {
            return DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
        }

        public virtual IPrefetchPath CreatePrefetchPath()
        {
            return DataFactory.EntityPrefetchPathFactory.CreatePrefetchPath(this.EntityName) as IPrefetchPath;
        }

        public virtual IPrefetchPathElement CreatePrefetchPathElement(string destinationEntityName)
        {
            return DataFactory.EntityPrefetchPathFactory.CreatePrefetchPathElement(this.EntityName, destinationEntityName) as IPrefetchPathElement;
        }



        #endregion
    }
}
