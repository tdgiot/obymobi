﻿using System;
using System.Web.UI;
using DevExpress.Web;
using DevExpress.XtraGrid;
using Dionysos.Data;
using Dionysos.Interfaces.Data;
using Dionysos.Web.Security;
using Dionysos.Web.UI.DevExControls;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Class for displaying collections from a datasource
    /// </summary>
    public abstract class PageOverviewDataSourceCollection : PageDataSourceCollection, IGridViewEntityCollectionReady
    {
        #region Fields

        private GridViewEntityCollection mainGridView;
        private IView selectedView;

        #endregion

        #region Properties

        /// <summary>
        /// The GridView that should be populated and handled by the logic of the PageEntityCollection
        /// </summary>
        public GridViewEntityCollection MainGridView
        {
            get
            {
                if (this.mainGridView == null)
                {
                    // On first call set the ClientInstanceName
                    this.mainGridView = this.GetMainGridView();
                    this.mainGridView.ClientInstanceName = "gridView" + this.UniqueID;
                }

                return this.mainGridView;
            }
            set
            {
                this.mainGridView = value;
            }
        }

        /// <summary>
        /// Gets or sets the currently selected view
        /// </summary>
        public IView SelectedView
        {
            get
            {
                if (this.selectedView == null)
                {
                   this.selectedView = ViewUtil.GetView(this.Page.GetType().BaseType.FullName, this.EntityName, UserManager.CurrentUser.UserId);
                }

                return this.selectedView;
            }
            set
            {
                this.selectedView = value;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PageOverviewDataSourceCollection"/> class.
        /// </summary>
        protected PageOverviewDataSourceCollection()
        {
            this.Load += new EventHandler(PageOverviewDataSourceCollection_Load);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the Load event of the PageOverviewDataSourceCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void PageOverviewDataSourceCollection_Load(object sender, EventArgs e)
        {
            this.ConfigureGridView();

            if (String.IsNullOrEmpty(this.EntityPageUrl))
            {
                this.MultiValidatorDefault.AddError("LET OP: De EntityPageUrl is nog niet gevuld!");
                this.Validate();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Resets the auto filter.
        /// </summary>
        public void ResetAutoFilter()
        {
            this.MainGridView.ResetAutoFilter();
        }

        public GridViewDataColumn GetColumnByName(string name)
        {
            if (MainGridView.Columns[name] is GridViewDataColumn column)
            {
                return column;
            }

            return null;
        }

        public void SetColumnSortMode(string columnName, ColumnSortMode sortMode)
        {
            if (GetColumnByName(columnName) is GridViewDataColumn column)
            {
                column.Settings.SortMode = sortMode;
            }
        }

        /// <summary>
        /// Configures the grid view.
        /// </summary>
        private void ConfigureGridView()
        {
            this.MainGridView.ShowDeleteColumn = false;
            this.MainGridView.ShowEditColumn = false;
            this.MainGridView.ShowDeleteHyperlinkColumn = true;
            this.MainGridView.ShowEditHyperlinkColumn = true;
            //this.MainGridView.DataSourceForceStandardPaging = true;

            if (UserManager.CurrentUser != null &&
                UserManager.CurrentUser.PageSize > 0)
            {
                this.MainGridView.SettingsPager.PageSize = UserManager.CurrentUser.PageSize;
            }
            else
            {
                this.MainGridView.SettingsPager.PageSize = Dionysos.ConfigurationManager.GetInt(DionysosWebConfigurationConstants.PageSize);
            }
        }

        /// <summary>
        /// Method to load the DataSource
        /// </summary>
        /// <returns>
        /// True if initialization was succesful, False if not
        /// </returns>
        public abstract override bool InitializeDataSource();

        /// <summary>
        /// Method to bind the DataSource to the relevant Control(s)
        /// </summary>
        /// <returns>
        /// True if initialization was succesful, False if not
        /// </returns>
        public override bool InitializeDataBindings()
        {
            this.MainGridView.DataSourceID = this.DataSourceId;
            
            return true;
        }

        /// <summary>
        /// Get the first GridView of the subpanel and use it as MainGridView
        /// </summary>
        /// <returns></returns>
        private GridViewEntityCollection GetMainGridView()
        {
            this.CollectControls();

            // Walk through the control collection of this page
            for (int i = 0; i < this.ControlList.Count; i++)
            {
                // Get a control from the control collection
                // and check whether the control is a data grid
                Control control = this.ControlList[i];
                if (control is GridViewEntityCollection)
                {
                    return control as GridViewEntityCollection;
                }
            }

            return null;
        }

        #endregion
    }
}
