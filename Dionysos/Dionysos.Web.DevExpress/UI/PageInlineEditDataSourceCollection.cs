﻿using System;
using System.Web.UI;
using DevExpress.Web;
using Dionysos.Interfaces.Data;
using Dionysos.Data;
using Dionysos.Web.Security;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Datasource page for LLBLGen DataSources
    /// </summary>
    public abstract class PageInlineEditDataSourceCollection : PageDataSourceCollection, IGridViewEntityCollectionReady
	{
		#region Fields

		private Dionysos.Web.UI.DevExControls.GridViewEntityCollection mainGridView;
		private IView selectedView = null;

		#endregion

		#region Methods

		protected PageInlineEditDataSourceCollection()
		{
			this.HookUpEvents();
		}

		private void HookUpEvents()
		{
			this.Load += new EventHandler(PageInlineEditDataSourceCollection_Load);
		}

		/// <summary>
		/// Method to load the DataSource
		/// </summary>
		/// <returns></returns>
		public abstract override bool InitializeDataSource();

		/// <summary>
		/// Method to bind the DataSource to the relevant Control(s)
		/// </summary>
		/// <returns></returns>
		public override bool InitializeDataBindings()
		{
			this.MainGridView.DataSourceID = this.DataSourceId;
			this.MainGridView.DataBind();

			return true;
		}

		/// <summary>
		/// Get the first GridView of the subpanel and use it as MainGridView
		/// </summary>
		/// <returns></returns>
		private Dionysos.Web.UI.DevExControls.GridViewEntityCollection GetMainGridView()
		{
			// Walk through the control collection of this page
			for (int i = 0; i < this.ControlList.Count; i++)
			{
				// Get a control from the control collection
				// and check whether the control is a data grid
				Control control = this.ControlList[i];
				if (control is Dionysos.Web.UI.DevExControls.GridViewEntityCollection)
				{
					return control as Dionysos.Web.UI.DevExControls.GridViewEntityCollection;
				}
			}
			return null;
		}

		void ConfigureGridView()
		{
			this.MainGridView.ShowDeleteColumn = true;
			this.MainGridView.ShowEditColumn = true;
			this.MainGridView.ShowAddNewButton = true;
			this.MainGridView.ClientSideEvents.RowDblClick = string.Format("function(s, e) {{ {0}.StartEditRow(e.visibleIndex) }}", this.MainGridView.ClientInstanceName);
			this.MainGridView.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.NextColumn;
		}

		#endregion

		#region Event Handlers

		void PageInlineEditDataSourceCollection_Load(object sender, EventArgs e)
		{
			this.ConfigureGridView();
		}

		#endregion

		#region Properties

		/// <summary>
		/// The GridView that should be populated and handled by the logic of the PageEntityCollection
		/// </summary>
		public Dionysos.Web.UI.DevExControls.GridViewEntityCollection MainGridView
		{
			get
			{
				if (this.mainGridView == null)
				{
					// On first call set the ClientInstanceName					
					this.mainGridView = this.GetMainGridView();
					this.mainGridView.ClientInstanceName = "gridView" + this.UniqueID;
				}
				return this.mainGridView;
			}
			set
			{
				this.mainGridView = value;
			}
		}

		/// <summary>
		/// Gets or sets the currently selected view
		/// </summary>
		public IView SelectedView
		{
			get
			{
				if (this.selectedView == null)
				{
					this.selectedView = ViewUtil.GetView(this.Page.GetType().BaseType.FullName, this.EntityName, UserManager.CurrentUser.UserId);
				}

				return this.selectedView;
			}
			set
			{
				this.selectedView = value;
			}
		}

		#endregion
	}
}
