﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;

namespace Dionysos.Web
{
	/// <summary>
	/// Configuration class for Dionysos.Web
	/// </summary>
	public class DionysosWebDevExConfigInfo : ConfigurationItemCollection
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the B2BConfiguration class
		/// </summary>
		public DionysosWebDevExConfigInfo()
		{
			string sectionName = "Dionysos.DevEx";
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.ComboBoxDefaultCssClass, "Standaard CSS class voor ComboBox's", "input", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.TextBoxDefaultCssClass, "Standaard CSS class voor Textbox's", "input", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.InvalidControlCssClass, "Standaard CSS class voor invalid controls", "input", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.RequiredControlCssClass, "Standaard CSS class voor verplichte controls", "input", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.DefaultRequiredButEmptyError, "Standaard error tekst voor verplichtte controls die geen waarde hebben", "<strong>{0}</strong>: Is verplicht en heeft nog geen waarde.", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.DefaultRequiredFieldErrorText, "Standaard error tekst voor verplichtte controls die geen waarde hebben", "Veld is verplicht.", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.FilterRowInputDelay, "Vertraging in ms tussen invoeren van filter waarde en het uitvoeren van de zoekopdracht in een gridview", "750", typeof(int)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.GridViewButtonEditImageUrl, "Standaard icoon voor de Bewerken knop in een GridView", "~/Images/Icons/pencil.png", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.GridViewButtonDeleteImageUrl, "Standaard icoon voor de Verwijderen knop in een GridView", "~/Images/Icons/delete.png", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.AllowCustomViews, "Het wel of niet toestaan van gebruik van Custom Views", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.AllowSubPanelCustomViews, "Het wel of niet toestaan van gebruik van Custom Views op tabbladen.", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.EnableCallbacksForControls, "Gebruik Callback voor GridView Filters en Sortering (i.p.v. Postbacks)", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.PersistAutoFiltersToSessionForCollectionPages, "Onthoud alle filters op overzicht pagina's", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.PersistAutoFiltersToSessionForUserControls, "Onthoud alle filters op overzichten in tabbladen", false, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.GridViewUsePaging, "Gebruik paging in GridViews", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.CacheComboBoxDataSources, "Cache EntityCollections voor combo boxes.", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.ComboBoxEnableCallbackMode, "Inschakelen van asynchroon inladen van combo boxes.", false, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.ComboBoxCallbackPageSize, "Aantal ingeladen items indien combo boxes asynchroon ingeladen worden.", 100, typeof(int), true));
			this.Add(new ConfigurationItem(sectionName, DevExConfigConstants.ComboBoxEnableEntityLink, "Inschakelen van links naar entity pagina voor combo boxes.", false, typeof(bool), true));
		}

		#endregion
	}
}
