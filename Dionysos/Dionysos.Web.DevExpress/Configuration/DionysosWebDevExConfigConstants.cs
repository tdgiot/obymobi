﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web
{
	/// <summary>
	/// Configuration class which contains constants for the names of configuration items
	/// </summary>
	public class DevExConfigConstants
	{
		/// <summary>
		/// Default CSS class for ComboBox.
		/// </summary>
		public const string ComboBoxDefaultCssClass = "ComboBoxDefaultCssClass";

		/// <summary>
		/// Default CSS class for TextBox.
		/// </summary>
		public const string TextBoxDefaultCssClass = "TextBoxDefaultCssClass";

		/// <summary>
		/// Default CSS class addition for required controls.
		/// </summary>
		public const string RequiredControlCssClass = "RequiredControlCssClass";

		/// <summary>
		/// Default CSS class addition for invalid controls.
		/// </summary>
		public const string InvalidControlCssClass = "InvalidControlCssClass";

		/// <summary>
		/// Default control required, but empty error.
		/// </summary>
		public const string DefaultRequiredButEmptyError = "DefaultRequiredButEmptyError";

		/// <summary>
		/// Default required field error.
		/// </summary>
		public const string DefaultRequiredFieldErrorText = "DefaultRequiredFieldErrorText";

		/// <summary>
		/// Default filter row input delay.
		/// </summary>
		public const string FilterRowInputDelay = "FilterRowInputDelay";

		/// <summary>
		/// Default ImageUrl for Edit Buttons in GridViews.
		/// </summary>
		public const string GridViewButtonEditImageUrl = "GridViewButtonEditImageUrl";

		/// <summary>
		/// Default ImageUrl for Delete Buttons in GridViews.
		/// </summary>
		public const string GridViewButtonDeleteImageUrl = "GridViewButtonDeleteImageUrl";

		/// <summary>
		/// Auto persist filters to session for collection pages.
		/// </summary>
		public const string PersistAutoFiltersToSessionForCollectionPages = "PersistAutoFiltersToSessionForCollectionPages";

		/// <summary>
		/// Auto persist filters to session for user controls.
		/// </summary>
		public const string PersistAutoFiltersToSessionForUserControls = "PersistAutoFiltersToSessionForUserControls";

		/// <summary>
		/// Allow custom views.
		/// </summary>
		public const string AllowCustomViews = "AllowCustomViews";

		/// <summary>
		/// Allow custom views on sub panels
		/// </summary>
		public const string AllowSubPanelCustomViews = "AllowSubPanelCustomViews";

		/// <summary>
		/// Enable callbacks for controls.
		/// </summary>
		public const string EnableCallbacksForControls = "EnableCallbacksForControls";

		/// <summary>
		/// Use paging in gridviews.
		/// </summary>
		public const string GridViewUsePaging = "GridViewUsePaging";

		/// <summary>
		/// Cache combo box data sources.
		/// </summary>
		public const string CacheComboBoxDataSources = "CacheComboBoxDataSources";

		/// <summary>
		/// Enable callback mode for combo boxes.
		/// </summary>
		public const string ComboBoxEnableCallbackMode = "ComboBoxEnableCallbackMode";

		/// <summary>
		/// The callback page size for combo boxes.
		/// </summary>
		public const string ComboBoxCallbackPageSize = "ComboBoxCallbackPageSize";

		/// <summary>
		/// Enable linking to entity page for combo boxes.
		/// </summary>
		public const string ComboBoxEnableEntityLink = "ComboBoxEnableEntityLink";
	}
}
