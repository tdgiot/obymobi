using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;

namespace Dionysos.Data
{
    /// <summary>
    /// Class which represents a field on a table in a database
    /// </summary>
    public class Field : IField
    {
        #region Fields

        private string name = string.Empty;
        private System.Type dataType = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Data.Field type
        /// </summary>
        public Field()
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx-media.Data.Field type using the specified name
        /// </summary>
        public Field(string name)
        {
            if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Parameter 'name' is empty
            }
            else
            {
                this.name = name;
            }
        }

        /// <summary>
        /// Constructs an instance of the Lynx-media.Data.Field type using the specified name and datatype
        /// </summary>
        public Field(string name, System.Type dataType)
        {
            if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Parameter 'name' is empty
            }
            else if (Instance.ArgumentIsEmpty(dataType, "dataType"))
            {
                // Parameter 'dataType' is empty
            }
            else
            {
                this.name = name;
                this.dataType = dataType;
            }
        }

        #endregion

        #region Methods

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the field
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the datatype of the field
        /// </summary>
        public System.Type DataType
        {
            get
            {
                return this.dataType;
            }
            set
            {
                this.dataType = value;
            }
        }

        #endregion
    }
}
