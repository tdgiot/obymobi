﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using System.Web;
using System.Collections;

namespace Dionysos.Data
{
	/// <summary>
	/// Utility class which can be used for UI Elements
	/// </summary>
	public class UIElementUtil
	{
		#region Fields

		static UIElementUtil instance = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the UIElementUtil class if the instance has not been initialized yet
		/// </summary>
		public UIElementUtil()
		{
			if (instance == null)
			{
				// first create the instance
				instance = new UIElementUtil();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the static UIElementUtil instance
		/// </summary>
		private void Init()
		{
		}

		/// <summary>
		/// Gets the UIElementEntity for the specified control
		/// </summary>
		/// <param name="control">The Control to get the UIElementEntity for</param>
		/// <returns>A UIElementEntity instance</returns>
		public static IEntity GetUIElementEntity(Control control)
		{
			IEntity uiElementEntity = null;

			if (Instance.ArgumentIsEmpty(control, "control"))
			{
				// Parameter 'control' is empty
			}
			else
			{
				uiElementEntity = GetUIElementByTypeName(control.GetType().BaseType.FullName);
			}

			return uiElementEntity;
		}

		/// <summary>
		/// Retrieve all UIElement for a module, also takes in account UiElementCustoms
		/// </summary>
		/// <param name="moduleNameSystem"></param>
		/// <returns></returns>
		public static IEntityCollection GetUIElementsForModule(string moduleNameSystem)
		{
			string cacheKey = "Dionysos.Data.UIElementUtil.GetUIElementsForModule-" + moduleNameSystem;

			IEntityCollection uiElements = HttpContext.Current.Cache[cacheKey] as IEntityCollection;
			if (uiElements == null)
			{
				// Get UI elements for module
				uiElements = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression("UIElement", "ModuleNameSystem", ComparisonOperator.Equal, moduleNameSystem, null);
				IEntityView uiElementsView = uiElements.DefaultView;

				// Retrieve custom values
				IEntityCollection uiElementsCustom = LLBLGenEntityCollectionUtil.GetEntityCollection("UIElementCustom");
				PredicateExpression filter;
				foreach (IEntity uiElementCustom in uiElementsCustom)
				{
					filter = new PredicateExpression();
					filter.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("UIElement", "TypeNameFull", uiElementCustom.Fields["TypeNameFull"].CurrentValue));
					uiElementsView.Filter = filter;

					if (uiElementsView.Count == 0 &&
						((string)uiElementCustom.Fields["ModuleNameSystem"].CurrentValue) == moduleNameSystem)
					{
						// Custom UI element has another module than default UI element
						IEntityCollection uiElements2 = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression("UIElement", "TypeNameFull", ComparisonOperator.Equal, uiElementCustom.Fields["TypeNameFull"].CurrentValue, null);
						if (uiElements2.Count == 1)
						{
							IEntity currentUiElement = uiElements2[0];
							LLBLGenEntityUtil.CopyFields(uiElementCustom, currentUiElement, new ArrayList(), false);
							uiElements.Add(currentUiElement);
						}
					}
					else if (uiElementsView.Count == 1)
					{
						// Update to custom value
						IEntity currentUiElement = uiElementsView[0];

						// Custom value available
						if (uiElementCustom.Fields["ModuleNameSystem"].CurrentValue == null ||
							uiElementCustom.Fields["ModuleNameSystem"].CurrentValue == currentUiElement.Fields["ModuleNameSystem"].CurrentValue)
						{
							// Same module, so change values
							LLBLGenEntityUtil.CopyFields(uiElementCustom, currentUiElement, new ArrayList(), false);
						}
						else
						{
							// Module is changed, so remove from current module
							uiElements.Remove(currentUiElement);
						}
					}
				}

				HttpContext.Current.Cache[cacheKey] = uiElements;
			}

			return uiElements;
		}

		/// <summary>
		/// Get a UIElementEntity based on it's full TypeName
		/// </summary>
		/// <param name="typeNameFull">TypeName of the UIElement</param>
		/// <returns>The found UIElement or null on 0 or > 1 results</returns>
		public static IEntity GetUIElementByTypeName(string typeNameFull)
		{
			string cacheKey = "Dionysos.Data.UIElementUtil.GetUIElementByTypeName-" + typeNameFull;

			IEntity uiElementEntity = HttpContext.Current.Cache[cacheKey] as IEntity;
			if (uiElementEntity == null)
			{
				uiElementEntity = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression("UIElement", "TypeNameFull", ComparisonOperator.Equal, typeNameFull);

				if (uiElementEntity != null)
				{
					// Try to get custom
					IEntity uiElementCustomEntity = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression("UIElementCustom", "TypeNameFull", ComparisonOperator.Equal, uiElementEntity.Fields["TypeNameFull"].CurrentValue);
					if (uiElementCustomEntity != null)
					{
						LLBLGenEntityUtil.CopyFields(uiElementCustomEntity, uiElementEntity, new ArrayList(), false);
					}

					// Add to cache
					HttpContext.Current.Cache[cacheKey] = uiElementEntity;
				}
			}

			return uiElementEntity;
		}

		/// <summary>
		/// Gets the UI element by the entity name.
		/// </summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <returns>
		/// The UI element.
		/// </returns>
		public static IEntity GetUIElementByEntityName(string entityName)
		{
			string cacheKey = "Dionysos.Data.UIElementUtil.GetUIElementByEntityName-" + entityName;

			IEntity uiElementEntity = HttpContext.Current.Cache[cacheKey] as IEntity;
			if (uiElementEntity == null)
			{
				uiElementEntity = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression("UIElement", "EntityName", ComparisonOperator.Equal, entityName.RemoveRight("Entity"));

				if (uiElementEntity != null)
				{
					// Try to get custom
					IEntity uiElementCustomEntity = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression("UIElementCustom", "TypeNameFull", ComparisonOperator.Equal, uiElementEntity.Fields["TypeNameFull"].CurrentValue);
					if (uiElementCustomEntity != null)
					{
						LLBLGenEntityUtil.CopyFields(uiElementCustomEntity, uiElementEntity, new ArrayList(), false);
					}

					// Add to cache
					HttpContext.Current.Cache[cacheKey] = uiElementEntity;
				}
			}

			return uiElementEntity;
		}

		#endregion
	}
}