﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data
{
	/// <summary>
	/// Attribute class which is used to indicate which database fields are used on a custom property.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class DatabaseFieldsAttribute : Attribute
	{
		#region Fields

		/// <summary>
		/// The database fields.
		/// </summary>
		private readonly string[] databaseFields;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the database fields.
		/// </summary>
		/// <value>
		/// The database fields.
		/// </value>
		/// <remarks>
		/// The names of database fields used in the property. If set, allows queries to be optimized (eg. only retrieve the needed database fields).
		/// </remarks>
		public string[] DatabaseFields
		{
			get
			{
				return this.databaseFields;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DatabaseFieldsAttribute"/> class.
		/// </summary>
		public DatabaseFieldsAttribute()
			: this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="DatabaseFieldsAttribute"/> class.
		/// </summary>
		/// <param name="databaseFields">The database fields.</param>
		public DatabaseFieldsAttribute(params string[] databaseFields)
		{
			this.databaseFields = databaseFields ?? new string[0];
		}

		#endregion
	}
}
