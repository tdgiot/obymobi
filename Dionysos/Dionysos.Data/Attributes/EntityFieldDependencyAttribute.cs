﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data
{
	/// <summary>
	/// Attribute class which is used to indicate that a property is used for the IncludeFieldsList for an overview page in the CMS.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class EntityFieldDependencyAttribute : Attribute
    {
        #region Properties

        public List<int> FieldIndexes = new List<int>();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityFieldDependencyAttribute"/> class.
        /// </summary>
        public EntityFieldDependencyAttribute(int fieldIndex)
        {
            this.FieldIndexes.Add(fieldIndex);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityFieldDependencyAttribute"/> class.
        /// </summary>
        public EntityFieldDependencyAttribute(int fieldIndex1, int fieldIndex2)
        {
            this.FieldIndexes.Add(fieldIndex1);
            this.FieldIndexes.Add(fieldIndex2);
        }

		#endregion
	}
}
