﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data
{
	/// <summary>
	/// Attribute class which is used to indicate that a property must be displayed in a data grid view.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class DataGridViewColumnVisibleAttribute : Attribute
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DataGridViewColumnVisibleAttribute"/> class.
		/// </summary>
		public DataGridViewColumnVisibleAttribute()
		{ }

		#endregion
	}
}
