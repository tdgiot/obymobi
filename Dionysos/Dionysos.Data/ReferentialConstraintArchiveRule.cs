﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data
{
    /// <summary>
    /// Enumator for setting delete rules for referential constraints
    /// </summary>
    public enum ReferentialConstraintArchiveRule : int
    {
        /// <summary>
        /// No action is being performed on foreign records
        /// </summary>
        NoAction = 0,
        /// <summary>
        /// Foreign records are being archived if a record is being archived
        /// </summary>
        Cascade = 1,   
    }
}
