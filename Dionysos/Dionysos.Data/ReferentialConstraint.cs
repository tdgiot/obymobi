﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data
{
    /// <summary>
    /// Class which represents a referential constraint definition for data integrity
    /// </summary>
    public class ReferentialConstraint
    {
        #region Fields

        private string fieldName = string.Empty;
        private ReferentialConstraintDeleteRule deleteRule = ReferentialConstraintDeleteRule.NoAction;
		private ReferentialConstraintArchiveRule archiveRule = ReferentialConstraintArchiveRule.NoAction;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Data.ReferentialConstraint type
        /// </summary>
        public ReferentialConstraint()
        {
        }

        /// <summary>
        /// Constructs an instance of the Dionysos.Data.ReferentialConstraint type using the specified fieldname
        /// </summary>
        public ReferentialConstraint(string fieldName)
        {
            this.fieldName = fieldName;
        }

        /// <summary>
        /// Constructs an instance of the Dionysos.Data.ReferentialConstraint type using the specified fieldname and delete rule
        /// </summary>
		public ReferentialConstraint(string fieldName, ReferentialConstraintDeleteRule deleteRule, ReferentialConstraintArchiveRule archiveRule)
        {
            this.fieldName = fieldName;
            this.deleteRule = deleteRule;
			this.archiveRule = archiveRule;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the fieldname
        /// </summary>
        public string FieldName
        {
            get
            {
                return this.fieldName;
            }
            set
            {
                this.fieldName = value;
            }
        }

        /// <summary>
        /// Gets or sets the delete rule
        /// </summary>
        public ReferentialConstraintDeleteRule DeleteRule
        {
            get
            {
                return this.deleteRule;
            }
            set
            {
                this.deleteRule = value;
            }
        }

		/// <summary>
		/// Gets or sets the delete rule
		/// </summary>
		public ReferentialConstraintArchiveRule ArchiveRule
		{
			get
			{
				return this.archiveRule;
			}
			set
			{
				this.archiveRule = value;
			}
		}

        #endregion
    }
}
