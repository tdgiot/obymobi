﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.Json {

    /// <summary>
    /// Marker attribute for the Json serialization.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class JsonDataAttribute : Attribute {
    }
}
