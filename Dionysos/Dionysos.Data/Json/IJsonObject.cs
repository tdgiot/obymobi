﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.Json {

    /// <summary>
    /// 
    /// </summary>
    public interface IJsonObject {

        #region Methods /////////////////////////////////////////////////////////////////

        /// <summary>
        /// Toes the json string.
        /// </summary>
        /// <returns></returns>
        string ToJsonString();

        #endregion
    }
}
