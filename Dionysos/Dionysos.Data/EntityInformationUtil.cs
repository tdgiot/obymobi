﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Collections;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Interfaces.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Reflection;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;

namespace Dionysos.Data
{
	/// <summary>
	/// Utility class which can be used to retrieve entity information for LLBLGen entities
	/// </summary>
	public static class EntityInformationUtil
	{
		#region Fields

		/// <summary>
		/// Entity Fields based on Database fields of the Entity
		/// </summary>
		public const string EntityFieldTypeDatabase = "EntityFieldTypeDatabase";

		/// <summary>
		/// Entity Fields based on Properties defined in the Entity code
		/// </summary>
		public const string EntityFieldTypeCode = "EntityFieldTypeCode";

        /// <summary>
        /// Local EntityInformation cache for when we don't have a valid HttpContext
        /// </summary>
        public static ConcurrentDictionary<string, IEntityInformation> EntityInformationCache = new ConcurrentDictionary<string, IEntityInformation>();

		#endregion

		#region EntityInformation retrieval methods

		/// <summary>
		/// Get the information about a Entity
		/// </summary>
		/// <param name="entity">The entity to get the entity information for</param>
		/// <returns>EntityInformation for the Entity</returns>
		public static IEntityInformation GetEntityInformation(IEntity entity)
		{
			return GetEntityInformation(entity.LLBLGenProEntityName);
		}

		/// <summary>
		/// Get the information about a Entity
		/// </summary>
		/// <param name="entityName">The name of the entity to get the entity information for</param>
		/// <returns>EntityInformation for the Entity</returns>
		public static IEntityInformation GetEntityInformation(string entityName)
		{
			return GetEntityInformation(entityName, true);
		}

		/// <summary>
		/// Get the friendly name for an entity
		/// </summary>
		/// <param name="entity">Entity</param>
		/// <returns></returns>
		public static string GetFriendlyName(IEntity entity)
		{
			return GetEntityInformation(LLBLGenUtil.GetEntityName(entity)).FriendlyName;
		}

		/// <summary>
		/// Get the friendly name for an entity
		/// </summary>
		/// <param name="entityName">Entity</param>
		/// <returns></returns>
		public static string GetFriendlyName(string entityName)
		{
			return GetEntityInformation(entityName).FriendlyName;
		}

		/// <summary>
		/// Get the friendly name plural for an entity
		/// </summary>
		/// <param name="entityName">Entity</param>
		/// <returns></returns>
		public static string GetFriendlyNamePlural(string entityName)
		{
			return GetEntityInformation(entityName).FriendlyNamePlural;
		}

		/// <summary>
		/// Get the friendly name for an entities field
		/// </summary>
		/// <param name="entityName">Entity Name</param>
		/// <param name="fieldName">Field Name</param>
		/// <returns></returns>
		public static string GetFieldNameFriendlyName(string entityName, string fieldName)
		{
			string friendlyName = null;
			IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(entityName);

			IEntityFieldInformation entityFieldInformation = entityInformation.AllFields.FirstOrDefault(eif => eif.Key.Equals(fieldName, StringComparison.OrdinalIgnoreCase)).NullSafe(eif => eif.Value);
			if (entityFieldInformation != null)
			{
				friendlyName = entityFieldInformation.FriendlyName;
			}

			return friendlyName;
		}

		/// <summary>
		/// Get the information about a Entity
		/// </summary>
		/// <param name="entityName">The name of the entity to get the entity information for</param>
		/// <param name="fromCache">Flag which indicates whether the entity information should be retrieved from the cache</param>
		/// <returns>EntityInformation for the Entity</returns>
		public static IEntityInformation GetEntityInformation(string entityName, bool fromCache)
		{
			IEntityInformation toReturn = null;

			entityName = LLBLGenUtil.GetEntityName(entityName);
			// Also take in account the UICulture for the Caching
			string cacheKey = "entityInformation-" + entityName + "-" + EntityInformationUtil.CurrentUICultureLanguageCode;

			// Check whether the entity information exists in cache
		    if (fromCache)
		    {
		        if (System.Web.HttpContext.Current != null)
		        {
		            if (System.Web.HttpContext.Current.Cache[cacheKey] != null)
		                toReturn = System.Web.HttpContext.Current.Cache[cacheKey] as IEntityInformation;
		        }
		        else
		        {
		            // If HttpContext is NULL, try to get value from local cache
		            EntityInformationCache.TryGetValue(cacheKey, out toReturn);
		        }
		    }

		    // Not found in cache
			if (toReturn == null)
			{
				// Create empty entity of the specified entity name
				IEntity entity = Dionysos.DataFactory.EntityFactory.GetEntity(entityName) as IEntity;

				// Create empty entity of the EntityInformation type to use the fieldinfo for predicates
				toReturn = EntityInformationUtil.GetEmptyEntityInformationInstance(entityName);

				// Translate if required
				if (toReturn != null &&
					Dionysos.Global.TranslationProvider != null &&
					EntityInformationUtil.TranslationRequired)
				{
					// FriendlyName
					string translationKey = null;
					string translation = null;

					translationKey = EntityInformationUtil.GetEntityInformationTranslationKey(toReturn.EntityName, "FriendlyName");
					if (!String.IsNullOrEmpty(translationKey))
					{
						translation = Dionysos.Global.TranslationProvider.GetTranslation(translationKey, false);
						if (!String.IsNullOrEmpty(translation))
						{
							toReturn.FriendlyName = translation;
						}
					}

					// FriendlyNamePlural
					translationKey = EntityInformationUtil.GetEntityInformationTranslationKey(toReturn.EntityName, "FriendlyNamePlural");
					if (!String.IsNullOrEmpty(translationKey))
					{
						translation = Dionysos.Global.TranslationProvider.GetTranslation(translationKey, false);
						if (!String.IsNullOrEmpty(translation))
						{
							toReturn.FriendlyNamePlural = translation;
						}
					}
				}

				if (!Instance.Empty(toReturn))
				{
					// Get the entity field information collection
					// for the specified entity name
					IEntityCollection entityFieldInformationCollection = EntityInformationUtil.GetEntityFieldInformationEntityCollection(entityName);

					// Now add all fields to the List
					toReturn.AllFields = new Dictionary<string, IEntityFieldInformation>();
					toReturn.DatabaseFields = new Dictionary<string, IEntityFieldInformation>();
					toReturn.CodeFields = new Dictionary<string, IEntityFieldInformation>();
					toReturn.PrimaryKeyFields = new Dictionary<string, IEntityFieldInformation>();
					toReturn.ForeignKeyFields = new Dictionary<string, IEntityFieldInformation>();
					toReturn.RelationInformation = new Dictionary<string, IEntityRelationInformation>();

					for (int i = 0; i < entityFieldInformationCollection.Count; i++)
					{
						IEntity entityFieldInformationEntity = entityFieldInformationCollection[i];
						IEntityFieldInformation current = entityFieldInformationEntity as IEntityFieldInformation;

						// Translate if Required
						if (Dionysos.Global.TranslationProvider != null &&
							EntityInformationUtil.TranslationRequired)
						{
							string translationKey = EntityInformationUtil.GetEntityFieldInformationTranslationKey(current, "FriendlyName");
							if (!String.IsNullOrEmpty(translationKey))
							{
								string translation = Dionysos.Global.TranslationProvider.GetTranslation(translationKey, false);
								if (!String.IsNullOrEmpty(translation))
								{
									current.FriendlyName = translation;
								}
							}
						}

						// Get the EntityField instance using the
						// fieldname from the entity field information
						IEntityField entityField = entity.Fields[current.FieldName];
						if (current.EntityFieldType == EntityInformationUtil.EntityFieldTypeCode)
						{
							// Code-Field, which is a custom property defined on the entity
							if (!Member.HasProperty(entity, current.FieldName))
							{
								// No property exists with that name
								//if (!(entity is IEntityFieldInformation)) // Prevent infinite recursion c.q. stack overflow
								//	entityFieldInformationEntity.Delete();
								continue;
							}
							else
							{
								// Add the field information to the EntityFields HashTable
							    if (!toReturn.AllFields.ContainsKey(current.FieldName))
							    {
							        toReturn.AllFields.Add(current.FieldName, current);
							        toReturn.CodeFields.Add(current.FieldName, current);
							    }
							}
						}
						else
						{
							if (entityField == null)
							{
								// No entity field was found
								//if (!(entity is IEntityFieldInformation)) // Prevent infinite recursion c.q. stack overflow
								//	entityFieldInformationEntity.Delete();
								continue;
							}
							else if (entityField.IsPrimaryKey)
							{
                                if (!toReturn.PrimaryKeyFields.ContainsKey(current.FieldName))
							    {
							        // Add the field information to the PrimaryKeys HashTable
							        toReturn.PrimaryKeyFields.Add(current.FieldName, current);
							    }
							}
							else if (entityField.IsForeignKey)
							{
								IEntityRelation relation = LLBLGenRelationUtil.GetRelation(entity, entityField.Name);
								if (Instance.Empty(relation))
								{
									// This just means to much information is added in the database, a field that's deleted or renamed, no hard error needed
								}
								else
								{
									IEntityFieldCore pkField = relation.GetPKEntityFieldCore(0) as IEntityFieldCore;
									IEntityFieldCore fkField = relation.GetFKEntityFieldCore(0) as IEntityFieldCore;
									IFieldPersistenceInfo pkPersistenceInfo = relation.GetPKFieldPersistenceInfo(0);
									IFieldPersistenceInfo fkPersistenceInfo = relation.GetFKFieldPersistenceInfo(0);

									IEntityInformation relatedEntityInformationEntity = EntityInformationUtil.GetEntityInformationEntity(pkPersistenceInfo.SourceObjectName);
									if (!Instance.Empty(relatedEntityInformationEntity))
									{
										// Get the friendly name from the related entity
										string relatedEntityInformationEntityFriendlyName = string.Empty;

										relatedEntityInformationEntityFriendlyName = relatedEntityInformationEntity.FriendlyName;

										// Translate if required
										if (Dionysos.Global.TranslationProvider != null &&
											EntityInformationUtil.TranslationRequired)
										{
											string translationKey = EntityInformationUtil.GetEntityInformationTranslationKey(pkPersistenceInfo.SourceObjectName, "FriendlyName");
											if (!String.IsNullOrEmpty(translationKey))
											{
												string translation = Dionysos.Global.TranslationProvider.GetTranslation(translationKey, false);
												if (!String.IsNullOrEmpty(translation))
												{
													relatedEntityInformationEntityFriendlyName = translation;
												}
											}
										}

										// Create a EntityRelationInformation instance
										EntityRelationInformation relationInformation = new EntityRelationInformation();
										relationInformation.RelatedEntityName = pkPersistenceInfo.SourceObjectName;
										relationInformation.RelatedEntityFriendlyName = relatedEntityInformationEntityFriendlyName;
										relationInformation.RelationType = relation.TypeOfRelation;
										relationInformation.MappedFieldName = relation.MappedFieldName;
										relationInformation.PrimaryKeyFieldName = pkField.Name;
										relationInformation.ForeignKeyFieldName = fkField.Name;

										if (!toReturn.RelationInformation.ContainsKey(pkPersistenceInfo.SourceObjectName))
										{
											// And the EntityRelationInformation instance to the Relations HashTable
											toReturn.RelationInformation.Add(pkPersistenceInfo.SourceObjectName, relationInformation);
										}

										// Set the friendly name to the field information item in the database
										((IEntityFieldInformation)entityFieldInformationEntity).EntityRelationInformation = relationInformation;
										entityFieldInformationEntity.SetNewFieldValue("FriendlyName", relatedEntityInformationEntityFriendlyName);
										entityFieldInformationEntity.Save();

										// And set the friendly name to the IEntityFieldInformation instance
										current.FriendlyName = relatedEntityInformationEntityFriendlyName;
									}
								}

							    if (!toReturn.ForeignKeyFields.ContainsKey(current.FieldName))
							    {
							        // Add the field information to the Foreign Keys HashTable
							        toReturn.ForeignKeyFields.Add(current.FieldName, current);
							    }
							}

						    if (!toReturn.AllFields.ContainsKey(current.FieldName))
						    {
						        // Add the field information to the EntityFields HashTable
						        toReturn.AllFields.Add(current.FieldName, current);
						        toReturn.DatabaseFields.Add(current.FieldName, current);
						    }
						}
					}

				    try
				    {
				        // Add to cache if possible
				        EntityInformationCache.AddOrUpdate(cacheKey, toReturn, (key, oldValue) => toReturn);
				        if (System.Web.HttpContext.Current != null)
				        {
				            System.Web.HttpContext.Current.Cache.Add(cacheKey, toReturn, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Normal, null);
				        }
				    }
				    catch (Exception)
				    {
				        // ignored
				    }
				}
				else
				{
					// GK don't error just return null and handle that yourself.
					// throw new Dionysos.TechnicalException("Er is geen EntityInformation gevonden voor Entity {0}", entityName);
				}
			}

			return toReturn;
		}

		/// <summary>
		/// Gets an IEntityInformation instance for the specified entity name
		/// </summary>
		/// <param name="entityName">The name of the entity to get the entity information for</param>
		/// <returns>An Dionysos.Interfaces.Data.IEntityInformation instance</returns>
		public static IEntityInformation GetEmptyEntityInformationInstance(string entityName)
		{
			IEntityInformation entityInformation = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else
			{
				entityInformation = GetEntityInformationEntity(entityName) as IEntityInformation;
			}

			return entityInformation;
		}

		/// <summary>
		/// Gets an IEntity instance for the EntityInformation entity using the specified entity name
		/// </summary>
		/// <param name="entityName">The name of the entity to get the EntityInformationEntity instance for</param>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance</returns>
		public static IEntityInformation GetEntityInformationEntity(string entityName)
		{
			IEntityInformation entityInformationEntity = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else
			{
				entityName = LLBLGenUtil.GetEntityName(entityName);
				entityInformationEntity = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression("EntityInformation", "EntityName", ComparisonOperator.Equal, entityName) as IEntityInformation;

				// Get Custom
				if (entityInformationEntity != null)
				{
					IEntity entityInformationCustomEntity = null;
					entityInformationCustomEntity = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression("EntityInformationCustom", "EntityName", ComparisonOperator.Equal, entityName);
					if (entityInformationCustomEntity != null && !entityInformationCustomEntity.IsNew)
						LLBLGen.LLBLGenEntityUtil.CopyFields(entityInformationCustomEntity, (IEntity)entityInformationEntity, new ArrayList(), false);
				}
			}

			return entityInformationEntity;
		}

		/// <summary>
		/// Gets an IEntityCollection instance for the EntityFieldInformation entity using the specified entity name
		/// </summary>
		/// <param name="entityName">The name of the entity to get the EntityFieldInformationEntity instance for</param>
		/// <returns>An SD.LLBGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
		public static IEntityCollection GetEntityFieldInformationEntityCollection(string entityName)
		{
			Trace.Assert(!Instance.Empty(entityName), "Parameter 'entityName' is empty.");

			IEntityCollection entityFieldInformationEntityCollection = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression("EntityFieldInformation", "EntityName", ComparisonOperator.Equal, entityName);

			// Retrieve any Custom values
			IEntityCollection entityFieldInformationCustomerEntityCollection = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression("EntityFieldInformationCustom", "EntityName", ComparisonOperator.Equal, entityName);

			for (int i = 0; i < entityFieldInformationCustomerEntityCollection.Count; i++)
			{
				IEntity currentCustom = entityFieldInformationCustomerEntityCollection[i];

				for (int j = 0; j < entityFieldInformationEntityCollection.Count; j++)
				{
					IEntity currentGeneral = entityFieldInformationEntityCollection[j];
					if (currentCustom.Fields["FieldName"].CurrentValue.ToString() == currentGeneral.Fields["FieldName"].CurrentValue.ToString())
					{
						// Copy values from the custom to the general
						LLBLGen.LLBLGenEntityUtil.CopyFields(currentCustom, currentGeneral, new ArrayList(), false);
					}
				}
			}

			return entityFieldInformationEntityCollection;
		}

		/// <summary>
		/// Gets an IEntityFieldInformation instance for the specified entity name and field name
		/// </summary>
		/// <param name="entityName">The name of the entity to get the entity field information for</param>
		/// <param name="fieldName">The name of the field to get the entity field information for</param>
		/// <returns>An Dionysos.Interfaces.Data.IEntityFieldInformation instance</returns>
		public static IEntityFieldInformation xGetEntityFieldInformation(string entityName, string fieldName)
		{
			IEntityFieldInformation entityFieldInformation = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
			{
				// Parameter 'fieldName' is empty
			}
			else
			{
				entityFieldInformation = xGetEntityFieldInformationEntity(entityName, fieldName) as IEntityFieldInformation;
			}

			return entityFieldInformation;
		}

		/// <summary>
		/// Gets an IEntity instance for the EntityFieldInformation entity using the specified entity name and field name
		/// </summary>
		/// <param name="entityName">The name of the entity to get the EntityFieldInformationEntity instance for</param>
		/// <param name="fieldName">The name of the field to get the EntityFieldInformationEntity instance for</param>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance</returns>
		public static IEntity xGetEntityFieldInformationEntity(string entityName, string fieldName)
		{
			IEntity entityFieldInformationEntity = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
			{
				// Parameter 'fieldName' is empty
			}
			else
			{
				entityFieldInformationEntity = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression("EntityFieldInformation", new string[] { "EntityName", "FieldName" }, new ComparisonOperator[] { ComparisonOperator.Equal, ComparisonOperator.Equal }, new object[] { entityName, fieldName });
			}

			return entityFieldInformationEntity;
		}

		/// <summary>
		/// Return the ShowField value as a string
		/// </summary>
		/// <param name="entity">Entity to retrieve the value for</param>
		/// <returns>Value or string.empty if not available</returns>
		public static string GetShowFieldNameValue(IEntity entity)
		{
			string toReturn = "";

			IEntityInformation info = Dionysos.Data.EntityInformationUtil.GetEntityInformation(entity);

			if (info.ShowFieldName.Length == 0)
				throw new Dionysos.TechnicalException("Fieldname is not set on '{0}'", info.EntityName);
			else if (entity.Fields[info.ShowFieldName] == null && !Member.HasProperty(entity, info.ShowFieldName))
				throw new Dionysos.TechnicalException("Fieldname '{0}' is not available as field or property on '{1}'", info.ShowFieldName, info.FriendlyName);
			else if (entity.Fields[info.ShowFieldName] != null)
				toReturn = entity.Fields[info.ShowFieldName].CurrentValue != null ? entity.Fields[info.ShowFieldName].CurrentValue.ToString() : "[FIELDNAME EMPTY]";
			else if (Member.HasProperty(entity, info.ShowFieldName))
				toReturn = (string)Member.InvokeProperty(entity, info.ShowFieldName);

			return toReturn;
		}

		/// <summary>
		/// Return the ShowFieldBreadCrumb value as a string, uses ShowFieldName as backup when no value for the breadcrumb is found
		/// </summary>
		/// <param name="entity">Entity to retrieve the value for</param>
		/// <returns>Value or string.empty if not available</returns>
		public static string GetShowFieldNameBreadCrumbValue(IEntity entity)
		{
			string toReturn = "";

			IEntityInformation info = Dionysos.Data.EntityInformationUtil.GetEntityInformation(entity);

			if (info.ShowFieldNameBreadCrumb.Length == 0 ||
				(entity.Fields[info.ShowFieldNameBreadCrumb] == null && !Member.HasProperty(entity, info.ShowFieldNameBreadCrumb)))
			{
				// No problem, second chance on GetShowFieldName
			}
			else if (entity.Fields[info.ShowFieldNameBreadCrumb] != null)
				toReturn = entity.Fields[info.ShowFieldNameBreadCrumb].CurrentValue != null ? entity.Fields[info.ShowFieldNameBreadCrumb].CurrentValue.ToString() : string.Empty;
			else if (Member.HasProperty(entity, info.ShowFieldNameBreadCrumb))
				toReturn = (string)Member.InvokeProperty(entity, info.ShowFieldNameBreadCrumb);

			try
			{
				if (toReturn.Length == 0)
					toReturn = GetShowFieldNameValue(entity);
			}
			catch (Exception ex)
			{
				throw new Dionysos.TechnicalException(ex, "Weergaveveld voor BreadCrumb mislukt voor '{0}', Weergave: '{1}', Weergave BreadCrumb: '{2}'",
					info.FriendlyName, info.ShowFieldName, info.ShowFieldNameBreadCrumb);
			}

			return toReturn;
		}

		#endregion

		#region EntityInformation Globalization Logic

		private static string CurrentUICultureLanguageCode
		{
			get
			{
				return Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.ToLower();
			}
		}

		/// <summary>
		/// Determine if Translation is required, this is only when current language is not Dutch.
		/// </summary>
		private static bool TranslationRequired
		{
			get
			{
				// GK Breaking change?
				if (Dionysos.Global.GlobalizationHelper == null)
					return !Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.Equals("nl", StringComparison.OrdinalIgnoreCase);
				else
					return Dionysos.Global.GlobalizationHelper.TranslationRequired;
			}
		}

		/// <summary>
		/// Retrieve the TranslationKey used for retrieving a translation for a certainfield contained in the EntityFieldInformationEntity
		/// </summary>
		/// <param name="entityFieldInformationEntity">The EntityFieldInformationEntity object containing the information</param>
		/// <param name="entityFieldInformationFieldName">The Field of the EntityFieldInformationEntity to retrieve the translation for</param>
		/// <returns></returns>
		public static string GetEntityFieldInformationTranslationKey(IEntityFieldInformation entityFieldInformationEntity, string entityFieldInformationFieldName)
		{
			return GetEntityFieldInformationTranslationKey(entityFieldInformationEntity.EntityName, entityFieldInformationEntity.FieldName, entityFieldInformationFieldName);
		}

		/// <summary>
		/// Retrieve the TranslationKey used for retrieving a translation for a certainfield contained in the EntityFieldInformationEntity
		/// </summary>
		/// <param name="entityName">Entity name to which the field belongs</param>
		/// <param name="fieldName">Fieldname of the field on the Entity</param>
		/// <param name="entityFieldInformationFieldName">FieldName of which you want to retrieve the translation from the EntityFieldInformation</param>
		/// <returns></returns>
		public static string GetEntityFieldInformationTranslationKey(string entityName, string fieldName, string entityFieldInformationFieldName)
		{
			return string.Format("EntityFieldInformation.{0}.{1}.{2}",
									entityName,
									fieldName,
									entityFieldInformationFieldName);
		}

		/// <summary>
		/// Retrieve the TranslationKey used for retrieving a translation for a certainfield contained in the EntityInformationEntity
		/// </summary>
		/// <param name="entityName">Entity Name</param>
		/// <param name="entityInformationFieldName">Field</param>
		/// <returns></returns>
		public static string GetEntityInformationTranslationKey(string entityName, string entityInformationFieldName)
		{
			entityName = LLBLGenUtil.GetEntityName(entityName);
			return string.Format("EntityInformation.{0}.{1}",
									entityName,
									entityInformationFieldName);
		}

		#endregion
	}
}