using System;
using System.IO;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
	/// <summary>
	/// Singleton utility class for performing general LLBLGen actions 
	/// </summary>
	public static class LLBLGenUtil
	{
		#region EntityType

		/// <summary>
		/// Gets a System.Enum instance containing the entity types
		/// </summary>
		/// <returns>A System.Enum instance containing the entity types</returns>
		public static Enum GetEntityTypeEnumerator()
		{
			Enum entityTypeEnumerator = null;

			Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
			if (Instance.Empty(dataAssemblyInfo))
			{
				// Data assembly does not exist
				// or is not added to the Global.AssemblyInfo
				throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
			}
			else
			{
				Assembly assembly = dataAssemblyInfo.Assembly;
				if (Instance.Empty(assembly))
				{
					// Assembly is empty
					throw new Dionysos.EmptyException("Data assembly is empty");
				}
				else
				{
					entityTypeEnumerator = GetEntityTypeEnumerator(assembly);
				}
			}

			return entityTypeEnumerator;
		}

		/// <summary>
		/// Gets a System.Enum instance containing the entity types
		/// </summary>
		/// <param name="assembly">The System.Reflection.Assembly instance to get the entity type enumerator from</param>
		/// <returns>A System.Enum instance containing the entity types</returns>
		public static Enum GetEntityTypeEnumerator(System.Reflection.Assembly assembly)
		{
			Enum entityTypeEnumerator = null;

			if (Instance.ArgumentIsEmpty(assembly, "assembly"))
			{
				// Parameter 'assembly' is empty
			}
			else
			{
				string fullTypeName = string.Format("{0}.EntityType", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""));

				System.Type type = assembly.GetType(fullTypeName);
				if (Instance.Empty(type))
				{
					// The type could not be found using the fullTypeName
					throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
				}
				else
				{
					entityTypeEnumerator = Dionysos.InstanceFactory.CreateInstance(assembly, type) as Enum;
				}
			}

			return entityTypeEnumerator;
		}

		#endregion

		#region Transaction

		/// <summary>
		/// Gets a ITransaction instance
		/// </summary>
		/// <param name="isolationLevel">Isolation Level</param>
		/// <param name="transactionName">TransactionName</param>
		/// <returns>A ITransaction instance</returns>
		public static ITransaction GetTransaction(System.Data.IsolationLevel isolationLevel, string transactionName)
		{
			ITransaction toReturn = null;

			Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
			if (Instance.Empty(dataAssemblyInfo))
			{
				// Data assembly does not exist
				// or is not added to the Global.AssemblyInfo
				throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
			}
			else
			{
				Assembly assembly = dataAssemblyInfo.Assembly;
				if (Instance.Empty(assembly))
				{
					// Assembly is empty
					throw new Dionysos.EmptyException("Data assembly is empty");
				}
				else
				{
					toReturn = GetTransaction(assembly, isolationLevel, transactionName);
				}
			}

			return toReturn;
		}

		/// <summary>
		/// Gets a ITransaction instance
		/// </summary>
		/// <param name="assembly">The System.Reflection.Assembly instance to get the ITransaction from</param>
		/// <param name="isolationLevel">Isolation Level</param>
		/// <param name="transactionName">TransactionName</param>
		/// <returns>A ITransaction instance</returns>
		public static ITransaction GetTransaction(System.Reflection.Assembly assembly, System.Data.IsolationLevel isolationLevel, string transactionName)
		{
			ITransaction toReturn = null;

			if (!Instance.ArgumentIsEmpty(assembly, "assembly"))
			{
				string fullTypeName = string.Format("{0}.HelperClasses.Transaction", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""));

				System.Type type = assembly.GetType(fullTypeName);
				if (Instance.Empty(type))
				{
					// The type could not be found using the fullTypeName
					throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
				}
				else
				{
					toReturn = Dionysos.InstanceFactory.CreateInstance(assembly, type, isolationLevel, transactionName) as ITransaction;
				}
			}

			return toReturn;
		}

		#endregion

		#region EntityName

		/// <summary>
		/// Gets the entity name of the specified entity instance without the 'Entity' postfix
		/// </summary>
		/// <param name="entity">The IEntity instance to get the entity name for</param>
		/// <returns>A System.String instance containing the entity name</returns>
		public static string GetEntityName(IEntity entity)
		{
			return GetEntityName(entity.LLBLGenProEntityName);
		}

		/// <summary>
		/// Gets the entity name of the specified string without the 'Entity' postfix
		/// </summary>
		/// <param name="entityName">The string instance to get the entity name for</param>
		/// <returns>A System.String instance containing the entity name</returns>
		public static string GetEntityName(string entityName)
		{
			if (entityName.EndsWith("Entity"))
			{
				int index = entityName.LastIndexOf("Entity");
				entityName = entityName.Substring(0, index);
			}

			return entityName;
		}

		/// <summary>
		/// Gets the names of the entity types from the specified assembly
		/// </summary>
		/// <returns>A System.String array containing the names of the entity types</returns>
		public static string[] GetEntityNames()
		{
			Enum entityTypeEnum = GetEntityTypeEnumerator();
			return Enum.GetNames(entityTypeEnum.GetType());
		}

		/// <summary>
		/// Gets the names of the entity types from the specified assembly
		/// </summary>
		/// <param name="assembly">The System.Reflection.Assmebly instance to get the entity type names from</param>
		/// <returns>A System.String array containing the names of the entity types</returns>
		public static string[] GetEntityNames(System.Reflection.Assembly assembly)
		{
			string[] entityNames = new string[0];

			if (Instance.ArgumentIsEmpty(assembly, "assembly"))
			{
				// Parameter 'assembly' is empty
			}
			else
			{
				Enum entityTypeEnum = GetEntityTypeEnumerator(assembly);
				entityNames = Enum.GetNames(entityTypeEnum.GetType());
			}

			return entityNames;
		}

		#endregion

		#region FieldIndex

		/// <summary>
		/// Gets a System.Enum instance containing the fieldindex for the specified entity
		/// </summary>
		/// <param name="entityName">The name of the entity to get the fieldindex enumerator for</param>
		/// <returns>A System.Enum instance containing the fieldindex</returns>
		public static Enum GetFieldIndexEnumerator(string entityName)
		{
			Enum fieldIndexEnumerator = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else
			{
				Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
				if (Instance.Empty(dataAssemblyInfo))
				{
					// Data assembly does not exist
					// or is not added to the Global.AssemblyInfo
					throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
				}
				else
				{
					Assembly assembly = dataAssemblyInfo.Assembly;
					if (Instance.Empty(assembly))
					{
						// Assembly is empty
						throw new Dionysos.EmptyException("Data assembly is empty");
					}
					else
					{
						fieldIndexEnumerator = GetFieldIndexEnumerator(entityName, assembly);
					}
				}
			}

			return fieldIndexEnumerator;
		}

		/// <summary>
		/// Gets a System.Enum instance containing the fieldindex for the specified entity
		/// </summary>
		/// <param name="entityName">The name of the entity to get the fieldindex enumerator for</param>
		/// <param name="assembly">The System.Reflection.Assembly instance to get the fieldindex enumerator from</param>
		/// <returns>A System.Enum instance containing the fieldindex</returns>
		public static Enum GetFieldIndexEnumerator(string entityName, System.Reflection.Assembly assembly)
		{
			Enum fieldIndexEnumerator = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(assembly, "assembly"))
			{
				// Parameter 'assembly' is empty
			}
			else
			{
				string fullTypeName = string.Format("{0}.{1}FieldIndex", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), LLBLGenUtil.GetEntityName(entityName));

				System.Type type = assembly.GetType(fullTypeName);
				if (Instance.Empty(type))
				{
					// The type could not be found using the fullTypeName
					throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
				}
				else
				{
					fieldIndexEnumerator = Dionysos.InstanceFactory.CreateInstance(assembly, type) as Enum;
				}
			}

			return fieldIndexEnumerator;
		}

		/// <summary>
		/// Gets a System.Int32 instance containg the fieldindex for the specified field from the specified entity name
		/// </summary>
		/// <param name="entityName">The name of the entity to get the fieldindex for</param>
		/// <param name="fieldName">The name of the field to get the fieldindex for</param>
		/// <returns>A System.Int32 instance containing the fieldindex</returns>
		public static int GetFieldIndex(string entityName, string fieldName)
		{
			int fieldIndex = -1;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
			{
				// Parameter 'fieldName' is empty
			}
			else
			{
				Enum fieldIndexEnum = GetFieldIndexEnumerator(entityName);
				Enum temp = (Enum)Enum.Parse(fieldIndexEnum.GetType(), fieldName);
				fieldIndex = Convert.ToInt32(temp);
			}

			return fieldIndex;
		}

		/// <summary>
		/// Gets a System.Int32 instance containg the fieldindex for the specified field from the specified entity name
		/// </summary>
		/// <param name="entityName">The name of the entity to get the fieldindex for</param>
		/// <param name="fieldName">The name of the field to get the fieldindex for</param>
		/// <param name="assembly">The System.Reflection.Assembly instance to get the fieldindex from</param>
		/// <returns>A System.Int32 instance containing the fieldindex</returns>
		public static int GetFieldIndex(string entityName, string fieldName, System.Reflection.Assembly assembly)
		{
			int fieldIndex = -1;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
			{
				// Parameter 'fieldName' is empty
			}
			else if (Instance.ArgumentIsEmpty(assembly, "assembly"))
			{
				// Parameter 'assembly' is empty
			}
			else
			{
				Enum fieldIndexEnum = GetFieldIndexEnumerator(entityName, assembly);
				Enum temp = (Enum)Enum.Parse(fieldIndexEnum.GetType(), fieldName);
				fieldIndex = Convert.ToInt32(temp);
			}

			return fieldIndex;
		}

		#endregion

		#region Referential Constraints

		/// <summary>
		/// Checks if the referential constraints are available
		/// </summary>
		public static void CheckReferentialConstraints()
		{
			// Create a dummy referential constraint entity
			// to use the fields for filtering
			IEntity referentialConstraintEntity = DataFactory.EntityFactory.GetEntity("ReferentialConstraint") as IEntity;

			if (Instance.Empty(referentialConstraintEntity))
			{
				throw new EmptyException("Variable 'referentialConstraintEntity' is empty.");
			}
			else
			{
				// Get the entity names
				string[] entityNames = GetEntityNames();

				// Walk through the entity names
				for (int i = 0; i < entityNames.Length; i++)
				{
					string entityName = GetEntityName(entityNames[i]);

					// Get an entity to get access to its fields
					// TODO: this solution is not very elegant
					IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
					if (Instance.Empty(entity))
					{
						throw new EmptyException("Variable 'entity' is empty.");
					}
					else
					{
						// Get the field names for the current entity
						string[] fields = DataFactory.EntityUtil.GetFields(entityName);

						// Walk through the field names
						for (int j = 0; j < fields.Length; j++)
						{
							string fieldName = fields[j];
							if (fieldName.Contains("_"))
							{
								// Field names which contain an "_" are 
								// inherited fieldnames using entity inheritance
							}
							else
							{
								// Get the corresponding IEntityField instance from the IEntity instance
								IEntityField entityField = entity.Fields[fieldName];
								if (Instance.Empty(entityField))
								{
									throw new EmptyException("Variable 'entityField' is empty.");
								}
								else
								{
									// Logic for referential constrains
									if (entityField.IsForeignKey)
									{
										// Found a foreign key
										// Now check if it already exists in the database
										IEntityCollection entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("ReferentialConstraint") as IEntityCollection;
										if (Instance.Empty(entityCollection))
										{
											throw new EmptyException("Variable 'entityCollection' is empty.");
										}
										else
										{
											PredicateExpression filter = new PredicateExpression();
											filter.Add(new FieldCompareValuePredicate(referentialConstraintEntity.Fields["EntityName"], ComparisonOperator.Equal, entityName));
											filter.Add(new FieldCompareValuePredicate(referentialConstraintEntity.Fields["ForeignKeyName"], ComparisonOperator.Equal, fieldName));

											entityCollection.GetMulti(filter);

											if (entityCollection.Count == 0)
											{
												IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("ReferentialConstraint") as IEntity;
												if (Instance.Empty(entityToInsert))
												{
													throw new EmptyException("Variable 'entityToInsert' is empty.");
												}
												else
												{
													entityToInsert.SetNewFieldValue("EntityName", entityName);
													entityToInsert.SetNewFieldValue("ForeignKeyName", fieldName);
													entityToInsert.Save();
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		#endregion

		#region PerfetchPath extensions

		/// <summary>
		/// Gets the IPrefetchPathElement for the specified entity type.
		/// </summary>
		/// <param name="prefetchPath">The IPrefetchPath to get the IPrefetchPathElement from.</param>
		/// <param name="entityType">The entity type.</param>
		/// <returns>The IPrefetchPathElement if found; otherwise null.</returns>
		public static IPrefetchPathElement Get(this IPrefetchPath prefetchPath, int entityType)
		{
			for (int i = 0; i < prefetchPath.Count; i++)
			{
				IPrefetchPathElement current = prefetchPath[i];
				if (current.ToFetchEntityType == entityType)
				{
					return current;
				}
			}

			return null;
		}

		/// <summary>
		/// Checks if the exception is a Prefetch error, if so creates an understandable error for prefetch error.
		/// </summary>
		/// <typeparam name="T">EntityType enum of the LLBLGen Data Project you're working with</typeparam>
		/// <param name="ex">The exception that might contain the prefetch error</param>
		/// <param name="understandableException">The understandable exception.</param>
		/// <returns></returns>
		public static bool TryGetUnderstandableErrorForPrefetchError<T>(Exception ex, out Exception understandableException) where T : struct, IComparable, IFormattable, IConvertible
		{
			understandableException = null;
			bool toReturn = false;

			if (ex.Message.Contains("prefetch path for root entity type"))
			{
				// Example error text: "The prefetch path element at index 0 in the passed in prefetch path for root entity type 13 is meant for root entity type 11 which isn't a subtype of 13. This means that you've added a prefetch path node to a Path of an unrelated entity, like adding OrderDetailsEntity.PrefetchPathProduct to a prefetch path for CustomerEntity."                        
				string errorText = ex.Message;
				errorText = ex.Message.Substring(ex.Message.IndexOf("prefetch path for root entity type ") + "prefetch path for root entity type ".Length);
				var actualEntityThatPathIsFor = Convert.ToInt32(errorText.Substring(0, errorText.IndexOf(" ")));
				errorText = ex.Message.Substring(ex.Message.IndexOf("is meant for root entity type ") + "is meant for root entity type ".Length);
				var incorrectEntityThatItIs = Convert.ToInt32(errorText.Substring(0, errorText.IndexOf(" ")));

				T pathIsMeantForEntityType = actualEntityThatPathIsFor.ToEnum<T>();
				T subElementIsMeantForEntity = incorrectEntityThatItIs.ToEnum<T>();

				understandableException = new Exception(StringUtil.FormatSafe("Prefetch Path is not working: You have added a prefetch path to the subpath of '{1}' that's meant for '{0}'. Correct it.", subElementIsMeantForEntity, pathIsMeantForEntityType), ex);
				toReturn = true;
			}

			return toReturn;
		}

		#endregion

		#region Crappy code

		/// <summary>
		/// Retrieves a list of all the entity names in an assembly
		/// </summary>
		/// <param name="assembly">The filename of the assembly containing the entity classes</param>
		/// <returns>An Arraylist containing the names of the entity classes</returns>
		//[Obsolete("This property is obsolete. Support may be discontinued in next version.")]
		public static ArrayList GetEntityNamesFromAssembly(string assembly)
		{
			// Create the arraylist where the entity names are being stored in
			ArrayList entityNamesList = null;

			if (assembly.Length == 0)
			{
				throw new ArgumentNullException("assembly");
			}
			else if (!File.Exists(assembly))
			{
				throw new FileNotFoundException(string.Format("File {0} does not exist!", assembly));
			}
			else
			{
				// Initialize the arraylist where the entity names are being stored in
				entityNamesList = new ArrayList();

				// The data assembly file exists
				string[] types = Dionysos.TypeUtil.GetTypeStringsFromAssembly(assembly);

				for (int i = 0; i < types.Length; i++)
				{
					string type = types[i];
					string entityName = GetEntityNameFromTypeString(type);
					if (entityName != string.Empty && entityName.ToLower() != "common")
					{
						Dionysos.Diagnostics.Debug.WriteLine(entityName as string);
						entityNamesList.Add(entityName as string);
					}
				}
			}

			return entityNamesList;
		}

		/// <summary>
		/// Retrieves the entity name of a type string
		/// </summary>
		/// <param name="type">The type to retrieve the entity name from</param>
		/// <returns>A string containing the entity name</returns>
		//[Obsolete("This property is obsolete. Support may be discontinued in next version.")]
		public static string GetEntityNameFromTypeString(string type)
		{
			string entityName = string.Empty;

			if (type.Length == 0)
			{
				throw new ArgumentNullException("type");
			}
			else
			{
				if (type.Contains("EntityClasses"))
				{
					string entityType = type;
					entityType = entityType.Replace("EntityClasses", "");

					int nIndex1 = entityType.IndexOf("..");
					int nIndex2 = entityType.IndexOf("Entity");

					entityName = entityType.Substring(nIndex1 + 2, nIndex2 - (nIndex1 + 2));
				}
			}

			return entityName;
		}

		#endregion
	}
}