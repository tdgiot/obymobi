using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Utility class for SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instances
    /// </summary>
    public class LLBLGenEntityCollectionUtil : Dionysos.Interfaces.IEntityCollectionUtil
    {
        #region Fields

        private LLBLGenPrefetchPathPool prefetchPathPool = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the EntityCollectionUtil class
        /// </summary>
        public LLBLGenEntityCollectionUtil()
        {
            this.Init();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static EntityCollectionUtil instance
        /// </summary>
        private void Init()
        {
            this.prefetchPathPool = new LLBLGenPrefetchPathPool();
        }

        #region Old shit?

        /// <summary>
        /// Refreshes the SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to refresh</param>
        public void Refresh(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection)
        {
            if (Instance.ArgumentIsEmpty(entityCollection, "entityCollection"))
            {
                // Parameter 'entityCollection' is empty
            }
            else
            {
                // Create some dummy vars to get the System.Type instances from
                SD.LLBLGen.Pro.ORMSupportClasses.IPredicate predicate = new SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression() as SD.LLBLGen.Pro.ORMSupportClasses.IPredicate;
                Int64 maxItemsToReturn = 0;
                SD.LLBLGen.Pro.ORMSupportClasses.ISortExpression sortExpression = new SD.LLBLGen.Pro.ORMSupportClasses.SortExpression() as SD.LLBLGen.Pro.ORMSupportClasses.ISortExpression;
                SD.LLBLGen.Pro.ORMSupportClasses.IRelationCollection relations = new SD.LLBLGen.Pro.ORMSupportClasses.RelationCollection() as SD.LLBLGen.Pro.ORMSupportClasses.RelationCollection;
                SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath prefetchPath = new SD.LLBLGen.Pro.ORMSupportClasses.PrefetchPath(0) as SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath;

                // Get the prefetchpath item from the prefetchpath pool
                Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem prefetchPathItem = this.prefetchPathPool[DataFactory.EntityCollectionUtil.GetEntityName(entityCollection), entityCollection.GetType().Assembly];

                // Invoke the GetMulti method to get the entity collection from the database
                Dionysos.Reflection.Member.InvokeMethod(entityCollection, "GetMulti", new System.Type[] { predicate.GetType(), maxItemsToReturn.GetType(), sortExpression.GetType(), relations.GetType(), prefetchPath.GetType() }, new object[] { null, 0, null, null, prefetchPathItem.PrefetchPath });
            }
        }

        /// <summary>
        /// Sort the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance using the specified field names
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to sort</param>
        /// <param name="fieldNames">The names of the fields to sort on (in the specified order)</param>
        public void Sort(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection, params string[] fieldNames)
        {
        }

        /// <summary>
        /// Sets a filter on the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance using the specified filter items
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to set the filter on</param>
        /// <param name="filterItems">A string array of filter items</param>
        public void SetFilter(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection, params string[] filterItems)
        {
        }

        /// <summary>
        /// Clears the current filter on the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to clear the filter from</param>
        public void ClearFilter(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection)
        {
        }

        /// <summary>
        /// Gets the entity name of the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to get the entity name from</param>
        /// <returns>A System.String containing the entity name</returns>
        public string GetEntityName(SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection)
        {
            string entityName = string.Empty;

            if (Instance.ArgumentIsEmpty(entityCollection, "entityCollection"))
            {
                // Parameter 'entityCollection' is empty
            }
            else
            {
                string typeString = TypeUtil.GetTypeString(entityCollection);
                int index = typeString.IndexOf("CollectionClasses.");
                typeString = typeString.Substring(index + 18);
                typeString = typeString.Replace("Collection", "");
                entityName = typeString;
            }

            return entityName;
        }

        #endregion

        /// <summary>
        /// Gets an IEntityCollection instance using the a FieldCompareValue predicate expression created by the specified arguments
        /// </summary>
        /// <param name="entityName">The name of the entity to get the entity collection for</param>
        /// <param name="fieldNames">The names of the fields which are used to compare the values with</param>
        /// <param name="comparisonOperators">The operators used for comparison</param>
        /// <param name="values">The values which are used for comparison</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection GetEntityCollectionUsingFieldCompareValuePredicateExpression(string entityName, string[] fieldNames, ComparisonOperator[] comparisonOperators, object[] values, ITransaction transaction)
        {
            IEntityCollection entityCollection = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else if (Instance.ArgumentIsEmpty(fieldNames, "fieldNames"))
            {
                // Parameter 'fieldNames' is empty
            }
            else if (Instance.ArgumentIsEmpty(values, "values"))
            {
                // Parameter 'values' is empty
            }
            else
            {
                PredicateExpression filter = LLBLGenFilterUtil.GetFieldCompareValuePredicateExpression(entityName, fieldNames, comparisonOperators, values);
                if (Instance.Empty(filter))
                {
                    throw new EmptyException("Variable 'filter' is empty.");
                }
                else
                {
                    entityCollection = GetEntityCollectionUsingPredicateExpression(entityName, filter, transaction);
                }

            }

            return entityCollection;
        }

        public static IEntityCollection GetEntityCollectionUsingFieldCompareValuePredicateExpression(string entityName, string[] fieldNames, ComparisonOperator[] comparisonOperators, object[] values)
        {
            return GetEntityCollectionUsingFieldCompareValuePredicateExpression(entityName, fieldNames, comparisonOperators, values, null);
        }

        public static IEntityCollection GetEntityCollectionUsingFieldLikePredicateExpression(string entityName, Dictionary<string, string> filterFieldsAndValues)
        {
            return GetEntityCollectionUsingFieldLikePredicateExpression(entityName, filterFieldsAndValues, null);
        }

        /// <summary>
        /// Gets an IEntityCollection instance using the a FieldCompareValue predicate expression created by the specified arguments
        /// </summary>
        /// <param name="entityName">The name of the entity to get the entity collection for</param>
        /// <param name="filterFieldsAndValues">The names of the fields to filter on with their filter values</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection GetEntityCollectionUsingFieldLikePredicateExpression(string entityName, Dictionary<string, string> filterFieldsAndValues, ITransaction transaction)
        {
            IEntityCollection entityCollection = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else if (Instance.ArgumentIsEmpty(filterFieldsAndValues, "filterFieldsAndValues"))
            {
                // Parameter 'values' is empty
            }
            else
            {
                PredicateExpression filterComplete = new PredicateExpression();
                foreach (var element in filterFieldsAndValues)
                {
                    filterComplete.Add(LLBLGenFilterUtil.GetFieldLikePredicate(entityName, element.Key, element.Value));
                }

                if (Instance.Empty(filterComplete))
                {
                    throw new EmptyException("Variable 'filter' is empty.");
                }
                else
                {
                    entityCollection = GetEntityCollectionUsingPredicateExpression(entityName, filterComplete, transaction);
                }

            }

            return entityCollection;
        }

        /// <summary>
        /// Gets an IEntityCollection instance using the a FieldCompareValue predicate expression created by the specified arguments
        /// </summary>
        /// <param name="entityName">The name of the entity to get the entity collection for</param>
        /// <param name="fieldNamesAndValues">Dictionary containing fields, and predicate information</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection GetEntityCollectionUsingFieldCompareValuePredicateExpression(string entityName, Dictionary<string, KeyValuePair<ComparisonOperator, object>> fieldNamesAndValues, ITransaction transaction)
        {
            string[] fields = new string[fieldNamesAndValues.Count];
            object[] values = new object[fieldNamesAndValues.Count];
            ComparisonOperator[] operators = new ComparisonOperator[fieldNamesAndValues.Count];
            int i = 0;
            foreach (var item in fieldNamesAndValues)
            {
                fields[i] = item.Key;
                values[i] = item.Value.Value;
                operators[i] = item.Value.Key;
                i++;
            }

            return GetEntityCollectionUsingFieldCompareValuePredicateExpression(entityName, fields, operators, values, transaction);
        }

        /// <summary>
        /// Gets an IEntityCollection instance using the a FieldCompareValue predicate expression created by the specified arguments
        /// </summary>
        /// <param name="entityName">The name of the entity to get the entity collection for</param>
        /// <param name="fieldName">The name of the field which are used to compare the value with</param>
        /// <param name="comparisonOperator">The operator used for comparison</param>
        /// <param name="value">The value which are used for comparison</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection GetEntityCollectionUsingFieldCompareValuePredicateExpression(string entityName, string fieldName, ComparisonOperator comparisonOperator, object value)
        {
            return GetEntityCollectionUsingFieldCompareValuePredicateExpression(entityName, new string[] { fieldName }, new ComparisonOperator[] { comparisonOperator }, new object[] { value }, null);
        }

        /// <summary>
        /// Gets an IEntityCollection instance using the a FieldCompareValue predicate expression created by the specified arguments
        /// </summary>
        /// <param name="entityName">The name of the entity to get the entity collection for</param>
        /// <param name="fieldName">The name of the field which are used to compare the value with</param>
        /// <param name="comparisonOperator">The operator used for comparison</param>
        /// <param name="value">The value which are used for comparison</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection GetEntityCollectionUsingFieldCompareValuePredicateExpression(string entityName, string fieldName, ComparisonOperator comparisonOperator, object value, ITransaction transaction)
        {
            return GetEntityCollectionUsingFieldCompareValuePredicateExpression(entityName, new string[] { fieldName }, new ComparisonOperator[] { comparisonOperator }, new object[] { value }, transaction);
        }

        /// <summary>
        /// Gets an IEntityCollection instance and executes GetMulti using the specified filter
        /// </summary>
        /// <param name="entityName">The name of the entity to get an entity collection for</param>
        /// <param name="filter">The PredicateExpression which contains the filter</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection GetEntityCollectionUsingPredicateExpression(string entityName, PredicateExpression filter, ITransaction transaction)
        {
            IEntityCollection entityCollection = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else if (Instance.ArgumentIsEmpty(filter, "filter"))
            {
                // Parameter 'filter' is empty
            }
            else
            {

                entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(entityName) as IEntityCollection;

                if (transaction != null)
                {
                    transaction.Add((ITransactionalElement)entityCollection);
                }

                if (Instance.Empty(entityCollection))
                {
                    throw new EmptyException("Variable 'entityCollection' is empty.");
                }
                else
                {
                    entityCollection.GetMulti(filter);
                }
            }

            return entityCollection;
        }

        /// <summary>
        /// Gets an IEntityCollection instance (does not execute GetMulti)
        /// </summary>
        /// <param name="entityName">The name of the entity to get an entity collection for</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection CreateEntityCollection(string entityName)
        {
            IEntityCollection entityCollection = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else
            {
                entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(entityName) as IEntityCollection;
                if (Instance.Empty(entityCollection))
                {
                    throw new EmptyException("Variable 'entityCollection' is empty.");
                }
            }

            return entityCollection;
        }

        public static IEntityCollection CreateEntityCollection<TEntity>()
            where TEntity : EntityBase
        {
            string typeName = typeof(TEntity).Name;
            if (typeName.EndsWith("Entity"))
            {
                typeName = typeName.Remove(typeName.Length - 6);
            }
            return GetEntityCollection(typeName);
        }

        /// <summary>
        /// Gets an IEntityCollection instance and executes GetMulti(null)
        /// </summary>
        /// <param name="entityName">The name of the entity to get an entity collection for</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection GetEntityCollection(string entityName)
        {
            return GetEntityCollection(entityName, null);
        }

        /// <summary>
        /// Gets an IEntityCollection instance and executes GetMulti(null)
        /// </summary>
        /// <param name="entityName">The name of the entity to get an entity collection for</param>
        /// <param name="getMultiFilter">Filter to be used in the GetMulti that's performed.</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static IEntityCollection GetEntityCollection(string entityName, PredicateExpression getMultiFilter)
        {
            IEntityCollection entityCollection = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else
            {
                entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(entityName) as IEntityCollection;
                if (Instance.Empty(entityCollection))
                {
                    throw new EmptyException("Variable 'entityCollection' is empty.");
                }
                else
                {
                    entityCollection.GetMulti(getMultiFilter);
                }
            }

            return entityCollection;
        }       


        /// <summary>
        /// Deletes all entities that match the filter. Will retrieve all entities and delete them one by one, so that
        /// all injectables code is ran. Also run's this in a transaction, so either everything succeeds or failes.
        /// Will throw exception when they occur.
        /// </summary>
        /// <param name="EntityName"></param>
        /// <param name="filter"></param>        
        public static void DeleteMultiValidated(string entityName, PredicateExpression filter)
        {
            LLBLGenEntityCollectionUtil.DeleteMultiValidated(entityName, filter, null, null);
        }

        /// <summary>
        /// Deletes all entities that match the filter. Will retrieve all entities and delete them one by one, so that
        /// all injectables code is ran. Also run's this in a transaction, so either everything succeeds or failes.
        /// Will throw exception when they occur.
        /// </summary>
        /// <param name="EntityName"></param>
        /// <param name="filter"></param>        
        public static void DeleteMultiValidated(string entityName, PredicateExpression filter, RelationCollection relations)
        {
            LLBLGenEntityCollectionUtil.DeleteMultiValidated(entityName, filter, relations, null);
        }

        /// <summary>
        /// Deletes all entities that match the filter. Will retrieve all entities and delete them one by one, so that
        /// all injectables code is ran. Also run's this in a transaction, so either everything succeeds or failes.
        /// Will throw exception when they occur.
        /// </summary>
        /// <param name="EntityName"></param>
        /// <param name="filter"></param>
        /// <param name="relations"></param>
        public static void DeleteMultiValidated(string entityName, PredicateExpression filter, RelationCollection relations = null, ITransaction transaction = null)
        {
            // Create a new transaction                        
            int lastAttemptedToDeleteEntityPk = -1;
            bool transactionIsLocal = (transaction == null);
            if (transactionIsLocal)
                transaction = LLBLGenUtil.GetTransaction(System.Data.IsolationLevel.ReadCommitted, "DeleteMultiValidated" + entityName + "-" + DateTime.UtcNow.DateTimeToSimpleDateTimeStamp());

            // Start working with the transaction
            try
            {
                IEntityCollection collection = LLBLGenEntityCollectionUtil.CreateEntityCollection(entityName);
                transaction.Add((ITransactionalElement)collection);
                collection.GetMulti(filter, relations);

                foreach (IEntity entity in collection)
                {
                    transaction.Add((ITransactionalElement)entity);
                    entity.Delete();
                }

                lastAttemptedToDeleteEntityPk = -2;

                // Commit the transaction
                if (transactionIsLocal)
                    transaction.Commit();
            }
            catch (Exception ex)
            {
                // Something went wrong, rollback the full transaction
                if (transactionIsLocal)
                    transaction.Rollback();

                string exceptionMessage = string.Format("DeleteMultiValidated for Collection of type: '{0}' at entity with PK: '{1}'",
                                                entityName, lastAttemptedToDeleteEntityPk);
                Exception newEx = new Exception(exceptionMessage, ex);

                throw newEx;
            }
            finally
            {
                // Dispose the Transaction to free the DB connection
                if (transactionIsLocal)
                    transaction.Dispose();
            }

        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Lynx_media.Data.LLBLGen.v1.PrefetchPathPool instance
        /// </summary>
        public LLBLGenPrefetchPathPool PrefetchPathPool
        {
            get
            {
                return this.prefetchPathPool;
            }
        }

        #endregion
    }
}