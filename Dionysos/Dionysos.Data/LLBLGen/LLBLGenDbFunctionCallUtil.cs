﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
	/// <summary>
	/// Utility class for DBFunctionCall.
	/// </summary>
	public static class LLBLGenDbFunctionCallUtil
	{
		/// <summary>
		/// Calculates the difference between the specified field and the value.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="value">The value.</param>
		/// <returns>
		/// A value ranging from 0 through 4: 0 indicates weak or no similarity, and 4 indicates strong similarity or the same values.
		/// </returns>
		public static EntityField Difference(IEntityField field, object value)
		{
			return field.SetExpression(new DbFunctionCall("DIFFERENCE", new object[] { field.Clone(), value }));
		}

		/// <summary>
		/// Calculates the soundex code for the specified field.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <returns>
		/// Returns a four-character (SOUNDEX) code to evaluate the similarity of two strings.
		/// </returns>
		public static EntityField SoundEx(IEntityField field)
		{
			return field.SetExpression(new DbFunctionCall("SOUNDEX", new object[] { field.Clone() }));
		}

		/// <summary>
		/// Replaces <c>null</c> values with the specified value.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="value">The value.</param>
		/// <returns>
		/// Returns the value of the field if it is not <c>null</c>; otherwise, the specified value.
		/// </returns>
		public static EntityField IsNull(IEntityField field, object value)
		{
			return field.SetExpression(new DbFunctionCall("ISNULL", new object[] { field.Clone(), value }));
		}

		/// <summary>
		/// Returns a value indicating whether the specified field has a value.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <returns>
		/// Returns a value indicating whether the specified field has a value.
		/// </returns>
		public static EntityField HasValue(IEntityField field)
		{
			return field.SetExpression(new DbFunctionCall("CASE WHEN {0} IS NOT NULL THEN 1 ELSE 0 END", new object[] { field.Clone() }));
		}

		/// <summary>
		/// Returns the left (first) part of the specified field with the specified number of characters.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="length">The length.</param>
		/// <returns>
		/// Returns the left (first) part of the specified field with the specified number of characters.
		/// </returns>
		public static EntityField Left(IEntityField field, int length)
		{
			return field.SetExpression(new DbFunctionCall("LEFT", new object[] { field.Clone(), length }));
		}

		/// <summary>
		/// Returns the right (last) part of the specified field with the specified number of characters.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="length">The length.</param>
		/// <returns>
		/// Returns the right (last) part of the specified field with the specified number of characters.
		/// </returns>
		public static EntityField Right(IEntityField field, int length)
		{
			return field.SetExpression(new DbFunctionCall("RIGHT", new object[] { field.Clone(), length }));
		}

		/// <summary>
		/// Returns the maximum value of the fields
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="field2">The secondary field.</param>
		/// <returns>
		/// Return the maximum value of the fields.
		/// </returns>
		public static EntityField Max(IEntityField field, IEntityField field2)
		{
			return field.SetExpression(new DbFunctionCall("CASE WHEN {0} >= {1} THEN {0} ELSE {1} END", new object[] { field.Clone(), field2.Clone() }));
		}

		/// <summary>
		/// Returns the length of the specified field.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <returns>
		/// Returns the length of the specified field.
		/// </returns>
		public static EntityField Length(IEntityField field)
		{
			return field.SetExpression(new DbFunctionCall("LEN", new object[] { field.Clone() }));
		}
	}
}
