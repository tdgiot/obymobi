using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Factory class for getting related SD.LLBLGen.Pro.ORMSupportClasses.IEntity instances with
    /// </summary>
    public class LLBLGenRelatedEntityFactory
    {
        #region Fields

        static LLBLGenRelatedEntityFactory instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the RelatedEntityFactory class if the instance has not been initialized yet
        /// </summary>
        public LLBLGenRelatedEntityFactory()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new LLBLGenRelatedEntityFactory();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static RelatedEntityFactory instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Gets a related SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance from an existing SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance
        /// </summary>
        /// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to get the related SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance from</param>
        /// <param name="relatedEntityName">The name of the related entity</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance</returns>
        public static SD.LLBLGen.Pro.ORMSupportClasses.IEntity GetRelatedEntity(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity, string relatedEntityName)
        {
            SD.LLBLGen.Pro.ORMSupportClasses.IEntity relatedEntity = Dionysos.Reflection.Member.InvokeProperty(entity, relatedEntityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
            if (Instance.Empty(relatedEntity))
            {
                throw new Dionysos.EmptyException("Variable 'relatedEntity' is empty");
            }
            return relatedEntity;
        }

        #endregion
    }
}