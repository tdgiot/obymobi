using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Factory class for creating IEntityCollection instances with
    /// </summary>
    public class LLBLGenEntityCollectionFactory : Dionysos.Interfaces.IEntityCollectionFactory
    {
        #region Fields

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Data.LLBLGen.LLBLGenEntityCollectionFactory class
        /// </summary>
        public LLBLGenEntityCollectionFactory()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new System.Object instance using the specified entityname from the default data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <returns>An System.Object instance containing the entity collection</returns>
        public object GetEntityCollection(string entityName)
        {
            SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityCollectionName' is empty
            }
            else
            {
                Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
                if (Instance.Empty(dataAssemblyInfo))
                {
                    // Data assembly does not exist
                    // or is not added to the Global.AssemblyInfo
                    throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
                }
                else
                {
                    // Get the assembly from the assembly information
                    Assembly assembly = dataAssemblyInfo.Assembly;

                    // And finally get the entity collection
                    entityCollection = GetEntityCollection(entityName, assembly) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
                }
            }

            return entityCollection;
        }

        /// <summary>
        /// Creates a new System.Object instance using the specified entityname from the specified data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <param name="assembly">The assembly which contains the entity type</param>
        /// <returns>An System.Object instance containing the entity collection</returns>
        public object GetEntityCollection(string entityName, Assembly assembly)
        {
            SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityCollectionName"))
            {
                // Parameter 'entityCollectionName' is empty
            }
            else if (Instance.ArgumentIsEmpty(assembly, "assembly"))
            {
                // Parameter 'assembly' is empty
            }
            else
            {
                entityName = LLBLGenUtil.GetEntityName(entityName);
                string fullTypeName = string.Format("{0}.CollectionClasses.{1}Collection", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), entityName);

                System.Type type = assembly.GetType(fullTypeName);
                if (type == null)
                {
                    // The type could not be found using the fullTypeName
                    throw new Dionysos.TechnicalException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
                }
                else
                {
                    entityCollection = Dionysos.InstanceFactory.CreateInstance(assembly, type) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
                }
            }

            return entityCollection;
        }

        #endregion
    }
}