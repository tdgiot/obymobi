using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Collection class used for storing Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instances in
    /// </summary>
    public class LLBLGenPrefetchPathPool : ICollection<Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the PrefetchPathPool class
        /// </summary>	
        public LLBLGenPrefetchPathPool()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance to the collection
        /// </summary>
        /// <param name="prefetchPath">The Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance to add to the collection</param>
        public void Add(Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem prefetchPath)
        {
            this.items.Add(prefetchPath);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance is already in the collection
        /// </summary>
        /// <param name="prefetchPath">The Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance to check</param>
        /// <returns>True if the Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem prefetchPath)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem) == prefetchPath)
                {
                    contains = true;
                    break;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance from this collection
        /// </summary>
        /// <param name="prefetchPath">The Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance to remove</param>
        public bool Remove(Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem prefetchPath)
        {
            this.items.Remove(prefetchPath);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.PrefetchPathPool
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.PrefetchPathPool
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets a Lynx_media.Data.LLBLGen.v1.PrefetchPathItem from the collection using the specified entity name
        /// </summary>
        /// <param name="entityName">The name of the entity to get the prefetch path for</param>
        /// <returns>A Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance</returns>
        private Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem GetPrefetchPath(string entityName)
        {
            Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem prefetchPathItem = null;

            // Walk through the prefetchpath items of this collection
            for (int i = 0; i < this.Count; i++)
            {
                Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem temp = this[i];
                if (temp.EntityName == entityName)
                {
                    prefetchPathItem = temp;
                    break;
                }
            }

            // Create a new prefetchpath item if no one is found
            if (prefetchPathItem == null)
            {
                prefetchPathItem = new LLBLGenPrefetchPathItem(entityName);
                this.Add(prefetchPathItem);
            }

            return prefetchPathItem;
        }

        /// <summary>
        /// Gets a Lynx_media.Data.LLBLGen.v1.PrefetchPathItem from the collection using the specified entity name
        /// </summary>
        /// <param name="entityName">The name of the entity to get the prefetch path for</param>
        /// <param name="assembly">The System.Reflection.Assembly to get the prefetchpath item from</param>
        /// <returns>A Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance</returns>
        private Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem GetPrefetchPath(string entityName, System.Reflection.Assembly assembly)
        {
            Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem prefetchPathItem = null;

            // Walk through the prefetchpath items of this collection
            for (int i = 0; i < this.Count; i++)
            {
                Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem temp = this[i];
                if (temp.EntityName == entityName)
                {
                    prefetchPathItem = temp;
                    break;
                }
            }

            // Create a new prefetchpath item if no one is found
            if (prefetchPathItem == null)
            {
                prefetchPathItem = new LLBLGenPrefetchPathItem(entityName, assembly);
                this.Add(prefetchPathItem);
            }

            return prefetchPathItem;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance to get</param>
        /// <returns>An Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance</returns>
        public Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance from the collection using the specified entity name
        /// </summary>
        /// <param name="entityName">The entity name of the entity collection of the Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance to get</param>
        /// <returns>An Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance</returns>
        public Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem this[string entityName]
        {
            get
            {
                return this.GetPrefetchPath(entityName);
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance from the collection using the specified entity name
        /// </summary>
        /// <param name="entityName">The entity name of the entity collection of the Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance to get</param>
        /// <param name="assembly">The System.Reflection.Assembly instance to get the prefetchpath item from</param>
        /// <returns>An Lynx_media.Data.LLBLGen.v1.PrefetchPathItem instance</returns>
        public Dionysos.Data.LLBLGen.LLBLGenPrefetchPathItem this[string entityName, System.Reflection.Assembly assembly]
        {
            get
            {
                return this.GetPrefetchPath(entityName, assembly);
            }
        }

        #endregion
    }
}