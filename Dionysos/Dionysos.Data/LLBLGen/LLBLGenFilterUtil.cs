﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
	/// <summary>
	/// Utility class which can be used to create LLBLGen filters
	/// </summary>
	public class LLBLGenFilterUtil
	{
		#region Fields

		static LLBLGenFilterUtil instance = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the LLBLGenFilterUtil class if the instance has not been initialized yet
		/// </summary>
		public LLBLGenFilterUtil()
		{
			if (instance == null)
			{
				// first create the instance
				instance = new LLBLGenFilterUtil();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the static LLBLGenFilterUtil instance
		/// </summary>
		private void Init()
		{
		}

		#region FieldCompareValue methods

		/// <summary>
		/// Gets a FieldCompareValuePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="comparisonOperator">The operator used for comparing the values</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <param name="negate">Flag which indicates whether the predicate should be negated</param>
		/// <example>
		/// Field = 3 
		/// Field != "Foo"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareValuePredicate instance</returns>
		public static FieldCompareValuePredicate GetFieldCompareValuePredicate(string entityName, string fieldName, ComparisonOperator comparisonOperator, object value, bool negate)
		{
			FieldCompareValuePredicate predicate = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
			{
				// Parameter 'fieldName' is empty
			}
			else if (Instance.ArgumentIsEmpty(value, "value"))
			{
				// Parameter 'value' is empty
			}
			else
			{
				IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
				if (Instance.Empty(entity))
				{
					throw new EmptyException("Entity of type '{0}' can't be created.", entityName);
				}
				else
				{
					IEntityField entityField = entity.Fields[fieldName];
					if (Instance.Empty(entityField))
					{
						throw new EmptyException("EntityField '{0}' is not found on Entity of Type '{1}'.", fieldName, entityName);
					}
					else
					{
						predicate = new FieldCompareValuePredicate(entityField, comparisonOperator, value, negate);
					}
				}
			}

			return predicate;
		}

		/// <summary>
		/// Gets a FieldCompareValuePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="comparisonOperator">The operator used for comparing the values</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <example>
		/// Field = 3 
		/// Field != "Foo"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareValuePredicate instance</returns>
		public static FieldCompareValuePredicate GetFieldCompareValuePredicate(string entityName, string fieldName, ComparisonOperator comparisonOperator, object value)
		{
			return GetFieldCompareValuePredicate(entityName, fieldName, comparisonOperator, value, false);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="comparisonOperators">The operators used for comparing the values</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <example>
		/// Field = 3 
		/// Field != "Foo"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValuePredicateExpression(string entityName, string[] fieldNames, ComparisonOperator[] comparisonOperators, object[] values)
		{
			return GetFieldCompareValuePredicateExpression(entityName, fieldNames, comparisonOperators, values, false);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="comparisonOperators">The operators used for comparing the values</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <param name="createOrPredicates">Create OR predicate instead of AND</param>
		/// <example>
		/// Field = 3 
		/// Field != "Foo"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValuePredicateExpression(string entityName, string[] fieldNames, ComparisonOperator[] comparisonOperators, object[] values, bool createOrPredicates)
		{
			PredicateExpression filter = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldNames, "fieldNames"))
			{
				// Parameter 'fieldNames' is empty
			}
			else if (Instance.ArgumentIsEmpty(comparisonOperators, "comparisonOperators"))
			{
				// Parameter 'comparisonOperators' is empty
			}
			else if (Instance.ArgumentIsEmpty(values, "values"))
			{
				// Parameter 'values' is empty
			}
			else if ((fieldNames.Length != comparisonOperators.Length) || (fieldNames.Length != values.Length) || (comparisonOperators.Length != values.Length))
			{
				throw new TechnicalException("The length of arrays 'fieldNames', 'comparisonOperators' and 'values' should be equal.");
			}
			else
			{
				filter = new PredicateExpression();

				for (int i = 0; i < fieldNames.Length; i++)
				{
					if (createOrPredicates && i != 0)
						filter.AddWithOr(GetFieldCompareValuePredicate(entityName, fieldNames[i], comparisonOperators[i], values[i]));
					else
						filter.AddWithAnd(GetFieldCompareValuePredicate(entityName, fieldNames[i], comparisonOperators[i], values[i]));
				}
			}

			return filter;
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="comparisonOperator">The operator used for comparing the values</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <example>
		/// Field = 3 
		/// Field != "Foo"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValuePredicateExpression(string entityName, string[] fieldNames, ComparisonOperator comparisonOperator, object[] values)
		{
			ComparisonOperator[] comparisonOperators = new ComparisonOperator[fieldNames.Length];
			for (int i = 0; i < fieldNames.Length; i++)
			{
				comparisonOperators[i] = comparisonOperator;
			}

			return GetFieldCompareValuePredicateExpression(entityName, fieldNames, comparisonOperators, values);

		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="comparisonOperator">The operator used for comparing the value</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <example>
		/// Field = 3 
		/// Field != "Foo"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValuePredicateExpression(string entityName, string fieldName, ComparisonOperator comparisonOperator, object value)
		{
			return GetFieldCompareValuePredicateExpression(entityName, new string[] { fieldName }, comparisonOperator, new object[] { value });
		}

		/// <summary>
		/// Gets a FieldCompareValuePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <example>
		/// Field = 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareValuePredicate instance</returns>
		public static FieldCompareValuePredicate GetFieldCompareValueEqualPredicate(string entityName, string fieldName, object value)
		{
			return GetFieldCompareValuePredicate(entityName, fieldName, ComparisonOperator.Equal, value);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified condition
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldName">The names of the fields to compares the values for</param>
		/// <param name="value">The values for comparison with the field values</param>
		/// <example>
		/// Field = 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValueEqualPredicateExpression(string entityName, string fieldName, object value)
		{
			return GetFieldCompareValueEqualPredicateExpression(entityName, new string[] { fieldName }, new object[] { value });
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <example>
		/// Field = 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValueEqualPredicateExpression(string entityName, string[] fieldNames, object[] values)
		{
			return GetFieldCompareValuePredicateExpression(entityName, fieldNames, ComparisonOperator.Equal, values);
		}

		/// <summary>
		/// Gets a FieldCompareValuePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <example>
		/// Field >= 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareValuePredicate instance</returns>
		public static FieldCompareValuePredicate GetFieldCompareValueGreaterEqualPredicate(string entityName, string fieldName, object value)
		{
			return GetFieldCompareValuePredicate(entityName, fieldName, ComparisonOperator.GreaterEqual, value);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <example>
		/// Field >= 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareGreaterEqualPredicateExpression(string entityName, string[] fieldNames, object[] values)
		{
			return GetFieldCompareValuePredicateExpression(entityName, fieldNames, ComparisonOperator.GreaterEqual, values);
		}

		/// <summary>
		/// Gets a FieldCompareValuePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <example>
		/// Field > 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareValuePredicate instance</returns>
		public static FieldCompareValuePredicate GetFieldCompareValueGreaterThanPredicate(string entityName, string fieldName, object value)
		{
			return GetFieldCompareValuePredicate(entityName, fieldName, ComparisonOperator.GreaterThan, value);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <example>
		/// Field > 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValueGreaterThanPredicateExpression(string entityName, string[] fieldNames, object[] values)
		{
			return GetFieldCompareValuePredicateExpression(entityName, fieldNames, ComparisonOperator.GreaterThan, values);
		}

		/// <summary>
		/// Gets a FieldCompareValuePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <example>
		/// Field lesser than 3
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareValuePredicate instance</returns>
		public static FieldCompareValuePredicate GetFieldCompareValueLessEqualPredicate(string entityName, string fieldName, object value)
		{
			return GetFieldCompareValuePredicate(entityName, fieldName, ComparisonOperator.LessEqual, value);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <example>
		/// Field lesser than 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValueLessEqualPredicateExpression(string entityName, string[] fieldNames, object[] values)
		{
			return GetFieldCompareValuePredicateExpression(entityName, fieldNames, ComparisonOperator.LessEqual, values);
		}

		/// <summary>
		/// Gets a FieldCompareValuePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <example>
		/// Field lesser equal 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareValuePredicate instance</returns>
		public static FieldCompareValuePredicate GetFieldCompareValueLesserThanPredicate(string entityName, string fieldName, object value)
		{
			return GetFieldCompareValuePredicate(entityName, fieldName, ComparisonOperator.LesserThan, value);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <example>
		/// Field lesser equal 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValueLesserThanPredicateExpression(string entityName, string[] fieldNames, object[] values)
		{
			return GetFieldCompareValuePredicateExpression(entityName, fieldNames, ComparisonOperator.LesserThan, values);
		}

		/// <summary>
		/// Gets a FieldCompareValuePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="value">The value for comparison with the field value</param>
		/// <example>
		/// Field != 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareValuePredicate instance</returns>
		public static FieldCompareValuePredicate GetFieldCompareValueNotEqualPredicate(string entityName, string fieldName, object value)
		{
			return GetFieldCompareValuePredicate(entityName, fieldName, ComparisonOperator.NotEqual, value);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareValuePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="values">The values for comparison with the field values</param>
		/// <example>
		/// Field != 3 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareValueNotEqualPredicateExpression(string entityName, string[] fieldNames, object[] values)
		{
			return GetFieldCompareValuePredicateExpression(entityName, fieldNames, ComparisonOperator.NotEqual, values);
		}

		#endregion

		#region FieldCompareNull methods

		/// <summary>
		/// Gets a FieldCompareNullPredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="negate">Flag which indicates whether the predicate should be negated</param>
		/// <example>
		/// Field Is NULL 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareNullPredicate instance</returns>
		public static FieldCompareNullPredicate GetFieldCompareNullPredicate(string entityName, string fieldName, bool negate)
		{
			FieldCompareNullPredicate predicate = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
			{
				// Parameter 'fieldName' is empty
			}
			else
			{
				IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
				if (Instance.Empty(entity))
				{
					throw new EmptyException("Variable 'entity' is empty.");
				}
				else
				{
					IEntityField entityField = entity.Fields[fieldName];
					if (Instance.Empty(entityField))
					{
						throw new EmptyException("Variable 'entityField' is empty.");
					}
					else
					{
						predicate = new FieldCompareNullPredicate(entityField, negate);
					}
				}
			}

			return predicate;
		}

		/// <summary>
		/// Gets a FieldCompareNullPredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <example>
		/// Field Is NULL 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareNullPredicate instance</returns>
		public static FieldCompareNullPredicate GetFieldCompareNullPredicate(string entityName, string fieldName)
		{
			return GetFieldCompareNullPredicate(entityName, fieldName, false);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareNullPredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="negates">Flags which indicates whether the predicates should be negated</param>
		/// <example>
		/// Field Is NULL 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareNullPredicateExpression(string entityName, string[] fieldNames, bool[] negates)
		{
			PredicateExpression filter = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldNames, "fieldNames"))
			{
				// Parameter 'fieldNames' is empty
			}
			else if (fieldNames.Length != negates.Length)
			{
				throw new TechnicalException("The length of arrays 'fieldNames' and 'negates' should be equal.");
			}
			else
			{
				filter = new PredicateExpression();

				for (int i = 0; i < fieldNames.Length; i++)
				{
					filter.Add(GetFieldCompareNullPredicate(entityName, fieldNames[i], negates[i]));
				}
			}

			return filter;
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldCompareNullPredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <example>
		/// Field Is NULL 
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldCompareNullPredicateExpression(string entityName, string[] fieldNames)
		{
			bool[] negates = new bool[fieldNames.Length];
			for (int i = 0; i < fieldNames.Length; i++)
			{
				negates[i] = false;
			}

			return GetFieldCompareNullPredicateExpression(entityName, fieldNames, negates);
		}

		#endregion

		#region FieldLike methods

		/// <summary>
		/// Gets a FieldLikePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="pattern">The pattern to compare in the like functions</param>
		/// <param name="negate">Flag which indicates whether the predicate should be negated</param>
		/// <example>
		/// Field LIKE "Foo%"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareNullPredicate instance</returns>
		public static FieldLikePredicate GetFieldLikePredicate(string entityName, string fieldName, string pattern, bool negate)
		{
			FieldLikePredicate predicate = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
			{
				// Parameter 'fieldName' is empty
			}
			else if (Instance.ArgumentIsEmpty(pattern, "pattern"))
			{
				// Parameter 'pattern' is empty
			}
			else
			{
				IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
				if (Instance.Empty(entity))
				{
					throw new EmptyException("Variable 'entity' is empty.");
				}
				else
				{
					IEntityField entityField = entity.Fields[fieldName];
					if (Instance.Empty(entityField))
					{
						throw new EmptyException("Variable 'entityField' is empty.");
					}
					else
					{
						predicate = new FieldLikePredicate(entityField, pattern, negate);
					}
				}
			}

			return predicate;
		}

		/// <summary>
		/// Gets a FieldLikePredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="pattern">The pattern to compare in the like functions</param>
		/// <example>
		/// Field LIKE "Foo%"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.FieldCompareNullPredicate instance</returns>
		public static FieldLikePredicate GetFieldLikePredicate(string entityName, string fieldName, string pattern)
		{
			return GetFieldLikePredicate(entityName, fieldName, pattern, false);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldLikePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="patterns">The patterns to compare in the like functions</param>
		/// <param name="negates">Flags which indicates whether the predicates should be negated</param>
		/// <example>
		/// Field LIKE "Foo%"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldLikePredicateExpression(string entityName, string[] fieldNames, string[] patterns, bool[] negates)
		{
			PredicateExpression filter = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldNames, "fieldNames"))
			{
				// Parameter 'fieldNames' is empty
			}
			else if (Instance.ArgumentIsEmpty(patterns, "patterns"))
			{
				// Parameter 'patterns' is empty
			}
			else if ((fieldNames.Length != negates.Length) || (fieldNames.Length != patterns.Length) || (patterns.Length != negates.Length))
			{
				throw new TechnicalException("The length of arrays 'fieldNames', 'patterns' and 'negates' should be equal.");
			}
			else
			{
				filter = new PredicateExpression();

				for (int i = 0; i < fieldNames.Length; i++)
				{
					filter.Add(GetFieldLikePredicate(entityName, fieldNames[i], patterns[i], negates[i]));
				}
			}

			return filter;
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldLikePredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="patterns">The patterns to compare in the like functions</param>
		/// <example>
		/// Field LIKE "Foo%"
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldLikePredicateExpression(string entityName, string[] fieldNames, string[] patterns)
		{
			bool[] negates = new bool[fieldNames.Length];
			for (int i = 0; i < fieldNames.Length; i++)
			{
				negates[i] = false;
			}

			return GetFieldLikePredicateExpression(entityName, fieldNames, patterns, negates);
		}

		#endregion

		#region FieldBetweenPredicate method

		/// <summary>
		/// Gets a FieldBetweenPredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="valueBegin">The start value of the between range</param>
		/// <param name="valueEnd">The end value of the between range</param>
		/// <param name="negate">Flag which indicates whether the pedicate should be negated</param>
		/// <example>
		/// Field BETWEEN 3 AND 5
		/// Field BETWEEN field2 AND 4
		/// </example>
		/// <returns>A SD.LLBLGen.Pro.ORMSupportClasses.FieldBetweenPredicate instance</returns>
		public static FieldBetweenPredicate GetFieldBetweenPredicate(string entityName, string fieldName, object valueBegin, object valueEnd, bool negate)
		{
			FieldBetweenPredicate predicate = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
			{
				// Parameter 'fieldName' is empty
			}
			else if (Instance.ArgumentIsEmpty(valueBegin, "valueBegin"))
			{
				// Parameter 'valueBegin' is empty
			}
			else if (Instance.ArgumentIsEmpty(valueEnd, "valueEnd"))
			{
				// Parameter 'valueEnd' is empty
			}
			else
			{
				IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
				if (Instance.Empty(entity))
				{
					throw new EmptyException("Variable 'entity' is empty.");
				}
				else
				{
					IEntityField entityField = entity.Fields[fieldName];
					if (Instance.Empty(entityField))
					{
						throw new EmptyException("Variable 'entityField' is empty.");
					}
					else
					{
						predicate = new FieldBetweenPredicate(entityField, valueBegin, valueEnd, negate);
					}
				}
			}

			return predicate;
		}

		/// <summary>
		/// Gets a FieldBetweenPredicate instance which can be used in PredicateExpression filters
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the field to create the predicate for</param>
		/// <param name="fieldName">The name of the field to compare the value for</param>
		/// <param name="valueBegin">The start value of the between range</param>
		/// <param name="valueEnd">The end value of the between range</param>
		/// <example>
		/// Field BETWEEN 3 AND 5
		/// Field BETWEEN field2 AND 4
		/// </example>
		/// <returns>A SD.LLBLGen.Pro.ORMSupportClasses.FieldBetweenPredicate instance</returns>
		public static FieldBetweenPredicate GetFieldBetweenPredicate(string entityName, string fieldName, object valueBegin, object valueEnd)
		{
			return GetFieldBetweenPredicate(entityName, fieldName, valueBegin, valueEnd, false);
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldBetweenPredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="valuesBegin">The start values of the between ranges</param>
		/// <param name="valuesEnd">The end values of the between ranges</param>
		/// <param name="negates">Flags which indicates whether the predicates should be negated</param>
		/// <example>
		/// Field BETWEEN 3 AND 5
		/// Field BETWEEN field2 AND 4
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldBetweenPredicateExpression(string entityName, string[] fieldNames, object[] valuesBegin, object[] valuesEnd, bool[] negates)
		{
			PredicateExpression filter = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldNames, "fieldNames"))
			{
				// Parameter 'fieldNames' is empty
			}
			else if (Instance.ArgumentIsEmpty(valuesBegin, "valuesBegin"))
			{
				// Parameter 'valuesBegin' is empty
			}
			else if (Instance.ArgumentIsEmpty(valuesEnd, "valuesEnd"))
			{
				// Parameter 'valuesEnd' is empty
			}
			else if ((fieldNames.Length != valuesBegin.Length) || (fieldNames.Length != valuesEnd.Length) || (fieldNames.Length != negates.Length) || (valuesBegin.Length != valuesEnd.Length) || (valuesBegin.Length != negates.Length) || (valuesEnd.Length != negates.Length))
			{
				throw new TechnicalException("The length of arrays 'fieldNames', 'valuesBegin', 'valuesEnd' and 'negates' should be equal.");
			}
			else
			{
				filter = new PredicateExpression();

				for (int i = 0; i < fieldNames.Length; i++)
				{
					filter.Add(GetFieldBetweenPredicate(entityName, fieldNames[i], valuesBegin[i], valuesEnd[i], negates[i]));
				}
			}

			return filter;
		}

		/// <summary>
		/// Gets a PredicateExpression instance containing FieldBetweenPredicates for the specified conditions
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the fields to create the predicates for</param>
		/// <param name="fieldNames">The names of the fields to compares the values for</param>
		/// <param name="valuesBegin">The start values of the between ranges</param>
		/// <param name="valuesEnd">The end values of the between ranges</param>
		/// <example>
		/// Field BETWEEN 3 AND 5
		/// Field BETWEEN field2 AND 4
		/// </example>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression instance</returns>
		public static PredicateExpression GetFieldBetweenPredicateExpression(string entityName, string[] fieldNames, object[] valuesBegin, object[] valuesEnd)
		{
			bool[] negates = new bool[fieldNames.Length];
			for (int i = 0; i < fieldNames.Length; i++)
			{
				negates[i] = false;
			}

			return GetFieldBetweenPredicateExpression(entityName, fieldNames, valuesBegin, valuesEnd, negates);
		}

		#endregion

		#endregion
	}
}