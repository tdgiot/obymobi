﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace System
{
    /// <summary>
    /// Class for easily adding Entities and EntityCollections to an transaction
    /// </summary>
    public static class LLBLGenTransactionUtil
    {
        /// <summary>
        /// Get the current transaction of the Transactional Element
        /// </summary>
        /// <param name="transactionalElement">Get the current transaction of the Transactional Element</param>
        public static ITransaction GetCurrentTransaction(this IEntity transactionalElement)
        {
            return ((ITransactionalElement)transactionalElement).Transaction;
        }

        /// <summary>
        /// Get the current transaction of the Transactional Element
        /// </summary>
        /// <param name="transactionalElement">Get the current transaction of the Transactional Element</param>
        /// <returns></returns>
        public static ITransaction GetCurrentTransaction(this IEntityCollection transactionalElement)
        {
            return ((ITransactionalElement)transactionalElement).Transaction;
        }

        /// <summary>
        /// Adds the Transactional Element to the Transaction 
        /// </summary>
        /// <param name="transactionalElement">Element to make part of the Transaction</param>
        /// <param name="transaction">Transaction to add the element to, it allowed to be null</param>
        /// <returns>The supplied Transaction (could be null)</returns>
        public static ITransaction AddToTransaction(this ITransactionalElement transactionalElement, ITransaction transaction)
        {
            if (transaction != null)
                transaction.Add(transactionalElement);

            return transaction;
        }

        /// <summary>
        /// Adds the Participating Transaction Element to the Transaction of the Existing Transactional Element
        /// </summary>
        /// <param name="participatingElement">The element that should be made participating in the Transaction</param>
        /// <param name="existingTransactionElement">The element already part of the Transaction</param>
        /// <returns>The Transaction (could be null)</returns>
        private static ITransaction AddToTransaction(this ITransactionalElement participatingElement, ITransactionalElement existingTransactionElement)
        {
            if (existingTransactionElement.Transaction != null)
                existingTransactionElement.Transaction.Add(participatingElement);

            return existingTransactionElement.Transaction;
        }

        ///// <summary>
        ///// Adds the Transactional Element to the Transaction 
        ///// </summary>
        ///// <param name="transactionalElement">Element to make part of the Transaction</param>
        ///// <param name="transaction">Transaction to add the element to, it allowed to be null</param>
        ///// <returns>The supplied Transaction (could be null)</returns>
        //public static ITransaction AddToTransaction(this IEntityCollection transactionalElement, ITransactionalElement existingTransactionElement)
        //{
        //    return AddToTransaction((ITransactionalElement)transactionalElement, (ITransactionalElement)existingTransactionElement);
        //}

        /// <summary>
        /// Adds the Transactional Element to the Transaction 
        /// </summary>
        /// <param name="transactionalElement">Element to make part of the Transaction</param>
        /// <param name="transaction">Transaction to add the element to, it allowed to be null</param>
        /// <returns>The supplied Transaction (could be null)</returns>
        public static ITransaction AddToTransaction(this IEntityCollection transactionalElement, IEntity existingTransactionElement)
        {
            return LLBLGenTransactionUtil.AddToTransaction((ITransactionalElement)transactionalElement, (ITransactionalElement)existingTransactionElement);
        }

        /// <summary>
        /// Adds the Transactional Element to the Transaction 
        /// </summary>
        /// <param name="transactionalElement">Element to make part of the Transaction</param>
        /// <param name="transaction">Transaction to add the element to, it allowed to be null</param>
        /// <returns>The supplied Transaction (could be null)</returns>
        public static ITransaction AddToTransaction(this IEntityCollection transactionalElement, IEntityCollection existingTransactionElement)
        {
            return LLBLGenTransactionUtil.AddToTransaction((ITransactionalElement)transactionalElement, (ITransactionalElement)existingTransactionElement);
        }

        /// <summary>
        /// Adds the Transactional Element to the Transaction 
        /// </summary>
        /// <param name="transactionalElement">Element to make part of the Transaction</param>
        /// <param name="transaction">Transaction to add the element to, it allowed to be null</param>
        /// <returns>The supplied Transaction (could be null)</returns>
        public static ITransaction AddToTransaction(this IEntity transactionalElement, IEntity existingTransactionElement)
        {
            return LLBLGenTransactionUtil.AddToTransaction((ITransactionalElement)transactionalElement, (ITransactionalElement)existingTransactionElement);
        }

        /// <summary>
        /// Adds the Transactional Element to the Transaction 
        /// </summary>
        /// <param name="transactionalElement">Element to make part of the Transaction</param>
        /// <param name="transaction">Transaction to add the element to, it allowed to be null</param>
        /// <returns>The supplied Transaction (could be null)</returns>
        public static ITransaction AddToTransaction(this IEntity transactionalElement, IEntityCollection existingTransactionElement)
        {
            return LLBLGenTransactionUtil.AddToTransaction((ITransactionalElement)transactionalElement, (ITransactionalElement)existingTransactionElement);
        }
    }
}
