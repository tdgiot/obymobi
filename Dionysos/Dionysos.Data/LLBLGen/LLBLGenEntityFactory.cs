using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.LLBLGen
{
	/// <summary>
	/// Factory class for creating SD.LLBLGen.Pro.ORMSupportClasses.IEntity instances with
	/// </summary>
	public class LLBLGenEntityFactory : Dionysos.Interfaces.IEntityFactory
	{
		#region Fields

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the EntityFactory class if the instance has not been initialized yet
		/// </summary>
		public LLBLGenEntityFactory()
		{
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the static EntityFactory instance
		/// </summary>
		private void Init()
		{
		}

		/// <summary>
		/// Creates a new System.Object instance using the specified entityname from the default data assembly
		/// </summary>
		/// <param name="entityName">The name of the entity to create</param>
		/// <returns>An IEntity instance</returns>
		public object GetEntity(string entityName)
		{
			SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = null;
            
		    if (Instance.ArgumentIsEmpty(entityName, "entityName") || String.IsNullOrEmpty(entityName))
			{
				// Parameter 'entityName' is empty
			}
			else
			{
				Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
				if (Instance.Empty(dataAssemblyInfo))
				{
					// Data assembly does not exist
					// or is not added to the Global.AssemblyInfo
					throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
				}
				else
				{
					Assembly assembly = dataAssemblyInfo.Assembly;
					if (Instance.Empty(assembly))
					{
						// Assembly is empty
						throw new Dionysos.EmptyException("Data assembly is empty");
					}
					else
					{
						entityName = LLBLGenUtil.GetEntityName(entityName);
						string fullTypeName = string.Format("{0}.EntityClasses.{1}Entity", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), entityName);

						System.Type type = assembly.GetType(fullTypeName);
						if (type == null)
						{
							// The type could not be found using the fullTypeName
							throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
						}
						else
						{
							entity = Dionysos.InstanceFactory.CreateInstance(assembly, type) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
						}
					}
				}
			}

			return entity;
		}


		///// <summary>
		///// Creates a new System.Object instance using the specified DaoName from the default data assembly
		///// </summary>
		///// <param name="entityName">The name of the entity to create the DaoClass for</param>
		///// <returns>An IEntity instance</returns>
		//public object GetDao(string entityName)
		//{
		//    SD.LLBLGen.Pro.ORMSupportClasses.IDao dao = null;

		//    if (Instance.ArgumentIsEmpty(entityName, "entityName"))
		//    {
		//        // Parameter 'entityName' is empty
		//    }
		//    else
		//    {
		//        Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
		//        if (Instance.Empty(dataAssemblyInfo))
		//        {
		//            // Data assembly does not exist
		//            // or is not added to the Global.AssemblyInfo
		//            throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
		//        }
		//        else
		//        {
		//            Assembly assembly = dataAssemblyInfo.Assembly;
		//            if (Instance.Empty(assembly))
		//            {
		//                // Assembly is empty
		//                throw new Dionysos.EmptyException("Data assembly is empty");
		//            }
		//            else
		//            {
		//                if (entityName.EndsWith("Entity"))
		//                {
		//                    entityName = entityName.Replace("Entity", "");
		//                }

		//                string fullTypeName = string.Format("{0}.DaoClasses.{1}DAO", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), entityName);

		//                System.Type type = assembly.GetType(fullTypeName);
		//                if (type == null)
		//                {
		//                    // The type could not be found using the fullTypeName
		//                    throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
		//                }
		//                else
		//                {
		//                    dao = Dionysos.InstanceFactory.CreateInstance(assembly, type) as SD.LLBLGen.Pro.ORMSupportClasses.IDao;
		//                }
		//            }
		//        }
		//    }

		//    return dao;
		//}

		/// <summary>
		/// Creates a new System.Object instance using the specified entityname from the specified data assembly
		/// </summary>
		/// <param name="entityName">The name of the entity to create</param>
		/// <param name="assembly">The assembly which contains the entity type</param>
		/// <returns>An System.Object instance</returns>
		public object GetEntity(string entityName, Assembly assembly)
		{
			SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(assembly, "assembly"))
			{
				// Parameter 'assembly' is empty
			}
			else
			{
				entityName = LLBLGenUtil.GetEntityName(entityName);
				string fullTypeName = string.Format("{0}.EntityClasses.{1}Entity", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), entityName);

				System.Type type = assembly.GetType(fullTypeName);
				if (type == null)
				{
					// The type could not be found using the fullTypeName
					throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
				}
				else
				{
					entity = Dionysos.InstanceFactory.CreateInstance(assembly, type) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
				}
			}

			return entity;
		}

		/// <summary>
		/// Gets an System.Object instance using the specified entityname and primary key value from the default data assembly
		/// </summary>
		/// <param name="entityName">The name of the entity to create</param>
		/// <param name="primaryKeyValue">The primary key value of the entity to get</param>
		/// <returns>An System.Object instance</returns>
		public object GetEntity(string entityName, object primaryKeyValue)
		{
			SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(primaryKeyValue, "primaryKeyValue"))
			{
				// Parameter 'primaryKeyValue' is empty
			}
			else
			{
				Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
				if (Instance.Empty(dataAssemblyInfo))
				{
					// Data assembly does not exist
					// or is not added to the Global.AssemblyInfo
					throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
				}
				else
				{
					Assembly assembly = dataAssemblyInfo.Assembly;
					if (Instance.Empty(assembly))
					{
						// Assembly is empty
						throw new Dionysos.EmptyException("Data assembly is empty");
					}
					else
					{
						entityName = LLBLGenUtil.GetEntityName(entityName);
						string fullTypeName = string.Format("{0}.EntityClasses.{1}Entity", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), entityName);

						System.Type type = assembly.GetType(fullTypeName);
						if (type == null)
						{
							// The type could not be found using the fullTypeName
							throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
						}
						else
						{
							entity = Dionysos.InstanceFactory.CreateInstance(assembly, type, primaryKeyValue) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
						}
					}
				}
			}

			return entity;
		}

		/// <summary>
		/// Creates a new System.Object instance using the specified entityname and primary key value from the specified data assembly
		/// </summary>
		/// <param name="entityName">The name of the entity to create</param>
		/// <param name="primaryKeyValue">The primary key value of the entity to get</param>
		/// <param name="assembly">The assembly which contains the entity type</param>
		/// <returns>An System.Object instance</returns>
		public object GetEntity(string entityName, object primaryKeyValue, Assembly assembly)
		{
			SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(primaryKeyValue, "primaryKeyValue"))
			{
				// Parameter 'assembly' is empty
			}
			else if (Instance.ArgumentIsEmpty(assembly, "assembly"))
			{
				// Parameter 'assembly' is empty
			}
			else
			{
				entityName = LLBLGenUtil.GetEntityName(entityName);
				string fullTypeName = string.Format("{0}.EntityClasses.{1}Entity", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), entityName);

				System.Type type = assembly.GetType(fullTypeName);
				if (type == null)
				{
					// The type could not be found using the fullTypeName
					throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
				}
				else
				{
					entity = Dionysos.InstanceFactory.CreateInstance(assembly, type, primaryKeyValue) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
				}
			}

			return entity;
		}

		#endregion
	}
}