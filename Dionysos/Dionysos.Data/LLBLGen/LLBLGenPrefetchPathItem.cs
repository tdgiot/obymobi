using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Class used for storing SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath instances with
    /// </summary>
    public class LLBLGenPrefetchPathItem
    {
        #region Fields

        private SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath prefetchPath = null;
        private string entityName = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the PrefetchPathItem class
        /// </summary>	
        public LLBLGenPrefetchPathItem(string entityName)
        {
            // Set the entity name
            this.entityName = entityName;
            
            // And initialize the prefetch path
            this.InitializePrefetchPath();
        }

        /// <summary>
        /// Constructs an instance of the PrefetchPathItem class
        /// </summary>	
        public LLBLGenPrefetchPathItem(string entityName, System.Reflection.Assembly assembly)
        {
            // Set the entity name
            this.entityName = entityName;

            // And initialize the prefetch path
            this.InitializePrefetchPath(assembly);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initialize the SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath instance 
        /// for this Lynx_media.Data.LLBLGen.v1.PrefetchPathItem
        /// </summary>
        private void InitializePrefetchPath()
        {
            Enum entityTypeEnumerator = LLBLGenUtil.GetEntityTypeEnumerator();
            if (Instance.Empty(entityTypeEnumerator))
            {
                throw new Dionysos.EmptyException("Variable 'entityTypeEnumerator' is empty");
            }
            else
            {
                Enum temp = (Enum)Enum.Parse(entityTypeEnumerator.GetType(), string.Format("{0}Entity", this.entityName));
                int entityTypeIndex = Convert.ToInt32(temp);

                // Initialize the prefetch path
                this.prefetchPath = new SD.LLBLGen.Pro.ORMSupportClasses.PrefetchPath(entityTypeIndex);

                // Initialize a corresponding entity
                LLBLGenEntityFactory entityFactory = new LLBLGenEntityFactory();
                SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = entityFactory.GetEntity(this.entityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
                if (entity == null)
                {
                    throw new Dionysos.EmptyException("Variable 'entity' is empty");
                }
                else
                {
                    string[] members = Dionysos.Reflection.Member.GetMemberNames(entity);

                    for (int i = 0; i < members.Length; i++)
                    {
                        string member = members[i];

                        if (member.StartsWith("Prefetch") && !member.Contains("Via") && !member.Contains("Collection"))
                        {
                            SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPathElement prefetchPathElement = Dionysos.Reflection.Member.InvokeProperty(entity, member) as SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPathElement;
                            if (Instance.Empty(prefetchPathElement))
                            {
                                throw new Dionysos.EmptyException("Variable 'prefetchPathElement' is empty");
                            }
                            else
                            {
                                this.prefetchPath.Add(prefetchPathElement);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Initialize the SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath instance 
        /// for this Lynx_media.Data.LLBLGen.v1.PrefetchPathItem
        /// </summary>
        /// <param name="assembly">The System.Reflection.Assembly instance to get the prefetchpath item from</param>
        private void InitializePrefetchPath(System.Reflection.Assembly assembly)
        {
            if (Instance.ArgumentIsEmpty(assembly, "assembly"))
            {
                // Parameter 'assembly' is empty
            }
            else
            {
                Enum entityTypeEnumerator = LLBLGenUtil.GetEntityTypeEnumerator(assembly);
                if (Instance.Empty(entityTypeEnumerator))
                {
                    throw new Dionysos.EmptyException("Variable 'entityTypeEnumerator' is empty");
                }
                else
                {
                    Enum temp = (Enum)Enum.Parse(entityTypeEnumerator.GetType(), string.Format("{0}Entity", this.entityName));
                    int entityTypeIndex = Convert.ToInt32(temp);

                    // Initialize the prefetch path
                    this.prefetchPath = new SD.LLBLGen.Pro.ORMSupportClasses.PrefetchPath(entityTypeIndex);

                    // Initialize a corresponding entity
                    LLBLGenEntityFactory entityFactory = new LLBLGenEntityFactory();
                    SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = entityFactory.GetEntity(this.entityName, assembly) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
                    if (entity == null)
                    {
                        throw new Dionysos.EmptyException("Variable 'entity' is empty");
                    }
                    else
                    {
                        string[] members = Dionysos.Reflection.Member.GetMemberNames(entity);

                        for (int i = 0; i < members.Length; i++)
                        {
                            string member = members[i];

                            if (member.StartsWith("Prefetch") && !member.Contains("Via") && !member.Contains("Collection"))
                            {
                                SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPathElement prefetchPathElement = Dionysos.Reflection.Member.InvokeProperty(entity, member) as SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPathElement;
                                if (Instance.Empty(prefetchPathElement))
                                {
                                    throw new Dionysos.EmptyException("Variable 'prefetchPathElement' is empty");
                                }
                                else
                                {
                                    this.prefetchPath.Add(prefetchPathElement);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath associated with this item
        /// </summary>
        public SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath PrefetchPath
        {
            get
            {
                return this.prefetchPath;
            }
            set
            {
                this.prefetchPath = value;
            }
        }

        /// <summary>
        /// Gets or sets the entity name of the Lynx_media.Data.LLBLGen.v1.PrefetchPathItem
        /// </summary>
        public string EntityName
        {
            get
            {
                return this.entityName;
            }
            set
            {
                this.entityName = value;
            }
        }

        #endregion
     }
}
