using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Dionysos;
using Dionysos.Reflection;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Interfaces.Data;
using Dionysos.Diagnostics;
using Dionysos.Collections;

namespace Dionysos.Data.LLBLGen
{
	/// <summary>
	/// Utility class for SD.LLBLGen.Pro.ORMSupportClasses.IEntity instances.
	/// </summary>
	public class LLBLGenEntityUtil : Dionysos.Interfaces.IEntityUtil
	{
		#region Methods

		/// <summary>
		/// Gets the field names for the specified entity.
		/// </summary>
		/// <param name="entityName">The name of the entity to retrieve the field names for.</param>
		/// <returns>
		/// A System.String array containing the names of the fields.
		/// </returns>
		/// <exception cref="EmptyException">
		/// Data assembly does not exist or is not added to the Global.AssemblyInfo.
		/// or
		/// Data assembly is empty.
		/// </exception>
		public string[] GetFields(string entityName)
		{
			string[] fields = null;

			if (!String.IsNullOrEmpty(entityName))
			{
				Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
				if (dataAssemblyInfo == null)
				{
					// Data assembly does not exist or is not added to the Global.AssemblyInfo
					throw new EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo.");
				}

				Assembly assembly = dataAssemblyInfo.Assembly;
				if (assembly == null)
				{
					// Assembly is empty
					throw new EmptyException("Data assembly is empty.");
				}

				fields = GetFields(entityName, assembly);
			}

			return fields;
		}

		/// <summary>
		/// Gets the field names for the specified entity from the specified assembly
		/// </summary>
		/// <param name="entityName">The name of the entity to retrieve the field names for</param>
		/// <param name="assembly">The assembly which contains the entity</param>
		/// <returns>A System.String array containing the names of the fields</returns>
		public string[] GetFields(string entityName, Assembly assembly)
		{
			string[] fields = null;

			if (!String.IsNullOrEmpty(entityName) &&
				assembly != null)
			{
				Enum fieldIndex = LLBLGenUtil.GetFieldIndexEnumerator(entityName, assembly);
				if (fieldIndex == null)
				{
					throw new EmptyException("Variable 'fieldIndex' is empty.");
				}

				// Retrieve the names of the fields from the fieldindex enumerator
				fields = Enum.GetNames(fieldIndex.GetType());

				if (fields.Length > 0)
				{
					// Remove the last item from the array because the last item
					// 'AmountOfFields' is always automatically generated
					string[] temp = new string[fields.Length - 1];

					for (int i = 0; i < (fields.Length - 1); i++)
					{
						temp[i] = fields[i];
					}

					fields = temp;
				}
			}

			return fields;
		}

		/// <summary>
		/// Deletes an Entity
		/// </summary>
		/// <param name="entityName">Entity Name</param>
		/// <param name="entityId">Entity Id (PK valee)</param>
		/// <returns>If it was succesfull</returns>
		public bool DeleteEntity(string entityName, int entityId)
		{
			var entity = DataFactory.EntityFactory.GetEntity(entityName, entityId) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;

			return DeleteEntity(entity, false);
		}

		/// <summary>
		/// Deletes an entity
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance that has to be deleted</param>
		/// <returns>True is the entity is deleted, False if not</returns>
		public bool DeleteEntity(IEntity entity)
		{
			return this.DeleteEntity(entity, false);
		}

		/// <summary>
		/// Deletes an entity
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance that has to be deleted, make sure it has a Transaction if requitred</param>
		/// <param name="forceCascadedDelete">Flag which indicates whether the related records should be deleted always</param>
		/// <returns>True is the entity is deleted, False if not</returns>
		public bool DeleteEntity(IEntity entity, bool forceCascadedDelete)
		{
			bool succes = false;

			if (entity != null)
			{
				if (!forceCascadedDelete)
				{
					string noActionMessage = CheckForRestrictedRelatedEntities(entity);
					if (!String.IsNullOrEmpty(noActionMessage))
					{
						throw new EntityDeleteException("Het item van soort '{0}' ({1})  kan niet verwijderd worden, omdat er nog andere items zijn die naar het te verwijderen item verwijzen:\r\n\r\n{2}",
							EntityInformationUtil.GetFriendlyName(entity.LLBLGenProEntityName), entity.LLBLGenProEntityName, noActionMessage);
					}
				}

				// Delete the related entities
				DeleteRelatedEntities(entity, forceCascadedDelete);

				// Finally, delete the entity using the delete entity method
				succes = entity.Delete();
			}

			return succes;
		}

		/// <summary>
		/// Checks whether the specified entity can be deleted according to referential constraints
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to delete the related entities for</param>
		public string CheckForRestrictedRelatedEntities(IEntity entity)
		{
			string noActionMessage = String.Empty;

			if (entity != null)
			{
				// Get a collection contains the relations of the entity (both 1:M and 1:1)
				IRelationCollection relationsOneToMany = LLBLGenRelationUtil.GetRelationsByRelationType(entity, RelationType.OneToMany);
				IRelationCollection relationsOneToOne = LLBLGenRelationUtil.GetRelationsByRelationType(entity, RelationType.OneToOne);

				// Combine 1:1 and 1:M relations.
				IRelationCollection relations = relationsOneToMany;
				foreach (IRelation r in relationsOneToOne) relations.Add(r);

				// Walk through the relations
				for (int j = 0; j < relations.Count; j++)
				{
					IEntityRelation relation = relations[j] as IEntityRelation;
					if (relation == null)
					{
						throw new EmptyException("Variable 'relation' is empty.");
					}

					if (relation.TypeOfRelation == RelationType.OneToOne && !relation.StartEntityIsPkSide)
					{
						// GK This is a 1:1 relation off which the current entity is related, therefore we shouldn't process this one.
						// I even think we could do without the TypeOfRelationCheck
					}
					else
					{
						// Get the field information from the relation
						IEntityFieldCore pkField = relation.GetPKEntityFieldCore(0) as IEntityFieldCore;
						IEntityFieldCore fkField = relation.GetFKEntityFieldCore(0) as IEntityFieldCore;
						IFieldPersistenceInfo pkPersistenceInfo = relation.GetPKFieldPersistenceInfo(0);
						IFieldPersistenceInfo fkPersistenceInfo = relation.GetFKFieldPersistenceInfo(0);

						// Get the referential constraint for the entity and foreign key combination from the database
						IEntity constraint = GetReferentialConstraintCustomEntity(fkPersistenceInfo.SourceObjectName, fkField.Name);
						if (constraint == null)
						{
							constraint = GetReferentialConstraintEntity(fkPersistenceInfo.SourceObjectName, fkField.Name);
						}

						ReferentialConstraintDeleteRule deleteRule = ReferentialConstraintDeleteRule.NoAction;
						if (constraint != null)
						{
							deleteRule = (ReferentialConstraintDeleteRule)constraint.Fields["DeleteRule"].CurrentValue;

							// Create and initialize a filter to get the related entities based on the current relation
							PredicateExpression filter = new PredicateExpression();
							filter.Add(new FieldCompareValuePredicate(fkField, fkPersistenceInfo, ComparisonOperator.Equal, entity.Fields[pkField.Name].CurrentValue));
							filter.Add(new FieldCompareValuePredicate(fkField, fkPersistenceInfo, ComparisonOperator.NotEqual, DBNull.Value)); // Foreign key must never be null

							// Create and initialize an IEntityCollection to retrieve
							// the related entities from the database
							IEntityCollection entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(fkPersistenceInfo.SourceObjectName) as IEntityCollection;
							entityCollection.AddToTransaction(entity);
							entityCollection.GetMulti(filter);

							// Walk through the related entities
							for (int k = 0; k < entityCollection.Count; k++)
							{
								IEntity relatedEntity = entityCollection[k] as IEntity;
								if (relatedEntity == null)
								{
									throw new EmptyException("Variable 'relatedEntity' is empty.");
								}

								// Check whether the entity can be deleted according to the referential constraint
								if (deleteRule == ReferentialConstraintDeleteRule.NoAction)
								{
									// No action, so the entity can't be deleted
									string entityTypeName = TypeUtil.GetTypeString(relatedEntity);
									string relatedFieldValue = relatedEntity.Fields[0].CurrentValue.ToString();

									// Get EntityName
									string entityName = entityTypeName.Substring(entityTypeName.LastIndexOf('.') + 1);
									IEntityInformation entityInfo = EntityInformationUtil.GetEntityInformationEntity(entityName) as IEntityInformation;
									string displayEntityValue = relatedFieldValue;
									if (entityInfo != null)
									{
										entityName = entityInfo.FriendlyName;

										// Get friendly display value
										if (!String.IsNullOrEmpty(entityInfo.ShowFieldName) &&
											relatedEntity.Fields[entityInfo.ShowFieldName] != null &&
											relatedEntity.Fields[entityInfo.ShowFieldName].CurrentValue != null)
										{
											displayEntityValue = relatedEntity.Fields[entityInfo.ShowFieldName].CurrentValue.ToString().Trim();
										}
									}

									if (String.IsNullOrEmpty(entityName))
									{
										entityName = entityTypeName.Substring(entityTypeName.LastIndexOf('.') + 1);
									}

									noActionMessage += String.Format(" - {1} - '{0}' verwijst nog naar het te verwijderen item. (Pk: {2}).\r\n",
														displayEntityValue, entityTypeName, relatedFieldValue);
								}
							}
						}
					}
				}
			}

			return noActionMessage;
		}

		/// <summary>
		/// Checks whether the specified entity can be deleted according to referential constraints
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to delete the related entities for</param>
		public void DeleteRelatedEntities(IEntity entity)
		{
			this.DeleteRelatedEntities(entity, false);
		}

		/// <summary>
		/// Checks whether the specified entity can be deleted according to referential constraints
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to delete the related entities for</param>
		/// <param name="forceCascadedDelete">Flag which indicates whether the related records should be deleted always</param>
		public void DeleteRelatedEntities(IEntity entity, bool forceCascadedDelete)
		{
			if (entity != null)
			{
				// Get a collection contains the relations of the entity
				IRelationCollection relationsOneToMany = LLBLGenRelationUtil.GetRelationsByRelationType(entity, RelationType.OneToMany);
				IRelationCollection relationsOneToOne = LLBLGenRelationUtil.GetRelationsByRelationType(entity, RelationType.OneToOne);

				// Combine 1:1 and 1:M relations.
				IRelationCollection relations = relationsOneToMany;
				foreach (IRelation r in relationsOneToOne) relations.Add(r);

				// Walk through the relations
				for (int j = 0; j < relations.Count; j++)
				{
					IEntityRelation relation = relations[j] as IEntityRelation;
					if (relation == null)
					{
						throw new EmptyException("Variable 'relation' is empty.");
					}

					if (relation.TypeOfRelation == RelationType.OneToOne && !relation.StartEntityIsPkSide)
					{
						// GK This is a 1:1 relation off which the current entity is related, therefore we shouldn't process this one.
						// I even think we could do without the TypeOfRelationCheck
					}
					else
					{
						// Get the field information from the relation
						IEntityFieldCore pkField = relation.GetPKEntityFieldCore(0) as IEntityFieldCore;
						IEntityFieldCore fkField = relation.GetFKEntityFieldCore(0) as IEntityFieldCore;
						IFieldPersistenceInfo pkPersistenceInfo = relation.GetPKFieldPersistenceInfo(0);
						IFieldPersistenceInfo fkPersistenceInfo = relation.GetFKFieldPersistenceInfo(0);

						// Get the referential constraint for the entity and foreign key combination from the database
						ReferentialConstraintDeleteRule deleteRule;
						if (forceCascadedDelete)
						{
							// Check whether the related records should be deleted always
							deleteRule = ReferentialConstraintDeleteRule.Cascade;
						}
						else
						{
							IEntity constraint = GetReferentialConstraintCustomEntity(fkPersistenceInfo.SourceObjectName, fkField.Name);
							if (constraint == null)
							{
								constraint = GetReferentialConstraintEntity(fkPersistenceInfo.SourceObjectName, fkField.Name);
                            }

							// Default behaviour:  NoAction.
							if (constraint == null)
								deleteRule = ReferentialConstraintDeleteRule.NoAction;
							else
								deleteRule = (ReferentialConstraintDeleteRule)constraint.Fields["DeleteRule"].CurrentValue;
						}

						// Create and initialize a filter to get the related entities based on the current relation
						PredicateExpression filter = new PredicateExpression();
						filter.Add(new FieldCompareValuePredicate(fkField, fkPersistenceInfo, ComparisonOperator.Equal, entity.Fields[pkField.Name].CurrentValue));
						filter.Add(new FieldCompareValuePredicate(fkField, fkPersistenceInfo, ComparisonOperator.NotEqual, DBNull.Value)); // Foreign key must never be null

						// Create and initialize an IEntityCollection to retrieve the related entities from the database
						IEntityCollection entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(fkPersistenceInfo.SourceObjectName) as IEntityCollection;
						entityCollection.AddToTransaction(entity);
						entityCollection.GetMulti(filter);

						// Walk through the related entities
						for (int k = 0; k < entityCollection.Count; k++)
						{
							IEntity relatedEntity = entityCollection[k] as IEntity;
							relatedEntity.AddToTransaction(entity);
							if (relatedEntity == null)
							{
								throw new EmptyException("Variable 'relatedEntity' is empty while deleting {0} with PK {1}.", entity.LLBLGenProEntityName, entity.PrimaryKeyFields[0].CurrentValue);
							}

							switch (deleteRule)
							{
								case ReferentialConstraintDeleteRule.NoAction:
									// No action, so the entity can't be deleted
                                    throw new TechnicalException("'No Action'-contraint was set for the relation with {0}, constraint is required to delete {1}.", relatedEntity.LLBLGenProEntityName, entity.LLBLGenProEntityName);
								case ReferentialConstraintDeleteRule.Cascade:
									// Cascade, so the entity and the depending entity may be deleted
									DeleteEntity(relatedEntity, forceCascadedDelete);
									break;
								case ReferentialConstraintDeleteRule.SetNull:
									// Set null, the foreign key field of the depending entity has to be set to string.empty
									if (relatedEntity.Fields[fkField.Name] != null)
									{
										object formerFkId = relatedEntity.Fields[fkField.Name].CurrentValue;

										if (!relatedEntity.SetNewFieldValue(fkField.Name, null)) throw new TechnicalException("Column '{0}' cannot be set to null on Entity of Type '{1}' because it does not allow null values.", fkField.Name, relatedEntity.LLBLGenProEntityName);

										// Set former ID if present
										if (relatedEntity.Fields["Former" + fkField.Name] != null)
										{
											relatedEntity.SetNewFieldValue("Former" + fkField.Name, formerFkId);
										}

										relatedEntity.Save();
									}
									break;
								case ReferentialConstraintDeleteRule.SetDefault:
									// Set default, I don't have a clue
									break;
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Archive all related entities according to the ArchiveRules specified in the Referential Constraints
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to archive the related entities for</param>
		/// <param name="newArchivedValue">Sets which value should be set to the Archived field</param>
		public void ArchiveRelatedEntities(IEntity entity, bool newArchivedValue)
		{
			ArchiveRelatedEntities(entity, newArchivedValue, null);
		}

		/// <summary>
		/// Archive all related entities according to the ArchiveRules specified in the Referential Constraints
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to archive the related entities for</param>
		/// <param name="newArchivedValue">Sets which value should be set to the Archived field</param>
		/// <param name="processedRelations">List to keep track of processed relations in the recursion to prevent loops</param>
		public void ArchiveRelatedEntities(IEntity entity, bool newArchivedValue, List<string> processedRelations)
		{
			if (processedRelations == null)
				processedRelations = new List<string>();

			if (Instance.ArgumentIsEmpty(entity, "entity"))
			{
				// Parameter 'entity' is empty
			}
			else
			{
				// Get a collection contains the relations of the entity
				// Get a collection contains the relations of the entity (both 1:M and 1:1)
				IRelationCollection relationsOneToMany = LLBLGenRelationUtil.GetRelationsByRelationType(entity, RelationType.OneToMany);
				IRelationCollection relationsOneToOne = LLBLGenRelationUtil.GetRelationsByRelationType(entity, RelationType.OneToOne);

				// Combine 1:1 and 1:M relations.
				IRelationCollection relations = relationsOneToMany;
				foreach (IRelation r in relationsOneToOne)
					relations.Add(r);

				// Walk through the relations
				for (int j = 0; j < relations.Count; j++)
				{
					IEntityRelation relation = relations[j] as IEntityRelation;
					if (Instance.Empty(relation))
					{
						throw new EmptyException("Variable 'relation' is empty.");
					}
					else if (relation.TypeOfRelation == RelationType.OneToOne && !relation.StartEntityIsPkSide)
					{
						// GK This is a 1:1 relation off which the current entity is related, therefore we shouldn't process this one.
						// I even think we could do without the TypeOfRelationCheck
					}
					else
					{
						// Get the field information from the relation
						IEntityFieldCore pkField = relation.GetPKEntityFieldCore(0) as IEntityFieldCore;
						IEntityFieldCore fkField = relation.GetFKEntityFieldCore(0) as IEntityFieldCore;
						IFieldPersistenceInfo pkPersistenceInfo = relation.GetPKFieldPersistenceInfo(0);
						IFieldPersistenceInfo fkPersistenceInfo = relation.GetFKFieldPersistenceInfo(0);

						// Get the referential constraint for the entity
						// and foreign key combination from the database
						IEntity constraint = GetReferentialConstraintCustomEntity(fkPersistenceInfo.SourceObjectName, fkField.Name);
						if (Instance.Empty(constraint))
						{
							constraint = GetReferentialConstraintEntity(fkPersistenceInfo.SourceObjectName, fkField.Name);
						}

						// Default behaviour:  NoAction.
						ReferentialConstraintArchiveRule archiveRule;
						if (Instance.Empty(constraint) || constraint.Fields["ArchiveRule"] == null || constraint.Fields["ArchiveRule"].CurrentValue == null)
							archiveRule = ReferentialConstraintArchiveRule.NoAction;
						else
							archiveRule = (ReferentialConstraintArchiveRule)constraint.Fields["ArchiveRule"].CurrentValue;

						// Create and initialize a filter to get the related entities
						// based on the current relation
						PredicateExpression filter = new PredicateExpression();
						filter.Add(new FieldCompareValuePredicate(fkField, fkPersistenceInfo, ComparisonOperator.Equal, entity.Fields[pkField.Name].CurrentValue));
						filter.Add(new FieldCompareValuePredicate(fkField, fkPersistenceInfo, ComparisonOperator.NotEqual, DBNull.Value)); // Foreign key must never be null

						// Create and initialize an IEntityCollection to retrieve
						// the related entities from the database
						IEntityCollection entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(fkPersistenceInfo.SourceObjectName) as IEntityCollection;
						entityCollection.AddToTransaction(entity);
						entityCollection.GetMulti(filter);


						// Walk through the related entities
						for (int k = 0; k < entityCollection.Count; k++)
						{
							IEntity relatedEntity = entityCollection[k] as IEntity;
							relatedEntity.AddToTransaction(entity);
							if (Instance.Empty(relatedEntity))
							{
								throw new EmptyException("Variable 'relatedEntity' is empty while archiving {0} with PK {1}.", entity.LLBLGenProEntityName, entity.PrimaryKeyFields[0].CurrentValue);
							}
							else
							{
								switch (archiveRule)
								{
									case ReferentialConstraintArchiveRule.NoAction:
										// No action, just leave the entity
										break;
									case ReferentialConstraintArchiveRule.Cascade:
										// Cascade, so the entity and the depending entity may be archived
										// If this rleation was already processed in this recursion, skip to prevent loops
										Debug.WriteLine(relation.ToString());
										if (processedRelations.Contains(relation.ToString()))
											continue;
										else
											processedRelations.Add(relation.ToString());

										relatedEntity.SetNewFieldValue("Archived", newArchivedValue);
										relatedEntity.Save();
										ArchiveRelatedEntities(relatedEntity, newArchivedValue, processedRelations);

										processedRelations.Remove(relation.ToString());

										// Remove from thread
										break;
									default:
										throw new Dionysos.TechnicalException("No implementation for archivation using ReferentialConstraintArchiveRule '{0}' on Relation '{1}' for EntityType '{2}'", archiveRule.ToString(), relation.ToString(), relatedEntity.LLBLGenProEntityName);
								}
							}
						}

					}
				}
			}
		}

		/// <summary>
		/// Retrieves all ForeignKeyFieldnames from the DB
		/// </summary>
		/// <param name="entity">Entity to search the FK fields on</param>
		/// <returns>List of fieldnames of FK fields</returns>
		public static List<string> GetForeignKeyFieldNames(IEntity entity)
		{
			List<string> toReturn = new List<string>();

			for (int i = 0; i < entity.Fields.Count; i++)
			{
				IEntityField current = entity.Fields[i];
				if (current.IsForeignKey)
				{
					if (current.Name.Contains("_"))
					{
						// GK: According to MB:
						// Field names which contain an "_" are
						// inherited fieldnames using entity inheritance
					}
					else if (current.IsForeignKey)
					{
						// Valid foreignkeyfield
						toReturn.Add(current.Name);
					}
				}
			}

			return toReturn;
		}

		/// <summary>
		/// Gets a entity containing the custom referential constraint for the specified entity name and foreign key name
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the foreign key</param>
		/// <param name="foreignKeyName">The name of the foreign key to get the referential constraint for</param>
		private IEntity GetReferentialConstraintCustomEntity(string entityName, string foreignKeyName)
		{
			IEntity constraint = null;

			IEntity referentialConstraintCustomEntity = DataFactory.EntityFactory.GetEntity("ReferentialConstraintCustom") as IEntity;
			if (Instance.Empty(referentialConstraintCustomEntity))
			{
				throw new EmptyException("Variable 'referentialConstraintCustomEntity' is empty.");
			}
			else
			{
				// Create and initialize a filter to get the referential constraint rule from the database
				PredicateExpression referentialConstraintCustomFilter = new PredicateExpression();
				referentialConstraintCustomFilter.Add(new FieldCompareValuePredicate(referentialConstraintCustomEntity.Fields["EntityName"], ComparisonOperator.Equal, entityName));
				referentialConstraintCustomFilter.Add(new FieldCompareValuePredicate(referentialConstraintCustomEntity.Fields["ForeignKeyName"], ComparisonOperator.Equal, foreignKeyName));

				IEntityCollection referentialConstraintsCustom = DataFactory.EntityCollectionFactory.GetEntityCollection("ReferentialConstraintCustom") as IEntityCollection;
				if (Instance.Empty(referentialConstraintsCustom))
				{
					throw new EmptyException("Variable 'referentialConstraintsCustom' is empty.");
				}
				else
				{
					// Get the custom referential constraint entities
					referentialConstraintsCustom.GetMulti(referentialConstraintCustomFilter);

					if (referentialConstraintsCustom.Count == 0)
					{
						// No custom referential constraint found
					}
					else if (referentialConstraintsCustom.Count > 1)
					{
						throw new TechnicalException(string.Format("Multiple custom referential constraints found for entity '{0}' foreign key '{1}'", entityName, foreignKeyName));
					}
					else
					{
						constraint = referentialConstraintsCustom[0];
					}
				}
			}

			return constraint;
		}

		/// <summary>
		/// Gets a entity containing the referential constraint for the specified entity name and foreign key name
		/// </summary>
		/// <param name="entityName">The name of the entity which contains the foreign key</param>
		/// <param name="foreignKeyName">The name of the foreign key to get the referential constraint for</param>
		private IEntity GetReferentialConstraintEntity(string entityName, string foreignKeyName)
		{
			IEntity constraint = null;

			IEntity referentialConstraintEntity = DataFactory.EntityFactory.GetEntity("ReferentialConstraint") as IEntity;
			if (Instance.Empty(referentialConstraintEntity))
			{
				throw new EmptyException("Variable 'referentialConstraintEntity' is empty.");
			}
			else
			{
				// Create and initialize a filter to get the referential constraint rule from the database
				PredicateExpression referentialConstraintFilter = new PredicateExpression();
				referentialConstraintFilter.Add(new FieldCompareValuePredicate(referentialConstraintEntity.Fields["EntityName"], ComparisonOperator.Equal, entityName));
				referentialConstraintFilter.Add(new FieldCompareValuePredicate(referentialConstraintEntity.Fields["ForeignKeyName"], ComparisonOperator.Equal, foreignKeyName));

				IEntityCollection referentialConstraints = DataFactory.EntityCollectionFactory.GetEntityCollection("ReferentialConstraint") as IEntityCollection;
				if (Instance.Empty(referentialConstraints))
				{
					throw new EmptyException("Variable 'referentialConstraints' is empty.");
				}
				else
				{
					// Get the referential constraint entities
					referentialConstraints.GetMulti(referentialConstraintFilter);

					if (referentialConstraints.Count == 0)
					{
						// No referential constraint found
					}
					else if (referentialConstraints.Count > 1)
					{
						throw new TechnicalException(string.Format("Multiple referential constraints found for entity '{0}' foreign key '{1}'", entityName, foreignKeyName));
					}
					else
					{
						constraint = referentialConstraints[0];
					}
				}
			}

			return constraint;
		}

		/// <summary>
		/// Retrieves a list containing the entities related to this entity
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to get the related entities from</param>
		/// <returns>A List instance containing the related entities</returns>
		public static List<IEntity> GetRelatedEntities(IEntity entity)
		{
			// Create and initialize the related entities list
			List<IEntity> relatedEntitiesList = new List<IEntity>();

			if (Instance.ArgumentIsEmpty(entity, "entity"))
			{
				// Parameter 'entity' is empty
			}
			else
			{
				IRelationCollection relations = LLBLGenRelationUtil.GetRelationsByRelationType(entity, RelationType.OneToMany);
				for (int j = 0; j < relations.Count; j++)
				{
					IEntityRelation relation = relations[j] as IEntityRelation;
					if (Instance.Empty(relation))
					{
						throw new EmptyException("Variable 'relation' is empty.");
					}
					else
					{
						IEntityFieldCore pkField = relation.GetPKEntityFieldCore(0) as IEntityFieldCore;
						IEntityFieldCore fkField = relation.GetFKEntityFieldCore(0) as IEntityFieldCore;
						IFieldPersistenceInfo pkPersistenceInfo = relation.GetPKFieldPersistenceInfo(0);
						IFieldPersistenceInfo fkPersistenceInfo = relation.GetFKFieldPersistenceInfo(0);

						PredicateExpression filter = new PredicateExpression();
						filter.Add(new FieldCompareValuePredicate(fkField, fkPersistenceInfo, ComparisonOperator.Equal, entity.Fields[pkField.Name].CurrentValue));
						filter.Add(new FieldCompareValuePredicate(fkField, fkPersistenceInfo, ComparisonOperator.NotEqual, DBNull.Value)); // Foreign key must never be null

						IEntityCollection entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(fkPersistenceInfo.SourceObjectName) as IEntityCollection;
						entityCollection.GetMulti(filter);

						for (int k = 0; k < entityCollection.Count; k++)
						{
							IEntity relatedEntity = entityCollection[k] as IEntity;
							relatedEntitiesList.Add(relatedEntity);
						}
					}
				}
			}

			return relatedEntitiesList;
		}

		/// <summary>
		/// Copy Fields from one Entity to another (Primary key fields are never copied).
		/// </summary>
		/// <param name="sourceEntity">Entity to take the values from</param>
		/// <param name="targetEntity">Entity to copy the values to</param>
		/// <param name="fieldsNotToBeCopied">Fields that should not be copied. (Primary key fields are never copied)</param>
		/// <returns></returns>
		public static IEntity CopyFields(IEntity sourceEntity, IEntity targetEntity, ArrayList fieldsNotToBeCopied)
		{
			return CopyFields(sourceEntity, targetEntity, fieldsNotToBeCopied, true);
		}

		/// <summary>
		/// Copy Fields from one Entity to another (Primary key fields are never copied).
		/// </summary>
		/// <param name="sourceEntity">Entity to take the values from</param>
		/// <param name="targetEntity">Entity to copy the values to</param>
		/// <param name="fieldsNotToBeCopied">Fields that should not be copied. (Primary key fields are never copied)</param>
		/// <param name="copyNullValues">bool indicating if field with null values should be copied</param>
		/// <returns></returns>
		public static IEntity CopyFields(IEntity sourceEntity, IEntity targetEntity, ArrayList fieldsNotToBeCopied, bool copyNullValues)
		{
			if (Instance.Empty(sourceEntity))
			{
				throw new Exception("Empty SourceEntity in CopyFields of LLBLGenEntityUtil");
			}
			else if (Instance.Empty(targetEntity))
			{
				throw new Exception("Empty TargetEntity in CopyFields of LLBLGenEntityUtil");
			}

			if (fieldsNotToBeCopied == null)
				fieldsNotToBeCopied = new ArrayList();

			// Standard not to be copied:
			fieldsNotToBeCopied.Add("Created");
            fieldsNotToBeCopied.Add("CreatedUTC");
			fieldsNotToBeCopied.Add("CreatedBy");
			fieldsNotToBeCopied.Add("Updated");
            fieldsNotToBeCopied.Add("UpdatedUTC");
			fieldsNotToBeCopied.Add("UpdatedBy");
			fieldsNotToBeCopied.Add("Deleted");
			fieldsNotToBeCopied.Add("Archived");

			// GK This might seem a little bit 'lazy' or 'dirty' it happened a few times
			// that IEntityFields were added instead of the Names of the fields, let's just resolve those:
			for (int i = 0; i < fieldsNotToBeCopied.Count; i++)
			{
				if (fieldsNotToBeCopied[i] is IEntityFieldCore)
					fieldsNotToBeCopied[i] = ((IEntityFieldCore)fieldsNotToBeCopied[i]).Name;
				else if (fieldsNotToBeCopied[i] is IEntityField)
					fieldsNotToBeCopied[i] = ((IEntityField)fieldsNotToBeCopied[i]).Name;
				else if (fieldsNotToBeCopied[i] is IEntityField2)
					fieldsNotToBeCopied[i] = ((IEntityField2)fieldsNotToBeCopied[i]).Name;
			}

			for (int i = 0; i < sourceEntity.Fields.Count; i++)
			{
				IEntityField sourceField = sourceEntity.Fields[i];
				// Check NOT PrimaryKey AND Field Exists on target entity AND it's not READONLY on the target
				if (!sourceField.IsPrimaryKey && targetEntity.Fields[sourceField.Name] != null && !targetEntity.Fields[sourceField.Name].IsReadOnly)
				{
					// chck if the value is != null if copyNullValues is false
					if (copyNullValues || (!copyNullValues && sourceField.CurrentValue != null))
					{
						// Check if it should be skipped
						if (fieldsNotToBeCopied == null || fieldsNotToBeCopied.IndexOf(sourceField.Name) < 0)
						{
							targetEntity.SetNewFieldValue(sourceField.Name, sourceField.CurrentValue);
						}
					}
				}
			}

			return targetEntity;
		}

		/// <summary>
		/// Copies the fields from the source to a new result entity.
		/// </summary>
		/// <typeparam name="T">The type of the result entity.</typeparam>
		/// <param name="source">The source entity.</param>
		/// <returns>The result entity.</returns>
		public static T CopyFields<T>(IEntity source)
			where T : IEntity, new()
		{
			return CopyFields<T>(source, new T(), null, false);
		}

		/// <summary>
		/// Copies the fields from the source to a new result entity, using the specified field mappings.
		/// </summary>
		/// <typeparam name="T">The type of the result entity.</typeparam>
		/// <param name="source">The source entity.</param>
		/// <param name="fieldMappings">The mappings between the source and result entity fields.</param>
		/// <returns>The result entity.</returns>
		public static T CopyFields<T>(IEntity source, IDictionary<string, string> fieldMappings)
			where T : IEntity, new()
		{
			return CopyFields<T>(source, new T(), fieldMappings, false);
		}

		/// <summary>
		/// Copies the fields from the source to the result entity.
		/// </summary>
		/// <typeparam name="T">The type of the result entity.</typeparam>
		/// <param name="source">The source entity.</param>
		/// <param name="result">The result entity.</param>
		/// <returns>The result entity.</returns>
		public static T CopyFields<T>(IEntity source, T result)
			where T : IEntity
		{
			return CopyFields<T>(source, result, null, false);
		}

		/// <summary>
		/// Copies the fields from the source to the result entity, using the specified field mappings.
		/// </summary>
		/// <typeparam name="T">The type of the result entity.</typeparam>
		/// <param name="source">The source entity.</param>
		/// <param name="result">The result entity.</param>
		/// <param name="fieldMappings">The mappings between the source and result entity fields.</param>
		/// <returns>The result entity.</returns>
		public static T CopyFields<T>(IEntity source, T result, IDictionary<string, string> fieldMappings)
			where T : IEntity
		{
			return CopyFields<T>(source, result, fieldMappings, false);
		}

		/// <summary>
		/// Copies the fields from the source to the result entity, using the specified field mappings.
		/// </summary>
		/// <typeparam name="T">The type of the result entity.</typeparam>
		/// <param name="source">The source entity.</param>
		/// <param name="result">The result entity.</param>
		/// <param name="fieldMappings">The mappings between the source and result entity fields.</param>
		/// <param name="onlyCopyMappedFields">If set to <c>true</c>, only copies the mapped fields.</param>
		/// <returns>The result entity.</returns>
		public static T CopyFields<T>(IEntity source, T result, IDictionary<string, string> fieldMappings, bool onlyCopyMappedFields)
			where T : IEntity
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}

			if (result == null)
			{
				throw new ArgumentNullException("result");
			}

			if (fieldMappings == null)
			{
				fieldMappings = new Dictionary<string, string>();
			}

			// Add default field mappings (if not already mapped)
			string[] ignoreFields = new string[] { "Archived", "Created", "CreatedBy", "Updated", "UpdatedBy", "Deleted" };
			foreach (string ignoreField in ignoreFields)
			{
				if (!fieldMappings.ContainsKey(ignoreField))
				{
					fieldMappings.Add(ignoreField, null);
				}
			}

			// Copy fields
			foreach (IEntityField sourceField in source.Fields)
			{
				// Get result field
				IEntityField resultField = null;
				string resultFieldName;
				if (fieldMappings.TryGetValue(sourceField.Name, out resultFieldName))
				{
					if (resultFieldName != null)
					{
						resultField = result.Fields[resultFieldName];
					}
				}
				else if (!onlyCopyMappedFields)
				{
					resultField = result.Fields[sourceField.Name];
				}

				// Copy field
				if (resultField != null &&
					!resultField.IsReadOnly &&
					!resultField.IsPrimaryKey)
				{
					result.SetNewFieldValue(resultField.Name, sourceField.CurrentValue);
				}
			}

			return result;
		}

		/// <summary>
		/// Parse all Fields and Values to 1 string for the supplied entity
		/// </summary>
		/// <param name="entity">Entity to retrieve the values from</param>
		/// <returns>Return the string</returns>
		public static string GetFieldsAndValues(IEntity entity)
		{
			StringBuilder sbOutput = new StringBuilder();

			List<string[]> changedFields = new List<string[]>();
			for (int i = 0; i < entity.Fields.Count; i++)
			{
				IEntityField field = entity.Fields[i];
				sbOutput.Append(field.Name.PadRight(35, ' '));
				sbOutput.Append(" - ");

				string current = string.Empty;
				if (field.CurrentValue != null)
					current = field.CurrentValue.ToString();
				else
					current = string.Empty;
				current = current.PadRight(40, ' ');
				sbOutput.Append(current);

				string newvalue = string.Empty;

				if (field.CurrentValue != field.DbValue)
				{
					if (field.DbValue != null)
						newvalue = field.DbValue.ToString();
					newvalue = newvalue.PadRight(40, ' ');

					sbOutput.Append(" - was: ");
					sbOutput.Append(newvalue);
					changedFields.Add(new string[] { field.Name, current, newvalue });
				}
				sbOutput.Append("\r\n");
			}

			// Add Changed fields
			sbOutput.Append("\r\n\r\n[Changed Fields]\r\n");
			sbOutput.AppendFormat("{0} - {1} - {2}\r\n", "Fieldname".PadRight(35, ' '), "[New value]".PadRight(40, ' '), "[Old value]".PadRight(40, ' '));

			foreach (var field in changedFields)
			{
				sbOutput.AppendFormat("{0} - {1} - {2}\r\n", field[0].PadRight(35, ' '), field[1], field[2]);
			}

			return sbOutput.ToString();
		}

		/// <summary>
		/// Get a CSV string of all changed values in the form:
		/// FieldName;DbValue;CurrentValue
		/// </summary>
		/// <param name="entity">Entity</param>
		/// <returns>Csv</returns>
		public static string GetChangedFieldsAndValuesAsCsv(IEntity entity)
		{
			List<string> fieldsToExclude = new List<string> { "Created", "CreatedBy", "Updated", "UpdatedBy" };
			StringBuilder sbOutput = new StringBuilder();

			List<string[]> changedFields = new List<string[]>();
			for (int i = 0; i < entity.Fields.Count; i++)
			{
				IEntityField field = entity.Fields[i];
				if (field.CurrentValue != field.DbValue && !fieldsToExclude.Contains(field.Name))
					sbOutput.AppendLine(string.Format("{0};{1};{2}", field.Name, field.DbValue, field.CurrentValue));
			}

			return sbOutput.ToString();
		}

		/// <summary>
		/// Get the PrimaryKey column of a Entity
		/// </summary>
		/// <param name="entityName">Entity to work with</param>
		/// <returns>Returns the PrimaryKeyField</returns>
		/// <exception cref="Dionysos.TechnicalException">Throws a exception if it's unable to determine the PK column</exception>
		public static SD.LLBLGen.Pro.ORMSupportClasses.IEntityField GetPkFieldInfo(string entityName)
		{
			return GetPkFieldInfo((IEntity)Dionysos.DataFactory.EntityFactory.GetEntity(entityName));
		}

		/// <summary>
		/// Get the PrimaryKey column of a Entity
		/// </summary>
		/// <param name="entity">Entity to work with</param>
		/// <returns>Returns the PrimaryKeyField</returns>
		/// <exception cref="Dionysos.TechnicalException">Throws a exception if it's unable to determine the PK column</exception>
		public static SD.LLBLGen.Pro.ORMSupportClasses.IEntityField GetPkFieldInfo(IEntity entity)
		{
			SD.LLBLGen.Pro.ORMSupportClasses.IEntityField toReturn = null;

			if (entity.PrimaryKeyFields.Count == 1)
			{
				toReturn = entity.PrimaryKeyFields[0];
			}
			else if (entity.PrimaryKeyFields.Count > 0)
			{
				int identityFieldCount = 0;
				for (int i = 0; i < entity.PrimaryKeyFields.Count; i++)
				{
					if (entity.PrimaryKeyFields[i].IsIdentity)
					{
						identityFieldCount++;
						toReturn = entity.PrimaryKeyFields[i];
					}
				}

				if (identityFieldCount == 0)
				{
					throw new Dionysos.TechnicalException(string.Format("The entity has Primary Key(s) but none of the type IsIdentity (Type: {0})", entity.LLBLGenProEntityName));
				}
				else if (identityFieldCount > 1)
				{
					throw new Dionysos.TechnicalException(string.Format("The entity has multiple Primary Key(s) of which multiple are IsIdentity, could not determine correct PK to use (Type: {0})", entity.LLBLGenProEntityName));
				}
			}
			else
			{
				throw new Dionysos.TechnicalException(string.Format("0 Primary Keys found on Entity (Type: {0})", entity.LLBLGenProEntityName));
			}

			return toReturn;
		}

		/// <summary>
		/// Retrieve a IEntityCollection from a Property (For example, OrderItems from an Order)
		/// </summary>
		/// <param name="entity">The entity which has to property to retrieve the collection form (i.e. Order)</param>
		/// <param name="collectionPropertyName">The property name of the collection to retrieve (i.e. OrderItemCollection, or Contacts, this can be different from the class name)</param>
		/// <example>For an order this could be: Order.OrderItemCollection, so the collectionPropertyName = "OrderItemCollection"
		/// For it could also be different from the collection classname. If for example Customers could have products they own and products the have on a favorites list it could be
		/// Customer.WishList (containing a ProductCollection) and Customer.CurrentlyOwned (containing a ProductCollection).
		/// </example>
		/// <returns></returns>
		public static IEntityCollection GetCollectionFromProperty(IEntity entity, string collectionPropertyName)
		{
			return Member.InvokeProperty(entity, collectionPropertyName) as IEntityCollection;
		}

		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// Doesn't throw any errors on multiple or none found.
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <param name="entity">Entity to be filled with the found result, should be initialized, returned as NULL if the fetched collection.count != 1</param>
		/// <returns>Returns true if a succesfull fetch was done (entity != null)</returns>
		public static IEntity GetSingleEntityUsingPredicateExpression(IPredicate filter, IEntity entity)
		{
			return GetSingleEntityUsingPredicateExpression(filter, null, null, entity, false, false);
		}

		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// Doesn't throw any errors on multiple or none found.
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <returns>Returns true if a succesfull fetch was done (entity != null)</returns>
		public static EntityType GetSingleEntityUsingPredicateExpression<EntityType>(IPredicate filter)
			where EntityType : IEntity
		{
			IEntity entity = (IEntity)typeof(EntityType).GetConstructor(new System.Type[] { }).Invoke(new object[] { });
			return (EntityType)GetSingleEntityUsingPredicateExpression(filter, null, null, entity, false, false);
		}

		/// <summary>
		/// Retrieve a single IEntity using a PredicateExpression and RelationCollection. Returns <value>null</value> if multiple or none found.
		/// </summary>
		/// <typeparam name="EntityType">The IEntity type to return.</typeparam>
		/// <param name="filter">The PredicateExpression to use for filtering.</param>
		/// <param name="relations">The RelationCollection to use to allow filtering on related entities.</param>
		/// <returns>Returns the IEntity if the result is a single item; otherwise <value>null</value>.</returns>
		public static EntityType GetSingleEntityUsingPredicateExpression<EntityType>(IPredicate filter, RelationCollection relations)
			where EntityType : IEntity
		{
			IEntity entity = (IEntity)typeof(EntityType).GetConstructor(new System.Type[] { }).Invoke(new object[] { });
			return (EntityType)GetSingleEntityUsingPredicateExpression(filter, relations, null, (IEntity)entity, false, false, null);
		}

		/// <summary>
		/// Retrieve a single IEntity using a PredicateExpression and RelationCollection. Returns <value>null</value> if multiple or none found.
		/// </summary>
		/// <typeparam name="EntityType">The IEntity type to return.</typeparam>
		/// <param name="filter">The PredicateExpression to use for filtering.</param>
		/// <param name="relations">The RelationCollection to use to allow filtering on related entities.</param>
		/// <param name="prefetchPath">Prefetch path for the fetch</param>
		/// <returns>Returns the IEntity if the result is a single item; otherwise <value>null</value>.</returns>
		public static EntityType GetSingleEntityUsingPredicateExpression<EntityType>(IPredicate filter, RelationCollection relations, PrefetchPath prefetchPath)
			where EntityType : IEntity
		{
			IEntity entity = (IEntity)typeof(EntityType).GetConstructor(new System.Type[] { }).Invoke(new object[] { });
			return (EntityType)GetSingleEntityUsingPredicateExpression(filter, relations, prefetchPath, (IEntity)entity, false, false, null);
		}

		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// Doesn't throw any errors on multiple or none found.
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <param name="entity">Entity to be filled with the found result, should be initialized, returned as NULL if the fetched collection.count != 1</param>
		/// <returns>Returns true if a succesfull fetch was done (entity != null)</returns>
		public static EntityType GetSingleEntityUsingPredicateExpression<EntityType>(IPredicate filter, EntityType entity)
			where EntityType : IEntity
		{
			return (EntityType)GetSingleEntityUsingPredicateExpression(filter, null, null, (IEntity)entity, false, false);
		}

		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// Doesn't throw any errors on multiple or none found.
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <param name="collectionToFetchWith">Supply the collection to fetch the entities with, handy when you want to specify the transaction</param>
		/// <returns>Returns true if a succesfull fetch was done (entity != null)</returns>
		public static EntityType GetSingleEntityUsingPredicateExpression<EntityType>(IPredicate filter, IEntityCollection collectionToFetchWith)
			where EntityType : IEntity
		{
			IEntity entity = (IEntity)typeof(EntityType).GetConstructor(new System.Type[] { }).Invoke(new object[] { });
			return (EntityType)GetSingleEntityUsingPredicateExpression(filter, null, null, entity, false, false, collectionToFetchWith);
		}

		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// Doesn't throw any errors on multiple or none found.
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <param name="entity">Entity to be filled with the found result, should be initialized, returned as NULL if the fetched collection.count != 1</param>
		/// <param name="collectionToFetchWith">Supply the collection to fetch the entities with, handy when you want to specify the transaction</param>
		/// <returns>Returns true if a succesfull fetch was done (entity != null)</returns>
		public static EntityType GetSingleEntityUsingPredicateExpression<EntityType>(IPredicate filter, EntityType entity, IEntityCollection collectionToFetchWith)
			where EntityType : IEntity
		{
			return (EntityType)GetSingleEntityUsingPredicateExpression(filter, null, null, (IEntity)entity, false, false, collectionToFetchWith);
		}



		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// Doesn't throw any errors on multiple or none found.
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <param name="relations">Relations needed by the Filter</param>
		/// <param name="entity">Entity to be filled with the found result, should be initialized, returned as NULL if the fetched collection.count != 1</param>
		/// <returns>Returns true if a succesfull fetch was done (entity != null)</returns>
		public static IEntity GetSingleEntityUsingPredicateExpression(IPredicate filter, RelationCollection relations, IEntity entity)
		{
			return GetSingleEntityUsingPredicateExpression(filter, relations, null, entity, false, false);
		}


		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// Doesn't throw any errors on multiple or none found.
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <param name="relations">Relations needed by the Filter</param>
		/// <param name="prefetchPath">Prefetch path for the fetch</param>
		/// <param name="entity">Entity to be filled with the found result, should be initialized, returned as NULL if the fetched collection.count != 1</param>
		/// <returns>Returns true if a succesfull fetch was done (entity != null)</returns>
		public static IEntity GetSingleEntityUsingPredicateExpression(IPredicate filter, RelationCollection relations, PrefetchPath prefetchPath, IEntity entity)
		{
			return GetSingleEntityUsingPredicateExpression(filter, relations, prefetchPath, entity, false, false);
		}

		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <param name="relations">Relations needed by the Filter</param>
		/// <param name="prefetchPath">Prefetch path for the fetch</param>
		/// <param name="entity">Entity to be filled with the found result, should be initialized, returned as NULL if the fetched collection.count != 1</param>
		/// <param name="errorOnMultiple">Indicate if the function should throw a MultipleFoundNotAllowed error when multiple items are found</param>
		/// <param name="errorOnNone">Indicate if the function should throw a NoneFoundNotAllowed error when zero items are found</param>
		/// <returns>Returns the count of records found</returns>
		public static IEntity GetSingleEntityUsingPredicateExpression(IPredicate filter, RelationCollection relations, PrefetchPath prefetchPath,
			IEntity entity, bool errorOnMultiple, bool errorOnNone)
		{
			return GetSingleEntityUsingPredicateExpression(filter, relations, prefetchPath, entity, errorOnMultiple, errorOnNone, null);
		}

		/// <summary>
		/// Retrieve a single entity using a predicate expression
		/// </summary>
		/// <param name="filter">Filter to use for selecting the Entity</param>
		/// <param name="relations">Relations needed by the Filter</param>
		/// <param name="prefetchPath">Prefetch path for the fetch</param>
		/// <param name="entity">Entity to be filled with the found result, should be initialized, returned as NULL if the fetched collection.count != 1</param>
		/// <param name="errorOnMultiple">Indicate if the function should throw a MultipleFoundNotAllowed error when multiple items are found</param>
		/// <param name="errorOnNone">Indicate if the function should throw a NoneFoundNotAllowed error when zero items are found</param>
		/// <param name="collectionToFetchWith">Supply the collection to fetch the entities with, handy when you want to specify the transaction</param>
		/// <returns>Returns the count of records found</returns>
		public static IEntity GetSingleEntityUsingPredicateExpression(IPredicate filter, RelationCollection relations, PrefetchPath prefetchPath,
			IEntity entity, bool errorOnMultiple, bool errorOnNone, IEntityCollection collectionToFetchWith)
		{
			if (entity == null)
				throw new Dionysos.EmptyException("Parameter entity is null! Has to be intialized with the correct entitytype");

			// Use the entity to retrieve the entityname, then get rid of it!
			string entityTypeName = Dionysos.Data.LLBLGen.LLBLGenUtil.GetEntityName(entity);
			if (collectionToFetchWith == null)
				collectionToFetchWith = Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(entityTypeName) as IEntityCollection;

			// Retrieve entities based on supplied parameters (only request 2, since we don't need more to do the checks)
			collectionToFetchWith.GetMulti(filter, 2, null, relations, prefetchPath);

			// Determine result
			int uniqueMarkerForPredicateToQueryText = Dionysos.RandomUtil.Next(0, 10000);
			int recordCount = collectionToFetchWith.Count;
			if (recordCount == 1)
				entity = collectionToFetchWith[0];
			else if (recordCount > 1)
			{
				if (errorOnMultiple)
				{
					throw new Dionysos.MultipleFoundNotAllowed("Multiple entities found of type '{0}' for filter '{1}'", entityTypeName, filter.ToQueryText());
				}
			}
			else if (recordCount == 0)
			{
				if (errorOnNone)
				{
					throw new Dionysos.MultipleFoundNotAllowed("No entities found of type '{0}' for filter '{1}'", entityTypeName, filter.ToQueryText());
				}
			}

			if (entity.IsNew)
				entity = null;

			return entity;
		}

		/// <summary>
		/// Gets an IEntity instance using a FieldCompareValue predicate expression created by the specified arguments,
		/// </summary>
		/// <param name="entityName">The name of the entity to get the entity collection for</param>
		/// <param name="fieldNames">The names of the fields which are used to compare the values with</param>
		/// <param name="comparisonOperators">The operators used for comparison</param>
		/// <param name="values">The values which are used for comparison</param>        
		/// <returns>
		/// An SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance, null when found != 1
		/// </returns>
		public static IEntity GetEntityUsingFieldCompareValuePredicateExpression(string entityName, string[] fieldNames, ComparisonOperator[] comparisonOperators, object[] values)
		{
			return GetEntityUsingFieldCompareValuePredicateExpression(entityName, fieldNames, comparisonOperators, values, null);
		}


		/// <summary>
		/// Gets an IEntity instance using a FieldCompareValue predicate expression created by the specified arguments,
		/// </summary>
		/// <param name="entityName">The name of the entity to get the entity collection for</param>
		/// <param name="fieldNames">The names of the fields which are used to compare the values with</param>
		/// <param name="comparisonOperators">The operators used for comparison</param>
		/// <param name="values">The values which are used for comparison</param>
		/// <param name="transaction">The transaction to fetch with.</param>
		/// <returns>
		/// An SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance, null when found != 1
		/// </returns>
		public static IEntity GetEntityUsingFieldCompareValuePredicateExpression(string entityName, string[] fieldNames, ComparisonOperator[] comparisonOperators, object[] values, ITransaction transaction)
		{
			IEntity entity = null;

			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else if (Instance.ArgumentIsEmpty(fieldNames, "fieldNames"))
			{
				// Parameter 'fieldNames' is empty
			}
			else if (Instance.ArgumentIsEmpty(values, "values"))
			{
				// Parameter 'values' is empty
			}
			else
			{
				IEntityCollection entityCollection = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression(entityName, fieldNames, comparisonOperators, values, transaction);

				if (Instance.Empty(entityCollection))
				{
					throw new EmptyException("Variable 'entityCollection' is empty.");
				}
				else
				{
					if (entityCollection.Count == 0)
					{
						// No entities found
					}
					else if (entityCollection.Count == 1)
					{
						// Single entity found
						entity = entityCollection[0];
					}
					else if (entityCollection.Count > 0)
					{
						// Multiple entities found
					}
				}
			}

			return entity;
		}

		/// <summary>
		/// Gets an IEntity instance using a FieldCompareValue predicate expression created by the specified arguments (null when found != 1)
		/// </summary>
		/// <param name="entityName">The name of the entity to get the entity collection for</param>
		/// <param name="fieldName">The names of the fields which are used to compare the values with</param>
		/// <param name="comparisonOperator">The operators used for comparison</param>
		/// <param name="value">The values which are used for comparison</param>
		/// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance, null when found != 1</returns>
		public static IEntity GetEntityUsingFieldCompareValuePredicateExpression(string entityName, string fieldName, ComparisonOperator comparisonOperator, object value)
		{
			return GetEntityUsingFieldCompareValuePredicateExpression(entityName, new string[] { fieldName }, new ComparisonOperator[] { comparisonOperator }, new object[] { value });
		}

		public static IEntity GetEntityUsingFieldCompareValuePredicateExpression(string entityName, string fieldName, ComparisonOperator comparisonOperator, object value, ITransaction transaction)
		{
			return GetEntityUsingFieldCompareValuePredicateExpression(entityName, new string[] { fieldName }, new ComparisonOperator[] { comparisonOperator }, new object[] { value }, transaction);
		}

		/// <summary>
		/// (GK Experimental) Get the specific EntityRelation object for a Field (only usable for m:1 relations)
		/// </summary>
		/// <param name="startingEntity">Entity for the start of the relation (For example OrderItem to get to Order)</param>
		/// <param name="foreignKeyFieldOnStartingEntity">ForeignKey field (for example: OrderId on OrderItem)</param>
		/// <returns>RelatedEntity</returns>
		public IEntityRelation GetRelationByField(IEntity startingEntity, IEntityField foreignKeyFieldOnStartingEntity)
		{
			IRelationCollection relations = LLBLGenRelationUtil.GetRelations(startingEntity);
			for (int i = 0; i < relations.Count; i++)
			{
				IEntityRelation rel = relations[i] as IEntityRelation;
				if ((rel.TypeOfRelation == RelationType.ManyToOne) &&
				(rel.GetAllFKEntityFieldCoreObjects().Count == 1) &&
				(rel.GetFKEntityFieldCore(0).Name == foreignKeyFieldOnStartingEntity.Name))
					return rel;
			}
			return null;
		}
		
		#endregion
	}
}