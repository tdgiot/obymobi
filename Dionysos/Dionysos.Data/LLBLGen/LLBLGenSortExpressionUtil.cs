﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
	/// <summary>
	/// Utility class for SortExpression.
	/// </summary>
	public static class LLBLGenSortExpressionUtil
	{
		/// <summary>
		/// Adds the range.
		/// </summary>
		/// <param name="sort">The sort.</param>
		/// <param name="sortClauses">The sort clauses.</param>
		public static void AddRange(this ISortExpression sort, ISortExpression sortClauses)
		{
			LLBLGenSortExpressionUtil.AddRange(sort, sortClauses.Cast<SortClause>());
		}

		/// <summary>
		/// Adds the range.
		/// </summary>
		/// <param name="sort">The sort.</param>
		/// <param name="sortClauses">The sort clauses.</param>
		public static void AddRange(this ISortExpression sort, IEnumerable<SortClause> sortClauses)
		{
			foreach (SortClause sortClause in sortClauses)
			{
				sort.Add(sortClause);
			}
		}

		/// <summary>
		/// Adds the sort clause using the specified sort operator and the first field value that is not <c>NULL</c>.
		/// </summary>
		/// <param name="sort">The sort.</param>
		/// <param name="sortOperator">The sort operator.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// The index of the sort clause.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">sort</exception>
		/// <exception cref="System.ArgumentException">fields</exception>
		public static int AddUsingIsNull(this ISortExpression sort, SortOperator sortOperator, params IEntityField[] fields)
		{
			if (sort == null) throw new ArgumentNullException("sort");
			if (fields == null) throw new ArgumentNullException("fields");
			if (fields.Length == 0) throw new ArgumentException("fields");

			// Create field to sort on
			IEntityField sortField = LLBLGenSortExpressionUtil.GetSortFieldUsingIsNull(fields);

			// Create and add sort clause
			SortClause sortClause = new SortClause(sortField, sortOperator);
			sortClause.EmitAliasForExpressionAggregateField = false;

			return sort.Add(sortClause);
		}

		/// <summary>
		/// Inserts the sort clause using the specified sort operator and the first field value that is not <c>NULL</c>.
		/// </summary>
		/// <param name="sort">The sort.</param>
		/// <param name="index">The index.</param>
		/// <param name="sortOperator">The sort operator.</param>
		/// <param name="fields">The fields.</param>
		/// <exception cref="System.ArgumentNullException">sort</exception>
		/// <exception cref="System.ArgumentException">fields</exception>
		public static void InsertUsingIsNull(this ISortExpression sort, int index, SortOperator sortOperator, params IEntityField[] fields)
		{
			if (sort == null) throw new ArgumentNullException("sort");
			if (fields == null) throw new ArgumentNullException("fields");
			if (fields.Length == 0) throw new ArgumentException("fields");

			// Create field to sort on
			IEntityField sortField = LLBLGenSortExpressionUtil.GetSortFieldUsingIsNull(fields);

			// Create and add sort clause
			SortClause sortClause = new SortClause(sortField, sortOperator);
			sortClause.EmitAliasForExpressionAggregateField = false;

			sort.Insert(index, sortClause);
		}

		/// <summary>
		/// Reverses the sort order.
		/// </summary>
		/// <param name="sort">The sort.</param>
		/// <exception cref="System.ArgumentNullException">sort</exception>
		public static void ReverseSortOrder(this ISortExpression sort)
		{
			if (sort == null) throw new ArgumentNullException("sort");

			foreach (SortClause sortClause in sort)
			{
				if (sortClause.SortOperatorToUse == SortOperator.Ascending)
				{
					sortClause.SortOperatorToUse = SortOperator.Descending;
				}
				else
				{
					sortClause.SortOperatorToUse = SortOperator.Ascending;
				}
			}
		}

		/// <summary>
		/// Adds the sort clause using the specified sort operator and fields.
		/// </summary>
		/// <param name="sort">The sort.</param>
		/// <param name="sortOperator">The sort operator.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// The index of the sort clause.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">sort</exception>
		/// <exception cref="System.ArgumentException">fields</exception>
		public static int AddUsingHasValue(this ISortExpression sort, SortOperator sortOperator, params IEntityField[] fields)
		{
			if (sort == null) throw new ArgumentNullException("sort");
			if (fields == null) throw new ArgumentNullException("fields");
			if (fields.Length == 0) throw new ArgumentException("fields");

			// Create field to sort on
			IEntityField sortField = LLBLGenSortExpressionUtil.GetSortFieldUsingIsNull(fields);

			// Create and add sort clause
			SortClause sortClause = new SortClause(LLBLGenDbFunctionCallUtil.HasValue(sortField), sortOperator);
			sortClause.EmitAliasForExpressionAggregateField = false;

			return sort.Add(sortClause);
		}

		/// <summary>
		/// Inserts the sort clause using the specified sort operator and fields.
		/// </summary>
		/// <param name="sort">The sort.</param>
		/// <param name="index">The index.</param>
		/// <param name="sortOperator">The sort operator.</param>
		/// <param name="fields">The fields.</param>
		/// <exception cref="System.ArgumentNullException">sort</exception>
		/// <exception cref="System.ArgumentException">fields</exception>
		public static void InsertUsingHasValue(this ISortExpression sort, int index, SortOperator sortOperator, params IEntityField[] fields)
		{
			if (sort == null) throw new ArgumentNullException("sort");
			if (fields == null) throw new ArgumentNullException("fields");
			if (fields.Length == 0) throw new ArgumentException("fields");

			// Create field to sort on
			IEntityField sortField = LLBLGenSortExpressionUtil.GetSortFieldUsingIsNull(fields);

			// Create and add sort clause
			SortClause sortClause = new SortClause(LLBLGenDbFunctionCallUtil.HasValue(sortField), sortOperator);
			sortClause.EmitAliasForExpressionAggregateField = false;

			sort.Insert(index, sortClause);
		}

		/// <summary>
		/// Gets the sort field for the first field value that is not <c>NULL</c>.
		/// </summary>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">fields</exception>
		/// <exception cref="System.ArgumentException">fields</exception>
		private static IEntityField GetSortFieldUsingIsNull(params IEntityField[] fields)
		{
			if (fields == null) throw new ArgumentNullException("fields");
			if (fields.Length == 0) throw new ArgumentException("fields");

			// Create field to sort on
			IEntityField sortField = null;
			foreach (IEntityField field in fields)
			{
				if (sortField == null)
				{
					sortField = field;
				}
				else
				{
					sortField = LLBLGenDbFunctionCallUtil.IsNull(sortField, field);
				}
			}

			return sortField;
		}
	}
}
