using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Collection class used for storing SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instances in
    /// </summary>
    public class LLBLGenEntityCollectionPool : ICollection<SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the LLBLGenEntityCollectionPool class
        /// </summary>	
        public LLBLGenEntityCollectionPool()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to the collection
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to add to the collection</param>
        public void Add(SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection)
        {
            this.items.Add(entityCollection);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance is already in the collection
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to check</param>
        /// <returns>True if the SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance is in the collection, False if not</returns>
        public bool Contains(SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection) == entityCollection)
                {
                    contains = true;
                    break;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance from this collection
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to remove</param>
        public bool Remove(SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection)
        {
            this.items.Remove(entityCollection);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.EntityCollectionPool
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection> GetEnumerator()
        {
            return (IEnumerator<SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.EntityCollectionPool
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets an SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance from the collection using the specified entity name
        /// If no SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance is found, a new SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance is created
        /// </summary>
        /// <param name="entityName">The entity name of the SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection GetEntityCollection(string entityName)
        {
            SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection = null;

            // Walk through the SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instances of this collection
            for (int i = 0; i < this.Count; i++)
            {
                SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection temp = this[i];
                if (DataFactory.EntityCollectionUtil.GetEntityName(temp) == entityName)
                {
                    entityCollection = temp;
                    break;
                }
            }

            // No entity collection is found in the pool
            // So create a new one using the specified entity name
            if (entityCollection == null)
            {
                entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(entityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
                this.Add(entityCollection);
            }

            return entityCollection;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to get</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection this[int index]
        {
            get
            {
                return this.items[index] as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
            }
        }

        /// <summary>
        /// Gets an SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance from the collection with the specified entity name
        /// </summary>
        /// <param name="entityName">The entity name of the SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to get</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection this[string entityName]
        {
            get
            {
                return this.GetEntityCollection(entityName);
            }
        }

        #endregion
    }
}