﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
	/// <summary>
	/// Class for performing action with LLBLGenDataSources
	/// </summary>
	public class LLBLGenDataSourceUtil
	{
		/// <summary>
		/// Get a LLBLGenProDataSource for a EntityCollection
		/// </summary>
		/// <param name="dataSourceId">dataSourceId</param>
		/// <param name="entityTypeName">entityTypeName</param>
		/// <param name="livePersistence">livePersistence</param>
		/// <param name="cacheLocation">cacheLocation</param>
		/// <param name="enablePaging">enablePaging</param>
		/// <param name="selectParameter">selectParameter</param>
		/// <returns>LLBLGenProDataSource</returns>
		public static LLBLGenProDataSource GetEntityCollectionDataSource(string dataSourceId, string entityTypeName, bool livePersistence, DataSourceCacheLocation cacheLocation, bool enablePaging, Parameter selectParameter)
		{
			// Initalize DataSource
			LLBLGenProDataSource ds = new LLBLGenProDataSource();

			// Initialize some standard values
			ds.ID = dataSourceId;
			ds.LivePersistence = livePersistence;
			ds.CacheLocation = cacheLocation;
			ds.DataContainerType = DataSourceDataContainerType.EntityCollection;
			ds.EnablePaging = enablePaging;
			
			// Retrieve the full type, assembly name of the collection to load
			SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection collection = Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(entityTypeName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
			Type typeOfCollection = collection.GetType();
			ds.EntityCollectionTypeName = string.Format("{0}, {1}", typeOfCollection.FullName, typeOfCollection.Assembly);

			if(selectParameter != null)
				ds.SelectParameters.Add(selectParameter);

			return ds;
		}

		/// <summary>
		/// Get a LLBLGenProDataSource for a EntityCollection
		/// </summary>
		/// <param name="dataSourceId">dataSourceId</param>
		/// <param name="entityTypeName">entityTypeName</param>
		/// <returns>LLBLGenProDataSource</returns>
		public static LLBLGenProDataSource GetEntityCollectionDataSource(string dataSourceId, string entityTypeName) 
		{
			return GetEntityCollectionDataSource(dataSourceId, entityTypeName, true, DataSourceCacheLocation.None, true, null);
		}
	}
}
