﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Factory class for creating relation instances with
    /// </summary>
    public class LLBLGenEntityPrefetchPathFactory : Dionysos.Interfaces.IEntityPrefetchPathFactory
    {
        #region Fields

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the RelationFactory class if the instance has not been initialized yet
        /// </summary>
        public LLBLGenEntityPrefetchPathFactory()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static RelationFactory instance
        /// </summary>
        private void Init()
        {
        }

        public object CreatePrefetchPath(string entityName)
        {
            IPrefetchPath prefetchPath = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName") || String.IsNullOrEmpty(entityName))
            {
                // Parameter 'entityName' is empty
            }
            else
            {
                Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
                if (Instance.Empty(dataAssemblyInfo))
                {
                    // Data assembly does not exist
                    // or is not added to the Global.AssemblyInfo
                    throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
                }
                else
                {
                    Assembly assembly = dataAssemblyInfo.Assembly;
                    if (Instance.Empty(assembly))
                    {
                        // Assembly is empty
                        throw new Dionysos.EmptyException("Data assembly is empty");
                    }
                    else
                    {                        
                        try
                        {
                            IEntity entity = (IEntity)new LLBLGenEntityFactory().GetEntity(entityName);

                            int sourceEntityTypeType = (int)this.GetEntityType(entityName);                            
                            if (sourceEntityTypeType > 0)
                            {
                                // Create the prefetch path
                                prefetchPath = new PrefetchPath(sourceEntityTypeType);
                            }
                        }
                        catch
                        {
                            prefetchPath = null;
                        }                                     
                    }
                }
            }            

            return prefetchPath;
        }
        
        public object CreatePrefetchPathElement(string sourceEntityName, string destinationEntityName)
        {
            IPrefetchPathElement prefetchPathElement = null;

            if (Instance.ArgumentIsEmpty(sourceEntityName, "entityName") || String.IsNullOrEmpty(sourceEntityName) || String.IsNullOrEmpty(destinationEntityName))
            {
                // Parameter 'entityName' is empty
            }
            else
            {
                Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
                if (Instance.Empty(dataAssemblyInfo))
                {
                    // Data assembly does not exist
                    // or is not added to the Global.AssemblyInfo
                    throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
                }
                else
                {
                    Assembly assembly = dataAssemblyInfo.Assembly;
                    if (Instance.Empty(assembly))
                    {
                        // Assembly is empty
                        throw new Dionysos.EmptyException("Data assembly is empty");
                    }
                    else
                    {
                        try                       
                        {
                            IEntity entity = (IEntity)new LLBLGenEntityFactory().GetEntity(sourceEntityName);

                            if (!destinationEntityName.Contains("Entity"))
                                destinationEntityName = string.Format("{0}Entity", destinationEntityName);

                            int sourceEntityTypeType = (int)this.GetEntityType(sourceEntityName);
                            int destinationEntityType = (int)this.GetEntityType(destinationEntityName);
                            if (sourceEntityTypeType > 0 && destinationEntityType > 0)
                            {
                                IEntityRelation relation = (IEntityRelation)entity.GetRelationsForFieldOfType(destinationEntityName)[0];

                                // Create the PrefetchPathElement
                                prefetchPathElement = new PrefetchPathElement((IEntityCollection)new LLBLGenEntityCollectionFactory().GetEntityCollection(destinationEntityName, assembly),
                                    relation,
                                    sourceEntityTypeType,
                                    destinationEntityType,
                                    0,
                                    null,
                                    null,
                                    null,
                                    string.Format("{0}Entity", LLBLGenUtil.GetEntityName(destinationEntityName)), 
                                    relation.TypeOfRelation);                                
                            }
                        }
                        catch
                        {
                            prefetchPathElement = null;
                        }
                    }
                }
            }

            return prefetchPathElement;
        }

        public object GetEntityType(string entityName)
        {
            int entityType = -1;

            if (Instance.ArgumentIsEmpty(entityName, "entityName") || String.IsNullOrEmpty(entityName))
            {
                // Parameter 'entityName' is empty
            }
            else
            { 
                Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
                if (Instance.Empty(dataAssemblyInfo))
                {
                    // Data assembly does not exist
                    // or is not added to the Global.AssemblyInfo
                    throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
                }
                else
                { 
                    Assembly assembly = dataAssemblyInfo.Assembly;
                    if (Instance.Empty(assembly))
                    {
                        // Assembly is empty
                        throw new Dionysos.EmptyException("Data assembly is empty");
                    }
                    else
                    {
                        string fullTypeName = string.Format("{0}.EntityType", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""));

                        if (!entityName.Contains("Entity"))
                            entityName = string.Format("{0}Entity", entityName);

                        Type enumType = assembly.GetType(fullTypeName);
                        if (enumType != null)
                        {
                            System.Reflection.FieldInfo fieldInfo = enumType.GetField(entityName);
                            if (fieldInfo != null)
                            {                                
                                entityType = (int)fieldInfo.GetRawConstantValue();
                            }
                        }                        
                    }
                }
            }

            return entityType;
        }

        #endregion
    }
}