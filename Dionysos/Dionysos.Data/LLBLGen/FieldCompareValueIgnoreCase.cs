﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
	/// <summary>
	/// A case insensitive field value comparison filter.
	/// </summary>
	public class FieldCompareValueIgnoreCasePredicate : FieldCompareValuePredicate
	{
		#region Properties

		/// <summary>
		/// Gets a value indicating whether to use a case sensitive collation.
		/// </summary>
		/// <value>
		/// <c>true</c> if a case sensitive collation is used; otherwise, <c>false</c>.
		/// </value>
		public new bool CaseSensitiveCollation
		{
			get
			{
				return base.CaseSensitiveCollation;
			}
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public override object Value
		{
			get
			{
				return base.Value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldCompareValueIgnoreCasePredicate" /> class.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="value">The value.</param>
		public FieldCompareValueIgnoreCasePredicate(IEntityField field, string value)
			: this(field, value, null, false)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldCompareValueIgnoreCasePredicate" /> class.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="value">The value.</param>
		/// <param name="negate">If set to <c>true</c> negates the filter.</param>
		public FieldCompareValueIgnoreCasePredicate(IEntityField field, string value, bool negate)
			: this(field, value, null, negate)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldCompareValueIgnoreCasePredicate" /> class.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="value">The value.</param>
		/// <param name="objectAlias">The object alias.</param>
		public FieldCompareValueIgnoreCasePredicate(IEntityField field, string value, string objectAlias)
			: this(field, value, objectAlias, false)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldCompareValueIgnoreCasePredicate" /> class.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="value">The value.</param>
		/// <param name="objectAlias">The object alias.</param>
		/// <param name="negate">If set to <c>true</c> negates the filter.</param>
		public FieldCompareValueIgnoreCasePredicate(IEntityField field, string value, string objectAlias, bool negate)
			: base(field, ComparisonOperator.Equal, value != null ? value.ToUpperInvariant() : value, objectAlias, negate)
		{
			base.CaseSensitiveCollation = true;
		}

		#endregion
	}
}
