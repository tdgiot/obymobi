﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Dionysos;
using Dionysos.Reflection;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Interfaces.Data;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Utility class which can be used to support LLBLGen relation classes
    /// </summary>
    public class LLBLGenRelationUtil
    {
        #region Fields

        static LLBLGenRelationUtil instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the LLBLGenRelationUtil class if the instance has not been initialized yet
        /// </summary>
        public LLBLGenRelationUtil()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new LLBLGenRelationUtil();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static LLBLGenRelationUtil instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Creates a new System.Object instance using the specified entityname from the default data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <returns>An IEntity instance</returns>
        private static object GetRelationsObject(string entityName)
        {
            object relations = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else
            {
                Dionysos.Reflection.AssemblyInfo dataAssemblyInfo = Global.AssemblyInfo["Data"];
                if (Instance.Empty(dataAssemblyInfo))
                {
                    // Data assembly does not exist
                    // or is not added to the Global.AssemblyInfo
                    throw new Dionysos.EmptyException("Data assembly does not exist or is not added to the Global.AssemblyInfo");
                }
                else
                {
                    Assembly assembly = dataAssemblyInfo.Assembly;
                    if (Instance.Empty(assembly))
                    {
                        // Assembly is empty
                        throw new Dionysos.EmptyException("Data assembly is empty");
                    }
                    else
                    {
                        if (entityName.EndsWith("Relation"))
                        {
                            entityName = entityName.Replace("Relation", "");
                        }

                        string fullTypeName = string.Format("{0}.RelationClasses.{1}Relations", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), entityName);

                        System.Type type = assembly.GetType(fullTypeName);
                        if (type == null)
                        {
                            // The type could not be found using the fullTypeName
                            throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
                        }
                        else
                        {
                            relations = Dionysos.InstanceFactory.CreateInstance(assembly, type);
                        }
                    }
                }
            }

            return relations;
        }

        /// <summary>
        /// Creates a new System.Object instance using the specified entityname from the specified data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <param name="assembly">The assembly which contains the entity type</param>
        /// <returns>An System.Object instance</returns>
        private static object GetRelationsObject(string entityName, Assembly assembly)
        {
            object relations = null;

            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else if (Instance.ArgumentIsEmpty(assembly, "assembly"))
            {
                // Parameter 'assembly' is empty
            }
            else
            {
                if (entityName.EndsWith("Relation"))
                {
                    entityName = entityName.Replace("Relation", "");
                }

                string fullTypeName = string.Format("{0}.RelationClasses.{1}Relations", assembly.ManifestModule.Name.Replace(".dll", "").Replace(".DLL", ""), entityName);

                System.Type type = assembly.GetType(fullTypeName);
                if (type == null)
                {
                    // The type could not be found using the fullTypeName
                    throw new Dionysos.EmptyException(string.Format("Type '{0}' does not exist. Inconsistent naming convention of data project detected", fullTypeName));
                }
                else
                {
                    relations = Dionysos.InstanceFactory.CreateInstance(assembly, type);
                }
            }

            return relations;
        }

        /// <summary>
        /// Gets the relations from an entity instance
        /// </summary>
        /// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to get the relations from</param>
        /// <returns>An IRelationCollection instance containing the relations</returns>
        public static SD.LLBLGen.Pro.ORMSupportClasses.IRelationCollection GetRelations(IEntity entity)
        {
            SD.LLBLGen.Pro.ORMSupportClasses.RelationCollection relations = new SD.LLBLGen.Pro.ORMSupportClasses.RelationCollection();

            if (Instance.ArgumentIsEmpty(entity, "entity"))
            {
                // Parameter 'entity' is empty
            }
            else
            {
                object relationsObject = GetRelationsObject(Dionysos.Data.LLBLGen.LLBLGenUtil.GetEntityName(entity));
                if (Instance.Empty(relationsObject))
                {
                    throw new EmptyException("Variable 'relationsObject' is empty.");
                }
                else
                {
                    string[] members = Dionysos.Reflection.Member.GetMemberNames(relationsObject);
                    for (int i = 0; i < members.Length; i++)
                    {
                        string member = members[i].ToUpper();
                        if (member.Contains("USING") && !member.Contains("GET_") && !member.Contains("SET_"))
                        {
                            IEntityRelation relation = Dionysos.Reflection.Member.InvokeProperty(relationsObject, members[i]) as IEntityRelation;
                            if (!Instance.Empty(relation))
                            {
                                relations.Add(relation);
                            }
                        }
                    }
                }
            }

            return relations;
        }

        /// <summary>
        /// Gets the one-to-many relations from an entity instance
        /// </summary>
        /// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to get the relations from</param>
        /// <param name="relationType">The SD.LLBLGen.Pro.ORMSupportClasses.RelationType instance to get the relations for</param>
        /// <returns>An IRelationCollection instance containing the relations</returns>
        public static SD.LLBLGen.Pro.ORMSupportClasses.IRelationCollection GetRelationsByRelationType(IEntity entity, RelationType relationType)
        {
            IRelationCollection relations = new RelationCollection();

            IRelationCollection temp = GetRelations(entity);
            for (int i = 0; i < temp.Count; i++)
            {
                IEntityRelation relation = temp[i] as IEntityRelation;
                if (Instance.Empty(relation))
                {
                    throw new EmptyException("Variable 'relation' is empty.");
                }
                else
                {
                    if (relation.TypeOfRelation == relationType)
                    {
                        relations.Add(relation);
                    }
                }
            }

            return relations;
        }

        /// <summary>
        /// Gets a relation from an entity instance for the specified foreign key name
        /// </summary>
        /// <param name="entity">The entity to get the relation from</param>
        /// <param name="foreignKeyName">The name of the foreign key field to get the relation for</param>
        /// <returns>A IRelation instance containing the relation</returns>
        public static SD.LLBLGen.Pro.ORMSupportClasses.IEntityRelation GetRelation(IEntity entity, string foreignKeyName)
        {
            IEntityRelation relationToReturn = null;

            IRelationCollection relations = GetRelations(entity);
            for (int i = 0; i < relations.Count; i++)
            {
                IEntityRelation relation = relations[i] as IEntityRelation;
                if (Instance.Empty(relation))
                {
                    throw new EmptyException("Variable 'relation' is empty.");
                }
                else
                {
                    // Get the foreign key field from the relation
                    IEntityFieldCore fkField = relation.GetFKEntityFieldCore(0) as IEntityFieldCore;
                    if (fkField.Name.ToUpper() == foreignKeyName.ToUpper())
                    {
                        relationToReturn = relation;
                        break;
                    }
                }
            }

            return relationToReturn;
        }

        #endregion
    }
}