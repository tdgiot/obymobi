﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Data.LLBLGen
{
    public static class EntityCollection
    {
        /// <summary>
        /// Gets the amount of entity objects in the database, when taking into account the filter and relations specified.
        /// </summary>
        public static int GetDbCount<T>(IPredicate predicate, IRelationCollection relations) where T : IEntityCollection, new()
        {
            T entityCollection = new T();
            return entityCollection.GetDbCount(predicate, relations);
        }

        /// <summary>
        /// Gets the amount of entity objects in the database, when taking into account the filter specified.
        /// </summary>
        public static int GetDbCount<T>(IPredicate predicate) where T : IEntityCollection, new()
        {
            return EntityCollection.GetDbCount<T>(predicate, null);
        }

        /// <summary>
        /// Creates and returns an entity collection of the specified type and retrieves all entity objects which match the specified parameters.
        /// </summary>
        public static T GetMulti<T>(IPredicate predicate, ISortExpression sort, IRelationCollection relations, IPrefetchPath prefetch, ExcludeIncludeFieldsList fields) where T : IEntityCollection, new()
        {
            T entityCollection = new T();
            entityCollection.GetMulti(predicate, 0, sort, relations, prefetch, fields, 0, 0);

            return entityCollection;
        }

        /// <summary>
        /// Creates and returns an entity collection of the specified type and retrieves all entity objects which match the specified parameters.
        /// </summary>
        public static T GetMulti<T>(IPredicate predicate, ISortClause sort, IEntityRelation relation, IPrefetchPath prefetch, params IEntityFieldCore[] includeFields) where T : IEntityCollection, new()
        {
            SortExpression sortExpression = null;
            if (sort != null) sortExpression = new SortExpression(sort);

            RelationCollection relations = null;
            if (relation != null) relations = new RelationCollection(relation);

            IncludeFieldsList fields = null;
            if (includeFields != null && includeFields.Length > 0 && !(includeFields.Length == 1 && includeFields[0] == null))
                fields = new IncludeFieldsList(includeFields);

            return EntityCollection.GetMulti<T>(predicate, sortExpression, relations, prefetch, fields);
        }
        
        /// <summary>
        /// Creates and returns an entity collection of the specified type and retrieves all entity objects which match the specified parameters.
        /// </summary>
        public static T GetMulti<T>(IPredicate predicate, IEntityRelation relation, IEntityFieldCore field) where T : IEntityCollection, new()
        {
            return EntityCollection.GetMulti<T>(predicate, null, relation, null, field);
        }

        /// <summary>
        /// Creates and returns an entity collection of the specified type and retrieves all entity objects which match the specified filter.
        /// </summary>
        public static TEntityCollection GetMulti<TEntityCollection>(IPredicate predicate) where TEntityCollection : IEntityCollection, new()
        {
            return EntityCollection.GetMulti<TEntityCollection>(predicate, null, null);
        }

        /// <summary>
        /// Retrieves a single entity of the specified type which matches the specified parameters.
        /// </summary>
        public static TEntity GetEntity<TEntityCollection, TEntity>(IPredicate predicate, ISortClause sort, IEntityRelation relation, IPrefetchPath prefetch, params IEntityFieldCore[] includeFields) 
            where TEntityCollection : IEntityCollection, new() 
            where TEntity : IEntity
        {
            SortExpression sortExpression = null;
            if (sort != null) sortExpression = new SortExpression(sort);

            RelationCollection relations = null;
            if (relation != null) relations = new RelationCollection(relation);

            IncludeFieldsList fields = null;
            if (includeFields != null && includeFields.Length > 0)
                fields = new IncludeFieldsList(includeFields);

            return EntityCollection.GetEntity<TEntityCollection, TEntity>(predicate, sortExpression, relations, prefetch, fields);
        }

        /// <summary>
        /// Retrieves a single entity of the specified type which matches the specified parameters.
        /// </summary>
        public static TEntity GetEntity<TEntityCollection, TEntity>(IPredicate predicate, ISortExpression sort, IRelationCollection relations, IPrefetchPath prefetch, ExcludeIncludeFieldsList fields)
            where TEntityCollection : IEntityCollection, new()
            where TEntity : IEntity
        {
            TEntity entity = default(TEntity);

            TEntityCollection entityCollection = GetMulti<TEntityCollection>(predicate, sort, relations, prefetch, fields);
            if (entityCollection.Count == 0)
            {
                // No entity found
                throw new ApplicationException(string.Format("No entities of type '{0}' were found for the specified parameters.", typeof(TEntity).Name));
            }
            else if (entityCollection.Count > 1)
            {
                // Multiple entities found
                throw new ApplicationException(string.Format("Multiple entities of type '{0}' were found for the specified parameters.", typeof(TEntity).Name));
            }
            else
            {
                entity = (TEntity)entityCollection[0];
            }

            return entity;
        }

        /// <summary>
        /// Retrieves a single entity of the specified type which matches the specified parameters.
        /// </summary>
        public static TEntity GetEntity<TEntityCollection, TEntity>(IPredicate predicate, IEntityRelation relation, IEntityFieldCore field)
            where TEntityCollection : IEntityCollection, new()
            where TEntity : IEntity
        {
            return EntityCollection.GetEntity<TEntityCollection, TEntity>(predicate, null, relation, null, field);
        }
    }
}
