using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Factory class for getting related SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instances with
    /// </summary>
    public class LLBLGenRelatedEntityCollectionFactory
    {
        #region Fields

        static LLBLGenRelatedEntityCollectionFactory instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the RelatedEntityCollectionFactory class if the instance has not been initialized yet
        /// </summary>
        public LLBLGenRelatedEntityCollectionFactory()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new LLBLGenRelatedEntityCollectionFactory();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static RelatedEntityCollectionFactory instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Gets a related SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance from an existing SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance
        /// </summary>
        /// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to get the related SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance from</param>
        /// <param name="relatedEntityCollectionName">The name of the related entity</param>
        /// <returns>An SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance</returns>
        public static SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection GetRelatedEntityCollection(SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity, string relatedEntityCollectionName)
        {
            SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection relatedEntityCollection = Dionysos.Reflection.Member.InvokeProperty(entity, relatedEntityCollectionName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
            if (Instance.Empty(relatedEntityCollection))
            {
                throw new Dionysos.EmptyException("Variable 'relatedEntityCollection' is empty");
            }
            return relatedEntityCollection;
        }

        #endregion
    }
}