﻿using System.Collections;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Utility class for working with LLBLGen
    /// </summary>
    public static class LLBLGenLinqUtil
	{
		/// <summary>
		/// Converts the IEnumerable to an EntityCollection.
		/// </summary>
		/// <typeparam name="T">The entity collection type.</typeparam>
		/// <param name="source">The source.</param>
		/// <returns>
		/// The entity collection with all source items.
		/// </returns>
		public static T ToEntityCollection<T>(this IEnumerable source)
			where T : IEntityCollection, new()
		{
			T entityCollection = new T();

			foreach (IEntity sourceItem in source)
			{
				entityCollection.Add(sourceItem);
			}

			return entityCollection;
		}
	}
}
