﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.LLBLGen
{
    /// <summary>
    /// Factory class for creating relation instances with
    /// </summary>
    public class LLBLGenRelationFactory
    {
        #region Fields

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the RelationFactory class if the instance has not been initialized yet
        /// </summary>
        public LLBLGenRelationFactory()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static RelationFactory instance
        /// </summary>
        private void Init()
        {
        }



        #endregion
    }
}