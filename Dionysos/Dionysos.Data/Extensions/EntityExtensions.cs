﻿using System;
using System.Linq;
using Dionysos.Interfaces.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data.Extensions
{
    public static class EntityExtensions
    {
        public static string GetDefaultEntityEditPage(this IEntity entity)
        {
            IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(entity);
            string defaultEntityEditPage = entityInformation.DefaultEntityEditPage;

            if (defaultEntityEditPage.IsNullOrWhiteSpace())
            {
                throw new Exception($"No DefaultEntityEditPage set for entity of type {entity.GetType().Name}, configure it in the EntityInformation table in the database.");
            }

            return defaultEntityEditPage;
        }

        public static string GetDefaultEntityCollectionPage(this IEntity entity)
        {
            IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(entity);
            string defaultEntityCollectionPage = entityInformation.DefaultEntityCollectionPage;

            if (defaultEntityCollectionPage.IsNullOrWhiteSpace())
            {
                throw new Exception($"No DefaultEntityCollectionPage set for entity of type {entity.GetType().Name}, configure it in the EntityInformation table in the database.");
            }

            return defaultEntityCollectionPage;
        }

        public static bool IsChanged(this IEntity entity, EntityField entityField) => entity.Fields[entityField.Name].IsChanged;

        public static bool HasAnyChanged(this IEntity entity, params EntityField[] entityFields) => entityFields.Any(entity.IsChanged);
    }
}
