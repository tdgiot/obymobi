﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data
{
    /// <summary>
    /// Enumator for setting delete rules for referential constraints
    /// </summary>
    public enum ReferentialConstraintDeleteRule : int
    {
        /// <summary>
        /// No action is being performed on foreign records
        /// </summary>
        NoAction = 0,
        /// <summary>
        /// Foreign records are being deleted is a record is being deleted
        /// </summary>
        Cascade = 1,
        /// <summary>
        /// Foreign keys are being set to null if a record is being deleted
        /// </summary>
        SetNull = 2,
        /// <summary>
        /// Default value is being set if a record is being deleted
        /// </summary>
        SetDefault = 3
    }
}
