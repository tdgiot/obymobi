﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Data
{
    public class NonTransactionalTransaction
    {
        #region Fields

        private Stack<Record> records = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the NonTransactionalTransaction type.
        /// </summary>
        public NonTransactionalTransaction()
        {
            this.records = new Stack<Record>();
        }

        #endregion

        #region Methods

        public void Add(string entityName, int id)
        {
            this.records.Push(new Record(entityName, id));
        }

        public void Rollback()
        {
            foreach (Record record in this.records)
            {
                IEntity entity = DataFactory.EntityFactory.GetEntity(record.EntityName, record.Id) as IEntity;
                if (entity != null)
                    entity.Delete();
            }
        }

        #endregion

        #region Inner types

        private class Record
        {
            public Record(string entityName, int id)
            {
                this.EntityName = entityName;
                this.Id = id;
            }

            public string EntityName { get; set; }
            public int Id { get; set; }
        }

        #endregion
    }
}
