﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Dionysos.Data
{
	public static class DataRowUtil
	{
		/// <summary>
		/// Get Int value, if column value is Empty NULL is returend, invalid INT value throws an error
		/// </summary>
		/// <param name="row">row to look in</param>
		/// <param name="column">column to get value from</param>
		/// <returns></returns>
		public static int? GetInt(this DataRow row, string column)
		{
			row.ValidateColumnExists(column);

			int? toReturn = null;
			int value;
			if(row[column].ToString().Length == 0)
				toReturn = null;
			else if (!int.TryParse(row[column].ToString(), out value))
			{
				throw new Dionysos.FunctionalException("De waarvan van kolom '{0}' is '{1}', dit kan niet worden omgezet naar een geheel getal (int).", column, row[column].ToString());
			}			
			else
				toReturn = value;

			return toReturn;
		}

		/// <summary>
		/// Get Decimal value, if column value is Empty NULL is returend, invalid decimal value throws an error
		/// </summary>
		/// <param name="row">row to look in</param>
		/// <param name="column">column to get value from</param>
		/// <returns></returns>
		public static decimal? GetDecimal(this DataRow row, string column)
		{
			row.ValidateColumnExists(column);

			decimal? toReturn = null;
			decimal value;
			if (row[column].ToString().Length == 0)
				toReturn = null;
			else if (!decimal.TryParse(row[column].ToString(), out value))
			{
				throw new Dionysos.FunctionalException("De waarvan van kolom '{0}' is '{1}', dit kan niet worden omgezet naar een decimaal getal (decimal).", column, row[column].ToString());
			}
			else
				toReturn = value;

			return toReturn;
		}

		/// <summary>
		/// Get double value, if column value is Empty NULL is returend, invalid double value throws an error
		/// </summary>
		/// <param name="row">row to look in</param>
		/// <param name="column">column to get value from</param>
		/// <returns></returns>
		public static double? GetDouble(this DataRow row, string column)
		{
			row.ValidateColumnExists(column);

			double? toReturn = null;
			double value;
			if (row[column].ToString().Length == 0)
				toReturn = null;
			else if (!double.TryParse(row[column].ToString(), out value))
			{
				throw new Dionysos.FunctionalException("De waarvan van kolom '{0}' is '{1}', dit kan niet worden omgezet naar een decimaal getal (double).", column, row[column].ToString());
			}
			else
				toReturn = value;

			return toReturn;
		}

		/// <summary>
		/// Get string value
		/// </summary>
		/// <param name="row">row to look in</param>
		/// <param name="column">column to get value from</param>
		/// <returns></returns>
		public static string GetString(this DataRow row, string column)
		{
			row.ValidateColumnExists(column);
			return row[column].ToString();			
		}

		public static void ValidateColumnExists(this DataRow row, string column)		
		{
			if (row.Table.Columns.IndexOf(column) < 0)
				throw new Dionysos.FunctionalException("Op de rij is geen kolom '{0}' aanwezig.", column);
		}
	}
}
