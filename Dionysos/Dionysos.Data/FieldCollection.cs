using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;

namespace Dionysos.Data
{
    /// <summary>
    /// Collection class used for storing Lynx_media.Data.Field instances in
    /// </summary>
    public class FieldCollection : ICollection<Dionysos.Data.Field>, IFieldCollection
    {
        #region Fields

        private ArrayList items;
        private string entityName = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the FieldCollection class
        /// </summary>	
        public FieldCollection()
        {
            this.items = new ArrayList();
        }

        /// <summary>
        /// Constructs an instance of the FieldCollection class using the specified entity name
        /// </summary>	
        public FieldCollection(string entityName)
        {
            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else
            {
                this.entityName = entityName;
                this.items = new ArrayList();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Lynx_media.Data.Field instance to the collection
        /// </summary>
        /// <param name="field">The Lynx_media.Data.Field instance to add to the collection</param>
        public void Add(Dionysos.Data.Field field)
        {
            this.items.Add(field);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Lynx_media.Data.Field instance is already in the collection
        /// </summary>
        /// <param name="field">The Lynx_media.Data.Field instance to check</param>
        /// <returns>True if the Lynx_media.Data.Field instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Data.Field field)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Data.Field) == field)
                {
                    contains = true;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Data.Field[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Lynx_media.Data.Field instance from this collection
        /// </summary>
        /// <param name="field">The Lynx_media.Data.Field instance to remove</param>
        public bool Remove(Dionysos.Data.Field field)
        {
            this.items.Remove(field);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.FieldCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Data.Field> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Data.Field>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.FieldCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Get a Lynx_media.Interfaces.IField instance from the collection using the specified name
        /// </summary>
        /// <param name="name">The name of the field to get from the collection</param>
        /// <returns>A Lynx_media.Interfaces.IField instance</returns>
        private IField GetField(string name)
        {
            IField field = null;

            // Walk through the Lynx_media.Data.Field instances of this collection
            for (int i = 0; i < this.Count; i++)
            {
                IField temp = this[i];
                if (temp.Name == name)
                {
                    field = temp;
                    break;
                }
            }

            return field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Interfaces.IField instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx_media.Interfaces.IField instance to get</param>
        /// <returns>An Lynx_media.Interfaces.IField instance</returns>
        public Dionysos.Interfaces.IField this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Interfaces.IField;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Interfaces.IField instance from the collection using the specified name
        /// </summary>
        /// <param name="name">The name of the Lynx_media.Interfaces.IField instance to get</param>
        /// <returns>A Lynx_media.Interfaces.IField instance</returns>
        public Dionysos.Interfaces.IField this[string name]
        {
            get
            {
                return this.GetField(name);
            }
        }

        /// <summary>
        /// Gets or sets the name of the entity
        /// </summary>
        public string EntityName
        {
            get
            {
                return this.entityName;
            }
            set
            {
                this.entityName = value;
            }
        }

        #endregion
    }
}