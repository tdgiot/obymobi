﻿using System;
using System.Collections.Generic;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Interfaces.Data;
using Dionysos.Data.LLBLGen;
using System.Diagnostics;
using Dionysos.Reflection;
using System.Linq;

namespace Dionysos.Data
{
    /// <summary>
    /// Utility class which can be used for DataGridView views
    /// </summary>
    public static class ViewUtil
    {
        /// <summary>
        /// Returns the custom view based on the the provided (default or custom) view.
        /// </summary>
        /// <param name="view">The IView instance to get or create a custom view for</param>
        /// <param name="userId">The id of the user to create the custom view for</param>
        /// <returns>An IView instance</returns>
        public static IView GetOrCreateCustomView(IView view, int userId)
        {
            if (view == null)
            {
                throw new ArgumentNullException(nameof(view));
            }

            if (view.IsCustomView)
            {
                return view;
            }

            IEntity viewEntity = view as IEntity;
            Trace.Assert(!Instance.Empty(viewEntity), "Parameter 'view' could not be casted to type 'IEntity'.");

            IEntity viewCustomEntity = DataFactory.EntityFactory.GetEntity("ViewCustom") as IEntity;
            Trace.Assert(!Instance.Empty(viewCustomEntity), "Entity of type 'ViewCustomEntity' could not be initialized.");

            IEntityCollection viewCustomCollection = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression("ViewCustom", new string[] { "EntityName", "UiElementTypeNameFull" }, new ComparisonOperator[] { ComparisonOperator.Equal, ComparisonOperator.Equal }, new object[] { viewEntity.Fields["EntityName"].CurrentValue, viewEntity.Fields["UiElementTypeNameFull"].CurrentValue });
            Trace.Assert(!Instance.Empty(viewCustomCollection), "Entity collection of type 'ViewCustomCollection' could not be initialized.");

            viewCustomEntity.SetNewFieldValue("Name", string.Format("Overzicht {0}", (viewCustomCollection.Count + 1)));
            viewCustomEntity.SetNewFieldValue("EntityName", viewEntity.Fields["EntityName"].CurrentValue);
            viewCustomEntity.SetNewFieldValue("UiElementTypeNameFull", viewEntity.Fields["UiElementTypeNameFull"].CurrentValue);
            viewCustomEntity.SetNewFieldValue("UserId", userId);
            viewCustomEntity.Save();

            viewCustomEntity.Refetch();

            IEntityCollection viewItemCollection = Dionysos.Reflection.Member.InvokeProperty(viewEntity, "ViewItemCollection") as IEntityCollection;
            Trace.Assert(!Instance.Empty(viewItemCollection), "Property 'ViewItemCollection' could be not retrieved properly.");

            for (int j = 0; j < viewItemCollection.Count; j++)
            {
                IEntity viewItem = viewItemCollection[j];
                IEntity viewItemCustomEntity = DataFactory.EntityFactory.GetEntity("ViewItemCustom") as IEntity;
                Trace.Assert(!Instance.Empty(viewItemCustomEntity), "Entity of type 'ViewItemCustom' could not be initialized.");
                viewItemCustomEntity.SetNewFieldValue("ViewCustomId", viewCustomEntity.Fields["ViewCustomId"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("EntityName", viewItem.Fields["EntityName"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("FieldName", viewItem.Fields["FieldName"].CurrentValue);
                //viewItemCustomEntity.SetNewFieldValue("FriendlyName", viewItem.Fields["FriendlyName"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("ShowOnGridView", viewItem.Fields["ShowOnGridView"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("DisplayPosition", viewItem.Fields["DisplayPosition"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("DisplayFormatString", viewItem.Fields["DisplayFormatString"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("SortPosition", viewItem.Fields["SortPosition"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("SortOperator", viewItem.Fields["SortOperator"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("Width", viewItem.Fields["Width"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("Type", viewItem.Fields["Type"].CurrentValue);
                viewItemCustomEntity.SetNewFieldValue("UiElementTypeNameFull", viewItem.Fields["UiElementTypeNameFull"].CurrentValue);
                viewItemCustomEntity.Save();
            }

            return viewCustomEntity as IView;
        }

        public static IView SaveCustomViewUpdates(IView view, int userId)
        {
            IEntity viewEntity = view as IEntity;
            if (viewEntity == null)
            {
                throw new InvalidProgramException("Unable to view updates, as it's not an entity");
            }

            if (!view.ViewItems.ContainsDirtyContents)
            {
                return view;
            }

            if (!view.IsCustomView)
            {
                return GetOrCreateCustomView(view, userId);
            }

            foreach (IEntity updatedItem in view.ViewItems.DirtyEntities)
            {
                updatedItem.Save();
            }

            return view;
        }

        public static IView GetView(string UIElementFullType, string entityName, int userId)
        {
            var customView = GetCustomView(UIElementFullType, entityName, userId);
            if (customView != null)
            {
                return customView;
            }

            IView defaultView = GetDefaultView(UIElementFullType, entityName);
            if (defaultView == null)
            {
                throw new TechnicalException("No default views found for UIElement '{0}' and entity name '{1}'. Check whether the view exists in the master & local database. " +
                    "Views are only generated if the EntityName is placed in the UIElement record!", UIElementFullType, entityName);
            }

            // TODO [GdV]: Not every default view seems to have items (like ApplicationConfiguration). 
            // Ensure all default views have items before enabling this check for each type of UIElement
            //if (defaultView.ViewItems.Count == 0)
            //{
            //    throw new TechnicalException("No defaultviewitems in default view found for UIElement '{0}' and entity name '{1}'. Check whether any of the columns has been marked as default display columns!",
            //        UIElementFullType, entityName);
            //}

            return defaultView;
        }

        /// <summary>
        /// Gets the first default view for the specified UIElementId, which may be null if it does not exist.
        /// </summary>
        /// <param name="UIElementFullType">The type of the UIElementEntity instance to get the default views for</param>
        /// <param name="entityName">The name of the entity to get the views for</param>
        /// <returns>The first default view, or null if it doesn't exist</returns>
        private static IView GetDefaultView(string UIElementFullType, string entityName)
        {
            IEntityCollection defaultViewEntities = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression("View", new string[] { "UiElementTypeNameFull", "EntityName" },
                new ComparisonOperator[] { ComparisonOperator.Equal, ComparisonOperator.Equal }, new object[] { UIElementFullType, entityName });

            return ConvertToViewCollection(defaultViewEntities).FirstOrDefault();
        }

        /// <summary>
        /// Gets the first custom view for the specified UIElementId and user id, which may be null if it does not exist.
        /// </summary>
        /// <param name="UIElementFullType">The type of the UIElementEntity instance to get the custom views for</param>
        /// <param name="userId">The id of the user to get the custom views for</param>
        /// <param name="entityName">The name of the entity to get the views for</param>
        /// <returns>The first custom view, or null if it doesn't exist</returns>
        private static IView GetCustomView(string UIElementFullType, string entityName, int userId)
        {
            IEntityCollection customViewEntities = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression("ViewCustom", new string[] { "UiElementTypeNameFull", "EntityName", "UserId" },
                new ComparisonOperator[] { ComparisonOperator.Equal, ComparisonOperator.Equal, ComparisonOperator.Equal }, new object[] { UIElementFullType, entityName, userId });
            return ConvertToViewCollection(customViewEntities).FirstOrDefault();
        }

        private static List<IView> ConvertToViewCollection(IEntityCollection entityCollection)
        {
            if (entityCollection == null)
            {
                throw new EmptyException("Variable 'viewCollection' is empty.");
            }

            List<IView> views = new List<IView>();
            foreach (IEntity viewEntity in entityCollection)
            {
                IView view = viewEntity as IView;
                if (view == null)
                {
                    throw new EmptyException("Variable 'view' is empty.");
                }
                else
                {
                    views.Add(view);
                }
            }

            return views;
        }

        /// <summary>
        /// Get the FriendlyName of a Related Field of a viewItem
        /// </summary>
        /// <param name="parentEntity"></param>
        /// <param name="viewItem"></param>
        /// <returns></returns>
        public static string GetFriendlyNameForRelatedField(this IViewItem viewItem)
        {
            string friendlyName = String.Empty;

            // Get field names
            string[] fieldNames = viewItem.FieldName.Split('.', StringSplitOptions.RemoveEmptyEntries);

            // Get instance and information for the parent entity
            IEntity entityInstance = DataFactory.EntityFactory.GetEntity(viewItem.EntityName) as IEntity;
            IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(entityInstance);

            for (int i = 0; i < fieldNames.Length - 1; i++)
            {
                // Update instance and information for the related entity
                entityInstance = Member.InvokeProperty(entityInstance, fieldNames[i]) as IEntity;
                IEntityInformation relatedEntityInformation = EntityInformationUtil.GetEntityInformation(entityInstance);
                IEntityRelationInformation relationInformation = entityInformation.RelationInformation[relatedEntityInformation.EntityName];
                entityInformation = relatedEntityInformation;

                // Add friendly name of related entity
                friendlyName = StringUtil.CombineWithSeperator(".", friendlyName, relationInformation.RelatedEntityFriendlyName);
            }

            // Add friendly name of field
            if (friendlyName.Length == 0 ||
                entityInformation.ShowFieldName != fieldNames[fieldNames.Length - 1])
            {
                // Only add last fieldname if no parent entities are found or the field is not the show field
                friendlyName = StringUtil.CombineWithSeperator(".", friendlyName, EntityInformationUtil.GetFieldNameFriendlyName(entityInformation.EntityName, fieldNames[fieldNames.Length - 1]));
            }

            return friendlyName;
        }
    }
}