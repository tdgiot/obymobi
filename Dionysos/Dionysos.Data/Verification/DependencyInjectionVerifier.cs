﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos;
using Dionysos.Reflection;
using Dionysos.Interfaces;
using Dionysos.Data.LLBLGen;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data
{
    /// <summary>
    /// Verifier class which verifies if dependency injection is enabled
    /// </summary>
    public class DependencyInjectionVerifier : IVerifier
    {
        #region Fields

        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Data.DependencyInjectionVerifier class
        /// </summary>
        public DependencyInjectionVerifier()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes the verifier and checks whether dependency injection is enabled
        /// </summary>
        /// <returns>True if verification was succesful, False if not</returns>
        public bool Verify()
        {
            bool succes = true;

            // Check dependency injection
            IEntity entity = DataFactory.EntityFactory.GetEntity(LLBLGenUtil.GetEntityName(LLBLGenUtil.GetEntityNames()[0])) as IEntity;
            if (entity != null && entity.AuthorizerToUse == null)
            {
                succes = false;
                errorMessage += "Dependency injection niet ingeschakeld - configureer dit in de web.config!";
            }            

            return succes;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the error message if verification failed
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
