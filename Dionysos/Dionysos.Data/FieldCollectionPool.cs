using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Dionysos.Reflection;
using Dionysos.Data.LLBLGen;

namespace Dionysos.Data
{
    /// <summary>
    /// Collection class used for storing Lynx_media.Data.FieldCollection instances in
    /// </summary>
    public class FieldCollectionPool : ICollection<Dionysos.Data.FieldCollection>, Dionysos.Interfaces.IFieldCollectionPool
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the FieldCollectionPool class
        /// </summary>	
        public FieldCollectionPool()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Lynx_media.Data.FieldCollection instance to the collection
        /// </summary>
        /// <param name="fieldCollection">The Lynx_media.Data.FieldCollection instance to add to the collection</param>
        public void Add(Dionysos.Data.FieldCollection fieldCollection)
        {
            this.items.Add(fieldCollection);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Lynx_media.Data.FieldCollection instance is already in the collection
        /// </summary>
        /// <param name="fieldCollection">The Lynx_media.Data.FieldCollection instance to check</param>
        /// <returns>True if the Lynx_media.Data.FieldCollection instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Data.FieldCollection fieldCollection)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Data.FieldCollection) == fieldCollection)
                {
                    contains = true;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Data.FieldCollection[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Lynx_media.Data.FieldCollection instance from this collection
        /// </summary>
        /// <param name="fieldCollection">The Lynx_media.Data.FieldCollection instance to remove</param>
        public bool Remove(Dionysos.Data.FieldCollection fieldCollection)
        {
            this.items.Remove(fieldCollection);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.FieldCollectionPool
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Data.FieldCollection> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Data.FieldCollection>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.FieldCollectionPool
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets a Lynx_media.Interfaces.IFieldCollection instance from the collection using the specified entity name
        /// </summary>
        /// <param name="entityName">The entity name of the fieldcollection to get from the collection</param>
        /// <returns>A Lynx_media.Interfaces.IFieldCollection instance</returns>
        private Dionysos.Interfaces.IFieldCollection GetFieldCollection(string entityName)
        {
            Dionysos.Interfaces.IFieldCollection fieldCollection = null;

            // Walk through the Lynx_media.Data.FieldCollection instances of this collection
            for (int i = 0; i < this.Count; i++)
            {
                Dionysos.Interfaces.IFieldCollection temp = this[i];
                if (temp.EntityName == entityName)
                {
                    fieldCollection = temp;
                    break;
                }
            }

            if (fieldCollection == null)
            {
                // Create and initialize a FieldCollection instance
                // according to the entity name
                FieldCollection fieldCollection2 = new FieldCollection(entityName);

                // Create and initialize a LLBLGenEntityUtil instance
                // and get the fields for the specified entity name
                LLBLGenEntityUtil entityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();
                string[] fields = entityUtil.GetFields(entityName);

                // Invoke an object based on the entity name
                // in order to retrieve the types of the datacolumns
                object entity = DataFactory.EntityFactory.GetEntity(entityName);
                if (Instance.Empty(entity))
                {
                    throw new EmptyException("Variable 'entity' is empty.");
                }
                else
                {
                    for (int i = 0; i < fields.Length; i++)
                    {
                        // Create and initialize a Field instance
                        // and set the properties
                        Field field = new Field();
                        field.Name = fields[i];
                        field.DataType = Member.GetPropertyType(entity, fields[i]);

                        // Add the field instance to field collection instance
                        fieldCollection2.Add(field);
                    }
                }

                // Finally, add the field collection instance to the field collection pool
                this.Add(fieldCollection2);
                fieldCollection = fieldCollection2;
            }

            return fieldCollection;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Interfaces.IFieldCollection instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx_media.Interfaces.IFieldCollection instance to get</param>
        /// <returns>An Lynx_media.Interfaces.IFieldCollection instance</returns>
        public Dionysos.Interfaces.IFieldCollection this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Data.FieldCollection;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Interfaces.IFieldCollection instance from the collection using the specified entity name
        /// </summary>
        /// <param name="entityName">The entity name of the Lynx_media.Interfaces.IFieldCollection instance to get</param>
        /// <returns>An Lynx_media.Interfaces.IFieldCollection instance</returns>
        public Dionysos.Interfaces.IFieldCollection this[string entityName]
        {
            get
            {
                return this.GetFieldCollection(entityName);
            }
        }

        #endregion
    }
}