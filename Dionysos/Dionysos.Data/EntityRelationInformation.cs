﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Interfaces.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Data
{
    /// <summary>
    /// Class which represents information about an entity relation
    /// </summary>
    public class EntityRelationInformation : IEntityRelationInformation
    {
        #region Fields

        private string relatedEntityName = string.Empty;
        private string relatedEntityFriendlyName = string.Empty;
        private RelationType relationType;
        private string mappedFieldName = string.Empty;
		private string foreignKeyFieldName = string.Empty;
		private string primaryKeyFieldName = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Data.EntityRelationInformation tpye
        /// </summary>
        public EntityRelationInformation()
        {
        }

        #endregion

        #region Properties

		/// <summary>
		/// Gets or sets the primaryKeyFieldName
		/// </summary>
		public string PrimaryKeyFieldName
		{
			get
			{
				return this.primaryKeyFieldName;
			}
			set
			{
				this.primaryKeyFieldName = value;
			}
		}



		/// <summary>
		/// Gets or sets the foreignKeyFieldName
		/// </summary>
		public string ForeignKeyFieldName
		{
			get
			{
				return this.foreignKeyFieldName;
			}
			set
			{
				this.foreignKeyFieldName = value;
			}
		}


        /// <summary>
        /// Gets or sets the entity name of related entity
        /// </summary>
        public string RelatedEntityName
        {
            get
            {
                return this.relatedEntityName;
            }
            set
            {
                this.relatedEntityName = value;
            }
        }

        /// <summary>
        /// Gets or sets the entity name of related entity
        /// </summary>
        public string RelatedEntityFriendlyName
        {
            get
            {
                return this.relatedEntityFriendlyName;
            }
            set
            {
                this.relatedEntityFriendlyName = value;
            }
        }

        /// <summary>
        /// Gets or sets the relation type
        /// </summary>
        public RelationType RelationType
        {
            get
            {
                return this.relationType;
            }
            set
            {
                this.relationType = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field the relation is mapped to
        /// </summary>
        public string MappedFieldName
        {
            get
            {
                return this.mappedFieldName;
            }
            set
            {
                this.mappedFieldName = value;
            }
        }

        #endregion
    }
}
