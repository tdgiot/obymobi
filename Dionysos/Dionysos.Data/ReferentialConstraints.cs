﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data
{
    /// <summary>
    /// Collection class used for storing Dionysos.Data.ReferentialConstraint instances in
    /// </summary>
    public class ReferentialConstraintCollection : ICollection<Dionysos.Data.ReferentialConstraint>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ReferentialConstraintCollection class
        /// </summary>	
        public ReferentialConstraintCollection()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Dionysos.Data.ReferentialConstraint instance to the collection
        /// </summary>
        /// <param name="referentialConstraint">The Dionysos.Data.ReferentialConstraint instance to add to the collection</param>
        public void Add(Dionysos.Data.ReferentialConstraint referentialConstraint)
        {
            this.items.Add(referentialConstraint);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Dionysos.Data.ReferentialConstraint instance is already in the collection
        /// </summary>
        /// <param name="referentialConstraint">The Dionysos.Data.ReferentialConstraint instance to check</param>
        /// <returns>True if the Dionysos.Data.ReferentialConstraint instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Data.ReferentialConstraint referentialConstraint)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Data.ReferentialConstraint) == referentialConstraint)
                {
                    contains = true;
                    break;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Data.ReferentialConstraint[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Dionysos.Data.ReferentialConstraint instance from this collection
        /// </summary>
        /// <param name="referentialConstraint">The Dionysos.Data.ReferentialConstraint instance to remove</param>
        public bool Remove(Dionysos.Data.ReferentialConstraint referentialConstraint)
        {
            this.items.Remove(referentialConstraint);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.ReferentialConstraintCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Data.ReferentialConstraint> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Data.ReferentialConstraint>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.ReferentialConstraintCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private ReferentialConstraint GetReferentialConstraint(string fieldName)
        {
            ReferentialConstraint referentialConstraint = null;

            for (int i = 0; i < this.Count; i++)
            {
                ReferentialConstraint temp = this[i];
                if (temp.FieldName == fieldName)
                {
                    referentialConstraint = temp;
                    break;
                }
            }

            if (referentialConstraint == null)
            {
                referentialConstraint = new ReferentialConstraint(fieldName, ReferentialConstraintDeleteRule.NoAction, ReferentialConstraintArchiveRule.NoAction);
                this.Add(referentialConstraint);
            }

            return referentialConstraint;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Dionysos.Data.ReferentialConstraint instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Dionysos.Data.ReferentialConstraint instance to get</param>
        /// <returns>An Dionysos.Data.ReferentialConstraint instance</returns>
        public Dionysos.Data.ReferentialConstraint this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Data.ReferentialConstraint;
            }
        }

        /// <summary>
        /// Gets an Dionysos.Data.ReferentialConstraint instance from the collection from the specified index
        /// </summary>
        /// <param name="fieldName">The name of the Dionysos.Data.ReferentialConstraint instance to get</param>
        /// <returns>An Dionysos.Data.ReferentialConstraint instance</returns>
        public Dionysos.Data.ReferentialConstraint this[string fieldName]
        {
            get
            {
                return this.GetReferentialConstraint(fieldName);
            }
        }

        #endregion
    }
}