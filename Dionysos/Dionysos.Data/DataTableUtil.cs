using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Dionysos.Data
{
    /// <summary>
    /// Utility Class with Utils for a DataTable
    /// </summary>
    public static class DataTableUtil
    {
        /// <summary>
        /// Get the row that matches the filterString, throws an exception with more than one match
        /// </summary>
        /// <param name="filterString"></param>
        /// <returns></returns>
        public static DataRow GetRow(this DataTable dt, string filterString)
        {
            DataRow toReturn;

            DataRow[] dataRows = dt.Select(filterString);
            if (dataRows.Length > 1)
                throw new FunctionalException("Multiple rows match '{0}'", filterString);
            else if (dataRows.Length == 1)
                toReturn = dataRows[0];
            else
                toReturn = null;

            return toReturn;
        }

        /// <summary>
        /// Filter a DataTable and get a DataTable returned
        /// </summary>
        /// <param name="dt">DataTable to do filtering in</param>
        /// <param name="filterString">Filter string (as used in DataTable.Select())</param>
        /// <returns>A new DataTable containing records which comply with the Filter</returns>
        public static DataTable FilterTable(this DataTable dt, string filterString)
        {
            DataRow[] filteredRows = dt.Select(filterString);
            DataTable filteredDt = dt.Clone();

            DataRow dr;
            foreach (DataRow oldDr in filteredRows)
            {
                dr = filteredDt.NewRow();
                for (int i = 0; i < dt.Columns.Count; i++)
                    dr[dt.Columns[i].ColumnName] = oldDr[dt.Columns[i].ColumnName];
                filteredDt.Rows.Add(dr);

            }

            return filteredDt;
        }

        /// <summary>
        /// Fill a DataTable with the selection from oDataSet using sFilterExpression
        /// </summary>
        /// <param name="dataTable">The source datatable</param>
        /// <param name="filterExpression">The filter expression to use</param>
        /// <param name="sortExpression">Extra sort information</param>
        public static DataTable GetTableFromSelection(DataTable dataTable, string filterExpression, string sortExpression)
        {
            // Array of datarows to store the results
            DataRow[] dataRowResults;
            DataTable tableCopy;

            // Get a clon eof the current table
            tableCopy = dataTable.Clone();
            // Clear the current rows
            tableCopy.Rows.Clear();

            // get the rows using the filter etc
            dataRowResults = dataTable.Select(filterExpression, sortExpression, DataViewRowState.CurrentRows);

            // import the results into the table
            foreach (DataRow currentRow in dataRowResults)
            {
                tableCopy.ImportRow(currentRow);
            }

            return tableCopy;
        }

        /// <summary>
        /// Clones the DataTable including the data in the Rows
        /// </summary>
        /// <param name="dt">DataTable to do filtering in</param>
        /// <returns>A new DataTable containing records which comply with the Filter</returns>
        public static DataTable CloneDataTable(DataTable dt)
        {
            return DataTableUtil.FilterTable(dt, string.Empty);
        }

        /// <summary>
        /// Returns a new instance of the datatable sorted by the supplied sortExpression
        /// </summary>
        /// <param name="dt">DataTable to do sorting on</param>
        /// <param name="sortExpression">Sort Expression</param>
        /// <returns>A new DataTable containing records in order of the sorting</returns>
        public static DataTable Sort(DataTable dt, string sortExpression)
        {
            DataTable sortedDt = dt.Clone();

            dt.DefaultView.Sort = sortExpression;
            DataRow drOrg, drTarget;
            for (int i = 0; i < dt.DefaultView.Count; i++)
            {
                drOrg = dt.DefaultView[i].Row;
                drTarget = sortedDt.NewRow();
                for (int j = 0; j < dt.Columns.Count; j++)
                    drTarget[dt.Columns[j].ColumnName] = drOrg[dt.Columns[j].ColumnName];
                sortedDt.Rows.Add(drTarget);
            }

            return sortedDt;
        }

        /// <summary>
        /// Appends the value to the row.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="name">The column name.</param>
        /// <param name="value">The value.</param>
        public static void AppendValueToRow(this DataRow row, string name, object value)
        {
            // Add column if it does not yet exists
            if (!row.Table.Columns.Contains(name))
            {
                row.Table.Columns.Add(name, typeof(string));                                    
            }

            // Add value
            if (value == null)
            {
                row[name] = String.Empty;
            }
            else
            {
                row[name] = value.ToString();
            }
        }

        /// <summary>
        /// Ensures the column exists.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="name">The column name.</param>
        public static void EnsureColumn(this DataRow row, string name)
        {
            // Add column with empty value if it does not yet exists
            if (!row.Table.Columns.Contains(name))
            {
                row.AppendValueToRow(name, null);
            }
        }

        /// <summary>
        /// Create a CSV file based on the DataTable (with headers and ';' as separator)
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <returns></returns>
        public static StringBuilder ToCsv(this DataTable dt)
        {
            return dt.ToCsv(true, ";", '"');
        }

        /// <summary>
        /// Create a CSV file based on the DataTable
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="renderHeaders">Render headers as first row</param>
        /// <param name="seperator">Seperator to use</param>
        /// <param name="quoteCharacter">Charachter to use to surround column values</param>
        /// <returns></returns>
        public static StringBuilder ToCsv(this DataTable dt, bool renderHeaders, string seperator, char quoteCharacter)
        {
            StringBuilder output = new StringBuilder();

            string headingsRow = "";
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                // Skip CssClass
                if (dt.Columns[i].ColumnName == "CssClass")
                    continue;

                if (i > 0)
                    headingsRow = headingsRow + seperator + dt.Columns[i].ColumnName;
                else
                    headingsRow = dt.Columns[i].ColumnName;
            }

            output.AppendLine(headingsRow);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string valuesRow = "";
                DataRow r = dt.Rows[i];
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    // Skip CssClass
                    if (dt.Columns[j].ColumnName == "CssClass")
                        continue;

                    string value = r[j].ToString();
                    if (value.Contains("\n") || value.Contains("\r") || value.Contains(seperator) || value.Contains(quoteCharacter.ToString()))
                    {
                        // Esacpe quoteCharacters in value by adding a quoteCharacter in front of them
                        if (value.Contains(quoteCharacter.ToString()))
                            value = value.Replace(quoteCharacter.ToString(), new string(quoteCharacter, 2));

                        // Surround value with quoteCharacater
                        value = string.Format("{0}{1}{0}", quoteCharacter, value);
                    }

                    if (j > 0)
                        valuesRow = valuesRow + seperator + value;
                    else
                        valuesRow = value;
                }

                valuesRow = valuesRow.Replace("\r", string.Empty);
                valuesRow = valuesRow.Replace("\n", string.Empty);

                output.AppendLine(valuesRow);
            }

            return output;
        }

        /// <summary>
        /// Convert the DataTable to a Simple HtmlTable
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="tableCssClass">Class for the Table tag</param>
        /// <param name="firstRowCssClass">Class for the first Tr tag</param>
        /// <param name="firstColumnCssClass">Class for the first Td classes in the rows, except for in the header row</param>
        /// <returns></returns>
        public static string ToHtmlTable(this DataTable dt, string tableCssClass, string firstRowCssClass, string firstColumnCssClass)
        {
            return DataTableUtil.ToHtmlTable(dt, tableCssClass, firstRowCssClass, firstColumnCssClass, true, string.Empty, string.Empty);
        }

        /// <summary>
        /// Convert the DataTable to a Simple HtmlTable
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="tableCssClass">Class for the Table tag</param>
        /// <param name="firstRowCssClass">Class for the first Tr tag</param>
        /// <param name="firstColumnCssClass">Class for the first Td classes in the rows, except for in the header row</param>
        /// <param name="renderTableHeader">Render TH cells in first row</param>
        /// <param name="sortingId">SortingId, should be unique per page</param>
        /// <param name="defaultSortDirection">Default sort direction</param>
        /// <returns></returns>
        public static string ToHtmlTable(this DataTable dt, string tableCssClass, string firstRowCssClass, string firstColumnCssClass, bool renderTableHeader, string sortingId, string defaultSortDirection)
        {
            StringBuilder html = new StringBuilder();

            string sortField = dt.Columns[0].ColumnName;
            string sortDirection = defaultSortDirection;
            string sortFieldQsParameter = "sortf-" + sortingId;
            string sortDirectionQsParameter = "sortd-" + sortingId;
            string cleanUrlToAddSortingQueryString = string.Empty;

            // No sorting when there's no HttpContext
            bool useSorting = (HttpContext.Current != null && sortingId.Length > 0);

            if (useSorting)
            {
                if (HttpContext.Current.Request.QueryString[sortFieldQsParameter] != null)
                    sortField = HttpContext.Current.Request.QueryString["sortf-" + sortingId];

                if (HttpContext.Current.Request.QueryString[sortDirectionQsParameter] != null)
                    sortDirection = HttpContext.Current.Request.QueryString["sortd-" + sortingId];

                NameValueCollection queryParameters = HttpUtility.ParseQueryString(HttpContext.Current.Request.Url.Query);
                cleanUrlToAddSortingQueryString = "?";
                foreach (var key in queryParameters.AllKeys)
                {
                    if (key != sortFieldQsParameter && key != sortDirectionQsParameter)
                    {
                        if (cleanUrlToAddSortingQueryString.Length > 1)
                            cleanUrlToAddSortingQueryString += "&";

                        cleanUrlToAddSortingQueryString += string.Format("{0}={1}", key, queryParameters.GetValues(key)[0]);
                    }
                }
            }


            // Table Opening Tag
            html.AppendFormat("<table cellpadding=\"0\" cellspacing=\"0\" class=\"{0}\">", tableCssClass);

            // Render Headers
            html.AppendFormat("<tr class=\"{0}\">", firstRowCssClass);
            foreach (DataColumn column in dt.Columns)
            {
                // Skip CssClass
                if (column.ColumnName == "CssClass" || column.ColumnName.Contains(".|."))
                    continue;

                string captionText = column.Caption.Length > 0 ? column.Caption : column.ColumnName;

                if (!useSorting)
                    html.AppendFormat("<th>{0}</th>", captionText);
                else
                {
                    string sortDirectionToLink = "ASC";

                    string hrefCssClass = string.Empty;
                    if (sortField == column.ColumnName && sortDirection == "ASC")
                    {
                        sortDirectionToLink = "DESC";
                        hrefCssClass = "asc";
                    }
                    else if (sortField == column.ColumnName)
                        hrefCssClass = "desc";


                    string queryString = cleanUrlToAddSortingQueryString;
                    queryString += string.Format("&{0}={1}&{2}={3}",
                        sortFieldQsParameter, column.ColumnName, sortDirectionQsParameter, sortDirectionToLink);

                    string url = HttpContext.Current.Request.Url.AbsolutePath + queryString;

                    html.AppendFormat("<th><a href=\"{0}\" class=\"{1}\">{2}</a></th>", url, hrefCssClass, captionText);
                }
            }
            html.AppendFormat("</tr>");

            // If we sort, first sort the datatable
            if (useSorting)
            {
                dt.DefaultView.Sort = string.Format("{0} {1}", sortField, sortDirection);
                dt = dt.DefaultView.ToTable();
            }

            // Render Rows
            bool even = true;
            foreach (DataRow row in dt.Rows)
            {
                string rowCssClass = "";
                if (dt.Columns.IndexOf("CssClass") >= 0 && row["CssClass"] != null && row["CssClass"].ToString().Length > 0)
                    rowCssClass = row["CssClass"].ToString();

                rowCssClass += even ? "even" : "odd";
                even = !even;

                html.AppendFormat("<tr class=\"{0}\">", rowCssClass);


                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    // Skip CssClass
                    DataColumn column = dt.Columns[i];
                    if (column.ColumnName == "CssClass" || column.ColumnName.Contains(".|."))
                        continue;

                    // Check for a cell-level cssclass
                    string cssClassColumn = column.ColumnName + ".|." + "CssClass";
                    string cssCellClass = string.Empty;
                    if (dt.Columns.Contains(cssClassColumn))
                        cssCellClass = row[cssClassColumn].ToString();

                    // Check for a cell-level href
                    string hrefColumn = column.ColumnName + ".|." + "Href";
                    string cellValue = row[i].ToString();
                    if (dt.Columns.Contains(hrefColumn) && !row[hrefColumn].ToString().IsNullOrWhiteSpace())
                        cellValue = string.Format("<a href=\"{0}\">{1}</a>", row[hrefColumn].ToString(), row[i].ToString());

                    if (i == 0)
                        html.AppendFormat("<td class=\"{0} {1}\">{2}</td>", firstColumnCssClass, cssCellClass, cellValue);
                    else
                        html.AppendFormat("<td class=\"{0}\">{1}</td>", cssCellClass, cellValue);
                }
                html.AppendFormat("</tr>");
            }

            // Table Closing Tag
            html.AppendFormat("</table>");

            return html.ToString();
        }

        /// <summary>
        /// Synchronize the Columns between all the DataTables
        /// </summary>
        /// <param name="dataTables">DataTables</param>
        public static void SynchronizeDataColumns(KeyValuePair<int, DataTable>[] dataTables)
        {
            DataTable[] dataTableArray = new DataTable[dataTables.Length];
            for (int i = 0; i < dataTables.Length; i++)
            {
                dataTableArray[i] = dataTables[i].Value;
            }

            DataTableUtil.SynchronizeDataColumns(dataTableArray);
        }

        /// <summary>
        /// Synchronize the Columns between all the DataTables
        /// </summary>
        /// <param name="dataTables">DataTables</param>
        public static void SynchronizeDataColumns(DataTable[] dataTables)
        {
            List<string> columnNames = new List<string>();

            // Gather all ColumnNames
            foreach (var dataTable in dataTables)
                foreach (DataColumn column in dataTable.Columns)
                    if (!columnNames.Contains(column.ColumnName))
                        columnNames.Add(column.ColumnName);

            // Add Missing columns to each datatable
            foreach (var dataTable in dataTables)
                foreach (var columnName in columnNames)
                    if (!dataTable.Columns.Contains(columnName))
                        dataTable.Columns.Add(columnName);
        }

        public static DataTable Transpose(this DataTable original)
        {
            DataTable toReturn = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            toReturn.Columns.Add(original.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in original.Rows)
            {
                string newColName = inRow[0].ToString();
                toReturn.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rCount = 1; rCount <= original.Columns.Count - 1; rCount++)
            {
                DataRow newRow = toReturn.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = original.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= original.Rows.Count - 1; cCount++)
                {
                    string colValue = original.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                toReturn.Rows.Add(newRow);
            }

            return toReturn;
        }
    }
}