﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Data.Csv
{
    /// <summary>
    /// Struct containing information for a fieldmapping
    /// </summary>
    public class FieldMapping
    {

        #region Fields

        private string sourceFieldname = null;
        private string targetFieldname = null;
        private string targetFieldDisplayName = null;
        private bool required = false;

        #endregion

        #region Methods

        /// <summary>
        /// Create an instance of fieldmapping
        /// </summary>
        /// <param name="targetFieldname">Target fieldname</param>
        /// <param name="required">Indicate if the field is required</param>
        public FieldMapping(string targetFieldname, bool required)
            : this(targetFieldname, string.Empty, string.Empty, required)
        {
        }

        /// <summary>
        /// Create an instance of fieldmapping
        /// </summary>
        /// <param name="targetFieldname">Target fieldname</param>
        /// <param name="sourceFieldname">Source fieldname</param>
        /// <param name="targetFieldDisplayName">Target fieldname Displayname</param>
        /// <param name="required">Indicate if the field is required</param>
        public FieldMapping(string targetFieldname, string sourceFieldname, string targetFieldDisplayName, bool required)
        {
            this.targetFieldDisplayName = targetFieldDisplayName;
            this.sourceFieldname = sourceFieldname;
            this.targetFieldname = targetFieldname;
            this.required = required;
        }

        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the targetFieldDisplayName
        /// </summary>
        public string TargetFieldDisplayName
        {
            get
            {
                if (this.targetFieldDisplayName.Length > 0)
                {
                    return this.targetFieldDisplayName;
                }
                else
                {
                    return this.targetFieldname;
                }
            }
            set
            {
                this.targetFieldDisplayName = value;
            }
        }

        /// <summary>
        /// Gets or sets the targetFieldname
        /// </summary>
        public string TargetFieldname
        {
            get
            {
                return this.targetFieldname;
            }
            set
            {
                this.targetFieldname = value;
            }
        }

        /// <summary>
        /// Gets or sets the sourceFieldname
        /// </summary>
        public string SourceFieldname
        {
            get
            {
                return this.sourceFieldname;
            }
            set
            {
                this.sourceFieldname = value;
            }
        }

        /// <summary>
        /// Gets or sets the required
        /// </summary>
        public bool Required
        {
            get
            {
                return this.required;
            }
            set
            {
                this.required = value;
            }
        }

        #endregion

    }

}
