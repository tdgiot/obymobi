﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Web;

namespace Dionysos.Payment
{
	public class PaymentManager
	{
		#region Fields

		static PaymentManager instance = null;

		private IPaymentProvider paymentProvider = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the PaymentManager class if the instance has not been initialized yet
		/// </summary>
		static PaymentManager()
		{
			if (instance == null)
			{
				// first create the instance
				instance = new PaymentManager();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the static PaymentManager instance
		/// </summary>
		private void Init()
		{
		}

		public static IPaymentResponse ExecutePaymentRequest(string partnerId, int amount, string bankId, string description, string reportUrl, string returnUrl)
		{
			return PaymentManager.PaymentProvider.ExecutePaymentRequest(partnerId, amount, bankId, description, reportUrl, returnUrl);
		}

		public static IPaymentCheckResponse ExecuteCheckPaymentRequest(string partnerId, string transactionId)
		{
			return PaymentManager.PaymentProvider.ExecutePaymentCheckRequest(partnerId, transactionId);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the payment provider instance
		/// </summary>
		public static IPaymentProvider PaymentProvider
		{
			get
			{
				if (Instance.Empty(instance.paymentProvider))
				{
					throw new PaymentProviderNotSetException("Payment provider not set in Global.asax file.");
				}
				return instance.paymentProvider;
			}
			set
			{
				instance.paymentProvider = value;
			}
		}

		/// <summary>
		/// Gets the collection of banks used for iDEAL payment
		/// </summary>
		public static BankCollection Banks
		{
			get
			{
				return PaymentManager.PaymentProvider.Banks;
			}
		}

		#endregion
	}
}