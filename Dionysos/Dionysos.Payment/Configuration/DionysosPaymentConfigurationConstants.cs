﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment
{
    /// <summary>
    /// Configuration class which contains constants for the names of configuration items
    /// </summary>
    public class DionysosPaymentConfigurationConstants
    {
        #region Fields

        /// <summary>
        /// Account number for payment processing
        /// </summary>
        public const string AccountNumber = "PaymentAccountNr";
        public const string PaymentText = "PaymentText";
        public const string PaymentTestMode = "PaymentTestMode";

        #endregion
    }
}
