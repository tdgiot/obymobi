﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;

namespace Dionysos.Payment
{
    /// <summary>
    /// Configuration information class for Dionysos.Payment
    /// </summary>
    public class DionysosPaymentConfigurationInfo : ConfigurationItemCollection
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.DionysosPaymentConfigurationInfo type
        /// </summary>
        public DionysosPaymentConfigurationInfo()
        {
            string sectionName = "Dionysos.Payment";
            this.Add(new ConfigurationItem(sectionName, DionysosPaymentConfigurationConstants.AccountNumber, "Account nummer voor online betalingen", string.Empty, typeof(string)));
            this.Add(new ConfigurationItem(sectionName, DionysosPaymentConfigurationConstants.PaymentText, "Omschrijving bij betalingen", "Betaling webwinkel nr", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, DionysosPaymentConfigurationConstants.PaymentTestMode, "Testen van betalingen via iDEAL", false, typeof(bool)));
        }

        #endregion
    }
}
