﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment
{
    /// <summary>
    /// Enumerator class which represents the status of a payment
    /// </summary>    
    public enum PaymentStatus
    {
        /// <summary>
        /// Payment has an unknown status 
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The payment has been paid successfully
        /// </summary>
        Paid = 1,

        /// <summary>
        /// The payment failed
        /// </summary>
        Failed = 2
    }
}
