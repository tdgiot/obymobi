﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment
{
    public class Bank
    {
        #region Fields

        private string name = string.Empty;
        private string id = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.Bank type
        /// </summary>
        public Bank()
        {
        }

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.Bank type using the specified name
        /// </summary>
        /// <param name="name">The name of the bank</param>
        public Bank(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.Bank type using the specified name and id
        /// </summary>
        /// <param name="name">The name of the bank</param>
        /// <param name="id">The id of the bank</param>
        public Bank(string name, string id)
        {
            this.name = name;
            this.id = id;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the bank
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the id of the bank
        /// </summary>
        public string Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        #endregion
    }
}
