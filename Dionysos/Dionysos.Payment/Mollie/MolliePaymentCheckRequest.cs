﻿using System;
using System.Collections.Generic;
using System.Text;

using Dionysos.Payment;

namespace Dionysos.Payment.Mollie
{
    /// <summary>
    /// Class which represents a check on payments
    /// </summary>
    public class MolliePaymentCheckRequest : IPaymentCheckRequest
    {
        #region Fields

        private string partnerId = string.Empty;
        private string transactionId = string.Empty;
        private bool testMode = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.Mollie.MolliePaymentCheckRequest type using the specified parameters
        /// </summary>
        /// <param name="partnerId">The Mollie.nl account number</param>
        /// <param name="transactionId">The id of the transaction</param>
        public MolliePaymentCheckRequest(string partnerId, string transactionId)
        {
            if (Instance.ArgumentIsEmpty(partnerId, "partnerId"))
			{
				// Parameter 'partnerId' is empty
			}
            else if (Instance.ArgumentIsEmpty(transactionId, "transactionId"))
            {
                // Parameter 'transactionId' is empty
            }
            else
            {
                this.partnerId = partnerId;
                this.transactionId = transactionId;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a string representation for this Mollie payment check request to use in the querystring
        /// </summary>
        /// <returns>A System.String instance representing this payment check request</returns>
        public override string ToString()
        {
            string request = string.Empty;

            if (Instance.Empty(this.partnerId))
            {
                throw new EmptyException("Variable 'partnerId' is empty!");
            }
            else
            {
                request += string.Format("partnerid={0}", this.partnerId);
            }

            request += "&";

            if (Instance.Empty(this.transactionId))
            {
                throw new EmptyException("Variable 'transactionId' is empty!");
            }
            else
            {
                request += string.Format("transaction_id={0}", this.transactionId);
            }

            if (this.testMode)
            {
                request += "&testmode=true";
            }

            return request;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a flag which indicates whether the testmode is enabled
        /// </summary>
        public bool TestMode
        {
            get
            {
                return this.testMode;
            }
            set
            {
                this.testMode = value;
            }
        }

        #endregion
    }
}
