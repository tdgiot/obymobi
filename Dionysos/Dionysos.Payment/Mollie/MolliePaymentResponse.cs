﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment.Mollie
{
    /// <summary>
    /// Class which represents a reponse to a payment request
    /// </summary>
    public class MolliePaymentResponse : IPaymentResponse
    {
        #region Fields

        private string errorcode = string.Empty;
        private string transactionId = string.Empty;
        private int amount = 0;
        private string currency = string.Empty;
        private string url = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.Mollie.MolliePaymentReponse typ
        /// </summary>
        public MolliePaymentResponse()
        {
        }

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.Mollie.MolliePaymentReponse type using the specified parameters
        /// </summary>
        /// <param name="errorcode">The optional errorcode</param>
        /// <param name="transactionId">The id of the transaction</param>
        /// <param name="amount">The payment amount IN CENTS!!!</param>
        /// <param name="currency">The currency of the payment</param>
        /// <param name="url">The url to return to after processing the payment</param>
        public MolliePaymentResponse(string errorcode, string transactionId, int amount, string currency, string url)
        {
            this.errorcode = errorcode;
            this.transactionId = transactionId;
            this.amount = amount;
            this.currency = currency;
            this.url = url;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the PropertyName
        /// </summary>
        public string ErrorCode
        {
            get
            {
                return this.errorcode;
            }
            set
            {
                this.errorcode = value;
            }
        }

        /// <summary>
        /// Gets or sets the the id of the transaction
        /// </summary>
        public string TransactionId
        {
            get
            {
                return this.transactionId;
            }
            set
            {
                this.transactionId = value;
            }
        }

        /// <summary>
        /// Gets or sets the payment amount IN CENTS!!!
        /// </summary>
        public int Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.amount = value;
            }
        }

        /// <summary>
        /// Gets or sets the PropertyName
        /// </summary>
        public string Currency
        {
            get
            {
                return this.currency;
            }
            set
            {
                this.currency = value;
            }
        }

        /// <summary>
        /// Gets or sets the url to return to after processing the payment
        /// </summary>
        public string Url
        {
            get
            {
                return this.url;
            }
            set
            {
                this.url = value;
            }
        }

        #endregion
    }
}
