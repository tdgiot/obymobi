﻿using System;
using System.Web;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Dionysos;
using Dionysos.Net;
using Dionysos.Web;

namespace Dionysos.Payment.Mollie
{
    public class MolliePaymentProvider : IPaymentProvider
    {
        #region Fields

        private BankCollection banks = null;
        private const string address = "http://www.mollie.nl/xml/ideal?a=";

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.MolliePaymentProvider class
        /// </summary>
        public MolliePaymentProvider()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves a banklist using a http request and initialize the bank collection
        /// </summary>
        private void GetBanks()
        {
            string url = address + "banklist";

            if (ConfigurationManager.GetBool(DionysosPaymentConfigurationConstants.PaymentTestMode))
                url += "&testmode=true";

            DataSet dataSet = HttpRequestHelper.GetResponseAsDataSet(url);
            if (Instance.Empty(dataSet))
            {
                throw new EmptyException("Variable 'dataSet' is empty!");
            }
            else
            {
                if (dataSet.Tables.Count > 1)
                {
                    // Initialize the BankCollection instance
                    this.banks = new BankCollection();

                    // And walk through the dataset in order to
                    // fill the BankCollection instance
                    for (int i = 0; i < dataSet.Tables[1].Rows.Count; i++)
                    {
                        DataRow row = dataSet.Tables[1].Rows[i];
                        if (Instance.Empty(row))
                        {
                            throw new EmptyException("Variable 'row' is empty!");
                        }
                        else
                        {
                            this.banks.Add(new Bank(row.ItemArray.GetValue(1) as string, row.ItemArray.GetValue(0) as string));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Executes a payment request and returns a payment response
        /// </summary>
        /// <param name="partnerId">The Mollie.nl account number</param>
        /// <param name="amount">The payment amount IN CENTS!!!</param>
        /// <param name="bankId">The id of the bank</param>
        /// <param name="description">The description for the payment (max length 29 characters)</param>
        /// <param name="reportUrl">The url for the extra check</param>
        /// <param name="returnUrl">The url to return to after processing the payment</param>
        /// <returns>An Dionysos.Payment.IPaymentResponse instance if the request was successful, Null if not</returns>
        public IPaymentResponse ExecutePaymentRequest(string partnerId, int amount, string bankId, string description, string reportUrl, string returnUrl)
        {
            IPaymentResponse response = null;

            MolliePaymentRequest paymentRequest = new MolliePaymentRequest(partnerId, amount, bankId, description, reportUrl, returnUrl);
            paymentRequest.TestMode = ConfigurationManager.GetBool(DionysosPaymentConfigurationConstants.PaymentTestMode);

            if (Instance.ArgumentIsEmpty(paymentRequest, "paymentRequest"))
            {
                // Parameter 'paymentRequest' is empty
            }
            else
            {
                string fullAddress = address + "fetch&" + paymentRequest.ToString();

                XmlTextReader textReader = HttpRequestHelper.GetResponseAsXmlReader(fullAddress) as XmlTextReader;
                if (Instance.Empty(textReader))
                {
                    throw new EmptyException("Variable 'textReader' is empty!");
                }
                else
                {
                    string errorcode = string.Empty;
                    string transactionId = string.Empty;
                    int checkAmount = 0;
                    string currency = string.Empty;
                    string paymentUrl = string.Empty;

                    while (textReader.Read())
                    {
                        if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "errorcode")
                        {
                            if (errorcode.Length > 0)
                            {
                                errorcode += ",";
                            }
                            errorcode += textReader.ReadString();
                        }
                        if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "transaction_id")
                        {
                            transactionId = textReader.ReadString();
                        }
                        if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "amount")
                        {
                            string checkAmountString = textReader.ReadString();
                            int.TryParse(checkAmountString, out checkAmount);
                        }
                        if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "currency")
                        {
                            currency = textReader.ReadString();
                        }
                        if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "URL")
                        {
                            paymentUrl = textReader.ReadString();
                        }
                    }

                    // Initialize the MolliePaymentReponse instance
                    response = new MolliePaymentResponse(errorcode, transactionId, checkAmount, currency, paymentUrl);
                }
            }

            return response;
        }

        /// <summary>
        /// Executes a payment check request and returns a payment check response
        /// </summary>
        /// <param name="partnerId">The Mollie.nl account number</param>
        /// <param name="transactionId">The transaction id of the payment</param>
        /// <returns>An Dionysos.Payment.IPaymentCheckResponse instance if the request was successful, Null if not</returns>
        public IPaymentCheckResponse ExecutePaymentCheckRequest(string partnerId, string transactionId)
        {
            IPaymentCheckResponse response = null;

            if (Instance.ArgumentIsEmpty(partnerId, "partnerId"))
			{
				// Parameter 'partnerId' is empty
			}
            else if (Instance.ArgumentIsEmpty(transactionId, "transactionId"))
            {
                // Parameter 'transactionId' is empty
            }
            else
            {
                MolliePaymentCheckRequest paymentCheckRequest = new MolliePaymentCheckRequest(partnerId, transactionId);
                paymentCheckRequest.TestMode = ConfigurationManager.GetBool(DionysosPaymentConfigurationConstants.PaymentTestMode);

                if (Instance.Empty(paymentCheckRequest))
                {
                    throw new EmptyException("Variable 'paymentCheckRequest' is empty.");
                }
                else
                {
                    string fullAddress = address + "check&" + paymentCheckRequest.ToString();

                    XmlTextReader textReader = HttpRequestHelper.GetResponseAsXmlReader(fullAddress) as XmlTextReader;
                    if (Instance.Empty(textReader))
                    {
                        throw new EmptyException("Variable 'textReader' is empty!");
                    }
                    else
                    {
                        string errorcode = string.Empty;
                        string checkTransactionId = string.Empty;
                        int amount = 0;
                        string currency = string.Empty;
                        bool paid = false;
                        string consumerName = string.Empty;
                        string consumerAccount = string.Empty;
                        string consumerCity = string.Empty;

                        while (textReader.Read())
                        {
                            if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "errorcode")
                            {
                                errorcode = textReader.ReadString();
                            }
                            if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "transaction_id")
                            {
                                checkTransactionId = textReader.ReadString();
                            }
                            if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "amount")
                            {
                                amount = textReader.ReadElementContentAsInt();
                            }
                            if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "currency")
                            {
                                currency = textReader.ReadString();
                            }
                            if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "payed")
                            {
                                paid = textReader.ReadElementContentAsBoolean();
                            }
                            if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "consumerName")
                            {
                                consumerName = textReader.ReadString();
                            }
                            if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "consumerAccount")
                            {
                                consumerAccount = textReader.ReadString();
                            }
                            if (textReader.MoveToContent() == XmlNodeType.Element && textReader.Name == "consumerCity")
                            {
                                consumerCity = textReader.ReadString();
                            }

                            // Initialize the MolliePaymentReponse instance
                            response = new MolliePaymentCheckResponse(errorcode, checkTransactionId, amount, currency, paid, consumerName, consumerAccount, consumerCity);
                        }
                    }
                }
            }

            return response;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the banks for this payment provider
        /// </summary>
        public BankCollection Banks
        {
            get
            {
                if (Instance.Empty(this.banks))
                {
                    this.GetBanks();
                }
                return this.banks;
            }
        }

        #endregion
    }
}
