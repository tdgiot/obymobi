﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment.Mollie
{
    /// <summary>
    /// Class which represents a request for payment for the Mollie payment provider
    /// </summary>
    public class MolliePaymentRequest : IPaymentRequest
    {
        #region Fields

        private string partnerId = string.Empty;
        private int amount = 0;
        private string bankId = string.Empty;
        private string description = string.Empty;
        private string reportUrl = string.Empty;
        private string returnUrl = string.Empty;
        private bool testMode = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.Mollie.MolliePaymentRequest type using the specified parameters
        /// </summary>
        /// <param name="partnerId">The Mollie.nl account number</param>
        /// <param name="amount">The payment amount IN CENTS!!!</param>
        /// <param name="bankId">The id of the bank</param>
        /// <param name="description">The description for the payment (max length 29 characters)</param>
        /// <param name="reportUrl">The url for the extra check</param>
        /// <param name="returnUrl">The url to return to after processing the payment</param>
        public MolliePaymentRequest(string partnerId, int amount, string bankId, string description, string reportUrl, string returnUrl)
        {
            if (Instance.ArgumentIsEmpty(partnerId, "partnerId"))
            {
                // Parameter 'partnerId' is empty
            }
            else if (Instance.ArgumentIsEmpty(bankId, "bankId"))
			{
				// Parameter 'bankId' is empty
			}
            else if (bankId.Length != 4)
            {
                throw new TechnicalException("Variable 'bankId' should consist of four characters.");
            }
            else if (Instance.ArgumentIsEmpty(description, "description"))
            {
                // Parameter 'description' is empty
            }
            else if (Instance.ArgumentIsEmpty(reportUrl, "reportUrl"))
            {
                // Parameter 'reportUrl' is empty
            }
            else if (Instance.ArgumentIsEmpty(returnUrl, "returnUrl"))
            {
                // Parameter 'returnUrl' is empty
            }
            else
            {
                this.partnerId = partnerId;
                this.amount = amount;
                this.bankId = bankId;
                if (description.Length > 29)
                {
                    this.description = description.Substring(0, 29);
                }
                else
                {
                    this.description = description;
                }
                this.reportUrl = reportUrl;
                this.returnUrl = returnUrl;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a string representation for this Mollie payment request to use in the querystring
        /// </summary>
        /// <returns>A System.String instance representing this payment request</returns>
        public override string ToString()
        {
            string request = string.Empty;

            if (Instance.Empty(this.partnerId))
            {
                throw new EmptyException("Variable 'partnerId' is empty!");
            }
            else
            {
                request += string.Format("partnerid={0}", this.partnerId);
            }

            request += "&";

            if (Instance.Empty(this.amount))
            {
                throw new EmptyException("Variable 'amount' is empty!");
            }
            else
            {
                request += string.Format("amount={0}", this.amount);
            }

            request += "&";

            if (Instance.Empty(this.bankId))
            {
                throw new EmptyException("Variable 'bankId' is empty!");
            }
            else
            {
                request += string.Format("bank_id={0}", this.bankId);
            }

            request += "&";

            if (Instance.Empty(this.description))
            {
                throw new EmptyException("Variable 'description' is empty!");
            }
            else
            {
                request += string.Format("description={0}", this.description);
            }

            request += "&";

            if (Instance.Empty(this.reportUrl))
            {
                throw new EmptyException("Variable 'reportUrl' is empty!");
            }
            else
            {
                request += string.Format("reporturl={0}", this.reportUrl);
            }

            request += "&";

            if (Instance.Empty(this.returnUrl))
            {
                throw new EmptyException("Variable 'returnUrl' is empty!");
            }
            else
            {
                request += string.Format("returnurl={0}", this.returnUrl);
            }

            if (this.testMode)
            {
                request += "&testmode=true";
            }

            return request;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Mollie.nl account number
        /// </summary>
        public string PartnerId
        {
            get
            {
                return this.partnerId;
            }
            set
            {
                this.partnerId = value;
            }
        }

        /// <summary>
        /// Gets or sets the payment amount IN CENTS!!!
        /// </summary>
        public int Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.amount = value;
            }
        }

        /// <summary>
        /// Gets or sets the id of the bank
        /// </summary>
        public string BankId
        {
            get
            {
                return this.bankId;
            }
            set
            {
                this.bankId = value;
            }
        }

        /// <summary>
        /// Gets or sets the description for the payment
        /// </summary>
        public string Description
        {
            get
            {
                return this.description;
            }
            set
            {
                this.description = value;
            }
        }

        /// <summary>
        /// Gets or sets the url for the extra check
        /// </summary>
        public string ReportUrl
        {
            get
            {
                return this.reportUrl;
            }
            set
            {
                this.reportUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the url to return to after processing the payment
        /// </summary>
        public string ReturnUrl
        {
            get
            {
                return this.returnUrl;
            }
            set
            {
                this.returnUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether we are testing
        /// </summary>
        public bool TestMode
        {
            get
            {
                return this.testMode;
            }
            set
            {
                this.testMode = value;
            }
        }

        #endregion
    }
}
