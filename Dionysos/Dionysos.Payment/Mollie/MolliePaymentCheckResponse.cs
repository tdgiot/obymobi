﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment.Mollie
{
    /// <summary>
    /// Class which represents a payment check response
    /// </summary>
    public class MolliePaymentCheckResponse : IPaymentCheckResponse
    {
        #region Fields

        private string errorcode = string.Empty;
        private string transactionId = string.Empty;
        private int amount;
        private string currency = string.Empty;
        private bool payed = false;
        private string consumerName = string.Empty;
        private string consumerAccount = string.Empty;
        private string consumerCity = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Payment.Mollie.MolliePaymentCheckResponse class using the specified parameters
        /// </summary>
        /// <param name="transactionId">the id of the transaction</param>
        /// <param name="amount">The payment amount IN CENTS!!!</param>
        /// <param name="currency">The currency of the payment</param>
        /// <param name="payed">Flag which indicates if the transaction has been payed</param>
        /// <param name="consumerName">Name of the consumer</param>
        /// <param name="consumerAccount">Account number of the consumer</param>
        /// <param name="consumerCity">City of the consumer</param>
        public MolliePaymentCheckResponse(string errorcode, string transactionId, int amount, string currency, bool payed, string consumerName, string consumerAccount, string consumerCity)
        {
            this.errorcode = errorcode;
            this.transactionId = transactionId;
            this.amount = amount;
            this.currency = currency;
            this.payed = payed;
            this.consumerName = consumerName;
            this.consumerAccount = consumerAccount;
            this.consumerCity = consumerCity;
        }		

        #endregion

		#region Methods

		/// <summary>
		/// Get all property values in a string formatted querystring like style
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{			
			return string.Format("Errorcode={0}&TransactionId={1}&Amount={2}&Currency={3}&Payed={4}&ConsumerName={5}&ConsumerAccount={6}&ConsumerCity={7}",
				this.errorcode, this.transactionId, this.amount, this.currency, this.payed, this.consumerName, this.consumerAccount, this.consumerCity);			
		}

		#endregion

		#region Properties

		/// <summary>
        /// Gets or sets the error code
        /// </summary>
        public string ErrorCode
        {
            get
            {
                return this.errorcode;
            }
            set
            {
                this.errorcode = value;
            }
        }

        /// <summary>
        /// Gets or sets the id of the transaction
        /// </summary>
        public string TransactionId
        {
            get
            {
                return this.transactionId;
            }
            set
            {
                this.transactionId = value;
            }
        }

        /// <summary>
        /// Gets or sets the payment amount IN CENTS!!!
        /// </summary>
        public int Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.amount = value;
            }
        }

        /// <summary>
        /// Gets or sets the currency for the payment
        /// </summary>
        public string Currency
        {
            get
            {
                return this.currency;
            }
            set
            {
                this.currency = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicated if the transaction has been payed
        /// </summary>
        public bool Payed
        {
            get
            {
                return this.payed;
            }
            set
            {
                this.payed = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the consumer
        /// </summary>
        public string ConsumerName
        {
            get
            {
                return this.consumerName;
            }
            set
            {
                this.consumerName = value;
            }
        }

        /// <summary>
        /// Gets or sets the account number of the consumer
        /// </summary>
        public string ConsumerAccount
        {
            get
            {
                return this.consumerAccount;
            }
            set
            {
                this.consumerAccount = value;
            }
        }

        /// <summary>
        /// Gets or sets the city of the consumer
        /// </summary>
        public string ConsumerCity
        {
            get
            {
                return this.consumerCity;
            }
            set
            {
                this.consumerCity = value;
            }
        }

        #endregion
    }
}
