﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment
{
	/// <summary>
	/// Interface which is used to implement iDEAL payment providers
	/// </summary>
	public interface IPaymentProvider
	{
		#region Methods

		/// <summary>
		/// Executes a payment request and returns a payment response
		/// </summary>
		/// <param name="partnerId">The Mollie.nl account number</param>
		/// <param name="amount">The payment amount IN CENTS!!!</param>
		/// <param name="bankId">The id of the bank</param>
		/// <param name="description">The description for the payment (max length 29 characters)</param>
		/// <param name="reportUrl">The url for the extra check</param>
		/// <param name="returnUrl">The url to return to after processing the payment</param>
		/// <returns>An Dionysos.Payment.IPaymentResponse instance if the request was successful, Null if not</returns>
		IPaymentResponse ExecutePaymentRequest(string partnerId, int amount, string bankId, string description, string reportUrl, string returnUrl);

		/// <summary>
		/// Executes a payment check request and returns a payment check response
		/// </summary>
		/// <param name="partnerId">The Mollie.nl account number</param>
		/// <param name="transactionId">The transaction id of the payment</param>
		/// <returns>An IPaymentCheckRequest instance</returns>
		IPaymentCheckResponse ExecutePaymentCheckRequest(string partnerId, string transactionId);

		#endregion

		#region Properties

		/// <summary>
		/// Gets the banks for this payment provider
		/// </summary>
		BankCollection Banks
		{
			get;
		}

		#endregion
	}
}
