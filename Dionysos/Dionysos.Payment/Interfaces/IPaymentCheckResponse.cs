﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment
{
    /// <summary>
    /// Interface which is used to implement payment check responses classes
    /// </summary>
    public interface IPaymentCheckResponse
    {
    }
}
