﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Payment
{
    /// <summary>
    /// Collection class used for storing Dionysos.Payment.Bank instances in
    /// </summary>
    public class BankCollection : ICollection<Dionysos.Payment.Bank>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the BankCollection class
        /// </summary>	
        public BankCollection()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Dionysos.Payment.Bank instance to the collection
        /// </summary>
        /// <param name="bank">The Dionysos.Payment.Bank instance to add to the collection</param>
        public void Add(Dionysos.Payment.Bank bank)
        {
            this.items.Add(bank);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Dionysos.Payment.Bank instance is already in the collection
        /// </summary>
        /// <param name="bank">The Dionysos.Payment.Bank instance to check</param>
        /// <returns>True if the Dionysos.Payment.Bank instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Payment.Bank bank)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Payment.Bank) == bank)
                {
                    contains = true;
                    break;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Payment.Bank[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Dionysos.Payment.Bank instance from this collection
        /// </summary>
        /// <param name="bank">The Dionysos.Payment.Bank instance to remove</param>
        public bool Remove(Dionysos.Payment.Bank bank)
        {
            this.items.Remove(bank);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.BankCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Payment.Bank> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Payment.Bank>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.BankCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets a Dionysos.Payment.Bank instance with the specified name
        /// </summary>
        /// <param name="name">The name of the bank to get</param>
        /// <returns>A Dionysos.Payment.Bank instance</returns>
        public Bank GetBankByName(string name)
        {
            Bank bank = null;

            for (int i = 0; i < this.items.Count; i++)
            {
                Bank temp = this.items[i] as Bank;
                if (temp.Name == name)
                {
                    bank = temp;
                    break;
                }
            }

            return bank;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Dionysos.Payment.Bank instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Dionysos.Payment.Bank instance to get</param>
        /// <returns>An Dionysos.Payment.Bank instance</returns>
        public Dionysos.Payment.Bank this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Payment.Bank;
            }
        }

        /// <summary>
        /// Gets an Dionysos.Payment.Bank instance from the collection with the specified name
        /// </summary>
        /// <param name="index">The name of the Dionysos.Payment.Bank instance to get</param>
        /// <returns>An Dionysos.Payment.Bank instance</returns>
        public Dionysos.Payment.Bank this[string name]
        {
            get
            {
                return this.GetBankByName(name);
            }
        }

        #endregion
    }
}