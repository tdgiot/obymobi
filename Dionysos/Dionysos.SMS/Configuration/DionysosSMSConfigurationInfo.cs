﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;

namespace Dionysos.SMS
{
    /// <summary>
    /// Configuration information class for Dionysos.SMS
    /// </summary>
    public class DionysosSMSConfigurationInfo : ConfigurationItemCollection
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.SMS.DionysosSMSConfigurationInfo type
        /// </summary>
        public DionysosSMSConfigurationInfo()
        {
            string sectionName = "Dionysos.SMS";
            this.Add(new ConfigurationItem(sectionName, DionysosSMSConfigurationConstants.Username, "Gebruikersnaam voor het verzenden van SMS berichten", string.Empty, typeof(string)));
            this.Add(new ConfigurationItem(sectionName, DionysosSMSConfigurationConstants.Password, "Wachtwoord voor het verzenden van SMS berichten", string.Empty, typeof(string)));
        }

        #endregion
    }
}
