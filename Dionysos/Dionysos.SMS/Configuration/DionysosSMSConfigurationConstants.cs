﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.SMS
{
    /// <summary>
    /// Configuration class which contains constants for the names of configuration items
    /// </summary>
    public class DionysosSMSConfigurationConstants
    {
        #region Fields

        /// <summary>
        /// Account number for payment processing
        /// </summary>
        public const string Username = "Username";
        public const string Password = "Password";

        #endregion
    }
}
