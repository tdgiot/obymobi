﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Dionysos.SMS
{
    public class MollieSMSRequest : ISMSRequest
    {
        public enum MollieResult : int
        {
            NotSet = 0,
            Success = 10,
            UsernameMissing = 20,
            PasswordMissing = 21,
            NoOrInvalidOriginator = 22,
            NoRecipientsSupplied = 23,
            NoMessageBodySupplied = 24,
            InvalidRecipientSupplied = 25,
            InvalidOriginator = 26,
            InvalidMessageBody = 27,
            IncompatibleCharSet = 28,
            UnknownParameterIssue = 29,
            InvalidUsernameAndOrPassword = 30,
            InsufficientCredits = 31,
            GatewayUnreachable = 98,
            UnknownError = 99,
        }

        #region Fields

        private string postUrl = "http://api.messagebird.com";
        private string securePostUrl = "https://api.messagebird.com";
        private string postPath = "/xml/sms/";
        private string username = string.Empty;
        private string password = string.Empty;
        private string originator = string.Empty;
        private string recipients = string.Empty;
        private string message = string.Empty;
        private string md5_password = string.Empty;
        private string gateway = "1";
        private string reference = string.Empty;
        private DateTime deliveryDate = DateTime.MinValue;
        private string type = string.Empty;
        private string udh = string.Empty;
        private int successCount;
        private int resultCode;
        private string resultMessage = string.Empty;
        private bool success = false;
        private bool useSecureConnection = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.SMS.MollieSMSRequest type
        /// </summary>
        public MollieSMSRequest()
        {
            this.Username = ConfigurationManager.GetString(DionysosSMSConfigurationConstants.Username);
            this.Password = ConfigurationManager.GetString(DionysosSMSConfigurationConstants.Password);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Verstuur SMS
        /// </summary>	
        /// <returns>
        /// Een string met het xml antwoord van Mollie
        /// </returns>
        public string Send()
        {
            string returnValue = string.Empty;

            string postString = this.PostUrl + this.PostPath;
            if (this.UseSecureConnection)
                postString = this.SecurePostUrl + this.PostPath;

            if (this.Username != string.Empty &&
                this.Password != string.Empty &&
                this.Originator != string.Empty &&
                this.Recipients != string.Empty &&
                this.Message != string.Empty)
            {
                string dataString = "username=" + this.Username;
                dataString += "&" + "password=" + this.Password;
                dataString += "&" + "originator=" + this.Originator;
                dataString += "&" + "recipients=" + this.Recipients;
                if (this.Gateway != string.Empty)
                    dataString += "&" + "gateway=" + this.Gateway;
                if (this.Reference != string.Empty)
                    dataString += "&" + "reference=" + this.Reference;
                dataString += "&" + "message=" + this.Message.Replace("&", "");

                if (this.Type != string.Empty)
                    dataString += "&" + "type=" + this.Type;

                if (this.UDH != string.Empty)
                    dataString += "&" + "udh=" + this.UDH;

                if (this.DeliveryDate != null)
                {
                    StringBuilder objDeliveryDate = new StringBuilder();
                    objDeliveryDate.Append(this.DeliveryDate.Year.ToString("0000"));
                    objDeliveryDate.Append(this.DeliveryDate.Month.ToString("00"));
                    objDeliveryDate.Append(this.DeliveryDate.Day.ToString("00"));
                    objDeliveryDate.Append(this.DeliveryDate.Hour.ToString("00"));
                    objDeliveryDate.Append(this.DeliveryDate.Minute.ToString("00"));
                    objDeliveryDate.Append(this.DeliveryDate.Second.ToString("00"));
                    dataString += "&" + "deliverydate=" + objDeliveryDate;
                }

                XmlTextReader xmlReader = new XmlTextReader(string.Format("{0}?{1}", postString, dataString));

                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element)
                    {
                        switch (xmlReader.LocalName)
                        {
                            case "recipients":
                                {
                                    this.SuccessCount = int.Parse(xmlReader.ReadString());
                                    returnValue += "recipients : " + this.SuccessCount.ToString() + "\r\n";
                                    break;
                                }
                            case "success":
                                {
                                    this.Success = bool.Parse(xmlReader.ReadString());
                                    returnValue += "success : " + this.Success.ToString() + "\r\n";
                                    break;
                                }
                            case "resultcode":
                                {
                                    this.ResultCode = int.Parse(xmlReader.ReadString());
                                    returnValue += "resultcode : " + this.ResultCode.ToString() + "\r\n";
                                    break;
                                }
                            case "resultmessage":
                                {
                                    this.ResultMessage = xmlReader.ReadString();
                                    returnValue += "resultmessage : " + this.ResultMessage + "\r\n";
                                    break;
                                }
                        }
                    }
                }
            }

            // Antwoord is als :
            // <?xml version="1.0"?>
            // <response>
            // <item type="sms">
            //         <recipients>1</recipients>
            //         <success>true</success>
            //         <resultcode>10</resultcode>
            //         <resultmessage>Message successfully sent.</resultmessage>
            //     </item>
            // </response>
            return returnValue;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the url to post the sms request to
        /// </summary>
        public string PostUrl
        {
            get
            {
                return this.postUrl;
            }
            set
            {
                this.postUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the url to post the sms request to
        /// </summary>
        public string SecurePostUrl
        {
            get
            {
                return this.securePostUrl;
            }
            set
            {
                this.securePostUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the path to post the sms request to
        /// </summary>
        public string PostPath
        {
            get
            {
                return this.postPath;
            }
            set
            {
                this.postPath = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mollie.nl username
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mollie.nl password
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }

        /// <summary>
        /// Gets or sets the sender of the message
        /// </summary>
        /// <remarks>
        /// Maximum of 14 digits or 11 characters
        /// </remarks>
        public string Originator
        {
            get
            {
                return this.originator;
            }
            set
            {
                this.originator = value;
            }
        }

        /// <summary>
        /// Gets or sets the recipients (comma separated)
        /// </summary>
        public string Recipients
        {
            get
            {
                return this.recipients;
            }
            set
            {
                this.recipients = value;
            }
        }

        /// <summary>
        /// Gets or sets the message of the SMS
        /// </summary>
        /// <remarks>
        /// Maximum 160 characters
        /// </remarks>
        public string Message
        {
            get
            {
                return this.message;
            }
            set
            {
                this.message = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mollie.nl password encrypted with MD5
        /// </summary>
        public string MD5_Password
        {
            get
            {
                return this.md5_password;
            }
            set
            {
                this.md5_password = value;
            }
        }

        /// <summary>
        /// Gets or sets the gateway for sending the SMS with
        /// </summary>
        /// <remarks>
        /// Gateway = 1 : Deze gateway zit direct aangesloten bij de Nederlandse operators. Het voordeel hiervan is dat de SMS-berichten met de meeste betrouwbaarheid worden verstuurd. Deze gateway is ideaal voor partijen die zeker willen zijn dat de berichten met de meeste betrouwbaarheid en snelheid verstuurd worden.
        /// Gateway = 2 (default) : Deze gateway is gebouwd om koste efficiënt SMS te kunnen versturen. Ons systeem kijkt per operator en land wat de beste en goedkoopste manier is om de berichten te versturen. Deze gateway is ideaal voor partijen die koste efficiëntie belangrijker vinden dan de volledige betrouwbaarheid.
        /// Gateway = 3 : Deze gateway heeft beperkte mogelijkheden, zo is het bijvoorbeeld niet mogelijk om een eigen afzender te kiezen. Verstuurt u naar veel nummers tegelijk via deze gateway dan zullen deze ook met een beperkte snelheid van +/- 10 per seconden verwerkt worden.
        /// Gateway = 8 : Deze gateway is speciaal voor het vaste net en werkt alleen naar Nederlandse nummers. De afzender is altijd een Nederlands 06-nummer. Het unieke van deze gateway is dat deze naast mobiele nummers ook naar het vaste net kan versturen.
        /// Zie ook: http://www.mollie.nl/informatie/sms/gateways/nerdisch
        /// </remarks>
        public string Gateway
        {
            get
            {
                return this.gateway;
            }
            set
            {
                this.gateway = value;
            }
        }

        /// <summary>
        /// Gets or sets the reference for delivery
        /// </summary>
        /// <remarks>
        /// 50 - delivered 
        /// 51 - sent 
        /// 52 - buffered 
        /// 53 - delivery failed
        /// 54 - delivery not allowed/possible
        /// 55 - destination off
        /// 56 - destination does not respond
        /// 57 - error at destination
        /// 58 - memory full at destination
        /// 59 - unknown destination
        /// </remarks>
        public string Reference
        {
            get
            {
                return this.reference;
            }
            set
            {
                this.reference = value;
            }
        }

        /// <summary>
        /// Gets or sets the delivery date
        /// </summary>
        public DateTime DeliveryDate
        {
            get
            {
                return this.deliveryDate;
            }
            set
            {
                this.deliveryDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of SMS-message
        /// </summary>
        /// <remarks>
        /// normal
        /// wappush
        /// vcard
        /// flash
        /// binary
        /// long
        /// </remarks>
        public string Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
            }
        }

        /// <summary>
        /// Gets or sets the header of the SMS-message (only when type = binary)
        /// </summary>
        public string UDH
        {
            get
            {
                return this.udh;
            }
            set
            {
                this.udh = value;
            }
        }

        /// <summary>
        /// Gets or sets the count of successful sents
        /// </summary>
        public int SuccessCount
        {
            get
            {
                return this.successCount;
            }
            set
            {
                this.successCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the result code of the sms sending
        /// </summary>
        public int ResultCode
        {
            get
            {
                return this.resultCode;
            }
            set
            {
                this.resultCode = value;
            }
        }

        /// <summary>
        /// Gets the result enum.
        /// </summary>
        public MollieResult ResultEnum
        {
            get
            {
                MollieResult result;
                if (EnumUtil.TryParse(this.ResultCode, out result))
                    return result;
                else
                    return MollieResult.NotSet;
            }
        }

        /// <summary>
        /// Gets or sets the result message of the sms sending
        /// </summary>
        public string ResultMessage
        {
            get
            {
                return this.resultMessage;
            }
            set
            {
                this.resultMessage = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether sending was successful
        /// </summary>
        public bool Success
        {
            get
            {
                return this.success;
            }
            set
            {
                this.success = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether the secure connection should be used
        /// </summary>
        public bool UseSecureConnection
        {
            get
            {
                return this.useSecureConnection;
            }
            set
            {
                this.useSecureConnection = value;
            }
        }

        #endregion
    }
}
