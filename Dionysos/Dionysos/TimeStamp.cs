using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Class which represents a timestamp
    /// </summary>
    public class TimeStamp
    {
        #region Fields

        static TimeStamp instance = null;

        private int year = 0;
        private int month = 0;
        private int day = 0;
        private int hour = 0;
        private int minute = 0;
        private int second = 0;
        private int millisecond = 0;
        private long ticks = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the TimeStamp class
        /// </summary>	
        public TimeStamp()
        {
            System.DateTime currentDateTime = System.DateTime.Now;
            this.year                   = currentDateTime.Year;
            this.month                  = currentDateTime.Month;
            this.day                    = currentDateTime.Day;
            this.hour                   = currentDateTime.Hour;
            this.minute                 = currentDateTime.Minute;
            this.second                 = currentDateTime.Second;
            this.millisecond            = currentDateTime.Millisecond;
            this.ticks                  = currentDateTime.Ticks;
        }

        /// <summary>
        /// Constructs an instance of the TimeStamp class
        /// </summary>	
        /// <param name="dateTime">The DateTime instance to construct the TimeStamp for</param>
        public TimeStamp(System.DateTime dateTime)
        {
            this.year                   = dateTime.Year;
            this.month                  = dateTime.Month;
            this.day                    = dateTime.Day;
            this.hour                   = dateTime.Hour;
            this.minute                 = dateTime.Minute;
            this.second                 = dateTime.Second;
            this.millisecond            = dateTime.Millisecond;
            this.ticks                  = dateTime.Ticks;
        }

        /// <summary>
        /// Constructs an instance of the TimeStamp class
        /// </summary>	
        /// <param name="timeStamp">The System.String instance which contains the timestamp to construct the TimeStamp for</param>
        public TimeStamp(string timeStamp)
        {
            if (Instance.ArgumentIsEmpty(timeStamp, "timeStamp"))
            {
                // Argument 'timeStamp' is empty
            }
            else
            {
                this.year = Int32.Parse(timeStamp.Substring(0, 4));
                this.month = Int32.Parse(timeStamp.Substring(4, 2));
                this.day = Int32.Parse(timeStamp.Substring(6, 2));
                this.hour = Int32.Parse(timeStamp.Substring(8, 2));
                this.minute = Int32.Parse(timeStamp.Substring(10, 2));
                if (timeStamp.Length > 12)
                    this.second = Int32.Parse(timeStamp.Substring(12, 2));
                if (timeStamp.Length > 14)
                    this.millisecond = Int32.Parse(timeStamp.Substring(14, 3));
            }
        }

        /// <summary>
        /// Constructs a static instance of the Lynx-media.TimeStamp type
        /// </summary>
        static TimeStamp()
        {
            if (instance == null)
            {
                instance = new TimeStamp();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Converts an instance of the TimeStamp type to its equivalent string representation
        /// </summary>
        /// <returns>The string representation of this instance of the TimeStamp type</returns>
        public override string ToString()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}{6}", this.year.ToString("0000"), this.month.ToString("00"), this.day.ToString("00"), this.hour.ToString("00"), this.minute.ToString("00"), this.second.ToString("00"), this.millisecond.ToString("000"));
        }

        /// <summary>
        /// Updates this instance of the TimeStamp to the current date and time
        /// </summary>
        public void Update()
        {
            System.DateTime currentDateTime = System.DateTime.Now;
            this.year = currentDateTime.Year;
            this.month = currentDateTime.Month;
            this.day = currentDateTime.Day;
            this.hour = currentDateTime.Hour;
            this.minute = currentDateTime.Minute;
            this.second = currentDateTime.Second;
            this.millisecond = currentDateTime.Millisecond;
        }

        /// <summary>
        /// Retrieves a DateTime instance from this TimeStamp instance
        /// </summary>
        /// <returns>A DateTime instance</returns>
        public System.DateTime GetDateTime()
        {
            return new System.DateTime(this.year, this.month, this.day, this.hour, this.minute, this.second, this.millisecond);
        }

        /// <summary>
        /// Creates a string containing a timestamp of the specified DateTime instance
        /// </summary>
        /// <param name="dateTime">The DateTime instance to create a timestamp for</param>
        /// <returns>A System.String instance containing the timestamp</returns>
        public static string CreateTimeStamp(System.DateTime dateTime)
        {
            return CreateTimeStamp(dateTime, true);
        }

        /// <summary>
        /// Creates a string containing a timestamp of the specified DateTime instance
        /// </summary>
        /// <param name="dateTime">The DateTime instance to create a timestamp for</param>
        /// <param name="includeMilliSeconds">flag which indicates whether milliseconds should be included in the timestamp</param>
        /// <returns>A System.String instance containing the timestamp</returns>
        public static string CreateTimeStamp(System.DateTime dateTime, bool includeMilliSeconds)
        {
            string timeStamp = string.Empty;
            if (includeMilliSeconds)
            {
                timeStamp = string.Format("{0}{1}{2}{3}{4}{5}{6}", dateTime.Year.ToString("0000"), dateTime.Month.ToString("00"), dateTime.Day.ToString("00"), dateTime.Hour.ToString("00"), dateTime.Minute.ToString("00"), dateTime.Second.ToString("00"), dateTime.Millisecond.ToString("000"));
            }
            else
            {
                timeStamp = string.Format("{0}{1}{2}{3}{4}{5}", dateTime.Year.ToString("0000"), dateTime.Month.ToString("00"), dateTime.Day.ToString("00"), dateTime.Hour.ToString("00"), dateTime.Minute.ToString("00"), dateTime.Second.ToString("00"));
            }
            return timeStamp;
        }

        /// <summary>
        /// Creates a string containing a timestamp of the specified DateTime instance
        /// </summary>
        /// <returns>A System.String instance containing the timestamp</returns>
        public static string CreateTimeStamp()
        {
            return CreateTimeStamp(DateTime.Now);
        }

        /// <summary>
        /// Creates a string containing a timestamp of the specified DateTime instance
        /// </summary>
        /// <returns>A System.String instance containing the timestamp</returns>
        public static string CreateTimeStamp(bool includeMilliSeconds)
        {
            return CreateTimeStamp(DateTime.Now, includeMilliSeconds);
        }

        /// <summary>
        /// Gets a DateTime instance from the specified timestamp string
        /// </summary>
        /// <param name="timeStamp">The System.String instance containing the timestamp</param>
        /// <returns>A DateTime instance</returns>
        public static System.DateTime GetDateTime(string timeStamp)
        {
            System.DateTime dateTime = System.DateTime.Now;
            
            if (Instance.ArgumentIsEmpty(timeStamp, "timeStamp"))
            {
                // Argument 'timeStamp' is empty
            }
            else
            {
                TimeStamp temp = new TimeStamp(timeStamp);
                dateTime = temp.GetDateTime();
            }

            return dateTime;
        }

        /// <summary>
        /// Parses a System.String instance to a TimeStamp instance 
        /// </summary>
        /// <param name="timeStamp">The System.String instance which contains the timestamp</param>
        /// <returns>A TimeStamp instance</returns>
        public static TimeStamp Parse(string timeStamp)
        {
            TimeStamp timeStmp = null;
            
            if (Instance.ArgumentIsEmpty(timeStamp, "timeStamp"))
            {
                // Argument 'timeStamp' is empty
            }
            else
            {
                timeStmp = new TimeStamp(timeStamp);
            }

            return timeStmp;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the year
        /// </summary>
        public int Year
        {
            get
            {
                return this.year;
            }
            set
            {
                this.year = value;
            }
        }

        /// <summary>
        /// Gets or sets the month
        /// </summary>
        public int Month
        {
            get
            {
                return this.month;
            }
            set
            {
                this.month = value;
            }
        }

        /// <summary>
        /// Gets or sets the day of the month
        /// </summary>
        public int Day
        {
            get
            {
                return this.day;
            }
            set
            {
                this.day = value;
            }
        }

        /// <summary>
        /// Gets or sets the hour
        /// </summary>
        public int Hour
        {
            get
            {
                return this.hour;
            }
            set
            {
                this.hour = value;
            }
        }

        /// <summary>
        /// Gets or sets the minute
        /// </summary>
        public int Minute
        {
            get
            {
                return this.minute;
            }
            set
            {
                this.minute = value;
            }
        }

        /// <summary>
        /// Gets or sets the second
        /// </summary>
        public int Second
        {
            get
            {
                return this.second;
            }
            set
            {
                this.second = value;
            }
        }

        /// <summary>
        /// Gets or sets the millisecond
        /// </summary>
        public int Millisecond
        {
            get
            {
                return this.millisecond;
            }
            set
            {
                this.millisecond = value;
            }
        }

        /// <summary>
        /// Gets or sets the ticks
        /// </summary>
        public long Ticks
        {
            get
            {
                return this.ticks;
            }
            set
            {
                this.ticks = value;
            }
        }

        #endregion
    }
}
