﻿using System.Collections.Generic;

namespace Dionysos
{
    public static class DictionaryUtil
    {
        /// <summary>
        /// Returns the value in the dictionary based on the key or returns a default value
        /// </summary>
        /// <typeparam name="TKey">The key in the dictionary.</typeparam>
        /// <typeparam name="TValue">The default value to return when the key can't be found in the dictionary.</typeparam>
        /// <param name="dictionary">The dictionary to search.</param>
        /// <param name="key">The key to search for in the dictionary.</param>
        /// <returns></returns>
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue ret;
            // Ignore return value
            dictionary.TryGetValue(key, out ret);
            return ret;
        }
    }
}
