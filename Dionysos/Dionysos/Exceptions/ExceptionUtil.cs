using System;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Singleton utility class used for processing exception with
    /// </summary>
    public static class ExceptionUtil
    {
        #region Methods

        /// <summary>
        /// Gets all messages of an Exception and it's inner exceptions.
        /// (Never tested ;)).
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="includeStackTraces"></param>
        /// <returns></returns>
        public static string GetAllMessages(this Exception exception, bool includeStackTraces = false)
        {
            string toReturn = string.Empty;
            toReturn += exception.Message + "\r\n";

            if (includeStackTraces)
                toReturn += exception.ProcessStackTrace() + "\r\n";
            
            if (exception.InnerException != null)
            {
                toReturn += exception.InnerException.GetAllMessages(includeStackTraces);                
            }

            return toReturn;
        }


        /// <summary>
        /// Returns the full stack trace including all inner exceptions
        /// </summary>
        /// <param name="exception">The System.Exception instance to get the full stack trace for</param>
        /// <returns>A System.String instance containing the full stack trace</returns>
        [Obsolete("Use ProcessStackTrace")]
        public static string GetFullStackTrace(this Exception exception)
        {            
            return ProcessStackTrace(exception);
            // GK Was (and in my opinion was wrong, you don't get the full stack trace)
            // return ProcessStackTrace(exception.GetBaseException());
        }

        /// <summary>
        /// Processes the stack trace of an exception to a formatted
        /// </summary>
        /// <param name="exception">The Exception instance to process the stack trace for</param>
        /// <param name="recursive"></param>
        /// <returns>A formatted string containg the stack trace of the specified exception</returns>
        public static string ProcessStackTrace(this Exception exception, bool recursive = false)
        {
            var stackTrace = new StringBuilder();

            if (Instance.ArgumentIsEmpty(exception, "exception") || exception == null)
            {
                // Parameter 'exception' is empty
            }
            else if (Instance.ArgumentIsEmpty(exception.StackTrace, "exception.StackTrace") || exception.StackTrace == null)
            {
                // Parameter 'exception.StackTrace' is empty
            }
            else
            {
                string[] stackTraceLines;
                try
                {
                    string[] split = {"\r\n"};
                    stackTraceLines = exception.StackTrace.Split(split, StringSplitOptions.None);
                }
                catch
                {
                    stackTraceLines = new[] {exception.StackTrace};
                }

                const string tab = "\t";

                stackTrace.AppendLine("Message: " + exception.Message);

                for (int i = 0; i < stackTraceLines.Length; i++)
                {
                    int counter = i + 1;
                    stackTrace.Append(counter.ToString("000") + ". ");

                    for (int j = 0; j < i; j++)
                    {
                        stackTrace.Append(tab);
                    }

                    stackTrace.AppendLine(stackTraceLines[i].TrimStart());
                }
            }

            if (recursive && exception != null && exception.InnerException != null)
            {
                stackTrace.AppendLine(ProcessStackTrace(exception.InnerException, recursive)); // GAKR - Seems this was a bug, so I added , recursive (otherwise recursing would stop after 1 level.
            }

            return stackTrace.ToString();
        }

        #endregion
    }
}