﻿using System;

namespace Dionysos
{
    /// <summary>
    /// Exception class used for throwing entity not found exceptions
    /// </summary>
    public class EntityNotFoundException : ApplicationException
    {
        /// <summary>
        /// Constructs an instance of the EntityNotFoundException class
        /// </summary>
        /// <param name="message"></param>
        public EntityNotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructs an instance of the EntityNotFoundException class
        /// </summary>
        /// <param name="message">Message format</param>
        /// <param name="args">Elements to format</param>
        public EntityNotFoundException(string message, params object[] args)
            : base(string.Format(message, args))
        {
        }
    }
}
