using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for empty instances
    /// </summary>
    public class AuthorizationException : FunctionalException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Exceptions.EmptyException class
        /// </summary>	
        public AuthorizationException(string message) : base(message)
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="message">Message format</param>
        /// <param name="args">Elements to format</param>
		public AuthorizationException(string message, params object[] args)
            : base(string.Format(message, args))
        {             
        }

        #endregion
    }
}
