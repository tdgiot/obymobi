using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Exception class used for throwing functional exceptions.
	/// </summary>
    [Serializable]
	public class FunctionalException : ApplicationException
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FunctionalException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public FunctionalException(string message)
			: base(message)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FunctionalException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="innerException">The inner exception.</param>
		public FunctionalException(string message, Exception innerException)
			: base(message, innerException)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FunctionalException"/> class.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An Object array containing zero or more objects to format.</param>
		public FunctionalException(string format, params object[] args)
			: this(null, null, format, args)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FunctionalException"/> class.
		/// </summary>
		/// <param name="innerException">The inner exception.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An Object array containing zero or more objects to format.</param>
		public FunctionalException(Exception innerException, string format, params object[] args)
			: this(null, null, format, args)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FunctionalException"/> class.
		/// </summary>
		/// <param name="provider">An IFormatProvider implementation that supplies culture-specific formatting information.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An Object array containing zero or more objects to format.</param>
		public FunctionalException(IFormatProvider provider, string format, params object[] args)
			: this(null, provider, format, args)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FunctionalException"/> class.
		/// </summary>
		/// <param name="innerException">The inner exception.</param>
		/// <param name="provider">An IFormatProvider implementation that supplies culture-specific formatting information.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An Object array containing zero or more objects to format.</param>
		public FunctionalException(Exception innerException, IFormatProvider provider, string format, params object[] args)
			: base(String.Format(provider, format, args), innerException)
		{ }

		#endregion
	}
}
