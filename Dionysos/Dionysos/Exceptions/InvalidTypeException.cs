using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for when payments go wrong
    /// </summary>
    public class PaymentException : TechnicalException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.PaymentException class
        /// </summary>	
        public PaymentException(string message) : base(message)
        {
        }

        #endregion
    }
}

