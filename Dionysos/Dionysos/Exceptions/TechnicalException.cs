using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class used for throwing technical exceptions
    /// </summary>
    public class TechnicalException : ApplicationException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="message"></param>
        public TechnicalException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="message">Message format</param>
        /// <param name="args">Elements to format</param>
        public TechnicalException(string message, params object[] args)
            : base(StringUtilLight.FormatSafe(message, args))
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="innerException">Inner Exception</param>
        /// <param name="message">Message format</param>
        /// <param name="args">Elements to format</param>
        public TechnicalException(Exception innerException, string message, params object[] args)
            : base(StringUtilLight.FormatSafe(message, args), innerException)
        {
        }

        #endregion
    }
}
