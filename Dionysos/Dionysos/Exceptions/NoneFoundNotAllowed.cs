using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for empty instances
    /// </summary>
    public class NoneFoundNotAllowed : TechnicalException
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Exceptions.EmptyException class
        /// </summary>	
        public NoneFoundNotAllowed(string message) : base(message)
        {
        }

        /// <summary>
		/// Constructs an instance of the Lynx_media.EmptyException class
        /// </summary>
        /// <param name="message">Message format</param>
        /// <param name="args">Elements to format</param>
		public NoneFoundNotAllowed(string message, params object[] args)
			: base(string.Format(message, args))
        {             
        }

        #endregion

        #region Methods



        #endregion

        #region Properties



        #endregion

        #region Event handlers



        #endregion
    }
}
