using System;

namespace Dionysos
{
    /// <summary>
    /// Exception class used for throwing file not found exceptions
    /// </summary>
    public class FileNotFoundException : ApplicationException
    {
        /// <summary>
        /// Constructs an instance of the FileNotFoundException class
        /// </summary>
        /// <param name="message"></param>
        public FileNotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructs an instance of the FileNotFoundException class
        /// </summary>
        /// <param name="message">Message format</param>
        /// <param name="args">Elements to format</param>
        public FileNotFoundException(string message, params object[] args)
            : base(string.Format(message, args))
        {
        }
    }
}
