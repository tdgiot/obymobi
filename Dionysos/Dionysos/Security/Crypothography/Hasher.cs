using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Dionysos.Security.Cryptography
{
	/// <summary>
	/// Gets hash values using MD5, SHA1, SHA256 or SHA512.
	/// </summary>
	public static class Hasher
	{
        public static string GetHash(Stream stream, HashType hashType)
        {
            byte[] bytes = new byte[stream.Length];
            stream.Position = 0;
            stream.Read(bytes, 0, (int)stream.Length);

            return Hasher.GetHash(bytes, hashType);
        }

        public static string GetHashForFile(string path, HashType hashType)
        {
            byte[] fileBytes;
            using(FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                fileBytes = new byte[fs.Length];
                fs.Read(fileBytes, 0, (int)fs.Length);
            }

            return Hasher.GetHash(fileBytes, hashType);
        }

        public static string GetHash(byte[] bytes, HashType hashType)
        {                        
            // Initialize appropriate hashing algorithm class.
            HashAlgorithm hash;
            switch (hashType)
            {
                case HashType.MD5:
                    hash = new MD5CryptoServiceProvider();
                    break;
                case HashType.SHA1:
                    hash = new SHA1Managed();
                    break;
                case HashType.SHA256:
                    hash = new SHA256Managed();
                    break;
                case HashType.SHA384:
                    hash = new SHA384Managed();
                    break;
                case HashType.SHA512:
                    hash = new SHA512Managed();
                    break;
                default:
                    throw new NotImplementedException("HashType is not yet implemented.");
            }

            // Compute hash value of our plain text.
            byte[] hashBytes = hash.ComputeHash(bytes);

            // Convert result into a string.
            string hashValue = BitConverter.ToString(hashBytes).Replace("-", String.Empty);

            // Return the result.
            return hashValue;
        }
 

		/// <summary>
		/// Gets the hashed text using the specified hash type.
		/// </summary>
		/// <param name="text">The text to hash.</param>
		/// <param name="hashType">The hash type.</param>
		/// <returns>Returns the hash.</returns>
		public static string GetHash(string text, HashType hashType)
		{            
			// Convert text into a byte array.            
            byte[] textBytes = Encoding.UTF8.GetBytes(text);
            return Hasher.GetHash(textBytes, hashType);			
		}

		/// <summary>
		/// Checks the hash by hashing the original using the hash type and comparing it to the hashed string (ignores casing).
		/// </summary>
		/// <param name="original">The string to hash and compare.</param>
		/// <param name="hashString">The hashed string.</param>
		/// <param name="hashType">The hash type.</param>
		/// <returns>Returns true if the hashed matches; otherwise false.</returns>
		public static bool CheckHash(string original, string hashString, HashType hashType)
		{
			return GetHash(original, hashType).Equals(hashString, StringComparison.OrdinalIgnoreCase);
		}
	}

	/// <summary>
	/// The available hash types.
	/// </summary>
	public enum HashType
	{
		/// <summary>
		/// MD5 hash type.
		/// </summary>
		MD5,
		/// <summary>
		/// SHA-1 hash type.
		/// </summary>
		SHA1,
		/// <summary>
		/// SHA-256 hash type.
		/// </summary>
		SHA256,
		/// <summary>
		/// SHA-384 hash type.
		/// </summary>
		SHA384,
		/// <summary>
		/// SHA-512 hash type.
		/// </summary>
		SHA512
	}
}
