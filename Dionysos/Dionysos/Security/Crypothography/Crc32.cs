using System;
using System.IO;
using System.Security.Cryptography;

namespace Dionysos.Security.Cryptography
{
	/// <summary>
	/// Calculate crc hash from a file
	/// </summary>
	/// <example>
	/// Crc32 crc32 = new Crc32();
	/// String hash = String.Empty;
	///
	/// using (FileStream fs = File.Open("c:\\myfile.txt", FileMode.Open)) {
	/// 	foreach (byte b in crc32.ComputeHash(fs)) {
	///         hash += b.ToString("x2").ToLower();
	///     }
	/// }
	///
	/// Console.WriteLine("CRC-32 is {0}", hash);
	/// </example>
	public class Crc32 : HashAlgorithm
	{
		private const UInt32 DEFAULT_POLYNOMIAL = 0xedb88320;
		private const UInt32 DEFAULT_SEED = 0xffffffff;
		private static UInt32[] defaultTable;

		private readonly UInt32 seed;
		private readonly UInt32[] table;
		private UInt32 hash;

		/// <summary>
		/// Compute crc32 hash for file
		/// </summary>
		/// <param name="file">The file.</param>
		/// <returns></returns>
		public static string ComputeHash(string file)
		{
			var crc32 = new Crc32();
			var hash = String.Empty;

			using (var fs = File.Open(file, FileMode.Open))
			{
				foreach (byte b in crc32.ComputeHash(fs))
				{
					hash += b.ToString("x2").ToLower();
				}
			}

			return hash;
		}

	    public static string ComputeHashBuffer(byte[] buffer)
	    {
            var crc32 = new Crc32();
            var hash = String.Empty;

            foreach (byte b in crc32.ComputeHash(buffer))
            {
                hash += b.ToString("x2").ToLower();
            }

	        return hash;
	    }

	    /// <summary>
		/// Initializes a new instance of the <see cref="Crc32"/> class.
		/// </summary>
		public Crc32()
		{
			table = InitializeTable(DEFAULT_POLYNOMIAL);
			seed = DEFAULT_SEED;
			Initialize();
		}

		/// <summary>
		/// Constructor. If you dont know what polynomial and seed do, don't use them.
		/// </summary>
		/// <param name="polynomial"></param>
		/// <param name="seed"></param>
		public Crc32(UInt32 polynomial, UInt32 seed)
		{
			table = InitializeTable(polynomial);
			this.seed = seed;
			Initialize();
		}

		/// <summary>
		/// Gets the size, in bits, of the computed hash code.
		/// </summary>
		/// <returns>The size, in bits, of the computed hash code.</returns>
		public override int HashSize
		{
			get { return 32; }
		}

		/// <summary>
		/// Initializes an implementation of the <see cref="T:System.Security.Cryptography.HashAlgorithm" /> class.
		/// </summary>
		public override sealed void Initialize()
		{
			hash = seed;
		}

		/// <summary>
		/// Hashes the core.
		/// </summary>
		/// <param name="buffer">The buffer.</param>
		/// <param name="start">The start.</param>
		/// <param name="length">The length.</param>
		protected override void HashCore(byte[] buffer, int start, int length)
		{
			hash = CalculateHash(table, hash, buffer, start, length);
		}

		/// <summary>
		/// When overridden in a derived class, finalizes the hash computation after the last data is processed by the cryptographic stream object.
		/// </summary>
		/// <returns>
		/// The computed hash code.
		/// </returns>
		protected override byte[] HashFinal()
		{
			byte[] hashBuffer = UInt32ToBigEndianBytes(~hash);
			HashValue = hashBuffer;
			return hashBuffer;
		}

		/// <summary>
		/// Cmpute hash from byte array buffer
		/// </summary>
		/// <param name="buffer">The buffer.</param>
		/// <returns></returns>
		public static UInt32 Compute(byte[] buffer)
		{
			return ~CalculateHash(InitializeTable(DEFAULT_POLYNOMIAL), DEFAULT_SEED, buffer, 0, buffer.Length);
		}

		/// <summary>
		/// Compute hash from byte array buffer with custom seed. If you dont know what the seed is for, dont use it.
		/// </summary>
		/// <param name="seed">The seed.</param>
		/// <param name="buffer">The buffer.</param>
		/// <returns></returns>
		public static UInt32 Compute(UInt32 seed, byte[] buffer)
		{
			return ~CalculateHash(InitializeTable(DEFAULT_POLYNOMIAL), seed, buffer, 0, buffer.Length);
		}

		/// <summary>
		/// Compute hash from byte array with custom polynomial and seed. If you dont know what those, don't use it.
		/// </summary>
		/// <param name="polynomial">The polynomial.</param>
		/// <param name="seed">The seed.</param>
		/// <param name="buffer">The buffer.</param>
		/// <returns></returns>
		public static UInt32 Compute(UInt32 polynomial, UInt32 seed, byte[] buffer)
		{
			return ~CalculateHash(InitializeTable(polynomial), seed, buffer, 0, buffer.Length);
		}

		/// <summary>
		/// Initializes the table.
		/// </summary>
		/// <param name="polynomial">The polynomial.</param>
		/// <returns></returns>
		private static UInt32[] InitializeTable(UInt32 polynomial)
		{
			if (polynomial == DEFAULT_POLYNOMIAL && defaultTable != null)
				return defaultTable;

			var createTable = new UInt32[256];
			for (int i = 0; i < 256; i++)
			{
				var entry = (UInt32)i;
				for (int j = 0; j < 8; j++)
					if ((entry & 1) == 1)
						entry = (entry >> 1) ^ polynomial;
					else
						entry = entry >> 1;
				createTable[i] = entry;
			}

			if (polynomial == DEFAULT_POLYNOMIAL)
				defaultTable = createTable;

			return createTable;
		}

		/// <summary>
		/// Calculates the hash.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="seed">The seed.</param>
		/// <param name="buffer">The buffer.</param>
		/// <param name="start">The start.</param>
		/// <param name="size">The size.</param>
		/// <returns></returns>
		private static UInt32 CalculateHash(UInt32[] table, UInt32 seed, byte[] buffer, int start, int size)
		{
			UInt32 crc = seed;
			for (int i = start; i < size; i++)
				unchecked
				{
					crc = (crc >> 8) ^ table[buffer[i] ^ crc & 0xff];
				}
			return crc;
		}

		/// <summary>
		/// Us the int32 to big endian bytes.
		/// </summary>
		/// <param name="x">The x.</param>
		/// <returns></returns>
		private byte[] UInt32ToBigEndianBytes(UInt32 x)
		{
			return new[]
				{
					(byte) ((x >> 24) & 0xff),
					(byte) ((x >> 16) & 0xff),
					(byte) ((x >> 8) & 0xff),
					(byte) (x & 0xff)
				};
		}
	}
}