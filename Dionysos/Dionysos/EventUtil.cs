using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class EventUtil
    {
        /// <summary>
        /// Raises the specified handler, does null checking and only calls each listeners once, even if it's subscribed multiple times
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler">The handler.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The args.</param>
        public static void Raise<T>(this EventHandler<T> handler, object sender, bool arg) where T : EventArgs
        {
            EventUtil.InternalRaise(handler, sender, new object[] { new EventArgumentBool(arg) });   
        }

        /// <summary>
        /// Raises the specified handler, does null checking and only calls each listeners once, even if it's subscribed multiple times
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler">The handler.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The args.</param>
        public static void Raise<T>(this EventHandler<T> handler, object sender, T args) where T : EventArgs
        {
            EventUtil.InternalRaise(handler, sender, args);   
        }

        private static void InternalRaise<T>(EventHandler<T> handler, object sender, params object[] args) where T : EventArgs
        {
            // GK This might be a perfomance hit because of the checking for double events and dyanmicinvoke
            // Two alternatives:
            // - Rewrite the code in such a way that it can be used with generics
            // - Write a code generator to create an invoker method for each EventHandler
            if (handler == null)
                return;

            Dictionary<Reflection.MethodInfo, List<object>> calledMethods = new Dictionary<Reflection.MethodInfo, List<object>>();
            foreach (Delegate listener in handler.GetInvocationList())
            {
                try
                {
                    if (calledMethods.ContainsKey(listener.Method) && calledMethods[listener.Method].Contains(listener.Target))
                    {
						Log.Warning("EventUtil", "InternalRaise", "A double handler subscription for {0}.{1}. - Not called", handler.Target.GetType().Name, handler.Method.Name);
                    }
                    else
                    {
                        if(args.Length == 1)
                            listener.DynamicInvoke(sender, args[0]);
                        else 
                            listener.DynamicInvoke(sender, args);

                        if (!calledMethods.ContainsKey(listener.Method))
                        {
                            calledMethods.Add(listener.Method, new List<object>());
                        }
                        
                        calledMethods[listener.Method].Add(listener.Target);                        
                    }                    
                }
                catch (Exception ex)
                {
					if(ex.GetType() == typeof(System.Reflection.TargetInvocationException) && ex.InnerException != null)
						Log.Error("EventUtil", "InternalRaise", "A handler that called to {0}.{1} caused an Exception: {2}\n{3}", handler.Target.GetType().Name, handler.Method.Name, ex.InnerException.Message, ex.InnerException.StackTrace);
					else
						Log.Error("EventUtil", "InternalRaise", "A handler that called to {0}.{1} caused an Exception: {2}\n{3}", handler.Target.GetType().Name, handler.Method.Name, ex.Message, ex.StackTrace);
                }
            }
        }
    }
}
