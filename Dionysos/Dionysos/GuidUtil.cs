using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Singleton utility class which can be used for operations on Guid's
	/// </summary>
	public class GuidUtil
	{
		#region Fields

		static GuidUtil instance = null;
        static Object lockObj = new Object();
		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the GuidUtil class if the instance has not been initialized yet
		/// </summary>
		public GuidUtil()
		{
			if (instance == null)
			{
				// first create the instance
				instance = new GuidUtil();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the static GuidUtil instance
		/// </summary>
		private void Init()
		{
		}

		/// <summary>
		/// Creates a new string representation of a guid
		/// </summary>
		/// <returns>A System.String instance representing the guid</returns>
		public static string CreateGuid()
		{
            // GK Added lock, in theory two parallel threads could create the same guids.
            lock (GuidUtil.lockObj)
            {
                if(!GuidUtil.GuidPrefix.IsNullOrWhiteSpace())
                    return GuidUtil.GuidPrefix + Guid.NewGuid().ToString().Replace("-", String.Empty);
                else
                    return Guid.NewGuid().ToString().Replace("-", String.Empty);
            }
		}

        private static string guidPrefix;


        /// <summary>
        /// Gets or sets the GUID prefix, prefixes CreateGuid() results.
        /// </summary>
        /// <value>
        /// The GUID prefix.
        /// </value>
        public static string GuidPrefix
        {
            get
            {
                return GuidUtil.guidPrefix;
            }
            set
            {
                GuidUtil.guidPrefix = value;
            }
        }

		#endregion
	}
}