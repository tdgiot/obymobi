﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
    public static class DoubleUtil
    {
        public static double? TryParse(string value)
        {
            double d;
            if (double.TryParse(value, out d))
                return d;
            else
                return null;
        }
    }
}
