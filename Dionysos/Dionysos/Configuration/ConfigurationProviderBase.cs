using Dionysos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Configuration
{
	/// <summary>
	/// Base class for configuration providers.
	/// </summary>
	public abstract class ConfigurationProviderBase : IConfigurationProvider
	{
		#region Data Access for Settings (mostly abstract)

		/// <summary>
		/// Gets the current value of the specified configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetValue(string name)
		{
			return GetValue(name, null);
		}

		/// <summary>
		/// Gets the current value of the specified configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetValue(string name, bool applicationSpecific)
		{
			return this.GetValue(name, (bool?)applicationSpecific);
		}

		/// <summary>
		/// Gets the current value of the specified configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		protected abstract string GetValue(string name, bool? applicationSpecific);

		/// <summary>
		/// Gets the current value of the specified application settings item
		/// </summary>
		/// <param name="item">The name of the application settings item</param>
		/// <returns>A System.Object instance containing the current value</returns>
		public abstract string GetValueRefreshed(string item);

		/// <summary>
		/// Gets the current value of the specified application settings item
		/// </summary>
		/// <param name="item">The name of the application settings item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>A System.Object instance containing the current value</returns>
		public abstract string GetValueRefreshed(string item, bool applicationSpecific);

		/// <summary>
		/// Sets the value of the specified configuration item 
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="value">The value to set to the configuration item</param>
		public void SetValue(string name, object value)
		{
			this.SetValue(name, value, null);
		}

		/// <summary>
		/// Sets the value of the specified configuration item 
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <param name="value">The value to set to the configuration item</param>
		public void SetValue(string name, object value, bool applicationSpecific)
		{
			this.SetValue(name, value, (bool?)applicationSpecific);
		}

		/// <summary>
		/// Sets the value of the specified configuration item 
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <param name="value">The value to set to the configuration item</param>
		protected abstract void SetValue(string name, object value, bool? applicationSpecific);

		#endregion

		#region Utils / Helpers

		/// <summary>
		/// Get's if the setting is defined for the current running application.
		/// This might not be the case when a certain set of ConfigInfo is not initialized in the application start.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>
		/// bool
		/// </returns>
		public bool IsDefined(string name)
		{
			ConfigurationItem configurationItem = this.GetConfigurationItem(name);

			// Check whether the configuration item information has been supplied
			return (configurationItem != null);
		}

		/// <summary>
		/// Gets configuration information for the specified configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item, based on the configuration constants</param>
		/// <returns>A Dionysos.Configuration.ConfigurationItem instance containing information about the configuration item</returns>
		private ConfigurationItem GetConfigurationItem(string name)
		{
			ConfigurationItem configurationItem = null;

			for (int i = 0; i < GlobalLight.ConfigurationInfo.Count; i++)
			{
				ConfigurationItemCollection configurationItems = GlobalLight.ConfigurationInfo[i];
				for (int j = 0; j < configurationItems.Count; j++)
				{
					ConfigurationItem temp = configurationItems[j];
					if (temp.Name.ToUpper() == name.ToUpper())
					{
						configurationItem = temp;
						break;
					}
				}

				if (!Instance.Empty(configurationItem))
				{
					break;
				}
			}

			return configurationItem;
		}


		#endregion

		#region Get/Set values typed

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public bool GetBool(string name)
		{
			return Boolean.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public bool GetBool(string name, bool applicationSpecific)
		{
			return Boolean.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public bool GetBoolRefreshed(string name)
		{
			return Boolean.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public bool GetBoolRefreshed(string name, bool applicationSpecific)
		{
			return Boolean.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Byte instance containing the current value</returns>
		public byte GetByte(string name)
		{
			return Byte.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Byte instance containing the current value</returns>
		public byte GetByte(string name, bool applicationSpecific)
		{
			return Byte.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Byte instance containing the current value</returns>
		public byte GetByteRefreshed(string name)
		{
			return Byte.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Byte instance containing the current value</returns>
		public byte GetByteRefreshed(string name, bool applicationSpecific)
		{
			return Byte.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Char instance containing the current value</returns>
		public char GetChar(string name)
		{
			return Char.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Char instance containing the current value</returns>
		public char GetChar(string name, bool applicationSpecific)
		{
			return Char.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Char instance containing the current value</returns>
		public char GetCharRefreshed(string name)
		{
			return Char.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Char instance containing the current value</returns>
		public char GetCharRefreshed(string name, bool applicationSpecific)
		{
			return Char.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.DateTime instance containing the current value</returns>
		public DateTime GetDateTime(string name)
		{
			return DateTime.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.DateTime instance containing the current value</returns>
		public DateTime GetDateTime(string name, bool applicationSpecific)
		{
			return DateTime.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.DateTime instance containing the current value</returns>
		public DateTime GetDateTimeRefreshed(string name)
		{
			return DateTime.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.DateTime instance containing the current value</returns>
		public DateTime GetDateTimeRefreshed(string name, bool applicationSpecific)
		{
			return DateTime.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Decimal instance containing the current value</returns>
		public decimal GetDecimal(string name)
		{
			return Decimal.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Decimal instance containing the current value</returns>
		public decimal GetDecimal(string name, bool applicationSpecific)
		{
			return Decimal.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Decimal instance containing the current value</returns>
		public decimal GetDecimalRefreshed(string name)
		{
			return Decimal.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Decimal instance containing the current value</returns>
		public decimal GetDecimalRefreshed(string name, bool applicationSpecific)
		{
			return Decimal.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Double instance containing the current value</returns>
		public double GetDouble(string name)
		{
			return Double.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Double instance containing the current value</returns>
		public double GetDouble(string name, bool applicationSpecific)
		{
			return Double.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Double instance containing the current value</returns>
		public double GetDoubleRefreshed(string name)
		{
			return Double.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Double instance containing the current value</returns>
		public double GetDoubleRefreshed(string name, bool applicationSpecific)
		{
			return Double.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Single instance containing the current value</returns>
		public float GetFloat(string name)
		{
			return Single.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Single instance containing the current value</returns>
		public float GetFloat(string name, bool applicationSpecific)
		{
			return Single.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Single instance containing the current value</returns>
		public float GetFloatRefreshed(string name)
		{
			return Single.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Single instance containing the current value</returns>
		public float GetFloatRefreshed(string name, bool applicationSpecific)
		{
			return Single.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int32 instance containing the current value</returns>
		public int GetInt(string name)
		{
			return Int32.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int32 instance containing the current value</returns>
		public int GetInt(string name, bool applicationSpecific)
		{
			return Int32.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int32 instance containing the current value</returns>
		public int GetIntRefreshed(string name)
		{
			return Int32.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int32 instance containing the current value</returns>
		public int GetIntRefreshed(string name, bool applicationSpecific)
		{
			return Int32.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int64 instance containing the current value</returns>
		public long GetLong(string name)
		{
			return Int64.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int64 instance containing the current value</returns>
		public long GetLong(string name, bool applicationSpecific)
		{
			return Int64.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int64 instance containing the current value</returns>
		public long GetLongRefreshed(string name)
		{
			return Int64.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int64 instance containing the current value</returns>
		public long GetLongRefreshed(string name, bool applicationSpecific)
		{
			return Int64.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int16 instance containing the current value</returns>
		public short GetShort(string name)
		{
			return Int16.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int16 instance containing the current value</returns>
		public short GetShort(string name, bool applicationSpecific)
		{
			return Int16.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int16 instance containing the current value</returns>
		public short GetShortRefreshed(string name)
		{
			return Int16.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int16 instance containing the current value</returns>
		public short GetShortRefreshed(string name, bool applicationSpecific)
		{
			return Int16.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetString(string name)
		{
			return this.GetValue(name);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetString(string name, bool applicationSpecific)
		{
			return this.GetValue(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetStringRefreshed(string name)
		{
			return this.GetValueRefreshed(name);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetStringRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets a password
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetPassword(string name)
		{
			return GetPassword(name, false);
		}

		/// <summary>
		/// Gets a password
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetPassword(string name, bool applicationSpecific)
		{
			string value = this.GetValue(name, applicationSpecific);
			if (value.Length > 0)
				value = Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(value);

			return value;
		}

		/// <summary>
		/// Sets a password value
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		public void SetPassword(string name, string value)
		{
			SetPassword(name, value, false);
		}

		/// <summary>
		/// Sets a password value
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		public void SetPassword(string name, string value, bool applicationSpecific)
		{
			if (value.Length > 0)
				value = Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(value);

			this.SetValue(name, value);
		}

		#endregion
	}
}
