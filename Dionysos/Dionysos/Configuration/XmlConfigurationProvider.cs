using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Linq;
using System.Xml.XPath;
//using Dionysos.IO;
using Dionysos.Interfaces;
using System.Threading;
//using Dionysos.Diagnostics;

namespace Dionysos.Configuration
{
	/// <summary>
	/// Configuration provider class which is used for reading/writing configuration values from xml files
	/// </summary>
	public class XmlConfigurationProvider : IConfigurationProvider
	{
		#region Fields

		/// <summary>
		/// The default config file.
		/// </summary>
		protected const string defaultConfigFile = "AppSettings.xml";

		/// <summary>
		/// The manual config file path.
		/// </summary>
		protected string manualConfigFilePath = string.Empty;

		/// <summary>
		/// The locker object.
		/// </summary>
		private object locker = new object();
        private object xmlDocumentLocker = new object();

        private string configMemoryBackup;
        private readonly Action<String> logCallback;

        #endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Lynx-media.Configuration.XmlApplicationSettings class
		/// </summary>	
		public XmlConfigurationProvider()
		{
		}

        /// <summary>
        /// Constructs an instance of the Lynx-media.Configuration.XmlApplicationSettings class
        /// </summary>	
        public XmlConfigurationProvider(Action<String> logCallback)
        {
            this.logCallback = logCallback;
        }

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the path to the configuration file override
		/// </summary>
		public string ManualConfigFilePath
		{
			get
			{
				return this.manualConfigFilePath;
			}
			set
			{
				this.manualConfigFilePath = value;
			}
		}

		#endregion

		#region Methods

        private void Log(string message)
        {
            if (logCallback != null)
            {
                logCallback(message);
            }
        }

	    private XmlDocument LoadBackupFile()
	    {
            string backupPath = this.GetXmlDocumentFullPath() + ".bak";

            try
            {
                return this.GetXmlDocument(backupPath, "", true);
            }
            catch
            {
                Log("Failed to load backup config file, checking in-memory backup.");
                if (String.IsNullOrWhiteSpace(configMemoryBackup))
                {
                    Log("In-memory backup has not been created.");
                    return null;
                }
                else
                {
                    return this.GetXmlDocument(backupPath, configMemoryBackup);
                }
            }
	    }

		/// <summary>
		/// Get XML Document
		/// </summary>
		/// <returns></returns>
		private XmlDocument GetXmlDocument()
		{
            lock(this.xmlDocumentLocker)
            {
                try
                {
                    bool anyFileExists = File.Exists(this.GetXmlDocumentFullPath()) ||
                                         File.Exists(this.GetXmlDocumentFullPath() + ".bak") ||
                                         !string.IsNullOrWhiteSpace(this.configMemoryBackup);
                    return this.GetXmlDocument(this.GetXmlDocumentFullPath(), "", anyFileExists);
                }
                catch
                {
                    Log("Failed to load primary config file, trying backup file...");

                    XmlDocument doc = LoadBackupFile();
                    if (doc == null)
                    {
                        Log("Unable to load configuration and backup configuration");
                        throw new Exception("Unable to load configuration and backup configuration");
                    }

                    // Save main configuration for next time
                    Log("Saving backup file as primary config...");
                    SaveXml(this.GetXmlDocumentFullPath(), doc);

                    return doc;
                }
            }
        }

		/// <summary>
		/// Get XML Document
		/// </summary>
		/// <returns></returns>
		private string GetXmlDocumentFullPath()
		{
			if (this.manualConfigFilePath.Length > 0)
				return this.manualConfigFilePath;
			else
				return Path.Combine(GlobalLight.ApplicationInfo.BasePath, defaultConfigFile);
		}

		/// <summary>
		/// Loads the XML.
		/// </summary>
		/// <param name="configFile">The config file.</param>
		/// <returns></returns>
		protected virtual string LoadXml(string configFile)
		{
            if (File.Exists(configFile))
            {
                return File.ReadAllText(configFile);
            }
			
            return string.Empty;
		}

		/// <summary>
		/// Saves the XML.
		/// </summary>
		/// <param name="configFile">The config file.</param>
		/// <param name="doc">The doc.</param>
		protected void SaveXml(string configFile, XmlDocument doc)
		{
			string xml;
			using (var stringWriter = new StringWriter())
			{
				XmlWriterSettings settings = new XmlWriterSettings();
				settings.Indent = true;
				settings.IndentChars = "  ";
				settings.Encoding = Encoding.UTF8;
				using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
				{
					doc.WriteTo(xmlTextWriter);
					xmlTextWriter.Flush();
					xml = stringWriter.GetStringBuilder().ToString();
				}
			}
			this.SaveXml(configFile, xml);
		}

		/// <summary>
		/// Saves the XML.
		/// </summary>
		/// <param name="configFile">The config file.</param>
		/// <param name="xml">The XML.</param>
        protected virtual void SaveXml(string configFile, string xml)
		{
		    try
		    {
		        // Check if XML is valid
		        XmlDocument doc = new XmlDocument();
		        doc.LoadXml(xml);

                // Save xml to memory backup (just in case)
                this.configMemoryBackup = xml;
                
                // File content is fine, save it
                WriteAndCheckFile(configFile, xml);

                // Create backup file
                WriteAndCheckFile(configFile + ".bak", xml);
            }
		    catch (Exception ex)
		    {
		        Log(string.Format("Something went wrong while writing config to file. Exception: {0}", ex.GetAllMessages()));
		    }
		}

        private bool WriteAndCheckFile(string filename, string contents, bool retry = true)
        {
            File.WriteAllText(filename, contents);

            string output = File.ReadAllText(filename);

            if (!output.Equals(contents, StringComparison.CurrentCultureIgnoreCase))
            {
                if (retry)
                {
                    Log(string.Format("Failed to write '{0}' content to disk. Trying again...", filename));
                    return WriteAndCheckFile(filename, contents, false);
                }

                Log(string.Format("Failed (again) to write '{0}' to disk. Something is going wrong, corrupt disk?", filename));
                return false;
            }

            if (!retry)
            {
                Log(string.Format("Writing content for file '{0}' to disk succeeded on second try.", filename));
            }

            return true;
        }

		/// <summary>
		/// Saves the values of the application settings instance to disk using the specified filename
		/// </summary>
		/// <param name="configFile">The path to write the configuration file to</param>
		/// <returns>True if saving was succesful, False if not</returns>
		private XmlDocument GetXmlDocument(string configFile, string backupXml = "", bool throwOnError = false)
		{
			// Create and initialize a XmlDocument instance
			lock (locker)
			{
				XmlDocument doc = new XmlDocument();
                string xml = String.IsNullOrWhiteSpace(backupXml) ? this.LoadXml(configFile) : backupXml;
                if (StringUtilLight.IsNullOrWhiteSpace(xml) && !throwOnError)
				{
					// Create the top node and append it to the XmlDocument instance
					XmlNode appSettingsNode = doc.CreateNode(XmlNodeType.Element, "configuration", string.Empty);
					doc.AppendChild(appSettingsNode);

					// Walk through the application settings sections
					for (int i = 0; i < GlobalLight.ConfigurationInfo.Count; i++)
					{
						// Debug.WriteLine("Create XML");
						var configurationItems = GlobalLight.ConfigurationInfo[i].OrderBy(ci => ci.Name).OrderBy(ci => ci.Section).ToList();

						string currentSectionName = string.Empty;
						if (configurationItems.Count() > 0)
						{
							XmlNode sectionNode = null;

							for (int j = 0; j < configurationItems.Count(); j++)
							{
								ConfigurationItem configurationItem = configurationItems[j];

								if (configurationItems[j].Section != currentSectionName)
								{
									// Add Section node <section name="Dyonisos">                       
									sectionNode = doc.CreateNode(XmlNodeType.Element, "section", string.Empty);
									XmlAttribute sectionName = doc.CreateAttribute("name");
									sectionName.Value = configurationItems[j].Section;
									sectionNode.Attributes.Append(sectionName);
									appSettingsNode.AppendChild(sectionNode);
									currentSectionName = configurationItems[j].Section;
								}

								XmlNode itemNode = doc.CreateNode(XmlNodeType.Element, "item", string.Empty);
								XmlAttribute itemName = doc.CreateAttribute("name");
								itemName.InnerText = configurationItem.Name;
								itemNode.Attributes.Append(itemName);
								if (configurationItem.ApplicationSpecific)
								{
									XmlAttribute itemApplicationSpecific = doc.CreateAttribute("applicationName");
									itemApplicationSpecific.InnerText = GlobalLight.ApplicationInfo.ApplicationName;
									itemNode.Attributes.Append(itemApplicationSpecific);
								}

								XmlNode itemFriendlyName = doc.CreateNode(XmlNodeType.Element, "friendlyname", string.Empty);
								itemFriendlyName.InnerText = configurationItem.FriendlyName;
								itemNode.AppendChild(itemFriendlyName);

								XmlNode type = doc.CreateNode(XmlNodeType.Element, "type", string.Empty);
								type.InnerText = configurationItem.Type.ToString();
								itemNode.AppendChild(type);

								XmlNode value = doc.CreateNode(XmlNodeType.Element, "value", string.Empty);
								value.InnerText = configurationItem.DefaultValue.ToString();
								itemNode.AppendChild(value);

								sectionNode.AppendChild(itemNode);
							}
						}
					}

					// Save the settings file to disk
					this.SaveXml(configFile, doc);
				}
				else
				{
                    // Debug.WriteLine("Load XML");                    
					doc.LoadXml(xml);
				}

				return doc;
			}
		}

		/// <summary>
		/// Get's if the setting is defined for the current running application.
		/// This might not be the case when a certain set of ConfigInfo is not initialized in the application start.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>
		/// bool
		/// </returns>
		public bool IsDefined(string name)
		{
			ConfigurationItem configurationItem = this.GetConfigurationItem(name);

			// Check whether the configuration item information has been supplied
			return (configurationItem != null);
		}

		/// <summary>
		/// Gets configuration information for the specified configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item, based on the configuration constants</param>
		/// <returns>A Dionysos.Configuration.ConfigurationItem instance containing information about the configuration item</returns>
		private ConfigurationItem GetConfigurationItem(string name)
		{
			ConfigurationItem configurationItem = null;

			for (int i = 0; i < GlobalLight.ConfigurationInfo.Count; i++)
			{
				ConfigurationItemCollection configurationItems = GlobalLight.ConfigurationInfo[i];
				for (int j = 0; j < configurationItems.Count; j++)
				{
					ConfigurationItem temp = configurationItems[j];
					if (temp.Name.ToUpper() == name.ToUpper())
					{
						configurationItem = temp;
						break;
					}
				}

				if (!Instance.Empty(configurationItem))
				{
					break;
				}
			}

			return configurationItem;
		}

		private XmlNode RetrieveXmlItemNode(string name, bool? applicationSpecific, XmlDocument doc)
		{
			XmlNode item = null;

			if (doc == null)
			{
				doc = this.GetXmlDocument();
			}
			ConfigurationItem configurationItem = this.GetConfigurationItem(name);
			if (configurationItem == null)
			{
				throw new EmptyException(string.Format("No configuration item found for configuration constant '{0}'. Check the corresponding configuration info class.", name));
			}
			else
			{
				// Retrieven nodes
				string xmlSearchExpression = string.Empty;
				if (applicationSpecific ?? configurationItem.ApplicationSpecific)
					xmlSearchExpression = string.Format("//section[@name='{0}']/item[@name='{1}' and @applicationName='{2}']", configurationItem.Section, configurationItem.Name, GlobalLight.ApplicationInfo.ApplicationName);
				else
					xmlSearchExpression = string.Format("//section[@name='{0}']/item[@name='{1}' and not(@applicationName)]", configurationItem.Section, configurationItem.Name);

				XmlNodeList configItems = doc.SelectNodes(xmlSearchExpression);

				if (configItems.Count == 0)
				{
					item = null;
				}
				else if (configItems.Count > 1)
				{
					throw new Dionysos.TechnicalException(string.Format("Er is meer dan 1 ConfigurationItem gevonden in de xml configuratie file voor {0}.{1}", configurationItem.Section, configurationItem.Name));
				}
				else
				{
					item = configItems[0];
					if (item["value"] == null)
					{
						throw new EmptyException(string.Format("XmlNode '{0}.{1}' does not have a field 'value'.", configurationItem.Section, configurationItem.Name));
					}
					if (item["friendlyname"] == null)
					{
						throw new EmptyException(string.Format("XmlNode '{0}.{1}' does not have a field 'friendlyname'.", configurationItem.Section, configurationItem.Name));
					}
					if (item["type"] == null)
					{
						throw new EmptyException(string.Format("XmlNode '{0}.{1}' does not have a field 'type'.", configurationItem.Section, configurationItem.Name));
					}
				}
			}

			return item;
		}

		/// <summary>
		/// Gets an entity containing the configuration for the specified configuration section
		/// </summary>
		/// <param name="name">The name of the configuration value</param>
		/// <returns>An IEntity instance containing the configuration if the configuration value exists, null if not</returns>
		private string RetrieveXmlItemValue(string name)
		{
			return RetrieveXmlItemValue(name, null);
		}

		/// <summary>
		/// Gets an entity containing the configuration for the specified configuration section
		/// </summary>
		/// <param name="name">The name of the configuration value</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration item is application specific</param>
		/// <returns>An IEntity instance containing the configuration if the configuration value exists, null if not</returns>
		private string RetrieveXmlItemValue(string name, bool? applicationSpecific)
		{
			string toReturn = string.Empty;

			// Get the configruation item from the ConfigurationItemInfo collection
			// this contains the (meta)data (NOT the value) of the retrieved config item
			ConfigurationItem configurationItem = this.GetConfigurationItem(name);

			// Check whether the configuration item information has been supplied
			if (Instance.Empty(configurationItem))
			{
				throw new EmptyException(string.Format("No configuration item found for configuration constant '{0}'. Check the initialization of the configuration info in the GlobalLight Application class.", name));
			}
			else if (Instance.Empty(configurationItem.Name))
			{
				throw new EmptyException(string.Format("Configuration item '{0}' does not have a name.", name));
			}
			else if (Instance.Empty(configurationItem.Section))
			{
				throw new EmptyException(string.Format("Configuration item '{0}' does not have a section.", name));
			}
			else
			{
				XmlNode item = this.RetrieveXmlItemNode(name, applicationSpecific, null);
				if (item != null)
				{
					toReturn = item["value"].InnerText;
				}
			}

			return toReturn;
		}

		#endregion

		#region IConfigurationProvider Members

		/// <summary>
		/// Gets the current value of the specified configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetValue(string name)
		{
			return GetValue(name, null);
		}

		/// <summary>
		/// Gets the current value of the specified configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetValue(string name, bool applicationSpecific)
		{
			return this.GetValue(name, (bool?)applicationSpecific);
		}

		/// <summary>
		/// Gets the current value of the specified configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		private string GetValue(string name, bool? applicationSpecific)
		{

			lock (locker)
			{
				// Debug.WriteLine("GetValue: " + name + " - 1 - LOCK START");				
				string value = this.RetrieveXmlItemValue(name, applicationSpecific);
				if (Instance.Empty(value))
                {
                    // DK 201905229 - Fixed GK "not bugskie, but still a bugsky"
                    string defaultValue = this.GetConfigurationItem(name).DefaultValue.ToString();
                    if (value.Equals(defaultValue, StringComparison.CurrentCultureIgnoreCase))
                    {
                        // Return value if default value is the same, otherwise we're writing to disk for no reason
                        return value;
                    }

                    // GK 20080131
                    // Not found, return default value
                    try
					{
						value = this.GetConfigurationItem(name).DefaultValue.ToString();
                        this.SetValue(name, value, applicationSpecific);
                    }
					catch (Exception ex)
					{
						throw new Dionysos.TechnicalException("No default value could be found OR set for setting: " + name + "\n\r Error: " + ex.Message);
					}
				}

                return value;
			}
		}

		/// <summary>
		/// Gets the current value of the specified application settings item
		/// </summary>
		/// <param name="item">The name of the application settings item</param>
		/// <returns>A System.Object instance containing the current value</returns>
		public string GetValueRefreshed(string item)
		{
			return this.GetValue(item);
		}

		/// <summary>
		/// Gets the current value of the specified application settings item
		/// </summary>
		/// <param name="item">The name of the application settings item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>A System.Object instance containing the current value</returns>
		public string GetValueRefreshed(string item, bool applicationSpecific)
		{
			return this.GetValue(item, applicationSpecific);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public bool GetBool(string name)
		{
			return Boolean.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public bool GetBool(string name, bool applicationSpecific)
		{
			return Boolean.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public bool GetBoolRefreshed(string name)
		{
			return Boolean.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public bool GetBoolRefreshed(string name, bool applicationSpecific)
		{
			return Boolean.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Byte instance containing the current value</returns>
		public byte GetByte(string name)
		{
			return Byte.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Byte instance containing the current value</returns>
		public byte GetByte(string name, bool applicationSpecific)
		{
			return Byte.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Byte instance containing the current value</returns>
		public byte GetByteRefreshed(string name)
		{
			return Byte.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Byte instance containing the current value</returns>
		public byte GetByteRefreshed(string name, bool applicationSpecific)
		{
			return Byte.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Char instance containing the current value</returns>
		public char GetChar(string name)
		{
			return Char.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Char instance containing the current value</returns>
		public char GetChar(string name, bool applicationSpecific)
		{
			return Char.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Char instance containing the current value</returns>
		public char GetCharRefreshed(string name)
		{
			return Char.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Char instance containing the current value</returns>
		public char GetCharRefreshed(string name, bool applicationSpecific)
		{
			return Char.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.DateTime instance containing the current value</returns>
		public DateTime GetDateTime(string name)
		{
			return DateTime.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.DateTime instance containing the current value</returns>
		public DateTime GetDateTime(string name, bool applicationSpecific)
		{
			return DateTime.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.DateTime instance containing the current value</returns>
		public DateTime GetDateTimeRefreshed(string name)
		{
			return DateTime.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.DateTime instance containing the current value</returns>
		public DateTime GetDateTimeRefreshed(string name, bool applicationSpecific)
		{
			return DateTime.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Decimal instance containing the current value</returns>
		public decimal GetDecimal(string name)
		{
			return Decimal.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Decimal instance containing the current value</returns>
		public decimal GetDecimal(string name, bool applicationSpecific)
		{
			return Decimal.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Decimal instance containing the current value</returns>
		public decimal GetDecimalRefreshed(string name)
		{
			return Decimal.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Decimal instance containing the current value</returns>
		public decimal GetDecimalRefreshed(string name, bool applicationSpecific)
		{
			return Decimal.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Double instance containing the current value</returns>
		public double GetDouble(string name)
		{
			return Double.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Double instance containing the current value</returns>
		public double GetDouble(string name, bool applicationSpecific)
		{
			return Double.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Double instance containing the current value</returns>
		public double GetDoubleRefreshed(string name)
		{
			return Double.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Double instance containing the current value</returns>
		public double GetDoubleRefreshed(string name, bool applicationSpecific)
		{
			return Double.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Single instance containing the current value</returns>
		public float GetFloat(string name)
		{
			return Single.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Single instance containing the current value</returns>
		public float GetFloat(string name, bool applicationSpecific)
		{
			return Single.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Single instance containing the current value</returns>
		public float GetFloatRefreshed(string name)
		{
			return Single.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Single instance containing the current value</returns>
		public float GetFloatRefreshed(string name, bool applicationSpecific)
		{
			return Single.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int32 instance containing the current value</returns>
		public int GetInt(string name)
		{
			return Int32.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int32 instance containing the current value</returns>
		public int GetInt(string name, bool applicationSpecific)
		{
			return Int32.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int32 instance containing the current value</returns>
		public int GetIntRefreshed(string name)
		{
			return Int32.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int32 instance containing the current value</returns>
		public int GetIntRefreshed(string name, bool applicationSpecific)
		{
			return Int32.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int64 instance containing the current value</returns>
		public long GetLong(string name)
		{
			return Int64.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int64 instance containing the current value</returns>
		public long GetLong(string name, bool applicationSpecific)
		{
			return Int64.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int64 instance containing the current value</returns>
		public long GetLongRefreshed(string name)
		{
			return Int64.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int64 instance containing the current value</returns>
		public long GetLongRefreshed(string name, bool applicationSpecific)
		{
			return Int64.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int16 instance containing the current value</returns>
		public short GetShort(string name)
		{
			return Int16.Parse(this.GetValue(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int16 instance containing the current value</returns>
		public short GetShort(string name, bool applicationSpecific)
		{
			return Int16.Parse(this.GetValue(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.Int16 instance containing the current value</returns>
		public short GetShortRefreshed(string name)
		{
			return Int16.Parse(this.GetValueRefreshed(name));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Int16 instance containing the current value</returns>
		public short GetShortRefreshed(string name, bool applicationSpecific)
		{
			return Int16.Parse(this.GetValueRefreshed(name, applicationSpecific));
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetString(string name)
		{
			return this.GetValue(name);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetString(string name, bool applicationSpecific)
		{
			return this.GetValue(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetStringRefreshed(string name)
		{
			return this.GetValueRefreshed(name);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetStringRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets a password
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetPassword(string name)
		{
			return GetPassword(name, false);
		}

		/// <summary>
		/// Gets a password
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetPassword(string name, bool applicationSpecific)
		{
			string value = this.GetValue(name, applicationSpecific);
			if (!value.IsNullOrWhiteSpace())
				value = Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(value);

			return value;
		}

		/// <summary>
		/// Sets the value of the specified configuration item 
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="value">The value to set to the configuration item</param>
		public void SetValue(string name, object value)
		{
			this.SetValue(name, value, null);
		}

		/// <summary>
		/// Sets the value of the specified configuration item 
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <param name="value">The value to set to the configuration item</param>
		public void SetValue(string name, object value, bool applicationSpecific)
		{
			this.SetValue(name, value, (bool?)applicationSpecific);
		}

		/// <summary>
		/// Sets a password value
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		public void SetPassword(string name, string value)
		{
			SetPassword(name, value, false);
		}

		/// <summary>
		/// Sets a password value
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		public void SetPassword(string name, string value, bool applicationSpecific)
		{
            if (!value.IsNullOrWhiteSpace())
                value = Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(value);
            else
                value = string.Empty;

			this.SetValue(name, value);
		}

		/// <summary>
		/// Sets the value of the specified configuration item 
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <param name="value">The value to set to the configuration item</param>
		private void SetValue(string name, object value, bool? applicationSpecific)
		{
			lock (locker)
			{
				// Debug.WriteLine("SetValue: " + name + " - 2b - LOCK START");				
				// First try to get the XML node at once            
				XmlDocument doc;
				doc = this.GetXmlDocument();
				XmlNode itemNode = this.RetrieveXmlItemNode(name, applicationSpecific, doc);

				if (itemNode != null)
				{
                    string newValue = value.ToString();
                    string oldValue = itemNode["value"].InnerText;

                    if (newValue.Equals(oldValue, StringComparison.CurrentCultureIgnoreCase))
                    {
                        // Same value, just return, don't save
                        return;
                    }

                    // YES, THE BEST CASE: JUST UPDATE THE VALUE
                    itemNode["value"].InnerText = value.ToString();
                }
				else
				{
					// NO, A LOT TO DO AND CHECK
					// Try to find the section first
					ConfigurationItem configurationItem = this.GetConfigurationItem(name);
					XmlNode sectionNode = null;

					// Retrieven node
					string xmlSearchExpression = string.Format("//section[@name='{0}']", configurationItem.Section, configurationItem.Name);
					XmlNodeList configSections = doc.SelectNodes(xmlSearchExpression);

					// Check if we found teh section
					if (configSections.Count == 0)
					{
						// Not found
						sectionNode = null;
					}
					else if (configSections.Count > 1)
					{
						// Multiple found
						throw new Dionysos.TechnicalException(string.Format("Er is meer dan 1 Section gevonden in de xml configuratie file voor {0}", configurationItem.Section));
					}
					else
					{
						// 1 found, succes!
						sectionNode = configSections[0];
					}

					// Check if we found the section
					if (sectionNode == null)
					{
						// Section not yet available, so add.
						// First retrieve configuarion node
						xmlSearchExpression = "/configuration";
						XmlNodeList rootSections = doc.SelectNodes(xmlSearchExpression);
						if (rootSections.Count != 1)
						{
							throw new Dionysos.TechnicalException("Er zijn 0 of meerdere <configuration> elementen gevonden in appSettings.xml");
						}

						// We throw error if rootSection.Count != 1 so can savely use it here
						XmlNode appSettingsNode = rootSections[0];

						// Create and add section node
						sectionNode = doc.CreateNode(XmlNodeType.Element, "section", string.Empty);
						XmlAttribute sectionName = doc.CreateAttribute("name");
						sectionName.Value = configurationItem.Section;
						sectionNode.Attributes.Append(sectionName);
						appSettingsNode.AppendChild(sectionNode);
					}

					// We now have the section, let's add the item                
					itemNode = doc.CreateNode(XmlNodeType.Element, "item", string.Empty);
					XmlAttribute itemName = doc.CreateAttribute("name");
					itemName.InnerText = configurationItem.Name;
					itemNode.Attributes.Append(itemName);
					if (applicationSpecific ?? configurationItem.ApplicationSpecific)
					{
						XmlAttribute itemApplicationSpecific = doc.CreateAttribute("applicationName");
						itemApplicationSpecific.InnerText = GlobalLight.ApplicationInfo.ApplicationName;
						itemNode.Attributes.Append(itemApplicationSpecific);
					}

					XmlNode itemFriendlyName = doc.CreateNode(XmlNodeType.Element, "friendlyname", string.Empty);
					itemFriendlyName.InnerText = configurationItem.FriendlyName;
					itemNode.AppendChild(itemFriendlyName);

					XmlNode type = doc.CreateNode(XmlNodeType.Element, "type", string.Empty);
					type.InnerText = configurationItem.Type.ToString();
					itemNode.AppendChild(type);

					XmlNode valueElement = doc.CreateNode(XmlNodeType.Element, "value", string.Empty);
					valueElement.InnerText = value.ToString();
					itemNode.AppendChild(valueElement);

					sectionNode.AppendChild(itemNode);
				}

				this.SaveXml(this.GetXmlDocumentFullPath(), doc);
				doc = null;
				// Debug.WriteLine("SetValue: " + name + " - 2c - LOCK END");
			}
		}

		#endregion
	}
}
