using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Configuration
{
    /// <summary>
    /// Configuration class which represents an application settings item
    /// </summary>
    public class ApplicationSettingsItem
    {
        #region Fields

        private string name = string.Empty;
        private object value = null;
        private object defaultValue = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ApplicationSettingsItem class
        /// </summary>	
        public ApplicationSettingsItem()
        {
        }

        /// <summary>
        /// Constructs an instance of the ApplicationSettingsItem class using the specified name
        /// </summary>	
        public ApplicationSettingsItem(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Constructs an instance of the ApplicationSettingsItem class using the specified name
        /// </summary>	
        public ApplicationSettingsItem(string name, object value)
        {
            this.name = name;
            this.value = value;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Object instance containing the current value</returns>
        public object GetValue()
        {
            return this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Boolean instance containing the current value</returns>
        public bool GetValueAsBoolean()
        {
            return (bool)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Byte instance containing the current value</returns>
        public byte GetValueAsByte()
        {
            return (byte)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Char instance containing the current value</returns>
        public char GetValueAsChar()
        {
            return (char)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Decimal instance containing the current value</returns>
        public decimal GetValueAsDecimal()
        {
            return (decimal)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Double instance containing the current value</returns>
        public double GetValueAsDouble()
        {
            return (double)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Single instance containing the current value</returns>
        public float GetValueAsFloat()
        {
            return (float)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Int32 instance containing the current value</returns>
        public int GetValueAsInt()
        {
            return (int)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Int64 instance containing the current value</returns>
        public long GetValueAsLong()
        {
            return (long)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.Int16 instance containing the current value</returns>
        public short GetValueAsShort()
        {
            return (short)this.value;
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <returns>An System.String instance containing the current value</returns>
        public string GetValueAsString()
        {
            return this.value as string;
        }

        /// <summary>
        /// Gets the current value of this application settings item. 
        /// If no value has been set, the current value is being set to the default value
        /// </summary>
        /// <param name="defaultValue">The default value of this application settings item</param>
        /// <returns>An System.Object instance containing the current value</returns>
        public object GetSetValue(object defaultValue)
        {
            this.value = defaultValue;
            return defaultValue;
        }

        /// <summary>
        /// Sets the value of the current application settings item 
        /// </summary>
        /// <param name="value">The value to set to the application settings item</param>
        public void SetValue(object value)
        {
            this.value = value;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the application settings item
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the current value of the application settings item
        /// </summary>
        public object Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /// <summary>
        /// Gets or sets the default value of the application settings item
        /// </summary>
        public object DefaultValue
        {
            get
            {
                return this.defaultValue;
            }
            set
            {
                this.defaultValue = value;
            }
        }

        #endregion

        #region Event handlers



        #endregion
    }
}
