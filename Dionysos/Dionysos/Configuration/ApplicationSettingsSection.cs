using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Configuration
{
    /// <summary>
    /// Abstract configuration class which represents an configuration section of an application. This class must be inherited.
    /// </summary>
    public class ApplicationSettingsSection
    {
        #region Fields

        private string name = string.Empty;
        private ApplicationSettingsItemCollection items = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ApplicationSettingsSection class
        /// </summary>	
        public ApplicationSettingsSection()
        {
            this.items = new ApplicationSettingsItemCollection();
        }

        /// <summary>
        /// Constructs an instance of the ApplicationSettingsSection class using the specified name
        /// </summary>	
        public ApplicationSettingsSection(string name)
        {
            this.name = name;
            this.items = new ApplicationSettingsItemCollection();
        }

        #endregion

        #region Methods

        private ApplicationSettingsItem GetApplicationSettingsItem(string sectionName)
        {
            ApplicationSettingsItem section = null;

            for (int i = 0; i < this.items.Count; i++)
            {
                ApplicationSettingsItem temp = this.items[i] as ApplicationSettingsItem;
                if (temp.Name == sectionName)
                {
                    section = temp;
                    break;
                }
            }

            return section;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the application settings section
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets the application settings items
        /// </summary>
        public ApplicationSettingsItemCollection Items
        {
            get
            {
                return this.items;
            }
        }

        /// <summary>
        /// Gets an Lynx-media.Configuration.ApplicationSettingsItem instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx-media.Configuration.ApplicationSettingsItem instance to get</param>
        /// <returns>An Lynx-media.Configuration.ApplicationSettingsItem instance</returns>
        public ApplicationSettingsItem this[int index]
        {
            get
            {
                return this.items[index] as ApplicationSettingsItem;
            }
        }

        /// <summary>
        /// Gets an Lynx-media.Configuration.ApplicationSettingsItem instance from the collection with the specified name
        /// </summary>
        /// <param name="name">The index of the Lynx-media.Configuration.ApplicationSettingsItem instance to get</param>
        /// <returns>An Lynx-media.Configuration.ApplicationSettingsItem instance</returns>
        public ApplicationSettingsItem this[string name]
        {
            get
            {
                return this.GetApplicationSettingsItem(name);
            }
        }

        #endregion

        #region Event handlers



        #endregion
    }
}
