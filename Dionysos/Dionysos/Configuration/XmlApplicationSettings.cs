using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Dionysos.Interfaces;
using System.IO;

namespace Dionysos.Configuration
{
    /// <summary>
    /// Configuration class which is used for reading/writing application configuration values
    /// </summary>
    public class XmlApplicationSettings : IApplicationSettings
    {
        #region Fields

        private const string defaultConfigFile = "AppSettings.xml";
        private ApplicationSettingsSectionCollection applicationSettingsSections = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Configuration.XmlApplicationSettings class
        /// </summary>	
        public XmlApplicationSettings()
        {
            this.applicationSettingsSections = new ApplicationSettingsSectionCollection();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the values from the default configuration file
        /// </summary>
        /// <returns>True if loading the values was succesful, False if not</returns>
        public bool Load()
        {
            return this.Load(defaultConfigFile);
        }

        /// <summary>
        /// Loads the values from a specified configuration file
        /// </summary>
        /// <param name="configFile">The path to the file to load the configuration from</param>
        /// <returns>True if loading the values was succesful, False if not</returns>
        public bool Load(string configFile)
        {
            if (File.Exists(configFile))
            {
                // Empty the current application settings
                this.applicationSettingsSections = new ApplicationSettingsSectionCollection();
                
                // Create and initialize a XmlDocument instance
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(configFile);

                XmlNode appSettingsNode = xmlDocument.FirstChild;

                // Walk through the application settings sections
                for (int i = 0; i < appSettingsNode.ChildNodes.Count; i++)
                {
                    XmlNode sectionNode = appSettingsNode.ChildNodes[i];
                    ApplicationSettingsSection section = new ApplicationSettingsSection(sectionNode.Name);

                    // Walk through the application settings items for a specific section
                    for (int j = 0; j < sectionNode.ChildNodes.Count; j++)
                    {
                        XmlNode itemNode = sectionNode.ChildNodes[j];
                        ApplicationSettingsItem item = new ApplicationSettingsItem(itemNode.Name, itemNode.InnerText);

                        section.Items.Add(item);
                    }

                    this.applicationSettingsSections.Add(section);
                }
            }
           
            return true;
        }

        /// <summary>
        /// Saves the values of the application settings instance to disk using the default configuration filename
        /// </summary>
        /// <returns>True if saving was succesful, False if not</returns>
        public bool Save()
        {
            return this.Save(defaultConfigFile);
        }

        /// <summary>
        /// Saves the values of the application settings instance to disk using the specified filename
        /// </summary>
        /// <param name="configFile">The path to write the configuration file to</param>
        /// <returns>True if saving was succesful, False if not</returns>
        public bool Save(string configFile)
        {
            // Create and initialize a XmlDocument instance
            XmlDocument xmlDocument = new XmlDocument();

            // Create the top node and append it to the XmlDocument instance
            XmlNode appSettingsNode = xmlDocument.CreateNode(XmlNodeType.Element, "ApplicationSettings", string.Empty);
            xmlDocument.AppendChild(appSettingsNode);

            // Walk through the application settings sections
            for (int i = 0; i < this.applicationSettingsSections.Count; i++)
            {
                ApplicationSettingsSection section = this.applicationSettingsSections[i];

                XmlNode sectionNode = xmlDocument.CreateNode(XmlNodeType.Element, section.Name.ToLower(), string.Empty);
                appSettingsNode.AppendChild(sectionNode);

                // Walk through the application settings items
                for (int j = 0; j < section.Items.Count; j++)
                {
                    ApplicationSettingsItem item = section.Items[j];

                    XmlNode itemNode = xmlDocument.CreateNode(XmlNodeType.Element, item.Name.ToLower(), string.Empty);
                    itemNode.InnerText = item.Value.ToString();
                    sectionNode.AppendChild(itemNode);
                }
            }

            // Save the settings file to disk
            xmlDocument.Save(configFile);
            
            return true;
        }

        /// <summary>
        /// Gets the current value of the specified application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Object instance containing the current value</returns>
        public object GetValue(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValue();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Boolean instance containing the current value</returns>
        public bool GetValueAsBoolean(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsBoolean();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Byte instance containing the current value</returns>
        public byte GetValueAsByte(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsByte();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Char instance containing the current value</returns>
        public char GetValueAsChar(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsChar();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Decimal instance containing the current value</returns>
        public decimal GetValueAsDecimal(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsDecimal();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Double instance containing the current value</returns>
        public double GetValueAsDouble(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsDouble();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Single instance containing the current value</returns>
        public float GetValueAsFloat(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsFloat();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Int32 instance containing the current value</returns>
        public int GetValueAsInt(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsInt();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Int64 instance containing the current value</returns>
        public long GetValueAsLong(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsLong();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Int16 instance containing the current value</returns>
        public short GetValueAsShort(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsShort();
        }

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.String instance containing the current value</returns>
        public string GetValueAsString(string section, string item)
        {
            return this.applicationSettingsSections[section].Items[item].GetValueAsString();
        }

        /// <summary>
        /// Gets the current value of the specifed application settings item. 
        /// If no value has been set, the current value is being set to the default value
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <param name="defaultValue">The default value of this application settings item</param>
        /// <returns>An System.Object instance containing the current value</returns>
        public object GetSetValue(string section, string item, object defaultValue)
        {
            return this.applicationSettingsSections[section].Items[item].GetSetValue(defaultValue);
        }

        /// <summary>
        /// Sets the value of the specified application settings item 
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <param name="value">The value to set to the application settings item</param>
        public void SetValue(string section, string item, object value)
        {
            this.applicationSettingsSections[section].Items[item].SetValue(value);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the collection which contains the ApplicationSettingsSection items
        /// </summary>
        public ApplicationSettingsSectionCollection Sections
        {
            get
            {
                return this.applicationSettingsSections;
            }
            set
            {
                this.applicationSettingsSections = value;
            }
        }

        /// <summary>
        /// Gets an ApplicationSettingsSection instance using the specified index
        /// </summary>
        /// <param name="index">The index of the ApplicationSettingsSection instance</param>
        /// <returns>An ApplicationSettingsSection instance containing the configuration values for that section</returns>
        public ApplicationSettingsSection this[int index]
        {
            get
            {
                return this.applicationSettingsSections[index];
            }
        }

        /// <summary>
        /// Gets an ApplicationSettingsSection instance using the specified section name
        /// </summary>
        /// <param name="name">The name of the ApplicationSettingsSection instance</param>
        /// <returns>An ApplicationSettingsSection instance containing the configuration values for that section</returns>
        public ApplicationSettingsSection this[string name]
        {
            get
            {
                return this.applicationSettingsSections[name];
            }
        }


        #endregion

        #region Event handlers



        #endregion
    }
}
