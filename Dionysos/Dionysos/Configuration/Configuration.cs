//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;

//namespace Dionysos.Configuration
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public class Configuration
//    {
//        #region Fields

//        //private ConfigurationSectionCollection configurationSections = null;

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Constructs an instance of the Dionysos.Configuration.Configuration class
//        /// </summary>	
//        public Configuration()
//        {
//            //this.configurationSections = new ConfigurationSectionCollection();
//        }

//        #endregion

//        #region Methods

//        ///// <summary>
//        ///// Loads the values from the default configuration file
//        ///// </summary>
//        ///// <returns>True if loading the values was succesful, False if not</returns>
//        //public bool Load()
//        //{
//        //    return Dionysos.Global.ConfigurationProvider.Load();
//        //}

//        ///// <summary>
//        ///// Saves the values of the application settings instance to disk using the default configuration filename
//        ///// </summary>
//        ///// <returns>True if saving was succesful, False if not</returns>
//        //public bool Save()
//        //{
//        //    return Dionysos.Global.ConfigurationProvider.Save();
//        //}

//        /// <summary>
//        /// Gets the current value of the specified application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Object instance containing the current value</returns>
//        public object GetValue(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValue(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Boolean instance containing the current value</returns>
//        public bool GetValueAsBoolean(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsBoolean(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Byte instance containing the current value</returns>
//        public byte GetValueAsByte(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsByte(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Char instance containing the current value</returns>
//        public char GetValueAsChar(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsChar(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Decimal instance containing the current value</returns>
//        public decimal GetValueAsDecimal(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsDecimal(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Double instance containing the current value</returns>
//        public double GetValueAsDouble(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsDouble(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Single instance containing the current value</returns>
//        public float GetValueAsFloat(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsFloat(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Int32 instance containing the current value</returns>
//        public int GetValueAsInt(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsInt(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Int64 instance containing the current value</returns>
//        public long GetValueAsLong(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsLong(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.Int16 instance containing the current value</returns>
//        public short GetValueAsShort(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsShort(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of this application settings item
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <returns>An System.String instance containing the current value</returns>
//        public string GetValueAsString(string section, string item)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetValueAsString(section, item);
//        }

//        /// <summary>
//        /// Gets the current value of the specifed application settings item. 
//        /// If no value has been set, the current value is being set to the default value
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <param name="defaultValue">The default value of this application settings item</param>
//        /// <returns>An System.Object instance containing the current value</returns>
//        public object GetSetValue(string section, string item, object defaultValue)
//        {
//            return Dionysos.Global.ConfigurationProvider.GetSetValue(section, item, defaultValue);
//        }

//        /// <summary>
//        /// Sets the value of the specified application settings item 
//        /// </summary>
//        /// <param name="section">The name of the section which contains the application settings item</param>
//        /// <param name="item">The name of the application settings item</param>
//        /// <param name="value">The value to set to the application settings item</param>
//        public void SetValue(string section, string item, object value)
//        {
//            Dionysos.Global.ConfigurationProvider.SetValue(section, item, value);
//        }

//        #endregion

//        //#region Properties

//        ///// <summary>
//        ///// Gets or sets the ConfigurationSectionCollection from this Configuration instance
//        ///// </summary>
//        //public ConfigurationSectionCollection Sections
//        //{
//        //    get
//        //    {
//        //        return this.configurationSections;
//        //    }
//        //    set
//        //    {
//        //        this.configurationSections = value;
//        //    }
//        //}

//        ///// <summary>
//        ///// Gets the ConfigurationSection instance for the specified index
//        ///// </summary>
//        ///// <param name="index">The index to get the ConfigurationSection instance for</param>
//        ///// <returns>A ConfigurationSection instance</returns>
//        //public ConfigurationSection this[int index]
//        //{
//        //    get
//        //    {
//        //        return this.configurationSections[index];
//        //    }
//        //}

//        ///// <summary>
//        ///// Gets the ConfigurationSection instance for the specified name
//        ///// </summary>
//        ///// <param name="name">The name of the ConfigurationSection instance to get</param>
//        ///// <returns>A ConfigurationSection instance</returns>
//        //public ConfigurationSection this[string name]
//        //{
//        //    get
//        //    {
//        //        return this.configurationSections[name];
//        //    }
//        //}

//        //#endregion
//    }
//}