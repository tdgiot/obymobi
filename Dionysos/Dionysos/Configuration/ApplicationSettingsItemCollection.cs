using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Configuration
{
	/// <summary>
	/// Collection class used for storing Lynx-media.Configuration.ApplicationSettingsItem instances in
	/// </summary>
	public class ApplicationSettingsItemCollection : ICollection<ApplicationSettingsItem>
	{
		#region Fields

		private ArrayList items;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the ApplicationSettingsItemCollection class
		/// </summary>	
		public ApplicationSettingsItemCollection()
		{
			this.items = new ArrayList();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds an Lynx-media.Configuration.ApplicationSettingsItem instance to the collection
		/// </summary>
		/// <param name="section">The Lynx-media.Configuration.ApplicationSettingsItem instance to add to the collection</param>
		public void Add(ApplicationSettingsItem section)
		{
			this.items.Add(section);
		}

		/// <summary>
		/// Clears all the items in the collection
		/// </summary>
		public void Clear()
		{
			this.items.Clear();
		}

		/// <summary>
		/// Checks whether the specified Lynx-media.Configuration.ApplicationSettingsItem instance is already in the collection
		/// </summary>
		/// <param name="section">The Lynx-media.Configuration.ApplicationSettingsItem instance to check</param>
		/// <returns>True if the Lynx-media.Configuration.ApplicationSettingsItem instance is in the collection, False if not</returns>
		public bool Contains(ApplicationSettingsItem section)
		{
			bool contains = false;

			for (int i = 0; i < this.items.Count; i++)
			{
				if ((this.items[i] as ApplicationSettingsItem) == section)
				{
					contains = true;
				}
			}

			return contains;
		}

		/// <summary>
		/// Copies the items from this collection to an array at the specified index
		/// </summary>
		/// <param name="array">The array to copy the items to</param>
		/// <param name="index">The index to copy the items at</param>
		public void CopyTo(ApplicationSettingsItem[] array, int index)
		{
			this.items.CopyTo(array, index);
		}

		/// <summary>
		/// Removes the specified Lynx-media.Configuration.ApplicationSettingsItem instance from this collection
		/// </summary>
		/// <param name="section">The Lynx-media.Configuration.ApplicationSettingsItem instance to remove</param>
		public bool Remove(ApplicationSettingsItem section)
		{
			this.items.Remove(section);
			return true;
		}

		/// <summary>
		/// Returns an enumerator for a range of elements in the Lynx-media.Windows.Forms.Collections.ControlCollection
		/// </summary>
		/// <returns>The enumerator instance</returns>
		public IEnumerator<ApplicationSettingsItem> GetEnumerator()
		{
			return (IEnumerator<ApplicationSettingsItem>)this.items.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for a range of elements in the Lynx-media.Windows.Forms.Collections.ControlCollection
		/// </summary>
		/// <returns>The enumerator instance</returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private ApplicationSettingsItem GetApplicationSettingsItem(string itemName)
		{
			ApplicationSettingsItem item = null;

			for (int i = 0; i < this.items.Count; i++)
			{
				ApplicationSettingsItem temp = this.items[i] as ApplicationSettingsItem;
				if (temp.Name == itemName)
				{
					item = temp;
					break;
				}
			}

			if (item == null)
			{
				// No item found, so create a new one
				item = new ApplicationSettingsItem(itemName);
				this.items.Add(item);
			}

			return item;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the amount of items in this collection
		/// </summary>
		public int Count
		{
			get
			{
				return this.items.Count;
			}
		}

		/// <summary>
		/// Boolean value indicating whether this collection is read only or not
		/// </summary>
		public bool IsReadOnly
		{
			get
			{
				return this.items.IsReadOnly;
			}
		}

		/// <summary>
		/// Gets an Lynx-media.Configuration.ApplicationSettingsItem instance from the collection from the specified index
		/// </summary>
		/// <param name="index">The index of the Lynx-media.Configuration.ApplicationSettingsItem instance to get</param>
		/// <returns>An Lynx-media.Configuration.ApplicationSettingsItem instance</returns>
		public ApplicationSettingsItem this[int index]
		{
			get
			{
				return this.items[index] as ApplicationSettingsItem;
			}
		}

		/// <summary>
		/// Gets an Lynx-media.Configuration.ApplicationSettingsItem instance from the collection with the specified name
		/// </summary>
		/// <param name="name">The index of the Lynx-media.Configuration.ApplicationSettingsItem instance to get</param>
		/// <returns>An Lynx-media.Configuration.ApplicationSettingsItem instance</returns>
		public ApplicationSettingsItem this[string name]
		{
			get
			{
				return this.GetApplicationSettingsItem(name);
			}
		}

		#endregion
	}
}
