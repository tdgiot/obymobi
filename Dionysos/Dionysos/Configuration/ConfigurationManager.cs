using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Configuration;
using Dionysos.Interfaces;

namespace Dionysos
{
	/// <summary>
	/// Static wrapper class for Global.ConfigurationProvider.
	/// </summary>
	public static class ConfigurationManager
	{
		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		[Obsolete("Use GetString instead.")]
		public static string GetValue(string name)
		{
			return GlobalLight.ConfigurationProvider.GetValue(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		[Obsolete("Use GetString instead.")]
		public static string GetValue(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetValue(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		[Obsolete("Use GetStringRefreshed instead.")]
		public static string GetValueRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetValueRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		[Obsolete("Use GetStringRefreshed instead.")]
		public static string GetValueRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetValueRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static bool GetBool(string name)
		{
			return GlobalLight.ConfigurationProvider.GetBool(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static bool GetBool(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetBool(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static bool GetBoolRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetBoolRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static bool GetBoolRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetBoolRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static byte GetByte(string name)
		{
			return GlobalLight.ConfigurationProvider.GetByte(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static byte GetByte(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetByte(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static byte GetByteRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetByteRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static byte GetByteRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetByteRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static char GetChar(string name)
		{
			return GlobalLight.ConfigurationProvider.GetChar(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static char GetChar(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetChar(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static char GetCharRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetCharRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static char GetCharRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetCharRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static DateTime GetDateTime(string name)
		{
			return GlobalLight.ConfigurationProvider.GetDateTime(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static DateTime GetDateTime(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetDateTime(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static DateTime GetDateTimeRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetDateTimeRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static DateTime GetDateTimeRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetDateTimeRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static decimal GetDecimal(string name)
		{
			return GlobalLight.ConfigurationProvider.GetDecimal(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static decimal GetDecimal(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetDecimal(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static decimal GetDecimalRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetDecimalRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static decimal GetDecimalRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetDecimalRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static double GetDouble(string name)
		{
			return GlobalLight.ConfigurationProvider.GetDouble(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static double GetDouble(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetDouble(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static double GetDoubleRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetDoubleRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static double GetDoubleRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetDoubleRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static float GetFloat(string name)
		{
			return GlobalLight.ConfigurationProvider.GetFloat(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static float GetFloat(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetFloat(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static float GetFloatRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetFloatRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static float GetFloatRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetFloatRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static int GetInt(string name)
		{
			return GlobalLight.ConfigurationProvider.GetInt(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static int GetInt(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetInt(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static int GetIntRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetIntRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static int GetIntRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetIntRefreshed(name, applicationSpecific);
		}

        public static List<int> GetIntegerList(string name)
        {
            string integerListAsString = GlobalLight.ConfigurationProvider.GetString(name);

			string[] revenueCenters = integerListAsString.Split(',');

            return revenueCenters.Select(r => int.Parse(r)).ToList();
		}

        /// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static long GetLong(string name)
		{
			return GlobalLight.ConfigurationProvider.GetLong(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static long GetLong(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetLong(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static long GetLongRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetLongRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static long GetLongRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetLongRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static short GetShort(string name)
		{
			return GlobalLight.ConfigurationProvider.GetShort(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static short GetShort(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetShort(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static short GetShortRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetShortRefreshed(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static short GetShortRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetShortRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static string GetString(string name)
		{
			return GlobalLight.ConfigurationProvider.GetString(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public static string GetString(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetString(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.String instance containing the current value</returns>
		public static string GetStringRefreshed(string name)
		{
			return GlobalLight.ConfigurationProvider.GetStringRefreshed(name);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public static string GetStringRefreshed(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetStringRefreshed(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		public static string GetPassword(string name)
		{
			return GetPassword(name, false);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.String instance containing the current value</returns>
		public static string GetPassword(string name, bool applicationSpecific)
		{
			return GlobalLight.ConfigurationProvider.GetPassword(name, applicationSpecific);
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		public static void SetValue(string name, object value)
		{
			GlobalLight.ConfigurationProvider.SetValue(name, value);
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		public static void SetValue(string name, object value, bool applicationSpecific)
		{
			GlobalLight.ConfigurationProvider.SetValue(name, value, applicationSpecific);
		}

		/// <summary>
		/// Sets the password.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		public static void SetPassword(string name, string value)
		{
			SetPassword(name, value, false);
		}
		
		/// <summary>
		/// Sets the password.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		public static void SetPassword(string name, string value, bool applicationSpecific)
		{
            GlobalLight.ConfigurationProvider.SetPassword(name, value, applicationSpecific);
		}

		/// <summary>
		/// Get's if the setting is defined for the current running application.
		/// This might not be the case when a certain set of ConfigInfo is not initialized in the application start.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>
		/// bool
		/// </returns>
		public static bool IsDefined(string name)
		{
			return GlobalLight.ConfigurationProvider.IsDefined(name);
		}
	}
}