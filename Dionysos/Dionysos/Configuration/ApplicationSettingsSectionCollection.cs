using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Configuration
{
    /// <summary>
    /// Collection class used for storing Lynx-media.Configuration.ApplicationSettingsSection instances in
    /// </summary>
    public class ApplicationSettingsSectionCollection : ICollection<ApplicationSettingsSection>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ApplicationSettingsSectionCollection class
        /// </summary>	
        public ApplicationSettingsSectionCollection()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Lynx-media.Configuration.ApplicationSettingsSection instance to the collection
        /// </summary>
        /// <param name="section">The Lynx-media.Configuration.ApplicationSettingsSection instance to add to the collection</param>
        public void Add(ApplicationSettingsSection section)
        {
            this.items.Add(section);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Lynx-media.Configuration.ApplicationSettingsSection instance is already in the collection
        /// </summary>
        /// <param name="section">The Lynx-media.Configuration.ApplicationSettingsSection instance to check</param>
        /// <returns>True if the Lynx-media.Configuration.ApplicationSettingsSection instance is in the collection, False if not</returns>
        public bool Contains(ApplicationSettingsSection section)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as ApplicationSettingsSection) == section)
                {
                    contains = true;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(ApplicationSettingsSection[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Lynx-media.Configuration.ApplicationSettingsSection instance from this collection
        /// </summary>
        /// <param name="section">The Lynx-media.Configuration.ApplicationSettingsSection instance to remove</param>
        public bool Remove(ApplicationSettingsSection section)
        {
            this.items.Remove(section);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Windows.Forms.Collections.ControlCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<ApplicationSettingsSection> GetEnumerator()
        {
            return (IEnumerator<ApplicationSettingsSection>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Windows.Forms.Collections.ControlCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private ApplicationSettingsSection GetApplicationSettingsSection(string sectionName)
        {
            ApplicationSettingsSection section = null;
            
            for (int i = 0; i < this.items.Count; i++)
            {
                ApplicationSettingsSection temp = this.items[i] as ApplicationSettingsSection;
                if (temp.Name == sectionName)
                {
                    section = temp;
                    break;
                }
            }

            if (section == null)
            {
                // No section found, so create a new one
                section = new ApplicationSettingsSection(sectionName);
                this.items.Add(section);
            }

            return section;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Lynx-media.Configuration.ApplicationSettingsSection instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx-media.Configuration.ApplicationSettingsSection instance to get</param>
        /// <returns>An Lynx-media.Configuration.ApplicationSettingsSection instance</returns>
        public ApplicationSettingsSection this[int index]
        {
            get
            {
                return this.items[index] as ApplicationSettingsSection;
            }
        }

        /// <summary>
        /// Gets an Lynx-media.Configuration.ApplicationSettingsSection instance from the collection with the specified name
        /// </summary>
        /// <param name="name">The index of the Lynx-media.Configuration.ApplicationSettingsSection instance to get</param>
        /// <returns>An Lynx-media.Configuration.ApplicationSettingsSection instance</returns>
        public ApplicationSettingsSection this[string name]
        {
            get
            {
                return this.GetApplicationSettingsSection(name);
            }
        }

        #endregion
    }
}
