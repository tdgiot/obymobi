using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Logging
{
    public class LogItem
    {
        public LogItem()
        {
            this.Level = LoggingLevel.Verbose;
            this.Text = string.Empty;
        }

        public LogItem(LoggingLevel level, string text)
        {
            this.Level = level;
            this.Text = text;
        }

        public LoggingLevel Level { get; set; }
        public string Text { get; set; }
    }
}
