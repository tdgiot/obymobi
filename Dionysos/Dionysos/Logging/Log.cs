using Dionysos;
using Dionysos.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

// GK In system, because you want to use this everywhere.
namespace System
{
    public class Log
    {
        public static void LogWithLevel(LoggingLevel level, string className, string methodName, string text, params object[] args)
        {
            string log = Log.prepareLogText(className, methodName, text, args);

            GlobalLight.LoggingProvider.Log(level, log);
        }

        public static void Info(string className, string methodName, string text, params object[] args)
        {
            Log.Info(true, className, methodName, text, args);
        }

        public static void Info(bool logToFile, string className, string methodName, string text, params object[] args)
        {
            string log = Log.prepareLogText(className, methodName, text, args);

			Log.WriteToApplicationOutput("INFORMATION: " + log);                       

			if (logToFile && !Log.SuppressFileOutput)
                GlobalLight.LoggingProvider.Log(LoggingLevel.Info, log);
        }

        public static void Debug(string className, string methodName, string text, params object[] args)
        {
            Log.Debug(true, className, methodName, text, args);
        }

        public static void Debug(bool logToFile, string className, string methodName, string text, params object[] args)
        {
            string log = Log.prepareLogText(className, methodName, text, args);

			Log.WriteToApplicationOutput(log);                       

			if (logToFile && !Log.SuppressFileOutput)
                GlobalLight.LoggingProvider.Log(LoggingLevel.Debug, log);
        }

        public static void Warning(string className, string methodName, string text, params object[] args)
        {
            Log.Warning(true, className, methodName, text, args);
        }

        public static void Warning(bool logToFile, string className, string methodName, string text, params object[] args)
        {
            string log = Log.prepareLogText(className, methodName, text, args);

			Log.WriteToApplicationOutput("WARNING: " + log);                       

			if (logToFile && !Log.SuppressFileOutput)
                GlobalLight.LoggingProvider.Log(LoggingLevel.Warning, log);
        }

        public static void Error(string className, string methodName, string text, params object[] args)
        {
            Log.Error(true, className, methodName, text, args);
        }

        public static void Error(bool logToFile, string className, string methodName, string text, params object[] args)
        {
            string log = Log.prepareLogText(className, methodName, text, args);
			            
			Log.WriteToApplicationOutput("ERROR: " + log);

			if (logToFile && !Log.SuppressFileOutput)
                GlobalLight.LoggingProvider.Log(LoggingLevel.Error, log);
        }

        public static void Verbose(string className, string methodName, string text, params object[] args)
        {
            Verbose(true, className, methodName, text, args);
        }

        public static void Verbose(bool logToFile, string className, string methodName, string text, params object[] args)
        {
            string log = Log.prepareLogText(className, methodName, text, args);

			Log.WriteToApplicationOutput(log);

            if (logToFile && !Log.SuppressFileOutput)
                GlobalLight.LoggingProvider.Log(LoggingLevel.Verbose, Log.prepareLogText(className, methodName, text, args));
        }

        private static string prepareLogText(string className, string methodName, string text, params object[] args)
        {
            string dateTime = DateTime.Now.ToString("MM-dd HH:mm:ss");
			return StringUtilLight.FormatSafe("[{0}] {1}.{2}: {3}", dateTime, className, methodName, string.Format(text, args));
        }

		private static void WriteToApplicationOutput(string text)
		{
			// GK Done this because of:
			// http://stackoverflow.com/questions/18086112/xamarin-studio-why-all-the-whitespace-in-debug-writeline-output-after-update
			#if DEBUG

			if (!Log.SuppressApplicationOutput)
				Console.WriteLine(text);

			#endif
		}

        public static bool SuppressApplicationOutput { get; set; }
		public static bool SuppressFileOutput { get; set; }
    }
}
