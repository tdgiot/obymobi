using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dionysos.Logging
{
    public abstract class TypedAsyncLoggingProviderBase<TItemToLog> : TypedLoggingProviderBase<TItemToLog> where TItemToLog : LogItem
    {
        private BlockingCollection<TItemToLog> logQueue = new BlockingCollection<TItemToLog>();
		private DateTime lastHistoryCleanupUtc = DateTime.MinValue;
		protected int daysOfHistory = 7;

        public TypedAsyncLoggingProviderBase()
        {
            // Init consumer thread            
            Task.Factory.StartNew(() => this.ProcessQueue());
        }   

        public void Log(LoggingLevel level, TItemToLog itemToLog)
        {            
            this.logQueue.Add(itemToLog);
        }

        public override void Log(LoggingLevel level, string text)
        {
            this.Log(level, this.TextToLogItem(level, text));
        }

        /// <summary>
        /// To stay consistent with the logic of logging strings we need this conversion.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public abstract TItemToLog TextToLogItem(LoggingLevel level, string text);

        private void ProcessQueue()
        {
            while (true)
            {
                var entry = this.logQueue.Take();
                try
                {
                    this.PersistToLog(this.GetFilename(), entry);

                    // Clean up history once an hour
                    if ((DateTime.UtcNow - this.lastHistoryCleanupUtc).TotalMinutes > 60)
                    {
                        this.CleanupHistory();
                        this.lastHistoryCleanupUtc = DateTime.UtcNow;
                    }
                }
                catch
                {
                    Trace.TraceError("Persisting to log failed: {0}", entry.Text);
                }
            }
        }

        protected abstract void PersistToLog(string logfileName, TItemToLog entry);

		protected abstract void CleanupHistory();

        /// <summary>
        /// Gets the filename.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns></returns>
        public string GetFilename()
        {
            DateTime now = DateTime.Now;
            return string.Format("{0}{1}{2}.log", now.Year.ToString("0000"), now.Month.ToString("00"), now.Day.ToString("00"));
        }      
    }
}
