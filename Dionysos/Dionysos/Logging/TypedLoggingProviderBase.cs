using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Logging
{
    public abstract class TypedLoggingProviderBase<TItemToLog> : LoggingProviderBase, ITypedLoggingProvider<TItemToLog> where TItemToLog : LogItem
    {
    }
}
