﻿using Serilog;
using Serilog.Events;
using System;
namespace Dionysos.Logging
{
    public class SerilogProvider : IMultiLoggingProvider
    {
        private readonly ILogger logger;
        private readonly string applicationName;

        public SerilogProvider(ILogger logger, string applicationName)
        {
            this.logger = logger;
            this.applicationName = applicationName;
        }
        public void Debug(Enum type, string text, params object[] args)
        {
            Log(type, LoggingLevel.Debug, text, args);
        }
        public void Debug(string format, params object[] args)
        {
            Log(LoggingLevel.Debug, format, args);
        }
        public void Error(Enum type, string text, params object[] args)
        {
            Log(type, LoggingLevel.Error, text, args);
        }
        public void Error(string format, params object[] args)
        {
            Log(LoggingLevel.Error, format, args);
        }
        public void Information(Enum type, string text, params object[] args)
        {
            Log(type, LoggingLevel.Info, text, args);
        }
        public void Information(string format, params object[] args)
        {
            Log(LoggingLevel.Info, format, args);
        }
        public void Verbose(Enum type, string text, params object[] args)
        {
            Log(type, LoggingLevel.Verbose, text, args);
        }
        public void Verbose(string format, params object[] args)
        {
            Log(LoggingLevel.Verbose, format, args);
        }
        public void Warning(Enum type, string text, params object[] args)
        {
            Log(type, LoggingLevel.Warning, text, args);
        }
        public void Warning(string format, params object[] args)
        {
            Log(LoggingLevel.Warning, format, args);
        }
        public void Log(LoggingLevel level, string text, params object[] args)
        {
            Log(null, level, text, args);
        }
        public void Log(Enum type, LoggingLevel level, string text, params object[] args)
        {
            LogEventLevel logEventLevel = ConvertCraveLoggingLevelToSerilogLevel(level);
            string prefix = type != null
                ? type.ToString()
                : applicationName;
            text = $"{prefix}({System.Diagnostics.Process.GetCurrentProcess().Id}): {text}";
            logger.Write(logEventLevel, text, args);
        }
        private static LogEventLevel ConvertCraveLoggingLevelToSerilogLevel(LoggingLevel loggingLevel)
        {
            switch (loggingLevel)
            {
                case LoggingLevel.Debug:
                    return LogEventLevel.Debug;
                case LoggingLevel.Verbose:
                    return LogEventLevel.Verbose;
                case LoggingLevel.Info:
                    return LogEventLevel.Information;
                case LoggingLevel.Warning:
                    return LogEventLevel.Warning;
                case LoggingLevel.Error:
                    return LogEventLevel.Error;
                default:
                    return LogEventLevel.Information;
            }
        }
    }
}