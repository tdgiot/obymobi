using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Dionysos.Logging
{
    public class AsyncWebAppLoggingProvider : IMultiLoggingProvider
    {
        private readonly BlockingCollection<LogEntry> logQueue;
        private readonly Dictionary<Enum, string> logTypes;

        private readonly string baseLoggingPath;
        private readonly int daysOfHistory;
        private readonly string applicationName;

        private DateTime lastHistoryCleanupUtc;

        public AsyncWebAppLoggingProvider(string basePath, string applicationName, int daysOfHistory)
        {
            this.logQueue = new BlockingCollection<LogEntry>();
            this.logTypes = new Dictionary<Enum, string>();

            this.baseLoggingPath = basePath;
            this.daysOfHistory = daysOfHistory;
            this.applicationName = applicationName;

            this.lastHistoryCleanupUtc = DateTime.UtcNow;

            if (!Directory.Exists(this.baseLoggingPath))
            {
                Directory.CreateDirectory(this.baseLoggingPath);
            }

            Task.Factory.StartNew(this.ProcessQueue, TaskCreationOptions.LongRunning);
        }

        public void AddLogType(Enum type)
        {
            if (!this.logTypes.ContainsKey(type))
            {
                this.logTypes.Add(type, type.ToString());
            }
        }

        public void Log(Enum type, LoggingLevel level, string text, params object[] args)
        {
            string prefix;
            if (type == null)
            {
                prefix = this.applicationName;
            }
            else if (!this.logTypes.TryGetValue(type, out prefix))
            {
                AddLogType(type);
                prefix = type.ToString();
            }

            var message = text.FormatSafe(args);
            var entry = new LogEntry(prefix, level, message);
            this.logQueue.Add(entry);
        }

        public void Verbose(Enum type, string text, params object[] args)
        {
            this.Log(type, LoggingLevel.Verbose, text, args);
        }

        public void Debug(Enum type, string text, params object[] args)
        {
            this.Log(type, LoggingLevel.Debug, text, args);
        }

        public void Information(Enum type, string text, params object[] args)
        {
            this.Log(type, LoggingLevel.Info, text, args);
        }

        public void Warning(Enum type, string text, params object[] args)
        {
            this.Log(type, LoggingLevel.Warning, text, args);
        }

        public void Error(Enum type, string text, params object[] args)
        {
            this.Log(type, LoggingLevel.Error, text, args);
        }

        public void Log(LoggingLevel level, string text, params object[] args)
        {
            Log(null, level, text, args);
        }

        public void Verbose(string format, params object[] args)
        {
            this.Log(null, LoggingLevel.Verbose, format, args);
        }

        public void Debug(string format, params object[] args)
        {
            this.Log(null, LoggingLevel.Debug, format, args);
        }

        public void Information(string format, params object[] args)
        {
            this.Log(null, LoggingLevel.Info, format, args);
        }

        public void Warning(string format, params object[] args)
        {
            this.Log(null, LoggingLevel.Warning, format, args);
        }

        public void Error(string format, params object[] args)
        {
            this.Log(null, LoggingLevel.Error, format, args);
        }

        #region Processing        
        private void ProcessQueue()
        {
            while (true)
            {
                // Take will wait until there's something to do
                LogEntry entry = null;
                try
                {
                    entry = null;
                    entry = logQueue.Take();

                    // Write to log
                    WriteToFile(entry);

                    // Clean up history once an hour
                    if ((DateTime.UtcNow - this.lastHistoryCleanupUtc).TotalMinutes > 60)
                    {
                        this.CleanupHistory();
                    }
                }
                catch (Exception ex)
                {
                    if (ex is ThreadAbortException)
                    {
                        return;
                    }

                    try
                    {
                        // Log Issue                     
                        LogEntry le = new LogEntry("AsyncWebAppLoggingProvider", LoggingLevel.Error, "Failed to log: {0}\r\n{1}".FormatSafe(ex.Message, ex.StackTrace));
                        WriteToFile(le);
                    }
                    catch
                    {
                        // Suck the exception up.
                        if (TestUtil.IsPcDeveloper)
                            throw;
                    }
                }
            }
        }

        private void WriteToFile(LogEntry entry)
        {
            string filepath = Path.Combine(this.baseLoggingPath, this.GetFilename(entry));

            using (StreamWriter writer = File.AppendText(filepath))
            {
                // Write as LogCat
                entry.Text = entry.Text.Replace("\r\n", "\\r\\n").Replace("\n", "\\n").Replace("\r", "\\r");

                writer.Write(string.Format("{0:MM-dd HH:mm:ss.fff}: {1}/{2}({3}): {4}", DateTime.Now, entry.LevelCharacter, entry.FilePrefix, System.Diagnostics.Process.GetCurrentProcess().Id, entry.Text));
                writer.Write("\n");
            }
        }

        private void CleanupHistory()
        {
            // Clean up
            foreach (string logType in this.logTypes.Values)
            {
                var files = Directory.GetFiles(this.baseLoggingPath, logType + "-*");
                foreach (var file in files)
                {
                    var fi = new FileInfo(file);
                    if ((DateTime.Now - fi.LastWriteTime).Days > this.daysOfHistory)
                    {
                        fi.Delete();
                    }
                }
            }

            this.lastHistoryCleanupUtc = DateTime.UtcNow;
        }

        /// <summary>
        /// Gets the filename for log entry
        /// </summary>
        /// <param name="entry">Log entry.</param>
        /// <returns></returns>
        private string GetFilename(LogEntry entry)
        {
            return string.Format("{0}-{1:yyyyMMdd}.log", entry.FilePrefix, DateTime.Today);
        }

        private class LogEntry
        {
            private readonly LoggingLevel level;

            public string FilePrefix { get; private set; }
            public string Text { get; set; }

            public LogEntry(string filePrefix, LoggingLevel level, string text)
            {
                this.level = level;

                this.FilePrefix = filePrefix;
                this.Text = text;
            }

            public char LevelCharacter
            {
                get
                {
                    switch (this.level)
                    {
                        case LoggingLevel.Debug:
                            return 'D';
                        case LoggingLevel.Verbose:
                            return 'V';
                        case LoggingLevel.Info:
                            return 'I';
                        case LoggingLevel.Warning:
                            return 'W';
                        case LoggingLevel.Error:
                            return 'E';
                        default:
                            return 'I';
                    }
                }
            }
        }

        #endregion
    }
}
