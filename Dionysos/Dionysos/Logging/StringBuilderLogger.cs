﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Logging
{
    public class StringBuilderLogger : LoggingProviderBase
    {
        public StringBuilder StringBuilder { get; private set; }

        public StringBuilderLogger()
        {
            this.StringBuilder = new StringBuilder();
        }

        public StringBuilderLogger(StringBuilder stringBuilder)
        {
            this.StringBuilder = stringBuilder;
        }

        public override void Log(LoggingLevel level, string text)
        {
            this.StringBuilder.AppendLine(text);
        }
    }
}
