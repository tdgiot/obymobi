using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Logging
{
    public class BlackHoleLogger : LoggingProviderBase
    {
        public override void Log(LoggingLevel level, string text)
        {
            this.YouWillNeverEscape(level, text);
        }

        private void YouWillNeverEscape(LoggingLevel level, string text)
        {
            // Nothing can escape now.
            if (TestUtil.IsPcDeveloper)
                System.Diagnostics.Debug.WriteLine(level.ToString() + " " + text);
            return;
        } 
    }
}
