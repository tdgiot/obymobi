using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Logging
{
    public interface ITypedLoggingProvider<TItemToLog> : ILoggingProvider where TItemToLog : LogItem
    {
    }
}
