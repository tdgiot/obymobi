using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dionysos;

namespace Dionysos.Logging
{
    public class AsyncLoggingProvider : LoggingProviderBase
    {
        private DateTime lastHistoryCleanup = DateTime.MinValue;
        private string logPath;
        private string logFilenamePrefix;
        private string applicationName;        
        private int daysOfHistory;
        private BlockingCollection<LogEntry> logQueue = new BlockingCollection<LogEntry>();

        public AsyncLoggingProvider(string logPath, string logFilenamePrefix, int daysOfHistory, string applicationName)
        {
            if (daysOfHistory < 1)
                throw new InvalidDataException("Days of history must be at least 1");

            this.logPath = logPath;
            this.logFilenamePrefix = logFilenamePrefix;
            this.daysOfHistory = daysOfHistory;
            this.applicationName = applicationName;
           
            // Init consumer thread            
            Task.Factory.StartNew(() => this.ProcessQueue(), TaskCreationOptions.LongRunning); // Added LongRunning 18-08-2014 - If we get issues now than this might be the cause.
        }       

        public override void Log(LoggingLevel level, string text)
        {
            LogEntry entry = new LogEntry(level, text);
            this.logQueue.Add(entry);
        }                

        private void ProcessQueue()
        {
            while (true)
            {
                LogEntry entry = null;
                try
                {
                    // Take will wait until there's something to do
                    entry = this.logQueue.Take();

                    if (entry == null)
                        continue;

                    // Write to log
                    this.PersistToLog(entry);

                    // Clean up history once an hour
                    if ((DateTime.UtcNow - this.lastHistoryCleanup).TotalMinutes > 60)
                    {
                        this.CleanupHistory();
                    }
                }
                catch (Exception)
                {
                    if (TestUtil.IsPcDeveloper)
                        throw;
                    /*try
                    {
                        // Log Issue                     
                        LogEntry le = new LogEntry(LoggingLevel.Error, "Failed to log: {0}\r\n{1}".FormatSafe(ex.Message, ex.StackTrace));
                        this.PersistToLog(le, "AsyncLoggingProvider");
                    }
                    catch
                    {
                        // Suck the exception up.
                        if (TestUtil.IsPcDeveloper)
                        {
                            // Suck the exception up.
                            if (TestUtil.IsPcDeveloper)
                                throw;
                        }
                    }*/                  
                }  
            }
        }

        protected virtual void PersistToLog(LogEntry entry, string fileNameprefix = "")
        {
            string filepath = Path.Combine(this.logPath, this.GetFilename(fileNameprefix));

            using (StreamWriter writer = File.AppendText(filepath))
            {
                // Write as LogCat
                entry.Text = entry.Text.Replace("\r\n", "\\r\\n").Replace("\n", "\\n").Replace("\r", "\\r");

                writer.Write(string.Format("{0:MM-dd HH:mm:ss.fff}: {1}/{2}({3}): {4}", DateTime.Now, entry.LevelCharacter, this.applicationName, System.Diagnostics.Process.GetCurrentProcess().Id, entry.Text));
                writer.Write("\n");
           }        
        }

        protected virtual void CleanupHistory()
        {
            // Clean up
            var files = Directory.GetFiles(this.logPath, this.logFilenamePrefix + "-*");
            foreach (var file in files)
            {
                FileInfo fi = new FileInfo(file);
                if ((DateTime.Now - fi.LastWriteTime).Days > this.daysOfHistory)
                    File.Delete(file);
            }

            this.lastHistoryCleanup = DateTime.UtcNow;
        }

        /// <summary>
        /// Gets the filename.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns></returns>
        public string GetFilename(string fileNameprefix = "")
        {            
            if(fileNameprefix.IsNullOrWhiteSpace())
                return string.Format("{0}-{1:yyyyMMdd}.log", this.logFilenamePrefix, DateTime.Now);
            else
                return string.Format("{0}-{1:yyyyMMdd}.log", fileNameprefix, DateTime.Now);
        }

        protected class LogEntry
        {
            public LogEntry(LoggingLevel level, string text)
            {
                this.Level = level;
                this.Text = text;
            }
            public string Text { get; set; }
            public LoggingLevel Level { get; set; }
            public char LevelCharacter
            {
                get
                {
                    switch (this.Level)
                    {
                        case LoggingLevel.Debug:
                            return 'D';
                        case LoggingLevel.Verbose:                            
                            return 'V';
                        case LoggingLevel.Info:
                            return 'I';
                        case LoggingLevel.Warning:
                            return 'W';
                        case LoggingLevel.Error:
                            return 'E';
                        default:
                            return 'I';
                    }
                }
            }
        }
    }
}
