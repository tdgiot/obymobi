using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos // GK I Would prefer it in .Logging, but for backward compatibability it isn't.. 
{
    /// <summary>
    /// The logging level.
    /// </summary>
    public enum LoggingLevel : int
    {
        /// <summary>
        /// Extended logging, which means that everything is logged
        /// </summary>
        [StringValue("Verbose logging.")]
        Debug = 400,

        /// <summary>
        /// Extended logging, which means that everything is logged
        /// </summary>
        [StringValue("Debug logging.")]
        Verbose = 300,

        /// <summary>
        /// Normal logging, which means that only changes in state are logged 
        /// </summary>
        [StringValue("Info logging.")]
        Info = 200,

        /// <summary>
        /// Extended logging, which means that everything is logged
        /// </summary>
        [StringValue("Warning logging.")]
        Warning = 100,

        /// <summary>
        /// Extended logging, which means that everything is logged
        /// </summary>
        [StringValue("Error logging.")]
        Error = 0
    }
}
