using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Logging
{
    public abstract class LoggingProviderBase : ILoggingProvider
    {
        public abstract void Log(LoggingLevel level, string text);

        public void Log(LoggingLevel level, string text, params object[] args)
        { 
            this.Log(level, text.FormatSafe(args));
        }

        public void Verbose(string text)
        {
            this.Log(LoggingLevel.Verbose, text);
        }

        public void Debug(string text)
        {
            this.Log(LoggingLevel.Debug, text);
        }

        public void Information(string text)
        {
            this.Log(LoggingLevel.Info, text);
        }

        public void Warning(string text)
        {
            this.Log(LoggingLevel.Warning, text);
        }

        public void Error(string text)
        {
            this.Log(LoggingLevel.Error, text);
        }

        public void Verbose(string format, params object[] args)
        {
            this.Log(LoggingLevel.Verbose, format, args);
        }

        public void Debug(string format, params object[] args)
        {
            this.Log(LoggingLevel.Debug, format, args);
        }

        public void Information(string format, params object[] args)
        {
            this.Log(LoggingLevel.Info, format, args);
        }

        public void Warning(string format, params object[] args)
        {
            this.Log(LoggingLevel.Warning, format, args);
        }

        public void Error(string format, params object[] args)
        {
            this.Log(LoggingLevel.Error, format, args);
        }
}
}
