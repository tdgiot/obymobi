using System;

namespace Dionysos.Logging
{
    public interface IMultiLoggingProvider : ILoggingProvider
    {
        void Log(Enum type, LoggingLevel level, string text, params object[] args);

        void Verbose(Enum type, string text, params object[] args);

        void Debug(Enum type, string text, params object[] args);

        void Information(Enum type, string text, params object[] args);

        void Warning(Enum type, string text, params object[] args);

        void Error(Enum type, string text, params object[] args);
    }
}
