namespace Dionysos.Logging
{
    public interface ILoggingProvider
    {
        void Log(LoggingLevel level, string text, params object[] args);

        void Verbose(string format, params object[] args);

        void Debug(string format, params object[] args);

        void Information(string format, params object[] args);

        void Warning(string format, params object[] args);

        void Error(string format, params object[] args);        
    }
}
