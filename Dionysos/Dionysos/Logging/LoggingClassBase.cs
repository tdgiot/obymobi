using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos // GK I Would prefer it in .Logging, but for backward compatibability it isn't.. 
{
    /// <summary>
    /// This class is intended as a base class for classes FROM which you want to log.
    /// So it's not intended as a logging provider base.
    /// </summary>
    public abstract class LoggingClassBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="level">The level.</param>
        /// <param name="log">The log.</param>
        public delegate void LogHandler(object sender, LoggingLevel level, string log);

        /// <summary>
        /// Occurs when [logged].
        /// </summary>
        public event LogHandler Logged;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="log">The log.</param>
        /// <param name="args">The args.</param>
        public virtual void OnLogged(LoggingLevel level, string log, params object[] args)
        {
            string formatted = this.LogPrefix() + " - " + log.FormatSafe(args);
            var listener = this.Logged;
            if (listener != null)
                listener(this, level, formatted);
        }

        /// <summary>
        /// Logs the prefix.
        /// </summary>
        /// <returns></returns>
        public abstract string LogPrefix();

        /// <summary>
        /// Log on the 'Verbose' level
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="args">Arguments to be used in formatting message</param>
        public void LogVerbose(string message, params object[] args)
        {
            this.OnLogged(LoggingLevel.Verbose, message, args);
        }

        /// <summary>
        /// Log on the 'Debug' level
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="args">Arguments to be used in formatting message</param>
        public void LogDebug(string message, params object[] args)
        {
            this.OnLogged(LoggingLevel.Debug, message, args);
        }

        /// <summary>
        /// Log on the 'Info' level
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="args">Arguments to be used in formatting message</param>
        public void LogInfo(string message, params object[] args)
        {
            this.OnLogged(LoggingLevel.Info, message, args);
        }

        /// <summary>
        /// Log on the 'Warning' level
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="args">Arguments to be used in formatting message</param>
        public void LogWarning(string message, params object[] args)
        {
            this.OnLogged(LoggingLevel.Warning, message, args);
        }

        /// <summary>
        /// Log on the 'Error' level
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="args">Arguments to be used in formatting message</param>
        public void LogError(string message, params object[] args)
        {
            this.OnLogged(LoggingLevel.Error, message, args);
        }
    }
}
