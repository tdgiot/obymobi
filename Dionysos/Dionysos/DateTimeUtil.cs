using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;


namespace Dionysos
{
    /// <summary>
    /// Singleton class which contains methods to perform operations on System.DateTime instances
    /// </summary>
    public static class DateTimeUtil
    {
        public static readonly DateTime MinDateValueSql = new DateTime(1753, 1, 1, 0, 0, 1);

        /// <summary>
        /// Return a DateTime with Date of the input value and Time 23:59:59
        /// </summary>
        /// <param name="date">DateTime to take Date part From</param>
        /// <returns>DateTime with time 23:59:59</returns>
        public static DateTime MakeEndOfDay(this DateTime date)
        {
            return new System.DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        /// <summary>
        /// Return a DateTime with Date of the input value and Time 00:00:00
        /// </summary>
        /// <param name="date">DateTime to take Date part From</param>
        /// <returns>DateTime with time 00:00:00</returns>
        public static System.DateTime MakeBeginOfDay(this System.DateTime date)
        {
            return new System.DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }

        /// <summary>
        /// Returns a DateTime with the Date of Today and specified time
        /// </summary>
        /// <param name="hour">Hours</param>
        /// <param name="minutes">Minutes</param>
        /// <returns>Returns a DateTime with the Date of Today and specified time</returns>
        public static System.DateTime GetDateTimeForToday(int hour, int minutes)
        {
            return new System.DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hour, minutes, 0);
        }

        /// <summary>
        /// Combine 2 DateTimes to one, first one will be used for the Time and second for the Date part.
        /// </summary>
        /// <param name="forTime">DateTime with Time part to be used</param>
        /// <param name="forDate">DateTime with Date part to be used</param>
        /// <returns>1 DateTime containing the requested Date and Time</returns>
        public static System.DateTime CombineDateAndTime(System.DateTime forTime, System.DateTime forDate)
        {
            return new System.DateTime(forDate.Year, forDate.Month, forDate.Day, forTime.Hour, forTime.Minute, forTime.Second);
        }

        /// <summary>
        /// Get someones age in years
        /// </summary>
        /// <param name="birthDate">Birthdate</param>
        /// <returns>Years old</returns>
        public static int GetAgeInYears(this DateTime birthDate)
        {
            // cache the current time
            DateTime now = DateTime.Today; // today is fine, don't need the timestamp from now
            // get the difference in years
            int years = now.Year - birthDate.Year;
            // subtract another year if we're before the
            // birth day in the current year
            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
                --years;

            return years;
        }

        /// <summary>
        /// Get a DateTime for a existing DateTime for the Date and a string for the time
        /// </summary>
        /// <param name="forDate">Date</param>
        /// <param name="timeString">Time in 24h format</param>
        /// <returns></returns>
        public static DateTime DateTimeFromTime24hString(DateTime forDate, string timeString)
        {
            DateTime toReturn = DateTime.MinValue;
            Regex time = new Regex(@"(?<hours>[0-9]{1,2}):(?<minutes>[0-9]{2})");

            MatchCollection matches = time.Matches(timeString);
            if (matches.Count != 1)
                throw new Dionysos.FunctionalException("Time notation '{0}' is invalid", timeString);
            else
            {
                int hours = Convert.ToInt32(matches[0].Groups["hours"].Value);
                int minutes = Convert.ToInt32(matches[0].Groups["minutes"].Value);
                toReturn = new DateTime(forDate.Year, forDate.Month, forDate.Day, hours, minutes, 0);
            }

            return toReturn;
        }

        /// <summary>
        /// Convert DateTime to YYYYMMDD format
        /// </summary>
        /// <param name="date">DateTime to use</param>
        /// <returns>String DateStamp for Date</returns>
        public static string DateTimeToSimpleDateStamp(this System.DateTime date)
        {
            return date.Year.ToString("0000") + date.Month.ToString("00") + date.Day.ToString("00");
        }

        /// <summary>
        /// Convert DateTime to YYYYMMDDHHMMSS format
        /// </summary>
        /// <param name="date">DateTime to use</param>
        /// <returns>String DateTimeStamp for Date</returns>
        public static string DateTimeToSimpleDateTimeStamp(this System.DateTime date)
        {
            return date.Year.ToString("0000") + date.Month.ToString("00") + date.Day.ToString("00") + date.Hour.ToString("00") + date.Minute.ToString("00") + date.Second.ToString("00");
        }

        /// <summary>
        /// Convert DateTime.Now to YYMMDDHHMMSSTTTTTTTT (Ticks) format
        /// </summary>
        public static string DateTimeToSimpleDateTimeTicksStamp()
        {
            return DateTime.Now.DateTimeToSimpleDateTimeTicksStamp();
        }

        /// <summary>
        /// Convert DateTime to YYMMDDHHMMSSTTTTTTTT (Ticks) format
        /// </summary>
        /// <param name="date">DateTime to use</param>
        /// <returns>String DateTimeStamp for Date</returns>
        public static string DateTimeToSimpleDateTimeTicksStamp(this System.DateTime date)
        {
            string ticks = date.Ticks.ToString();
            ticks = ticks.Substring(ticks.Length - 8, 4);
            return date.ToString("yy") + date.Month.ToString("00") + date.Day.ToString("00") + date.Hour.ToString("00") + date.Minute.ToString("00") + date.Second.ToString("00") + ticks;
        }

        /// <summary>
        /// Convert DateTime to Rfc822 Date GMT (the TimeSpan changes the DateTime to UTC/GMT datetime)       
        /// </summary>
        /// /// <param name="datetime">DateTime</param>
        /// <param name="timeZoneOffSet">Offset from GMT/UTC</param>
        /// <returns>DateTime adjusted as RFC822</returns>
        public static string DateTimeToRfc822(this DateTime datetime, TimeSpan timeZoneOffSet)
        {
            datetime = datetime + timeZoneOffSet;
            return datetime.ToString("R");
            //return datetime.ToString("ddd, dd MMM yyyy HH':'mm':'ss 'GMT'");
        }

        /// <summary>
        /// Converts a datetime to a specific timezone
        /// FO: Temporary moved this from Dionysos.DateTimeUtil.cs to hotfix just the Obymobi.Web assembly
        /// </summary>
        /// <param name="date">DateTime to convert</param>
        /// <param name="fromTimeZoneInfo">The timezone to convert from</param>
        /// <param name="toTimeZoneInfo">The timezone to convert to</param>
        /// <returns>DateTime in new timezone</returns>
        public static DateTime ToTimeZone(this DateTime date, TimeZoneInfo fromTimeZoneInfo, TimeZoneInfo toTimeZoneInfo)
        {
            return TimeZoneInfo.ConvertTime(date, fromTimeZoneInfo, toTimeZoneInfo);
        }

        /// <summary>
        /// Convert string in format YYYYMMDD to DateTime, returns 00000000 if stamp is invalid
        /// </summary>
        /// <param name="dateStamp">DateStamp to use</param>
        /// <returns>DateTime accor</returns>
        public static System.DateTime SimpleDateStampToDateTime(this string dateStamp)
        {
            System.DateTime retval = System.DateTime.MinValue;
            try
            {
                if (dateStamp.Length == 8)
                {
                    int year = Convert.ToInt32(dateStamp.Substring(0, 4));
                    int month = Convert.ToInt32(dateStamp.Substring(4, 2));
                    int day = Convert.ToInt32(dateStamp.Substring(6, 2));

                    if (year >= 1900 && year <= 2100 && month >= 1 && month <= 12 && day >= 0 && day <= System.DateTime.DaysInMonth(year, month))
                    {
                        retval = new System.DateTime(year, month, day);
                    }
                }
            }
            catch
            {
            }

            return retval;
        }

        /// <summary>
        /// Convert string in format YYYYMMDDHHMMSS to DateTime, returns 00000000 if stamp is invalid
        /// </summary>
        /// <param name="dateStamp">DateTimeStamp to use</param>
        /// <returns>DateTime accorind to stamp</returns>
        public static System.DateTime SimpleDateTimeStampToDateTime(this string dateStamp)
        {
            System.DateTime retval = System.DateTime.MinValue;
            try
            {
                if (dateStamp.Length == 14)
                {
                    int year = Convert.ToInt32(dateStamp.Substring(0, 4));
                    int month = Convert.ToInt32(dateStamp.Substring(4, 2));
                    int day = Convert.ToInt32(dateStamp.Substring(6, 2));
                    int hour = Convert.ToInt32(dateStamp.Substring(8, 2));
                    int minute = Convert.ToInt32(dateStamp.Substring(10, 2));
                    int second = Convert.ToInt32(dateStamp.Substring(12, 2));

                    if (year >= 1900 && year <= 2100 && month >= 1 && month <= 12 && day >= 0 && day <= System.DateTime.DaysInMonth(year, month))
                    {
                        retval = new System.DateTime(year, month, day, hour, minute, second);
                    }
                }
            }
            catch
            {
            }

            return retval;
        }

        /// <summary>
        /// Return a DayOfWeek instance based on the specified day number (start on monday = 1)
        /// </summary>
        /// <returns>A DayOfWeek instance</returns>
        public static DayOfWeek GetDayOfWeek()
        {
            return GetDayOfWeek(GetDayNoOfWeek());
        }

        /// <summary>
        /// Return a DayOfWeek instance based on the specified day number (start on monday = 1)
        /// </summary>
        /// <param name="dayNo">The number of the day to get the DayOfWeek instance for</param>
        /// <returns>A DayOfWeek instance</returns>
        public static DayOfWeek GetDayOfWeek(int dayNo)
        {
            DayOfWeek dayOfWeek = DateTime.Now.DayOfWeek;

            switch (dayNo)
            {
                case 0:
                    throw new ApplicationException("Day 0 does not exist!");
                case 1:
                    dayOfWeek = DayOfWeek.Monday;
                    break;
                case 2:
                    dayOfWeek = DayOfWeek.Tuesday;
                    break;
                case 3:
                    dayOfWeek = DayOfWeek.Wednesday;
                    break;
                case 4:
                    dayOfWeek = DayOfWeek.Thursday;
                    break;
                case 5:
                    dayOfWeek = DayOfWeek.Friday;
                    break;
                case 6:
                    dayOfWeek = DayOfWeek.Saturday;
                    break;
                case 7:
                    dayOfWeek = DayOfWeek.Sunday;
                    break;
            }

            return dayOfWeek;
        }

        /// <summary>
        /// Gets the day of the current week (start on monday)
        /// </summary>
        /// <returns>The day number of the specified date</returns>
        public static int GetDayNoOfWeek()
        {
            return GetDayNoOfWeek(DateTime.Now);
        }

        /// <summary>
        /// Gets the day of the week (start on monday)
        /// </summary>
        /// <param name="date">The DateTime instance to get the day number for</param>
        /// <returns>The day number of the specified date</returns>
        public static int GetDayNoOfWeek(this DateTime date)
        {
            return GetDayNoOfWeek(date.DayOfWeek);
        }

        /// <summary>
        /// Gets the day of the week (start on monday)
        /// </summary>
        /// <param name="dayOfWeek">The DayOfWeek instance to get the day number for</param>
        /// <returns>The day number of the specified date</returns>
        public static int GetDayNoOfWeek(DayOfWeek dayOfWeek)
        {
            int dayNo = 0;

            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    dayNo = 1;
                    break;
                case DayOfWeek.Tuesday:
                    dayNo = 2;
                    break;
                case DayOfWeek.Wednesday:
                    dayNo = 3;
                    break;
                case DayOfWeek.Thursday:
                    dayNo = 4;
                    break;
                case DayOfWeek.Friday:
                    dayNo = 5;
                    break;
                case DayOfWeek.Saturday:
                    dayNo = 6;
                    break;
                case DayOfWeek.Sunday:
                    dayNo = 7;
                    break;
                default:
                    break;
            }
            return dayNo;
        }

        /// <summary>
        /// Gets the name of the current day in dutch
        /// </summary>
        /// <returns>The name of the day in Dutch</returns>
        public static string GetDutchDayName()
        {
            return GetDayName(DateTime.Now);
        }

        /// <summary>
        /// Get the name of the day!
        /// </summary>
        /// <param name="dateTime">The DateTime instance to get the name of the day for</param>
        /// <returns>Name of the day in Dutch</returns>
        public static string GetDayName(this DateTime dateTime)
        {
            return GetDayName(Dionysos.DateTimeUtil.GetDayNoOfWeek(dateTime));
        }

        /// <summary>
        /// Gets the name of the specified day in Dutch
        /// </summary>
        /// <param name="dayNoOfWeek">The number of the day of the week </param>
        /// <returns>The name of the day in Dutch</returns>
        public static string GetDayName(int dayNoOfWeek)
        {
            string dayFullName = string.Empty;
            switch (dayNoOfWeek)
            {
                case 1:
                    dayFullName = "maandag";
                    break;
                case 2:
                    dayFullName = "dinsdag";
                    break;
                case 3:
                    dayFullName = "woensdag";
                    break;
                case 4:
                    dayFullName = "donderdag";
                    break;
                case 5:
                    dayFullName = "vrijdag";
                    break;
                case 6:
                    dayFullName = "zaterdag";
                    break;
                case 7:
                    dayFullName = "zondag";
                    break;
                default:
                    break;
            }
            return dayFullName;
        }

        /// <summary>
        /// Gets the name of the specified day in English
        /// </summary>
        /// <param name="dayNoOfWeek">The number of the day of the week </param>
        /// <returns>The name of the day in English</returns>
        public static string GetDayNameEnglish(int dayNoOfWeek)
        {
            // GK Probably this can also be done by generating certain week
            // add days, etc.
            string dayFullName = string.Empty;
            switch (dayNoOfWeek)
            {
                case 1:
                    dayFullName = "Monday";
                    break;
                case 2:
                    dayFullName = "Tuesday";
                    break;
                case 3:
                    dayFullName = "Wednesday";
                    break;
                case 4:
                    dayFullName = "Thursday";
                    break;
                case 5:
                    dayFullName = "Friday";
                    break;
                case 6:
                    dayFullName = "Saturday";
                    break;
                case 7:
                    dayFullName = "Sunday";
                    break;
                default:
                    break;
            }
            return dayFullName;
        }

        /// <summary>
        /// Gets the name of the month in Dutch
        /// </summary>
        /// <param name="month">The number of the month</param>
        /// <returns>The name of the month in Dutch</returns>
        public static string GetMonthName(int month)
        {
            string monthName = string.Empty;

            switch (month)
            {
                case 1:
                    monthName = "januari";
                    break;
                case 2:
                    monthName = "februari";
                    break;
                case 3:
                    monthName = "maart";
                    break;
                case 4:
                    monthName = "april";
                    break;
                case 5:
                    monthName = "mei";
                    break;
                case 6:
                    monthName = "juni";
                    break;
                case 7:
                    monthName = "juli";
                    break;
                case 8:
                    monthName = "augustus";
                    break;
                case 9:
                    monthName = "september";
                    break;
                case 10:
                    monthName = "oktober";
                    break;
                case 11:
                    monthName = "november";
                    break;
                case 12:
                    monthName = "december";
                    break;
                default:
                    break;
            }

            return monthName;
        }

        /// <summary>
        /// Gets the name of the month in Dutch
        /// </summary>
        /// <param name="month">The number of the month</param>
        /// <returns>The name of the month in Dutch</returns>
        public static string GetMonthAbbreviation(int month)
        {
            string monthName = string.Empty;

            switch (month)
            {
                case 1:
                    monthName = "jan";
                    break;
                case 2:
                    monthName = "feb";
                    break;
                case 3:
                    monthName = "mrt";
                    break;
                case 4:
                    monthName = "apr";
                    break;
                case 5:
                    monthName = "mei";
                    break;
                case 6:
                    monthName = "jun";
                    break;
                case 7:
                    monthName = "jul";
                    break;
                case 8:
                    monthName = "aug";
                    break;
                case 9:
                    monthName = "sep";
                    break;
                case 10:
                    monthName = "okt";
                    break;
                case 11:
                    monthName = "nov";
                    break;
                case 12:
                    monthName = "dec";
                    break;
                default:
                    break;
            }

            return monthName;
        }

        /// <summary>
        /// Gets the quarter number based on the specified week number
        /// </summary>
        /// <param name="weekNumber">The number of the week to get the quarter number for</param>
        /// <returns>The number of the quarter</returns>
        public static int GetQuarter(int weekNumber)
        {
            int quarter = 0;

            if (weekNumber != 0)
            {
                quarter = weekNumber / 4;
                int week2 = weekNumber % 4;

                if (week2 != 0)
                {
                    quarter++;
                }
            }

            return quarter;
        }


        /// <summary>
        /// Returns a DateTime instance based on the specified year, weeknumber and dayOfWeek (FIRST DAY OF WEEK MONDAY)
        /// </summary>
        /// <param name="year">The year of the date to get</param>
        /// <param name="weekNumber">The weeknumber of the date to get</param>
        /// <param name="dayOfWeek">The DayOfWeek instance of the date to get</param>
        /// <returns>A DateTime instance containing the date based on the year, weeknumber and day of week</returns>
        public static DateTime GetDate(int year, int weekNumber, DayOfWeek dayOfWeek)
        {
            // Create a DateTime instance for the first on january (week 52, 53 or 1)
            DateTime date = new DateTime(year, 1, 1);
            int weekOfFirstOfJanuary = GetWeekOfYear(date);

            // Set the date to the first day of the week in week 1            
            int daysToAdd = (int)DayOfWeek.Monday - (int)date.DayOfWeek;
            date = date.AddDays(daysToAdd);

            // Set the date to the first day of the specified weeknumber
            if (weekOfFirstOfJanuary > 51)
            {
                // Is in previous year, deducted two 
                date = date.AddDays(7 * (weekNumber - 2));
            }
            else
            {
                // Is in previous year, deducted one
                date = date.AddDays(7 * (weekNumber - 1));
            }

            // Set the date to the specified day of the specified weeknumber
            if (dayOfWeek == DayOfWeek.Sunday)
                daysToAdd = 6;
            else
                daysToAdd = (int)dayOfWeek - 1;
            date = date.AddDays(daysToAdd);
            return date;
        }

        /// <summary>
        /// Get First Day Of Week (FIRST DAY OF WEEK MONDAY)
        /// </summary>
        /// <param name="date">Date to look for</param>
        /// <returns>First day of the week the date is part of</returns>
        public static DateTime GetFirstDayOfWeek(this DateTime date)
        {
            DateTime firstDay;
            if ((int)date.DayOfWeek == 0)
                firstDay = date.AddDays(-6);
            else
                firstDay = date.AddDays(-1 * ((int)date.DayOfWeek - 1));

            return firstDay.Date;
        }

        /// <summary>
        /// Get First Day Of Week (FIRST DAY OF WEEK MONDAY)
        /// </summary>
        /// <param name="year">Year</param>
        /// <param name="weekNo">Week</param>
        /// <returns></returns>
        public static DateTime GetFirstDayOfWeek(int year, int weekNo)
        {
            Debug.Assert(weekNo >= 1);
            CalendarWeekRule rule = CalendarWeekRule.FirstFourDayWeek;

            DateTime jan1 = new DateTime(year, 1, 1);

            int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
            DateTime firstMonday = jan1.AddDays(daysOffset);
            Debug.Assert(firstMonday.DayOfWeek == DayOfWeek.Monday);

            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Monday);

            if (firstWeek <= 1)
            {
                weekNo -= 1;
            }

            DateTime result = firstMonday.AddDays(weekNo * 7);

            return result;
        }

        /// <summary>
        /// Get Last Day Of Week (FIRST DAY OF WEEK MONDAY)
        /// </summary>
        /// <param name="date">Date to look for</param>
        /// <returns>Last day of the week the date is part of</returns>
        public static DateTime GetLastDayOfWeek(this DateTime date)
        {
            return date.GetFirstDayOfWeek().AddDays(6);
        }

        /// <summary>
        /// Get the Quater of YUear for a date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int GetQuarterOfYear(this DateTime date)
        {
            int q;

            if (date.Month <= 3)
                q = 1;
            else if (date.Month <= 6)
                q = 2;
            else if (date.Month <= 9)
                q = 3;
            else
                q = 4;

            return q;
        }

        /// <summary>
        /// Get First Date Of Quarter
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetFirstDayOfQuarter(this DateTime date)
        {
            DateTime firstDayOfQuarter;

            if (date.Month <= 3)
                firstDayOfQuarter = new DateTime(date.Year, 1, 1);
            else if (date.Month <= 6)
                firstDayOfQuarter = new DateTime(date.Year, 4, 1);
            else if (date.Month <= 9)
                firstDayOfQuarter = new DateTime(date.Year, 7, 1);
            else
                firstDayOfQuarter = new DateTime(date.Year, 10, 1);

            return firstDayOfQuarter;
        }

        /// <summary>
        /// Get last date of quater
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetLastDayOfQuarter(this DateTime date)
        {
            DateTime lastDayOfQuarter;

            if (date.Month <= 3)
                lastDayOfQuarter = new DateTime(date.Year, 3, 1);
            else if (date.Month <= 6)
                lastDayOfQuarter = new DateTime(date.Year, 6, 1);
            else if (date.Month <= 9)
                lastDayOfQuarter = new DateTime(date.Year, 9, 1);
            else
                lastDayOfQuarter = new DateTime(date.Year, 12, 1);

            lastDayOfQuarter = lastDayOfQuarter.AddMonths(1).AddDays(-1);

            return lastDayOfQuarter;
        }

        /// <summary>
        /// Get first day of month
        /// </summary>
        /// <param name="date">date</param>
        /// <returns>First date of month</returns>
        public static DateTime GetFirstDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        /// <summary>
        /// Get last day of month
        /// </summary>
        /// <param name="date">date</param>
        /// <returns>Last date</returns>
        public static DateTime GetLastDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1);
        }

        /// <summary>
        /// Get first day of year
        /// </summary>
        /// <param name="date">date</param>
        /// <returns>First day of year</returns>
        public static DateTime GetFirstDayOfYear(this DateTime date)
        {
            return new DateTime(date.Year, 1, 1);
        }

        /// <summary>
        /// Get first day of year
        /// </summary>
        /// <param name="year">year</param>
        /// <returns>First day of year</returns>
        public static DateTime GetFirstDayOfYear(int year)
        {
            return new DateTime(year, 1, 1);
        }

        /// <summary>
        /// Get last day of year
        /// </summary>
        /// <param name="year">year</param>
        /// <returns>last day of year</returns>
        public static DateTime GetLastDayOfYear(int year)
        {
            return new DateTime(year, 12, 31);
        }

        /// <summary>
        /// Get last day of year
        /// </summary>
        /// <param name="date">date</param>
        /// <returns></returns>
        public static DateTime GetLastDayOfYear(this DateTime date)
        {
            return new DateTime(date.Year, 12, 31);
        }

        /// <summary>
        /// Verify if the date is the last day of the year
        /// </summary>
        /// <param name="date">date</param>
        /// <returns></returns>
        public static bool IsLastDayOfYear(this DateTime date)
        {
            return date == date.GetLastDayOfYear();
        }

        /// <summary>
        /// Verify if the date is the first day of the year
        /// </summary>
        /// <param name="date">date</param>
        /// <returns></returns>
        public static bool IsFirstDayOfYear(this DateTime date)
        {
            return date == date.GetFirstDayOfYear();
        }

        /// <summary>
        /// Verify if the date is the last day of the month
        /// </summary>
        /// <param name="date">date</param>
        /// <returns></returns>
        public static bool IsLastDayOfMonth(this DateTime date)
        {
            return date == date.GetLastDayOfMonth();
        }

        /// <summary>
        /// Verify if the date is the last day of the month
        /// </summary>
        /// <param name="date">date</param>
        /// <returns></returns>
        public static bool IsFirstDayOfMonth(this DateTime date)
        {
            return date == date.GetFirstDayOfMonth();
        }

        /// <summary>
        /// Determine of the supplied dates are FirstDay of Year and LastDaY of Year
        /// </summary>
        /// <param name="startDate">Begin of period</param>
        /// <param name="endDate">End of period</param>
        /// <returns></returns>
        public static bool IsFullYear(DateTime startDate, DateTime endDate)
        {
            if (startDate.Year == endDate.Year && startDate == startDate.GetFirstDayOfYear() && endDate == endDate.GetLastDayOfYear())
                return true;
            else
                return false;
        }

        /// <summary>
        /// Gets the week number of the current date
        /// </summary>
        /// <returns>The number of the current week</returns>
        public static int GetWeekOfYear()
        {
            return GetWeekOfYear(DateTime.Now);
        }

        /// <summary>
        /// Retrieve the Year belonging to the GetWeekOfYear for a specific date. Used for the corner cases where first or last days of the year belong the the previous or next year.
        /// </summary>
        /// <param name="date">The DateTime instance to get the year for the week number for</param>
        /// <returns>The year</returns>
        public static int GetWeekOfYearYear(this DateTime date)
        {
            int year;
            int week = date.GetWeekOfYear();

            if (date.Month == 12 && week == 1)
                year = date.Year + 1;
            else if (date.Month == 1 && week > 50)
                year = date.Year - 1;
            else
                year = date.Year;

            return year;
        }

        /// <summary>
        /// Gets the week number of the specified DateTime instance (FIRST DAY OF WEEK MONDAY)
        /// </summary>
        /// <param name="date">The DateTime instance to get the week number for</param>
        /// <returns>The number of the week</returns>
        public static int GetWeekOfYear(this DateTime date)
        {
            // Updated 2004.09.27. Cleaned the code and fixed a bug. Compared the algorithm with
            // code published here . Tested code successfully against the other algorithm 
            // for all dates in all years between 1900 and 2100.
            // Thanks to Marcus Dahlberg for pointing out the deficient logic.

            // Calculates the ISO 8601 Week Number
            // In this scenario the first day of the week is monday, 
            // and the week rule states that:
            // [...] the first calendar week of a year is the one 
            // that includes the first Thursday of that year and 
            // [...] the last calendar week of a calendar year is 
            // the week immediately preceding the first 
            // calendar week of the next year.
            // The first week of the year may thus start in the 
            // preceding year

            const int JAN = 1;
            const int DEC = 12;
            const int LASTDAYOFDEC = 31;
            const int FIRSTDAYOFJAN = 1;
            const int THURSDAY = 4;
            bool ThursdayFlag = false;

            // Get the day number since the beginning of the year
            int DayOfYear = date.DayOfYear;

            // Get the numeric weekday of the first day of the 
            // year (using sunday as FirstDay)
            int StartWeekDayOfYear = (int)(new DateTime(date.Year, JAN, FIRSTDAYOFJAN)).DayOfWeek;
            int EndWeekDayOfYear = (int)(new DateTime(date.Year, DEC, LASTDAYOFDEC)).DayOfWeek;

            // Compensate for the fact that we are using monday
            // as the first day of the week
            if (StartWeekDayOfYear == 0)
                StartWeekDayOfYear = 7;
            if (EndWeekDayOfYear == 0)
                EndWeekDayOfYear = 7;

            // Calculate the number of days in the first and last week
            int DaysInFirstWeek = 8 - (StartWeekDayOfYear);

            // If the year either starts or ends on a thursday it will have a 53rd week
            if (StartWeekDayOfYear == THURSDAY || EndWeekDayOfYear == THURSDAY)
                ThursdayFlag = true;

            // We begin by calculating the number of FULL weeks between the start of the year and
            // our date. The number is rounded up, so the smallest possible value is 0.
            int FullWeeks = (int)System.Math.Ceiling((DayOfYear - (DaysInFirstWeek)) / 7.0);

            int WeekNumber = FullWeeks;

            // If the first week of the year has at least four days, then the actual week number for our date
            // can be incremented by one.
            if (DaysInFirstWeek >= THURSDAY)
                WeekNumber = WeekNumber + 1;

            // If week number is larger than week 52 (and the year doesn't either start or end on a thursday)
            // then the correct week number is 1.
            if (WeekNumber > 52 && !ThursdayFlag)
                WeekNumber = 1;

            // If week number is still 0, it means that we are trying to evaluate the week number for a
            // week that belongs in the previous year (since that week has 3 days or less in our date's year).
            // We therefore make a recursive call using the last day of the previous year.
            if (WeekNumber == 0)
                WeekNumber = DateTimeUtil.GetWeekOfYear(new DateTime(date.Year - 1, DEC, LASTDAYOFDEC));
            return WeekNumber;

            #region Mathieu Bende

            //// Gets the Calendar instance associated with a CultureInfo.
            //int week = 0;

            //// Get the culture information
            //CultureInfo cultureInfo = new CultureInfo("nl-NL");
            //// Get the calendar
            //Calendar calendar = cultureInfo.Calendar;

            //// Get the week
            //week = calendar.GetWeekOfYear(dateTime, cultureInfo.DateTimeFormat.CalendarWeekRule, cultureInfo.DateTimeFormat.FirstDayOfWeek);

            //// Return the week.
            //return week;

            #endregion
        }

        /// <summary>
        /// Gets a string of the date and time in Dutch format
        /// </summary>
        /// <returns>A string containing the date and time</returns>
        public static string GetDateTimeString()
        {
            return string.Format("{0}-{1}-{2} {3}:{4}:{5}", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"));
        }

        /// <summary>
        /// Gets a string of the date in Dutch format
        /// </summary>
        /// <returns>A string containing the date</returns>
        public static string GetDateString()
        {
            return string.Format("{0}-{1}-{2}", DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), DateTime.Now.Year.ToString("0000"));
        }

        /// <summary>
        /// Gets a string of the time in Dutch format
        /// </summary>
        /// <returns>A string containing the current time</returns>
        public static string GetTimeString()
        {
            return GetTimeString(false);
        }

        /// <summary>
        /// Gets a string of the time in Dutch format
        /// </summary>
        /// <param name="includeMilliSeconds">A flag which indicates whether milliseconds must be included in the time string</param>
        /// <returns>A string containing the time</returns>
        public static string GetTimeString(bool includeMilliSeconds)
        {
            string timeString = string.Format("{0}:{1}:{2}", DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"));
            if (includeMilliSeconds)
            {
                timeString += ":" + DateTime.Now.Millisecond.ToString("000");
            }
            return timeString;
        }

        /// <summary>
        /// Gets a TimeSpan instance from two dates
        /// </summary>
        /// <param name="date1">The first DateTime instance</param>
        /// <param name="date2">The second DateTime instance</param>
        /// <returns>A TimeSpan instance containing the TimeSpan between two dates</returns>
        public static TimeSpan GetSpanBetweenDateTimes(DateTime date1, DateTime date2)
        {
            return ((date1 > date2) ? (date1 - date2) : (date2 - date1));
        }


        /// <summary>
        /// Totals the seconds to now.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static double TotalSecondsToNow(this DateTime dateTime)
        {
            return (DateTime.Now - dateTime).TotalSeconds;
        }

        /// <summary>
        /// Gets a System.String instance representing the timespan between the specified date and now
        /// </summary>
        /// <param name="date">The date to get the timespan till now from</param>
        /// <returns>A System.String instance containing the timespan representation</returns>
        public static string GetSpanTextBetweenDateTimeAndNow(DateTime date)
        {
            string spanText = string.Empty;

            DateTime now = DateTime.Now;

            TimeSpan timeSpan = DateTimeUtil.GetSpanBetweenDateTimes(date, now);
            TimeSpan timeSpanDays = DateTimeUtil.GetSpanBetweenDateTimes(date.MakeBeginOfDay(), now.MakeBeginOfDay());

            if (timeSpan.TotalDays > 7)
            {
                if (date.Year == now.Year)
                    spanText = string.Format("{0} om {1}", date.ToLongDateString().Substring(0, date.ToLongDateString().Length - 5), date.ToShortTimeString());
                else
                    spanText = string.Format("{0} om {1}", date.ToLongDateString(), date.ToShortTimeString());
            }
            else if (timeSpanDays.TotalDays > 1)
                spanText = string.Format("{0} om {1}", date.GetDayName(), date.ToShortTimeString());
            else if (timeSpan.Days == 1)
                spanText = string.Format("gisteren om {0}", date.ToShortTimeString());
            else if (timeSpan.TotalHours > 1)
                spanText = string.Format("vandaag om {0}", date.ToShortTimeString());
            else if (((int)timeSpan.TotalMinutes) > 0)
                spanText = string.Format("{0} minuten geleden", (int)timeSpan.TotalMinutes);
            else
                spanText = "minder dan een minuut geleden";

            return spanText;
        }

        /// <summary>
        /// Gets a System.String instance representing the timespan between the specified date and now (UTC)
        /// </summary>
        /// <param name="date">The date to get the timespan till now from</param>
        /// <param name="utcNow">Optional parameter for when using TimeZone, default is <see cref="DateTime.UtcNow"/></param>
        /// <returns>A System.String instance containing the timespan representation</returns>
        public static string GetSpanTextBetweenDateTimeAndUtcNow(DateTime date, DateTime? utcNow = null)
        {
            if (!utcNow.HasValue)
                utcNow = DateTime.UtcNow;

            TimeSpan timeSpan = GetSpanBetweenDateTimes(date, utcNow.Value);
            TimeSpan timeSpanDays = GetSpanBetweenDateTimes(date.MakeBeginOfDay(), utcNow.Value.MakeBeginOfDay());

            var spanText = date > utcNow ? "in about " : "about ";

            if (timeSpanDays.TotalDays > 1)
                spanText += string.Format("{0} days", (int)timeSpanDays.TotalDays);
            else if (timeSpanDays.Days == 1)
                spanText += string.Format("{0} hours", (int)timeSpan.TotalHours);
            else if (timeSpan.TotalHours > 2)
                spanText += string.Format("{0} hours", (int)timeSpan.TotalHours);
            else if (((int)timeSpan.TotalMinutes) > 0)
                spanText += string.Format("{0} minutes", (int)timeSpan.TotalMinutes);
            else
                spanText += string.Format("{0} seconds", (int)timeSpan.TotalSeconds);

            if (date < utcNow)
                spanText += " ago";

            return spanText;
        }

        /// <summary>
        /// Gets the number of the current quarter
        /// </summary>
        /// <returns>The number of the current quarter</returns>
        public static int GetQuarterNumber()
        {
            return GetQuarterNumber(DateTime.Now);
        }

        /// <summary>
        /// Gets the number of the quarter of the specified datetime instance
        /// </summary>
        /// <param name="dateTime">The DateTime instance to get the quarter number for</param>
        /// <returns>The number of the quarter</returns>
        public static int GetQuarterNumber(this DateTime dateTime)
        {
            int month = dateTime.Date.Month;
            int quarter = 0;

            switch (month)
            {
                case 1:
                case 2:
                case 3:
                    quarter = 1;
                    break;
                case 4:
                case 5:
                case 6:
                    quarter = 2;
                    break;
                case 7:
                case 8:
                case 9:
                    quarter = 3;
                    break;
                case 10:
                case 11:
                case 12:
                    quarter = 4;
                    break;
            }

            return quarter;
        }

        /// <summary>
        /// Validates if a combination year+month-from and year+month-till is valid. 
        /// </summary>
        /// <returns>True if valid, exception when in valid</returns>
        public static bool ValidateMonthYearBasedPeriod(int fromYear, int fromMonth, int tillYear, int tillMonth)
        {
            if (fromYear.ToString().Length != 4)
                throw new Dionysos.FunctionalException("'Vanaf jaar' moeten 4 cijfers zijn, fout voor: '{0}'", fromYear);
            if (fromMonth < 1 || fromMonth > 12)
                throw new Dionysos.FunctionalException("'Vanaf maand' moet tussen de 1 en 12 liggen, fout voor: '{0}'", fromMonth);
            if (tillYear.ToString().Length != 4)
                throw new Dionysos.FunctionalException("'Tot jaar' moeten 4 cijfers zijn, fout voor: '{0}'", fromYear);
            if (tillMonth < 1 || tillMonth > 12)
                throw new Dionysos.FunctionalException("'Tot maand' moet tussen de 1 en 12 liggen, fout voor: '{0}'", fromMonth);
            if (new DateTime(fromYear, fromMonth, 1) > new DateTime(tillYear, tillMonth, 1))
                throw new Dionysos.FunctionalException("De 'tot-period': {0} ligt voor de 'van-periode': {1}", StringUtilLight.CombineWithSeperator("", tillYear, tillMonth), StringUtilLight.CombineWithSeperator("", fromYear, fromMonth));
            return true;
        }

        /// <summary>
        /// Formats minutes to a more readable format 0u00m
        /// </summary>
        /// <param name="minutes">The total minutes to format</param>
        /// <returns>Formatted string</returns>
        public static string ToHourMinute(int minutes)
        {
            return ((int)(minutes / 60)).ToString() + "u" + ((int)(minutes % 60)).ToString().PadLeft(2, '0') + "m";
        }

        /// <summary>
        /// Gets the elapsed time string (i.e. 4m06s, 5h05m, 1d03h, etc.)
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="till">The till time (or null for DateTime.Now).</param>
        /// <returns></returns>
        public static string GetElapsedTimeString(this DateTime from, DateTime? till)
        {
            if (till == null)
                till = DateTime.Now;

            TimeSpan timeExpired = till.Value - from;

            string expiredTime = string.Empty;
            if (timeExpired.Days > 1)
                expiredTime = string.Format("{0}d", timeExpired.Days, timeExpired.Hours.ToString().PadLeft(2, '0'), timeExpired.Minutes.ToString().PadLeft(2, '0'), timeExpired.Seconds.ToString().PadLeft(2, '0'));
            else if (timeExpired.Days > 0)
                expiredTime = string.Format("{0}d{1}h", timeExpired.Days, timeExpired.Hours.ToString().PadLeft(2, '0'), timeExpired.Minutes.ToString().PadLeft(2, '0'), timeExpired.Seconds.ToString().PadLeft(2, '0'));
            else if (timeExpired.Hours > 0)
                expiredTime = string.Format("{0}h{1}m", timeExpired.Hours.ToString().PadLeft(2, '0'), timeExpired.Minutes.ToString().PadLeft(2, '0'), timeExpired.Seconds.ToString().PadLeft(2, '0'));
            else if (timeExpired.Minutes > 0)
                expiredTime = string.Format("{0}m{1}s", timeExpired.Minutes.ToString().PadLeft(2, '0'), timeExpired.Seconds.ToString().PadLeft(2, '0'));
            else if (timeExpired.Seconds > 0)
                expiredTime = string.Format("{0}s", timeExpired.Seconds);
            else if (timeExpired.Milliseconds > 0)
                expiredTime = string.Format("{0}ms", timeExpired.Seconds);

            return expiredTime;
        }

        /// <summary>
        /// Retrieve relative day text(eg. yesterday, today, tomorrow)		
        /// </summary>
        /// <param name="date">The date to compare to today</param>		
        /// <returns></returns>
        public static string GetRelativeDay(this DateTime date)
        {
            string toReturn;
            GetRelativeDay(date, 0, out toReturn);
            return toReturn;
        }

        /// <summary>
        /// Retrieve relative day text(eg. yesterday, today, tomorrow)
        /// if it's further away than maxDaysRelative it will return false and string.Empty
        /// </summary>
        /// <param name="date">The date to compare to today</param>
        /// <param name="maxDaysRelative">Max days to retrun a RelativeDayText for (0 = unlimited)</param>
        /// <param name="text">The text will be asigned to this parameter</param>
        /// <returns></returns>
        public static bool GetRelativeDay(this DateTime date, int maxDaysRelative, out string text)
        {
            text = string.Empty;
            int daysBetween = DateTimeUtil.MakeBeginOfDay(date).Subtract(DateTime.Today).Days;
            if (maxDaysRelative != 0 && (daysBetween > maxDaysRelative || daysBetween * -1 > maxDaysRelative))
            {
                // Outside scope;				
            }
            else
            {
                switch (daysBetween)
                {
                    case -2:
                        text = "eergisteren";
                        break;
                    case -1:
                        text = "gisteren";
                        break;
                    case 0:
                        text = "vandaag";
                        break;
                    case 1:
                        text = "morgen";
                        break;
                    case 2:
                        text = "overmorgen";
                        break;
                    default:
                        if (daysBetween < -2)
                            text = -daysBetween + " dagen geleden";
                        else
                            text = "over " + daysBetween + " dagen";
                        break;
                }
            }

            return text.Length > 0;
        }

        /// <summary>
        /// Get the months between 2 dates
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns></returns>
        public static int GetMonthsDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return System.Math.Abs(monthsApart);
        }

        /// <summary>
        /// Returns a relative day &amp; week Html string. 
        /// </summary>
        /// <param name="date">Date to check for</param>
        /// <returns></returns>
        public static string GetRelativeDayWithWeek(this DateTime date)
        {
            // Set title
            string toReturn = string.Empty;
            int currentWeek = DateTimeUtil.GetWeekOfYear(DateTime.Now.Date);
            int weekOfDate = date.GetWeekOfYear();

            if (date == DateTime.Now.Date.AddDays(-2))
            {
                toReturn = string.Format("<strong>Eergisteren</strong> ({0} - week {1})", Dionysos.DateTimeUtil.GetDayName(date).ToLower(), weekOfDate);
            }
            else if (date == DateTime.Now.Date.AddDays(-1))
            {
                toReturn = string.Format("<strong>Gisteren</strong> ({0} - week {1})", Dionysos.DateTimeUtil.GetDayName(date).ToLower(), weekOfDate);
            }
            else if (date == DateTime.Now.Date)
            {
                toReturn = string.Format("<strong>vandaag</strong> ({0} - week {1})", Dionysos.DateTimeUtil.GetDayName(date).ToLower(), weekOfDate);
            }
            else if (date == DateTime.Now.Date.AddDays(1))
            {
                toReturn = string.Format("<strong>morgen</strong> ({0} - week {1})", Dionysos.DateTimeUtil.GetDayName(date).ToLower(), weekOfDate);
            }
            else if (date == DateTime.Now.Date.AddDays(2))
            {
                toReturn = string.Format("<strong>overmorgen</strong> ({0} - week {1})", Dionysos.DateTimeUtil.GetDayName(date).ToLower(), weekOfDate);
            }
            else if (weekOfDate == currentWeek)
            {
                toReturn = string.Format("<strong>Deze week {0}</strong> (week {1})", Dionysos.DateTimeUtil.GetDayName(date).ToLower(), weekOfDate);
            }
            else if (weekOfDate == (currentWeek + 1))
            {
                toReturn = string.Format("<strong>Volgende week {0}</strong> (week {1}) ", Dionysos.DateTimeUtil.GetDayName(date).ToLower(), weekOfDate);
            }
            else
            {
                toReturn = string.Format("<strong>{1} - week {0}</strong>", weekOfDate, Dionysos.DateTimeUtil.GetDayName(date).ToLower());
            }

            return toReturn;
        }

        /// <summary>
        /// Created a DateTime object from unix timestamp
        /// </summary>
        /// <param name="unixTime">The unix timestamp to convert</param>
        /// <returns>DateTime object</returns>
        public static DateTime FromUnixTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        /// <summary>
        /// Created unix timestamp from supplied DateTime object
        /// </summary>
        /// <param name="date">DateTime object to convert</param>
        /// <returns>Unix timestamp</returns>
        public static long ToUnixTime(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalSeconds);
        }

        /// <summary>
        /// Create a DateTime object from unix timestamp in milliseconds
        /// </summary>
        /// <param name="unixTime"></param>
        /// <returns></returns>
        public static DateTime FromUnixTimeMillis(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(unixTime);
        }

        /// <summary>
        /// Parses the specified string to a DateTime.
        /// </summary>
        /// <param name="s">The string to parse.</param>
        /// <returns>
        /// The DateTime if parsed; otherwise, <c>null</c>.
        /// </returns>
        public static DateTime? Parse(string s)
        {
            return Parse(s, DateTimeFormatInfo.CurrentInfo, DateTimeStyles.None);
        }

        /// <summary>
        /// Parses the specified string to a DateTime.
        /// </summary>
        /// <param name="s">The string to parse.</param>
        /// <param name="provider">The provider.</param>
        /// <param name="styles">The DateTime styles.</param>
        /// <returns>The DateTime if parsed; otherwise, <c>null</c>.</returns>
        public static DateTime? Parse(string s, IFormatProvider provider, DateTimeStyles styles)
        {
            DateTime parsed;
            if (DateTime.TryParse(s, provider, styles, out parsed))
            {
                return parsed;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get next weekday from start date
        /// </summary>
        /// <param name="start"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        public static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }

        /// <summary>
        /// Converts the specified DateTime instance to local time using the specified TimeZoneInfo instance.
        /// </summary>
        /// <param name="dateTimeUtc">The DateTime instance to convert.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo instance to use in the conversion.</param>
        /// <returns>A <see cref="Nullable{DateTime}"/> instance containing the local time.</returns>
        public static DateTime? UtcToLocalTime(this DateTime? dateTimeUtc, TimeZoneInfo timeZoneInfo)
        {
            if (dateTimeUtc.HasValue)
                return TimeZoneInfo.ConvertTimeFromUtc(dateTimeUtc.Value, timeZoneInfo);
            else
                return null;
        }

        /// <summary>
        /// Converts the specified DateTime instance to local time using TimeZoneInf.Local.
        /// </summary>
        /// <param name="dateTimeUtc">The DateTime instance to convert.</param>
        /// <returns>A <see cref="Nullable{DateTime}"/> instance containing the local time.</returns>
        public static DateTime? UtcToLocalTime(this DateTime? dateTimeUtc)
        {
            return UtcToLocalTime(dateTimeUtc, TimeZoneInfo.Local);
        }

        /// <summary>
        /// Converts the specified DateTime instance to local time using the specified TimeZoneInfo instance.
        /// </summary>
        /// <param name="dateTimeUtc">The DateTime instance to convert.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo instance to use in the conversion.</param>
        /// <returns>A <see cref="DateTime"/> instance containing the local time.</returns>
        public static DateTime UtcToLocalTime(this DateTime dateTimeUtc, TimeZoneInfo timeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTimeUtc, timeZoneInfo);
        }

        /// <summary>
        /// Converts the specified DateTime instance to local time using TimeZoneInfo.Local.
        /// </summary>
        /// <param name="dateTimeUtc">The DateTime instance to convert.</param>
        /// <returns>A <see cref="DateTime"/> instance containing the local time.</returns>
        public static DateTime UtcToLocalTime(this DateTime dateTimeUtc)
        {
            return dateTimeUtc.UtcToLocalTime(TimeZoneInfo.Local);
        }

        /// <summary>
        /// Converts the specified DateTime instance to UTC time using the specified TimeZoneInfo instance.
        /// </summary>
        /// <param name="dateTimeLocal">The DateTime instance to convert.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo instance to use in the conversion.</param>
        /// <returns>A <see cref="Nullable{DateTime}"/> instance containing the UTC time.</returns>
        public static DateTime? LocalTimeToUtc(this DateTime? dateTimeLocal, TimeZoneInfo timeZoneInfo)
        {
            if (dateTimeLocal.HasValue)
                return TimeZoneInfo.ConvertTimeToUtc(dateTimeLocal.Value, timeZoneInfo);
            else
                return null;
        }

        /// <summary>
        /// Converts the specified DateTime instance to UTC time using TimeZoneInfo.Local.
        /// </summary>
        /// <param name="dateTimeUtc">The DateTime instance to convert.</param>
        /// <returns>A <see cref="Nullable{DateTime}"/> instance containing the UTC time.</returns>
        public static DateTime? LocalTimeToUtc(this DateTime? dateTimeLocal)
        {
            return LocalTimeToUtc(dateTimeLocal, TimeZoneInfo.Local);
        }

        /// <summary>
        /// Converts the specified DateTime instance to UTC time using the specified TimeZoneInfo instance.
        /// </summary>
        /// <param name="dateTimeLocal">The DateTime instance to convert.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo instance to use in the conversion.</param>
        /// <returns>A <see cref="DateTime"/> instance containing the UTC time.</returns>
        public static DateTime LocalTimeToUtc(this DateTime dateTimeLocal, TimeZoneInfo timeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeToUtc(dateTimeLocal, timeZoneInfo);
        }

        /// <summary>
        /// Converts the specified DateTime instance to UTC time using TimeZoneInfo.Local.
        /// </summary>
        /// <param name="dateTimeUtc">The DateTime instance to convert.</param>
        /// <returns>A <see cref="DateTime"/> instance containing the UTC time.</returns>
        public static DateTime LocalTimeToUtc(this DateTime dateTimeLocal)
        {
            return dateTimeLocal.LocalTimeToUtc(TimeZoneInfo.Local);
        }
    }
}
