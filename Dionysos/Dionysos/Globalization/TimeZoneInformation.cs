﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Globalization
{
    public static class TimeZoneInformation
    {

        /// <summary>
        /// Dateline Standard Time
        /// (UTC-12:00) International Date Line West
        /// </summary>
        public static TimeZoneInfo GetDatelineStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Dateline Standard Time;-720;(UTC-12:00) International Date Line West;Dateline Standard Time;Dateline Daylight Time;;");
        }

        /// <summary>
        /// UTC-11
        /// (UTC-11:00) Coordinated Universal Time-11
        /// </summary>
        public static TimeZoneInfo GetUTC11()
        {
            return TimeZoneInfo.FromSerializedString("UTC-11;-660;(UTC-11:00) Coordinated Universal Time-11;UTC-11;UTC-11;;");
        }

        /// <summary>
        /// Hawaiian Standard Time
        /// (UTC-10:00) Hawaii
        /// </summary>
        public static TimeZoneInfo GetHawaiianStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Hawaiian Standard Time;-600;(UTC-10:00) Hawaii;Hawaiian Standard Time;Hawaiian Daylight Time;;");
        }

        /// <summary>
        /// Alaskan Standard Time
        /// (UTC-09:00) Alaska
        /// </summary>
        public static TimeZoneInfo GetAlaskanStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Alaskan Standard Time;-540;(UTC-09:00) Alaska;Alaskan Standard Time;Alaskan Daylight Time;[01:01:0001;12:31:2006;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];][01:01:2007;12:31:9999;60;[0;02:00:00;3;2;0;];[0;02:00:00;11;1;0;];];");
        }

        /// <summary>
        /// Pacific Standard Time (Mexico)
        /// (UTC-08:00) Baja California
        /// </summary>
        public static TimeZoneInfo GetPacificStandardTimeMexico()
        {
            return TimeZoneInfo.FromSerializedString("Pacific Standard Time (Mexico);-480;(UTC-08:00) Baja California;Pacific Standard Time (Mexico);Pacific Daylight Time (Mexico);[01:01:0001;12:31:9999;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Pacific Standard Time
        /// (UTC-08:00) Pacific Time (US & Canada)
        /// </summary>
        public static TimeZoneInfo GetPacificStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Pacific Standard Time;-480;(UTC-08:00) Pacific Time (US & Canada);Pacific Standard Time;Pacific Daylight Time;[01:01:0001;12:31:2006;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];][01:01:2007;12:31:9999;60;[0;02:00:00;3;2;0;];[0;02:00:00;11;1;0;];];");
        }

        /// <summary>
        /// US Mountain Standard Time
        /// (UTC-07:00) Arizona
        /// </summary>
        public static TimeZoneInfo GetUSMountainStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("US Mountain Standard Time;-420;(UTC-07:00) Arizona;US Mountain Standard Time;US Mountain Daylight Time;;");
        }

        /// <summary>
        /// Mountain Standard Time (Mexico)
        /// (UTC-07:00) Chihuahua, La Paz, Mazatlan
        /// </summary>
        public static TimeZoneInfo GetMountainStandardTimeMexico()
        {
            return TimeZoneInfo.FromSerializedString("Mountain Standard Time (Mexico);-420;(UTC-07:00) Chihuahua, La Paz, Mazatlan;Mountain Standard Time (Mexico);Mountain Daylight Time (Mexico);[01:01:0001;12:31:9999;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Mountain Standard Time
        /// (UTC-07:00) Mountain Time (US & Canada)
        /// </summary>
        public static TimeZoneInfo GetMountainStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Mountain Standard Time;-420;(UTC-07:00) Mountain Time (US & Canada);Mountain Standard Time;Mountain Daylight Time;[01:01:0001;12:31:2006;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];][01:01:2007;12:31:9999;60;[0;02:00:00;3;2;0;];[0;02:00:00;11;1;0;];];");
        }

        /// <summary>
        /// Central America Standard Time
        /// (UTC-06:00) Central America
        /// </summary>
        public static TimeZoneInfo GetCentralAmericaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Central America Standard Time;-360;(UTC-06:00) Central America;Central America Standard Time;Central America Daylight Time;;");
        }

        /// <summary>
        /// Central Standard Time
        /// (UTC-06:00) Central Time (US & Canada)
        /// </summary>
        public static TimeZoneInfo GetCentralStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Central Standard Time;-360;(UTC-06:00) Central Time (US & Canada);Central Standard Time;Central Daylight Time;[01:01:0001;12:31:2006;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];][01:01:2007;12:31:9999;60;[0;02:00:00;3;2;0;];[0;02:00:00;11;1;0;];];");
        }

        /// <summary>
        /// Central Standard Time (Mexico)
        /// (UTC-06:00) Guadalajara, Mexico City, Monterrey
        /// </summary>
        public static TimeZoneInfo GetCentralStandardTimeMexico()
        {
            return TimeZoneInfo.FromSerializedString("Central Standard Time (Mexico);-360;(UTC-06:00) Guadalajara, Mexico City, Monterrey;Central Standard Time (Mexico);Central Daylight Time (Mexico);[01:01:0001;12:31:9999;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Canada Central Standard Time
        /// (UTC-06:00) Saskatchewan
        /// </summary>
        public static TimeZoneInfo GetCanadaCentralStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Canada Central Standard Time;-360;(UTC-06:00) Saskatchewan;Canada Central Standard Time;Canada Central Daylight Time;;");
        }

        /// <summary>
        /// SA Pacific Standard Time
        /// (UTC-05:00) Bogota, Lima, Quito
        /// </summary>
        public static TimeZoneInfo GetSAPacificStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("SA Pacific Standard Time;-300;(UTC-05:00) Bogota, Lima, Quito;SA Pacific Standard Time;SA Pacific Daylight Time;;");
        }

        /// <summary>
        /// Eastern Standard Time
        /// (UTC-05:00) Eastern Time (US & Canada)
        /// </summary>
        public static TimeZoneInfo GetEasternStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Eastern Standard Time;-300;(UTC-05:00) Eastern Time (US & Canada);Eastern Standard Time;Eastern Daylight Time;[01:01:0001;12:31:2006;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];][01:01:2007;12:31:9999;60;[0;02:00:00;3;2;0;];[0;02:00:00;11;1;0;];];");
        }

        /// <summary>
        /// US Eastern Standard Time
        /// (UTC-05:00) Indiana (East)
        /// </summary>
        public static TimeZoneInfo GetUSEasternStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("US Eastern Standard Time;-300;(UTC-05:00) Indiana (East);US Eastern Standard Time;US Eastern Daylight Time;[01:01:2006;12:31:2006;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];][01:01:2007;12:31:9999;60;[0;02:00:00;3;2;0;];[0;02:00:00;11;1;0;];];");
        }

        /// <summary>
        /// Venezuela Standard Time
        /// (UTC-04:30) Caracas
        /// </summary>
        public static TimeZoneInfo GetVenezuelaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Venezuela Standard Time;-270;(UTC-04:30) Caracas;Venezuela Standard Time;Venezuela Daylight Time;;");
        }

        /// <summary>
        /// Paraguay Standard Time
        /// (UTC-04:00) Asuncion
        /// </summary>
        public static TimeZoneInfo GetParaguayStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Paraguay Standard Time;-240;(UTC-04:00) Asuncion;Paraguay Standard Time;Paraguay Daylight Time;[01:01:0001;12:31:2008;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;3;2;6;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;3;1;6;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;2;6;];][01:01:2011;12:31:2011;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;2;6;];][01:01:2012;12:31:2012;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;1;6;];][01:01:2013;12:31:2013;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;3;4;6;];][01:01:2014;12:31:2014;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;2;6;];][01:01:2015;12:31:2015;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;2;6;];][01:01:2016;12:31:2016;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;2;6;];][01:01:2017;12:31:2017;60;[0;23:59:59.999;9;5;6;];[0;23:59:59.999;4;2;6;];][01:01:2018;12:31:2018;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;1;6;];][01:01:2019;12:31:2019;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;2;6;];][01:01:2020;12:31:2020;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;2;6;];][01:01:2021;12:31:9999;60;[0;23:59:59.999;10;1;6;];[0;23:59:59.999;4;2;6;];];");
        }

        /// <summary>
        /// Atlantic Standard Time
        /// (UTC-04:00) Atlantic Time (Canada)
        /// </summary>
        public static TimeZoneInfo GetAtlanticStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Atlantic Standard Time;-240;(UTC-04:00) Atlantic Time (Canada);Atlantic Standard Time;Atlantic Daylight Time;[01:01:0001;12:31:2006;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];][01:01:2007;12:31:9999;60;[0;02:00:00;3;2;0;];[0;02:00:00;11;1;0;];];");
        }

        /// <summary>
        /// Central Brazilian Standard Time
        /// (UTC-04:00) Cuiaba
        /// </summary>
        public static TimeZoneInfo GetCentralBrazilianStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Central Brazilian Standard Time;-240;(UTC-04:00) Cuiaba;Central Brazilian Standard Time;Central Brazilian Daylight Time;[01:01:0001;12:31:2006;60;[0;00:00:00;11;1;0;];[0;02:00:00;2;2;0;];][01:01:2007;12:31:2007;60;[0;00:00:00;10;2;0;];[0;00:00:00;2;5;0;];][01:01:2008;12:31:2008;60;[0;23:59:59.999;10;3;6;];[0;00:00:00;2;3;0;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;2;6;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2011;12:31:2011;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2012;12:31:2012;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;4;6;];][01:01:2013;12:31:2013;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2014;12:31:2014;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2015;12:31:2015;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2016;12:31:2016;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2017;12:31:2017;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;2;3;6;];][01:01:2018;12:31:2018;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2019;12:31:2019;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2020;12:31:2020;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2021;12:31:2021;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2022;12:31:2022;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2023;12:31:2023;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;2;4;6;];][01:01:2024;12:31:2024;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2025;12:31:2025;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2026;12:31:2026;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2027;12:31:2027;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2028;12:31:2028;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;2;3;6;];][01:01:2029;12:31:2029;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2030;12:31:2030;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2031;12:31:2031;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2032;12:31:2032;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;2;6;];][01:01:2033;12:31:2033;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2034;12:31:2034;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;2;4;6;];][01:01:2035;12:31:2035;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2036;12:31:2036;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2037;12:31:2037;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2038;12:31:2038;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2039;12:31:2039;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;4;6;];][01:01:2040;12:31:9999;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];];");
        }

        /// <summary>
        /// SA Western Standard Time
        /// (UTC-04:00) Georgetown, La Paz, Manaus, San Juan
        /// </summary>
        public static TimeZoneInfo GetSAWesternStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("SA Western Standard Time;-240;(UTC-04:00) Georgetown, La Paz, Manaus, San Juan;SA Western Standard Time;SA Western Daylight Time;;");
        }

        /// <summary>
        /// Pacific SA Standard Time
        /// (UTC-04:00) Santiago
        /// </summary>
        public static TimeZoneInfo GetPacificSAStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Pacific SA Standard Time;-240;(UTC-04:00) Santiago;Pacific SA Standard Time;Pacific SA Daylight Time;[01:01:0001;12:31:2007;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;3;2;6;];][01:01:2008;12:31:2008;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;3;5;6;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;3;2;6;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;4;1;6;];][01:01:2011;12:31:2011;60;[0;23:59:59.999;8;3;6;];[0;23:59:59.999;5;1;6;];][01:01:2012;12:31:2012;60;[0;23:59:59.999;9;1;6;];[0;23:59:59.999;4;5;6;];][01:01:2013;12:31:2013;60;[0;23:59:59.999;9;1;6;];[0;23:59:59.999;4;5;6;];][01:01:2014;12:31:9999;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;3;2;6;];];");
        }

        /// <summary>
        /// Newfoundland Standard Time
        /// (UTC-03:30) Newfoundland
        /// </summary>
        public static TimeZoneInfo GetNewfoundlandStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Newfoundland Standard Time;-210;(UTC-03:30) Newfoundland;Newfoundland Standard Time;Newfoundland Daylight Time;[01:01:0001;12:31:2006;60;[0;00:01:00;4;1;0;];[0;00:01:00;10;5;0;];][01:01:2007;12:31:2007;60;[0;00:01:00;3;2;0;];[0;00:01:00;11;1;0;];][01:01:2008;12:31:2008;60;[0;00:01:00;3;2;0;];[0;00:01:00;11;1;0;];][01:01:2009;12:31:2009;60;[0;00:01:00;3;2;0;];[0;00:01:00;11;1;0;];][01:01:2010;12:31:2010;60;[0;00:01:00;3;2;0;];[0;00:01:00;11;1;0;];][01:01:2011;12:31:2011;60;[0;00:01:00;3;2;0;];[0;02:00:00;11;1;0;];][01:01:2012;12:31:9999;60;[0;02:00:00;3;2;0;];[0;02:00:00;11;1;0;];];");
        }

        /// <summary>
        /// E. South America Standard Time
        /// (UTC-03:00) Brasilia
        /// </summary>
        public static TimeZoneInfo GetESouthAmericaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("E. South America Standard Time;-180;(UTC-03:00) Brasilia;E. South America Standard Time;E. South America Daylight Time;[01:01:0001;12:31:2006;60;[0;00:00:00;11;1;0;];[0;02:00:00;2;2;0;];][01:01:2007;12:31:2007;60;[0;00:00:00;10;2;0;];[0;00:00:00;2;5;0;];][01:01:2008;12:31:2008;60;[0;23:59:59.999;10;3;6;];[0;00:00:00;2;3;0;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;2;6;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2011;12:31:2011;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2012;12:31:2012;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;4;6;];][01:01:2013;12:31:2013;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2014;12:31:2014;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2015;12:31:2015;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2016;12:31:2016;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2017;12:31:2017;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;2;3;6;];][01:01:2018;12:31:2018;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2019;12:31:2019;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2020;12:31:2020;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2021;12:31:2021;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2022;12:31:2022;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2023;12:31:2023;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;2;4;6;];][01:01:2024;12:31:2024;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2025;12:31:2025;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2026;12:31:2026;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2027;12:31:2027;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2028;12:31:2028;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;2;3;6;];][01:01:2029;12:31:2029;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2030;12:31:2030;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2031;12:31:2031;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2032;12:31:2032;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;2;6;];][01:01:2033;12:31:2033;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2034;12:31:2034;60;[0;23:59:59.999;10;2;6;];[0;23:59:59.999;2;4;6;];][01:01:2035;12:31:2035;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2036;12:31:2036;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2037;12:31:2037;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2038;12:31:2038;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];][01:01:2039;12:31:2039;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;4;6;];][01:01:2040;12:31:9999;60;[0;23:59:59.999;10;3;6;];[0;23:59:59.999;2;3;6;];];");
        }

        /// <summary>
        /// Argentina Standard Time
        /// (UTC-03:00) Buenos Aires
        /// </summary>
        public static TimeZoneInfo GetArgentinaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Argentina Standard Time;-180;(UTC-03:00) Buenos Aires;Argentina Standard Time;Argentina Daylight Time;[01:01:2007;12:31:2007;60;[0;00:00:00;12;5;0;];[0;00:00:00;1;1;1;];][01:01:2008;12:31:2008;60;[0;23:59:59.999;10;3;6;];[0;00:00:00;3;3;0;];][01:01:2009;12:31:2009;60;[0;00:00:00;1;1;4;];[0;23:59:59.999;3;2;6;];];");
        }

        /// <summary>
        /// SA Eastern Standard Time
        /// (UTC-03:00) Cayenne, Fortaleza
        /// </summary>
        public static TimeZoneInfo GetSAEasternStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("SA Eastern Standard Time;-180;(UTC-03:00) Cayenne, Fortaleza;SA Eastern Standard Time;SA Eastern Daylight Time;;");
        }

        /// <summary>
        /// Greenland Standard Time
        /// (UTC-03:00) Greenland
        /// </summary>
        public static TimeZoneInfo GetGreenlandStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Greenland Standard Time;-180;(UTC-03:00) Greenland;Greenland Standard Time;Greenland Daylight Time;[01:01:0001;12:31:2008;60;[0;02:00:00;4;1;0;];[0;02:00:00;10;5;0;];][01:01:2009;12:31:2009;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;4;6;];][01:01:2010;12:31:2010;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;5;6;];][01:01:2011;12:31:2011;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;5;6;];][01:01:2012;12:31:2012;60;[0;22:00:00;3;4;6;];[0;23:00:00;10;5;6;];][01:01:2013;12:31:2013;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;5;6;];][01:01:2014;12:31:2014;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;5;6;];][01:01:2015;12:31:2015;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;4;6;];][01:01:2016;12:31:2016;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;5;6;];][01:01:2017;12:31:2017;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;5;6;];][01:01:2018;12:31:2018;60;[0;22:00:00;3;4;6;];[0;23:00:00;10;5;6;];][01:01:2019;12:31:2019;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;5;6;];][01:01:2020;12:31:2020;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;4;6;];][01:01:2021;12:31:9999;60;[0;22:00:00;3;5;6;];[0;23:00:00;10;5;6;];];");
        }

        /// <summary>
        /// Montevideo Standard Time
        /// (UTC-03:00) Montevideo
        /// </summary>
        public static TimeZoneInfo GetMontevideoStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Montevideo Standard Time;-180;(UTC-03:00) Montevideo;Montevideo Standard Time;Montevideo Daylight Time;[01:01:0001;12:31:2006;60;[0;02:00:00;9;2;0;];[0;02:00:00;3;2;0;];][01:01:2007;12:31:9999;60;[0;02:00:00;10;1;0;];[0;02:00:00;3;2;0;];];");
        }

        /// <summary>
        /// Bahia Standard Time
        /// (UTC-03:00) Salvador
        /// </summary>
        public static TimeZoneInfo GetBahiaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Bahia Standard Time;-180;(UTC-03:00) Salvador;Bahia Standard Time;Bahia Daylight Time;[01:01:2011;12:31:2011;60;[0;23:59:59.999;10;3;6;];[0;00:00:00;1;1;6;];][01:01:2012;12:31:2012;60;[0;00:00:00;1;1;0;];[0;23:59:59.999;2;4;6;];];");
        }

        /// <summary>
        /// UTC-02
        /// (UTC-02:00) Coordinated Universal Time-02
        /// </summary>
        public static TimeZoneInfo GetUTC02()
        {
            return TimeZoneInfo.FromSerializedString("UTC-02;-120;(UTC-02:00) Coordinated Universal Time-02;UTC-02;UTC-02;;");
        }

        /// <summary>
        /// Mid-Atlantic Standard Time
        /// (UTC-02:00) Mid-Atlantic
        /// </summary>
        public static TimeZoneInfo GetMidAtlanticStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Mid-Atlantic Standard Time;-120;(UTC-02:00) Mid-Atlantic;Mid-Atlantic Standard Time;Mid-Atlantic Daylight Time;[01:01:0001;12:31:9999;60;[0;02:00:00;3;5;0;];[0;02:00:00;9;5;0;];];");
        }

        /// <summary>
        /// Azores Standard Time
        /// (UTC-01:00) Azores
        /// </summary>
        public static TimeZoneInfo GetAzoresStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Azores Standard Time;-60;(UTC-01:00) Azores;Azores Standard Time;Azores Daylight Time;[01:01:0001;12:31:2011;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2012;12:31:2012;60;[0;02:00:00;3;5;0;];[0;01:00:00;10;5;0;];][01:01:2013;12:31:9999;60;[0;00:00:00;3;5;0;];[0;01:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Cape Verde Standard Time
        /// (UTC-01:00) Cape Verde Is.
        /// </summary>
        public static TimeZoneInfo GetCapeVerdeStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Cape Verde Standard Time;-60;(UTC-01:00) Cape Verde Is.;Cape Verde Standard Time;Cape Verde Daylight Time;;");
        }

        /// <summary>
        /// Morocco Standard Time
        /// (UTC) Casablanca
        /// </summary>
        public static TimeZoneInfo GetMoroccoStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Morocco Standard Time;0;(UTC) Casablanca;Morocco Standard Time;Morocco Daylight Time;[01:01:2008;12:31:2008;60;[0;23:59:59.999;5;5;6;];[0;23:59:59.999;8;5;0;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;5;5;0;];[0;23:59:59.999;8;3;4;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;5;1;6;];[0;23:59:59.999;8;1;6;];][01:01:2011;12:31:2011;60;[0;23:59:59.999;4;1;6;];[0;23:59:59.999;7;5;6;];][01:01:2012;12:31:2012;60;[0;02:00:00;4;5;0;];[0;03:00:00;9;5;0;];][01:01:2013;12:31:2013;60;[0;02:00:00;4;5;0;];[0;03:00:00;9;5;0;];][01:01:2014;12:31:9999;60;[0;02:00:00;4;5;0;];[0;03:00:00;9;5;0;];];");
        }

        /// <summary>
        /// UTC
        /// (UTC) Coordinated Universal Time
        /// </summary>
        public static TimeZoneInfo GetUTC()
        {
            return TimeZoneInfo.FromSerializedString("UTC;0;(UTC) Coordinated Universal Time;Coordinated Universal Time;Coordinated Universal Time;;");
        }

        /// <summary>
        /// GMT Standard Time
        /// (UTC) Dublin, Edinburgh, Lisbon, London
        /// </summary>
        public static TimeZoneInfo GetGMTStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("GMT Standard Time;0;(UTC) Dublin, Edinburgh, Lisbon, London;GMT Standard Time;GMT Daylight Time;[01:01:0001;12:31:9999;60;[0;01:00:00;3;5;0;];[0;02:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Greenwich Standard Time
        /// (UTC) Monrovia, Reykjavik
        /// </summary>
        public static TimeZoneInfo GetGreenwichStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Greenwich Standard Time;0;(UTC) Monrovia, Reykjavik;Greenwich Standard Time;Greenwich Daylight Time;;");
        }

        /// <summary>
        /// W. Europe Standard Time
        /// (UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna
        /// </summary>
        public static TimeZoneInfo GetWEuropeStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("W. Europe Standard Time;60;(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna;W. Europe Standard Time;W. Europe Daylight Time;[01:01:0001;12:31:9999;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Central Europe Standard Time
        /// (UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague
        /// </summary>
        public static TimeZoneInfo GetCentralEuropeStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Central Europe Standard Time;60;(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague;Central Europe Standard Time;Central Europe Daylight Time;[01:01:0001;12:31:9999;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Romance Standard Time
        /// (UTC+01:00) Brussels, Copenhagen, Madrid, Paris
        /// </summary>
        public static TimeZoneInfo GetRomanceStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Romance Standard Time;60;(UTC+01:00) Brussels, Copenhagen, Madrid, Paris;Romance Standard Time;Romance Daylight Time;[01:01:0001;12:31:9999;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Central European Standard Time
        /// (UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb
        /// </summary>
        public static TimeZoneInfo GetCentralEuropeanStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Central European Standard Time;60;(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb;Central European Standard Time;Central European Daylight Time;[01:01:0001;12:31:9999;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Libya Standard Time
        /// (UTC+01:00) Tripoli
        /// </summary>
        public static TimeZoneInfo GetLibyaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Libya Standard Time;60;(UTC+01:00) Tripoli;Libya Standard Time;Libya Daylight Time;[01:01:2012;12:31:2012;60;[0;00:00:00;1;1;0;];[0;02:00:00;11;2;6;];][01:01:2013;12:31:9999;60;[0;01:00:00;3;5;5;];[0;02:00:00;10;5;5;];];");
        }

        /// <summary>
        /// W. Central Africa Standard Time
        /// (UTC+01:00) West Central Africa
        /// </summary>
        public static TimeZoneInfo GetWCentralAfricaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("W. Central Africa Standard Time;60;(UTC+01:00) West Central Africa;W. Central Africa Standard Time;W. Central Africa Daylight Time;;");
        }

        /// <summary>
        /// Namibia Standard Time
        /// (UTC+01:00) Windhoek
        /// </summary>
        public static TimeZoneInfo GetNamibiaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Namibia Standard Time;60;(UTC+01:00) Windhoek;Namibia Standard Time;Namibia Daylight Time;[01:01:0001;12:31:2010;-60;[0;02:00:00;4;1;0;];[0;02:00:00;9;1;0;];][01:01:2011;12:31:9999;60;[0;02:00:00;9;1;0;];[0;02:00:00;4;1;0;];];");
        }

        /// <summary>
        /// GTB Standard Time
        /// (UTC+02:00) Athens, Bucharest
        /// </summary>
        public static TimeZoneInfo GetGTBStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("GTB Standard Time;120;(UTC+02:00) Athens, Bucharest;GTB Standard Time;GTB Daylight Time;[01:01:0001;12:31:9999;60;[0;03:00:00;3;5;0;];[0;04:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Middle East Standard Time
        /// (UTC+02:00) Beirut
        /// </summary>
        public static TimeZoneInfo GetMiddleEastStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Middle East Standard Time;120;(UTC+02:00) Beirut;Middle East Standard Time;Middle East Daylight Time;[01:01:0001;12:31:2009;60;[0;00:00:00;3;5;0;];[0;00:00:00;10;5;0;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;5;6;];][01:01:2011;12:31:2011;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;5;6;];][01:01:2012;12:31:2012;60;[0;23:59:59.999;3;4;6;];[0;23:59:59.999;10;5;6;];][01:01:2013;12:31:2013;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;5;6;];][01:01:2014;12:31:2014;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;5;6;];][01:01:2015;12:31:2015;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;4;6;];][01:01:2016;12:31:2016;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;5;6;];][01:01:2017;12:31:2017;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;5;6;];][01:01:2018;12:31:2018;60;[0;23:59:59.999;3;4;6;];[0;23:59:59.999;10;5;6;];][01:01:2019;12:31:2019;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;5;6;];][01:01:2020;12:31:2020;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;4;6;];][01:01:2021;12:31:9999;60;[0;23:59:59.999;3;5;6;];[0;23:59:59.999;10;5;6;];];");
        }

        /// <summary>
        /// Egypt Standard Time
        /// (UTC+02:00) Cairo
        /// </summary>
        public static TimeZoneInfo GetEgyptStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Egypt Standard Time;120;(UTC+02:00) Cairo;Egypt Standard Time;Egypt Daylight Time;[01:01:0001;12:31:2005;60;[0;00:00:00;4;5;5;];[0;23:59:59.999;9;5;4;];][01:01:2006;12:31:2006;60;[0;00:00:00;4;5;5;];[0;23:59:59.999;9;3;4;];][01:01:2007;12:31:2007;60;[0;23:59:59.999;4;5;4;];[0;23:59:59.999;9;1;4;];][01:01:2008;12:31:2008;60;[0;23:59:59.999;4;5;4;];[0;23:59:59.999;8;5;4;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;4;4;4;];[0;23:59:59.999;8;3;4;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;4;5;4;];[0;23:59:59.999;9;5;4;];];");
        }

        /// <summary>
        /// Syria Standard Time
        /// (UTC+02:00) Damascus
        /// </summary>
        public static TimeZoneInfo GetSyriaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Syria Standard Time;120;(UTC+02:00) Damascus;Syria Standard Time;Syria Daylight Time;[01:01:0001;12:31:2006;60;[0;23:59:59.999;3;5;5;];[0;23:59:59.999;9;3;3;];][01:01:2007;12:31:2007;60;[0;23:59:59.999;3;5;4;];[0;23:59:59.999;11;1;4;];][01:01:2008;12:31:2008;60;[0;23:59:59.999;4;1;4;];[0;23:59:59.999;10;5;5;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;3;5;4;];[0;23:59:59.999;10;5;4;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;4;1;4;];[0;23:59:59.999;10;5;4;];][01:01:2011;12:31:2011;60;[0;23:59:59.999;3;5;4;];[0;23:59:59.999;10;5;4;];][01:01:2012;12:31:2012;60;[0;23:59:59.999;4;1;4;];[0;23:59:59.999;10;5;4;];][01:01:2013;12:31:2013;60;[0;23:59:59.999;4;1;4;];[0;23:59:59.999;10;5;4;];][01:01:2014;12:31:2014;60;[0;23:59:59.999;4;1;4;];[0;23:59:59.999;10;5;4;];][01:01:2015;12:31:2015;60;[0;23:59:59.999;4;1;4;];[0;23:59:59.999;10;5;4;];][01:01:2016;12:31:2016;60;[0;23:59:59.999;3;5;4;];[0;23:59:59.999;10;5;4;];][01:01:2017;12:31:9999;60;[0;23:59:59.999;4;1;4;];[0;23:59:59.999;10;5;4;];];");
        }

        /// <summary>
        /// E. Europe Standard Time
        /// (UTC+02:00) E. Europe
        /// </summary>
        public static TimeZoneInfo GetEEuropeStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("E. Europe Standard Time;120;(UTC+02:00) E. Europe;E. Europe Standard Time;E. Europe Daylight Time;[01:01:0001;12:31:9999;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];];");
        }

        /// <summary>
        /// South Africa Standard Time
        /// (UTC+02:00) Harare, Pretoria
        /// </summary>
        public static TimeZoneInfo GetSouthAfricaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("South Africa Standard Time;120;(UTC+02:00) Harare, Pretoria;South Africa Standard Time;South Africa Daylight Time;;");
        }

        /// <summary>
        /// FLE Standard Time
        /// (UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius
        /// </summary>
        public static TimeZoneInfo GetFLEStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("FLE Standard Time;120;(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius;FLE Standard Time;FLE Daylight Time;[01:01:0001;12:31:9999;60;[0;03:00:00;3;5;0;];[0;04:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Turkey Standard Time
        /// (UTC+02:00) Istanbul
        /// </summary>
        public static TimeZoneInfo GetTurkeyStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Turkey Standard Time;120;(UTC+02:00) Istanbul;Turkey Standard Time;Turkey Daylight Time;[01:01:0001;12:31:2010;60;[0;03:00:00;3;5;0;];[0;04:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;03:00:00;3;5;1;];[0;04:00:00;10;5;0;];][01:01:2012;12:31:9999;60;[0;03:00:00;3;5;0;];[0;04:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Israel Standard Time
        /// (UTC+02:00) Jerusalem
        /// </summary>
        public static TimeZoneInfo GetIsraelStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Israel Standard Time;120;(UTC+02:00) Jerusalem;Jerusalem Standard Time;Jerusalem Daylight Time;[01:01:2005;12:31:2005;60;[0;02:00:00;4;1;5;];[0;02:00:00;10;2;0;];][01:01:2006;12:31:2006;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;1;0;];][01:01:2007;12:31:2007;60;[0;02:00:00;3;5;5;];[0;02:00:00;9;3;0;];][01:01:2008;12:31:2008;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;1;0;];][01:01:2009;12:31:2009;60;[0;02:00:00;3;5;5;];[0;02:00:00;9;5;0;];][01:01:2010;12:31:2010;60;[0;02:00:00;3;5;5;];[0;02:00:00;9;2;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;4;1;5;];[0;02:00:00;10;1;0;];][01:01:2012;12:31:2012;60;[0;02:00:00;3;5;5;];[0;02:00:00;9;4;0;];][01:01:2013;12:31:2013;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;5;0;];][01:01:2014;12:31:2014;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;5;0;];][01:01:2015;12:31:2015;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;5;0;];][01:01:2016;12:31:2016;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;5;0;];][01:01:2017;12:31:2017;60;[0;02:00:00;3;4;5;];[0;02:00:00;10;5;0;];][01:01:2018;12:31:2018;60;[0;02:00:00;3;4;5;];[0;02:00:00;10;5;0;];][01:01:2019;12:31:2019;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;5;0;];][01:01:2020;12:31:2020;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;5;0;];][01:01:2021;12:31:2021;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;5;0;];][01:01:2022;12:31:2022;60;[0;02:00:00;3;5;5;];[0;02:00:00;10;5;0;];][01:01:2023;12:31:9999;60;[0;02:00:00;3;4;5;];[0;02:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Jordan Standard Time
        /// (UTC+03:00) Amman
        /// </summary>
        public static TimeZoneInfo GetJordanStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Jordan Standard Time;180;(UTC+03:00) Amman;Jordan Standard Time;Jordan Daylight Time;[01:01:0001;12:31:2006;60;[0;00:00:00;3;5;4;];[0;01:00:00;9;5;5;];][01:01:2007;12:31:2007;60;[0;23:59:59.999;3;5;4;];[0;01:00:00;10;5;5;];][01:01:2008;12:31:2008;60;[0;23:59:59.999;3;5;4;];[0;01:00:00;10;5;5;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;3;5;4;];[0;01:00:00;10;5;5;];][01:01:2010;12:31:2010;60;[0;23:59:59.999;3;5;4;];[0;01:00:00;10;5;5;];][01:01:2011;12:31:2011;60;[0;23:59:59.999;3;5;4;];[0;01:00:00;10;5;5;];][01:01:2012;12:31:2012;60;[0;23:59:59.999;3;5;4;];[0;00:00:00;1;1;0;];];");
        }

        /// <summary>
        /// Arabic Standard Time
        /// (UTC+03:00) Baghdad
        /// </summary>
        public static TimeZoneInfo GetArabicStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Arabic Standard Time;180;(UTC+03:00) Baghdad;Arabic Standard Time;Arabic Daylight Time;[01:01:0001;12:31:2006;60;[0;03:00:00;4;1;0;];[0;04:00:00;10;1;0;];][01:01:2007;12:31:2007;60;[0;03:00:00;4;1;0;];[0;04:00:00;10;1;1;];];");
        }

        /// <summary>
        /// Kaliningrad Standard Time
        /// (UTC+03:00) Kaliningrad, Minsk
        /// </summary>
        public static TimeZoneInfo GetKaliningradStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Kaliningrad Standard Time;180;(UTC+03:00) Kaliningrad, Minsk;Kaliningrad Standard Time;Kaliningrad Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// Arab Standard Time
        /// (UTC+03:00) Kuwait, Riyadh
        /// </summary>
        public static TimeZoneInfo GetArabStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Arab Standard Time;180;(UTC+03:00) Kuwait, Riyadh;Arab Standard Time;Arab Daylight Time;;");
        }

        /// <summary>
        /// E. Africa Standard Time
        /// (UTC+03:00) Nairobi
        /// </summary>
        public static TimeZoneInfo GetEAfricaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("E. Africa Standard Time;180;(UTC+03:00) Nairobi;E. Africa Standard Time;E. Africa Daylight Time;;");
        }

        /// <summary>
        /// Iran Standard Time
        /// (UTC+03:30) Tehran
        /// </summary>
        public static TimeZoneInfo GetIranStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Iran Standard Time;210;(UTC+03:30) Tehran;Iran Standard Time;Iran Daylight Time;[01:01:0001;12:31:2005;60;[0;02:00:00;3;1;0;];[0;02:00:00;9;4;2;];][01:01:2008;12:31:2008;60;[0;23:59:59.999;3;3;4;];[0;23:59:59.999;9;3;6;];][01:01:2009;12:31:9999;60;[0;23:59:59.999;3;3;6;];[0;23:59:59.999;9;3;1;];];");
        }

        /// <summary>
        /// Arabian Standard Time
        /// (UTC+04:00) Abu Dhabi, Muscat
        /// </summary>
        public static TimeZoneInfo GetArabianStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Arabian Standard Time;240;(UTC+04:00) Abu Dhabi, Muscat;Arabian Standard Time;Arabian Daylight Time;;");
        }

        /// <summary>
        /// Azerbaijan Standard Time
        /// (UTC+04:00) Baku
        /// </summary>
        public static TimeZoneInfo GetAzerbaijanStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Azerbaijan Standard Time;240;(UTC+04:00) Baku;Azerbaijan Standard Time;Azerbaijan Daylight Time;[01:01:0001;12:31:9999;60;[0;04:00:00;3;5;0;];[0;05:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Russian Standard Time
        /// (UTC+04:00) Moscow, St. Petersburg, Volgograd
        /// </summary>
        public static TimeZoneInfo GetRussianStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Russian Standard Time;240;(UTC+04:00) Moscow, St. Petersburg, Volgograd;Russian Standard Time;Russian Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// Mauritius Standard Time
        /// (UTC+04:00) Port Louis
        /// </summary>
        public static TimeZoneInfo GetMauritiusStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Mauritius Standard Time;240;(UTC+04:00) Port Louis;Mauritius Standard Time;Mauritius Daylight Time;[01:01:2008;12:31:2008;60;[0;02:00:00;10;5;0;];[0;00:00:00;1;1;2;];][01:01:2009;12:31:2009;60;[0;00:00:00;1;1;4;];[0;02:00:00;3;5;0;];];");
        }

        /// <summary>
        /// Georgian Standard Time
        /// (UTC+04:00) Tbilisi
        /// </summary>
        public static TimeZoneInfo GetGeorgianStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Georgian Standard Time;240;(UTC+04:00) Tbilisi;Georgian Standard Time;Georgian Daylight Time;;");
        }

        /// <summary>
        /// Caucasus Standard Time
        /// (UTC+04:00) Yerevan
        /// </summary>
        public static TimeZoneInfo GetCaucasusStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Caucasus Standard Time;240;(UTC+04:00) Yerevan;Caucasus Standard Time;Caucasus Daylight Time;[01:01:0001;12:31:2011;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Afghanistan Standard Time
        /// (UTC+04:30) Kabul
        /// </summary>
        public static TimeZoneInfo GetAfghanistanStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Afghanistan Standard Time;270;(UTC+04:30) Kabul;Afghanistan Standard Time;Afghanistan Daylight Time;;");
        }

        /// <summary>
        /// West Asia Standard Time
        /// (UTC+05:00) Ashgabat, Tashkent
        /// </summary>
        public static TimeZoneInfo GetWestAsiaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("West Asia Standard Time;300;(UTC+05:00) Ashgabat, Tashkent;West Asia Standard Time;West Asia Daylight Time;;");
        }

        /// <summary>
        /// Pakistan Standard Time
        /// (UTC+05:00) Islamabad, Karachi
        /// </summary>
        public static TimeZoneInfo GetPakistanStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Pakistan Standard Time;300;(UTC+05:00) Islamabad, Karachi;Pakistan Standard Time;Pakistan Daylight Time;[01:01:2008;12:31:2008;60;[0;23:59:59.999;5;5;6;];[0;23:59:59.999;10;5;5;];][01:01:2009;12:31:2009;60;[0;23:59:59.999;4;2;2;];[0;23:59:59.999;10;5;6;];];");
        }

        /// <summary>
        /// India Standard Time
        /// (UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi
        /// </summary>
        public static TimeZoneInfo GetIndiaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("India Standard Time;330;(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi;India Standard Time;India Daylight Time;;");
        }

        /// <summary>
        /// Sri Lanka Standard Time
        /// (UTC+05:30) Sri Jayawardenepura
        /// </summary>
        public static TimeZoneInfo GetSriLankaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Sri Lanka Standard Time;330;(UTC+05:30) Sri Jayawardenepura;Sri Lanka Standard Time;Sri Lanka Daylight Time;;");
        }

        /// <summary>
        /// Nepal Standard Time
        /// (UTC+05:45) Kathmandu
        /// </summary>
        public static TimeZoneInfo GetNepalStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Nepal Standard Time;345;(UTC+05:45) Kathmandu;Nepal Standard Time;Nepal Daylight Time;;");
        }

        /// <summary>
        /// Central Asia Standard Time
        /// (UTC+06:00) Astana
        /// </summary>
        public static TimeZoneInfo GetCentralAsiaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Central Asia Standard Time;360;(UTC+06:00) Astana;Central Asia Standard Time;Central Asia Daylight Time;;");
        }

        /// <summary>
        /// Bangladesh Standard Time
        /// (UTC+06:00) Dhaka
        /// </summary>
        public static TimeZoneInfo GetBangladeshStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Bangladesh Standard Time;360;(UTC+06:00) Dhaka;Bangladesh Standard Time;Bangladesh Daylight Time;[01:01:2009;12:31:2009;60;[0;23:00:00;6;3;5;];[0;23:59:00;12;5;4;];];");
        }

        /// <summary>
        /// Ekaterinburg Standard Time
        /// (UTC+06:00) Ekaterinburg
        /// </summary>
        public static TimeZoneInfo GetEkaterinburgStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Ekaterinburg Standard Time;360;(UTC+06:00) Ekaterinburg;Ekaterinburg Standard Time;Ekaterinburg Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// Myanmar Standard Time
        /// (UTC+06:30) Yangon (Rangoon)
        /// </summary>
        public static TimeZoneInfo GetMyanmarStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Myanmar Standard Time;390;(UTC+06:30) Yangon (Rangoon);Myanmar Standard Time;Myanmar Daylight Time;;");
        }

        /// <summary>
        /// SE Asia Standard Time
        /// (UTC+07:00) Bangkok, Hanoi, Jakarta
        /// </summary>
        public static TimeZoneInfo GetSEAsiaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("SE Asia Standard Time;420;(UTC+07:00) Bangkok, Hanoi, Jakarta;SE Asia Standard Time;SE Asia Daylight Time;;");
        }

        /// <summary>
        /// N. Central Asia Standard Time
        /// (UTC+07:00) Novosibirsk
        /// </summary>
        public static TimeZoneInfo GetNCentralAsiaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("N. Central Asia Standard Time;420;(UTC+07:00) Novosibirsk;N. Central Asia Standard Time;N. Central Asia Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// China Standard Time
        /// (UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi
        /// </summary>
        public static TimeZoneInfo GetChinaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("China Standard Time;480;(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi;China Standard Time;China Daylight Time;;");
        }

        /// <summary>
        /// North Asia Standard Time
        /// (UTC+08:00) Krasnoyarsk
        /// </summary>
        public static TimeZoneInfo GetNorthAsiaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("North Asia Standard Time;480;(UTC+08:00) Krasnoyarsk;North Asia Standard Time;North Asia Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// Singapore Standard Time
        /// (UTC+08:00) Kuala Lumpur, Singapore
        /// </summary>
        public static TimeZoneInfo GetSingaporeStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Singapore Standard Time;480;(UTC+08:00) Kuala Lumpur, Singapore;Malay Peninsula Standard Time;Malay Peninsula Daylight Time;;");
        }

        /// <summary>
        /// W. Australia Standard Time
        /// (UTC+08:00) Perth
        /// </summary>
        public static TimeZoneInfo GetWAustraliaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("W. Australia Standard Time;480;(UTC+08:00) Perth;W. Australia Standard Time;W. Australia Daylight Time;[01:01:2006;12:31:2006;60;[0;02:00:00;12;1;0;];[0;00:00:00;1;1;0;];][01:01:2007;12:31:2007;60;[0;02:00:00;10;5;0;];[0;03:00:00;3;5;0;];][01:01:2008;12:31:2008;60;[0;02:00:00;10;5;0;];[0;03:00:00;3;5;0;];][01:01:2009;12:31:2009;60;[0;00:00:00;1;1;4;];[0;03:00:00;3;5;0;];];");
        }

        /// <summary>
        /// Taipei Standard Time
        /// (UTC+08:00) Taipei
        /// </summary>
        public static TimeZoneInfo GetTaipeiStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Taipei Standard Time;480;(UTC+08:00) Taipei;Taipei Standard Time;Taipei Daylight Time;;");
        }

        /// <summary>
        /// Ulaanbaatar Standard Time
        /// (UTC+08:00) Ulaanbaatar
        /// </summary>
        public static TimeZoneInfo GetUlaanbaatarStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Ulaanbaatar Standard Time;480;(UTC+08:00) Ulaanbaatar;Ulaanbaatar Standard Time;Ulaanbaatar Daylight Time;;");
        }

        /// <summary>
        /// North Asia East Standard Time
        /// (UTC+09:00) Irkutsk
        /// </summary>
        public static TimeZoneInfo GetNorthAsiaEastStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("North Asia East Standard Time;540;(UTC+09:00) Irkutsk;North Asia East Standard Time;North Asia East Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// Tokyo Standard Time
        /// (UTC+09:00) Osaka, Sapporo, Tokyo
        /// </summary>
        public static TimeZoneInfo GetTokyoStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Tokyo Standard Time;540;(UTC+09:00) Osaka, Sapporo, Tokyo;Tokyo Standard Time;Tokyo Daylight Time;;");
        }

        /// <summary>
        /// Korea Standard Time
        /// (UTC+09:00) Seoul
        /// </summary>
        public static TimeZoneInfo GetKoreaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Korea Standard Time;540;(UTC+09:00) Seoul;Korea Standard Time;Korea Daylight Time;;");
        }

        /// <summary>
        /// Cen. Australia Standard Time
        /// (UTC+09:30) Adelaide
        /// </summary>
        public static TimeZoneInfo GetCenAustraliaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Cen. Australia Standard Time;570;(UTC+09:30) Adelaide;Cen. Australia Standard Time;Cen. Australia Daylight Time;[01:01:0001;12:31:2007;60;[0;02:00:00;10;5;0;];[0;03:00:00;3;5;0;];][01:01:2008;12:31:9999;60;[0;02:00:00;10;1;0;];[0;03:00:00;4;1;0;];];");
        }

        /// <summary>
        /// AUS Central Standard Time
        /// (UTC+09:30) Darwin
        /// </summary>
        public static TimeZoneInfo GetAUSCentralStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("AUS Central Standard Time;570;(UTC+09:30) Darwin;AUS Central Standard Time;AUS Central Daylight Time;;");
        }

        /// <summary>
        /// E. Australia Standard Time
        /// (UTC+10:00) Brisbane
        /// </summary>
        public static TimeZoneInfo GetEAustraliaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("E. Australia Standard Time;600;(UTC+10:00) Brisbane;E. Australia Standard Time;E. Australia Daylight Time;;");
        }

        /// <summary>
        /// AUS Eastern Standard Time
        /// (UTC+10:00) Canberra, Melbourne, Sydney
        /// </summary>
        public static TimeZoneInfo GetAUSEasternStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("AUS Eastern Standard Time;600;(UTC+10:00) Canberra, Melbourne, Sydney;AUS Eastern Standard Time;AUS Eastern Daylight Time;[01:01:0001;12:31:2007;60;[0;02:00:00;10;5;0;];[0;03:00:00;3;5;0;];][01:01:2008;12:31:9999;60;[0;02:00:00;10;1;0;];[0;03:00:00;4;1;0;];];");
        }

        /// <summary>
        /// West Pacific Standard Time
        /// (UTC+10:00) Guam, Port Moresby
        /// </summary>
        public static TimeZoneInfo GetWestPacificStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("West Pacific Standard Time;600;(UTC+10:00) Guam, Port Moresby;West Pacific Standard Time;West Pacific Daylight Time;;");
        }

        /// <summary>
        /// Tasmania Standard Time
        /// (UTC+10:00) Hobart
        /// </summary>
        public static TimeZoneInfo GetTasmaniaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Tasmania Standard Time;600;(UTC+10:00) Hobart;Tasmania Standard Time;Tasmania Daylight Time;[01:01:0001;12:31:2007;60;[0;02:00:00;10;1;0;];[0;03:00:00;3;5;0;];][01:01:2008;12:31:9999;60;[0;02:00:00;10;1;0;];[0;03:00:00;4;1;0;];];");
        }

        /// <summary>
        /// Yakutsk Standard Time
        /// (UTC+10:00) Yakutsk
        /// </summary>
        public static TimeZoneInfo GetYakutskStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Yakutsk Standard Time;600;(UTC+10:00) Yakutsk;Yakutsk Standard Time;Yakutsk Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// Central Pacific Standard Time
        /// (UTC+11:00) Solomon Is., New Caledonia
        /// </summary>
        public static TimeZoneInfo GetCentralPacificStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Central Pacific Standard Time;660;(UTC+11:00) Solomon Is., New Caledonia;Central Pacific Standard Time;Central Pacific Daylight Time;;");
        }

        /// <summary>
        /// Vladivostok Standard Time
        /// (UTC+11:00) Vladivostok
        /// </summary>
        public static TimeZoneInfo GetVladivostokStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Vladivostok Standard Time;660;(UTC+11:00) Vladivostok;Vladivostok Standard Time;Vladivostok Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// New Zealand Standard Time
        /// (UTC+12:00) Auckland, Wellington
        /// </summary>
        public static TimeZoneInfo GetNewZealandStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("New Zealand Standard Time;720;(UTC+12:00) Auckland, Wellington;New Zealand Standard Time;New Zealand Daylight Time;[01:01:0001;12:31:2006;60;[0;02:00:00;10;1;0;];[0;03:00:00;3;3;0;];][01:01:2007;12:31:2007;60;[0;02:00:00;9;5;0;];[0;03:00:00;3;3;0;];][01:01:2008;12:31:9999;60;[0;02:00:00;9;5;0;];[0;03:00:00;4;1;0;];];");
        }

        /// <summary>
        /// UTC+12
        /// (UTC+12:00) Coordinated Universal Time+12
        /// </summary>
        public static TimeZoneInfo GetUTC12()
        {
            return TimeZoneInfo.FromSerializedString("UTC+12;720;(UTC+12:00) Coordinated Universal Time+12;UTC+12;UTC+12;;");
        }

        /// <summary>
        /// Fiji Standard Time
        /// (UTC+12:00) Fiji
        /// </summary>
        public static TimeZoneInfo GetFijiStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Fiji Standard Time;720;(UTC+12:00) Fiji;Fiji Standard Time;Fiji Daylight Time;[01:01:2009;12:31:2009;60;[0;02:00:00;11;5;0;];[0;00:00:00;1;1;4;];][01:01:2010;12:31:2010;60;[0;02:00:00;10;4;0;];[0;03:00:00;3;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;10;4;0;];[0;03:00:00;3;1;0;];][01:01:2012;12:31:2012;60;[0;02:00:00;10;3;0;];[0;03:00:00;1;4;0;];][01:01:2013;12:31:2013;60;[0;02:00:00;10;4;0;];[0;03:00:00;1;3;0;];][01:01:2014;12:31:9999;60;[0;02:00:00;10;4;0;];[0;03:00:00;1;4;0;];];");
        }

        /// <summary>
        /// Magadan Standard Time
        /// (UTC+12:00) Magadan
        /// </summary>
        public static TimeZoneInfo GetMagadanStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Magadan Standard Time;720;(UTC+12:00) Magadan;Magadan Standard Time;Magadan Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];];");
        }

        /// <summary>
        /// Kamchatka Standard Time
        /// (UTC+12:00) Petropavlovsk-Kamchatsky - Old
        /// </summary>
        public static TimeZoneInfo GetKamchatkaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Kamchatka Standard Time;720;(UTC+12:00) Petropavlovsk-Kamchatsky - Old;Kamchatka Standard Time;Kamchatka Daylight Time;[01:01:0001;12:31:9999;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];];");
        }

        /// <summary>
        /// Tonga Standard Time
        /// (UTC+13:00) Nuku'alofa
        /// </summary>
        public static TimeZoneInfo GetTongaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Tonga Standard Time;780;(UTC+13:00) Nuku'alofa;Tonga Standard Time;Tonga Daylight Time;;");
        }

        /// <summary>
        /// Samoa Standard Time
        /// (UTC+13:00) Samoa
        /// </summary>
        public static TimeZoneInfo GetSamoaStandardTime()
        {
            return TimeZoneInfo.FromSerializedString("Samoa Standard Time;780;(UTC+13:00) Samoa;Samoa Standard Time;Samoa Daylight Time;[01:01:2010;12:31:2010;60;[0;23:59:59.999;9;5;6;];[0;00:00:00;1;1;5;];][01:01:2011;12:31:2011;60;[0;00:00:00;9;5;0;];[0;01:00:00;4;1;0;];][01:01:2012;12:31:9999;60;[0;00:00:00;9;5;0;];[0;01:00:00;4;1;0;];];");
        }

    }
}
