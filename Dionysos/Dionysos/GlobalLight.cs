using Dionysos.Configuration;
using Dionysos.Interfaces;
using Dionysos.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
    public class GlobalLight
    {
        #region Constructor / Singleton

        private static readonly GlobalLight instance = new GlobalLight();

        private GlobalLight()
        {
        }

        public static GlobalLight Instance
        {
            get
            {
                return GlobalLight.instance;
            }
        }

        #endregion

        private Configuration.ConfigurationInfoCollection configurationInfo = new ConfigurationInfoCollection();
        private IApplicationInfo applicationInfo = null;
        private ILoggingProvider loggingProvider = new BlackHoleLogger(); // Init something, to prevent null errors.
        private IConfigurationProvider configurationProvider = null;

        /// <summary>
        /// Gets or sets the Dionysos.Interfaces.IApplicationInfo instance which contains information about the application
        /// </summary>
        public static IApplicationInfo ApplicationInfo
        {
            get
            {
                if (System.Instance.Empty(instance.applicationInfo))
                {
                    throw new ApplicationInfoNotSetException("GlobalLight.ApplicationInfo not set.");
                }
                return instance.applicationInfo;
            }
            set
            {
                instance.applicationInfo = value;
            }
        }

        public static ILoggingProvider LoggingProvider
        {
            get
            {
                if (System.Instance.Empty(instance.applicationInfo))
                {
                    throw new ApplicationInfoNotSetException("GlobalLight.LoggingProvider not set.");
                }
                return instance.loggingProvider;
            }
            set
            {
                instance.loggingProvider = value;
            }
        }

        /// <summary>
        /// Gets or sets the Dionysos.Configuration.ConfigurationInfoCollection instance
        /// </summary>
        public static ConfigurationInfoCollection ConfigurationInfo
        {
            get
            {
                return Instance.configurationInfo;
            }
            set
            {
                Instance.configurationInfo = value;
            }
        }

        /// <summary>
        /// Gets or sets the IConfigurationProvider instance
        /// </summary>
        public static IConfigurationProvider ConfigurationProvider
        {
            get
            {
                if (System.Instance.Empty(instance.configurationProvider))
                {
                    throw new ConfigurationProviderNotSetException("Configuration provider not set!");
                }
                else
                {
                    return instance.configurationProvider;
                }
            }
            set
            {
                instance.configurationProvider = value;
            }
        }
    }
}
