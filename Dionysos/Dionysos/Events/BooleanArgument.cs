using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public class EventArgumentBool : EventArgs
    {
        public EventArgumentBool(bool value)
        {
            this.Value = value;
        }

        public bool Value {get;set;}
    }
}
