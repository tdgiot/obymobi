using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Globalization;

namespace Dionysos.IO
{
	/// <summary>
	/// Static class for file operations
	/// </summary>
	public static class FileUtil
	{
		#region Fields

		/// <summary>
		/// The file size units used for formatting sizes.
		/// </summary>
		private static readonly List<string> fileSizeUnits = new List<string>() { "B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

		#endregion

		#region Methods

		/// <summary>
		/// Checks whether a file exists on disk
		/// </summary>
		/// <param name="file">The path to the file</param>
		/// <returns>True if the file exists, False if not</returns>
		public static bool FileExists(string file)
		{
			return System.IO.File.Exists(file);
		}


		/// <summary>
		/// Deletes the specified file.
		/// </summary>
		/// <param name="file">The file.</param>
		public static void Delete(string file)
		{
			System.IO.File.Delete(file);
		}

		/// <summary>
		/// Get all files in a directory and it's subdirectories
		/// </summary>
		/// <param name="directoryPath">Directory Path</param>
		/// <returns>List with files</returns>
		public static List<string> GetFilesRecursive(string directoryPath)
		{
			// found at: http://www.dotnetperls.com/recursively-find-files
			// 1.
			// Store results in the file results list.
			List<string> result = new List<string>();

			// 2.
			// Store a stack of our directories.
			Stack<string> stack = new Stack<string>();

			// 3.
			// Add initial directory.
			stack.Push(directoryPath);

			// 4.
			// Continue while there are directories to process
			while (stack.Count > 0)
			{
				// A.
				// Get top directory
				string dir = stack.Pop();

				try
				{
					// B
					// Add all files at this directory to the result List.
					result.AddRange(Directory.GetFiles(dir, "*.*"));

					// C
					// Add all directories at this directory.
					foreach (string dn in Directory.GetDirectories(dir))
					{
						stack.Push(dn);
					}
				}
				catch
				{
					// D
					// Could not open the directory
					throw;
				}
			}

			// More logical:
			result.Reverse();

			return result;
		}

		/// <summary>
		/// Deletes the files and directories recursively.
		/// </summary>
		/// <param name="path">The path.</param>
		public static void DeleteFilesRecursive(string path)
		{
			FileUtil.DeleteFilesRecursive(path, true);
		}

		/// <summary>
		/// Deletes the files and directories recursively.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="deleteDirectory">If set to <c>true</c> also deletes the directory.</param>
		public static void DeleteFilesRecursive(string path, bool deleteDirectory)
		{
			FileUtil.DeleteFilesRecursive(new DirectoryInfo(path), deleteDirectory);
		}

		/// <summary>
		/// Deletes the files and directories recursively.
		/// </summary>
		/// <param name="directoryInfo">The directory info.</param>
		public static void DeleteFilesRecursive(DirectoryInfo directoryInfo)
		{
			FileUtil.DeleteFilesRecursive(directoryInfo, true);
		}

		/// <summary>
		/// Deletes the files and directories recursively.
		/// </summary>
		/// <param name="directoryInfo">The directory info.</param>
		/// <param name="deleteDirectory">If set to <c>true</c> also deletes the directory / directories.</param>
		public static void DeleteFilesRecursive(DirectoryInfo directoryInfo, bool deleteDirectory)
		{
			foreach (DirectoryInfo subDirectoryInfo in directoryInfo.GetDirectories())
			{
				FileUtil.DeleteFilesRecursive(subDirectoryInfo, deleteDirectory); // GK Was True, changed to deleteDirectory
			}

			foreach (FileInfo fileInfo in directoryInfo.GetFiles())
			{
				fileInfo.Attributes = FileAttributes.Normal;
				fileInfo.Delete();
			}

			if (deleteDirectory)
			{
				directoryInfo.Delete();
			}
		}

		/// <summary>
		/// Strips the non file characters.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <returns></returns>
		[Obsolete("Use SanitizeFilename instead")]
		public static string StripNonFileCharacters(string fileName)
		{
			return SanitizeFilename(fileName);
		}

		/// <summary>
		/// Sanitizes the filename.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <returns>The sanitized file name.</returns>
		public static string SanitizeFilename(string fileName)
		{
			// http://stackoverflow.com/questions/146134/how-to-remove-illegal-characters-from-path-and-filenames/146162#146162
			string invalidCharacters = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
			return Regex.Replace(fileName, "[" + Regex.Escape(invalidCharacters) + "]", String.Empty, RegexOptions.Compiled);
		}

		/// <summary>
		/// Replaces the invalid file name and path characters with the specified replacement.
		/// </summary>
		/// <param name="fileName">The file file.</param>
		/// <param name="replacement">The replacement.</param>
		/// <returns>
		/// The file name without invalid characters.
		/// </returns>
		public static string ReplaceInvalidFileNameChars(string fileName, string replacement)
		{
			string invalidCharacters = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
			return Regex.Replace(fileName, "[" + Regex.Escape(invalidCharacters) + "]", replacement ?? String.Empty, RegexOptions.Compiled);
		}

		/// <summary>
		/// Gets the MD5 hash for a file.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <returns></returns>
		public static string GetMD5Hash(string filePath)
		{
            return FileUtil.GetMD5Hash(File.ReadAllBytes(filePath));
		}

        public static string GetMD5Hash(byte[] bytes)
        {
            // http://dotmac.rationalmind.net/2010/08/get-the-md5-hash-of-a-file-in-net/
            using (HashAlgorithm md5 = MD5.Create())
            {
                byte[] computedHash = md5.ComputeHash(bytes);

                StringBuilder sb = new StringBuilder();
                foreach (byte b in computedHash)
                {
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }

		/// <summary>
		/// Formats the size to a readable string.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <returns>
		/// The readable string.
		/// </returns>
		public static string FormatSize(decimal bytes)
		{
			return FileUtil.FormatSize(null, bytes, null);
		}

		/// <summary>
		/// Formats the size to a readable string.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <param name="roundingDecimals">The rounding decimals.</param>
		/// <returns>
		/// The readable string.
		/// </returns>
		public static string FormatSize(decimal bytes, int? roundingDecimals)
		{
			return FileUtil.FormatSize(null, bytes, roundingDecimals);
		}

		/// <summary>
		/// Formats the size to a readable string.
		/// </summary>
		/// <param name="provider">The provider.</param>
		/// <param name="bytes">The bytes.</param>
		/// <returns>
		/// The readable string.
		/// </returns>
		public static string FormatSize(IFormatProvider provider, decimal bytes)
		{
			return FileUtil.FormatSize(provider, bytes, null);
		}

		/// <summary>
		/// Formats the size to a readable string.
		/// </summary>
		/// <param name="provider">The provider.</param>
		/// <param name="bytes">The bytes.</param>
		/// <param name="roundingDecimals">The rounding decimals.</param>
		/// <returns>
		/// The readable string.
		/// </returns>
		public static string FormatSize(IFormatProvider provider, decimal bytes, int? roundingDecimals)
		{
			if (bytes < 0) throw new ArgumentOutOfRangeException("bytes", bytes, "The amount of bytes cannot be negative.");

			string fileSize = null;
			int fileSizeUnitIndex = 0, fileSizeUnitCount = FileUtil.fileSizeUnits.Count;

			do
			{
				decimal size = bytes;
				if (roundingDecimals.HasValue) size = System.Math.Round(size, roundingDecimals.Value);

				fileSize = size.ToString(provider) + " " + FileUtil.fileSizeUnits[fileSizeUnitIndex];

				bytes /= 1024m;
				fileSizeUnitIndex++;
			}
			while (bytes >= 1 && fileSizeUnitIndex < fileSizeUnitCount);

			return fileSize;
		}

		/// <summary>
		/// Copy an existing directory to a new location.
		/// </summary>
		/// <param name="sourceDirName">The directory to copy</param>
		/// <param name="destDirName">Path of the destination directory. Will be created if it doesn't exists</param>
		/// <param name="copySubDirs">When true sub-directories will also be copied. Default is false.</param>
		public static void CopyDirectory(string sourceDirName, string destDirName)
		{
			CopyDirectory(sourceDirName, destDirName, false);
		}

		/// <summary>
		/// Copy an existing directory to a new location.
		/// </summary>
		/// <param name="sourceDirName">The directory to copy</param>
		/// <param name="destDirName">Path of the destination directory. Will be created if it doesn't exists</param>
		/// <param name="copySubDirs">When true sub-directories will also be copied. Default is false.</param>
		public static void CopyDirectory(string sourceDirName, string destDirName, bool copySubDirs)
		{
			DirectoryInfo dir = new DirectoryInfo(sourceDirName);
			DirectoryInfo[] dirs = dir.GetDirectories();

			if (!dir.Exists)
			{
				throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
			}

			if (!Directory.Exists(destDirName))
			{
				Directory.CreateDirectory(destDirName);
			}

			FileInfo[] files = dir.GetFiles();
			foreach (FileInfo file in files)
			{
				string temppath = Path.Combine(destDirName, file.Name);
				file.CopyTo(temppath, false);
			}

			if (copySubDirs)
			{
				foreach (DirectoryInfo subdir in dirs)
				{
					string temppath = Path.Combine(destDirName, subdir.Name);
					CopyDirectory(subdir.FullName, temppath, copySubDirs);
				}
			}
		}

		#endregion
	}
}