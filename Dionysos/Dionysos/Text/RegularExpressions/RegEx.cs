using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Dionysos.Text.RegularExpressions
{
	/// <summary>
	/// Collection of static RegEx checking functions
	/// </summary>
	public class RegEx
	{
		/// <summary>
		/// Retrieve a zipcode Regex for the current region.
		/// </summary>
		/// <returns>A Regex instance to validate zipcodes.</returns>
		/// <exception cref="NotImplementedException">The specified region is not supported.</exception>
		public static Regex ZipcodeRegEx()
		{
			return ZipcodeRegEx(CultureInfo.CurrentUICulture);
		}

		/// <summary>
		/// Retrieve a zipcode Regex for the region of the specified culture.
		/// </summary>
		/// <param name="cultureInfo">The culture to get the Regex for.</param>
		/// <returns>A Regex instance to validate zipcodes.</returns>
		/// <exception cref="NotImplementedException">The specified region is not supported.</exception>
		public static Regex ZipcodeRegEx(CultureInfo cultureInfo)
		{
			return ZipcodeRegEx(new RegionInfo(cultureInfo.LCID));
		}

		/// <summary>
		/// Retrieve a zipcode Regex for the specified region.
		/// </summary>
		/// <param name="regionInfo">The region to get the Regex for.</param>
		/// <returns>A Regex instance to validate zipcodes.</returns>
		/// <exception cref="NotImplementedException">The specified region is not supported.</exception>
		public static Regex ZipcodeRegEx(RegionInfo regionInfo)
		{
			switch (regionInfo.Name)
			{
				case "NL":
					// Dutch zipcodes (4 digits ranging from 1000 to 9999, an optional space or more and 2 letters).
					// GK Replaced \s? with \s*
					return new Regex(@"^(?<Numbers>[1-9][0-9]{3})\s*(?<Characters>[a-zA-Z]{2})$");
				case "BE":
					// Belgium zipcodes (4 digits ranging from 1000 to 9992)
					return new Regex(@"^(?<Numbers>[1-9][0-9]{3})$");
				case "FR":
					// France zipcodes (5 digits ranging from 00000 to 99999)
					return new Regex(@"^(?<Numbers>[0-9]{5})$");
				case "ES":
					// Spanish zipcode (5 digits ranging from 01000 to 52000)
					return new Regex(@"^(?<Numbers>[0-5][0-9]{4})$");
				default:
					return null;
			}
		}

		/// <summary>
		/// Indicates whether the specified string is a zipcode in the current region.
		/// </summary>
		/// <param name="input">The string to check.</param>
		/// <returns>true if the specified string is a valid zipcode; otherwise false.</returns>
		/// <exception cref="NotImplementedException">The specified region is not supported.</exception>
		public static bool IsZipcode(string input)
		{
			return IsZipcode(input, CultureInfo.CurrentUICulture);
		}

		/// <summary>
		/// Indicates whether the specified string is a zipcode in the region of the specified culture.
		/// </summary>
		/// <param name="input">The string to check.</param>
		/// <param name="cultureInfo">The culture to check the string against.</param>
		/// <returns>true if the specified string is a valid zipcode; otherwise false.</returns>
		/// <exception cref="NotImplementedException">The specified region is not supported.</exception>
		public static bool IsZipcode(string input, CultureInfo cultureInfo)
		{
			return IsZipcode(input, new RegionInfo(cultureInfo.LCID));
		}

		/// <summary>
		/// Indicates whether the specified string is a zipcode in the specified region.
		/// </summary>
		/// <param name="input">The string to check.</param>
		/// <param name="regionInfo">The region to check the string against.</param>
		/// <returns>true if the specified string is a valid zipcode; otherwise false.</returns>
		/// <exception cref="NotImplementedException">The RegionInfo.CurrentRegion is not supported.</exception>
		public static bool IsZipcode(string input, RegionInfo regionInfo)
		{
			bool supported, valid = RegEx.IsZipcode(input, regionInfo, out supported);
			if (!supported) throw new NotImplementedException("ZipcodeRegEx not implemented for region: " + regionInfo.EnglishName);
			return valid;
		}

		/// <summary>
		/// Indicates whether the specified string is a zipcode in the specified region.
		/// </summary>
		/// <param name="input">The string to check.</param>
		/// <param name="regionInfo">The region to check the string against.</param>
		/// <param name="supported">If set to <c>true</c>, indicates that the region is supported.</param>
		/// <returns>
		/// true if the specified string is a valid zipcode; otherwise false.
		/// </returns>
		/// <exception cref="NotImplementedException">The RegionInfo.CurrentRegion is not supported.</exception>
		public static bool IsZipcode(string input, RegionInfo regionInfo, out bool supported)
		{
			Regex zipcodeRegex = RegEx.ZipcodeRegEx(regionInfo);

			// Set supported flag
			supported = zipcodeRegex != null;

			if (zipcodeRegex != null) return zipcodeRegex.IsMatch(input);
			else return false;
		}

		/// <summary>
		/// Check if supplied string is a integer
		/// </summary>
		/// <param name="textToCheck">String to check</param>
		/// <returns>boolean showing if it's a valid integer</returns>
		public static bool IsInteger(string textToCheck)
		{
			Regex integerFormat = new Regex(CommonRegExPatterns.IntegerPattern);
			return integerFormat.IsMatch(textToCheck);
		}

		/// <summary>
		/// Check if supplied string is a valid url address
		/// </summary>
		/// <param name="textToCheck">String to check</param>
		/// <returns>boolean showing if it's a valid url address</returns>
		public static bool IsEmailOrUrl(string textToCheck)
		{
			return (RegEx.IsEmail(textToCheck) || RegEx.IsUrl(textToCheck));
		}

        /// <summary>
        /// Check if supplied string is a valid url address
        /// </summary>
        /// <param name="textToCheck">String to check</param>
        /// <returns>boolean showing if it's a valid url address</returns>
        public static bool IsAdvancedUrl(string textToCheck)
        {
            Regex urlFormat = new Regex(CommonRegExPatterns.AdvancedUrlPattern);
            return urlFormat.IsMatch(textToCheck);
        }

		/// <summary>
		/// Check if supplied string is a valid url address
		/// </summary>
		/// <param name="textToCheck">String to check</param>
		/// <returns>boolean showing if it's a valid url address</returns>
		public static bool IsUrl(string textToCheck)
		{
			Regex urlFormat = new Regex(CommonRegExPatterns.UrlPattern);
			return urlFormat.IsMatch(textToCheck);
		}

		/// <summary>
		/// Check if supplied string is a valid email address
		/// </summary>
		/// <param name="textToCheck">String to check</param>
		/// <returns>boolean showing if it's a valid emailaddress</returns>
		public static bool IsEmail(string textToCheck)
		{
			Regex mailFormat = new Regex(CommonRegExPatterns.EmailPattern);
			return mailFormat.IsMatch(textToCheck);
		}

		/// <summary>
		/// Check if supplied string is a valid time
		/// </summary>
		/// <param name="textToCheck">Time</param>
		/// <returns>boolean indicating if it's a time</returns>
		public static bool IsTime(string textToCheck)
		{
			Regex timeFormat = new Regex(CommonRegExPatterns.Time24h);
			return timeFormat.IsMatch(textToCheck);
		}

		/// <summary>
		/// Check if supplied string is a valid time
		/// </summary>
		/// <param name="textToCheck">Time</param>
		/// <param name="hours">Hours (0 - 23)</param>
		/// <param name="minutes">Minutes (0 - 59)</param>
		/// <returns>boolean indicating if it's a time</returns>
		public static bool IsTime(string textToCheck, out int hours, out int minutes)
		{
			bool toReturn = false;
			hours = -1;
			minutes = -1;

			Regex timeFormat = new Regex(CommonRegExPatterns.Time24h);
			if (timeFormat.IsMatch(textToCheck))
			{
				toReturn = true;
				Match m = timeFormat.Match(textToCheck);
				hours = Convert.ToInt32(m.Groups[1].Value); // We start at 1, becuase 0 is the full match
				minutes = Convert.ToInt32(m.Groups[2].Value);
			}

			return toReturn;
		}

		/// <summary>
		/// Check if supplied string is a valid time and return a DateTime object containing the time of the textToCheck and the Date of today
		/// </summary>
		/// <param name="textToCheck">Time</param>
		/// <param name="time">DateTime object containing the time of the textToCheck and the Date of today</param>
		/// <returns>boolean indicating if it's a time</returns>
		public static bool IsTime(string textToCheck, out DateTime? time)
		{
			bool toReturn = false;
			time = null;
			int hours, minutes;

			if (IsTime(textToCheck, out hours, out minutes))
			{
				toReturn = true;
				DateTime forTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hours, minutes, 0);
				time = DateTimeUtil.CombineDateAndTime(forTime, DateTime.Now);
			}

			return toReturn;
		}


		/// <summary>
		/// Check if a string is an url, and when the scheme is missing we add "http://"
		/// </summary>
		/// <param name="textToCheck">Url to check</param>
		/// <param name="correctToUsableUrl">Boolean indicating if it should be corrected if wrong</param>
		/// <param name="correctUrl">Corrected url if it was wrong</param>
		/// <returns>If the textToCheck was an (incomplete) url.</returns>
		public static bool IsUrl(string textToCheck, bool correctToUsableUrl, out string correctUrl)
		{
			bool toReturn = false;
			correctUrl = textToCheck;

			// Check for URL
			Regex regexUrl = new Regex(CommonRegExPatterns.AdvancedUrlPattern, RegexOptions.ExplicitCapture);
			Match matchedUrl = regexUrl.Match(textToCheck);

			if (matchedUrl.Success)
			{
				// It's a URL, so return true in the end
				toReturn = true;

				// Check if the URL has to be corrected
				if (correctToUsableUrl)
				{
					if (matchedUrl.Groups["scheme"].Value.ToLowerInvariant() == "mailto")
					{
						// Don't correct mailto links
					}
					else if (matchedUrl.Groups["scheme"].Value.ToLowerInvariant() == "tel")
					{
						// Don't correct mailto links
					}
					else if (matchedUrl.Groups["scheme"].Value.Length == 0 && matchedUrl.Groups["host"].Value.Length > 0)
					{
						// Correct link that's missing the http sheme
						// and has a host part (so www.domain.com will become http://www.domain.com
						correctUrl = "http://" + textToCheck;
					}
				}
			}

			return toReturn;
		}

        /// <summary>
        /// Check if supplied string is a valid telephonenumber
        /// </summary>
        /// <param name="phonenumber">Phonenumber to check</param>
        /// <param name="correctPhonenumber">Correct phonenumber</param>
        /// <returns>boolean indicating if it's a time</returns>
        public static bool IsTelephonenumber(string phonenumber, out string correctPhonenumber)
        {
            bool success = true;

            correctPhonenumber = string.Empty;
            if (!Instance.Empty(phonenumber))
            {
                Regex phoneExpression = new Regex(@"^0(\d{2}-\d{7})|(\d{2}\s\d{7})|(\d{3}-\d{6})|(\d{10})|(\d{3}\s\d{6})|(6-\d{8})|(6\s\d{8})|(d{9})$");
                if (phoneExpression.IsMatch(phonenumber))
                {
                    correctPhonenumber = phonenumber;
                }
                else
                {
                    success = false;
                }
            }

            return success;
        }

		/// <summary>
		/// Retrieve an array of matches as output form a pattern on a string
		/// </summary>
		/// <param name="source">String containing the elements to be matched</param>
		/// <param name="matchPattern">RegEx Pattern to match with</param>
		/// <param name="findAllUnique">Boolean indicating if all should be added or only inique values</param>
		/// <returns>An array of matches</returns>
		public static Match[] FindSubstrings(string source, string matchPattern, bool findAllUnique)
		{
			SortedList uniqueMatches = new SortedList();
			Match[] retArray = null;
			Regex RE = new Regex(matchPattern, RegexOptions.Multiline);
			MatchCollection theMatches = RE.Matches(source);

			if (findAllUnique)
			{
				for (int counter = 0; counter < theMatches.Count; counter++)
				{
					if (!uniqueMatches.ContainsKey(theMatches[counter].Value))
					{
						uniqueMatches.Add(theMatches[counter].Value,
						theMatches[counter]);
					}
				}
				retArray = new Match[uniqueMatches.Count];
				uniqueMatches.Values.CopyTo(retArray, 0);
			}
			else
			{
				retArray = new Match[theMatches.Count];
				theMatches.CopyTo(retArray, 0);
			}

			return (retArray);
		}

        public static List<string> GetSubStrings(string source, string start, string end)
        {
            var reg = new Regex(string.Format("(?<={0})(.+?)(?={1})", start, end));
            MatchCollection matches = reg.Matches(source);
            
            var list = new List<string>();
            foreach (Match m in matches)
            {
                list.Add(m.Value);
            }

            return list;
        }
	}
}
