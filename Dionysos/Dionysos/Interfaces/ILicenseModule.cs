using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Interfaces
{
	/// <summary>
	/// Interface which is used to implement licensing for modules
	/// </summary>
	public interface ILicenseModule
	{
		/// <summary>
		/// The system name of the module
		/// </summary>
		string NameSystem { get; set; }

		/// <summary>
		/// The full name of the module
		/// </summary>
		string NameFull { get; set; }

		/// <summary>
		/// The short name of the module
		/// </summary>
		string NameShort { get; set; }

		/// <summary>
		/// The path to the icon of the module
		/// </summary>
		string IconPath { get; set; }

		/// <summary>
		/// Flag which indicates whether the module is licensed or not
		/// </summary>
		bool IsLicensed { get; }

		/// <summary>
		/// Gets or sets the UIElements.
		/// </summary>
		/// <value>The UIElements.</value>
		ICollection<ILicenseUIElement> UIElements { get; }

		/// <summary>
		/// Gets or sets the <see cref="Dionysos.Interfaces.ILicenseUIElement"/> with the specified NameSystem.
		/// </summary>
		/// <value>The <see cref="Dionysos.Interfaces.ILicenseUIElement"/> with the specified NameSystem.</value>
		ILicenseUIElement this[string nameSystem] { get; }
	}
}
