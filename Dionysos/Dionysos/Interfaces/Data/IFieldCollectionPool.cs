using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for IFieldCollection collection
    /// </summary>
    public interface IFieldCollectionPool
    {
        #region Properties

        /// <summary>
        /// Gets an IFieldCollection instance from this pool
        /// </summary>
        /// <param name="index">The index of the IFieldCollection instance in the pool</param>
        /// <returns>An IField instance</returns>
        IFieldCollection this[int index]
        {
            get;
        }

        /// <summary>
        /// Gets an IFieldCollection instance from this pool
        /// </summary>
        /// <param name="name">The name of the IFieldCollection instance in the pool</param>
        /// <returns>An IFieldCollection instance</returns>
        IFieldCollection this[string name]
        {
            get;
        }

        #endregion
    }
}
