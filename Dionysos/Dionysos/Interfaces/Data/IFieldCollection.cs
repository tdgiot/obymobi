using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for IField collection
    /// </summary>
    public interface IFieldCollection
    {
        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        int Count
        {
            get;
        }

        /// <summary>
        /// Gets or sets the name of the entity
        /// </summary>
        string EntityName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets an IField instance from the collection
        /// </summary>
        /// <param name="index">The index of the IField instance in the collection</param>
        /// <returns>An IField instance</returns>
        IField this[int index]
        {
            get;
        }

        /// <summary>
        /// Gets an IField instance from the collection
        /// </summary>
        /// <param name="name">The name of the IField instance in the collection</param>
        /// <returns>An IField instance</returns>
        IField this[string name]
        {
            get;
        }

        #endregion
    }
}
