using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for data fields
    /// </summary>
    public interface IField
    {
        #region Properties

        /// <summary>
        /// The name of the data field
        /// </summary>
        string Name
        {
            get;
        }

        /// <summary>
        /// The System.Type of the data field
        /// </summary>
        System.Type DataType
        {
            get;
        }

        #endregion
    }
}
