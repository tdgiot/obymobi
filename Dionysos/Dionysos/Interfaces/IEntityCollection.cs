using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
	///// <summary>
	///// Interface for database records collection object
	///// </summary>
	//public interface IEntityCollection
	//{
	//    #region Methods

	//    /// <summary>
	//    /// Initializes the entity object
	//    /// </summary>
	//    /// <returns>True if the initialisation was succesfull, False if not</returns>
	//    bool Initialize();

	//    #endregion

	//    #region Properties

	//    /// <summary>
	//    /// Gets or sets the name of the entity
	//    /// </summary>
	//    string EntityName
	//    {
	//        get;
	//        set;
	//    }

	//    /// <summary>
	//    /// Gets or sets the items in the entity collection
	//    /// </summary>
	//    IList Items
	//    {
	//        get;
	//        set;
	//    }

	//    /// <summary>
	//    /// Gets or sets an item in the entity collection at the specified index
	//    /// </summary>
	//    /// <param name="index">The index to get or set the item in the collection</param>
	//    /// <returns>An item of type System.Object</returns>
	//    object this[int index]
	//    {
	//        get;
	//        set;
	//    }

	//    #endregion
	//}
}
