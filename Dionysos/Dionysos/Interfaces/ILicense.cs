using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Dionysos.Interfaces
{
	/// <summary>
	/// Interface which is used to implement licensing
	/// </summary>
	public interface ILicense
	{
		/// <summary>
		/// Gets or sets the modules.
		/// </summary>
		/// <value>The modules.</value>
		ICollection<ILicenseModule> Modules { get; set; }

		/// <summary>
		/// Gets or sets the <see cref="Dionysos.Interfaces.ILicenseModule"/> with the specified NameSystem.
		/// </summary>
		/// <value>The <see cref="Dionysos.Interfaces.ILicenseModule"/> with the specified NameSystem.</value>
		ILicenseModule this[string nameSystem] { get; set; }
	}
}
