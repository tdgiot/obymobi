using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for environment information objects
    /// </summary>
    public interface IEnvironmentInfo
    {
        #region Methods

        /// <summary>
        /// Gets a string which represents the information about the environment
        /// </summary>
        /// <returns>A string containg the environment information</returns>
        string ToString();

        /// <summary>
        /// Gets a XML representation of the information about the environment
        /// </summary>
        void ToXML();

        #endregion
    }
}
