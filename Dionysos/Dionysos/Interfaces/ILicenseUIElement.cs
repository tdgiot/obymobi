using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface which is used to implement licensing for UI elements
    /// </summary>
    public interface ILicenseUIElement
    {
        /// <summary>
        /// The system name of the UI Element
        /// </summary>
        string NameSystem { get; set; }

        /// <summary>
        /// The full name of the UI Element
        /// </summary>
        string NameFull { get; set; }

        /// <summary>
        /// The short name of the UI Element
        /// </summary>
        string NameShort { get; set; }

        /// <summary>
        /// The url of the UI Element
        /// </summary>
        string Url { get; set; }

        /// <summary>
        /// Flag which indicates whether the UI Element should be displayed in a menu
        /// </summary>
        bool DisplayInMenu { get; set; }

        /// <summary>
        /// The full type name of the UI Element
        /// </summary>
        string TypeNameFull { get; set; }

        /// <summary>
        /// The sort order of the UI element
        /// </summary>
        int? SortOrder { get; set; }

        /// <summary>
        /// Flag which indicates whether the UI element is of free access
        /// </summary>
        bool FreeAccess { get; set; }

        /// <summary>
        /// Flag which indicates whether the UI Element is licensed or not
        /// </summary>
        bool IsLicensed { get; }
    }
}
