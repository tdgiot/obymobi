using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces.Configuration
{
    /// <summary>
    /// Interface which is being used to implement application module classes
    /// </summary>
    public interface IModule
    {
        #region Properties

        /// <summary>
        /// Get or sets the name of the Module
        /// </summary>
        string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Collection of related UIElements
        /// </summary>
        IUIElementCollection UIElements
        {
            get;
            set;
        }

        #endregion
    }
}
