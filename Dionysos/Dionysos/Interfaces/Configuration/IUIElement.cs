using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface which is being used to implement UIElement classes
    /// </summary>
    public interface IUIElement
    {
        #region Properties

        /// <summary>
        /// Header/Title text for the UIElement 
        /// </summary>
        string HeaderText
        {
            get;
            set;
        }

        /// <summary>
        /// The text to be displayed on navigation/menu elements pointing to this element
        /// </summary>
        string MenuText
        {
            get;
            set;
        }

        /// <summary>
        /// Can hold a Path/Url to the icon associated with this UIElement
        /// </summary>
        string IconLocation
        {
            get;
            set;
        }

        /// <summary>
        /// Full type name of the object
        /// </summary>
        string TypeName
        {
            get;
            set;
        }

        /// <summary>
        /// This element should be shown in the navigation controls (menu)
        /// </summary>
        bool DisplayInMenu
        {
            get;
            set;
        }

        #endregion
    }
}
