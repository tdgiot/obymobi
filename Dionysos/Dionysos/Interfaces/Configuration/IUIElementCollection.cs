using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface which is being used to implement UIElementCollection classes 
    /// </summary>
    public interface IUIElementCollection
    {
        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        int Count
        {
            get;
        }

        /// <summary>
        /// Gets an IUIElement instance from the collection
        /// </summary>
        /// <param name="index">The index of the IUIElement instance in the collection</param>
        /// <returns>An IUIElement instance</returns>
        IUIElement this[int index]
        {
            get;
        }

        /// <summary>
        /// Gets an IUIElement instance from the collection
        /// </summary>
        /// <param name="name">The name of the IUIElement instance in the collection</param>
        /// <returns>An IUIElement instance</returns>
        IUIElement this[string name]
        {
            get;
        }

        #endregion
    }
}
