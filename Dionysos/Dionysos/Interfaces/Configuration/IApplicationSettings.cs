using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface used for implementing application settings management
    /// </summary>
    public interface IApplicationSettings
    {
        #region Methods

        /// <summary>
        /// Loads the values from the default configuration file
        /// </summary>
        /// <returns>True if loading the values was succesful, False if not</returns>
        bool Load();

        /// <summary>
        /// Loads the values from a specified configuration file
        /// </summary>
        /// <param name="configFile">The path to the file to load the configuration from</param>
        /// <returns>True if loading the values was succesful, False if not</returns>
        bool Load(string configFile);

        /// <summary>
        /// Saves the values of the application settings instance to disk using the default configuration filename
        /// </summary>
        /// <returns>True if saving was succesful, False if not</returns>
        bool Save();

        /// <summary>
        /// Saves the values of the application settings instance to disk using the specified filename
        /// </summary>
        /// <param name="configFile">The path to write the configuration file to</param>
        /// <returns>True if saving was succesful, False if not</returns>
        bool Save(string configFile);

        /// <summary>
        /// Gets the current value of the specified application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Object instance containing the current value</returns>
        object GetValue(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Boolean instance containing the current value</returns>
        bool GetValueAsBoolean(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Byte instance containing the current value</returns>
        byte GetValueAsByte(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Char instance containing the current value</returns>
        char GetValueAsChar(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Decimal instance containing the current value</returns>
        decimal GetValueAsDecimal(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Double instance containing the current value</returns>
        double GetValueAsDouble(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Single instance containing the current value</returns>
        float GetValueAsFloat(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Int32 instance containing the current value</returns>
        int GetValueAsInt(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Int64 instance containing the current value</returns>
        long GetValueAsLong(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.Int16 instance containing the current value</returns>
        short GetValueAsShort(string section, string item);

        /// <summary>
        /// Gets the current value of this application settings item
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <returns>An System.String instance containing the current value</returns>
        string GetValueAsString(string section, string item);

        /// <summary>
        /// Gets the current value of the specifed application settings item. 
        /// If no value has been set, the current value is being set to the default value
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <param name="defaultValue">The default value of this application settings item</param>
        /// <returns>An System.Object instance containing the current value</returns>
        object GetSetValue(string section, string item, object defaultValue);

        /// <summary>
        /// Sets the value of the specified application settings item 
        /// </summary>
        /// <param name="section">The name of the section which contains the application settings item</param>
        /// <param name="item">The name of the application settings item</param>
        /// <param name="value">The value to set to the application settings item</param>
        void SetValue(string section, string item, object value);


        #endregion
    }
}
