using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for membership provider implementations
    /// </summary>
    public interface IMembershipProvider
    {
        #region Methods

        /// <summary>
        /// Gets the id from the user with the specified user name
        /// </summary>
        /// <param name="userName">The username to get the id for</param>
        /// <returns>A System.Int32 instance containing the user id</returns>
        int GetUserIdByUserName(string userName);

        #endregion
    }
}
