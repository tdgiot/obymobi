using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web
{
    public static class WebPathCombiner
    {
        /// <summary>
        /// Combines the paths to a url without a trailing slash
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns></returns>
        private static string CombinePathsDoIt(string a, string b)
        {
            if (a.IsNullOrWhiteSpace())
                return b;
            else if (b.IsNullOrWhiteSpace())
                return a;
            else
            {
                // Make 'a' end without a slash
                if (a.EndsWith("/"))
                    a = a.Substring(0, a.Length - 1);

                // Make 'b' start with a slash
                if (!b.StartsWith("/"))
                    b = "/" + b;

                return a + b;
            }
        }

        /// <summary>
        /// Combines the paths to a url without a trailing slash
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static string CombinePaths(params string[] paths)
        {
            if (paths == null || paths.Length == 0)
                return string.Empty;

            string toReturn = "";

            for (int i = 0; i < paths.Length; i++)
            {
                string path = paths[i];
				toReturn = WebPathCombiner.CombinePathsDoIt(toReturn, path);
            }
            
            return toReturn;
        }
    }
}
