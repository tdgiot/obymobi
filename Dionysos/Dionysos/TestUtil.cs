using Dionysos;
using System.Diagnostics;

namespace System
{
    /// <summary>
    /// Contains methods used in test environments.
    /// </summary>
    public static class TestUtil
    {
        #region Fields

        /// <summary>
        /// The machine name.
        /// </summary>
        private static string machineName;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the machine.
        /// </summary>
        /// <value>
        /// The name of the machine.
        /// </value>
        public static string MachineName
        {
            get
            {
                if (TestUtil.machineName == null)
                {
                    try
                    {
                        TestUtil.machineName = Environment.MachineName;
                    }
                    catch
                    {
                        TestUtil.machineName = String.Empty;
                    }
                }

                return TestUtil.machineName;
            }
        }

        private static bool? isPcDeveloper = null;
        /// <summary>
        /// Gets a value indicating whether this is a development pc.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is a development pc; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcDeveloper
        {
            get
            {
                if (isPcDeveloper.HasValue)
                    return isPcDeveloper.Value;
                else
                    return TestUtil.IsPcGabriel || TestUtil.IsPcRonald || TestUtil.IsPcMathieu || TestUtil.IsPcBattlestation || TestUtil.IsPcFloris || TestUtil.IsPcJosh;
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcDeveloper is set manually to: {0}", value));
                isPcDeveloper = value;
            }
        }

        private static bool? isPcMathieu = null;
        /// <summary>
        /// Gets a value indicating whether the hostname of this pc is Mathieu-PC or PC-Mathieu.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the hostname of this pc is Mathieu-PC or PC-Mathieu; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcMathieu
        {
            get
            {
                if (isPcMathieu.HasValue)
                    return isPcMathieu.Value;
                else
                    return TestUtil.MachineName.Equals("Mathieu-PC", StringComparison.OrdinalIgnoreCase) || TestUtil.MachineName.Equals("PC-Mathieu", StringComparison.OrdinalIgnoreCase);
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcMathieu is set manually to: {0}", value));
                isPcMathieu = value;
            }
        }

        private static bool? isPcBill = null;
        /// <summary>
        /// Gets a value indicating whether this pc is Bill
        /// </summary>   
        public static bool IsPcBill
        {
            get
            {
                if (isPcBill.HasValue)
					return isPcBill.Value;
                else
                    return false;
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcBill is set manually to: {0}", value));
                isPcBill = value;
            }
        }

        private static bool? isPcFloris = null;
        /// <summary>
        /// Gets a value indicating whether the hostname of this pc is Mathieu-PC.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the hostname of this pc is Mathieu-PC; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcFloris
        {
            get
            {
                if (isPcFloris.HasValue)
                    return isPcFloris.Value;
                else
                    return TestUtil.MachineName.Equals("Floris", StringComparison.OrdinalIgnoreCase) || TestUtil.MachineName.Equals("OTTO", StringComparison.OrdinalIgnoreCase);
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcFloris is set manually to: {0}", value));
                isPcFloris = value;
            }
        }

        private static bool? icPcGabriel = null;
        /// <summary>
        /// Gets a value indicating whether the hostname of this pc is Gabriel.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the hostname of this pc is Gabriel; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcGabriel
        {
            get
            {
                if (icPcGabriel.HasValue)
                    return icPcGabriel.Value;
                else
                    return TestUtil.MachineName.Equals("Gabriel", StringComparison.OrdinalIgnoreCase) || TestUtil.MachineName.Equals("MRG", StringComparison.OrdinalIgnoreCase);
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcGabriel is set manually to: {0}", value));
                icPcGabriel = value;
            }
        }

        private static bool? isPcRonald = null;
        /// <summary>
        /// Gets a value indicating whether the hostname of this pc is Ronald.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the hostname of this pc is Ronald; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcRonald
        {
            get
            {
                if (isPcRonald.HasValue)
                    return isPcRonald.Value;
                else
                    return TestUtil.MachineName.Equals("Ronald", StringComparison.OrdinalIgnoreCase);
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcRonald is set manually to: {0}", value));
                isPcRonald = value;
            }
        }

        private static bool? isPcJosh = null;
        /// <summary>
        /// Gets a value indicating whether the hostname of this pc is Josh.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the hostname of this pc is Ronald; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcJosh
        {
            get
            {
                if (isPcJosh.HasValue)
                    return isPcJosh.Value;
                else
                    return TestUtil.MachineName.Contains("Josh", StringComparison.OrdinalIgnoreCase);
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcJosh is set manually to: {0}", value));
                isPcJosh = value;
            }
        }

        private static bool? isPcBattleStationDanny = null;
        /// <summary>
        /// Gets a value indicating whether the machine name of this pc is BattleStationDanny.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the machine name of this pc is BattleStationDanny; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcBattleStationDanny
        {
            get
            {
                if (isPcBattleStationDanny.HasValue)
                    return isPcBattleStationDanny.Value;
                else
                    return TestUtil.MachineName.Equals("BATTLESTATIONDK", StringComparison.OrdinalIgnoreCase);
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcBattleStationDanny is set manually to: {0}", value));
                isPcBattleStationDanny = value;
            }
        }

        private static bool? isPcBattleStationErik = null;
        /// <summary>
        /// Gets a value indicating whether the machine name of this pc is BattleStationErik.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the machine name of this pc is BattleStationErik; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcBattleStationErik
        {
            get
            {
                if (isPcBattleStationErik.HasValue)
                    return isPcBattleStationErik.Value;
                else
                    return TestUtil.MachineName.Equals("BATTLESTATIONES", StringComparison.OrdinalIgnoreCase);
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcBattleStationErik is set manually to: {0}", value));
                isPcBattleStationErik = value;
            }
        }

        private static bool? isPcBattleStation = null;
        /// <summary>
        /// Gets a value indicating whether the machine name of this pc contains Battlestation.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the machine name of this pc contains Battlestation; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPcBattlestation
        {
            get
            {
                if (isPcBattleStation.HasValue)
                    return isPcBattleStation.Value;
                else
                    return StringUtilLight.Contains(TestUtil.MachineName, "BATTLESTATION", StringComparison.OrdinalIgnoreCase);
            }
            set
            {
                Debug.WriteLine(string.Format("WARNING: IsPcBattlestation is set manually to: {0}", value));
                isPcBattleStation = value;
            }
        }

        #endregion
    }
}
