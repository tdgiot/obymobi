﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using XPTable.Models;
using XPTable.Renderers;

namespace Dionysos.Windows.Forms
{
    public class XPTable : Table
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Windows.Forms.XPTable type
        /// </summary>
        public XPTable()
        {
            // Set the defaults
            this.AlternatingRowColor = Color.White;
            this.GridLines = GridLines.Both;
            this.FullRowSelect = true;
            this.SelectionStyle = SelectionStyle.Grid;
            this.EnableToolTips = true;
            this.MultiSelect = false;
            this.HeaderFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            this.HoverTime = 1000;
            this.NoItemsText = "Er bevinden zich geen items in deze lijst.";
            //this.UnfocusedSelectionBackColor = Color.FromArgb(201, 210, 221);
            //this.UnfocusedSelectionForeColor = Color.FromArgb(14, 66, 121);
            this.UnfocusedSelectionBackColor = Color.Transparent;
            this.UnfocusedSelectionForeColor = Color.Transparent;
            this.SortedColumnBackColor = Color.Transparent;

            GradientHeaderRenderer oGradientHeaderRenderer = new GradientHeaderRenderer();
            oGradientHeaderRenderer.ForeColor = Color.Black;
            oGradientHeaderRenderer.StartColor = Color.FromArgb(212, 208, 200);
            oGradientHeaderRenderer.EndColor = Color.FromArgb(212, 208, 200);
            this.HeaderRenderer = oGradientHeaderRenderer;
        }

        #endregion

        #region Methods

        public new void Refresh()
        {
        }

        public void Init()
        {
        }

        public void New()
        {
        }

        public void Edit()
        {
        }

        public void Delete()
        {
        }

        public void SelectById()
        {
        }

        public void GetSelectedRow()
        {
        }

        public void GetSelectedId()
        {
        }

        public void SetOrder(string fieldName)
        {
        }

        public void Search(string searchString, string fieldName)
        {
        }

        public Control GetOwnerForm(Control control)
        {
            //X_FormDataDialog oFormDataDialog = null;
            //X_FormSelectionDialog oFormSelectionDialog = null;
            //X_FormWizardDataDialog oFormWizardDataDialog = null;

            //while (control.Parent != null)
            //{
            //    try
            //    {
            //        oFormDataDialog = (X_FormDataDialog)control.Parent;
            //        break;
            //    }
            //    catch
            //    {
            //        oFormDataDialog = null;

            //        try
            //        {
            //            oFormSelectionDialog = (X_FormSelectionDialog)control.Parent;
            //            break;
            //        }
            //        catch
            //        {
            //            oFormSelectionDialog = null;

            //            try
            //            {
            //                oFormWizardDataDialog = (X_FormWizardDataDialog)control.Parent;
            //                break;
            //            }
            //            catch
            //            {
            //                oFormWizardDataDialog = null;
            //                control = control.Parent;
            //            }
            //        }
            //    }
            //}

            //if (oFormDataDialog != null)
            //{
            //    return oFormDataDialog;
            //}
            //else if (oFormSelectionDialog != null)
            //{
            //    return oFormSelectionDialog;
            //}
            //else if (oFormWizardDataDialog != null)
            //{
            //    return oFormWizardDataDialog;
            //}
            //else
            //{
                return null;
            //}
        }

        public void GoToFirst()
        {
        }

        public void GoToPrevious()
        {
        }

        public void GoToNext()
        {
        }

        public void GoToLast()
        {
        }

        public void SelectRow(int rowNo)
        {
            try
            {
                this.TableModel.Selections.SelectCells(rowNo, 0, rowNo, this.ColumnModel.Columns.Count - 1);
            }
            catch
            {
            }
        }

        #endregion

        #region Properties

        ///// <summary>
        ///// Gets or sets the ColorConditions
        ///// </summary>
        //public X_ColorConditionCollection ColorConditions
        //{
        //    get
        //    {
        //        return this.mColorConditionCollection;
        //    }
        //    set
        //    {
        //        this.mColorConditionCollection = value;
        //    }
        //}

        #endregion

        #region Event handlers

        public void OnItemNew(object sender, EventArgs e)
        {
            if (this.ItemNew != null)
            {
                this.ItemNew(sender, e);
            }
        }

        public void OnItemEdited(object sender, EventArgs e)
        {
            if (this.ItemEdited != null)
            {
                this.ItemEdited(sender, e);
            }
        }

        public void OnItemDeleted(object sender, EventArgs e)
        {
            if (this.ItemDeleted != null)
            {
                this.ItemDeleted(sender, e);
            }
        }

        #endregion

        #region Events

        public event EventHandler ItemNew;
        public event EventHandler ItemEdited;
        public event EventHandler ItemDeleted;

        #endregion
    }
}
