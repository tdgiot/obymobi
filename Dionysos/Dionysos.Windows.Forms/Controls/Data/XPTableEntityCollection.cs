﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Drawing;
using System.Windows.Forms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using XPTable.Models;
using XPTable.Events;
using Dionysos.Reflection;

namespace Dionysos.Windows.Forms
{
    public class XPTableEntityCollection : XPTable
    {
        #region Fields

        private string entityName = string.Empty;
        private IEntityCollection dataSource = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Windows.Forms.XPTableEntityCollection type
        /// </summary>
        public XPTableEntityCollection()
        {
            // Hookup the events
            this.HookupEvents();
        }

        #endregion

        #region Methods

        #region Initialization methods

        /// <summary>
        /// Initializes the entity collection and the table columns
        /// </summary>
        /// <param name="entityName">The name of the entity</param>
        public void InitEntityCollection(string entityName)
        {
            if (entityName != string.Empty && entityName.Length > 0)
            {
                this.entityName = entityName;
                this.dataSource = DataFactory.EntityCollectionFactory.GetEntityCollection(this.entityName) as IEntityCollection;
                this.InitColumns2();
            }
        }

        /// <summary>
        /// Initializes the columns of the table using the fields of the corresponding entity
        /// </summary>
        public void InitColumns2()
        {
            this.BeginUpdate();

            this.ColumnModel = new ColumnModel();
            this.ColumnModel.Columns.Clear();

            IEntity entity = DataFactory.EntityFactory.GetEntity(this.entityName) as IEntity;
            if (entity != null && entity.Fields.Count > 0)
            {
                for (int i = 0; i < entity.Fields.Count; i++)
                {
                    IEntityField field = entity.Fields[i];
                    if (field != null)
                    {
                        string sPropertyName = field.Name;
                        string sPropertyType = field.ActualDotNetType.ToString();

                        if (!sPropertyName.Contains("Via") && !sPropertyType.Contains("Collection") && sPropertyName != "ShowFields" && sPropertyName != "ShowFieldSeparator")
                        {
                            if (sPropertyName.StartsWith("Id"))
                            {
                                // Id columns
                                TextColumn oTextColumn = new TextColumn();
                                oTextColumn.Name = sPropertyName;
                                oTextColumn.Text = sPropertyName;
                                oTextColumn.ToolTipText = sPropertyName;

                                //if (X_Global.ShowIdValuesInBrowser)
                                //{
                                //    oTextColumn.Visible = true;
                                //    oTextColumn.Width = oBrowserField.Width;
                                //}
                                //else
                                //{
                                    oTextColumn.Visible = false;
                                //}

                                this.ColumnModel.Columns.Add(oTextColumn);
                            }
                            else
                            {
                                // Other than Id columns
                                if (sPropertyType == "System.Boolean")
                                {
                                    CheckBoxColumn oCheckBoxColumn = new CheckBoxColumn();
                                    oCheckBoxColumn.Name = sPropertyName;
                                    oCheckBoxColumn.Text = sPropertyName;
                                    oCheckBoxColumn.ToolTipText = sPropertyName;
                                    //oCheckBoxColumn.Width = oBrowserField.Width;
                                    oCheckBoxColumn.Alignment = ColumnAlignment.Center;
                                    oCheckBoxColumn.DrawText = false;
                                    this.ColumnModel.Columns.Add(oCheckBoxColumn);
                                }
                                else
                                {
                                    TextColumn oTextColumn = new TextColumn();
                                    oTextColumn.Name = sPropertyName;
                                    oTextColumn.Text = sPropertyName;
                                    oTextColumn.ToolTipText = sPropertyName;
                                    //oTextColumn.Width = oBrowserField.Width;
                                    this.ColumnModel.Columns.Add(oTextColumn);
                                }
                            }
                        }
                    }
                }
            }

            this.EndUpdate();
        }

        /// <summary>
        /// Hookup the events to the corresponding event handlers
        /// </summary>
        private void HookupEvents()
        {
            //this.DoubleClick += new EventHandler(X_DataGridViewEntityCollection_DoubleClick);
            //this.BeginSort += new XPTable.Events.ColumnEventHandler(X_TableEntityCollection_BeginSort);
        }

        #endregion

        #region New, Edit, Delete methods

        /// <summary>
        /// Creates a new entity and creates and displays a form for the entity
        /// </summary>
        public new void New()
        {
            //this.mEditForm = X_FormFactory.InvokeDataDialog(this.mEntityName);
            //if (this.mEditForm != null)
            //{
            //    X_Entity oEntity = new X_Entity(this.mEntityName, X_Global.DataAssembly, X_Global.EntityNamespacePrefix);

            //    if (oEntity != null && oEntity.Entity != null)
            //    {
            //        string sIdValue = string.Empty;
            //        string sForeignIdField = string.Empty;

            //        // Get the owner form
            //        Control oOwnerForm = null;
            //        oOwnerForm = this.GetOwnerForm(this);

            //        try
            //        {
            //            X_FormDataDialog oFormDataDialog = (X_FormDataDialog)oOwnerForm;

            //            if (oFormDataDialog != null)
            //            {
            //                // Get the entity of the datadialog owner form
            //                EntityBase oOwnerEntity = oFormDataDialog.Entity;

            //                // Get the primary keys for the entity of the owner datadialog form
            //                ArrayList oOwnerPrimaryKeys = oOwnerEntity.PrimaryKeyFields;
            //                if (oOwnerPrimaryKeys.Count == 1)
            //                {
            //                    EntityField oOwnerPrimaryKeyField = (EntityField)oOwnerPrimaryKeys[0];
            //                    sForeignIdField = oOwnerPrimaryKeyField.Name;
            //                    sIdValue = (string)oOwnerPrimaryKeyField.CurrentValue;

            //                    // Set the foreign key
            //                    if (sForeignIdField != string.Empty && sIdValue != string.Empty)
            //                    {
            //                        oEntity.Entity.Fields[sForeignIdField].CurrentValue = sIdValue;
            //                    }
            //                }
            //            }
            //        }
            //        catch
            //        {
            //        }

            //        // Create an Id for the new entity
            //        ArrayList oPrimaryKeys = oEntity.Entity.PrimaryKeyFields;
            //        if (oPrimaryKeys.Count == 1)
            //        {
            //            EntityField oPrimaryKeyField = (EntityField)oPrimaryKeys[0];
            //            string sId = X_UtilityID.CreateGuid(this.mEntityName);
            //            oPrimaryKeyField.CurrentValue = (string)sId;

            //            //// Add a recent item
            //            //X_UtilityRecentItems.AddRecentItem(sId, this.mEntityName, X_UtilityDate.GetDateTimeStamp());
            //        }

            //        this.mEditForm.Entity = oEntity.Entity;
            //        this.mEditForm.Owner = oOwnerForm;

            //        // Fire the ItemNew event
            //        this.OnItemNew(null, null);

            //        this.mEditForm.InitDataBindings();
            //        this.mEditForm.ShowDialog();
            //    }
            //}
        }

        /// <summary>
        /// Gets the currently selected entity and creates and displays a form for the entity
        /// </summary>
        public new void Edit()
        {
            //this.mEditForm = X_FormFactory.InvokeDataDialog(this.mEntityName);
            //if (this.mEditForm != null)
            //{
            //    //EntityBase oEntity = this.GetSelectedRow();
            //    string sSelectedId = this.GetSelectedId();
            //    X_Entity oEntityHelper = new X_Entity(this.mEntityName, sSelectedId, X_Global.DataAssembly, X_Global.EntityNamespacePrefix);

            //    if (oEntityHelper.Entity != null)
            //    {
            //        // Get the owner form
            //        Control oOwnerForm = null;
            //        oOwnerForm = this.GetOwnerForm(this);

            //        this.mEditForm.Entity = oEntityHelper.Entity;
            //        this.mEditForm.Owner = oOwnerForm;

            //        // Fire the ItemEdited event
            //        this.OnItemEdited(null, null);

            //        // Add a recent item
            //        string sId = oEntityHelper.Entity.Fields["Id" + this.mEntityName].CurrentValue.ToString();
            //        X_UtilityRecentItems.AddRecentItem(sId, this.mEntityName, X_UtilityDate.GetDateTimeStamp());

            //        this.mEditForm.InitDataBindings();
            //        this.mEditForm.ShowDialog();
            //    }
            //}
        }

        /// <summary>
        /// Deletes the currently selected entity
        /// </summary>
        public new void Delete()
        {
            //if (X_MessageBoxYesNoWarning.Show("Weet u zeker dat u het geselecteerde item wilt verwijderen?") == DialogResult.Yes)
            //{
            //    // Fire the ItemDeleted event
            //    this.OnItemDeleted(null, null);

            //    EntityBase oEntity = this.GetSelectedRow();
            //    if (oEntity != null)
            //    {
            //        //if (oEntity.Delete())
            //        if (X_UtilityLLBLGen.DeleteEntity(oEntity, X_Global.DataAssembly, X_Global.EntityCollectionNamespacePrefix))
            //        {
            //            X_UtilityLog.WriteLine(X_UtilityType.GetTypeString(oEntity) + " entity deleted.");
            //            this.Refresh();
            //        }
            //    }
            //}
        }

        #endregion

        /// <summary>
        /// Gets the currently selected row from the table
        /// </summary>
        /// <returns>The currently selected entity object</returns>
        public new IEntity GetSelectedRow()
        {
            IEntity selectedEntity = null;

            if (this.SelectedIndicies.Length > 0)
            {
                int nSelectedRowNr = this.SelectedIndicies[0];

                IEntityCollection entityCollection = (IEntityCollection)this.DataSource;
                selectedEntity = (IEntity)entityCollection[nSelectedRowNr];
            }

            return selectedEntity;
        }

        /// <summary>
        /// Gets the currently selected primary key id from the table
        /// </summary>
        /// <returns>The primary key id (string) from the currently selected record</returns>
        public new string GetSelectedId()
        {
            //IEntity entity = this.GetSelectedRow();

            //if (entity != null && entity.Fields.Count > 0)
            //{
            //    this.mSelectedId = entity.Fields[0].CurrentValue.ToString();
            //}
            //return this.mSelectedId;
            return string.Empty;
        }

        /// <summary>
        /// Selects an record using an primary key id
        /// </summary>
        /// <param name="id">The primary key id for the record to select</param>
        public void SelectById(string id)
        {
            //EntityCollectionBase oEntityCollectionBase = (EntityCollectionBase)this.DataSource;

            //if (oEntityCollectionBase != null && oEntityCollectionBase.Count > 0)
            //{
            //    EntityBase oEntityBase = (EntityBase)oEntityCollectionBase.Items[0];

            //    if (oEntityBase != null)
            //    {
            //        PropertyDescriptorCollection oProperties = oEntityBase.GetProperties(null);
            //        if (oProperties != null && oProperties.Count > 0)
            //        {
            //            try
            //            {
            //                PropertyDescriptor oProperty = oProperties["Id" + this.mEntityName];
            //                int nRowNr = oEntityCollectionBase.Find(oProperty, (string)id);
            //                this.Rows[nRowNr].Selected = true;
            //            }
            //            catch
            //            {
            //            }
            //        }
            //    }
            //}

            //if (this.mSelectedId != string.Empty)
            //{
            //    IEntityCollection entityCollection = (IEntityCollection)this.DataSource;

            //    if (entityCollection != null && entityCollection.Count > 0)
            //    {
            //        EntityBase oEntityBase = (EntityBase)entityCollection.Items[0];

            //        if (oEntityBase != null)
            //        {
            //            PropertyDescriptorCollection oProperties = oEntityBase.GetProperties(null);
            //            if (oProperties != null && oProperties.Count > 0)
            //            {
            //                try
            //                {
            //                    PropertyDescriptor oProperty = oProperties["Id" + this.entityName];
            //                    int nRowNr = entityCollection.Find(oProperty, (string)this.mSelectedId);
            //                    this.SelectRow(nRowNr);
            //                }
            //                catch
            //                {
            //                }
            //            }
            //        }
            //    }
            //}
        }

        public new void SelectById()
        {
            //if (this.mSelectedId != string.Empty)
            //{
            //    DataGridViewSelectedCellCollection oSelectedCellCollection = this.SelectedCells;
            //    EntityCollectionBase oEntityCollectionBase = (EntityCollectionBase)this.DataSource;

            //    if (oEntityCollectionBase != null && oEntityCollectionBase.Count > 0)
            //    {
            //        EntityBase oEntityBase = (EntityBase)oEntityCollectionBase.Items[0];

            //        if (oEntityBase != null)
            //        {
            //            PropertyDescriptorCollection oProperties = oEntityBase.GetProperties(null);
            //            if (oProperties != null && oProperties.Count > 0)
            //            {
            //                try
            //                {
            //                    PropertyDescriptor oProperty = oProperties["Id" + this.mEntityName];
            //                    int nRowNr = oEntityCollectionBase.Find(oProperty, (string)this.mSelectedId);
            //                    this.Rows[nRowNr].Selected = true;
            //                }
            //                catch
            //                {
            //                }
            //            }
            //        }
            //    }
            //}

            //if (this.mSelectedId != string.Empty)
            //{
            //    IEntityCollection entityCollection = (IEntityCollection)this.DataSource;

            //    if (entityCollection != null && entityCollection.Count > 0)
            //    {
            //        IEntity entity = entityCollection[0] as IEntity;

            //        if (entity != null)
            //        {
            //            PropertyDescriptorCollection oProperties = entity.GetProperties(null);
            //            if (oProperties != null && oProperties.Count > 0)
            //            {
            //                try
            //                {
            //                    PropertyDescriptor oProperty = oProperties["Id" + this.entityName];
            //                    //int nRowNr = entityCollection.Find(oProperty, (string)this.mSelectedId);
            //                    //this.SelectRow(nRowNr);
            //                }
            //                catch
            //                {
            //                }
            //            }
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Refreshes the table
        /// </summary>
        public new void Refresh()
        {
            this.BeginUpdate();

            //if (this.mEntityCollectionHelper != null)
            //{
            //    this.mEntityCollectionHelper.Refresh();
            //}

            // Clear the rows in the grid
            this.TableModel = new TableModel();
            this.TableModel.Rows.Clear();

            if (this.dataSource != null && this.dataSource.Count > 0)
            {
                for (int i = 0; i < this.dataSource.Count; i++)
                {
                    Row oRow = new Row();

                    IEntity entity = (IEntity)this.dataSource[i];
                    if (entity != null && entity.Fields.Count > 0)
                    {
                        for (int j = 0; j < entity.Fields.Count; j++)
                        {
                            IEntityField field = entity.Fields[j];

                            string sPropertyName = field.Name;
                            string sPropertyType = field.ActualDotNetType.ToString();

                            if (!sPropertyName.Contains("Via") && !sPropertyType.Contains("Collection") && sPropertyName != "ShowFields" && sPropertyName != "ShowFieldSeparator")
                            {
                                object oMemberValue = Member.InvokeProperty(entity, sPropertyName);

                                Cell oCell = new Cell();
                                oCell.Editable = false;

                                if (sPropertyType == "System.DateTime")
                                {
                                    DateTime oDateTime = (DateTime)oMemberValue;

                                    string sText = oDateTime.ToShortDateString();
                                    string sDate = oDateTime.Date.Day.ToString("00") + "-" + oDateTime.Date.Month.ToString("00") + "-" + oDateTime.Date.Year.ToString("0000");
                                    if (sDate == "01-01-0001")
                                    {
                                        sText = string.Empty;
                                    }
                                    oCell.Text = sText;
                                    oCell.ToolTipText = sText;
                                }
                                else if (sPropertyType == "System.Decimal")
                                {
                                    decimal nDecimal = (decimal)oMemberValue;

                                    string sText = nDecimal.ToString("c");
                                    oCell.Text = sText;
                                    oCell.ToolTipText = sText;
                                }
                                else if (sPropertyType == "System.Boolean")
                                {
                                    bool oBool = (bool)oMemberValue;
                                    if (oBool)
                                    {
                                        oCell.Checked = true;
                                        oCell.ToolTipText = "Ja";
                                    }
                                    else
                                    {
                                        oCell.Checked = false;
                                        oCell.ToolTipText = "Nee";
                                    }
                                }
                                else
                                {
                                    object oValue = oMemberValue;
                                    string sText = string.Empty;

                                    if (oValue != null)
                                    {
                                        sText = oValue.ToString();
                                    }
                                    oCell.Text = sText;
                                    oCell.ToolTipText = sText;
                                }

                                // Add the cell to the table row
                                oRow.Cells.Add(oCell);
                            }
                        }
                    }

                    this.TableModel.Rows.Add(oRow);
                }
            }

            this.EndUpdate();
        }

        /// <summary>
        /// Performs a search on the record which are being displayed
        /// </summary>
        /// <param name="searchString">The string to search for</param>
        /// <param name="fieldName">The field to search on</param>
        public new void Search(string searchString, string fieldName)
        {
            //this.mEntityCollectionHelper.ClearFilter();
            //this.mEntityCollectionHelper.Search(searchString, fieldName);
            //this.Refresh();
        }

        /// <summary>
        /// Changes the order of the records being displayed in the table
        /// </summary>
        /// <param name="fieldName">The field to sort on</param>
        public new void SetOrder(string fieldName)
        {
            //this.mEntityCollectionHelper.SetOrder(fieldName);
            //this.Refresh();
        }

        /// <summary>
        /// Manually set a value on a form
        /// </summary>
        /// <param name="fieldName">The field to set the value on</param>
        /// <param name="fieldValue">The value</param>
        public void SetEditFormFieldValue(string fieldName, object fieldValue)
        {
            //if (this.mEditForm != null && fieldName != null && fieldName.Length > 0 && fieldName != null)
            //{
            //    if (this.mEditForm.Entity != null)
            //    {
            //        EntityBase oEntity = this.mEditForm.Entity;

            //        if (oEntity != null && oEntity.Fields[fieldName] != null)
            //        {
            //            oEntity.Fields[fieldName].CurrentValue = fieldValue;
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Selects the top record in the table
        /// </summary>
        public new void GoToFirst()
        {
            if (this.dataSource != null && this.dataSource.Count > 0)
            {
                this.SelectRow(0);
            }
        }

        /// <summary>
        /// Selects the previous record in the table
        /// </summary>
        public new void GoToPrevious()
        {
            int[] nSelectedRows = this.TableModel.Selections.SelectedIndicies;
            int nSelectedRow = 0;

            if (nSelectedRows.Length > 0)
            {
                nSelectedRow = nSelectedRows[0];
            }

            int nPreviousRow = nSelectedRow - 1;
            if (nPreviousRow < 0)
            {
                nPreviousRow = 0;
            }

            if (this.dataSource != null && this.dataSource.Count > 0)
            {
                this.SelectRow(nPreviousRow);
            }
        }

        /// <summary>
        /// Selects the next record in the table
        /// </summary>
        public new void GoToNext()
        {
            int[] nSelectedRows = this.TableModel.Selections.SelectedIndicies;
            int nSelectedRow = 0;

            if (nSelectedRows.Length > 0)
            {
                nSelectedRow = nSelectedRows[0];
            }

            int nNextRow = nSelectedRow + 1;
            if (nNextRow > (this.dataSource.Count - 1))
            {
                nNextRow = this.dataSource.Count - 1;
            }

            if (this.dataSource != null && this.dataSource.Count > 0)
            {
                this.SelectRow(nNextRow);
            }
        }

        /// <summary>
        /// Selects the bottom record in the table
        /// </summary>
        public new void GoToLast()
        {
            if (this.dataSource != null && this.dataSource.Count > 0)
            {
                this.SelectRow(this.dataSource.Count - 1);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the entity
        /// </summary>
        [Category("Dionysos.Windows.Forms")]
        [Browsable(true)]
        [Description("The name of the entity")]
        public string EntityName
        {
            get
            {
                return this.entityName;
            }
            set
            {
                this.entityName = value;
            }
        }

        /// <summary>
        /// Gets or sets the datasource
        /// </summary>
        [Category("Dionysos.Windows.Forms")]
        [Browsable(false)]
        [Description("The datasource of the table")]
        public IEntityCollection DataSource
        {
            get
            {
                return this.dataSource;
            }
            set
            {
                this.dataSource = value;
                this.Refresh();
            }
        }

        #endregion
    }
}
