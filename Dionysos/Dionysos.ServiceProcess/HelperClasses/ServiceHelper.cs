﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;

namespace Dionysos.ServiceProcess.HelperClasses
{
    /// <summary>
    /// Helper class which contains functionality for Windows service handling
    /// </summary>
    public class ServiceHelper
    {
        /// <summary>
        /// Gets a System.ServiceProcess.ServiceController instance from the installed services
        /// using the specified service name
        /// </summary>
        /// <param name="serviceName">The name of the service to retrieve</param>
        /// <returns>A ServiceController instance if a service is found, null if not</returns>
        public static ServiceController GetServiceController(string serviceName)
        {
            ServiceController serviceController = null;

            ServiceController[] services = ServiceController.GetServices();
            for (int i = 0; i < services.Length; i++)
            {
                ServiceController service = services[i];
                if (service.ServiceName.Equals(serviceName))
                {
                    serviceController = service;
                    break;
                }
            }

            return serviceController;
        }

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="timeoutMilliseconds">The timeout milliseconds.</param>
        /// <returns>True if the service was started successfully, False if not</returns>
        public static bool StartService(string serviceName, int timeoutMilliseconds)
        {
            bool success = false;

            ServiceController serviceController = GetServiceController(serviceName);
            if (serviceController != null)
            {
                try
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    serviceController.Start();
                    serviceController.WaitForStatus(ServiceControllerStatus.Running, timeout);

                    success = true;
                }
                catch
                {
                    if (serviceController.Status == ServiceControllerStatus.Running || serviceController.Status == ServiceControllerStatus.StartPending)
                        success = true;
                }
            }

            return success;
        }
    }
}
