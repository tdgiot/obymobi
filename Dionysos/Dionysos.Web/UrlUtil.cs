﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;

namespace Dionysos.Web
{
	public class UrlUtil
    {
        #region Properties

        private static string SHORTNER_API_KEY_ENCODED = "QUl6YVN5RDZGMVpRTVBGeF9fSTVNWFBIUXdWd2I2UlRLdlAyT184";        

        private static string SHORTNER_API_KEY_DECODED = "";
        public static string SHORTNER_API_KEY
        {
            get
            {
                if (SHORTNER_API_KEY_DECODED.IsNullOrWhiteSpace())
                    SHORTNER_API_KEY_DECODED = Base64UrlDecode(SHORTNER_API_KEY_ENCODED);

                return SHORTNER_API_KEY_DECODED;
            }
        }

		static UrlUtil instance = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the UrlUtil class if the instance has not been initialized yet
		/// </summary>
		public UrlUtil()
		{
			if (instance == null)
			{
				// first create the instance
				instance = new UrlUtil();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the static UrlUtil instance
		/// </summary>
		private void Init()
		{
		}

        public static string Base64UrlDecode(string input)
        {
            var output = input;
            output = output.Replace('-', '+');
            output = output.Replace('_', '/');
            switch (output.Length % 4)
            {
                case 0: break;
                case 2: output += "=="; break;
                case 3: output += "="; break;
                default: throw new System.Exception("Illegal base64url string!");
            }
            var converted = Convert.FromBase64String(output);
            return Encoding.UTF8.GetString(converted);
        }

		/// <summary>
		/// Gets the full URL.
		/// </summary>
		/// <param name="relativeUrl">The relative URL.</param>
		/// <returns></returns>
		public static string GetFullUrl(string relativeUrl)
		{
			string toReturn = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
			if (HttpContext.Current.Request.Url.Port != 80)
				toReturn += ":" + HttpContext.Current.Request.Url.Port;

			toReturn += Path.Combine(toReturn, VirtualPathUtility.ToAbsolute(relativeUrl));

			return toReturn;
		}

		/// <summary>
		/// Gets the full url of the specified host and relative url
		/// </summary>
		/// <param name="host">The host of the url</param>
		/// <param name="resolvedUrl">The resolved url to get to full path for</param>
		/// <returns>A System.String instance containing the full url</returns>
		public static string GetFullUrl(string host, string resolvedUrl)
		{
			string url = string.Empty;

#if DEBUG
			host += string.Format(":{0}", System.Web.HttpContext.Current.Request.Url.Port);
#endif

			return string.Format("http://{0}{1}", host, resolvedUrl); ;
		}

		/// <summary>
		/// Retrieve the originally requested Url from a requested that ended as a 404 request.
		/// </summary>
		/// <param name="full404Url">A full url like: /vitronafrontpage/default.aspx?404;http://localhost:80/vitronafrontpage/Producten--Zonwering.html</param>
		/// <returns>Only the full url of the original request without any port numbers</returns>
		public static string GetRequestUrlFrom404Request(string full404Url)
		{
			string toReturn = Dionysos.StringUtil.GetAllAfterFirstOccurenceOf(full404Url, ';');

			// Remove any port numbers (http://studio.amteam.nl:80/vitrona/business/Actualiteiten)
			toReturn = Regex.Replace(toReturn, ":[0-9]{1,4}", string.Empty);

			return toReturn;
		}

		/// <summary>
		/// Removes ApplicationPath from URL
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public static string RemoveApplicationPathIfStartsWithApplicationPath(string url)
		{
			string applicationPath = HttpContext.Current.Request.ApplicationPath;

			if (!url.StartsWith("/"))
				url = "/" + url;

			if (url.StartsWith(applicationPath))
				url = url.Substring(applicationPath.Length);

			return url;
		}

        public static bool ValidateUrl(string url)
        {
            if (!string.IsNullOrEmpty(url) && Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
            {
                Uri tempValue;
                return (Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out tempValue));
            }
            return (false);
        }

        public static string Shorten(string url)
        {
            string post = "{\"longUrl\": \"" + url + "\"}";
            string shortUrl = url;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.googleapis.com/urlshortener/v1/url?key=" + UrlUtil.SHORTNER_API_KEY);

            try
            {
                request.ServicePoint.Expect100Continue = false;
                request.Method = "POST";
                request.ContentLength = post.Length;
                request.ContentType = "application/json";
                request.Headers.Add("Cache-Control", "no-cache");

                using (Stream requestStream = request.GetRequestStream())
                {
                    byte[] postBuffer = Encoding.ASCII.GetBytes(post);
                    requestStream.Write(postBuffer, 0, postBuffer.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader responseReader = new StreamReader(responseStream))
                        {
                            string json = responseReader.ReadToEnd();
                            shortUrl = Regex.Match(json, @"""id"": ?""(?<id>.+)""").Groups["id"].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // if Google's URL Shortner is down...
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return shortUrl;
        }

		#endregion
	}
}