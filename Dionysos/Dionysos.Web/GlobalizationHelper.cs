﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Web;
using Dionysos.Web.UI.WebControls;

namespace Dionysos.Web
{
	/// <summary>
	/// Class with helper functions for Control Translation
	/// </summary>
	public class GlobalizationHelper
    {
        private static bool? useGlobalization = null;

		/// <summary>
		/// Get the value form the AppSettings if we should Globalize the controls
		/// </summary>
		public static bool UseGlobalization
		{
			get
			{
                if (useGlobalization.HasValue)
                {
                    return useGlobalization.Value;
                }
                
                try
                {
                    System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();

                    useGlobalization = (bool) reader.GetValue("UseGlobalization", typeof(bool));
                }
                catch(Exception ex)
				{
					Debug.WriteLine(ex);
					useGlobalization = false;
                }

				return useGlobalization.Value;
			}
		}

		/// <summary>
		/// Get the value form the AppSettings if we should Learn non available controls
		/// </summary>
		public static bool LearnControls
		{
			get
			{
				if (HttpContext.Current.Application["Globalization.LearnControls"] != null)
					return (bool)HttpContext.Current.Application["Globalization.LearnControls"];
				else
					return false;
			}
			set
			{
				HttpContext.Current.Application["Globalization.LearnControls"] = value;
			}
		}

		/// <summary>
		/// Get the value form the AppSettings for the Default Culture
		/// </summary>
		public static string DefaultCulture
		{
			get
			{
				System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
				string toReturn = "nl-NL";

				try
				{
					toReturn = (string)reader.GetValue("DefaultCulture", typeof(string));
				}
				catch { }

				return toReturn;
			}
		}

		/// <summary>
		/// First 2 letters of the CultureInfo indicating the Language (en-UK would return 'en');
		/// </summary>
		public static string CurrentUICultureLanguageCode
		{
			get
			{
				return CurrentUICultureString.Substring(0, 2);
			}
		}

		/// <summary>
		/// Get the current CurrentUICulture.Name from the thread
		/// </summary>
		public static string CurrentUICultureString
		{
			get
			{
				return System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
			}
		}

		/// <summary>
		/// Get a Regex object with the Pattern for culture (nl-NL) matching
		/// </summary>
		/// <returns>A initialized Regex object</returns>
		public static Regex CultureMatchRegex
		{
			get
			{
				return new Regex("/[a-z]{2}\\-[a-z]{2}/");
			}
		}

		/// <summary>
		/// Collects the controls from the page and places them into an Lynx_media.Web.Collections.ControlCollection
		/// </summary>
		public void LocalizeControls(Dionysos.Web.UI.PageDefault page)
		{
			if (GlobalizationHelper.UseGlobalization)
			{
				for (int i = 0; i < page.Controls.Count; i++)
				{
					System.Web.UI.Control control = page.Controls[i];
					GlobalizationHelper.LocalizeControls(control, false);
				}

				// Localize Page Title
				if (page.LocalizeTitle)
				{
					string pageFileName = page.Request.FilePath.Remove(0, page.Request.FilePath.LastIndexOf("/") + 1).ToLower();
					page.Title = Global.TranslationProvider.GetTranslation(pageFileName + ".PageTitle", page.Title);
				}


			}
		}

		/// <summary>
		/// Gets the subcontrols from the specified control recursively
		/// </summary>
		/// <param name="control">The System.Web.UI.Control to get the subcontrols from</param>
		private static void LocalizeControls(System.Web.UI.Control control, bool inContentPlaceHolder)
		{
			// check for null, if so an exception will be thrown
			Instance.ArgumentIsEmpty(control, "control");

			if ((control.Controls.Count == 0))
			{
				// Localize
				GlobalizationHelper.PerformLocalization(control, inContentPlaceHolder);
			}
			else
			{
				// Determine if we ARE a placeholder, or in one. This to distinct MasterPage controls
				// from on Page Controls
				if (inContentPlaceHolder)
				{
					GlobalizationHelper.PerformLocalization(control, inContentPlaceHolder);
				}
				else if (control.GetType() == typeof(System.Web.UI.WebControls.ContentPlaceHolder))
				{
					GlobalizationHelper.PerformLocalization(control, inContentPlaceHolder);
					inContentPlaceHolder = true;
				}

				for (int i = 0; i < control.Controls.Count; i++)
				{
					GlobalizationHelper.LocalizeControls(control.Controls[i], inContentPlaceHolder);
				}
			}

			return;
		}

		/// <summary>
		/// Localize a control
		/// </summary>
		/// <param name="control">Control to Localize</param>
		/// <param name="controlInContentPlaceHolder">Indicate if the control resides in a controlContentPlaceholder</param>
		public static void PerformLocalization(System.Web.UI.Control control, bool controlInContentPlaceHolder)
		{
			string pageFileName = control.Page.Request.FilePath.Remove(0, control.Page.Request.FilePath.LastIndexOf("/") + 1).ToLower();

			if (control is Label)
			{
				Label controlAsLabel = control as Label;
				if (controlAsLabel.LocalizeText)
				{
					if (controlAsLabel.TranslationTag.Length > 0)
						controlAsLabel.Text = Global.TranslationProvider.GetTranslation(controlAsLabel.TranslationTag, CurrentUICultureLanguageCode, controlAsLabel.Text, LearnControls);
					else
					{
						string translationKey = string.Empty;
						if (controlInContentPlaceHolder)
						{
							// Page specific control
							translationKey = pageFileName + "." + Dionysos.StringUtil.RemoveLeadingLowerCaseCharacters(controlAsLabel.ID);
						}
						else
						{
							// Contorl on master page
							translationKey = "Masterpage" + "." + Dionysos.StringUtil.RemoveLeadingLowerCaseCharacters(controlAsLabel.ID);
						}
						controlAsLabel.Text = Global.TranslationProvider.GetTranslation(translationKey, CurrentUICultureLanguageCode, controlAsLabel.Text, LearnControls);
					}
				}
			}
			else if (control is HyperLink)
			{
				HyperLink controlAsHyperlink = control as HyperLink;
				if (controlAsHyperlink.LocalizeText)
				{
					if (controlAsHyperlink.TranslationTag.Length > 0)
						controlAsHyperlink.Text = Global.TranslationProvider.GetTranslation(controlAsHyperlink.TranslationTag, CurrentUICultureLanguageCode, controlAsHyperlink.Text, LearnControls);
					else
					{
						string translationKey = string.Empty;
						if (controlInContentPlaceHolder)
						{
							// Page specific control
							translationKey = pageFileName + "." + Dionysos.StringUtil.RemoveLeadingLowerCaseCharacters(controlAsHyperlink.ID);
						}
						else
						{
							// Contorl on master page
							translationKey = "Masterpage" + "." + Dionysos.StringUtil.RemoveLeadingLowerCaseCharacters(controlAsHyperlink.ID);
						}
						controlAsHyperlink.Text = Global.TranslationProvider.GetTranslation(translationKey, CurrentUICultureLanguageCode, controlAsHyperlink.Text, LearnControls);
					}
				}
			}
			else if (control is DropDownList)
			{
				DropDownList controlAsDropDownList = control as DropDownList;
				if (controlAsDropDownList.LocalizeDefaultItem && controlAsDropDownList.DefaultListItemText.Length > 0)
				{
					if (controlAsDropDownList.TranslationTag.Length > 0)
						controlAsDropDownList.Text = Global.TranslationProvider.GetTranslation(controlAsDropDownList.TranslationTag, CurrentUICultureLanguageCode, controlAsDropDownList.Text, LearnControls);
					else
					{
						string translationKey = string.Empty;
						if (controlInContentPlaceHolder)
						{
							// Page specific control
							translationKey = pageFileName + "." + Dionysos.StringUtil.RemoveLeadingLowerCaseCharacters(controlAsDropDownList.ID);
						}
						else
						{
							// Contorl on master page
							translationKey = "Masterpage" + "." + Dionysos.StringUtil.RemoveLeadingLowerCaseCharacters(controlAsDropDownList.ID);
						}

						// Check if the first item is default item
						if (controlAsDropDownList.Items.Count > 0 && controlAsDropDownList.Items[0].Text == controlAsDropDownList.DefaultListItemText)
						{
							controlAsDropDownList.Items[0].Text = Global.TranslationProvider.GetTranslation(translationKey, CurrentUICultureLanguageCode, controlAsDropDownList.Text, LearnControls);
						}
					}
				}
			}
		}
	}
}
