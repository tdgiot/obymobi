using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Dionysos.Web
{
	/// <summary>
	/// Summary description for RssFeedItem
	/// </summary>
	public class RssFeedItem
	{
		#region Fields

		#endregion

		#region Contructors

		public RssFeedItem()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#endregion

		#region Methods

		public void WriteElementToXmlWriter(System.Xml.XmlTextWriter writer, RssFeed feed)
		{
			writer.WriteStartElement("item");

			if (!Instance.Empty(this.Title))
				writer.WriteElementString("title", this.Title);
			if (!Instance.Empty(this.Link))
				writer.WriteElementString("link", this.Link);
			if (!Instance.Empty(this.Description))
			{
				// Add element for HTML
				//string body = string.Format("![CDATA[{0}]]",this.Description);
				string body = this.Description;
				writer.WriteElementString("description", body);
			}
			if (!Instance.Empty(this.Author))
				writer.WriteElementString("author", this.Author);
			if (!Instance.Empty(this.Category))
				writer.WriteElementString("category", this.Category);
			if (!Instance.Empty(this.Comments))
				writer.WriteElementString("comments", this.Comments);
			if (!Instance.Empty(this.Guid))
				writer.WriteElementString("guid", this.Guid);
			if (!Instance.Empty(this.PubDate))
				writer.WriteElementString("pubDate", this.PubDate.ToString("R"));

			// Source is special
			if (!Instance.Empty(this.SourceFeedUrl) && !Instance.Empty(this.SourceFeedTitle))
			{
				// Manual Source
				writer.WriteStartElement("source");
				writer.WriteAttributeString("url", this.SourceFeedUrl);
				writer.WriteValue(this.sourceFeedTitle);
				writer.WriteEndElement();
			}
			else
			{
				writer.WriteStartElement("source");
				writer.WriteAttributeString("url", feed.Link);
				writer.WriteValue(feed.Title);
				writer.WriteEndElement();
			}

			writer.WriteEndElement();
		}


		#endregion

		#region Properties

		private string title;
		/// <summary>
		/// The title of the item.
		/// </summary>
		public string Title
		{
			get { return title; }
			set { title = value; }
		}

		private string link;
		/// <summary>
		/// The URL of the item.
		/// </summary>
		public string Link
		{
			get { return link; }
			set { link = value; }
		}

		private string description;
		/// <summary>
		/// The item synopsis.
		/// </summary>
		public string Description
		{
			get { return description; }
			set { description = value; }
		}

		private string author;
		/// <summary>
		/// Email address of the author of the item.
		/// (optional)
		/// </summary>
		public string Author
		{
			get { return author; }
			set { author = value; }
		}

		private string category;
		/// <summary>
		/// Includes the item in one or more categories.
		/// (optional)
		/// </summary>
		public string Category
		{
			get { return category; }
			set { category = value; }
		}

		private string comments;
		/// <summary>
		/// URL of a page for comments relating to the item.
		/// (optional)
		/// </summary>
		public string Comments
		{
			get { return comments; }
			set { comments = value; }
		}

		private string enclosure;
		/// <summary>
		/// Describes a media object that is attached to the item. (NOT YET IMPLEMENTED)
		/// (optional)
		/// </summary>
		public string Enclosure
		{
			get { return enclosure; }
			set { enclosure = value; }
		}

		private string guid;
		/// <summary>
		/// A string that uniquely identifies the item. (Can be anything, for example the url to the article is also unique!)
		/// (optional)
		/// </summary>
		public string Guid
		{
			get { return guid; }
			set { guid = value; }
		}

		private DateTime pubDate = DateTime.MinValue;
		/// <summary>
		/// Indicates when the item was published. (i.e. Tue, 20 May 2003 08:56:02 GMT)
		/// </summary>
		public DateTime PubDate
		{
			get { return pubDate; }
			set { pubDate = value; }
		}

		private string sourceFeedTitle;
		/// <summary>
		/// The RSS channel that the item came from it's title. (On default derived from the containing feed) (optional)
		/// </summary>
		public string SourceFeedTitle
		{
			get { return sourceFeedTitle; }
			set { sourceFeedTitle = value; }
		}

		/// <summary>
		/// The RSS channel that the item came from it's url. (On default derived from the containing feed) (optional)
		/// </summary>
		public string SourceFeedUrl
		{
			get { return sourceFeedTitle; }
			set { sourceFeedTitle = value; }
		}
		#endregion
	}
}