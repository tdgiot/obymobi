using System;
using System.Data;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Dionysos.Web
{
	/// <summary>
	/// Summary description for RssFeed
	/// </summary>
	public class RssFeed
	{
		#region Fields
		#endregion

		#region Contructor

		public RssFeed()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#endregion

		#region Methods
		// TODO VALIDATION OF REQUIRED FIELDS
		public void GetFeedXml(System.IO.Stream stream)
		{
			XmlTextWriter xml = new XmlTextWriter(stream, Encoding.UTF8);
			xml.Formatting = Formatting.Indented;
			// Open document
			xml.WriteStartDocument();

			// Write mandatory start
			xml.WriteStartElement("rss");
			xml.WriteAttributeString("version", "2.0");
			xml.WriteAttributeString("xmlns:atom", "http://www.w3.org/2005/Atom");

			// Write Feed details (not items yet)
			xml.WriteStartElement("channel");

			// Write Fields
			if (!Instance.Empty(this.Title))
				xml.WriteElementString("title", this.Title);
			if (!Instance.Empty(this.Link))
				xml.WriteElementString("link", this.Link);
			if (!Instance.Empty(this.Link))
			{
				xml.WriteStartElement("atom:link");
				xml.WriteAttributeString("href", HttpContext.Current.Request.Url.ToString());
				xml.WriteAttributeString("rel", "self");
				xml.WriteEndElement();
			}
			if (!Instance.Empty(this.Description))
				xml.WriteElementString("description", this.Description);
			if (!Instance.Empty(this.Language))
				xml.WriteElementString("language", this.Language);
			if (!Instance.Empty(this.Copyright))
				xml.WriteElementString("copyright", this.Copyright);
			if (!Instance.Empty(this.ManagingEditor))
				xml.WriteElementString("managingEditor", this.ManagingEditor);
			if (!Instance.Empty(this.WebMaster))
				xml.WriteElementString("webMaster", this.WebMaster);
			if (!Instance.Empty(this.PubDate))
				xml.WriteElementString("pubDate", this.pubDate.ToString("R"));
			if (!Instance.Empty(this.LastBuildDate))
				xml.WriteElementString("lastBuildDate", this.LastBuildDate.ToString("R"));
			if (!Instance.Empty(this.Category))
				xml.WriteElementString("category", this.Category);
			if (!Instance.Empty(this.Generator))
				xml.WriteElementString("generator", this.Generator);
			if (!Instance.Empty(this.Docs))
				xml.WriteElementString("docs", this.Docs);
			if (!Instance.Empty(this.ImageTitle) && !Instance.Empty(this.ImageUrl))
			{
				xml.WriteStartElement("image");
				xml.WriteElementString("url", this.ImageUrl);
				xml.WriteElementString("title", this.ImageTitle);
				if (!Instance.Empty(this.ImageHeight))
					xml.WriteElementString("height", this.ImageHeight.ToString());
				if (!Instance.Empty(this.ImageWidth))
					xml.WriteElementString("width", this.ImageWidth.ToString());
				xml.WriteEndElement();
			}

			// Write the items
			for (int i = 0; i < this.Items.Count; i++)
			{
				this.Items[i].WriteElementToXmlWriter(xml, this);
			}

			// End Channel
			xml.WriteEndElement();

			// End Rss
			xml.WriteEndElement();

			//xml.WriteEndDocument();
			xml.Flush();
			xml.Close();
		}

		/// <summary>
		/// Returns the XML feed in the response and ends it.
		/// </summary>
		/// <param name="response">Response for the Request</param>
		public void WriteRssFeedAsResponse(HttpResponse response)
		{
			response.Clear();
			response.ContentType = "text/xml";
			this.GetFeedXml(response.OutputStream);
			response.End();
		}

		#endregion

		#region Properties

		private string title = "TITLE NOT SET";
		/// <summary>
		/// The name of the channel. It's how people refer to your service. If you have an HTML website that contains the same information as your RSS file, the title of your channel should be the same as the title of your website.
		/// </summary>
		public string Title
		{
			get
			{
				return this.title;
			}
			set
			{
				this.title = value;
			}
		}

		private string link = "LINK NOT SET";
		/// <summary>
		/// The URL to the HTML website corresponding to the channel.
		/// </summary>
		public string Link
		{
			get
			{
				return this.link;
			}
			set
			{
				this.link = value;
			}
		}

		private string description = "DESCRIPTION NOT SET";
		/// <summary>
		/// Phrase or sentence describing the channel.
		/// </summary>
		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		private string language;
		/// <summary>
		/// The language the channel is written in. This allows aggregators to group all Italian language sites, for example, on a single page. A list of allowable values for this element, as provided by Netscape, is here. You may also use values defined by the W3C.
		/// </summary>
		public string Language
		{
			get
			{
				return this.language;
			}
			set
			{
				this.language = value;
			}
		}

		private string copyright;
		/// <summary>
		/// Copyright notice for content in the channel.
		/// </summary>
		public string Copyright
		{
			get
			{
				return this.copyright;
			}
			set
			{
				this.copyright = value;
			}
		}

		private string managingEditor;
		/// <summary>
		/// Email address for person responsible for editorial content.
		/// </summary>
		public string ManagingEditor
		{
			get
			{
				return this.managingEditor;
			}
			set
			{
				this.managingEditor = value;
			}
		}

		private string webMaster;
		/// <summary>
		/// Email address for person responsible for technical issues relating to channel.
		/// </summary>
		public string WebMaster
		{
			get
			{
				return this.webMaster;
			}
			set
			{
				this.webMaster = value;
			}
		}

		private DateTime pubDate = DateTime.MinValue;
		/// <summary>
		/// The publication date for the content in the channel. For example, the New York Times publishes on a daily basis, the publication date flips once every 24 hours. That's when the pubDate of the channel changes. All date-times in RSS conform to the Date and Time Specification of RFC 822, with the exception that the year may be expressed with two characters or four characters (four preferred).
		/// </summary>
		public DateTime PubDate
		{
			get
			{
				return this.pubDate;
			}
			set
			{
				this.pubDate = value;
			}
		}

		private DateTime lastBuildDate = DateTime.MinValue;
		/// <summary>
		/// The last time the content of the channel changed.
		/// </summary>
		public DateTime LastBuildDate
		{
			get
			{
				return this.lastBuildDate;
			}
			set
			{
				this.lastBuildDate = value;
			}
		}

		private string category;

		/// <summary>
		/// Specify one or more categories that the channel belongs to. Follows the same rules as the item-level category element. More info.
		/// </summary>
		public string Category
		{
			get
			{
				return this.category;
			}
			set
			{
				this.category = value;
			}
		}

		private string generator;
		/// <summary>
		/// A string indicating the program used to generate the channel.
		/// </summary>
		public string Generator
		{
			get { return generator; }
			set { generator = value; }
		}

		private string docs = "http://www.rssboard.org/rss-specification";
		/// <summary>
		/// A URL that points to the documentation for the format used in the RSS file. (Default: http://www.rssboard.org/rss-specification)
		/// </summary>
		public string Docs
		{
			get { return docs; }
			set { docs = value; }
		}

		private string imageUrl;
		/// <summary>
		/// pecifies a GIF, JPEG or PNG image that can be displayed with the channel.
		/// </summary>
		public string ImageUrl
		{
			get { return imageUrl; }
			set { imageUrl = value; }
		}

		private string imageTitle;
		/// <summary>
		/// Alt text of the Image 
		/// </summary>
		public string ImageTitle
		{
			get { return imageTitle; }
			set { imageTitle = value; }
		}

		private int imageWidth;
		/// <summary>
		/// Width of the image (standard for RssFeeds = 88, maximum = 144)
		/// </summary>
		public int ImageWidth
		{
			get { return imageWidth; }
			set { imageWidth = value; }
		}

		private int imageHeight;
		/// <summary>
		/// Height of the image (standard for RssFeeds = 31, maximum = 400)
		/// </summary>
		public int ImageHeight
		{
			get { return imageHeight; }
			set { imageHeight = value; }
		}

		/// <summary>
		/// Collection of Items contained in the Feed
		/// </summary>
		private List<RssFeedItem> items = new List<RssFeedItem>();
		public List<RssFeedItem> Items
		{
			get
			{
				return this.items;
			}
			set
			{
				this.items = value;
			}
		}


		#endregion
	}
}