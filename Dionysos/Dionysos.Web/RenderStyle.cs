using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web
{
    /// <summary>
    /// The way a button should be displayed based on a readonly/edit view
    /// </summary>
    public enum RenderStyle
    {
        Labels,
        DisabledControls
    }
}
