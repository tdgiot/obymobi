﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Dionysos.Web
{
	public class RewriteFormHtmlTextWriter : HtmlTextWriter
	{
		private string _formAction;

		public RewriteFormHtmlTextWriter(System.IO.TextWriter writer)
			: base(writer)
		{

		}
		public override void RenderBeginTag(string tagName)
		{
			if (tagName.ToString().IndexOf("form") >= 0)
			{
				base.RenderBeginTag(tagName);
			}
		}
		public RewriteFormHtmlTextWriter(System.IO.TextWriter writer, string action)
			: base(writer)
		{

			this._formAction = action;

		}
		public override void WriteAttribute(string name, string value, bool fEncode)
		{
			if (name == "action")
			{
				value = _formAction;
			}
			base.WriteAttribute(name, value, fEncode);
		}

	}
}
