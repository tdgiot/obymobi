using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Dionysos.Web
{
	/// <summary>
	/// Represents an element in a query string.
	/// </summary>
	public class QueryStringElement : IComparable, IComparable<QueryStringElement>, IEquatable<QueryStringElement>
	{
		#region Properties

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public string Name { get; private set; }

		/// <summary>
		/// Gets or sets the value of the querystring element
		/// </summary>  
		public object Value { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="QueryStringElement"/> class.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		public QueryStringElement(string name, object value)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (String.IsNullOrEmpty(name)) throw new ArgumentException("Name cannot be empty.", "name");

			this.Name = name;
			this.Value = value;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Compares this instance with the specified object.
		/// </summary>
		/// <param name="obj">An object to compare with this instance.</param>
		/// <returns>
		/// A 32-bit signed integer that indicates whether this instance precedes, follows, or appears in the same position in the sort order as the value parameter.
		/// </returns>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="obj"/> is not the same type as this instance.
		/// </exception>
		public int CompareTo(object obj)
		{
			if (obj == null) return 1;

			QueryStringElement other = obj as QueryStringElement;
			if (other == null) throw new ArgumentException("obj is not the same type as this instance.", "obj");

			return this.CompareTo(other);
		}

		/// <summary>
		/// Compares this instance with the specified <see cref="QueryStringElement"/>.
		/// </summary>
		/// <param name="other">The <see cref="QueryStringElement"/> to compare with this instance.</param>
		/// <returns>
		/// A 32-bit signed integer that indicates whether this instance precedes, follows, or appears in the same position in the sort order as the value parameter.
		/// </returns>
		public int CompareTo(QueryStringElement other)
		{
			if (other == null) return 1;

			return String.Compare(this.Name, other.Name, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		public override bool Equals(object obj)
		{
			return this.Equals(obj as QueryStringElement);
		}

		/// <summary>
		/// Indicates whether the current object is equal to another object of the same type.
		/// </summary>
		/// <param name="other">The <see cref="QueryStringElement"/> to compare with this object.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="QueryStringElement"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		public bool Equals(QueryStringElement other)
		{
			if (other == null) return false;

			return String.Equals(this.Name, other.Name, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
		/// </returns>
		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return String.Format("{0}={1}", this.Name, HttpUtility.UrlEncode(Convert.ToString(this.Value)));
		}

		#endregion
	}
}
