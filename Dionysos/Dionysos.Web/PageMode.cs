using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web
{
    /// <summary>
    /// Enumerator class for the mode of pages which contain data entities
    /// </summary>
    public enum PageMode : int
    {
        /// <summary>
        /// No page mode has been set
        /// </summary>
        None,

        /// <summary>
        /// An entity is being added
        /// </summary>
        Add,

        /// <summary>
        /// The details of an entity are being viewed
        /// </summary>
        View,

        /// <summary>
        /// The details of an entity are being edited
        /// </summary>
        Edit,

        /// <summary>
        /// An entity is being deleted
        /// </summary>
        Delete
    }
}
