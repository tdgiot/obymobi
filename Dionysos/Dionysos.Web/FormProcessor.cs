using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.UI;
using Dionysos.Web.UI.WebControls;

namespace Dionysos.Web
{
	public class FormProcessor
	{
		#region Fields

		/// <summary>
		/// The parent control.
		/// </summary>
		private readonly Control parent = null;

		/// <summary>
		/// Indicates whether only visible controls are included.
		/// </summary>
		private bool onlyVisibleControls = true;

		/// <summary>
		/// The subject.
		/// </summary>
		private string subject = "Formulier vanaf de website";

		/// <summary>
		/// The variables text.
		/// </summary>
		private string variablesText = String.Empty;

		/// <summary>
		/// The posted files
		/// </summary>
		private readonly List<HttpPostedFile> postedFiles = new List<HttpPostedFile>();

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the SMTP host.
		/// </summary>
		/// <value>
		/// The SMTP host.
		/// </value>
		[Obsolete("Use the Host property.")]
		public string HeloHost
		{
			get
			{
				return this.Host;
			}
			set
			{
				this.Host = value;
			}
		}

		/// <summary>
		/// Gets or sets the SMTP host (if not set, uses default configuration value).
		/// </summary>
		/// <value>
		/// The SMTP host.
		/// </value>
		public string Host { get; set; }

		/// <summary>
		/// Gets or sets the port (if not set, uses default configuration value).
		/// </summary>
		/// <value>
		/// The port.
		/// </value>
		public int? Port { get; set; }

		/// <summary>
		/// Gets or sets whether to enable SSL (if not set, uses default configuration value).
		/// </summary>
		/// <value>
		/// Enable SSL.
		/// </value>
		public bool? EnableSsl { get; set; }

		/// <summary>
		/// Gets or sets the address of the sender (if not set, uses default configuration value).
		/// </summary>
		/// <value>
		/// The address of the sender.
		/// </value>
		public string EmailFrom { get; set; }

		/// <summary>
		/// Gets or sets the name of the sender.
		/// </summary>
		/// <value>
		/// The name of the sender.
		/// </value>
		public string EmailFromName { get; set; }

		/// <summary>
		/// Gets or sets the address of the recipient (if not set, uses default configuration value).
		/// </summary>
		/// <value>
		/// The address of the recipient.
		/// </value>
		public string EmailTo { get; set; }

		/// <summary>
		/// Gets or sets the name of the recipient.
		/// </summary>
		/// <value>
		/// The name of the recipient.
		/// </value>
		public string EmailToName { get; set; }

		/// <summary>
		/// Gets or sets the reply to address.
		/// </summary>
		/// <value>
		/// The reply to address.
		/// </value>
		public string ReplyTo { get; set; }

		/// <summary>
		/// Gets or sets the name of the reply to address.
		/// </summary>
		/// <value>
		/// The name of the reply to address.
		/// </value>
		public string ReplyToName { get; set; }

		/// <summary>
		/// Additional text that is appended to the email after the control values
		/// </summary>
		/// <value>
		/// The additional text.
		/// </value>
		public string AdditionalText { get; set; }

		/// <summary>
		/// Use this to include only the controls you want, for examle: tbNameMail
		/// </summary>
		/// <value>
		/// The key name post fix to include.
		/// </value>
		public string KeyNamePostFixToInclude { get; set; }

		/// <summary>
		/// Gets or sets the subject
		/// </summary>
		/// <value>
		/// The subject.
		/// </value>
		public string Subject
		{
			get
			{
				return this.subject;
			}
			set
			{
				this.subject = value;
			}
		}

		/// <summary>
		/// Gets or sets the controlCollectionToSentFrom, if specified this is used instead of the Page.Controls collection
		/// </summary>
		/// <value>
		/// The control collection to sent from.
		/// </value>
		public ControlCollection ControlCollectionToSentFrom { get; set; }

		/// <summary>
		/// Gets or sets the developer text.
		/// </summary>
		/// <value>
		/// The developer.
		/// </value>
		public string Developer { get; set; }

		/// <summary>
		/// Gets or sets if only visible controls must be send.
		/// </summary>
		/// <value>
		///   <c>true</c> if [only visible controls]; otherwise, <c>false</c>.
		/// </value>
		public bool OnlyVisibleControls
		{
			get
			{
				return this.onlyVisibleControls;
			}
			set
			{
				this.onlyVisibleControls = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to add attachments for file upload controls.
		/// </summary>
		/// <value>
		///   <c>true</c> if attachments are added for file upload controls; otherwise, <c>false</c>.
		/// </value>
		public bool AddAttachments { get; set; }

		#endregion

		#region Constructor and Methods

		/// <summary>
		/// Create a new instance of the FormProcessor class.
		/// </summary>
		/// <param name="parent">The Control to look in.</param>
		public FormProcessor(Control parent)
		{
			this.parent = parent;
		}

		/// <summary>
		/// Sends an e-mail using the configuration settings or the instance properties (instance properties take precedence).
		/// </summary>
		public void SentMailFromControlValues()
		{
			this.SentMailFromControlValues(null, null, null, null, null);
		}

		/// <summary>
		/// Sends an e-mail using the specified arguments and the default port number (25) or the instance properties (instance properties take precedence).
		/// </summary>
		/// <param name="emailTo">The recipient e-mail address.</param>
		/// <param name="emailFrom">The sender e-mail address.</param>
		public void SentMailFromControlValues(string emailTo, string emailFrom)
		{
			this.SentMailFromControlValues(emailTo, emailFrom, null, null, null);
		}

		/// <summary>
		/// Sends an e-mail using the specified arguments and the default port number (25) or the instance properties (instance properties take precedence).
		/// </summary>
		/// <param name="emailTo">The recipient e-mail address.</param>
		/// <param name="emailFrom">The sender e-mail address.</param>
		/// <param name="smtpHost">The SMTP host to deliver the e-mail to.</param>
		public void SentMailFromControlValues(string emailTo, string emailFrom, string smtpHost)
		{
			this.SentMailFromControlValues(emailTo, emailFrom, smtpHost, null, null);
		}

		/// <summary>
		/// Sends an e-mail using the specified arguments or the instance properties (instance properties take precedence).
		/// </summary>
		/// <param name="emailTo">The recipient e-mail address.</param>
		/// <param name="emailFrom">The sender e-mail address.</param>
		/// <param name="smtpHost">The SMTP host to deliver the e-mail to.</param>
		/// <param name="smtpPort">The SMTP port the host is listening on.</param>
		public void SentMailFromControlValues(string emailTo, string emailFrom, string smtpHost, int? smtpPort)
		{
			this.SentMailFromControlValues(emailTo, emailFrom, smtpHost, null, null);
		}

		/// <summary>
		/// Sends an e-mail using the specified arguments or the instance properties (instance properties take precedence).
		/// </summary>
		/// <param name="emailTo">The recipient e-mail address.</param>
		/// <param name="emailFrom">The sender e-mail address.</param>
		/// <param name="smtpHost">The SMTP host to deliver the e-mail to.</param>
		/// <param name="smtpPort">The SMTP port the host is listening on.</param>
		/// <param name="enableSsl">The enable SSL.</param>
		public void SentMailFromControlValues(string emailTo, string emailFrom, string smtpHost, int? smtpPort, bool? enableSsl)
		{
			this.GetVariablesFromControls();
			this.SendMail(emailTo, emailFrom, smtpHost, smtpPort, enableSsl);
		}

		/// <summary>
		/// Gets the variables from controls.
		/// </summary>
		private void GetVariablesFromControls()
		{
			// Get controls
			Dionysos.Web.UI.ControlCollection controlsToSent;
			if (this.ControlCollectionToSentFrom != null)
			{
				controlsToSent = new Dionysos.Web.UI.ControlCollection();
				controlsToSent.CollectControls(this.ControlCollectionToSentFrom);
			}
			else
			{
				if (this.parent is Dionysos.Web.UI.PageDefault)
				{
					Dionysos.Web.UI.PageDefault pageDefault = this.parent as Dionysos.Web.UI.PageDefault;
					controlsToSent = pageDefault.ControlList;
				}
				else
				{
					controlsToSent = new Dionysos.Web.UI.ControlCollection();
					controlsToSent.CollectControls(this.parent);
				}
			}

			// Add variables for controls
			for (int i = 0; i < controlsToSent.Count; i++)
			{
				Control ctrl = controlsToSent[i];

				if (String.IsNullOrEmpty(ctrl.ID) || !this.CheckToShow(ctrl))
				{
					// Do not parse control
				}
				else if (ctrl is TextBox)
				{
					TextBox tb = ctrl as TextBox;
					this.AddVariable(tb.FriendlyName, tb.Text);
				}
				else if (ctrl is DropDownList)
				{
					DropDownList ddl = ctrl as DropDownList;
					this.AddVariable(ddl.FriendlyName, ddl.SelectedValue);
				}
				else if (ctrl is CheckBox)
				{
					CheckBox cb = ctrl as CheckBox;

					string keyName = cb.Text;
					if (String.IsNullOrEmpty(keyName)) keyName = Dionysos.StringUtil.RemoveLeadingLowerCaseCharacters(cb.ID);

					string value = "Niet aangevinkt";
					if (cb.Checked) value = "Aangevinkt";

					this.AddVariable(keyName, value);
				}
				else if (ctrl is CheckBoxList)
				{
					CheckBoxList cbl = ctrl as CheckBoxList;

					if (cbl.SelectedItemsCount > 0)
					{
						string value = String.Join(", ", cbl.SelectedValues('|').Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries));
						this.AddVariable(cbl.FriendlyName, value);
					}
				}
				else if (ctrl is RadioButton)
				{
					RadioButton rb = ctrl as RadioButton;
					if (rb.Checked)
						this.AddVariable(rb.GroupName, rb.Text);
				}
				else if (ctrl is RadioButtonList)
				{
					RadioButtonList rbl = ctrl as RadioButtonList;

					string value = "Geen keuze gemaakt";
					if (rbl.SelectedIndex >= 0)
						value = rbl.SelectedValue;

					this.AddVariable(rbl.FriendlyName, value);
				}
				else if (this.AddAttachments && ctrl is FileUpload)
				{
					FileUpload fu = ctrl as FileUpload;

					if (fu.HasFile)
					{
						// Add attachment to mail
						this.AddVariable(fu.FriendlyName, fu.FileName);
						this.postedFiles.Add(fu.PostedFile);
					}
				}
			}
		}

		/// <summary>
		/// Adds the variable.
		/// </summary>
		/// <param name="keyName">Name of the key.</param>
		/// <param name="value">The value.</param>
		private void AddVariable(string keyName, string value)
		{
			string keyNameParsed = String.Empty;
			for (int i = 0; i < keyName.Length; i++)
			{
				if (char.IsUpper(keyName[i]))
				{
					keyNameParsed += " " + keyName[i];
				}
				else if (char.IsNumber(keyName[i]))
				{
					keyNameParsed += " " + keyName[i];
				}
				else
				{
					keyNameParsed += char.ToLower(keyName[i]);
				}
			}

			if (keyNameParsed != String.Empty)
				variablesText += String.Format("<tr><td><strong>{0}: </strong></td><td>{1}</td></tr>\n\r", HttpUtility.HtmlEncode(keyNameParsed), HttpUtility.HtmlEncode(value));
		}

		/// <summary>
		/// Checks to show.
		/// </summary>
		/// <param name="control">The control.</param>
		/// <returns></returns>
		private bool CheckToShow(Control control)
		{
			bool show = true;

			// Show only specified controls
			if (!String.IsNullOrEmpty(this.KeyNamePostFixToInclude) &&
				!control.ID.EndsWith(this.KeyNamePostFixToInclude, StringComparison.OrdinalIgnoreCase))
			{
				show = false;
			}

			// Exclude custom controls
			if (show)
			{
				string[] dontShow = new string[] { ".x", ".y", "SentTo", "ReturnUrl", "Subject" };
				for (int i = 0; i < dontShow.Length; i++)
				{
					if (control.ID.Contains(dontShow[i], StringComparison.OrdinalIgnoreCase))
					{
						show = false;
						break;
					}
				}
			}

			// Exclude invisible controls
			if (show && this.OnlyVisibleControls)
			{
				show = control.Visible;
			}

			return show;
		}

		/// <summary>
		/// Sends the mail.
		/// </summary>
		/// <param name="emailTo">The email to.</param>
		/// <param name="emailFrom">The email from.</param>
		/// <param name="smtpHost">The SMTP host.</param>
		/// <param name="smtpPort">The SMTP port.</param>
		/// <param name="smtpEnableSsl">The SMTP enable SSL.</param>
		private void SendMail(string emailTo, string emailFrom, string smtpHost, int? smtpPort, bool? smtpEnableSsl)
		{
			SmtpClient smtpClient = new SmtpClient();
			smtpClient.Host = this.Host.Nullify() ?? smtpHost.Nullify() ?? Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.SmtpHost);
			smtpClient.Port = this.Port ?? smtpPort ?? Global.ConfigurationProvider.GetInt(DionysosConfigurationConstants.SmtpPort);
			smtpClient.Timeout = Global.ConfigurationProvider.GetInt(DionysosConfigurationConstants.SmtpTimeout);
			smtpClient.EnableSsl = this.EnableSsl ?? smtpEnableSsl ?? Global.ConfigurationProvider.GetBool(DionysosConfigurationConstants.SmtpEnableSsl);

			// Optional values
			string smtpUserName = Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.SmtpUsername);
			string smtpPassword = Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.SmtpPassword);
			if (!String.IsNullOrEmpty(smtpUserName) &&
				!String.IsNullOrEmpty(smtpPassword))
			{
				smtpClient.Credentials = new System.Net.NetworkCredential(smtpUserName, smtpPassword);
			}

			using (MailMessage mailMessage = new MailMessage())
			{
				// From
				MailAddress fromAddress = new MailAddress(this.EmailFrom.Nullify() ?? emailFrom.Nullify() ?? Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.EmailSenderAddress), this.EmailFromName);
				mailMessage.From = fromAddress;

				// To
				MailAddress toAddress = new MailAddress(this.EmailTo.Nullify() ?? emailTo.Nullify() ?? Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.EmailRecipientAddress), this.EmailToName);
				mailMessage.To.Add(toAddress);

				// Subject
				mailMessage.Subject = this.Subject;

				// Reply to
				if (!String.IsNullOrEmpty(this.ReplyTo))
				{
				    mailMessage.ReplyToList.Add(new MailAddress(this.ReplyTo, this.ReplyToName));
				}

				// Message body content
				string htmlBody = "";
				htmlBody += "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
				htmlBody += "<html>";
				htmlBody += "<head>";
				htmlBody += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
				htmlBody += "<style type=\"text/css\">body,p,td,a,ul,li{font-family: Arial,Helvetica,sans-serif;font-size: 12px;color: #000000}td{text-align:left;vertical-align:middle}</style>";
				htmlBody += "</head>";
				htmlBody += "<body>";
				htmlBody += String.Format(CultureInfo.GetCultureInfo("nl-NL"), "<p>De volgende gegevens zijn vanaf de website verzonden op {0:d} om {0:t}.</p>", DateTime.Now);

				if (!String.IsNullOrEmpty(this.AdditionalText))
				{
					htmlBody += String.Format("<p>{0}</p>", this.AdditionalText);
				}

				htmlBody += "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				htmlBody += this.variablesText;
				htmlBody += "</table>";

				if (String.IsNullOrEmpty(this.Developer)) htmlBody += "<p><small>Mail verzonden met <a href=\"http://www.panoramastudios.nl\">Panorama Studios</a> FormMail v.1.1</small></p>";
				else htmlBody += this.Developer;

				// Add Dutch disclaimer
				StringBuilder disclaimer = new StringBuilder();
				disclaimer.AppendLine("<p>");
				disclaimer.AppendLine("De informatie in dit e-mail bericht (inclusief informatie in bijlagen) is uitsluitend bestemd voor de geadresseerde.");
				disclaimer.AppendLine("Dit e-mail bericht kan informatie bevatten van vertrouwelijk- of persoonlijke aard. Verstrekking aan en/of gebruik door anderen is niet toegestaan.");
				disclaimer.AppendLine("Indien u dit bericht ten onrechte ontvang verzoekt de afzender u vriendelijk om de afzender hiervan onmiddellijk op de hoogte te stellen en het bericht te vernietigen.");
				disclaimer.AppendLine("Er geldt geen garantie dat gebruik van e-mail veilig is of dat het gebruik van e-mail geschiedt zonder enige fout.");
				disclaimer.AppendLine("De afzender is op geen enkele wijze aansprakelijk voor enige fout of gebrek in de inhoud van dit e-mail bericht.");
				disclaimer.AppendLine("De afzender staat niet in voor de juiste en/of volledige overbrenging van de inhoud van onderhavig e-mail bericht, noch voor tijdige ontvangst daarvan.");
				disclaimer.AppendLine("</p>");

				// Add English disclaimer
				disclaimer.AppendLine("<p>");
				disclaimer.AppendLine("The information in this e-mail message (including any information contained in attachments hereto) is intended only for the use of the addressee.");
				disclaimer.AppendLine("This e-mail message can contain confidential- or privileged information.");
				disclaimer.AppendLine("If you receive this e-mail message unintentionally please notify the sender immediately and then delete this message.");
				disclaimer.AppendLine("E-mail transmission is not guaranteed to be secured or error free.");
				disclaimer.AppendLine("The sender is in no way liable for an accurate delivery of this e-mail and any errors or omissions in the content of this e-mail message, which may arise as a result of e-mail transmission.");
				disclaimer.AppendLine("</p>");

				// Add disclaimer to body
				htmlBody += disclaimer.ToString();
				htmlBody += "</body></html>";

				// GK ORDER TEXT > HTML is essential for Gmail
				AlternateView plainTextView = AlternateView.CreateAlternateViewFromString(StringUtilLight.StripHtml(htmlBody), new System.Net.Mime.ContentType("text/plain"));
				plainTextView.TransferEncoding = TransferEncoding.QuotedPrintable;
				mailMessage.AlternateViews.Add(plainTextView);

				AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlBody, new System.Net.Mime.ContentType("text/html"));
				htmlView.TransferEncoding = TransferEncoding.QuotedPrintable;
				mailMessage.AlternateViews.Add(htmlView);

				// Add attachments
				if (this.AddAttachments &&
					this.postedFiles.Count > 0)
				{
					foreach (HttpPostedFile postedFile in this.postedFiles)
					{
						mailMessage.Attachments.Add(new Attachment(postedFile.InputStream, Path.GetFileName(postedFile.FileName), postedFile.ContentType));
					}
				}

				smtpClient.Send(mailMessage);
			}
		}

		#endregion
	}
}

