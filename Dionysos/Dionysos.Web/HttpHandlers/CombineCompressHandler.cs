﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using Yahoo.Yui.Compressor;

namespace Dionysos.Web.HttpHandlers
{
	/// <summary>
	/// Combine and compress HTTP handler.
	/// </summary>
	public class CombineCompressHandler : IHttpHandler
	{
		#region Fields

		/// <summary>
		/// Gets the cache duration.
		/// </summary>
		protected readonly TimeSpan cacheDuration = TimeSpan.FromHours(3); // 3 hours

		/// <summary>
		/// Gets the expiry duration.
		/// </summary>
		protected readonly TimeSpan expiryDuration = TimeSpan.FromDays(365); // 1 year

		/// <summary>
		/// The allowed file extensions.
		/// </summary>
		protected readonly string[] allowedFileExtensions = new string[] { ".css", ".js" };

		#endregion

		#region IHttpHandler Members

		/// <summary>
		/// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler" /> instance.
		/// </summary>
		/// <returns>true if the <see cref="T:System.Web.IHttpHandler" /> instance is reusable; otherwise, false.</returns>
		public bool IsReusable
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler" /> interface.
		/// </summary>
		/// <param name="context">An <see cref="T:System.Web.HttpContext" /> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
		public void ProcessRequest(HttpContext context)
		{
			// Create a string for the cache
			string cacheKey = context.Request.Url.AbsoluteUri;

			StringBuilder sb = new StringBuilder();

			// Check what type of file it is (js or css) (because css.axd and js.axd can be called)
			string path = Path.GetFileNameWithoutExtension(context.Request.Path);

			// Get the accepted encoding (compression)
			string encoding = this.GetResponseEncoding(context);

			// Set content type
			string contentType = path == "js" || path == "javascript" ? "application/javascript" : (path == "css" ? "text/css" : "");
			context.Response.ContentType = contentType;

			// create the hash used for the etag
			string hash = this.GetMd5Sum(cacheKey);

			// Only add caching if version parameter is added
			if (!String.IsNullOrEmpty(context.Request.QueryString["version"]))
			{
				// Set etag
				context.Response.Cache.SetETag(hash);

				// Set max age
				TimeSpan expiryDuration = this.expiryDuration;
				context.Response.Cache.SetMaxAge(expiryDuration);

				// Set expiration
				DateTime expirationDate = DateTime.Now.Add(expiryDuration);
				context.Response.Cache.SetExpires(expirationDate);

				// Set cache control
				context.Response.Cache.SetCacheability(HttpCacheability.Public);
				context.Response.Cache.SetExpires(DateTime.MaxValue);
				context.Response.Cache.SetMaxAge(TimeSpan.MaxValue);
				context.Response.Cache.SetValidUntilExpires(false);
				context.Response.Cache.SetOmitVaryStar(true);
				context.Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
			}

			Uri baseUri = new Uri(context.Request.Url.AbsoluteUri),
				applicationBaseUri = baseUri.GetBase(context.Request.ApplicationPath.TrimEnd('/') + "/");

			// Check if file is in server cache (because of file dependency of the cache it will be removed when changed)
			string[] tempFiles;
			string data;
			if (!QueryStringHelper.TryGetValues("files", out tempFiles, ','))
			{
				// Invalid request
				context.Response.StatusCode = 400;
				data = String.Empty;
			}
			else if (!CacheHelper.TryGetValue(cacheKey, false, out data))
			{
				// Get the names of the requested files
				List<string> fileNames = new List<string>();
				foreach (string file in tempFiles)
				{
					Uri fileUri = new Uri(baseUri, file);
					if (!applicationBaseUri.IsBaseOf(fileUri))
					{
						throw new UnauthorizedAccessException();
					}

					string physicalFilePath;
					string fileData = this.GetLocalFile(context, fileUri, out physicalFilePath);
					if (!String.IsNullOrEmpty(physicalFilePath))
					{
						fileNames.Add(physicalFilePath);
					}

					if (!String.IsNullOrEmpty(fileData))
					{
						// Some files throw errors when the whitespace is stripped.  If this is the case, just add a query string of nostrip=1
						if (TestUtil.IsPcDeveloper) sb.AppendFormatLine("/*! BeginOfFile - {0} */", file.HtmlEncode());
						sb.AppendLine(fileData);
						if (TestUtil.IsPcDeveloper) sb.AppendFormatLine("/*! EndOfFile - {0} */", file.HtmlEncode());
					}
				}

				data = sb.ToString();
				if (!String.IsNullOrEmpty(data) &&
					ConfigurationManager.GetBool(DionysosWebConfigurationConstants.CssJsMinification))
				{
					if (contentType == "application/javascript")
					{
						data = new JavaScriptCompressor()
						{
							Encoding = Encoding.UTF8
						}.Compress(data);
					}
					else if (contentType == "text/css")
					{
						data = new CssCompressor().Compress(data);
					}
				}

				// If there were local files, put in cache and set dependency to the files.
				// This will ensure that if files have changed, we get the latest versions otherwise, just cache the files for a specified amount of time.
				if (fileNames.Count > 0) context.Cache.Insert(cacheKey, data, new CacheDependency(fileNames.ToArray()));
				else context.Cache.Insert(cacheKey, data, null, System.Web.Caching.Cache.NoAbsoluteExpiration, this.cacheDuration);
			}
			else if (this.IsCachedOnBrowser(context, hash))
			{
				// If it's in the server cache then it might be in the browser cache, if it is, send the not modified header and exit
				// we could never retrieve old info/non changed from the browser since we always sent new content in the if clause above if the server
				// cache is empty because of the depenendcy
				return;
			}

			// Write contents
			context.Response.Write(data);

			// If compression is accepted compress the output
			if (ConfigurationManager.GetBool(DionysosWebConfigurationConstants.CssJsCompressing))
			{
				if (encoding != "none")
				{
					if (encoding == "gzip")
					{
						context.Response.Filter = new GZipStream(context.Response.Filter, CompressionMode.Compress);
					}
					else
					{
						context.Response.Filter = new DeflateStream(context.Response.Filter, CompressionMode.Compress);
					}

					context.Response.AppendHeader("Content-Encoding", encoding);
					context.Response.AppendHeader("Vary", "Accept-Encoding");
				}
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets the local file.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="uri">The URI.</param>
		/// <param name="physicalFilePath">The physical file path.</param>
		/// <returns>
		/// The contents of the local file.
		/// </returns>
		private string GetLocalFile(HttpContext context, Uri uri, out string physicalFilePath)
		{
			string fileText = String.Empty;

			// Get file text
			try
			{
				// Local file
				string fileExtension = Path.GetExtension(uri.AbsolutePath);
				if (!this.allowedFileExtensions.Contains(fileExtension, StringComparer.InvariantCultureIgnoreCase))
				{
					throw new FileNotFoundException(String.Format("Could not find file '{0}'.", uri.AbsolutePath));
				}
				else
				{
					string filePath = context.Server.MapPath(uri.LocalPath);
					if (File.Exists(filePath))
					{
						fileText = File.ReadAllText(filePath);
						physicalFilePath = filePath;
					}
					else
					{
						physicalFilePath = null;
					}
				}

				// Fix relative paths for CSS
				if (!String.IsNullOrEmpty(fileText))
				{
					if (fileExtension.Equals(".css", StringComparison.OrdinalIgnoreCase))
					{
						// Get the depth of the handler
						int handlerFileDepth = context.Request.Url.AbsolutePath.Split(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries).Length;

						// Get the depth of the original file
						int fileDepth = uri.AbsolutePath.Split(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries).Length;

						// Only parse if file depth is different
						if (handlerFileDepth != fileDepth)
						{
							// Get offset (positive = remove relative paths, negative = add relative paths)
							int depthOffset = fileDepth - handlerFileDepth;

							fileText = Regex.Replace(fileText, @"(?<RelativePaths>\.\./)+", delegate(Match m)
							{
								int relativeDepth = m.Groups["RelativePaths"].Captures.Count;
								return String.Join("../", new string[relativeDepth - depthOffset + 1]);
							});
						}
					}
				}
			}
			catch (FileNotFoundException)
			{
				throw;
			}
			catch
			{
				physicalFilePath = null;
			}

			return fileText;
		}

		/// <summary>
		/// Gets the response encoding.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns>
		/// The response encoding.
		/// </returns>
		private string GetResponseEncoding(HttpContext context)
		{
			string encoding;
			bool gzip, deflate;

			// get the type of compression to use
			if (!string.IsNullOrEmpty(context.Request.ServerVariables["HTTP_ACCEPT_ENCODING"]))
			{
				// get supported compression methods
				string acceptedTypes = context.Request.ServerVariables["HTTP_ACCEPT_ENCODING"].ToLowerInvariant();
				gzip = acceptedTypes.Contains("gzip") || acceptedTypes.Contains("x-gzip") || acceptedTypes.Contains("*");
				deflate = acceptedTypes.Contains("deflate");
			}
			else
				gzip = deflate = false;

			//determin which to use
			encoding = gzip ? "gzip" : (deflate ? "deflate" : "none");

			// check for buggy versions of Internet Explorer
			if (context.Request.Browser.Browser == "IE")
			{
				if (context.Request.Browser.MajorVersion < 6)
					encoding = "none";
				else if (context.Request.Browser.MajorVersion == 6 &&
					!string.IsNullOrEmpty(context.Request.ServerVariables["HTTP_USER_AGENT"]) &&
					context.Request.ServerVariables["HTTP_USER_AGENT"].Contains("EV1"))
					encoding = "none";
			}

			return encoding;
		}

		/// <summary>
		/// Determines whether the specified hash is cached on the browser.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="hash">The hash.</param>
		/// <returns>
		///   <c>true</c> if the specified hash is cached on the browser; otherwise, <c>false</c>.
		/// </returns>
		private bool IsCachedOnBrowser(HttpContext context, string hash)
		{
			// check if the requesting browser sent an etag
			if (!string.IsNullOrEmpty(context.Request.ServerVariables["HTTP_IF_NONE_MATCH"]) &&
				context.Request.ServerVariables["HTTP_IF_NONE_MATCH"].Equals(hash))
			{
				context.Response.ClearHeaders();
				context.Response.AppendHeader("Etag", hash);
				context.Response.Status = "304 Not Modified";
				context.Response.AppendHeader("Content-Length", "0");
				return true;
			}
			return false;
		}

		/// <summary>
		/// Gets the MD5 sum.
		/// </summary>
		/// <param name="str">The STR.</param>
		/// <returns>The MD5 sum of the specified string.</returns>
		private string GetMd5Sum(string str)
		{
			// First we need to convert the string into bytes, which
			// means using a text encoder.
			Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

			// Create a buffer large enough to hold the string
			byte[] unicodeText = new byte[str.Length * 2];
			enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

			// Now that we have a byte array we can ask the CSP to hash it
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] result = md5.ComputeHash(unicodeText);

			// Build the final string by converting each byte
			// into hex and appending it to a StringBuilder
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < result.Length; i++)
			{
				sb.Append(result[i].ToString("X2"));
			}

			// And return it
			return sb.ToString();
		}

		#endregion
	}
}