﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace Dionysos.Web
{
	[DebuggerDisplay("Url = {url}"), Serializable]
	public class Crumble
	{
		private string url = string.Empty;
		private string pageTitle = string.Empty;
		private string entityType = string.Empty;
		private object entityPkValue = null;
		private PageType pageType = PageType.Standard;


		public Crumble(string url)
		{
			this.url = url;
		}

		public Crumble(string url, string pageTitle)
		{
			this.url = url;
			this.pageTitle = pageTitle;
		}

		public Crumble(string url, string pageTitle, string entityType, object entityPkValue)
		{
			this.url = url;
			this.pageTitle = pageTitle;
			this.entityType = entityType;
			this.entityPkValue = entityPkValue;
		}

		/// <summary>
		/// Get the PK value as Int or null if null or not valid Int
		/// </summary>
		public int? EntityPkValueInt
		{
			get
			{
				int? toReturn = null;
				int parsedInt;
				if (this.entityPkValue != null && int.TryParse(this.entityPkValue.ToString(), out parsedInt))
				{
					toReturn = parsedInt;
				}

				return toReturn;
			}
		}

		/// <summary>
		/// Gets or sets the EntityPkValue
		/// </summary>
		public object EntityPkValue
		{
			get
			{
				return this.entityPkValue;
			}
			set
			{
				this.entityPkValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the EntityType
		/// </summary>
		public string EntityType
		{
			get
			{
				return this.entityType;
			}
			set
			{
				this.entityType = value;
			}
		}

		/// <summary>
		/// Gets or sets the PageTitle
		/// </summary>
		public string PageTitle
		{
			get
			{
				return this.pageTitle;
			}
			set
			{
				this.pageTitle = System.Web.HttpContext.Current.Server.HtmlEncode(value);
			}
		}

		/// <summary>
		/// Gets or sets the Url
		/// </summary>
		public string Url
		{
			get
			{
				return this.url;
			}
			set
			{
				this.url = value;
			}
		}

		/// <summary>
		/// Gets or sets the pageType
		/// </summary>
		public PageType PageType
		{
			get
			{
				return this.pageType;
			}
			set
			{
				this.pageType = value;
			}
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return String.Format("{0} ({1})", this.PageTitle, this.Url);
		}
	}
}
