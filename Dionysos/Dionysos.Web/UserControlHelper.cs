﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.IO;
using System.Web;
using System.Collections.Specialized;
using System.Reflection;
using System.Web.UI.HtmlControls;

namespace Dionysos.Web
{
	/// <summary>
	/// Helper class for UserControls.
	/// </summary>
	public static class UserControlHelper
	{
		/// <summary>
		/// Internal class for rendering usercontrols without a form.
		/// </summary>
		private class FormlessPage : Page
		{
			/// <summary>
			/// Gets or sets a value indicating whether the page validates postback and callback events.
			/// </summary>
			/// <value></value>
			/// <returns>true if the page validates events; otherwise, false.The default is true.
			/// </returns>
			/// <exception cref="T:System.InvalidOperationException">
			/// The <see cref="P:System.Web.UI.Page.EnableEventValidation"/> property was set after the page was initialized.
			/// </exception>
			public override bool EnableEventValidation
			{
				get
				{
					return false;
				}
			}

			/// <summary>
			/// Confirms that an <see cref="T:System.Web.UI.HtmlControls.HtmlForm"/> control is rendered for the specified ASP.NET server control at run time.
			/// </summary>
			/// <param name="control">The ASP.NET server control that is required in the <see cref="T:System.Web.UI.HtmlControls.HtmlForm"/> control.</param>
			/// <exception cref="T:System.Web.HttpException">
			/// The specified server control is not contained between the opening and closing tags of the <see cref="T:System.Web.UI.HtmlControls.HtmlForm"/> server control at run time.
			/// </exception>
			/// <exception cref="T:System.ArgumentNullException">
			/// The control to verify is null.
			/// </exception>
			public override void VerifyRenderingInServerForm(Control control)
			{
			}
		}

		/// <summary>
		/// Renders the UserControl to a string.
		/// </summary>
		/// <param name="virtualPath">The virtualPath of the UserControl.</param>
		/// <param name="parameters">The parameters to pass into the relevant constructor.</param>
		/// <returns>The rendered UserControl.</returns>
		public static string RenderUserControl(string virtualPath, params object[] parameters)
		{
			// Check parameters
			if (virtualPath == null)
			{
				throw new ArgumentNullException("virtualPath");
			}
			if (HttpContext.Current == null)
			{
				throw new InvalidOperationException("HttpContext.Current is null.");
			}

			// Create Page and UserControl
			Page page = new FormlessPage();
			UserControl userControl = (UserControl)page.LoadControl(virtualPath, parameters);
			page.Controls.Add(userControl);

			// Output the UserControl
			StringWriter stringWriter = new StringWriter();
			HttpContext.Current.Server.Execute(page, stringWriter, true);

			return stringWriter.ToString();
		}

		/// <summary>
		/// Renders the UserControl to a string.
		/// </summary>
		/// <param name="control">The UserControl.</param>
		/// <returns>The rendered UserControl.</returns>
		public static string RenderUserControl(UserControl control)
		{
			// Check parameters
			if (control == null)
			{
				throw new ArgumentNullException("control");
			}
			if (HttpContext.Current == null)
			{
				throw new InvalidOperationException("HttpContext.Current is null.");
			}

			// Create Page and UserControl
			Page page = new FormlessPage();
			page.Controls.Add(control);

			// Output the UserControl
			StringWriter stringWriter = new StringWriter();
			HttpContext.Current.Server.Execute(page, stringWriter, true);

			return stringWriter.ToString();
		}

		/// <summary>
		/// Loads the UserControl using the virtualPath and the specified parameters.
		/// </summary>
		/// <typeparam name="T">The UserControl type.</typeparam>
		/// <param name="page">The Page object to load the control on.</param>
		/// <param name="virtualPath">The virtual path of the UserControl.</param>
		/// <param name="parameters">The parameters to pass into the relevant constructor.</param>
		/// <returns>
		/// The initialized UserControl.
		/// </returns>
		public static T LoadControl<T>(this Page page, string virtualPath, params object[] parameters)
			where T : UserControl
		{
			return (T)LoadControl(page, virtualPath, parameters);
		}

		/// <summary>
		/// Loads the UserControl using the virtualPath and the specified parameters.
		/// </summary>
		/// <param name="page">The Page object to load the control on.</param>
		/// <param name="virtualPath">The virtual path of the UserControl.</param>
		/// <param name="parameters">The parameters to pass into the relevant constructor.</param>
		/// <returns>The initialized UserControl.</returns>
		public static UserControl LoadControl(this Page page, string virtualPath, params object[] parameters)
		{
			// Use formless page if not set
			page = page ?? new FormlessPage();

			// Get a list of parameter types
			List<Type> constructorParameterTypes = new List<Type>();
			foreach (object parameter in parameters)
			{
				constructorParameterTypes.Add(parameter.GetType());
			}

			// Load the control
			UserControl userControl = (UserControl)page.LoadControl(virtualPath);

			// Find the relevant constructor
			ConstructorInfo constructorInfo = userControl.GetType().BaseType.GetConstructor(constructorParameterTypes.ToArray());

			// And then call the relevant constructor
			if (constructorInfo == null)
			{
				throw new MemberAccessException("The requested constructor was not found on: " + userControl.GetType().BaseType.ToString());
			}
			else
			{
				constructorInfo.Invoke(userControl, parameters);
			}

			return userControl;
		}
	}
}
