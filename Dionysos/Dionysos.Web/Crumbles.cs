﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;

namespace Dionysos.Web
{
	[Serializable]
	public class Crumbles : List<Crumble>
	{
		#region Methods

		/// <summary>
		/// Adds an item to the crumbles
		/// </summary>
		/// <param name="page">The page.</param>
		public void Add(Page page)
		{
			string pageTitle = page.Title;

            // TEMP Fix
            if (pageTitle.Contains(" | Crave Content Management System"))
                pageTitle = pageTitle.Substring(0, pageTitle.IndexOf(" | Crave Content Management System"));

			Dionysos.Web.UI.Page dionysosPage = page as Dionysos.Web.UI.Page;
			if (dionysosPage != null &&
				!String.IsNullOrEmpty(dionysosPage.CrumbleTitle))
			{
				pageTitle = dionysosPage.CrumbleTitle;
			}

			// init the item
			Crumble newItem = new Crumble(HttpContext.Current.Request.RawUrl, pageTitle);

			// don't add if it's same a last added item
			if (this.Count == 0)
			{
				// No need to check , Count = 0
				this.Add(newItem);
                this.Save();
			}
			else
			{
				bool urlsAreDifferent = false;

				// they are the same
				Crumble lastItem = this.GetLastItem();

				if (lastItem.Url != newItem.Url)
				{
					// GK 20080516
					// Let's so some more inteligent testing, if querystring is the same but the order is different
					QueryStringElementCollection qsLastItem = new QueryStringElementCollection(this.GetLastItem().Url);
					QueryStringElementCollection qsNewItem = new QueryStringElementCollection(newItem.Url);

					if (qsLastItem.Count != qsNewItem.Count)
					{
						// Different number of elements
						urlsAreDifferent = true;
					}
					else
					{
						// Check if the filenames are the same
						if (this.stripQueryStringFromUrl(newItem.Url) != this.stripQueryStringFromUrl(lastItem.Url))
						{
							urlsAreDifferent = true;
						}
						else
						{
							// The same count, so maybe the same
							for (int i = 0; i < qsLastItem.Count; i++)
							{
								if (qsNewItem.Contains(qsLastItem[i].Name))
								{
									QueryStringElement qsElement = qsNewItem[qsLastItem[i].Name];
									if (qsElement.Value.ToString() != qsLastItem[i].Value.ToString())
									{
										// Different element, so different!
										urlsAreDifferent = true;
										break;
									}
								}
							}
						}
					}
				}

				if (urlsAreDifferent)
				{
					// Check what we have to add
					IGuiEntitySimple entityPage = page as IGuiEntitySimple;
					IGuiEntityCollectionSimple collectionPage = page as IGuiEntityCollectionSimple;

					if (entityPage != null)
					{
						IEntity entity = entityPage.DataSource as IEntity;
						if (entity == null)
							throw new NotImplementedException("Crumbles zijn niet gemaakt voor Entity pages met een DataSource die niet van het type IEntity is");

						// Add PK in existing (could be null if new)
						if (entity.PrimaryKeyFields[0].CurrentValue != null && Convert.ToInt32(entity.PrimaryKeyFields[0].CurrentValue) > 0)
							newItem.EntityPkValue = entity.PrimaryKeyFields[0].CurrentValue;

						newItem.EntityType = LLBLGenUtil.GetEntityName(entityPage.EntityName);
						newItem.PageType = PageType.Entity;
					}
					else if (collectionPage != null)
					{
						newItem.EntityType = LLBLGenUtil.GetEntityName(collectionPage.EntityName);
						newItem.PageType = PageType.EntityCollection;
					}
					else
					{
						// No specific additional logic
					}

					this.Add(newItem);
                    this.Save();
				}
			}
		}

		private string stripQueryStringFromUrl(string url)
		{
			if (url.Contains("?"))
				url = url.Substring(0, url.IndexOf("?"));

			return url;
		}

		/// <summary>
		/// Updates the last crumble by replacing the last item by the specified url
		/// </summary>
		/// <param name="id">The id to set in the last crumble</param>
		/// <param name="entityDisplayname">The entity displayname.</param>
		public void UpdateLastCrumble(object id, string entityDisplayname)
		{
			if (this.Count > 0)
			{
				Crumble lastItem = this.GetLastItem();
				if (lastItem.Url.Contains("mode=add"))
				{
					lastItem.Url = lastItem.Url.Replace("mode=add", "mode=edit");

					// GK Quick fix for scenario: Add > Save > Save & Go
					lastItem.Url = StringUtil.Replace(lastItem.Url, "&id=", String.Empty, StringComparison.OrdinalIgnoreCase);
					lastItem.Url += string.Format("&id={0}", id);
					lastItem.EntityPkValue = id;
					if (entityDisplayname.Length > 0)
						lastItem.PageTitle = lastItem.PageTitle.Replace("Toevoegen", entityDisplayname);
					else
						lastItem.PageTitle = lastItem.PageTitle.Replace("Toevoegen", "NAAM ONBEKEND");
					
                    this.Save();
				}
			}
		}

		/// <summary>
		/// Gets the last item from the crumbles
		/// </summary>
		/// <returns>A System.String instance containing the last crumble</returns>
		public Crumble GetLastItem()
		{
			return this[this.Count - 1];
		}

		public void RemoveLastItem()
		{
			this.RemoveAt(this.Count - 1);

            this.Save();
		}

		/// <summary>
		/// Gets or sets the Crumbles collection
		/// </summary>
		public static Crumbles CurrentCrumbles
		{
			get
			{
			    Crumbles crumbles;
			    if (!SessionHelper.TryGetValue("Crumbles", out crumbles))
			    {
			        crumbles = new Crumbles();
			    }

			    return crumbles;
			}
		}

	    private void Save()
	    {
            SessionHelper.SetValue("Crumbles", this);
	    }

		public new void Clear()
        {
			base.Clear();
			SessionHelper.SetValue("Crumbles", new List<Crumble>());
        }

		#endregion

	}
}
