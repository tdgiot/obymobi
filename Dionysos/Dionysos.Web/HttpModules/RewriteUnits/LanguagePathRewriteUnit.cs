﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dionysos.Web;
using Dionysos.Web.HttpModules.RewriteUnits;
using System.Text.RegularExpressions;
using Dionysos;
using System.Globalization;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Parse language from the path (eg. /nl).
	/// </summary>
	public class LanguagePathRewriteUnit : CultureInfoPathRewriteUnit
	{
		/// <summary>
		/// The language regex.
		/// </summary>
		protected readonly Regex languageRegex = new Regex(@"^/(?<LanguageName>[a-z]{2})(/.+|$)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

		/// <summary>
		/// Gets the name of the culture from the path.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns>The culture name.</returns>
		protected override string GetCultureName(string path)
		{
			string cultureName = null;

			Match languageMatch = this.languageRegex.Match(path);
			if (languageMatch.Success)
			{
				// Get culture information from URL
				cultureName = languageMatch.Groups["LanguageName"].Value;

				// Create full culture name
				if (cultureName == "en")
				{
					cultureName += "-gb";
				}
				else
				{
					cultureName += "-" + cultureName;
				}
			}

			return cultureName;
		}

		/// <summary>
		/// Removes the name of the culture from the path.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns>The path with the culture name removed.</returns>
		protected override string RemoveCultureName(string path)
		{
			Match languageMatch = this.languageRegex.Match(path);
			if (languageMatch.Success)
			{
				// Remove culture information from URL
				path = path.Remove(0, languageMatch.Groups["LanguageName"].Value.Length + 1);
			}

			return path;
		}

		/// <summary>
		/// Resolves a pageUrl so the cultureString is added if ParseCultureInformation is true.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <param name="uiCulture">The CultureInfo to use when resolving the pageUrl.</param>
		/// <returns>The resolved pageUrl.</returns>
		public override string ResolveUrl(string pageUrl, CultureInfo uiCulture)
		{
			return this.ParseCultureInformation ? StringUtil.CombineWithForwardSlash(uiCulture.TwoLetterISOLanguageName.ToLowerInvariant(), pageUrl.TrimStart('/')) : pageUrl;
		}
	}
}