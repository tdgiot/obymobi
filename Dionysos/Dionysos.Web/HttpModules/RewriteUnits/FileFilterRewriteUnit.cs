﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Filter existing files from rewriting using the PhysicalPath of the current request.
	/// </summary>
	public class FileFilterRewriteUnit : RewriteUnitBase
	{
		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>The action the RewriteModule has to perform.</returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			RewriteAction action = RewriteAction.Continue;

			try
			{
				if (File.Exists(this.Context.Request.PhysicalPath))
				{
					// Cancel request processing
					action = RewriteAction.Exit;
				}
			}
			catch (PathTooLongException)
			{
				this.Context.Response.TrySkipIisCustomErrors = true;
				this.Context.Response.StatusCode = 400;

				action = RewriteAction.End;
			}
			catch (ArgumentException)
			{
				// Could not get physical path, return with 400 client error
				this.Context.Response.TrySkipIisCustomErrors = true;
				this.Context.Response.StatusCode = 400;

				action = RewriteAction.End;
			}

			return action;
		}
	}
}
