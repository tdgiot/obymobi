﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// If HTTPS is required, redirects HTTP requests to its HTTPS equivalent.
	/// </summary>
	public class HttpsRedirectRewriteUnit : RewriteUnitBase
	{
		#region Properties

		/// <summary>
		/// Gets a value indicating whether this rewrite unit is always used when resolving URLs (even when not hit while rewriting).
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this rewrite unit is always used when resolving URLs; otherwise, <c>false</c>.
		/// </value>
		public override bool AlwaysResolveUrl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this rewrite unit can resolve an absolute URL.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this rewrite unit can resolve an absolute URL; otherwise, <c>false</c>.
		/// </value>
		public override bool CanResolveAbsoluteUrl
		{
			get
			{
				return true;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>
		/// The action the RewriteModule has to perform.
		/// </returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			if (ConfigurationManager.GetBool(DionysosWebConfigurationConstants.RedirectToHttps) &&
				!rewrittenUrl.Uri.IsLoopback &&
				rewrittenUrl.Scheme != Uri.UriSchemeHttps)
			{
				rewrittenUrl.Scheme = Uri.UriSchemeHttps;
				rewrittenUrl.Port = -1; // Use default port for scheme

				return RewriteAction.RedirectPermanent;
			}

			return RewriteAction.Continue;
		}

		/// <summary>
		/// Resolves a pageUrl so modifications in this IRewriteUnit can be resolved back. By default no modifications will be done.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <param name="uiCulture">The CultureInfo to use when resolving the pageUrl.</param>
		/// <returns>
		/// The resolved pageUrl.
		/// </returns>
		public override string ResolveUrl(string pageUrl, CultureInfo uiCulture)
		{
			Uri absolutePageUrl;
			if (Uri.TryCreate(pageUrl, UriKind.Absolute, out absolutePageUrl) &&
				absolutePageUrl.Scheme != Uri.UriSchemeHttps)
			{
				UriBuilder resolvedPageUrl = new UriBuilder(absolutePageUrl);
				resolvedPageUrl.Scheme = Uri.UriSchemeHttps;
				resolvedPageUrl.Port = -1; // Use default port for scheme

				pageUrl = resolvedPageUrl.Uri.ToString();
			}

			return pageUrl;
		}

		#endregion
	}
}
