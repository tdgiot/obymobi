﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Web.HttpModules;
using System.IO;
using Dionysos;
using Dionysos.Web;
using System.Globalization;
using System.Net;
using Dionysos.Drawing;
using System.Drawing;
using System.Web;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	public class YouTubeThumbnailRewriteUnit : ThumbnailRewriteUnit
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="YouTubeThumbnailRewriteUnit"/> class.
		/// </summary>
		/// <param name="imageOverlayPath">The image overlay path.</param>
		public YouTubeThumbnailRewriteUnit(string imageOverlayPath)
			: this(imageOverlayPath, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="YouTubeThumbnailRewriteUnit"/> class.
		/// </summary>
		/// <param name="imageOverlayPath">The image overlay path.</param>
		/// <param name="uriTemplate">The URI template.</param>
		public YouTubeThumbnailRewriteUnit(string imageOverlayPath, string uriTemplate)
			: base(uriTemplate ?? "images/youtube/{width}x{height}/{videoId}.jpg", 480, 360, imageOverlayPath)
		{ }

		#endregion

		#region Methods

		/// <summary>
		/// Gets the thumbnail file path (downloads the thumbnail if not found).
		/// </summary>
		/// <param name="match">The URI match.</param>
		/// <returns>
		/// The physical file path to the thumbnail.
		/// </returns>
		protected override string GetThumbnailFilePath(UriTemplateMatch match)
		{
			// Get video id
			string videoId = match.BoundVariables["VIDEOID"];

			// Get YouTube thumbnail
			string thumbnailPath = Path.Combine(this.tempThumbnailDirectory, String.Format("{0}.jpg", videoId));
			if (!File.Exists(thumbnailPath))
			{
				using (WebClient wc = new WebClient())
				{
                    wc.Proxy = null;

					// Download thumbnail to temp path
					string thumbnailUrl = String.Format("http://img.youtube.com/vi/{0}/0.jpg", videoId);
					wc.DownloadFile(thumbnailUrl, thumbnailPath);
				}
			}

			return thumbnailPath;
		}

		#endregion
	}
}
