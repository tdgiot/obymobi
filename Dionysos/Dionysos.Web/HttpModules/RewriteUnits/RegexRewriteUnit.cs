﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Rewrites a matching Url to a specific page.
	/// </summary>
	public class RegexRewriteUnit : RewriteUnitBase
	{
		#region Fields

		protected readonly Dictionary<Regex, string> regexRewrites;
		protected readonly RewriteAction action;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="RegexRewriteUnit" /> class.
		/// </summary>
		/// <param name="regexRewrites">The dictionary with Regex patterns and replacement strings (uses Compiled, IgnoreCase and Singleline as Regex options).</param>
		public RegexRewriteUnit(Dictionary<string, string> regexRewrites)
			: this(regexRewrites, RewriteAction.Rewrite)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="RegexRewriteUnit" /> class.
		/// </summary>
		/// <param name="regexRewrites">The dictionary with Regex patterns and replacement strings.</param>
		/// <param name="options">The Regex options.</param>
		public RegexRewriteUnit(Dictionary<string, string> regexRewrites, RegexOptions options)
			: this(regexRewrites, options, RewriteAction.Rewrite)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="RegexRewriteUnit" /> class.
		/// </summary>
		/// <param name="regexRewrites">The dictionary with Regex patterns and replacement strings.</param>
		/// <param name="action">The action to perform when a match is found.</param>
		public RegexRewriteUnit(Dictionary<string, string> regexRewrites, RewriteAction action)
			: this(regexRewrites, RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline, action)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="RegexRewriteUnit" /> class.
		/// </summary>
		/// <param name="regexRewrites">The dictionary with Regex patterns and replacement strings.</param>
		/// <param name="options">The Regex options.</param>
		/// <param name="action">The action to perform when a match is found.</param>
		public RegexRewriteUnit(Dictionary<string, string> regexRewrites, RegexOptions options, RewriteAction action)
			: this(regexRewrites.ToDictionary(k => new Regex(k.Key, options), v => v.Value), action)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="RegexRewriteUnit" /> class.
		/// </summary>
		/// <param name="regexRewrites">The dictionary with Regex objects and replacement strings.</param>
		public RegexRewriteUnit(Dictionary<Regex, string> regexRewrites)
			: this(regexRewrites, RewriteAction.Rewrite)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="RegexRewriteUnit" /> class.
		/// </summary>
		/// <param name="regexRewrites">The dictionary with Regex objects and replacement strings.</param>
		/// <param name="action">The action to perform when a match is found.</param>
		/// <exception cref="System.ArgumentNullException">regexRewrites</exception>
		public RegexRewriteUnit(Dictionary<Regex, string> regexRewrites, RewriteAction action)
		{
			if (regexRewrites == null) throw new ArgumentNullException("regexRewrites");

			this.regexRewrites = regexRewrites;
			this.action = action;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>
		/// The action the RewriteModule has to perform.
		/// </returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			return this.Rewrite(ref rewrittenUrl, originalUrl, this.regexRewrites);
		}

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <param name="regexRewrites">The regex rewrites.</param>
		/// <returns>
		/// The action the RewriteModule has to perform.
		/// </returns>
		protected virtual RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl, Dictionary<Regex, string> regexRewrites)
		{
			UriBuilder url = rewrittenUrl.RemoveApplicationPath();

			foreach (KeyValuePair<Regex, string> regexRewrite in regexRewrites)
			{
				Match match = regexRewrite.Key.Match(url.Uri.PathAndQuery);
				if (match.Success)
				{
					// Replace the captured groups
					string[] pathAndQuery = Regex.Replace(regexRewrite.Value, @"\{([^{}]+)\}", new MatchEvaluator(m => match.Groups[m.Groups[1].Value].Value), RegexOptions.IgnoreCase).Split(new char[] { '?' }, 2, StringSplitOptions.RemoveEmptyEntries);

					// Set Path
					rewrittenUrl.Path = pathAndQuery[0];
					if (pathAndQuery.Length == 2)
					{
						// Set Query
						rewrittenUrl.Query = pathAndQuery[1];
					}

					// Add ApplicationPath back to rewrittenUrl
					rewrittenUrl = rewrittenUrl.AddApplicationPath();

					return this.action;
				}
			}

			return RewriteAction.Continue;
		}

		#endregion
	}
}
