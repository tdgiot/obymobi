﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.HttpModules;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Rewrites root to UrlRewriteDefaultPage and other pages using multiple handling files.
	/// </summary>
	public class HandlingFileRewriteUnit : RewriteUnitBase
	{
		#region Properties

		/// <summary>
		/// Gets the URL rewrite default page.
		/// </summary>
		public string UrlRewriteDefaultPage
		{
			get
			{
				return ConfigurationManager.GetString(DionysosWebConfigurationConstants.UrlRewriteDefaultPage);
			}
		}

		/// <summary>
		/// Gets a value indicating whether the URL is rewritten using multiple handling files.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the URL is rewritten using multiple handling files; otherwise, <c>false</c>.
		/// </value>
		public bool UrlRewriteByMultipleHandlingFiles
		{
			get
			{
				return ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UrlRewriteByMultipleHandlingFiles);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>
		/// The action the RewriteModule has to perform.
		/// </returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			// Remove the applicationPath
			rewrittenUrl = rewrittenUrl.RemoveApplicationPath();

			// Get PageUrl
			string pageUrl = rewrittenUrl.Path;

			// Rewrite root to UrlRewriteDefaultPage (if not associated with ContentEntity)
			if (pageUrl.Equals("/") && !String.IsNullOrEmpty(this.UrlRewriteDefaultPage))
			{
				// Rewrite to HandlingPage
				rewrittenUrl.Path = this.UrlRewriteDefaultPage;
				rewrittenUrl = rewrittenUrl.AddApplicationPath();

				return RewriteAction.Rewrite;
			}

			// Rewrite multiple handling files if set
			if (this.UrlRewriteByMultipleHandlingFiles)
			{
				List<string> pageUrlElements = pageUrl.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries).ToList();

				if (pageUrlElements.Count == 0)
				{
					pageUrlElements.Add("Default");
				}

				// Format pageUrlElements to PascalCasing
				pageUrlElements = pageUrlElements.Select(p => p.ToPascalCase()).ToList();

				string path = String.Format("{0}.aspx", StringUtil.CombineWithForwardSlash(pageUrlElements.ToArray()));
				string physicalPath = HttpContext.Current.Server.MapPath("~/" + path);

				if (!pageUrlElements.Any(p => String.IsNullOrEmpty(p)) && File.Exists(physicalPath))
				{
					// Rewrite to HandlingPage
					rewrittenUrl.Path = path;
					rewrittenUrl = rewrittenUrl.AddApplicationPath();

					return RewriteAction.Rewrite;
				}
			}

			// Add the removed applicationPath
			rewrittenUrl = rewrittenUrl.AddApplicationPath();

			return RewriteAction.Continue;
		}

		#endregion
	}
}
