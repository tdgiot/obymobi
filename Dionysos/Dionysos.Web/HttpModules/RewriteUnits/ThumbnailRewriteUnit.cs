﻿using Dionysos.Drawing;
using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	public abstract class ThumbnailRewriteUnit : RewriteUnitBase
	{
		#region Fields

		/// <summary>
		/// The UriTemplate to resolve to.
		/// </summary>
		protected readonly UriTemplate uriTemplate;

		/// <summary>
		/// The maximum width (0 if not set).
		/// </summary>
		protected readonly int maxWidth;

		/// <summary>
		/// The maximum height (0 if not set).
		/// </summary>
		protected readonly int maxHeight;

		/// <summary>
		/// The image overlay path.
		/// </summary>
		protected readonly string imageOverlayPath;

		/// <summary>
		/// The temporary thumbnail directory.
		/// </summary>
		protected readonly string tempThumbnailDirectory;

		/// <summary>
		/// The expiration time for the YouTube thumbnails.
		/// </summary>
		protected readonly TimeSpan expiration = TimeSpan.FromDays(7);

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ThumbnailRewriteUnit"/> class.
		/// </summary>
		/// <param name="uriTemplate">The URI template.</param>
		public ThumbnailRewriteUnit(string uriTemplate)
			: this(uriTemplate, 0, 0, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="ThumbnailRewriteUnit"/> class.
		/// </summary>
		/// <param name="uriTemplate">The URI template.</param>
		/// <param name="maxWidth">Width of the max.</param>
		/// <param name="maxHeight">Height of the max.</param>
		/// <param name="imageOverlayPath">The image overlay path.</param>
		public ThumbnailRewriteUnit(string uriTemplate, int maxWidth, int maxHeight, string imageOverlayPath)
		{
			if (uriTemplate == null) throw new ArgumentNullException("uriTemplate");
			if (String.IsNullOrEmpty(uriTemplate)) throw new ArgumentException("The uriTemplate cannot be empty.", "uriTemplate");

			// Set the image overlay path
			if (imageOverlayPath != null)
			{
				this.imageOverlayPath = this.Context.Server.MapPath(imageOverlayPath);

				// Validate image overlay path
				if (!File.Exists(this.imageOverlayPath)) throw new System.IO.FileNotFoundException("Could not find image overlay.", this.imageOverlayPath);
			}

			// Set the uriTemplate
			this.uriTemplate = new UriTemplate(uriTemplate);

			// Get temp directory
			string tempDirectory = this.Context.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.TempFileFolder));
			this.tempThumbnailDirectory = Path.Combine(tempDirectory, this.GetType().Name.RemoveRight("RewriteUnit"));

			// Create temp thumbnail directory (if not already exists)
			Directory.CreateDirectory(this.tempThumbnailDirectory);

			// Set maximim width and height
			this.maxWidth = maxWidth;
			this.maxHeight = maxHeight;

			// Cleanup on init
			this.CleanUp();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>
		/// The action the RewriteModule has to perform.
		/// </returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			RewriteAction action = RewriteAction.Continue;

			UriTemplateMatch match = this.uriTemplate.Match(rewrittenUrl.Uri.GetBase(), rewrittenUrl.Uri);
			if (match != null)
			{
				string thumbnailFilePath = null;
				try
				{
					thumbnailFilePath = this.GetThumbnailFilePath(match);
				}
				catch
				{
					action = RewriteAction.Exit;
				}

				if (action == RewriteAction.Continue &&
					!String.IsNullOrEmpty(thumbnailFilePath))
				{
					// Get width and height
					int width, height;
					if (String.IsNullOrEmpty(match.BoundVariables["WIDTH"]) ||
						!Int32.TryParse(match.BoundVariables["WIDTH"], NumberStyles.None, CultureInfo.InvariantCulture, out width) ||
						width < 0 ||
						(this.maxWidth != 0 && width > this.maxWidth) ||
						String.IsNullOrEmpty(match.BoundVariables["HEIGHT"]) ||
						!Int32.TryParse(match.BoundVariables["HEIGHT"], NumberStyles.None, CultureInfo.InvariantCulture, out height) ||
						height < 0 ||
						(this.maxHeight != 0 && height > this.maxHeight))
					{
						// Invalid width or height, use maximum dimensions
						width = this.maxWidth;
						height = this.maxHeight;
					}

					// Get thumbnail
					string tempThumbnailFilePath = Path.Combine(this.tempThumbnailDirectory, String.Format("{0}-w{1}-h{2}.jpg", Path.GetFileNameWithoutExtension(thumbnailFilePath), width, height));

					// Check if resized thumbnail exists
					if (!File.Exists(tempThumbnailFilePath))
					{
						// Resize and add overlay to thumbnail
						ImageUtil.ResizeAndSave(thumbnailFilePath, tempThumbnailFilePath, ImageCropPosition.Center, new Size(width, height));
						if (this.imageOverlayPath != null)
						{
							ImageUtil.AddOverlay(tempThumbnailFilePath, this.imageOverlayPath);
						}
					}

					// Add file dependancies
					this.Context.Response.AddFileDependency(thumbnailFilePath);
					this.Context.Response.AddFileDependency(tempThumbnailFilePath);

					// Add caching
					this.Context.Response.Cache.SetCacheability(HttpCacheability.Public);
					this.Context.Response.Cache.SetValidUntilExpires(false);
					this.Context.Response.Cache.SetOmitVaryStar(true);
					this.Context.Response.Cache.SetExpires(DateTime.Now.Add(this.expiration));
					this.Context.Response.Cache.SetLastModifiedFromFileDependencies();
					this.Context.Response.Cache.SetETagFromFileDependencies();

					// Transmit thumbnail
					this.Context.Response.ContentType = MimeTypeHelper.GetMimeType(tempThumbnailFilePath);
					this.Context.Response.TransmitFile(tempThumbnailFilePath);

					action = RewriteAction.End;
				}
			}

			return action;
		}

		/// <summary>
		/// Cleans up the thumbnails older than the specified expiration time.
		/// </summary>
		protected void CleanUp()
		{
			// Cleanup expired files
			foreach (string tempPath in Directory.GetFiles(this.tempThumbnailDirectory))
			{
				if (DateTime.UtcNow.Subtract(File.GetLastWriteTimeUtc(tempPath)) > this.expiration)
				{
					File.Delete(tempPath);
				}
			}
		}

		/// <summary>
		/// Gets the thumbnail file path (downloads the thumbnail if not found).
		/// </summary>
		/// <param name="match">The URI match.</param>
		/// <returns>
		/// The physical file path to the thumbnail.
		/// </returns>
		protected abstract string GetThumbnailFilePath(UriTemplateMatch match);

		#endregion
	}
}
