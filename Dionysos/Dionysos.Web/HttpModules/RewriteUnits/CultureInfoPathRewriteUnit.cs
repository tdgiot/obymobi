﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Globalization;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Parse culture information from the path (eg. /nl-nl).
	/// </summary>
	public class CultureInfoPathRewriteUnit : RewriteUnitBase
	{
		#region Fields

		/// <summary>
		/// The culture info regex.
		/// </summary>
		private readonly Regex cultureRegex = new Regex(@"^/(?<CultureName>[a-z]{2}-[a-z]{2})(/.+|$)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

		/// <summary>
		/// Use Accept-Language header.
		/// </summary>
		private static bool useAcceptLanguage = true;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating whether this rewrite unit is always used when resolving URLs (even when not hit while rewriting).
		/// </summary>
		/// <value>
		/// <c>true</c> if this rewrite unit is always used when resolving URLs; otherwise, <c>false</c>.
		/// </value>
		public override bool AlwaysResolveUrl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Gets whether to parse culture information from url; if not specified redirects to default culture.
		/// </summary>
		/// <value>
		/// <c>true</c> if [parse culture information]; otherwise, <c>false</c>.
		/// </value>
		protected bool ParseCultureInformation
		{
			get
			{
				return ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UrlRewriteCultureInformation, true);
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to use the Accept-Language header.
		/// </summary>
		/// <value>
		/// <c>true</c> if the Accept-Language header is used; otherwise, <c>false</c>.
		/// </value>
		public static bool UseAcceptLanguage
		{
			get
			{
				return useAcceptLanguage;
			}
			set
			{
				useAcceptLanguage = value;
			}
		}

		/// <summary>
		/// Gets or sets the culture validator.
		/// </summary>
		/// <value>
		/// The culture validator.
		/// </value>
		public static ICultureInfoValidator CultureValidator { get; set; }

		#endregion

		#region Methods

		/// <summary>
		/// Gets the name of the culture from the path.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns>
		/// The culture name.
		/// </returns>
		protected virtual string GetCultureName(string path)
		{
			string cultureName = null;

			Match cultureMatch = this.cultureRegex.Match(path);
			if (cultureMatch.Success)
			{
				// Get culture information from URL
				cultureName = cultureMatch.Groups["CultureName"].Value;
			}

			return cultureName;
		}

		/// <summary>
		/// Saves the culture name for the current request.
		/// </summary>
		/// <param name="cultureName">The culture name.</param>
		/// <param name="action">The action.</param>
		/// <returns>
		/// The action to use after saving the culture name.
		/// </returns>
		protected virtual void SaveCultureName(string cultureName, ref RewriteAction action)
		{ }

		/// <summary>
		/// Removes the name of the culture from the path.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns>
		/// The path with the culture name removed.
		/// </returns>
		protected virtual string RemoveCultureName(string path)
		{
			Match cultureMatch = this.cultureRegex.Match(path);
			if (cultureMatch.Success)
			{
				// Remove culture information from URL
				path = path.Remove(0, cultureMatch.Groups["CultureName"].Value.Length + 1);
			}

			return path;
		}

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>
		/// The action the RewriteModule has to perform.
		/// </returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			RewriteAction action = RewriteAction.Continue;

			// Parse culture information if needed
			if (this.ParseCultureInformation)
			{
				// Remove ApplicationPath before processing
				rewrittenUrl = rewrittenUrl.RemoveApplicationPath();

				// Get culture name
				string cultureName = this.GetCultureName(rewrittenUrl.Path);

				if (CultureInfoPathRewriteUnit.CultureValidator != null &&
					(cultureName == null || !CultureInfoPathRewriteUnit.CultureValidator.Validate(cultureName, false)))
				{
					// Reset culture name (because it is invalid)
					cultureName = null;

					// Try cultures from Accept-Language
					if (CultureInfoPathRewriteUnit.UseAcceptLanguage)
					{
						string[] userLanguages = HttpContext.Current.Request.UserLanguages ?? new string[0];
						for (int i = 0; i < userLanguages.Length; i++)
						{
							string userLanguage = userLanguages[i];

							// Remove ;q=
							decimal quality;
							if (userLanguage.Contains(";") &&
								Decimal.TryParse(userLanguage.GetAllAfterFirstOccurenceOf(";q="), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out quality) &&
								quality > 0)
							{
								userLanguage = userLanguage.GetAllBeforeFirstOccurenceOf(';', true);
							}

							// Create full culture name
							if (userLanguage.Length == 2)
							{
								if (userLanguage == "en")
								{
									userLanguage += "-gb";
								}
								else
								{
									userLanguage += "-" + userLanguage;
								}
							}

							if (CultureInfoPathRewriteUnit.CultureValidator.Validate(userLanguage, true))
							{
								cultureName = userLanguage;
								break;
							}
						}

						// Temporary redirect (user specific)
						action = RewriteAction.Redirect;
					}
					else
					{
						// Permanent redirect
						action = RewriteAction.RedirectPermanent;
					}

					if (cultureName == null)
					{
						// Use default culture
						cultureName = CultureInfoPathRewriteUnit.CultureValidator.DefaultCultureInfo.Name;
					}
				}
				else if (cultureName == null)
				{
					// Use current culture (set in Web.config)
					cultureName = CultureInfo.CurrentUICulture.Name;

					// Redirect, so culture information is stored in the URL
					action = RewriteAction.RedirectPermanent;
				}

				// Save culture name
				this.SaveCultureName(cultureName, ref action);

				// Create culture
				CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture(cultureName);
				if (action == RewriteAction.Continue)
				{
					// Remove culture information from Path
					rewrittenUrl.Path = this.RemoveCultureName(rewrittenUrl.Path);

					// Set current culture
					System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
					System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;

					// Set IsCultureSpecifiedInUrl (for backward compatibility)
					GenericRewriteModule.IsCultureSpecifiedInUrl = true;
				}
				else if (action == RewriteAction.Redirect ||
						 action == RewriteAction.RedirectPermanent)
				{
					// Redirect with culture information
					rewrittenUrl.Path = this.ResolveUrl(rewrittenUrl.Path, cultureInfo);
				}

				// Add removed ApplicationPath
				rewrittenUrl = rewrittenUrl.AddApplicationPath();
			}

			return action;
		}

		/// <summary>
		/// Resolves a pageUrl so the cultureString is added if ParseCultureInformation is true.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <param name="uiCulture">The CultureInfo to use when resolving the pageUrl.</param>
		/// <returns>
		/// The resolved pageUrl.
		/// </returns>
		/// <example>
		/// If you grab the CultureInformation from the rewrittenUrl, the returned rewrittenUrl doesn't contain the CultureInformation.
		/// To add the CultureInformation back to the pageUrl, generation of pageUrls have to call this method and this method adds back the grabbed CultureInformation.
		///   </example>
		public override string ResolveUrl(string pageUrl, CultureInfo uiCulture)
		{
			string resolvedUrl;

			if (this.ParseCultureInformation)
			{
				// Remove leading slash
				pageUrl = pageUrl.TrimStart('/');

				string cultureName = uiCulture.Name.ToLowerInvariant();
				if (String.IsNullOrEmpty(pageUrl))
				{
					// Culture name
					resolvedUrl = cultureName;
				}
				else
				{
					// Combine page url with culture name
					resolvedUrl = StringUtil.CombineWithForwardSlash(cultureName, pageUrl);
				}
			}
			else
			{
				resolvedUrl = pageUrl;
			}

			return resolvedUrl;
		}

		#endregion

		#region Interfaces

		/// <summary>
		/// Checks whether a specified culture is valid.
		/// </summary>
		public interface ICultureInfoValidator
		{
			/// <summary>
			/// Gets the default culture info.
			/// </summary>
			/// <value>
			/// The default culture info.
			/// </value>
			CultureInfo DefaultCultureInfo { get; }

			/// <summary>
			/// Validates the specified culture name.
			/// </summary>
			/// <param name="cultureName">The name of the culture.</param>
			/// <param name="acceptLanguage">If set to <c>true</c> validates whether the culture name is valid for use with the Accept-Language header.</param>
			/// <returns>
			///   <c>true</c> if the culture name is valid; otherwise, <c>false</c>.
			/// </returns>
			bool Validate(string cultureName, bool acceptLanguage);
		}

		#endregion
	}
}
