﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Web;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Parse CultureInformation from the domain name. If the domain name is not specified in the mapping, tries to redirect to the mapped domain using the default culture.
	/// </summary>
	public class CultureInfoDomainRewriteUnit : RewriteUnitBase
	{
		#region Fields

		/// <summary>
		/// ALways include the domain name (even if the specified culture is the same).
		/// </summary>
		protected readonly bool alwaysIncludeDomain;

		/// <summary>
		/// The culture info to domain mapping.
		/// </summary>
		private readonly Dictionary<CultureInfo, string> cultureInfoDomainMapping;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CultureInfoDomainRewriteUnit class.
		/// </summary>
		/// <param name="cultureInfoDomainMapping">The culture information to domain mapping.</param>
		public CultureInfoDomainRewriteUnit(Dictionary<CultureInfo, string> cultureInfoDomainMapping)
			: this(false, cultureInfoDomainMapping)
		{ }

		/// <summary>
		/// Initializes a new instance of the CultureInfoDomainRewriteUnit class.
		/// </summary>
		/// <param name="alwaysIncludeDomain">Always include the domain name (even if the specified culture is the same).</param>
		/// <param name="cultureInfoDomainMapping">The culture information to domain mapping.</param>
		public CultureInfoDomainRewriteUnit(bool alwaysIncludeDomain, Dictionary<CultureInfo, string> cultureInfoDomainMapping)
		{
			this.alwaysIncludeDomain = alwaysIncludeDomain;
			this.cultureInfoDomainMapping = cultureInfoDomainMapping ?? new Dictionary<CultureInfo, string>();
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets whether to parse culture information from url.
		/// </summary>
		protected bool ParseCultureInformation
		{
			get
			{
				return ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UrlRewriteCultureInformation, true);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>The action the RewriteModule has to perform.</returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			RewriteAction action = RewriteAction.Continue;

			// Parse culture information if needed
			if (this.ParseCultureInformation)
			{
				// Set culture information using domain mapping
				bool isCultureSpecifiedInUrl = false;
				foreach (KeyValuePair<CultureInfo, string> cultureInfoDomain in this.cultureInfoDomainMapping)
				{
					if (rewrittenUrl.Host.Equals(cultureInfoDomain.Value, StringComparison.OrdinalIgnoreCase) ||
						(!cultureInfoDomain.Value.StartsWith("www.", StringComparison.OrdinalIgnoreCase) && rewrittenUrl.Host.Equals("www." + cultureInfoDomain.Value, StringComparison.OrdinalIgnoreCase)) ||
						(cultureInfoDomain.Value.StartsWith("www.", StringComparison.OrdinalIgnoreCase) && rewrittenUrl.Host.Equals(cultureInfoDomain.Value.RemoveLeft("www.", StringComparison.OrdinalIgnoreCase), StringComparison.OrdinalIgnoreCase)))
					{
						// Set culture information
						System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfoDomain.Key;
						System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfoDomain.Key;
						isCultureSpecifiedInUrl = true;
						break;
					}
				}

				// If not set, try to redirect to default culture domain
				if (!isCultureSpecifiedInUrl && !rewrittenUrl.Uri.IsLoopback)
				{
					string domain;
					if (this.cultureInfoDomainMapping.TryGetValue(CultureInfo.CurrentUICulture, out domain) &&
						!rewrittenUrl.Host.Equals(domain))
					{
						// Set domain
						rewrittenUrl.Host = domain;
						action = RewriteAction.RedirectPermanent;
						isCultureSpecifiedInUrl = true;
					}
				}

				// Set IsCultureSpecifiedInUrl (for backward compatibility)
				GenericRewriteModule.IsCultureSpecifiedInUrl = isCultureSpecifiedInUrl;
			}

			return action;
		}

		/// <summary>
		/// Resolves a pageUrl so modifications in this IRewriteUnit can be resolved back. By default no modifications will be done.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <param name="uiCulture">The CultureInfo to use when resolving the pageUrl.</param>
		/// <returns>The resolved pageUrl.</returns>
		public override string ResolveUrl(string pageUrl, CultureInfo uiCulture)
		{
			// Get domain for culture information
			string domain;
			if (this.ParseCultureInformation &&
				this.cultureInfoDomainMapping.TryGetValue(uiCulture, out domain) &&
				(this.alwaysIncludeDomain || uiCulture.Name != CultureInfo.CurrentUICulture.Name))
			{
				// Trim trailing slash on pageUrl
				pageUrl = pageUrl.TrimEnd('/');

				// Construct base url
				Uri baseUri = HttpContext.Current.Request.Url.GetBase();
				UriBuilder url = new UriBuilder(baseUri.ToString() + pageUrl);

				// Replace domain
				url.Host = domain;

				// Reconstruct pageUrl
				pageUrl = url.Uri.ToString();
			}

			return pageUrl;
		}

		#endregion
	}
}
