﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	public class CultureInfoCookieRewriteUnit : CultureInfoPathRewriteUnit
	{
		#region Fields

		/// <summary>
		/// The cookie name.
		/// </summary>
		private readonly string cookieName;

		/// <summary>
		/// The the before the cookie expires (null to use session).
		/// </summary>
		private readonly TimeSpan? cookieExpires;

		/// <summary>
		/// The query string name.
		/// </summary>
		private readonly string queryStringName;

		#endregion

		#region Contructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CultureInfoCookieRewriteUnit" /> class.
		/// </summary>
		public CultureInfoCookieRewriteUnit()
			: this("CultureInfo", null, "CultureInfo")
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="CultureInfoCookieRewriteUnit" /> class.
		/// </summary>
		/// <param name="cookieName">The name of the cookie.</param>
		/// <param name="cookieExpires">The the before the cookie expires (null to use session).</param>
		/// <param name="queryStringName">The name of the query string.</param>
		/// <exception cref="System.ArgumentNullException">
		/// cookieName
		/// or
		/// queryStringName
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// The cookie name cannot be empty.;cookieName
		/// or
		/// The query string name cannot be empty.;queryStringName
		/// </exception>
		public CultureInfoCookieRewriteUnit(string cookieName, TimeSpan? cookieExpires, string queryStringName)
		{
			if (cookieName == null) throw new ArgumentNullException("cookieName");
			if (String.IsNullOrEmpty(cookieName)) throw new ArgumentException("The cookie name cannot be empty.", "cookieName");
			if (queryStringName == null) throw new ArgumentNullException("queryStringName");
			if (String.IsNullOrEmpty(queryStringName)) throw new ArgumentException("The query string name cannot be empty.", "queryStringName");

			this.cookieName = cookieName;
			this.cookieExpires = cookieExpires;
			this.queryStringName = queryStringName;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets the name of the culture from the cookie.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns>
		/// The culture name.
		/// </returns>
		protected override string GetCultureName(string path)
		{
			string cultureName = null;

			HttpCookie cookie = this.Context.Response.Cookies[this.cookieName];
			if (cookie != null &&
				String.IsNullOrEmpty(cookie.Value))
			{
				// Use request cookie is response is empty
				cookie = this.Context.Request.Cookies[this.cookieName];
			}

			if (cookie != null)
			{
				// Get value from cookie
				cultureName = cookie.Value;
			}

			return cultureName;
		}

		/// <summary>
		/// Saves the culture name.
		/// </summary>
		/// <param name="cultureName">The culture name.</param>
		protected override void SaveCultureName(string cultureName, ref RewriteAction action)
		{
			// Store value in cookie and remove query
			this.Context.Response.Cookies.Set(new HttpCookie(this.cookieName, cultureName)
			{
				Expires = this.cookieExpires.HasValue ? DateTime.Now.Add(this.cookieExpires.Value) : DateTime.MinValue
			});

			action = RewriteAction.Continue;
		}

		/// <summary>
		/// Removes the name of the culture from the path.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns>The path.</returns>
		protected override string RemoveCultureName(string path)
		{
			// Do nothing with the path (culture is stored in cookie)
			return path;
		}

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>
		/// The action the RewriteModule has to perform.
		/// </returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			RewriteAction action = RewriteAction.Continue;

			NameValueCollection queryString = HttpUtility.ParseQueryString(rewrittenUrl.Query);
			string cultureName = queryString[this.queryStringName];
			if (!String.IsNullOrEmpty(cultureName) &&
				(CultureInfoPathRewriteUnit.CultureValidator == null || CultureInfoPathRewriteUnit.CultureValidator.Validate(cultureName, false)))
			{
				// Save culture to cookie
				this.SaveCultureName(cultureName, ref action);
			}

			return base.Rewrite(ref rewrittenUrl, originalUrl);
		}

		/// <summary>
		/// Resolves a pageUrl so the cultureString is added if ParseCultureInformation is true.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <param name="uiCulture">The CultureInfo to use when resolving the pageUrl.</param>
		/// <returns>
		/// The resolved pageUrl.
		/// </returns>
		public override string ResolveUrl(string pageUrl, CultureInfo uiCulture)
		{
			string resolvedUrl;

			if (this.ParseCultureInformation &&
				this.GetCultureName(pageUrl) != uiCulture.Name)
			{
				// Add culture info to query string
				QueryStringHelper qsh = new QueryStringHelper();
				qsh.AddItem(this.queryStringName, uiCulture.Name);

				resolvedUrl = qsh.MergeQuerystringWithUrl(pageUrl, true);
			}
			else
			{
				resolvedUrl = pageUrl;
			}

			return resolvedUrl;
		}

		#endregion
	}
}
