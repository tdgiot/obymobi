﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Redirect to preferred domain if the current domain doesn't match (except for localhost) and adds the specified culture info to the path.
	/// </summary>
	public class PreferredDomainCultureInfoPathRewriteUnit : RewriteUnitBase
	{
		#region Fields

		/// <summary>
		/// The preferred domain.
		/// </summary>
		private readonly string preferredDomain;

		/// <summary>
		/// The domain to culture info mapping.
		/// </summary>
		private readonly Dictionary<string, CultureInfo> domainCultureInfoMapping;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PreferredDomainCultureInfoPathRewriteUnit class.
		/// </summary>
		/// <param name="domainCultureInfoMapping">The domain to culture info mapping.</param>
		public PreferredDomainCultureInfoPathRewriteUnit(Dictionary<string, CultureInfo> domainCultureInfoMapping)
			: this(null, domainCultureInfoMapping)
		{
			this.preferredDomain = ConfigurationManager.GetString(DionysosWebConfigurationConstants.PreferredDomain, true);
		}

		/// <summary>
		/// Initializes a new instance of the PreferredDomainCultureInfoPathRewriteUnit class.
		/// </summary>
		/// <param name="preferredDomain">The preferred domain.</param>
		/// <param name="domainCultureInfoMapping">The domain to culture info mapping.</param>
		public PreferredDomainCultureInfoPathRewriteUnit(string preferredDomain, Dictionary<string, CultureInfo> domainCultureInfoMapping)
		{
			this.preferredDomain = preferredDomain;
			this.domainCultureInfoMapping = domainCultureInfoMapping ?? new Dictionary<string, CultureInfo>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>The action the RewriteModule has to perform.</returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			if (!rewrittenUrl.Uri.IsLoopback &&
				!String.IsNullOrEmpty(this.preferredDomain) &&
				!rewrittenUrl.Host.Equals(this.preferredDomain, StringComparison.OrdinalIgnoreCase))
			{
				// Remove ApplicationPath
				rewrittenUrl = rewrittenUrl.RemoveApplicationPath();

				// Redirect domain to currect culture info
				foreach (KeyValuePair<string, CultureInfo> domainCultureInfo in this.domainCultureInfoMapping)
				{
					if (rewrittenUrl.Host.Equals(domainCultureInfo.Key, StringComparison.OrdinalIgnoreCase) ||
						(!domainCultureInfo.Key.StartsWith("www.", StringComparison.OrdinalIgnoreCase) && rewrittenUrl.Host.Equals("www." + domainCultureInfo.Key, StringComparison.OrdinalIgnoreCase)))
					{
						// Add culture info
						rewrittenUrl.Path = StringUtil.CombineWithForwardSlash(domainCultureInfo.Value.Name.ToLowerInvariant(), rewrittenUrl.Path);
						break;
					}
				}

				// Set host to the preferred domain (so we don't create a loop)
				rewrittenUrl.Host = this.preferredDomain;

				// Add ApplicationPath
				rewrittenUrl = rewrittenUrl.AddApplicationPath();

				// Redirect to new url
				return RewriteAction.RedirectPermanent;
			}

			return RewriteAction.Continue;
		}

		#endregion
	}
}
