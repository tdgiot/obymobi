﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Redirect to preferred domain if the current domain doesn't match (except for localhost).
	/// </summary>
	public class PreferredDomainRewriteUnit : RewriteUnitBase
	{
		#region Fields

		/// <summary>
		/// The preferred domain.
		/// </summary>
		private readonly string preferredDomain;

		/// <summary>
		/// The type of domain rewriting.
		/// </summary>
		private readonly DomainRewriteType? domainRewriteType;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating whether this rewrite unit is always used when resolving URLs (even when not hit while rewriting).
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this rewrite unit is always used when resolving URLs; otherwise, <c>false</c>.
		/// </value>
		public override bool AlwaysResolveUrl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this rewrite unit can resolve an absolute URL.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this rewrite unit can resolve an absolute URL; otherwise, <c>false</c>.
		/// </value>
		public override bool CanResolveAbsoluteUrl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Gets the preferred domain.
		/// </summary>
		protected string PreferredDomain
		{
			get
			{
				return this.preferredDomain ?? ConfigurationManager.GetString(DionysosWebConfigurationConstants.PreferredDomain);
			}
		}

		/// <summary>
		/// Gets the type of domain rewriting.
		/// </summary>
		/// <value>
		/// The type of domain rewriting.
		/// </value>
		protected DomainRewriteType DomainRewriteType
		{
			get
			{
				return this.domainRewriteType ?? (DomainRewriteType)ConfigurationManager.GetInt(DionysosWebConfigurationConstants.PreferredDomainRewriteType);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PreferredDomainRewriteUnit class.
		/// </summary>
		public PreferredDomainRewriteUnit()
			: this(null, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the PreferredDomainRewriteUnit class.
		/// </summary>
		/// <param name="preferredDomain">The preferred domain.</param>
		/// <param name="domainRewriteType">The type of domain rewriting.</param>
		public PreferredDomainRewriteUnit(string preferredDomain, DomainRewriteType? domainRewriteType)
		{
			this.preferredDomain = preferredDomain;
			this.domainRewriteType = domainRewriteType;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>The action the RewriteModule has to perform.</returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			RewriteAction action = RewriteAction.Continue;

			// Check for preferred domain
			if (!rewrittenUrl.Uri.IsLoopback)
			{
				switch (this.DomainRewriteType)
				{
					case DomainRewriteType.PreferredDomain:
						if (!String.IsNullOrEmpty(this.PreferredDomain) &&
							!rewrittenUrl.Host.Equals(this.PreferredDomain, StringComparison.OrdinalIgnoreCase))
						{
							// Replace host with preferred domain
							rewrittenUrl.Host = this.PreferredDomain;
							action = RewriteAction.RedirectPermanent;
						}
						break;
					case DomainRewriteType.AddWww:
						if (rewrittenUrl.Uri.HostNameType == UriHostNameType.Dns &&
							!rewrittenUrl.Host.StartsWith("www."))
						{
							// Add www. prefix
							rewrittenUrl.Host = rewrittenUrl.Host.Insert(0, "www.");
							action = RewriteAction.RedirectPermanent;
						}
						break;
					case DomainRewriteType.RemoveWww:
						if (rewrittenUrl.Uri.HostNameType == UriHostNameType.Dns &&
							rewrittenUrl.Host.StartsWith("www."))
						{
							// Remove www. prefix
							rewrittenUrl.Host = rewrittenUrl.Host.Remove(0, "www.".Length);
							action = RewriteAction.RedirectPermanent;
						}
						break;
				}
			}

			return action;
		}

		/// <summary>
		/// Resolves a pageUrl so modifications in this IRewriteUnit can be resolved back. By default no modifications will be done.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <param name="uiCulture">The CultureInfo to use when resolving the pageUrl.</param>
		/// <returns>
		/// The resolved pageUrl.
		/// </returns>
		/// <example>
		/// If you grab the CultureInformation from the rewrittenUrl, the returned rewrittenUrl doesn't contain the CultureInformation.
		/// To add the CultureInformation back to the pageUrl, generation of pageUrls have to call this method and this method adds back the grabbed CultureInformation.
		///   </example>
		public override string ResolveUrl(string pageUrl, CultureInfo uiCulture)
		{
			Uri absolutePageUrl;
			if (Uri.TryCreate(pageUrl, UriKind.Absolute, out absolutePageUrl))
			{
				switch (this.DomainRewriteType)
				{
					case DomainRewriteType.AddWww:
						if (!absolutePageUrl.Host.StartsWith("www."))
						{
							// Add www
							UriBuilder resolvedPageUrl = new UriBuilder(absolutePageUrl);
							resolvedPageUrl.Host = "www." + resolvedPageUrl.Host;
							pageUrl = resolvedPageUrl.Uri.ToString();
						}
						break;
					case DomainRewriteType.RemoveWww:
						if (absolutePageUrl.Host.StartsWith("www."))
						{
							// Remove www
							UriBuilder resolvedPageUrl = new UriBuilder(absolutePageUrl);
							resolvedPageUrl.Host = resolvedPageUrl.Host.Substring(0, "www.".Length);
							pageUrl = resolvedPageUrl.Uri.ToString();
						}
						break;
				}
			}

			return pageUrl;
		}

		#endregion
	}

	/// <summary>
	/// The type of domain rewriting.
	/// </summary>
	public enum DomainRewriteType
	{
		/// <summary>
		/// Rewrite only to the preferred domain (if set).
		/// </summary>
		PreferredDomain,
		/// <summary>
		/// Add www. prefix to any incoming domain.
		/// </summary>
		AddWww,
		/// <summary>
		/// Remove www. prefix from any incoming domain.
		/// </summary>
		RemoveWww
	}
}
