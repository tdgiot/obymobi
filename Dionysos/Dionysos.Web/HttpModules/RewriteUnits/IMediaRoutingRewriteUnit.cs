﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Drawing;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Represents an interface for getting the routed media paths.
	/// </summary>
	/// <typeparam name="T">The entity type.</typeparam>
	public interface IMediaRoutingRewriteUnit<T>
		where T : IEntity
	{
		#region Methods

		/// <summary>
		/// Gets the relative path.
		/// </summary>
		/// <param name="media">The media.</param>
		/// <returns>
		/// The relative path; otherwise, <c>null</c>.
		/// </returns>
		string GetRelativePath(T media);

		/// <summary>
		/// Gets the relative image path.
		/// </summary>
		/// <param name="media">The media.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="mediaRatioTypeSystemName">Name of the media ratio type system.</param>
		/// <returns>
		/// The relative image path; otherwise, <c>null</c>.
		/// </returns>
		string GetRelativeImagePath(T media, int width, int height, string mediaRatioTypeSystemName);

		/// <summary>
		/// Gets the relative image path.
		/// </summary>
		/// <param name="media">The media.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="margins">The margins.</param>
		/// <param name="backgroundColor">The hexadecimal background color for the margins.</param>
		/// <returns>
		/// The relative image path; otherwise, <c>null</c>.
		/// </returns>
		string GetRelativeImagePath(T media, int width, int height, Margins margins, string backgroundColor);

		#endregion
	}
}
