﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	/// <summary>
	/// Filter existing directories from rewriting using the PhysicalPath of the current request.
	/// </summary>
	public class DirectoryFilterRewriteUnit : RewriteUnitBase
	{
		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>The action the RewriteModule has to perform.</returns>
		public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
		{
			string physicalPath = HttpContext.Current.Request.PhysicalPath;
			if (Directory.Exists(physicalPath))
			{
				// Cancel request processing
				return RewriteAction.Exit;
			}

			return RewriteAction.Continue;
		}
	}
}
