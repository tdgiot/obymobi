﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
    /// <summary>
    /// If an ApplicationPath is used, makes sure the root url ends with a slash.
    /// </summary>
    public class ApplicationPathRewriteUnit : RewriteUnitBase
    {
        /// <summary>
        /// Rewrites a request.
        /// </summary>
        /// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
        /// <param name="originalUrl">The original Uri the request was made on.</param>
        /// <returns>The action the RewriteModule has to perform.</returns>
        public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
        {
            UriBuilder tempRewrittenUrl = rewrittenUrl.RemoveApplicationPath();

            if (!rewrittenUrl.Path.EndsWith("/") && tempRewrittenUrl.Path.Equals("/"))
            {
                // The original request didn't end with a slash and it is the root url
                rewrittenUrl.Path += "/";

                return RewriteAction.RedirectPermanent;
            }

            return RewriteAction.Continue;
        }
    }
}
