﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
    /// <summary>
    /// Rewrites requests using the 404 method.
    /// </summary>
    class Legacy404MethodRewriteUnit : RewriteUnitBase
    {
        /// <summary>
        /// Rewrites a request.
        /// </summary>
        /// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
        /// <param name="originalUrl">The original Uri the request was made on.</param>
        /// <returns>The action the RewriteModule has to perform.</returns>
        public override RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl)
        {
            if (rewrittenUrl.Query.StartsWith("?404;"))
            {
                rewrittenUrl = new UriBuilder(rewrittenUrl.Query.Substring(5));
            }

            return RewriteAction.Continue;
        }
    }
}
