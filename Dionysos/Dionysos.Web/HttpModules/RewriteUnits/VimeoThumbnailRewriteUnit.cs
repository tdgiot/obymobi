﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Web.HttpModules;
using System.IO;
using Dionysos;
using Dionysos.Web;
using System.Globalization;
using System.Net;
using Dionysos.Drawing;
using System.Drawing;
using System.Web;

namespace Dionysos.Web.HttpModules.RewriteUnits
{
	public class VimeoThumbnailRewriteUnit : ThumbnailRewriteUnit
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="VimeoThumbnailRewriteUnit"/> class.
		/// </summary>
		/// <param name="imageOverlayPath">The image overlay path.</param>
		public VimeoThumbnailRewriteUnit(string imageOverlayPath)
			: this(imageOverlayPath, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="VimeoThumbnailRewriteUnit"/> class.
		/// </summary>
		/// <param name="imageOverlayPath">The image overlay path.</param>
		/// <param name="uriTemplate">The URI template.</param>
		public VimeoThumbnailRewriteUnit(string imageOverlayPath, string uriTemplate)
			: base(uriTemplate ?? "images/vimeo/{width}x{height}/{videoId}.jpg", 640, 360, imageOverlayPath)
		{ }

		#endregion

		#region Methods

		/// <summary>
		/// Gets the thumbnail file path (downloads the thumbnail if not found).
		/// </summary>
		/// <param name="match">The URI match.</param>
		/// <returns>
		/// The physical file path to the thumbnail.
		/// </returns>
		protected override string GetThumbnailFilePath(UriTemplateMatch match)
		{
			// Get video id
			string videoId = match.BoundVariables["VIDEOID"];

			// Get YouTube thumbnail
			string thumbnailPath = Path.Combine(this.tempThumbnailDirectory, String.Format("{0}.jpg", videoId));
			if (!File.Exists(thumbnailPath))
			{
				using (WebClient wc = new WebClient())
				{
                    wc.Proxy = null;

					// Download thumbnail to temp path
					string thumbnailUrl = String.Format("http://ts.vimeo.com.s3.amazonaws.com/{0}/{1}/{2}_640.jpg.jpg", videoId.Substring(0, 3), videoId.Substring(3, 3), videoId);
					wc.DownloadFile(thumbnailUrl, thumbnailPath);
				}
			}

			return thumbnailPath;
		}

		#endregion
	}
}
