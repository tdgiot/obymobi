﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.HttpModules.ViewState
{
    public class MoveViewStateModule : System.Web.IHttpModule
    {
        public MoveViewStateModule() { }
        void System.Web.IHttpModule.Dispose() { }

        void System.Web.IHttpModule.Init(System.Web.HttpApplication context)
        {
            context.BeginRequest += new EventHandler(this.BeginRequestHandler);
        }

        void BeginRequestHandler(object sender, EventArgs e)
        {
            System.Web.HttpApplication application = (System.Web.HttpApplication)sender;
            application.Response.Filter = new MoveViewStateFilter(application.Response.Filter);
        }
    }
}
