﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.HttpModules
{
	/// <summary>
	/// Adds a RewriteEntity, RewritePageUrl and RewritePageUrlResolved property to the <see cref="Dionysos.Web.UI.PageDefault"/> class.
	/// </summary>
	public abstract class RewriteEntityPage : Dionysos.Web.UI.PageDefault
	{
		/// <summary>
		/// Gets the RewriteEntity from the HttpContext.Items. Redirects to 404 page if no RewriteEntity is found.
		/// </summary>
		public virtual IEntity RewriteEntity
		{
			get
			{
				IEntity rewriteEntity = this.Context.Items["RewriteEntity"] as IEntity;

				if (rewriteEntity == null)
				{
					WebShortcuts.RedirectTo404Page(true);
				}

				return rewriteEntity;
			}
		}

		/// <summary>
		/// Gets the PageUrl of the current RewriteEntity from the HttpContext.Items.
		/// </summary>
		public string RewritePageUrl
		{
			get
			{
				return this.Context.Items["RewritePageUrl"] as string;
			}
		}

		/// <summary>
		/// Gets the resolved PageUrl of the current RewriteEntity from the HttpContext.Items.
		/// </summary>
		public string RewritePageUrlResolved
		{
			get
			{
				string rewritePageUrl = this.RewritePageUrl;

				if (rewritePageUrl != null)
				{
					rewritePageUrl = rewritePageUrl.TrimStart('/');
				}

				return RewriteModule.ResolveUrl(rewritePageUrl);
			}
		}
	}

	/// <summary>
	/// Adds a strongly typed RewriteEntity <see cref="Dionysos.Web.HttpModules.RewriteEntityPage"/> class.
	/// </summary>
	/// <typeparam name="T">The type of the RewriteEntity.</typeparam>
	public abstract class RewriteEntityPage<T> : RewriteEntityPage
		where T : class, IEntity
	{
		/// <summary>
		/// Gets the strongly typed RewriteEntity. Redirects to 404 page if no RewriteEntity is found.
		/// </summary>
		public virtual new T RewriteEntity
		{
			get
			{
				return base.RewriteEntity as T;
			}
		}
	}
}
