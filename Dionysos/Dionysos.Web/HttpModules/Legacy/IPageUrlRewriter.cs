﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.HttpModules
{
	/// <summary>
	/// Interface for classes that can be set to the GenericRewriteModule to alter/replace it's rewrite logic
	/// </summary>
	public interface IPageUrlRewriter
	{
		/// <summary>
		/// Rewrite a URL and return if it was successfull (means a (custom) rule was found for this request.
		/// </summary>
		/// <param name="pageUrl">PageUrl as requested by the HTTP request</param>
		/// <param name="rewrittenPageUrl">Rewritten PageUrl as it should be handled</param>
		/// <param name="handlingPage">Page that should handle the request (string.Empty means default)</param>
		/// <returns>If a (custom) rule was found to rewrite this request</returns>
		bool Rewrite(string pageUrl, out string rewrittenPageUrl, out string handlingPage);
	}
}
