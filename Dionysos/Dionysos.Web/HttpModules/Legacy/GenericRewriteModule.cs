﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using Dionysos;
using Dionysos.Collections;

namespace Dionysos.Web.HttpModules
{
	/// <summary>
	/// Provides generic rewriting of requests
	/// </summary>
	public class GenericRewriteModule : System.Web.IHttpModule
	{
		#region Fields

		/// <summary>
		/// The DateTime the BeginRequest event occurred
		/// </summary>
		private DateTime beginRequest;

		/// <summary>
		/// HttpContextItemKeyOriginalRawUrl
		/// </summary>
		public const string HttpContextItemKeyOriginalRawUrl = "OriginalRawUrl";
		/// <summary>
		/// HttpContextItemKeyOriginalRequestPath
		/// </summary>
		public const string HttpContextItemKeyOriginalRequestPath = "OriginalRequestPath";
		/// <summary>
		/// HttpContextItemKeyIsUrlRewritten
		/// </summary>
		public const string HttpContextItemKeyIsUrlRewritten = "IsUrlRewritten";
		/// <summary>
		/// HttpContextItemKeyHandlingPage
		/// </summary>
		public const string HttpContextItemKeyHandlingPage = "HttpContextItemKeyHandlingPage";
		/// <summary>
		/// HttpContextItemKeyUsing404Method
		/// </summary>
		public const string HttpContextItemKeyUsing404Method = "HttpContextItemKeyUsing404Method";
		/// <summary>
		/// HttpContextItemKeyIsDefaultHandlingPage
		/// </summary>
		public const string HttpContextItemKeyIsDefaultHandlingPage = "HttpContextItemKeyIsDefaultHandlingPage";
		/// <summary>
		/// HttpContextItemKeyCultureSpecifiedInUrl
		/// </summary>
		public const string HttpContextItemKeyCultureSpecifiedInUrl = "HttpContextItemKeyCultureSpecifiedInUrl";

		#endregion

		#region IHttpModule Members

		/// <summary>
		/// Initializes a module and prepares it to handle requests.
		/// </summary>
		/// <param name="context">An System.Web.HttpApplication that provides access to the methods, properties, and events common to all application objects within an ASP.NET application</param>
		public void Init(System.Web.HttpApplication context)
		{
			context.ReleaseRequestState += new EventHandler(context_ReleaseRequestState);
			context.BeginRequest += new EventHandler(context_BeginRequest);
			context.EndRequest += new EventHandler(context_EndRequest);
		}

		/// <summary>
		/// Disposes of the resources (other than memory) used by the module that implements System.Web.IHttpModule.
		/// </summary>
		void IHttpModule.Dispose()
		{
		}

		#endregion

		#region Methods

		/// <summary>
		/// Check whether the current path of the request contains an excluded path from the configuration
		/// </summary>
		/// <param name="path">The current path of the request</param>
		/// <returns>True if the path is excluded, False if not</returns>
		private bool CheckIfPathIsExcluded(string path)
		{
			bool isExcluded = false;

			// Get the excluded paths from the configuration
			string excludedPathString = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.ExcludedPathsInPathRewriting, true);
			if (excludedPathString.Length > 0)
			{
				// Split the excluded paths into an string array
				string[] excludedPaths = excludedPathString.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);

				// Walk through the excluded paths and check whether the path of the request contains an excluded path
				for (int i = 0; i < excludedPaths.Length; i++)
				{
					if (path.Contains(excludedPaths[i], StringComparison.OrdinalIgnoreCase))
					{
						isExcluded = true;
						break;
					}
				}
			}

			return isExcluded;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Array of filenames without extension in the root, set like: [default, contact, companyprofile, etc]
		/// </summary>
		private string[] aspxFilesWithoutExtensionInApplicationRoot
		{
			get
			{
				string[] filenamesOnly;

				if (!CacheHelper.TryGetValue("AspxFilesInApplicationRoot", false, out filenamesOnly))
				{
					string mappedApplicationPath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath);
					string[] files = Directory.GetFiles(mappedApplicationPath, "*.aspx");

					filenamesOnly = new string[files.Length];
					for (int i = 0; i < files.Length; i++)
					{
						string current = files[i];
						filenamesOnly[i] = Path.GetFileNameWithoutExtension(files[i]);
					}

					CacheHelper.AddAbsoluteExpire(false, "AspxFilesInApplicationRoot", filenamesOnly, 120);
				}

				return filenamesOnly;
			}
		}

		/// <summary>
		/// Custom PageUrlRewriter for handling custom rules for PageUrl's
		/// </summary>
		public static IPageUrlRewriter CustomPageUrlRewriter
		{
			get
			{
				if (HttpContext.Current.Application["PageUrlRewriter"] != null)
					return (IPageUrlRewriter)HttpContext.Current.Application["PageUrlRewriter"];
				else
					return null;
			}
			set
			{
				HttpContext.Current.Application["PageUrlRewriter"] = value;
			}
		}

		/// <summary>
		/// Original RawUrl (before rewrite)
		/// </summary>
		public static string OriginalRawUrl
		{
			get
			{
				if (HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyOriginalRawUrl] != null)
				{
					return (string)HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyOriginalRawUrl];
				}
				else
				{
					return HttpContext.Current.Request.RawUrl;
				}
			}
			set
			{
				HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyOriginalRawUrl] = value;
			}
		}

		/// <summary>
		/// Original RequestPath (before rewrite)
		/// </summary>
		public static string OriginalRequestPath
		{
			get
			{
				if (HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyOriginalRequestPath] != null)
				{
					return (string)HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyOriginalRequestPath];
				}
				else
				{
					return HttpContext.Current.Request.Path;
				}
			}
			set
			{
				HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyOriginalRequestPath] = value;
			}
		}

		/// <summary>
		/// HandlingPage of the request (including .aspx)
		/// </summary>
		public static string HandlingPage
		{
			get
			{
				if (HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyHandlingPage] != null)
				{
					return (string)HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyHandlingPage];
				}
				else
				{
					return string.Empty;
				}
			}
			set
			{
				HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyHandlingPage] = value;
			}
		}

		/// <summary>
		/// Indicates if the GenericRewriteModule is using the 404 method
		/// </summary>
		public static bool Using404Method
		{
			get
			{
				if (HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyUsing404Method] != null)
					return (bool)HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyUsing404Method];
				else
					return false;
			}
			set
			{
				HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyUsing404Method] = value;
			}
		}

		/// <summary>
		/// Indicates if the HandlingPage is the default
		/// </summary>
		public static bool HandledByDefaultHandlingPage
		{
			get
			{
				if (HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyIsDefaultHandlingPage] != null)
					return (bool)HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyIsDefaultHandlingPage];
				else
					return false;
			}
			set
			{
				HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyIsDefaultHandlingPage] = value;
			}
		}

		/// <summary>
		/// Indicates if the current request included a Culture part
		/// </summary>
		public static bool IsCultureSpecifiedInUrl
		{
			get
			{
				if (HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyCultureSpecifiedInUrl] != null)
					return (bool)HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyCultureSpecifiedInUrl];
				else
					return false;
			}
			set
			{
				HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyCultureSpecifiedInUrl] = value;
			}
		}

		/// <summary>
		/// Indicates if the current request has been rewritten
		/// </summary>
		public static bool UrlRewritten
		{
			get
			{
				if (HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyIsUrlRewritten] != null)
					return (bool)HttpContext.Current.Items[GenericRewriteModule.HttpContextItemKeyIsUrlRewritten];
				else
					return false;
			}
		}

		/// <summary>
		/// Inidicate if the rewriter should read the culture from the requesturl and strip it off
		/// </summary>
		public static bool UrlRewriteCultureInformation
		{
			get
			{
				return Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UrlRewriteCultureInformation, true);
			}
		}

		/// <summary>
		/// Indicate if the rewriter should parse the whole url (except for culture if ParseCultureInformation == true) and add it as PageUrl to the querystring
		/// </summary>
		public static bool UrlRewritePageUrl
		{
			get
			{
				return Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UrlRewritePageUrl, true);
			}
		}

		/// <summary>
		/// The page which handles the PageUrl rewritten requests
		/// </summary>
		public static string UrlRewriteHandlingFile
		{
			get
			{
				return Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.UrlRewriteHandlingFile, true);
			}
		}

		/// <summary>
		/// The default to redirect to if no specific page is requested
		/// </summary>
		public static string UrlRewriteDefaultPage
		{
			get
			{
				return Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.UrlRewriteDefaultPage, true);
			}
		}


		/// <summary>
		/// Indicate if the rewriter should check for multiple HandlingFile's
		/// </summary>
		public static bool UrlRewriteByMultipleHandlingFiles
		{
			get
			{
				return Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UrlRewriteByMultipleHandlingFiles, true);
			}
		}

		/// <summary>
		/// The preferred domain or String.Empty if no domain is set
		/// </summary>
		public static string PreferredDomain
		{
			get
			{
				return Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.PreferredDomain, true);
			}
		}

		#endregion

		#region Events

		private void context_ReleaseRequestState(object sender, EventArgs e)
		{
			HttpResponse response = HttpContext.Current.Response;

			if (response.ContentType == "text/html")
			{
				response.Filter = new ResponseFilter(response.Filter);
			}
		}

		private void context_BeginRequest(object sender, EventArgs e)
		{
			// Set DateTime of the BeginRequest event
			this.beginRequest = DateTime.Now;

			// Start code for rewrite
			HttpApplication app = (HttpApplication)sender;

			string tryPath = app.Context.Request.Path.ToLower();

			Hitman.AddToText("Start request, tryPath = {0}", tryPath);

			// In case of II6 we need to work with the 404 method.
			bool using404Method = false;
			string rawUrl404 = string.Empty;
			if (app.Request.RawUrl.Contains("404;"))
			{
				using404Method = true;
				tryPath = UrlUtil.GetRequestUrlFrom404Request(app.Request.RawUrl);
				rawUrl404 = tryPath;

				Hitman.AddToText("Using 404 method (IIS6), tryPath = {0}", tryPath);
			}

			// Check for preferred domain (before rewriting request)
			string preferredDomain = GenericRewriteModule.PreferredDomain;
			if (!String.IsNullOrEmpty(preferredDomain) && !app.Request.Url.Host.Equals("localhost", StringComparison.OrdinalIgnoreCase))
			{
				// Using preferred domain
				string currentDomain = app.Request.Url.Host;
				if (!currentDomain.Equals(preferredDomain, StringComparison.OrdinalIgnoreCase))
				{
					// Get the current URI
					string currentUri = app.Request.Url.ToString();

					// If we are using the 404 method, use the original URL (not the one with ;404)
					if (using404Method)
					{
						// Create new URI by replacing the currentDomain with the preferredDomain
						currentUri = UrlUtil.GetRequestUrlFrom404Request(currentUri);
					}

					// Create new URI by replacing the currentDomain with the preferredDomain
					UriBuilder newUri = new UriBuilder(currentUri);
					newUri.Host = preferredDomain;

					// Get the new URI
					string redirectTo = newUri.ToString();

					// Remove Default.aspx from the URI
					redirectTo = Regex.Replace(redirectTo, "Default.aspx$", "", RegexOptions.IgnoreCase);

					// Redirect to preferred domain
					WebShortcuts.RedirectPermanent(redirectTo);
				}
			}

			// Don't rewrite ajax calls
			if (tryPath.Contains("/images/") ||
				tryPath.Contains("/css/") ||
				tryPath.Contains("/js/") ||
				tryPath.Contains("/swf/") ||
				tryPath.Contains("/media/") ||
				tryPath.Contains(".css") ||
				tryPath.Contains(".ico") ||
				tryPath.Contains(".js") ||
				tryPath.Contains(".swf") ||
				tryPath.Contains(".flv") ||
				tryPath.Contains(".axd") ||
				tryPath.Contains(".ashx") ||
				tryPath.Contains(".asmx") ||
				tryPath.Contains(".html") ||
				tryPath.Contains(".htm") ||
				tryPath.Contains(".pdf") ||
				tryPath.Contains(".png"))
			{
				// No rewriting to other content
				// Fix the url for the standard folders (images, css, js, swf);
			}
			else if (CheckIfPathIsExcluded(tryPath))
			{
				// No rewriting to excluded paths
			}
			else
			{
				string sPath;

				if (using404Method)
				{
					// Is only request path, not the domain, to www.website.com/virtualappfolder/testme.aspx will return : /virtualappfolder/testme.aspx
					string decodedRawUrl = app.Context.Server.UrlDecode(app.Context.Request.RawUrl);
					string requestUrl = app.Context.Request.Url.ToString();
					sPath = tryPath.Replace(requestUrl.Replace(decodedRawUrl, string.Empty), string.Empty);

					// Set the original sPath for retrieval during the request
					GenericRewriteModule.OriginalRequestPath = sPath;
					GenericRewriteModule.OriginalRawUrl = rawUrl404;
				}
				else
				{
					sPath = app.Context.Request.Path; // Is only request path, not the domain, to www.website.com/virtualappfolder/testme.aspx will return : /virtualappfolder/testme.aspx
					// Set the original sPath for retrieval during the request
					GenericRewriteModule.OriginalRequestPath = sPath;
					GenericRewriteModule.OriginalRawUrl = app.Context.Request.RawUrl;
				}

				sPath = sPath.ToLower();

				QueryStringHelper qs = new QueryStringHelper();
				bool rewriteRequired = false;

				Hitman.AddToText("Start rewriting, sPath = {0}, rewriteRequired = false", sPath);

				// Parse culture info
				if (GenericRewriteModule.UrlRewriteCultureInformation)
				{
					// Test if the URL contains a Culture (if so: remove it and set it as the current culture)           
					Regex regexCulture = GlobalizationHelper.CultureMatchRegex;
					if (regexCulture.IsMatch(sPath))
					{
						// Retrieve culture
						Match cultureMatch = regexCulture.Match(sPath); // Matches /nl-NL/ (with the slashes)
						string cultureString = cultureMatch.Value.Trim('/');

						// If cultureString is en-UK (incorrect culture), replace with en-GB (correct culture) and redirect
						if (cultureString.Equals("en-UK", StringComparison.OrdinalIgnoreCase))
						{
							WebShortcuts.RedirectPermanent(GenericRewriteModule.OriginalRawUrl.ReplaceFirstOccurrence(cultureString, "en-GB", StringComparison.OrdinalIgnoreCase));
						}

						// Set culture info
						System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture(cultureString);
						System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(cultureString);

						// Strip culture info from url & mark that we need rewriting
						sPath = sPath.Replace(cultureString + "/", string.Empty);
						rewriteRequired = true;

						// Mark that the Culture string was included in the request url
						GenericRewriteModule.IsCultureSpecifiedInUrl = true;

						Hitman.AddToText("Culture is specified in url, sPath = {0}", sPath);
					}
					else
					{
						// Rewrite to default culture
						string cultureUrl = StringUtil.CombineWithForwardSlash(HttpContext.Current.Request.ApplicationPath, GlobalizationHelper.DefaultCulture.ToLower());

						// Get sPath without ApplicationPath
						sPath = UrlUtil.RemoveApplicationPathIfStartsWithApplicationPath(sPath);

						if (sPath == String.Empty)
						{
							// Add trailing slash, so culture can be parsed out
							cultureUrl += "/";
						}
						else
						{
							// Add sPath, since it has a Url
							cultureUrl = StringUtil.CombineWithForwardSlash(cultureUrl, sPath);
						}

						// Add QueryString to end (but not when it is a ?404; querystring
						if (!using404Method && HttpContext.Current.Request.QueryString.Count > 0)
						{
							cultureUrl += "?" + HttpContext.Current.Request.QueryString.ToString();
						}

						WebShortcuts.RedirectPermanent(cultureUrl);

						// System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture(GlobalizationHelper.DefaultCulture);
						// System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(GlobalizationHelper.DefaultCulture);
					}
				}

				if (GenericRewriteModule.UrlRewritePageUrl)
				{
					// We need to strip all but the base url
					string applicationPath = app.Request.ApplicationPath;
					if (!applicationPath.EndsWith("/"))
						applicationPath += "/";
					applicationPath = applicationPath.ToLower();

					if (sPath.Contains(applicationPath))
					{
						string pageUrl;
						if (applicationPath.Length > 1) // 1 == ROOT = /
							pageUrl = sPath.Replace(applicationPath, string.Empty);
						else
							pageUrl = sPath;

						bool handlerFound = false;

						Hitman.AddToText("Url rewriting is on, pageUrl = ", pageUrl);

						// Use a PageUrlRewriter
						if (GenericRewriteModule.CustomPageUrlRewriter != null)
						{
							Hitman.AddToText("Using custom rewriter");

							string handlingPage;
							string rewrittenPageUrl;
							handlerFound = GenericRewriteModule.CustomPageUrlRewriter.Rewrite(pageUrl, out rewrittenPageUrl, out handlingPage);
							if (handlerFound)
							{
								sPath = string.Format("{0}{1}?PageUrl={2}", applicationPath, handlingPage, rewrittenPageUrl);
								HandlingPage = handlingPage;
								HandledByDefaultHandlingPage = false;
								handlerFound = true;
								rewriteRequired = true;

								Hitman.AddToText("Custom rewriter found handler, sPath = {0}", sPath);
							}
						}

						// Using multiple file handlers, /products/tools/super redirects to products.aspx?PageUrl=tools/super
						if (GenericRewriteModule.UrlRewriteByMultipleHandlingFiles)
						{
							Hitman.AddToText("Using multiple handingfiles");

							foreach (var file in aspxFilesWithoutExtensionInApplicationRoot)
							{
								Hitman.AddToText("HandlingFile: {0}", file);
							}

							// Parse the PageUrl to find the handling page
							string[] elements = pageUrl.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
							if (elements.Length > 0)
							{
								// Try to find if there's a handling page for the first element in the PageUrl							
								string fileName = Path.GetFileNameWithoutExtension(elements[0]).ToLower();
								if (StringArrayUtil.IndexOf(fileName, aspxFilesWithoutExtensionInApplicationRoot, true) >= 0)
								{
									string pageUrlParameters = "";
									for (int i = 1; i < elements.Length; i++)
									{
										string current = elements[i];
										pageUrlParameters = StringUtil.CombineWithForwardSlash(pageUrlParameters, current);
									}
									sPath = string.Format("{0}{1}.aspx?PageUrl={2}", applicationPath, fileName, pageUrlParameters);
									GenericRewriteModule.HandlingPage = fileName + ".aspx";
									// Check if it is the default rewrite handler or not
									if (GenericRewriteModule.HandlingPage.Equals(GenericRewriteModule.UrlRewriteHandlingFile, StringComparison.OrdinalIgnoreCase))
									{
										GenericRewriteModule.HandledByDefaultHandlingPage = true;
									}
									else
									{
										GenericRewriteModule.HandledByDefaultHandlingPage = false;
									}
									handlerFound = true;
									rewriteRequired = true;

									Hitman.AddToText("Handling file found, sPath = {0}", sPath);
								}
							}
						}

						if (!handlerFound)
						{
							Hitman.AddToText("No handler found");

							// If the pageUrl is the HandlingPage, set to string.Empty
							if (pageUrl.Length == 0 && GenericRewriteModule.UrlRewriteDefaultPage.Length > 0)
							{
								sPath = GenericRewriteModule.UrlRewriteDefaultPage; // +"?PageUrl=";
								rewriteRequired = true;

								Hitman.AddToText("Rewriting to UrlRewriteDefaultPage, sPath = {0}", sPath);
							}
							else
							{
								if (pageUrl.ToLower() == GenericRewriteModule.UrlRewriteHandlingFile.ToLower())
									pageUrl = string.Empty;

								// Now construct the rewrite url 
								sPath = string.Format("{0}{1}?PageUrl={2}", applicationPath, GenericRewriteModule.UrlRewriteHandlingFile, pageUrl);
								GenericRewriteModule.HandlingPage = GenericRewriteModule.UrlRewriteHandlingFile;
								GenericRewriteModule.HandledByDefaultHandlingPage = true;
								rewriteRequired = true;

								Hitman.AddToText("Rewriting to UrlRewriteHandlingFile, sPath = {0}", sPath);
							}
						}
					}
				}

				// Set 404 method
				GenericRewriteModule.Using404Method = using404Method;

				// Rewrite if needed
				if (rewriteRequired)
				{
					Hitman.AddToText("Rewrite required, sPath = {0}", sPath);

					// Hitman.Kill();

					app.Context.RewritePath(sPath);
					HttpContext.Current.Items.Add(GenericRewriteModule.HttpContextItemKeyIsUrlRewritten, true);
				}
			}
		}

		private void context_EndRequest(object o, EventArgs args)
		{
			// Get the time elapsed for the request
			TimeSpan elapsedTime = DateTime.Now - beginRequest;

			// Add header to the Response
			try
			{
				HttpContext.Current.Response.AppendHeader("X-ElapsedTime", elapsedTime.ToString());
			}
			catch
			{
				// Headers probably already sent, do not throw an exception
			}
		}

		#endregion
	}
}
