﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;

namespace Dionysos.Web.HttpModules
{
	/// <summary>
	/// Class containing functionality to act as a Response.Filter
	/// </summary>
	public class ResponseFilter : Stream
	{
		private Stream responseStream;
		private long position;
		private StringBuilder responseHtml;

		public ResponseFilter(Stream inputStream)
		{
			this.responseStream = inputStream;
			this.responseHtml = new StringBuilder();
		}

		#region Filter overrides

		public override bool CanRead
		{
			get { return true; }
		}

		public override bool CanSeek
		{
			get { return true; }
		}

		public override bool CanWrite
		{
			get { return true; }
		}

		public override void Close()
		{
			this.responseStream.Close();
		}

		public override void Flush()
		{
			this.responseStream.Flush();
		}

		public override long Length
		{
			get { return 0; }
		}

		public override long Position
		{
			get { return this.position; }
			set { this.position = value; }
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.responseStream.Seek(offset, origin);
		}

		public override void SetLength(long length)
		{
			this.responseStream.SetLength(length);
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			return this.responseStream.Read(buffer, offset, count);
		}
		#endregion

		#region Dirty work

		public override void Write(byte[] buffer, int offset, int count)
		{
			string strBuffer = System.Text.UTF8Encoding.UTF8.GetString(buffer, offset, count);

			// Wait for the closing </html> tag
			Regex eof = new Regex("</html>", RegexOptions.IgnoreCase);

			if (!eof.IsMatch(strBuffer))
			{
				this.responseHtml.Append(strBuffer);
			}
			else
			{
				this.responseHtml.Append(strBuffer);
				string hashCode = this.responseHtml.GetHashCode().ToString();

				// Transform if it was rewritten and the response and write it back out
				if (GenericRewriteModule.UrlRewritten)
				{
					// For each subfolder in the root folder we to a replace. This is to ensure relative urls will stay functioning
					string mappedApplicationPath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath);
					string[] subfolders;
					if (!CacheHelper.TryGetValue("ResponseFilterDirectories", false, out subfolders))
					{
						subfolders = Directory.GetDirectories(mappedApplicationPath);
						CacheHelper.AddAbsoluteExpire(false, "ResponseFilterDirectories", subfolders, 120);
					}

					for (int i = 0; i < subfolders.Length; i++)
					{
						string subFoldername = subfolders[i].Replace(mappedApplicationPath, string.Empty);
						if (subFoldername.StartsWith("\\"))
						{
							subFoldername = subFoldername.Substring(1);
						}
						this.responseHtml.Replace("\"" + subFoldername + "/", "\"" + VirtualPathUtility.ToAbsolute("~/" + subFoldername) + "/");
						this.responseHtml.Replace("url(" + subFoldername + "/", "url(" + VirtualPathUtility.ToAbsolute("~/" + subFoldername) + "/");
					}
				}

				// Write result to stream
				string finalHtml = this.responseHtml.ToString();
				byte[] data = System.Text.UTF8Encoding.UTF8.GetBytes(finalHtml);
				this.responseStream.Write(data, 0, data.Length);
			}
		}

		#endregion
	}
}