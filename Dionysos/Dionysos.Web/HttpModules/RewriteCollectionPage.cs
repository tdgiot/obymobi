﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections.Specialized;

namespace Dionysos.Web.HttpModules
{
    /// <summary>
    /// Adds a RewriteEntity property to the <see cref="Dionysos.Web.UI.PageDefault"/> class.
    /// </summary>
    /// <typeparam name="T">The class type of the RewriteEntity.</typeparam>
    public abstract class RewriteCollectionPage<T> : Dionysos.Web.UI.PageDefault
        where T : class
    {
        /// <summary>
        /// Gets the RewriteTemplateElements from the HttpContext.Current.Items.
        /// </summary>
        public NameValueCollection RewriteTemplateElements
        {
            get
            {
                return this.Context.Items["RewriteTemplateElements"] as NameValueCollection;
            }
        }

        /// <summary>
        /// Gets the PageUrl of the current RewriteTemplateElements from the HttpContext.Current.Items.
        /// </summary>
        public string RewritePageUrl
        {
            get
            {
                return this.Context.Items["RewritePageUrl"] as string;
            }
        }
    }
}
