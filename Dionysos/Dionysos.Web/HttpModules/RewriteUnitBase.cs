﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Web;

namespace Dionysos.Web.HttpModules
{
	/// <summary>
	/// Provides an implementation of a RewriteUnit with default behaviour.
	/// </summary>
	public abstract class RewriteUnitBase
	{
		#region Properties

		/// <summary>
		/// Gets the current context.
		/// </summary>
		/// <value>The current context.</value>
		protected HttpContext Context
		{
			get
			{
				return HttpContext.Current;
			}
		}

		/// <summary>
		/// Gets a value indicating whether a redirect from this rewrite unit can be overridden with a permanent redirect.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if redirect can be overridden; otherwise, <c>false</c>.
		/// </value>
		public virtual bool OverrideRedirect { get; private set; }

		/// <summary>
		/// Gets or sets a value indicating whether to skip overriding redirects when the domain is different (eg. the redirect is send directly to the client).
		/// </summary>
		/// <value>
		///   <c>true</c> if redirects to a different domain may not be overridden; otherwise, <c>false</c>.
		/// </value>
		public virtual bool SkipOverrideRedirectDifferentDomain { get; private set; }

		/// <summary>
		/// Gets a value indicating whether this rewrite unit is always used when resolving URLs (even when not hit while rewriting).
		/// </summary>
		/// <value>
		///   <c>true</c> if this rewrite unit is always used when resolving URLs; otherwise, <c>false</c>.
		/// </value>
		public virtual bool AlwaysResolveUrl { get; private set; }

		/// <summary>
		/// Gets a value indicating whether this rewrite unit can resolve an absolute URL.
		/// </summary>
		/// <value>
		///   <c>true</c> if this rewrite unit can resolve an absolute URL; otherwise, <c>false</c>.
		/// </value>
		public virtual bool CanResolveAbsoluteUrl { get; private set; }

		#endregion

		#region Methods

		/// <summary>
		/// Rewrites a request.
		/// </summary>
		/// <param name="rewrittenUrl">The processed Uri that is passed through each RewriteUnit.</param>
		/// <param name="originalUrl">The original Uri the request was made on.</param>
		/// <returns>The action the RewriteModule has to perform.</returns>
		public abstract RewriteAction Rewrite(ref UriBuilder rewrittenUrl, Uri originalUrl);

		/// <summary>
		/// Resolves a pageUrl so modifications in this IRewriteUnit can be resolved back. By default no modifications will be done.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <param name="uiCulture">The CultureInfo to use when resolving the pageUrl.</param>
		/// <returns>The resolved pageUrl.</returns>
		/// <example>
		/// If you grab the CultureInformation from the rewrittenUrl, the returned rewrittenUrl doesn't contain the CultureInformation.
		/// To add the CultureInformation back to the pageUrl, generation of pageUrls have to call this method and this method adds back the grabbed CultureInformation.
		/// </example>
		public virtual string ResolveUrl(string pageUrl, CultureInfo uiCulture)
		{
			return pageUrl;
		}

		#endregion
	}

	/// <summary>
	/// An action to perform after the RewriteUnit is completed.
	/// </summary>
	public enum RewriteAction
	{
		/// <summary>
		/// Continue rewriting, passes the rewrittenUrl to the next IRewriteUnit.
		/// </summary>
		Continue,
		/// <summary>
		/// Exit rewriting, cancels all previous rewrite actions.
		/// </summary>
		Exit,
		/// <summary>
		/// Exit rewriting and end response.
		/// </summary>
		End,
		/// <summary>
		/// Redirect to rewrittenUrl using an 302 Found.
		/// </summary>
		Redirect,
		/// <summary>
		/// Redirect to rewrittenUrl using an 301 Moved Permanently.
		/// </summary>
		RedirectPermanent,
		/// <summary>
		/// Rewrite to rewrittenUrl using RewritePath.
		/// </summary>
		Rewrite
	}
}
