﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Dionysos.Web.HttpModules.RewriteUnits;
using System.Collections.ObjectModel;

namespace Dionysos.Web.HttpModules
{
	/// <summary>
	/// Provides unit based rewriting for an application.
	/// </summary>
	public class RewriteModule : IHttpModule, IRequiresSessionState
	{
		#region Fields

		/// <summary>
		/// The locker object.
		/// </summary>
		private static object locker = new object();

		/// <summary>
		/// List of (comma seperated) paths excluded from rewriting.
		/// </summary>
		private static List<string> excludedPaths;

		/// <summary>
		/// List of IRewriteUnits used for rewriting.
		/// </summary>
		private static List<RewriteUnitBase> rewriteUnits;

		#endregion

		#region Properties

		/// <summary>
		/// Paths starting with an excluded path will be ignored.
		/// </summary>
		public static List<string> ExcludedPaths
		{
			get
			{
				// Double check locking
				if (excludedPaths == null)
				{
					lock (locker)
					{
						if (excludedPaths == null)
						{
							// Add database entries
							excludedPaths = ConfigurationManager.GetString(DionysosWebConfigurationConstants.ExcludedPathsInPathRewriting, true)
								.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

							// Fixed entries
							excludedPaths.Add("WebResource.axd");
							excludedPaths.Add("ScriptResource.axd");
							excludedPaths.Add("VSEnterpriseHelper.axd");
							excludedPaths.Add("css/css.axd");
							excludedPaths.Add("js/js.axd");
							excludedPaths.Add("DXR.axd");
							excludedPaths.Add("Media/Layout/");
						}
					}
				}

				return excludedPaths;
			}
			set
			{
				excludedPaths = value;
			}
		}

		/// <summary>
		/// Stores all IRewriteUnit instances used for rewriting.
		/// </summary>
		public static List<RewriteUnitBase> RewriteUnits
		{
			get
			{
				// Double check locking
				if (rewriteUnits == null)
				{
					lock (locker)
					{
						if (rewriteUnits == null)
						{
							rewriteUnits = new List<RewriteUnitBase>()
							{
								// First redirect preferred domain and application path trailing slash
								new PreferredDomainRewriteUnit(),
								new ApplicationPathRewriteUnit(),
								// Filter existing files
								new FileFilterRewriteUnit(),
								// Add legacy rewrite support
								new Legacy404MethodRewriteUnit(),
								// Rewrite culture info from path
								new CultureInfoPathRewriteUnit()
							};
						}
					}
				}

				return rewriteUnits;
			}
			set
			{
				rewriteUnits = value;
			}
		}

		/// <summary>
		/// Gets the last rewrite unit that has been used in this request.
		/// </summary>
		public static RewriteUnitBase LastRewriteUnit
		{
			get
			{
				RewriteUnitBase rewriteUnit = null;

				int index = LastRewriteUnitIndex;
				if (index >= 0)
				{
					rewriteUnit = RewriteUnits[index];
				}

				return rewriteUnit;
			}
		}

		/// <summary>
		/// Stores the last RewriteUnit index that has been used in this request.
		/// </summary>
		/// <value>
		/// The last index of the rewrite unit.
		/// </value>
		public static int LastRewriteUnitIndex
		{
			get
			{
				int? lastRewriteUnit = HttpContext.Current.Items["LastRewriteUnitIndex"] as int?;
				if (lastRewriteUnit.HasValue)
				{
					return lastRewriteUnit.Value;
				}
				else
				{
					return -1;
				}
			}
			private set
			{
				HttpContext.Current.Items["LastRewriteUnitIndex"] = value;
			}
		}

		#endregion

		#region IHttpModule Members

		/// <summary>
		/// Initializes a module and prepares it to handle requests.
		/// </summary>
		/// <param name="context">An System.Web.HttpApplication that provides access to the methods, properties, and events common to all application objects within an ASP.NET application.</param>
		public void Init(System.Web.HttpApplication context)
		{
			context.BeginRequest += new EventHandler(BeginRequest);
		}

		/// <summary>
		/// Disposes of the resources (other than memory) used by the module that implements System.Web.IHttpModule.
		/// </summary>
		public void Dispose()
		{ }

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles incomming requests to the ASP.NET application.
		/// </summary>
		/// <param name="sender">The System.Web.HttpApplication the incomming request came from.</param>
		/// <param name="e">The EventArgs passed to the event.</param>
		private void BeginRequest(object sender, EventArgs e)
		{
			// Set local variables
			HttpContext context = ((System.Web.HttpApplication)sender).Context;
			Uri originalUrl = context.Request.Url;
			UriBuilder rewrittenUrl = new UriBuilder(context.Request.Url);

			// Check for excluded paths
			ReadOnlyCollection<string> excludedPaths = RewriteModule.ExcludedPaths.AsReadOnly();
			if (excludedPaths.Count > 0)
			{
				UriBuilder removedApplicationPath = rewrittenUrl.RemoveApplicationPath();
				foreach (string excludedPath in excludedPaths)
				{
					if (removedApplicationPath.Path.TrimStart('/').StartsWith(excludedPath, StringComparison.OrdinalIgnoreCase))
					{
						// Cancel request processing
						return;
					}
				}
			}

			// Check all IRewriteUnits
			RewriteAction rewriteAction = RewriteAction.Continue;
			RewriteAction? redirectAction = null;
			Uri redirectUrl = null;
			ReadOnlyCollection<RewriteUnitBase> rewriteUnits = RewriteModule.RewriteUnits.AsReadOnly();
			for (int i = 0, loops = 0; i < rewriteUnits.Count; i++)
			{
				// Prevent looping
				if (loops > rewriteUnits.Count)
				{
					// End rewriting when loop count is bigger than rewriting units
					ErrorLoggerWeb.LogError("RewriteModule detected infinite loop, request ended.");
					rewriteAction = RewriteAction.End;
					break;
				}

				// Get rewrite unit
				RewriteUnitBase rewriteUnit = rewriteUnits[i];

				// Rewrite request using the rewriter
				rewriteAction = rewriteUnit.Rewrite(ref rewrittenUrl, originalUrl);

				// Save the index of the last RewriteUnit
				RewriteModule.LastRewriteUnitIndex = i;

				// Combine redirects
				if (rewrittenUrl != null &&
					!rewrittenUrl.Uri.Equals(redirectUrl) &&
					(rewriteAction == RewriteAction.Redirect || rewriteAction == RewriteAction.RedirectPermanent))
				{
					// Combine redirects
					if (!redirectAction.HasValue ||
						rewriteUnit.OverrideRedirect ||
						rewriteAction == RewriteAction.Redirect)
					{
						// First redirect, using override or temporary redirect
						redirectAction = rewriteAction;
					}
					redirectUrl = rewrittenUrl.Uri;

					// Redirect URL must be absolute
					if (!redirectUrl.IsAbsoluteUri)
					{
						throw new InvalidOperationException(String.Format("The redirect URL is not absolute: {0}.", redirectUrl.ToString()));
					}

					// Continue processing from the beginning (if still within the same domain)
					if (redirectUrl.Host == originalUrl.Host ||
						!rewriteUnit.SkipOverrideRedirectDifferentDomain)
					{
						RewriteModule.LastRewriteUnitIndex = 0;
						i = 0;
						loops++;
						continue;
					}
				}

				// Exit rewriting if returned uri is null, action is not continue or redirect action is set
				if (rewrittenUrl == null ||
					rewriteAction != RewriteAction.Continue ||
					(redirectAction.HasValue && rewriteAction != RewriteAction.Continue))
				{
					break;
				}
			}

			// Use redirect action if set
			if (redirectAction.HasValue && redirectUrl != null)
			{
				rewriteAction = redirectAction.Value;
				rewrittenUrl = new UriBuilder(redirectUrl);
			}

			// Rewrite the request
			switch (rewriteAction)
			{
				case RewriteAction.Continue:
					WebShortcuts.RedirectTo404Page();
					break;
				case RewriteAction.End:
					context.Response.End();
					break;
				case RewriteAction.Redirect:
					WebShortcuts.Redirect(rewrittenUrl.Uri.ToString());
					break;
				case RewriteAction.RedirectPermanent:
					WebShortcuts.RedirectPermanent(rewrittenUrl.Uri.ToString());
					break;
				case RewriteAction.Rewrite:
					context.RewritePath(rewrittenUrl.Uri.PathAndQuery);
					break;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Resolves the specified pageUrl in the CurrentUICulture using all RewriteUnits run in the current request.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <returns>A resolved, absolute pageUrl.</returns>
		public static string ResolveUrl(string pageUrl)
		{
			return ResolveUrl(pageUrl, CultureInfo.CurrentUICulture);
		}

		/// <summary>
		/// Resolves the specified pageUrl in the specified culture using all RewriteUnits run in the current request.
		/// </summary>
		/// <param name="pageUrl">The pageUrl to resolve.</param>
		/// <param name="uiCulture">The CultureInfo to use when resolving the pageUrl.</param>
		/// <returns>A resolved, absolute pageUrl.</returns>
		public static string ResolveUrl(string pageUrl, CultureInfo uiCulture)
		{
			if (pageUrl != null)
			{
				// Go through all used rewrite units
				ReadOnlyCollection<RewriteUnitBase> rewriteUnits = RewriteModule.RewriteUnits.AsReadOnly();
				for (int i = rewriteUnits.Count - 1, lastRewriteUnitIndex = RewriteModule.LastRewriteUnitIndex; i >= 0; i--)
				{
					// Get rewrite unit
					RewriteUnitBase rewriteUnit = rewriteUnits[i];

					// Skip if not hit and does not always have to resolve
					if (i > lastRewriteUnitIndex && !rewriteUnit.AlwaysResolveUrl) continue;

					// Only rewrite relative urls if unit does not specify if it can handle absolute urls
					if (rewriteUnit.CanResolveAbsoluteUrl || !Uri.IsWellFormedUriString(pageUrl, UriKind.Absolute))
					{
						pageUrl = rewriteUnit.ResolveUrl(pageUrl, uiCulture);
					}
				}

				// If not absolute, return relative to application path
				if (!Uri.IsWellFormedUriString(pageUrl, UriKind.Absolute))
				{
					// Remove slashes
					pageUrl = pageUrl.TrimEnd('/');

					// Return a relative url (with application path)
					Uri absoluteUri = new Uri(new Uri("http://localhost/"), pageUrl).AddApplicationPath();
					pageUrl = absoluteUri.PathAndQuery + absoluteUri.Fragment;
				}
			}

			return pageUrl;
		}

		#endregion
	}

	/// <summary>
	/// Extensions for the RewriteModule.RewriteUnits dictionary.
	/// </summary>
	public static class RewriteUnitsExtensions
	{
		/// <summary>
		/// Inserts the item before the first item of the specified type. If no item of the specified type is found, inserts the item in front of all items.
		/// </summary>
		/// <param name="rewriteUnits">The RewriteUnits Dictionary.</param>
		/// <param name="type">The type to insert the item in front of.</param>
		/// <param name="item">The item to insert.</param>
		public static void InsertBefore(this List<RewriteUnitBase> rewriteUnits, Type type, RewriteUnitBase item)
		{
			lock (rewriteUnits)
			{
				int index = 0;
				RewriteUnitBase firstRewriteUnitBase = rewriteUnits.FirstOrDefault(ru => ru.GetType() == type);
				if (firstRewriteUnitBase != null)
				{
					index = rewriteUnits.IndexOf(firstRewriteUnitBase);
				}

				rewriteUnits.Insert(index, item);
			}
		}

		/// <summary>
		/// Inserts the item after the first item of the specified type. If no item of the specified type is found, inserts the item after all items.
		/// </summary>
		/// <param name="rewriteUnits">The RewriteUnits Dictionary.</param>
		/// <param name="type">The type to insert the item after.</param>
		/// <param name="item">The item to insert.</param>
		public static void InsertAfter(this List<RewriteUnitBase> rewriteUnits, Type type, RewriteUnitBase item)
		{
			lock (rewriteUnits)
			{
				int index = rewriteUnits.Count;
				RewriteUnitBase firstRewriteUnitBase = rewriteUnits.FirstOrDefault(ru => ru.GetType() == type);
				if (firstRewriteUnitBase != null)
				{
					index = rewriteUnits.IndexOf(firstRewriteUnitBase) + 1;
				}

				rewriteUnits.Insert(index, item);
			}
		}

		/// <summary>
		/// Replaces the first item of the specified type with the item.
		/// </summary>
		/// <param name="rewriteUnits">The RewriteUnits Dictionary.</param>
		/// <param name="type">The type to replace.</param>
		/// <param name="item">The item to replace with.</param>
		/// <returns>True if a item of the specified type is found; otherwise returns false (and does not add the item).</returns>
		public static bool Replace(this List<RewriteUnitBase> rewriteUnits, Type type, RewriteUnitBase item)
		{
			bool replaced = false;

			lock (rewriteUnits)
			{
				RewriteUnitBase firstRewriteUnitBase = rewriteUnits.FirstOrDefault(ru => ru.GetType() == type);
				if (firstRewriteUnitBase != null)
				{
					int index = rewriteUnits.IndexOf(firstRewriteUnitBase);
					rewriteUnits[index] = item;

					replaced = true;
				}
			}

			return replaced;
		}

		/// <summary>
		/// Removes all items that are of the specified type.
		/// </summary>
		/// <param name="rewriteUnits">The RewriteUnits Dictionary.</param>
		/// <param name="type">The type to remove.</param>
		/// <returns>The number of elements removed from the RewriteUnits Dictionary.</returns>
		public static int RemoveAll(this List<RewriteUnitBase> rewriteUnits, Type type)
		{
			return rewriteUnits.RemoveAll(ru => ru.GetType() == type);
		}

		/// <summary>
		/// Determines whether the rewrite units contains the specified type.
		/// </summary>
		/// <param name="rewriteUnits">The rewrite units.</param>
		/// <param name="type">The type.</param>
		/// <returns>
		///   <c>true</c> if the rewrite units contains the specified type; otherwise, <c>false</c>.
		/// </returns>
		public static bool Contains(this List<RewriteUnitBase> rewriteUnits, Type type)
		{
			return rewriteUnits.Any(ru => ru.GetType() == type);
		}
	}
}
