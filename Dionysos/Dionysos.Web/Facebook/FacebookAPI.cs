/*
 * Copyright 2010 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using System.Globalization;
using Dionysos.Net;

namespace Dionysos.Web.Facebook
{
    /// <summary>
    /// The HTTP verb.
    /// </summary>
    public enum HttpVerb
    {
        /// <summary>
        /// GET verb.
        /// </summary>
        GET,
        /// <summary>
        /// POST verb.
        /// </summary>
        POST,
        /// <summary>
        /// DELETE verb.
        /// </summary>
        DELETE
    }

    /// <summary>
    /// Wrapper around the Facebook Graph API.
    /// </summary>
    public class FacebookAPI
    {
        #region Properties

        /// <summary>
        /// The access token used to authenticate API calls.
        /// </summary>
        /// <value>
        /// The access token.
        /// </value>
        public string AccessToken { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookAPI"/> class with public access only.
        /// </summary>
        public FacebookAPI()
            : this(null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookAPI"/> class using the specified token to authenticate.
        /// </summary>
        /// <param name="token">The access token used for authentication.</param>
        public FacebookAPI(string token)
        {
            this.AccessToken = token;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <param name="clientId">The client id (App ID).</param>
        /// <param name="clientSecret">The client secret (App Secret).</param>
        /// <param name="code">The code returned from the OAuth dialog.</param>
        /// <param name="redirectUri">The exact same redirect URI used when redirecting to the OAuth dialog.</param>
        /// <returns>
        /// The access token.
        /// </returns>
        public static string GetAccessToken(string clientId, string clientSecret, string code, string redirectUri)
        {
            TimeSpan expires;
            return FacebookAPI.GetAccessToken(clientId, clientSecret, code, redirectUri, out expires);
        }

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <param name="clientId">The client id (App ID).</param>
        /// <param name="clientSecret">The client secret (App Secret).</param>
        /// <param name="code">The code returned from the OAuth dialog.</param>
        /// <param name="redirectUri">The exact same redirect URI used when redirecting to the OAuth dialog.</param>
        /// <param name="expires">The duration before the access token expires (zero if infinite).</param>
        /// <returns>
        /// The access token.
        /// </returns>
        public static string GetAccessToken(string clientId, string clientSecret, string code, string redirectUri, out TimeSpan expires)
        {
            string response = FacebookAPI.MakeRequest(new Uri("https://graph.facebook.com/oauth/access_token"), HttpVerb.GET, new Dictionary<string, string>()
			{
				{ "client_id", clientId },
				{ "client_secret", clientSecret },
				{ "code", code },
				{ "redirect_uri", redirectUri }
			});

            // Parse response
            NameValueCollection responseParameters = HttpUtility.ParseQueryString(response);

            // Get access token expire time
            int expiresParameter;
            if (!String.IsNullOrEmpty(responseParameters["expires"]) &&
                Int32.TryParse(responseParameters["expires"], NumberStyles.Integer, CultureInfo.InvariantCulture, out expiresParameter))
            {
                expires = TimeSpan.FromSeconds(expiresParameter);
            }
            else
            {
                expires = TimeSpan.Zero;
            }

            return responseParameters["access_token"];
        }

        /// <summary>
        /// Makes a Facebook Graph API GET request.
        /// </summary>
        /// <param name="relativePath">The path for the call, e.g. /username.</param>
        /// <returns>
        /// The response as a JSON object.
        /// </returns>
        public JSONObject Get(string relativePath)
        {
            return this.Call(relativePath, HttpVerb.GET, null);
        }

        /// <summary>
        /// Makes a Facebook Graph API GET request.
        /// </summary>
        /// <param name="relativePath">The path for the call, e.g. /username.</param>
        /// <param name="args">A dictionary of key/value pairs that will get passed as query arguments.</param>
        /// <returns>
        /// The response as a JSON object.
        /// </returns>
        public JSONObject Get(string relativePath, Dictionary<string, string> args)
        {
            return this.Call(relativePath, HttpVerb.GET, args);
        }

        /// <summary>
        /// Makes a Facebook Graph API DELETE request.
        /// </summary>
        /// <param name="relativePath">The path for the call, e.g. /username.</param>
        /// <returns>
        /// The response as a JSON object.
        /// </returns>
        public JSONObject Delete(string relativePath)
        {
            return this.Call(relativePath, HttpVerb.DELETE, null);
        }

        /// <summary>
        /// Makes a Facebook Graph API POST request.
        /// </summary>
        /// <param name="relativePath">The path for the call, e.g. /username.</param>
        /// <param name="args">A dictionary of key/value pairs that will get passed as query arguments. These determine what will get set in the graph API.</param>
        /// <returns>
        /// The response as a JSON object.
        /// </returns>
        public JSONObject Post(string relativePath, Dictionary<string, string> args)
        {
            return this.Call(relativePath, HttpVerb.POST, args);
        }

        /// <summary>
        /// Makes a Facebook Graph API POST request.
        /// </summary>
        /// <param name="relativePath">The path for the call, e.g. /username.</param>
        /// <param name="args">A dictionary of key/value pairs that will get passed as query arguments. These determine what will get set in the graph API.</param>
        /// <param name="files">The files.</param>
        /// <returns>
        /// The response as a JSON object.
        /// </returns>
        public JSONObject Post(string relativePath, Dictionary<string, string> args, IDictionary<string, HttpFile> files)
        {
            return this.Call(relativePath, HttpVerb.POST, args, files);
        }

        /// <summary>
        /// Makes a Facebook Graph API Call.
        /// </summary>
        /// <param name="relativePath">The path for the call, e.g. /username.</param>
        /// <param name="httpVerb">The HTTP verb to use, e.g. GET, POST, DELETE.</param>
        /// <param name="args">A dictionary of key/value pairs that will get passed as query arguments.</param>
        /// <returns>
        /// The response as a JSON object.
        /// </returns>
        private JSONObject Call(string relativePath, HttpVerb httpVerb, Dictionary<string, string> args)
        {
            return this.Call(relativePath, httpVerb, args, null);
        }

        /// <summary>
        /// Makes a Facebook Graph API Call.
        /// </summary>
        /// <param name="relativePath">The path for the call, e.g. /username.</param>
        /// <param name="httpVerb">The HTTP verb to use, e.g. GET, POST, DELETE.</param>
        /// <param name="args">A dictionary of key/value pairs that will get passed as query arguments.</param>
        /// <param name="files">The files.</param>
        /// <returns>
        /// The response as a JSON object.
        /// </returns>
        private JSONObject Call(string relativePath, HttpVerb httpVerb, Dictionary<string, string> args, IDictionary<string, HttpFile> files)
        {
            Uri baseUrl = new Uri("https://graph.facebook.com");
            Uri url = new Uri(baseUrl, relativePath);
            if (args == null) args = new Dictionary<string, string>();
            if (!String.IsNullOrEmpty(this.AccessToken)) args["access_token"] = this.AccessToken;

            string response = FacebookAPI.MakeRequest(url, httpVerb, args, files);
            JSONObject obj = JSONObject.CreateFromString(response);
            if (obj.IsDictionary &&
                obj.Dictionary.ContainsKey("error"))
            {
                throw new FacebookAPIException(obj.Dictionary["error"].Dictionary["type"].String, obj.Dictionary["error"].Dictionary["message"].String);
            }

            return obj;
        }

        /// <summary>
        /// Make an HTTP request, with the given query arguments.
        /// </summary>
        /// <param name="url">The URL of the request.</param>
        /// <param name="httpVerb">The HTTP verb to use, e.g. GET, POST, DELETE.</param>
        /// <param name="args">A dictionary of key/value pairs that will get passed as query arguments.</param>
        /// <returns>
        /// The response of the request.
        /// </returns>
        public static string MakeRequest(Uri url, HttpVerb httpVerb, Dictionary<string, string> args)
        {
            return FacebookAPI.MakeRequest(url, httpVerb, args, null);
        }

        /// <summary>
        /// Make an HTTP request, with the given query arguments.
        /// </summary>
        /// <param name="url">The URL of the request.</param>
        /// <param name="httpVerb">The HTTP verb to use, e.g. GET, POST, DELETE.</param>
        /// <param name="args">A dictionary of key/value pairs that will get passed as query arguments.</param>
        /// <param name="files">The files.</param>
        /// <returns>
        /// The response of the request.
        /// </returns>
        public static string MakeRequest(Uri url, HttpVerb httpVerb, Dictionary<string, string> args, IDictionary<string, HttpFile> files)
        {
            if (httpVerb != HttpVerb.POST && files != null && files.Count > 0) throw new ArgumentException("The HTTP verb must be set to POST when uploading files.", "httpVerb");

            if (args != null &&
                args.Keys.Count > 0 &&
                httpVerb == HttpVerb.GET)
            {
                // Add GET parameters
                url = new Uri(url.ToString() + FacebookAPI.EncodeDictionary(args, true));
            }

            // Create request
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Proxy = null;
            request.Method = httpVerb.ToString();

            if (httpVerb == HttpVerb.POST)
            {
                if (files == null || files.Count == 0)
                {
                    // Add POST data
                    string postData = FacebookAPI.EncodeDictionary(args, false);

                    ASCIIEncoding encoding = new ASCIIEncoding();
                    byte[] postDataBytes = encoding.GetBytes(postData);

                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = postDataBytes.Length;

                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(postDataBytes, 0, postDataBytes.Length);
                    requestStream.Close();
                }
                else
                {
                    // Add multipart POST data
                    NameValueCollection form = new NameValueCollection();
                    foreach (KeyValuePair<string, string> arg in args)
                    {
                        form.Add(arg.Key, arg.Value);
                    }

                    request.PostMultipartFormData(form, files);
                }
            }

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError &&
                    ex.Response != null)
                {
                    using (StreamReader reader = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        string response = reader.ReadToEnd();
                        JSONObject obj = JSONObject.CreateFromString(response);
                        if (obj.IsDictionary &&
                            obj.Dictionary.ContainsKey("error"))
                        {
                            throw new FacebookAPIException(obj.Dictionary["error"].Dictionary["type"].String, obj.Dictionary["error"].Dictionary["message"].String, ex);
                        }
                    }
                }

                throw new FacebookAPIException("Server Error", ex.Message, ex);
            }
        }

        /// <summary>
        /// Encode a dictionary of key/value pairs as an HTTP query string.
        /// </summary>
        /// <param name="dict">The dictionary to encode.</param>
        /// <param name="questionMark">Whether or not to start it with a question mark (for GET requests).</param>
        /// <returns>
        /// The encoded dictionary.
        /// </returns>
        private static string EncodeDictionary(Dictionary<string, string> dict, bool questionMark)
        {
            StringBuilder sb = new StringBuilder();

            if (dict.Count > 0)
            {
                if (questionMark) sb.Append("?");

                bool isFirst = true;
                foreach (KeyValuePair<string, string> kvp in dict)
                {
                    if (!isFirst)
                    {
                        sb.Append("&");
                    }
                    isFirst = false;

                    sb.Append(HttpUtility.UrlEncode(kvp.Key));
                    sb.Append("=");
                    sb.Append(HttpUtility.UrlEncode(kvp.Value));
                }
            }

            return sb.ToString();
        }

        #endregion
    }
}
