/*
 * Copyright 2010 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Script.Serialization;

namespace Dionysos.Web.Facebook
{
	/// <summary>
	/// Represents an object encoded in JSON. Can be either a dictionary mapping strings to other objects, an array of objects, or a single object, which represents a scalar.
	/// </summary>
	public class JSONObject
	{
		#region Fields

		/// <summary>
		/// The string data.
		/// </summary>
		private string stringData;

		/// <summary>
		/// The array data.
		/// </summary>
		private JSONObject[] arrayData;

		/// <summary>
		/// The dictionary data.
		/// </summary>
		private Dictionary<string, JSONObject> dictionaryData;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating whether this instance is a dictionary.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is a dictionary; otherwise, <c>false</c>.
		/// </value>
		public bool IsDictionary
		{
			get
			{
				return this.dictionaryData != null;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is an array.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is an array; otherwise, <c>false</c>.
		/// </value>
		public bool IsArray
		{
			get
			{
				return this.arrayData != null;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is a string.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is a string; otherwise, <c>false</c>.
		/// </value>
		public bool IsString
		{
			get
			{
				return this.stringData != null;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is an integer.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is an integer; otherwise, <c>false</c>.
		/// </value>
		public bool IsInteger
		{
			get
			{
				Int64 tmp;
				return Int64.TryParse(this.stringData, out tmp);
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is a boolean.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is a boolean; otherwise, <c>false</c>.
		/// </value>
		public bool IsBoolean
		{
			get
			{
				bool tmp;
				return Boolean.TryParse(this.stringData, out tmp);
			}
		}

		/// <summary>
		/// Gets the dictionary.
		/// </summary>
		public Dictionary<string, JSONObject> Dictionary
		{
			get
			{
				return this.dictionaryData;
			}
		}

		/// <summary>
		/// Gets the array.
		/// </summary>
		public JSONObject[] Array
		{
			get
			{
				return this.arrayData;
			}
		}

		/// <summary>
		/// Gets the string.
		/// </summary>
		public string String
		{
			get
			{
				return this.stringData;
			}
		}

		/// <summary>
		/// Gets the integer.
		/// </summary>
		public long Integer
		{
			get
			{
				return Convert.ToInt64(this.stringData);
			}
		}

		/// <summary>
		/// Gets the boolean.
		/// </summary>
		public bool Boolean
		{
			get
			{
				return Convert.ToBoolean(this.stringData);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Prevents a default instance of the <see cref="JSONObject"/> class from being created.
		/// </summary>
		private JSONObject()
		{ }

		#endregion

		#region Methods

		/// <summary>
		/// Creates a JSONObject by parsing a string. This is the only correct way to create a JSONObject.
		/// </summary>
		/// <param name="s">The string.</param>
		/// <returns>
		/// The JSONObject instance.
		/// </returns>
		public static JSONObject CreateFromString(string s)
		{
			object obj;
			JavaScriptSerializer js = new JavaScriptSerializer();

			try
			{
				obj = js.DeserializeObject(s);
			}
			catch (ArgumentException ex)
			{
				throw new FacebookAPIException("JSONException", "Not a valid JSON string.", ex);
			}

			return JSONObject.Create(obj);
		}

		/// <summary>
		/// Prints the JSONObject as a formatted string, suitable for viewing.
		/// </summary>
		/// <returns>
		/// The displayable string.
		/// </returns>
		public string ToDisplayableString()
		{
			StringBuilder sb = new StringBuilder();
			JSONObject.RecursiveObjectToString(this, sb, 0);

			return sb.ToString();
		}

		/// <summary>
		/// Recursively constructs a JSONObject.
		/// </summary>
		/// <param name="obj">The object.</param>
		/// <returns>
		/// The JSONObject instance.
		/// </returns>
		private static JSONObject Create(object obj)
		{
			JSONObject jsonObject = new JSONObject();
			if (obj is object[])
			{
				object[] objArray = obj as object[];
				jsonObject.arrayData = new JSONObject[objArray.Length];
				for (int i = 0; i < jsonObject.arrayData.Length; ++i)
				{
					jsonObject.arrayData[i] = JSONObject.Create(objArray[i]);
				}
			}
			else if (obj is Dictionary<string, object>)
			{
				jsonObject.dictionaryData = new Dictionary<string, JSONObject>();
				Dictionary<string, object> dict = obj as Dictionary<string, object>;
				foreach (string key in dict.Keys)
				{
					jsonObject.dictionaryData[key] = JSONObject.Create(dict[key]);
				}
			}
			else if (obj != null)
			{
				jsonObject.stringData = obj.ToString();
			}

			return jsonObject;
		}

		/// <summary>
		/// Recursives the object to string.
		/// </summary>
		/// <param name="jsonObject">The JSONObject instacen.</param>
		/// <param name="sb">The string builder.</param>
		/// <param name="level">The level.</param>
		private static void RecursiveObjectToString(JSONObject jsonObject, StringBuilder sb, int level)
		{
			if (jsonObject.IsDictionary)
			{
				sb.AppendLine();
				JSONObject.RecursiveDictionaryToString(jsonObject, sb, level + 1);
			}
			else if (jsonObject.IsArray)
			{
				foreach (JSONObject o in jsonObject.Array)
				{
					JSONObject.RecursiveObjectToString(o, sb, level);
					sb.AppendLine();
				}
			}
			else
			{
				sb.Append(jsonObject.String);
			}
		}

		/// <summary>
		/// Recursives the dictionary to string.
		/// </summary>
		/// <param name="jsonObject">The JSONObject instance.</param>
		/// <param name="sb">The string builder.</param>
		/// <param name="level">The level.</param>
		private static void RecursiveDictionaryToString(JSONObject jsonObject, StringBuilder sb, int level)
		{
			foreach (KeyValuePair<string, JSONObject> kvp in jsonObject.Dictionary)
			{
				sb.Append(' ', level * 2);
				sb.Append(kvp.Key);
				sb.Append(" => ");
				JSONObject.RecursiveObjectToString(kvp.Value, sb, level);
				sb.AppendLine();
			}
		}

		#endregion
	}
}
