/*
 * Copyright 2010 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Dionysos.Web.Facebook
{
	/// <summary>
	/// The exception that is thrown when an error occurs while accessing the Facebook API.
	/// </summary>
	public class FacebookAPIException : Exception
	{
		#region Fields

		/// <summary>
		/// The type.
		/// </summary>
		private readonly string type;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the type.
		/// </summary>
		public string Type
		{
			get
			{
				return this.type;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FacebookAPIException"/> class.
		/// </summary>
		public FacebookAPIException()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FacebookAPIException"/> class.
		/// </summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0).</exception>
		protected FacebookAPIException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			if (info == null) throw new ArgumentNullException("info");

			this.type = info.GetString("type");
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FacebookAPIException"/> class with a specified type and error message.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="message">The message that describes the error.</param>
		public FacebookAPIException(string type, string message)
			: this(type, message, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="FacebookAPIException"/> class with a specified type, error message and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="message">The message that describes the error.</param>
		/// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
		public FacebookAPIException(string type, string message, Exception innerException)
			: base(message, innerException)
		{
			this.type = type;
		}

		#endregion

		#region Methods

		/// <summary>
		/// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with information about the exception.
		/// </summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is a null reference (Nothing in Visual Basic).</exception>
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null) throw new ArgumentNullException("info");

			info.AddValue("type", this.type);

			base.GetObjectData(info, context);
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			if (!String.IsNullOrEmpty(this.type))
			{
				sb.AppendLine("Type: " + this.type);
			}

			sb.Append(base.ToString());

			return sb.ToString();
		}

		#endregion
	}
}
