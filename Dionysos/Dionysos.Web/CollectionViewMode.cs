﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web
{
    /// <summary>
    /// Enumerator class for settings the view mode for data collections
    /// </summary>
    public enum CollectionViewMode : int
    {
        /// <summary>
        /// Data collection is being displayed in a list > which is presented in a GridView
        /// </summary>
        List = 1,

        /// <summary>
        /// Data collection is being displayed in tiles > which is presented in ListView
        /// </summary>
        Tiles = 2,

        /// <summary>
        /// Data collection is being displayed in details > which is presented in a Repeater
        /// </summary>
        Details = 3
    }
}
