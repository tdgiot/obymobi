using System;

namespace Dionysos.Web
{
    /// <summary>
    /// Enumerator used for setting the writemode of a page
    /// </summary>
    public enum WriteMode
    {
        /// <summary>
        /// View means that no values can be edited on a page.
        /// Controls on a page are read-only.
        /// </summary>
        View,
        /// <summary>
        /// Edit means that the values can be edited on a page.
        /// Controls on a page are enabled.
        /// </summary>
        Edit
    }
}
