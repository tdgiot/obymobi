﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;

namespace Dionysos.Web
{
    public class SessionTracker
    {
        #region Fields

        private HttpContext context = null;
        private DateTime expires = DateTime.MinValue;

        private string visitCount = string.Empty;

        private string userHostAddress = string.Empty;
        private string userAgent = string.Empty;

        private string originalReferrer = string.Empty;
        private string originalUrl = string.Empty;

        private string sessionReferrer = string.Empty;
        private string sessionUrl = string.Empty;

        private HttpBrowserCapabilities browser = null;

        private List<string> pages = null;

        #endregion

        #region Constructors

        public SessionTracker()
        {
            //HttpContext.Current allows us to gain access to all 
            // the intrinsic ASP context objects like Request, Response, Session, etc
            this.context = HttpContext.Current;

            // provides a default expiration for cookies
            this.expires = DateTime.Now.AddYears(1);

            // load up the tracker
            incrementVisitCount();

            this.userHostAddress = this.context.Request.UserHostAddress.ToString();
            this.userAgent = this.context.Request.UserAgent.ToString();

            if (this.context.Request.UrlReferrer != null)
            {
                // set original referrer if not set
                this.setOriginalReferrer(this.context.Request.UrlReferrer.ToString());
                this.sessionReferrer = this.context.Request.UrlReferrer.ToString();
            }

            if (this.context.Request.Url != null)
            {
                // set original url if not set
                this.setOriginalURL(this.context.Request.Url.ToString());
                this.sessionUrl = this.context.Request.Url.ToString();
            }

            // set the browser capabilities
            this.browser = this.context.Request.Browser;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Increment the visit count and save in a cookie
        /// </summary>
        public void incrementVisitCount()
        {
            string key = "VisitCount";

            // check is cookie has been set yet
            if (this.context.Request.Cookies.Get(key) == null)
                this.visitCount = "1";
            else
                this.visitCount = this.context.Request.Cookies.Get(key).Value + 1;

            // set or reset the cookie
            addCookie(key, this.visitCount);

            // Initialize the list
            this.pages = new List<string>();
        }

        /// <summary>
        /// Set the original referrer to a cookie
        /// </summary>
        /// <param name="val"></param>
        public void setOriginalReferrer(string val)
        {
            string key = "OriginalReferrer";

            // check is cookie has been set yet
            if (this.context.Request.Cookies.Get(key) != null)
                this.originalReferrer = this.context.Request.Cookies.Get(key).Value;
            else
            {
                addCookie(key, val);
                this.originalReferrer = val;
            }
        }


        /// <summary>
        /// Set the original url to a cookie
        /// </summary>
        /// <param name="val"></param>
        public void setOriginalURL(string val)
        {
            string key = "OriginalURL";

            // check is cookie has been set yet
            if (this.context.Request.Cookies.Get(key) != null)
                this.originalUrl = this.context.Request.Cookies.Get(key).Value;
            else
            {
                this.addCookie(key, val);
                this.originalUrl = val;
            }
        }

        /// <summary>
        /// Add the page to an arraylist in the session
        /// </summary>
        /// <param name="url"></param>
        public void addPage(string url)
        {
            // add the page tracker item to the array list
            this.pages.Add(url);
        }

        public bool hasAlreadyVisitedPage(string url)
        {
            return this.pages.Contains(url);
        }

        private void addCookie(string key, string value)
        {
            HttpCookie cookie = new HttpCookie(key, value);
            cookie.Expires = this.expires;
            this.context.Response.Cookies.Set(cookie);
        }

        #endregion

        #region Properties

        public string VisitCount
        {
            get
            {
                return this.visitCount;
            }
            set
            {
                this.visitCount = value;
            }
        }

        public string OriginalReferrer
        {
            get
            {
                return this.originalReferrer;
            }
        }

        public string OriginalUrl
        {
            get
            {
                return this.originalUrl;
            }
        }

        public string SessionReferrer
        {
            get
            {
                return this.sessionReferrer;
            }
        }

        public string SessionUrl
        {
            get
            {
                return this.sessionUrl;
            }
        }

        public string UserHostAddress
        {
            get
            {
                return this.userHostAddress;
            }
        }

        public string UserAgent
        {
            get
            {
                return this.userAgent;
            }
        }

        public List<string> Pages
        {
            get
            {
                return this.pages;
            }
        }

        public HttpBrowserCapabilities Browser
        {
            get
            {
                return this.browser;
            }
        }

        #endregion
    }

}
