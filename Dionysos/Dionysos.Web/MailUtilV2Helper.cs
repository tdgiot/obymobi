﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Web.UI;
using Dionysos.Web.UI.WebControls;

namespace Dionysos.Net.Mail
{
	public partial class MailUtilV2Helper
	{
		/// <summary>
		/// Retrieve template variabels from a ControlCollection
		/// </summary>
		/// <param name="controlCollection">ControlCollection containing controls</param>
		/// <returns>Template variables returned form control collection</returns>
		public static StringDictionary GetTemplateVariablesFromControlCollection(Dionysos.Web.UI.ControlCollection controlCollection)
		{
			StringDictionary templateVariables = new StringDictionary();

			for (int i = 0; i < controlCollection.Count; i++)
			{
				Control ctrl = controlCollection[i];

				string keyName = StringUtil.RemoveLeadingLowerCaseCharacters(ctrl.ID);
				string value = string.Empty;
				bool addKey = false;

				if (ctrl is TextBox)
				{
					TextBox tb = ctrl as TextBox;					
					value = tb.Text;
					addKey = true;
				}
				else if (ctrl is DropDownList)
				{
					DropDownList ddl = ctrl as DropDownList;					
					value = ddl.SelectedValue;
					addKey = true;
				}
				else if (ctrl is CheckBox)
				{
					CheckBox cb = ctrl as CheckBox;					
					if (cb.Text.Length == 0)
						keyName = Dionysos.StringUtil.RemoveLeadingLowerCaseCharacters(cb.ID);

					if (cb.Checked)
						value = "Aangevinkt";
					else
						value = "Niet aangevinkt";

					addKey = true;
				}
				else if (ctrl is RadioButton)
				{
					RadioButton rb = ctrl as RadioButton;					
					value = rb.Text;
					addKey = true;
				}
				else if (ctrl is RadioButtonList)
				{
					RadioButtonList rbl = ctrl as RadioButtonList;					
					if (rbl.SelectedIndex >= 0)
					{
						value = rbl.SelectedValue;
					}
					else
					{
						value = "Geen keuze gemaakt";
					}
					addKey = true;
				}

				// Pars keyName
				if(addKey && keyName.Length > 0 && templateVariables[keyName] == null)
					templateVariables.Add(keyName, StringUtil.ReplaceLineBreakWithBR(StringUtil.StripHtml(value)));
			}

			return templateVariables;
		}
	}
}
