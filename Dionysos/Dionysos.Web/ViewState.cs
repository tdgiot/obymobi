using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Web.UI;

namespace Dionysos.Web
{
    /// <summary>
    /// Class which represents a viewState
    /// </summary>
    public class ViewState
    {
        #region Fields

        static ViewState instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Web.ViewState type if the instance has not been initialized yet
        /// </summary>
        static ViewState()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new ViewState();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
        }

        /// <summary>
        /// Checks whether a viewState parameter exists in the viewState instance
        /// from the specified StateBag instance
        /// </summary>
        /// <param name="viewState">The StateBag instance to get the viewState instance from</param>
        /// <param name="name">The name of the viewState parameter</param>
        /// <returns>True if the viewState parameter exists, False if not</returns>
        public static bool HasParameter(StateBag viewState, string name)
        {
            bool hasParameter = true;

            if (Instance.ArgumentIsEmpty(viewState, "viewState"))
            {
                // Argument 'viewState' is empty
            }
            else if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Argument 'name' is empty
            }
            else
            {
                object value = viewState[name];
                if (Instance.Empty(value))
                {
                    hasParameter = false;
                }
            }

            return hasParameter;
        }

        /// <summary>
        /// Retrieves a viewState parameter from the viewState instance 
        /// from the specified StateBag instance
        /// </summary>
        /// <param name="viewState">The StateBag instance to get the viewState instance from</param>
        /// <param name="name">The name of the viewState parameter</param>
        /// <returns>An object instance containing the value of the viewState parameter</returns>
        public static object GetParameter(StateBag viewState, string name)
        {
            object value = null;

            if (Instance.ArgumentIsEmpty(viewState, "viewState"))
            {
                // Argument 'viewState' is empty
            }
            else if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Argument 'name' is empty
            }
            else
            {
                value = viewState[name];
                if (Instance.Empty(value))
                {
                    throw new Dionysos.EmptyException(string.Format("The value of viewState parameter '{0}' is empty!", name));
                }
            }

            return value;
        }

        /// <summary>
        /// Gets a System.Boolean value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Boolean instance containing the current value</returns>
        public static bool GetParameterAsBoolean(StateBag viewState, string name)
        {
            return (bool)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a System.Byte value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Byte instance containing the current value</returns>
        public static byte GetParameterAsByte(StateBag viewState, string name)
        {
            return (byte)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a System.Char value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Char instance containing the current value</returns>
        public static char GetParameterAsChar(StateBag viewState, string name)
        {
            return (char)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a System.Decimal value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Decimal instance containing the current value</returns>
        public static decimal GetParameterAsDecimal(StateBag viewState, string name)
        {
            return (decimal)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a System.Double value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Double instance containing the current value</returns>
        public static double GetParameterAsDouble(StateBag viewState, string name)
        {
            return (double)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a System.Single value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Single instance containing the current value</returns>
        public static float GetParameterAsFloat(StateBag viewState, string name)
        {
            return (float)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a System.Int32 value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int32 instance containing the current value</returns>
        public static int GetParameterAsInt(StateBag viewState, string name)
        {
            return (int)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a System.Int64 value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int64 instance containing the current value</returns>
        public static long GetParameterAsLong(StateBag viewState, string name)
        {
            return (long)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a System.Int16 value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int16 instance containing the current value</returns>
        public static short GetParameterAsShort(StateBag viewState, string name)
        {
            return (short)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a DateTime value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An DateTime instance containing the current value</returns>
        public static DateTime GetParameterAsDateTime(StateBag viewState, string name)
        {
            return (DateTime)GetParameter(viewState, name);
        }

        /// <summary>
        /// Gets a TimeStamp value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>A TimeStamp instance containing the current value</returns>
        public static TimeStamp GetParameterAsTimeStamp(StateBag viewState, string name)
        {
            return TimeStamp.Parse(GetParameterAsString(viewState, name));
        }

        /// <summary>
        /// Gets a System.Int32 instance of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int32 instance containing the current value</returns>
        public static int GetParameterAsEnumValue(StateBag viewState, string name, System.Type enumType)
        {
            Enum temp = (Enum)Enum.Parse(enumType, GetParameterAsString(viewState, name), true);
            return Convert.ToInt32(temp);
        }

        /// <summary>
        /// Gets a System.String value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.String instance containing the current value</returns>
        public static string GetParameterAsString(StateBag viewState, string name)
        {
            return GetParameter(viewState, name).ToString();
        }

        /// <summary>
        /// Gets a System.String value of a viewState parameter using the specified
        /// StateBag instance and the specified parameter name
        /// </summary>
        /// <returns>An System.String instance containing the current value</returns>
        public static System.ComponentModel.ListSortDirection GetParameterAsListSortDirection(StateBag viewState, string name)
        {
            return (System.ComponentModel.ListSortDirection)GetParameter(viewState, name);
        }

        #endregion
    }
}