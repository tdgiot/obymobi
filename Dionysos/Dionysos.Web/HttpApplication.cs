﻿using System;
using System.Diagnostics;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Dionysos.Verification;
using Dionysos.Web.Security;

namespace Dionysos.Web
{
	/// <summary>
	/// Defines the methods, properties, and events that are common to all application objects in an ASP.NET application. This class is the base class for applications that are defined by the user in the Global.asax file.
	/// </summary>
	public class HttpApplication : System.Web.HttpApplication
	{
		#region Fields

		/// <summary>
		/// Indicates whether the user manager is used.
		/// </summary>
		private bool? useUserManager = null;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether to use the user manager.
		/// </summary>
		/// <value>
		///   <c>true</c> if the user manager is used; otherwise, <c>false</c>.
		/// </value>
		public bool UseUserManager
		{
			get
			{
				return (this.useUserManager ?? (this.useUserManager = ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UseUserManager))).GetValueOrDefault();
			}
			set
			{
				this.useUserManager = value;
			}
		}

		/// <summary>
		/// Gets or sets the render stopwatch.
		/// </summary>
		/// <value>
		/// The render stopwatch.
		/// </value>
		public Stopwatch RenderStopwatch
		{
			get
			{
				return this.Context.Items["Dionysos.Web.HttpApplication.BeginRequest-RenderStopwatch"] as Stopwatch;
			}
			set
			{
				this.Context.Items["Dionysos.Web.HttpApplication.BeginRequest-RenderStopwatch"] = value;
			}
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the Start event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Application_Start(object sender, EventArgs e)
		{
			// These methods have been introduced becasue it's required by Crave to split these to
			// no changes to working have been made.
			this.DatabaseIndependentInitializationLogic();
			this.DatabaseDependentInitializationLogic();
		}

		protected void DatabaseIndependentInitializationLogic()
		{
			// Log instance start
			this.Application["Instance"] = DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.UtcNow);
		}

		protected void DatabaseDependentInitializationLogic()
		{
			VerifierCollection verifiers = new VerifierCollection();

			// Verify write permissions for error directory
			string errorDirectory = System.Web.Hosting.HostingEnvironment.MapPath(ErrorLoggerWeb.RelativeErrorDirectory);
			verifiers.Add(new WritePermissionVerifier(errorDirectory));

			// Verify write permissions for log directory
			string logDirectory = System.Web.Hosting.HostingEnvironment.MapPath(InformationLoggerWeb.RelativeLoggingDirectory);
			verifiers.Add(new WritePermissionVerifier(logDirectory));

			// Verify write permissions for temp directory
			string tempDirectory = System.Web.Hosting.HostingEnvironment.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.TempFileFolder));
			verifiers.Add(new WritePermissionVerifier(tempDirectory));

			if (!verifiers.Verify())
			{
				throw new VerificationException(verifiers.ErrorMessage);
			}

			// Set error map path
			ErrorLoggerWeb.ResolvedErrorDirectory = errorDirectory;

			if (ConfigurationManager.GetBool(DionysosWebConfigurationConstants.LogApplicationStartAndRestart))
			{
				InformationLoggerWeb.LogInformation("HttpApplication.Start");
			}
		}

		/// <summary>
		/// Handles the Start event of the Session control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Session_Start(object sender, EventArgs e)
		{
			// Code that runs when a new session is started
			if (this.Context.User.Identity.IsAuthenticated)
			{
				if (ConfigurationManager.GetBool(DionysosWebConfigurationConstants.LogApplicationStartAndRestart))
				{
					InformationLoggerWeb.LogInformation("SessionsRestart");
				}

				string applicationInstance = HttpApplication.GetApplicationInstance();
				if (!QueryStringHelper.HasValue<string>("sessionRestart") || QueryStringHelper.GetValue("sessionRestart") != applicationInstance &&
					(this.Session["SavedSessionId"] == null || this.Session["SavedSessionId"].ToString() != this.Session.SessionID))
				{
					FormsAuthentication.SignOut();
					HttpApplication.SetSessionId();

					// Need redirect restart
					Uri currentUrl = new Uri(this.Request.Url.GetBase(), this.Request.RawUrl);

					QueryStringHelper qs = new QueryStringHelper();
					qs.AddItem("sessionRestart", this.Application["Instance"].ToString());
					this.Response.Redirect(qs.MergeQuerystringWithUrl(currentUrl.GetLeftPart(UriPartial.Path), true, true), true);
				}
			}
		}

		/// <summary>
		/// Handles the AuthenticateRequest event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Application_AuthenticateRequest(object sender, EventArgs e)
		{
			// If Autlogon is enabled and the user is not signed in it's the first request from that users, otherwise he would be signed in automaticly.
			if (this.UseUserManager &&
				UserManager.Instance.AutoLogon)
			{
				if (this.Context.User == null ||
					this.Context.User.Identity == null ||
					!this.Context.User.Identity.IsAuthenticated)
				{
					// We have no Authenticated User.
					string autoLogonUserName = ConfigurationManager.GetString(DionysosWebConfigurationConstants.AutoLogonUsername);
					IManagableUser user = UserManager.Instance.GetUserByUsername(autoLogonUserName);
					if (user == null)
					{
						throw new TechnicalException("Er is een niet bestaande gebruikersnaam ingesteld voor de automatische login (AutoLogonUsername).");
					}

					// Set auth cookie
					FormsAuthentication.SetAuthCookie(user.UserId.ToString(), true);

					// Redirect to current Url
					WebShortcuts.RedirectUsingRawUrl();
				}
			}

			if (this.UseUserManager &&
				!String.IsNullOrEmpty(UserManager.Instance.UserEntityName))
			{
				if (this.Context.User != null)
				{
					if (this.Context.User.Identity.AuthenticationType != "Forms") throw new Exception("Only forms authentication is supported, not " + this.Context.User.Identity.AuthenticationType + ".");

					IIdentity identity = this.Context.User.Identity;
					IManagableUser user = UserManager.CurrentUser;
					this.Context.User = new GenericPrincipal(identity, user.Roles);
				}
			}
		}

		/// <summary>
		/// Handles the BeginRequest event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Application_BeginRequest(object sender, EventArgs e)
		{
			this.RenderStopwatch = Stopwatch.StartNew();
		}

		/// <summary>
		/// Handles the PreRequestHandlerExecute event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Application_PreRequestHandlerExecute(object sender, EventArgs e)
		{ }

		/// <summary>
		/// Handles the EndRequest event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Application_EndRequest(object sender, EventArgs e)
		{
			try
			{
				Stopwatch stopwatch = this.RenderStopwatch;
				if (stopwatch != null)
				{
					this.Response.AddHeader("X-Rendered-In", stopwatch.ElapsedMilliseconds.ToString());
				}

				if (this.Response.StatusCode >= 400 &&
					this.Response.StatusCode < 600)
				{
					this.Response.AddHeader("X-Robots-Tag", "noindex, noarchive, nocache");
				}
			}
			catch { }
		}

		/// <summary>
		/// Handles the End event of the Session control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Session_End(object sender, EventArgs e)
		{ }

		/// <summary>
		/// Handles the End event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Application_End(object sender, EventArgs e)
		{ }

		/// <summary>
		/// Handles the Error event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void Application_Error(object sender, EventArgs e)
		{
			// Code that runs when an unhandled error occurs
			try
			{
				ErrorLoggerWeb.LogError(Server.GetLastError().GetBaseException(), "Error from Application_Error in Dionysos.HttpApplication");
			}
			catch
			{ }
		}

		#endregion

		#region Methods

		/// <summary>
		/// Set the SessionId which is used to verify a session is linked to the correct user.
		/// </summary>
		/// <remarks>
		/// Should only be called in exceptional cases, normally only used by this class.
		/// </remarks>
		public static void SetSessionId()
		{
		    SessionHelper.SetValue("SavedSessionId", SessionHelper.SessionID);
		}

		/// <summary>
		/// Get the application instance (DateTimeStamp of first request to the instance).
		/// </summary>
		/// <returns>The application instance.</returns>
		public static string GetApplicationInstance()
		{
			if (HttpContext.Current.Application["Instance"] == null)
			{
				HttpContext.Current.Application["Instance"] = DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.UtcNow);
			}

			return (string)HttpContext.Current.Application["Instance"];
		}

		#endregion
	}
}