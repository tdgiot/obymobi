using System;
using System.Collections.Generic;
using System.Text;
using Dionysos;

namespace Dionysos.Web
{
    /// <summary>
    /// Exception class for when an invalid link has been called
    /// </summary>
    public class InvalidLinkException : FunctionalException
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Web.InvalidLinkException class
        /// </summary>	
        public InvalidLinkException(string message) : base(message)
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties



        #endregion

        #region Event handlers



        #endregion
    }
}

