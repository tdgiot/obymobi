﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using LinqToTwitter;

namespace Dionysos.Web.LinqToTwitter
{
	/// <summary>
	/// Get the credentials from the configuration.
	/// </summary>
	public class ConfigurationCredentials //: InMemoryCredentials
	{
		/// <summary>
		/// Gets or sets the consumer key.
		/// </summary>
		/// <value>
		/// The consumer key.
		/// </value>
		public string ConsumerKey
		{
			get
			{
				return ConfigurationManager.GetString(DionysosWebConfigurationConstants.TwitterConsumerKey);
			}
			set
			{
				ConfigurationManager.SetValue(DionysosWebConfigurationConstants.TwitterConsumerKey, value ?? String.Empty);
			}
		}

		/// <summary>
		/// Gets or sets the consumer secret.
		/// </summary>
		/// <value>
		/// The consumer secret.
		/// </value>
		public string ConsumerSecret
		{
			get
			{
				return ConfigurationManager.GetString(DionysosWebConfigurationConstants.TwitterConsumerSecret);
			}
			set
			{
				ConfigurationManager.SetValue(DionysosWebConfigurationConstants.TwitterConsumerSecret, value ?? String.Empty);
			}
		}

		/// <summary>
		/// Gets or sets the OAuth token.
		/// </summary>
		/// <value>
		/// The OAuth token.
		/// </value>
		public string OAuthToken
		{
			get
			{
				return ConfigurationManager.GetString(DionysosWebConfigurationConstants.TwitterToken);
			}
			set
			{
				ConfigurationManager.SetValue(DionysosWebConfigurationConstants.TwitterToken, value ?? String.Empty);
			}
		}

		/// <summary>
		/// Gets or sets the access token.
		/// </summary>
		/// <value>
		/// The access token.
		/// </value>
		public string AccessToken
		{
			get
			{
				return ConfigurationManager.GetString(DionysosWebConfigurationConstants.TwitterTokenSecret);
			}
			set
			{
				ConfigurationManager.SetValue(DionysosWebConfigurationConstants.TwitterTokenSecret, value ?? String.Empty);
			}
		}
	}
}
