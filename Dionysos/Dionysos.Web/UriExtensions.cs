﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections.Specialized;

namespace Dionysos.Web
{
	/// <summary>
	/// Extension class for Uri and UriBuilder.
	/// </summary>
	public static class UriExtensions
	{
		#region RemoveApplicationPath

		/// <summary>
		/// Remove the <see cref="HttpRequest.ApplicationPath"/> from the <see cref="System.Uri"/>.
		/// </summary>
		/// <param name="uri">The Uri instance to remove the ApplicationPath from.</param>
		/// <returns>Returns a new Uri instance without the ApplicationPath.</returns>
		public static Uri RemoveApplicationPath(this Uri uri)
		{
			return uri.RemoveApplicationPath(HttpContext.Current.Request.ApplicationPath);
		}

		/// <summary>
		/// Remove the specified ApplicationPath from the <see cref="System.Uri"/>.
		/// </summary>
		/// <param name="uri">The Uri instance to remove the ApplicationPath from.</param>
		/// <param name="applicationPath">The ApplicationPath to remove.</param>
		/// <returns>Returns a new Uri instance without the ApplicationPath.</returns>
		public static Uri RemoveApplicationPath(this Uri uri, string applicationPath)
		{
			return new UriBuilder(uri).RemoveApplicationPath(applicationPath).Uri;
		}

		/// <summary>
		/// Remove the <see cref="HttpRequest.ApplicationPath"/> from the <see cref="System.UriBuilder"/>.
		/// </summary>
		/// <param name="uriBuilder">The UriBuider instance to remove the ApplicationPath from.</param>
		/// <returns>Returns a new UriBuilder instance without the ApplicationPath.</returns>
		public static UriBuilder RemoveApplicationPath(this UriBuilder uriBuilder)
		{
			return uriBuilder.RemoveApplicationPath(HttpContext.Current.Request.ApplicationPath);
		}

		/// <summary>
		/// Remove the specified ApplicationPath from the <see cref="System.UriBuilder"/> instance.
		/// </summary>
		/// <param name="uriBuilder">The UriBuilder instance to remove the ApplicationPath from.</param>
		/// <param name="applicationPath">The ApplicationPath to remove.</param>
		/// <returns>Returns a new UriBuilder instance without the ApplicationPath.</returns>
		public static UriBuilder RemoveApplicationPath(this UriBuilder uriBuilder, string applicationPath)
		{
			UriBuilder newUri = new UriBuilder(uriBuilder.Uri);
			if (applicationPath.Length > 1 && newUri.Path.StartsWith(applicationPath, StringComparison.OrdinalIgnoreCase))
			{
				newUri.Path = newUri.Path.Remove(0, applicationPath.Length);
			}

			return newUri;
		}

		#endregion

		#region AddApplicationPath

		/// <summary>
		/// Adds the <see cref="HttpRequest.ApplicationPath"/> to the <see cref="System.Uri"/>.
		/// </summary>
		/// <param name="uri">The Uri instance to add the ApplicationPath to.</param>
		/// <returns>Returns a new Uri instance with the ApplicationPath added.</returns>
		public static Uri AddApplicationPath(this Uri uri)
		{
			return uri.AddApplicationPath(HttpContext.Current.Request.ApplicationPath);
		}

		/// <summary>
		/// Adds the <see cref="HttpRequest.ApplicationPath"/> to the <see cref="System.Uri"/>.
		/// </summary>
		/// <param name="uri">The Uri instance to add the ApplicationPath to.</param>
		/// <param name="conditionalAdd">If true, only adds the ApplicationPath if not already present; otherwise always add the ApplicationPath.</param>
		/// <returns>Returns a new Uri instance with the ApplicationPath added.</returns>
		public static Uri AddApplicationPath(this Uri uri, bool conditionalAdd)
		{
			return uri.AddApplicationPath(HttpContext.Current.Request.ApplicationPath, conditionalAdd);
		}

		/// <summary>
		/// Adds the specified ApplicationPath to the <see cref="System.Uri"/>.
		/// </summary>
		/// <param name="uri">The Uri instance to add the ApplicationPath to.</param>
		/// <param name="applicationPath">The ApplicationPath to add.</param>
		/// <returns>Returns a new Uri instance with the ApplicationPath added.</returns>
		public static Uri AddApplicationPath(this Uri uri, string applicationPath)
		{
			return uri.AddApplicationPath(applicationPath, false);
		}

		/// <summary>
		/// Adds the <see cref="HttpRequest.ApplicationPath"/> to the <see cref="System.Uri"/>.
		/// </summary>
		/// <param name="uri">The Uri instance to add the ApplicationPath to.</param>
		/// <param name="applicationPath">The ApplicationPath to add.</param>
		/// <param name="conditionalAdd">If true, only adds the ApplicationPath if not already present; otherwise always add the ApplicationPath.</param>
		/// <returns>Returns a new Uri instance with the ApplicationPath added.</returns>
		public static Uri AddApplicationPath(this Uri uri, string applicationPath, bool conditionalAdd)
		{
			return new UriBuilder(uri).AddApplicationPath(applicationPath, conditionalAdd).Uri;
		}

		/// <summary>
		/// Adds the <see cref="HttpRequest.ApplicationPath"/> to the <see cref="System.UriBuilder"/>.
		/// </summary>
		/// <param name="uriBuilder">The UriBuilder instance to add the ApplicationPath to.</param>
		/// <returns>Returns a new UriBuilder instance with the ApplicationPath added.</returns>
		public static UriBuilder AddApplicationPath(this UriBuilder uriBuilder)
		{
			return uriBuilder.AddApplicationPath(HttpContext.Current.Request.ApplicationPath);
		}

		/// <summary>
		/// Adds the <see cref="HttpRequest.ApplicationPath"/> to the <see cref="System.UriBuilder"/>.
		/// </summary>
		/// <param name="uriBuilder">The UriBuilder instance to add the ApplicationPath to.</param>
		/// <param name="conditionalAdd">If true, only adds the ApplicationPath if not already present; otherwise always add the ApplicationPath.</param>
		/// <returns>Returns a new UriBuilder instance with the ApplicationPath added.</returns>
		public static UriBuilder AddApplicationPath(this UriBuilder uriBuilder, bool conditionalAdd)
		{
			return uriBuilder.AddApplicationPath(HttpContext.Current.Request.ApplicationPath, conditionalAdd);
		}

		/// <summary>
		/// Adds the specified ApplicationPath to the <see cref="System.UriBuilder"/>.
		/// </summary>
		/// <param name="uriBuilder">The UriBuilder instance to add the ApplicationPath to.</param>
		/// <param name="applicationPath">The ApplicationPath to add.</param>
		/// <returns>Returns a new UriBuilder instance with the ApplicationPath added.</returns>
		public static UriBuilder AddApplicationPath(this UriBuilder uriBuilder, string applicationPath)
		{
			return uriBuilder.AddApplicationPath(applicationPath, false);
		}

		/// <summary>
		/// Adds the specified ApplicationPath to the <see cref="System.UriBuilder"/>.
		/// </summary>
		/// <param name="uriBuilder">The UriBuilder instance to add the ApplicationPath to.</param>
		/// <param name="applicationPath">The ApplicationPath to add.</param>
		/// <param name="conditionalAdd">If true, only adds the ApplicationPath if not already present; otherwise always add the ApplicationPath.</param>
		/// <returns>Returns a new UriBuilder instance with the ApplicationPath added.</returns>
		public static UriBuilder AddApplicationPath(this UriBuilder uriBuilder, string applicationPath, bool conditionalAdd)
		{
			UriBuilder newUri = new UriBuilder(uriBuilder.Uri);
			if (applicationPath.Length > 1 && (!conditionalAdd || !newUri.Path.StartsWith(applicationPath, StringComparison.OrdinalIgnoreCase)))
			{
				newUri.Path = newUri.Path.Insert(0, applicationPath);
			}

			return newUri;
		}

		#endregion

		#region AddQueryString

		/// <summary>
		/// Adds the query string.
		/// </summary>
		/// <param name="uri">The URI.</param>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns>
		/// A new Uri instance with the query string added.
		/// </returns>
		public static Uri AddQueryString(this Uri uri, string name, object value)
		{
			return uri.AddQueryString(name, value, false);
		}

		/// <summary>
		/// Adds the query string.
		/// </summary>
		/// <param name="uri">The URI.</param>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <param name="replace">If set to <c>true</c> replaces existing values.</param>
		/// <returns>
		/// A new Uri instance with the query string added.
		/// </returns>
		public static Uri AddQueryString(this Uri uri, string name, object value, bool replace)
		{
			return uri.AddQueryString(new NameValueCollection() {
				{ name, Convert.ToString(value) }
			}, replace);
		}

		/// <summary>
		/// Adds the query string.
		/// </summary>
		/// <param name="uri">The URI.</param>
		/// <param name="queryString">The query string.</param>
		/// <returns>
		/// A new Uri instance with the query string added.
		/// </returns>
		public static Uri AddQueryString(this Uri uri, NameValueCollection queryString)
		{
			return uri.AddQueryString(queryString, false);
		}

		/// <summary>
		/// Adds the query string.
		/// </summary>
		/// <param name="uri">The URI.</param>
		/// <param name="queryString">The query string.</param>
		/// <param name="replace">If set to <c>true</c> replaces existing values.</param>
		/// <returns>
		/// A new Uri instance with the query string added.
		/// </returns>
		public static Uri AddQueryString(this Uri uri, NameValueCollection queryString, bool replace)
		{
			return new UriBuilder(uri).AddQueryString(queryString, replace).Uri;
		}

		/// <summary>
		/// Adds the query string.
		/// </summary>
		/// <param name="uriBuilder">The URI builder.</param>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns>
		/// A new UriBuilder instance with the query string added.
		/// </returns>
		public static UriBuilder AddQueryString(this UriBuilder uriBuilder, string name, string value)
		{
			return uriBuilder.AddQueryString(name, value, false);
		}

		/// <summary>
		/// Adds the query string.
		/// </summary>
		/// <param name="uriBuilder">The URI builder.</param>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <param name="replace">If set to <c>true</c> replaces existing values.</param>
		/// <returns>
		/// A new UriBuilder instance with the query string added.
		/// </returns>
		public static UriBuilder AddQueryString(this UriBuilder uriBuilder, string name, string value, bool replace)
		{
			return uriBuilder.AddQueryString(new NameValueCollection() { { name, value } }, replace);
		}

		/// <summary>
		/// Adds the query string.
		/// </summary>
		/// <param name="uriBuilder">The URI builder.</param>
		/// <param name="queryString">The query string.</param>
		/// <returns>
		/// A new UriBuilder instance with the query string added.
		/// </returns>
		public static UriBuilder AddQueryString(this UriBuilder uriBuilder, NameValueCollection queryString)
		{
			return uriBuilder.AddQueryString(queryString, false);
		}

		/// <summary>
		/// Adds the query string.
		/// </summary>
		/// <param name="uriBuilder">The URI builder.</param>
		/// <param name="queryString">The query string.</param>
		/// <param name="replace">If set to <c>true</c> replaces existing values.</param>
		/// <returns>
		/// A new UriBuilder instance with the query string added.
		/// </returns>
		public static UriBuilder AddQueryString(this UriBuilder uriBuilder, NameValueCollection queryString, bool replace)
		{
			UriBuilder newUri = new UriBuilder(uriBuilder.Uri);
			NameValueCollection newQueryString = new NameValueCollection();
			if (!String.IsNullOrEmpty(newUri.Query) && newUri.Query.Length > 1)
			{
				newQueryString.Add(HttpUtility.ParseQueryString(newUri.Query.Substring(1)));
				newUri.Query = null;
			}

			// Replace or add values
			if (replace)
			{
				foreach (string key in queryString.AllKeys)
				{
					newQueryString.Set(key, queryString[key]);
				}
			}
			else
			{
				newQueryString.Add(queryString);
			}

			// Construct new query string
			if (newQueryString.Count > 0)
			{
				string newQuery = null;
				foreach (string key in newQueryString.AllKeys)
				{
					if (!String.IsNullOrEmpty(newQuery)) newQuery += "&";
					newQuery += String.Format("{0}={1}", key, Uri.EscapeDataString(newQueryString[key]));
				}
				newUri.Query = newQuery;
			}

			return newUri;
		}

		#endregion

		#region GetBase

		/// <summary>
		/// Gets the base Uri (scheme, authority and <see cref="HttpRequest.ApplicationPath"/>) form the Uri instance.
		/// </summary>
		/// <param name="uri">The Uri instance to get the base Uri from.</param>
		/// <returns>Returns a new Uri instance with the base Uri.</returns>
		public static Uri GetBase(this Uri uri)
		{
			return uri.GetBase(HttpContext.Current.Request.ApplicationPath);
		}

		/// <summary>
		/// Gets the base Uri (scheme, authority and specified ApplicationPath) form the Uri instance.
		/// </summary>
		/// <param name="uri">The Uri instance to get the base Uri from.</param>
		/// <param name="applicationPath">The ApplicationPath to add.</param>
		/// <returns>Returns a new Uri instance with the base Uri.</returns>
		public static Uri GetBase(this Uri uri, string applicationPath)
		{
			return new Uri(uri.GetLeftPart(UriPartial.Authority) + applicationPath);
		}

		#endregion

        /// <summary>
        /// Adds the query value to the url.
        /// </summary>
        /// <param name="originalUri">The original URI.</param>
        /// <param name="segment">The segment.</param>
        /// <returns>
        /// Uri.
        /// </returns>
        public static Uri AddQueryString(this Uri originalUri, string segment)
        {
            var uriBuilder = new UriBuilder(originalUri) { Query = segment };
            originalUri = uriBuilder.Uri;
            return originalUri;
        }

        /// <summary>
        /// Adds the segment value to the url.
        /// </summary>
        /// <param name="originalUri">The original URI.</param>
        /// <param name="segment">The segment .</param>
        /// <returns>Uri</returns>
        public static Uri AddSegment(this Uri originalUri, string segment)
        {
            originalUri = new Uri(originalUri.OriginalString + "/" + segment);
            return originalUri;
        }
	}
}
