﻿namespace Dionysos.Web.Sessions
{
    public interface ISessionStore
    {
        /// <summary>
        /// Gets whether HttpContext.Current.Session is not null.
        /// </summary>
        bool HasSession { get; }

        /// <summary>
        /// Gets unique SessionID
        /// </summary>
        string SessionID { get; }

        /// <summary>
        /// Tries to get the value from the Session using the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="key">The key of the Session value.</param>		
        /// <param name="value">When this method returns true, contains the value retrieved from the Session; otherwise returns the default value for the specified type.</param>
        /// <returns>true if the key and corresponding value exists in the Cache; otherwise, false.</returns>
        bool TryGetValue<T>(string key, out T value);

        /// <summary>
        /// Gets the value from the Session using the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="key">The key of the Session value.</param>
        /// <returns>Returns the value, if found and type matches; otherwise returns the default value.</returns>
        T GetValue<T>(string key);

        /// <summary>
        /// Indicates whether the key exists in the Session and is of the specified type.
        /// </summary>
        /// <typeparam name="T">The type of the value to check.</typeparam>
        /// <param name="key">The key of the Session value.</param>		
        /// <returns>true if the value exists in the Session and is of the specified type; otherwise false.</returns>
        bool HasValue<T>(string key);

        /// <summary>
        /// Tries to set the value to the Session using the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the value to set.</typeparam>
        /// <param name="key">The key for the Session value.</param>
        /// <param name="value">The value to set.</param>
        /// <returns>true if Session value is set; false if Session value is not set or removed.</returns>
        bool SetValue<T>(string key, T value);

        /// <summary>
        /// Removes the value with the specified key from the Session.
        /// </summary>
        /// <param name="key">The key for the Session value</param>
        /// <returns>true if Session value is removed; otherwise false.</returns>
        bool RemoveValue(string key);
    }
}
