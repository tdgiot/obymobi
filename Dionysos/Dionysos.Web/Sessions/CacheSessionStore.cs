﻿using System.Web;
using System.Web.Caching;

namespace Dionysos.Web.Sessions
{
    public class CacheSessionStore : ISessionStore
    {
        public bool HasSession
        {
            get { return (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Cache != null); }
        }

        public string SessionID
        {
            get
            {
                if (!HasSession) 
                    return string.Empty;

                return HttpContext.Current.Session.SessionID;
            }
        }

        public bool TryGetValue<T>(string key, out T value)
        {
            return CacheHelper.TryGetValue(key, true, out value);
        }

        public T GetValue<T>(string key)
        {
            T output;
            CacheHelper.TryGetValue(key, true, out output);
            
            return output;
        }

        public bool HasValue<T>(string key)
        {
            T value;
            return CacheHelper.TryGetValue(key, true, out value);
        }

        public bool SetValue<T>(string key, T value)
        {
            if (!HasSession) 
                return true;

            CacheHelper.AddSlidingExpire(true, key, value, null, HttpContext.Current.Session.Timeout, CacheItemPriority.NotRemovable);

            return true;
        }

        public bool RemoveValue(string key)
        {
            CacheHelper.Remove(true, key);
            return true;
        }
    }
}
