﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Dionysos.Web.Sessions
{
    public class DefaultSessionStore : ISessionStore
    {
        /// <summary>
        /// Gets whether HttpContext.Current.Session is not null.
        /// </summary>
        public bool HasSession
        {
            get { return HttpContext.Current != null && HttpContext.Current.Session != null; }
        }

        public string SessionID
        {
            get
            {
                if (HasSession)
                    return HttpContext.Current.Session.SessionID;

                return string.Empty;
            }
        }

        /// <summary>
        /// Tries to get the value from the Session using the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="key">The key of the Session value.</param>		
        /// <param name="value">When this method returns true, contains the value retrieved from the Session; otherwise returns the default value for the specified type.</param>
        /// <returns>true if the key and corresponding value exists in the Cache; otherwise, false.</returns>
        public bool TryGetValue<T>(string key, out T value)
        {
            HttpSessionState session = HttpContext.Current.Session;

            if (session != null)
            {
                return TryGetValue(session, key, out value);
            }

            value = default(T);
            return false;
        }

        /// <summary>
        /// Gets the value from the Session using the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="key">The key of the Session value.</param>
        /// <returns>Returns the value, if found and type matches; otherwise returns the default value.</returns>
        public T GetValue<T>(string key)
        {
            HttpSessionState session = HttpContext.Current.Session;

            if (session != null)
            {
                return GetValue<T>(session, key);
            }

            return default(T);
        }

        /// <summary>
		/// Indicates whether the key exists in the Session and is of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of the value to check.</typeparam>
		/// <param name="key">The key of the Session value.</param>		
		/// <returns>true if the value exists in the Session and is of the specified type; otherwise false.</returns>
		public bool HasValue<T>(string key)
		{
			HttpSessionState session = HttpContext.Current.Session;

			if (session != null)
			{
				return HasValue<T>(session, key);
			}

            return false;
		}

		/// <summary>
		/// Tries to set the value to the Session using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="key">The key for the Session value.</param>
		/// <param name="value">The value to set.</param>
		/// <returns>true if Session value is set; false if Session value is not set or removed.</returns>
		public bool SetValue<T>(string key, T value)
		{
			HttpSessionState session = HttpContext.Current.Session;
		    
			if (session != null)
			{
                if (session.IsReadOnly)
                    return false;

				return SetValue(session, key, value);
			}

		    return false;
		}

		/// <summary>
		/// Removes the value with the specified key from the Session.
		/// </summary>
		/// <param name="key">The key for the Session value</param>
		/// <returns>true if Session value is removed; otherwise false.</returns>
		public bool RemoveValue(string key)
		{
			HttpSessionState session = HttpContext.Current.Session;
            if (session.IsReadOnly)
                return false;

			if (session != null)
			{
				return RemoveValue(session, key);
			}

		    return false;
		}

		/// <summary>
		/// Tries to get the value from the Session using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="session">The Session object to get the value from.</param>
		/// <param name="key">The key of the Session value.</param>		
		/// <param name="value">When this method returns true, contains the value retrieved from the Session; otherwise returns the default value for the specified type.</param>
		/// <returns>true if the key and corresponding value exists in the Session; otherwise, false.</returns>
		private bool TryGetValue<T>(HttpSessionState session, string key, out T value)
		{
			if (session == null)
			{
				throw new ArgumentNullException("session");
			}

			if (key != null)
			{
				object sessionValue = session[key];

				if (sessionValue is T)
				{
					// Value is valid
					value = (T)sessionValue;
					return true;
				}
			}

			value = default(T);
			return false;
		}

		/// <summary>
		/// Gets the value from the Session using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="session">The Session object to get the value from.</param>
		/// <param name="key">The key of the Session value.</param>
		/// <returns>Returns the value, if found and type matches; otherwise returns the default value.</returns>
        private T GetValue<T>(HttpSessionState session, string key)
		{
			if (session == null)
			{
				throw new ArgumentNullException("session");
			}

			T value;
			TryGetValue(session, key, out value);

			return value;
		}

		/// <summary>
		/// Indicates whether the key exists in the Session and is of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of the value to check.</typeparam>
		/// <param name="session">The Session object to get the value from.</param>
		/// <param name="key">The key of the Session value.</param>		
		/// <returns>true if the value exists in the Session and is of the specified type; otherwise false.</returns>
        private bool HasValue<T>(HttpSessionState session, string key)
		{
			if (session == null)
			{
				throw new ArgumentNullException("session");
			}

			T value;
			return TryGetValue(session, key, out value);
		}

		/// <summary>
		/// Tries to set the value to the Session using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="session">The Session object to get the value from.</param>
		/// <param name="key">The key for the Session value.</param>
		/// <param name="value">The value to set.</param>
		/// <returns>true if Session value is set; false if Session value is not set or removed.</returns>
        private bool SetValue<T>(HttpSessionState session, string key, T value)
		{
			if (session == null)
			{
				throw new ArgumentNullException("session");
			}

		    if (!session.IsReadOnly && key != null)
		    {
		        if (value != null)
		        {
		            // Set value
		            session.Add(key, value);
		            return true;
		        }

		        // Remove value
		        session.Remove(key);
		    }

		    return false;
		}

		/// <summary>
		/// Removes the value with the specified key from the Session.
		/// </summary>
		/// <param name="key">The key for the Session value</param>
		/// <param name="session">The Session object to get the value from.</param>
		/// <returns>true if Session value is removed; otherwise false.</returns>
        private bool RemoveValue(HttpSessionState session, string key)
		{
			if (session == null)
			{
				throw new ArgumentNullException("session");
			}

			if (!session.IsReadOnly && key != null)
			{
				session.Remove(key);
				return true;
			}

		    return false;
		}
    }
}
