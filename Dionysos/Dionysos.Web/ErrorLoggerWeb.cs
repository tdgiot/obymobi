﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using Dionysos.Net.Mail;
using System.Net.Mail;
using Dionysos.Web.Security;
using System.ComponentModel;
using System.Security.Cryptography;

namespace Dionysos.Web
{
	public static class ErrorLoggerWeb
	{
		#region Fields

		/// <summary>
		/// The resolved directory path to store all error files (must be set if used without HttpContext).
		/// </summary>
		public static string ResolvedErrorDirectory = String.Empty;

		/// <summary>
		/// Backing field for EmailRecipientAddresses.
		/// </summary>
		private static string[] emailRecipientAddresses;

		/// <summary>
		/// Backing field for EmailSenderAddress.
		/// </summary>
		private static string emailSenderAddress;

		/// <summary>
		/// Backing field for EmailSenderName.
		/// </summary>
		private static string emailSenderName;

		#endregion

		#region Properties

		/// <summary>
		/// The relative directory path to store all error files.
		/// </summary>
		public static string RelativeErrorDirectory
		{
			get
			{
				string path = string.Empty;
				try
				{
					path = ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder);
				}
				catch
				{
					path = "~/App_Data/Error/";
				}

				return path;
			}
		}

		/// <summary>
		/// Gets or sets the email adresses to send the error logs to.
		/// </summary>
		/// <value>
		/// The email recipient addresses.
		/// </value>
		public static string[] EmailRecipientAddresses
		{
			get
			{
				if (ErrorLoggerWeb.emailRecipientAddresses == null)
				{
					ErrorLoggerWeb.emailRecipientAddresses = ConfigurationManager.GetString(DionysosWebConfigurationConstants.ErrorLoggerWebEmailRecipientAddresses).Split(';', StringSplitOptions.RemoveEmptyEntries);
				}

				return ErrorLoggerWeb.emailRecipientAddresses;
			}
			set
			{
				ErrorLoggerWeb.emailRecipientAddresses = value;
			}
		}

		/// <summary>
		/// Gets or sets the email address to use as sender.
		/// </summary>
		/// <value>
		/// The email sender address.
		/// </value>
		public static string EmailSenderAddress
		{
			get
			{
				if (ErrorLoggerWeb.emailSenderAddress == null)
				{
					ErrorLoggerWeb.emailSenderAddress = ConfigurationManager.GetString(DionysosConfigurationConstants.EmailSenderAddress);
				}

				return ErrorLoggerWeb.emailSenderAddress;
			}
			set
			{
				ErrorLoggerWeb.emailSenderAddress = value;
			}
		}

		/// <summary>
		/// Gets or sets the email name to use as sender.
		/// </summary>
		/// <value>
		/// The name of the email sender.
		/// </value>
		public static string EmailSenderName
		{
			get
			{
				if (ErrorLoggerWeb.emailSenderName == null)
				{
					ErrorLoggerWeb.emailSenderName = ConfigurationManager.GetString(DionysosConfigurationConstants.EmailSenderName);
				}

				return ErrorLoggerWeb.emailSenderName;
			}
			set
			{
				ErrorLoggerWeb.emailSenderName = value;
			}
		}

		/// <summary>
		/// Occurs before writing the error to the file.
		/// </summary>
		public static event EventHandler<CancelExceptionEventArgs> BeforeWriteFile;

		/// <summary>
		/// Occurs before mailing the error.
		/// </summary>
		public static event EventHandler<CancelExceptionEventArgs> BeforeMailError;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes the <see cref="ErrorLoggerWeb"/> class.
		/// </summary>
		static ErrorLoggerWeb()
		{
			// Add default filters
			ErrorLoggerWeb.BeforeMailError += ErrorLoggerWeb.FilterLocalRequest;
			ErrorLoggerWeb.BeforeMailError += ErrorLoggerWeb.FilterHttpRequestValidationException;
			ErrorLoggerWeb.BeforeMailError += ErrorLoggerWeb.FilterViewStateException;
			ErrorLoggerWeb.BeforeMailError += ErrorLoggerWeb.FilterHttpExceptionInvalidResources;
		}

		#endregion

		#region Filters

		/// <summary>
		/// Filters local requests.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="Dionysos.Web.CancelExceptionEventArgs"/> instance containing the event data.</param>
		public static void FilterLocalRequest(object sender, CancelExceptionEventArgs e)
		{
			HttpContext context = sender as HttpContext;

			if (context != null &&
				context.Request.IsLocal)
			{
				e.Cancel = true;
			}
		}

		/// <summary>
		/// Filters the HttpRequestValidationException.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="Dionysos.Web.CancelExceptionEventArgs"/> instance containing the event data.</param>
		public static void FilterHttpRequestValidationException(object sender, CancelExceptionEventArgs e)
		{
			if (e.Exception.GetBaseException() is System.Web.HttpRequestValidationException)
			{
				e.Cancel = true;
			}
		}

		/// <summary>
		/// Filters the ViewStateException.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="Dionysos.Web.CancelExceptionEventArgs"/> instance containing the event data.</param>
		public static void FilterViewStateException(object sender, CancelExceptionEventArgs e)
		{
			if (e.Exception.GetBaseException() is System.Web.UI.ViewStateException)
			{
				e.Cancel = true;
			}
		}

		/// <summary>
		/// Filters the HttpExceptions for invalid resources.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="Dionysos.Web.CancelExceptionEventArgs"/> instance containing the event data.</param>
		public static void FilterHttpExceptionInvalidResources(object sender, CancelExceptionEventArgs e)
		{
			if (e.Exception.GetBaseException() is System.Web.HttpException &&
				("This is an invalid webresource request.".Equals(e.Exception.Message) || "This is an invalid script resource request.".Equals(e.Exception.Message)))
			{
				e.Cancel = true;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Logs the error using HttpContext.Current.
		/// </summary>
		/// <param name="message">The additional message format.</param>
		/// <param name="args">Any additional items to format the message with.</param>
		public static void LogError(string message, params object[] args)
		{
			ErrorLoggerWeb.LogError(HttpContext.Current, null, message, args);
		}

		/// <summary>
		/// Logs the error using HttpContext.Current.
		/// </summary>
		/// <param name="exception">The exception to log.</param>
		/// <param name="message">The additional message format.</param>
		/// <param name="args">Any additional items to format the message with.</param>
		public static void LogError(Exception exception, string message, params object[] args)
		{
			ErrorLoggerWeb.LogError(HttpContext.Current, exception, message, args);
		}

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="context">The HttpContext to use.</param>
		/// <param name="message">The additional message format.</param>
		/// <param name="args">Any additional items to format the message with.</param>
		public static void LogError(HttpContext context, string message, params object[] args)
		{
			ErrorLoggerWeb.LogError(context, null, message, args);
		}

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="context">The HttpContext to use.</param>
		/// <param name="exception">The exception to log.</param>
		/// <param name="message">The additional message format.</param>
		/// <param name="args">Any additional items to format the message with.</param>
		public static void LogError(HttpContext context, Exception exception, string message, params object[] args)
		{
			// Build error text
			StringBuilder sb = new StringBuilder();
			try
			{
				if (context != null)
				{
					sb.AppendFormatLine("Error in: {0}", context.Request.Url);
					sb.AppendFormatLine("URL: {0}", new Uri(context.Request.Url, context.Request.RawUrl));
					sb.AppendFormatLine("HTTP method: {0}", context.Request.HttpMethod);
				}
				else
				{
					sb.AppendFormatLine("Error without HttpContext, probably seperate thread.");
				}

				sb.AppendLine();

				Exception ex = exception;
				while (ex != null)
				{
					sb.AppendFormatLine("Exception: {0}", ex.GetType().FullName);
					sb.AppendFormatLine("Message: {0}", ex.Message);
					sb.AppendFormatLine("Stack trace: {0}", ex.StackTrace);
					sb.AppendLine();

					// Go through full exception stack
					ex = ex.InnerException;
				}

				try
				{
					IManagableUser user = UserManager.CurrentUser;
					if (user != null)
					{
						sb.AppendFormatLine("Current user: {0} ({1})", user.Username, StringUtil.CombineWithSpace(user.Firstname, user.MiddleName, user.Lastname));
						sb.AppendLine();
					}
				}
				catch (Exception ex2)
				{
					sb.AppendFormatLine("Current user: {0} ({1})", "?", ex2.Message);
					sb.AppendLine();
				}

				if (context != null)
				{
					sb.AppendFormatLine("Referer: {0}", context.Request.UrlReferrer);
					sb.AppendFormatLine("IP address: {0}", context.Request.UserHostAddress);
					sb.AppendFormatLine("Hostname: {0}", context.Request.UserHostName);
					sb.AppendFormatLine("Languages: {0}", StringUtil.CombineWithSeperator(", ", context.Request.UserLanguages));
					sb.AppendFormatLine("User agent: {0}", context.Request.UserAgent);
				}

				sb.AppendLine();

				if (args == null || args.Length == 0)
				{
					sb.AppendFormat("Additional information: {0}", message);
				}
				else
				{
					sb.AppendFormat("Additional information: {0}", String.Format(message, args));
				}
			}
			catch
			{
				// Failure building error text
				return;
			}

			// Get error text
			string errorText = sb.ToString();

			// Write error to file
			bool writeFile = true;
			if (ErrorLoggerWeb.BeforeWriteFile != null && exception != null)
			{
				CancelExceptionEventArgs e = new CancelExceptionEventArgs(exception);
				ErrorLoggerWeb.BeforeWriteFile(context, e);
				writeFile = !e.Cancel;
			}

			if (writeFile)
			{
				try
				{
					// Write file
					string path = (context != null) ? context.Server.MapPath(RelativeErrorDirectory) : ErrorLoggerWeb.ResolvedErrorDirectory;
					if (!Directory.Exists(path))
					{
						Directory.CreateDirectory(path);
					}

					// Generate file name
					string fileName = DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now);
					if (exception != null)
					{
						fileName += "-" + exception.GetType().Name;
					}
					fileName += ".log";

					string filePath = Path.Combine(path, fileName);
					ErrorLoggerWeb.WriteFile(filePath, errorText);
				}
				catch
				{
					// Failure writing error file
				}
			}

			// Mail the error
			bool mailError = true;
			if (ErrorLoggerWeb.BeforeMailError != null && exception != null)
			{
				CancelExceptionEventArgs e = new CancelExceptionEventArgs(exception);
				ErrorLoggerWeb.BeforeMailError(context, e);
				mailError = !e.Cancel;
			}

			if (mailError)
			{
				try
				{
					// Send error log
					string[] recipientAddresses = EmailRecipientAddresses;
					if (recipientAddresses.Length != 0)
					{
						MailAddress from = new MailAddress(EmailSenderAddress, EmailSenderName);
						MailAddressCollection to = new MailAddressCollection();
						foreach (string recipientAddress in recipientAddresses)
						{
							to.Add(recipientAddress);
						}
						string subject = "Error: ";
						if (exception == null)
						{
							subject += new string(String.Format(message, args).TakeWhile(c => c != '\r' && c != '\n').ToArray());
						}
						else
						{
							subject += new string(exception.Message.TakeWhile(c => c != '\r' && c != '\n').ToArray());
						}

						ErrorLoggerWeb.MailError(from, to, subject, errorText);
					}
				}
				catch
				{
					// Failure sending error mail
				}
			}
		}

		/// <summary>
		/// Writes the file.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="errorText">The error text.</param>
		private static void WriteFile(string filePath, string errorText)
		{
			using (StreamWriter writer = File.CreateText(filePath))
			{
				writer.Write(errorText);
			}
		}

		/// <summary>
		/// Mails the error.
		/// </summary>
		/// <param name="from">From.</param>
		/// <param name="to">To.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="body">The body.</param>
		private static void MailError(MailAddress from, MailAddressCollection to, string subject, string body)
		{
			using (Dionysos.Net.Mail.MailMessage mailMessage = new Dionysos.Net.Mail.MailMessage())
			{
				// Set from
				mailMessage.From = from;

				// Set to
				foreach (MailAddress mailAddress in to)
				{
					mailMessage.To.Add(mailAddress);
				}

				// Set subject
				mailMessage.Subject = subject;

				// Set body
				mailMessage.Body = body;

				// Set misc options
				mailMessage.Priority = MailPriority.High;

				// Send mail
				MailUtil.SendMail(mailMessage);
			}
		}

		#endregion
	}

	/// <summary>
	/// Provides data for a cancelable exception event.
	/// </summary>
	public class CancelExceptionEventArgs : CancelEventArgs
	{
		#region Properties

		/// <summary>
		/// Gets the exception.
		/// </summary>
		public Exception Exception { get; private set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CancelExceptionEventArgs"/> class with the specified exception and the <see cref="Cancel"/> property set to <c>false</c>.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <exception cref="ArgumentNullException">exception is null.</exception>
		public CancelExceptionEventArgs(Exception exception)
			: this(exception, false)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="CancelExceptionEventArgs"/> class with the specified exception and the <see cref="Cancel"/> property set to the given value.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <param name="cancel"><c>true</c> to cancel the event; otherwise, <c>false</c>.</param>
		/// <exception cref="ArgumentNullException">exception is null.</exception>
		public CancelExceptionEventArgs(Exception exception, bool cancel)
			: base(cancel)
		{
			if (exception == null) throw new ArgumentNullException("exception");

			this.Exception = exception;
		}

		#endregion
	}
}
