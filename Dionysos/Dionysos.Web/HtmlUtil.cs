﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web
{
    public class HtmlUtil
    {
        #region Fields

        static HtmlUtil instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the HtmlUtil class if the instance has not been initialized yet
        /// </summary>
        public HtmlUtil()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new HtmlUtil();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static HtmlUtil instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Convert a string to a html string by replacing carriage line returns for break lines
        /// </summary>
        /// <param name="s">The string instance to convert to Html</param>
        /// <returns>A System.String instance containing the Html string</returns>
        public static string ConvertStringToHtml(string s)
        {
            return s.Replace("\r\n", "<br />");
        }

        #endregion
    }
}