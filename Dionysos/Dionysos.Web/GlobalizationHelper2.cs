﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Globalization;
using Dionysos.Web.UI;
using Dionysos.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Dionysos.Web.UI.WebControls;
using System.Web;

namespace Dionysos.Web
{
    /// <summary>
    /// Globalization Helper for Web
    /// </summary>
    public abstract class GlobalizationHelper2 : IGlobalizationHelper
    {
        /// <summary>
        /// Determine if Gui Translation is required, this is only when current language is not Dutch.
        /// </summary>
        public abstract bool TranslationRequired { get; }

        /// <summary>
        /// Retrieve the current TowLetterISOLanguageName from the Current UI Culture
        /// </summary>
        public static string CurrentUICultureLanguageCode
        {
            get
            {
                return Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.ToLower();
            }
        }

        /// <summary>
        /// Get the value form the AppSettings if we should Learn non available controls
        /// </summary>
        public bool AutoLearnNewControls
        {
            get
            {
                if (HttpContext.Current.Application["ExtraVestiging.Logic.EVAdmin.GlobalizationHelper.AutoLearnNewControls"] != null)
                    return (bool)HttpContext.Current.Application["ExtraVestiging.Logic.EVAdmin.GlobalizationHelper.AutoLearnNewControls"];
                else
                    return false;
            }
            set
            {
                HttpContext.Current.Application["ExtraVestiging.Logic.EVAdmin.GlobalizationHelper.AutoLearnNewControls"] = value;
            }
        }


        #region IGlobalizationHelper Members

        /// <summary>
        /// Localize all control that implement ILocalizableControl
        /// </summary>
        /// <param name="page"></param>
        public void LocalizeControls(System.Web.UI.Page page)
        {
            if (this.TranslationRequired)
            {
                if (page is PageDefault)
                {
                    // First Handle Page Controls
                    PageDefault pageDefault = page as PageDefault;
                    string languageCode = System.Threading.Thread.CurrentThread.CurrentUICulture.Name.Substring(0, 2);
                    pageDefault.CollectControls();
                    foreach (object control in pageDefault.ControlList)
                    {
                        if (control is ILocalizableControl)
                        {
                            var localizableControl = control as ILocalizableControl;
                            //Debug.WriteLine(((Control)control).ID);

                            // Translate all items
                            Dictionary<string, Translatable> translatableProperties = localizableControl.TranslatableProperties;
                            foreach (var item in translatableProperties)
                            {
                                item.Value.TranslationValue = Dionysos.Global.TranslationProvider.GetTranslation(item.Value.TranslationKey, languageCode, item.Value.TranslationValue, this.AutoLearnNewControls);
                            }

                            // Set the retrieved values
                            localizableControl.TranslatableProperties = translatableProperties;
                        }
                    }

                }
                else
                {
                    // No translation possible
                }
            }
        }


        #endregion
    }
}
