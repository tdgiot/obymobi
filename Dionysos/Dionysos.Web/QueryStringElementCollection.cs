using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Web;

namespace Dionysos.Web
{
	/// <summary>
	/// Represents a collection of query string elements.
	/// </summary>
	public class QueryStringElementCollection : List<QueryStringElement>
	{
		#region Properties

		/// <summary>
		/// Gets or sets the <see cref="QueryStringElement"/> with the specified name.
		/// </summary>
		public QueryStringElement this[string name]
		{
			get
			{
				return this.FirstOrDefault(qse => String.Equals(qse.Name, name, StringComparison.OrdinalIgnoreCase));
			}
			set
			{
				this.Add(name, value);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="QueryStringElementCollection"/> class.
		/// </summary>
		public QueryStringElementCollection()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="QueryStringElementCollection"/> class.
		/// </summary>
		/// <param name="url">The URL.</param>
		public QueryStringElementCollection(string url)
		{
			NameValueCollection values = HttpUtility.ParseQueryString(url.GetAllAfterFirstOccurenceOf('?', true));
			foreach (string key in values.AllKeys)
			{
				if (key == null) continue;

				this.Add(key, values[key]);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds the specified name and value.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		public void Add(string name, object value)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (String.IsNullOrEmpty(name)) throw new ArgumentException("Value cannot be empty.", "name");

			this.Add(new QueryStringElement(name, value));
		}

		/// <summary>
		/// Adds the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		public new void Add(QueryStringElement item)
		{
			if (item == null) throw new ArgumentNullException("item");

			this.Remove(item);
			base.Add(item);
		}

		/// <summary>
		/// Removes the query string element with the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>
		///   <c>true</c> if query string element is successfully removed; otherwise, false.
		/// </returns>
		public bool Remove(string name)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (String.IsNullOrEmpty(name)) throw new ArgumentException("Value cannot be empty.", "name");

			bool removed = false;
			QueryStringElement element = this[name];
			if (element != null)
			{
				removed = this.Remove(element);
			}

			return removed;
		}

		/// <summary>
		/// Determines whether a <see cref="QueryStringElement"/> with the specified name exists.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>
		///   <c>true</c> if a <see cref="QueryStringElement"/> with the specified name exists; otherwise, <c>false</c>.
		/// </returns>
		public bool Contains(string name)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (String.IsNullOrEmpty(name)) throw new ArgumentException("Value cannot be empty.", "name");

			return this[name] != null;
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			string value = String.Empty;

			if (this.Count > 0)
			{
				value = "?" + StringUtil.CombineWithSeperator("&", this.OrderBy(qse => qse.Name).Select(qse => qse.ToString()).ToArray());
			}

			return value;
		}

		#endregion
	}
}