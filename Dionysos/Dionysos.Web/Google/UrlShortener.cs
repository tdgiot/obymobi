﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Web.Script.Serialization;

namespace Dionysos.Web.Google
{
    /// <summary>
    /// Google Shortener API.
    /// </summary>
    public class UrlShortener
    {
        #region Fields

        /// <summary>
        /// The Google Shortener API URL.
        /// </summary>
        private Uri apiUrl = new Uri("https://www.googleapis.com/urlshortener/v1/url");

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Google API key.
        /// </summary>
        /// <value>
        /// The API key.
        /// </value>
        public string ApiKey { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlShortener"/> class.
        /// </summary>
        public UrlShortener()
            : this(Dionysos.Global.ConfigurationProvider.GetString(DionysosWebConfigurationConstants.GoogleGenericApiKey))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlShortener"/> class.
        /// </summary>
        /// <param name="apiKey">The API key.</param>
        public UrlShortener(string apiKey)
        {
            this.ApiKey = apiKey;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shortens the specified long URL.
        /// </summary>
        /// <param name="longUrl">The long URL.</param>
        /// <returns>The shorten response.</returns>
        public ShortenResponse Shorten(Uri longUrl)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Proxy = null;

                JavaScriptSerializer jss = new JavaScriptSerializer();
                Uri apiUrl = this.apiUrl;
                if (!String.IsNullOrEmpty(this.ApiKey))
                {
                    apiUrl = apiUrl.AddQueryString("key", this.ApiKey);
                }

                wc.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                return jss.Deserialize<ShortenResponse>(wc.UploadString(apiUrl, jss.Serialize(new
                {
                    longUrl = longUrl
                })));
            }
        }

        /// <summary>
        /// Expands the specified short URL.
        /// </summary>
        /// <param name="shortUrl">The short URL.</param>
        /// <returns>The expand response.</returns>
        public ExpandResponse Expand(Uri shortUrl)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Proxy = null;

                JavaScriptSerializer jss = new JavaScriptSerializer();
                Uri apiUrl = this.apiUrl;
                if (!String.IsNullOrEmpty(this.ApiKey))
                {
                    apiUrl = apiUrl.AddQueryString("key", this.ApiKey);
                }
                apiUrl = apiUrl.AddQueryString("shortUrl", shortUrl.ToString());

                return jss.Deserialize<ExpandResponse>(wc.DownloadString(apiUrl));
            }
        }

        #endregion

        #region Classes

        /// <summary>
        /// The shorten response.
        /// </summary>
        public class ShortenResponse
        {
            /// <summary>
            /// Gets or sets the kind.
            /// </summary>
            /// <value>
            /// The kind.
            /// </value>
            public string Kind { get; set; }

            /// <summary>
            /// Gets or sets the id.
            /// </summary>
            /// <value>
            /// The id.
            /// </value>
            public string Id { get; set; }

            /// <summary>
            /// Gets or sets the long URL.
            /// </summary>
            /// <value>
            /// The long URL.
            /// </value>
            public string LongUrl { get; set; }

            /// <summary>
            /// Returns a <see cref="System.String"/> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String"/> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return String.Format("Kind: {0}, Id: {1}, LongUrl: {2}", this.Kind, this.Id, this.LongUrl);
            }
        }

        /// <summary>
        /// The expand response.
        /// </summary>
        public class ExpandResponse : ShortenResponse
        {
            /// <summary>
            /// Gets or sets the status.
            /// </summary>
            /// <value>
            /// The status.
            /// </value>
            public string Status { get; set; }

            /// <summary>
            /// Returns a <see cref="System.String"/> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String"/> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return String.Format("Kind: {0}, Id: {1}, LongUrl: {2}, Status: {3}", this.Kind, this.Id, this.LongUrl, this.Status);
            }
        }

        #endregion
    }
}
