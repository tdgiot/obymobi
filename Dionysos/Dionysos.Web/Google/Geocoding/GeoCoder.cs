﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web;
using Dionysos.Geo;

namespace Dionysos.Web.Google.Geocoding
{
	public class GeoCoder
	{
		/// <summary>
		/// Looks up the address.
		/// </summary>
		/// <param name="address">The address that you want to geocode.</param>
		/// <param name="sensor">Indicates whether or not the geocoding request comes from a device with a location sensor.</param>
		/// <returns></returns>
		public static GeoResponse LookupAddress(string address, bool sensor = false, string apiKey = null)
		{
			GeoResponse result = null;

			// Build URL
			string url = String.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}&sensor={1}",
				HttpUtility.UrlEncode(address),
				sensor.ToString(CultureInfo.InvariantCulture).ToLowerInvariant());

			if (!String.IsNullOrEmpty(apiKey))
			{
				url += "&key=" + apiKey;
			}

			// Create request
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
			request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
			request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

			// Get response
			Stream responseStream = request.GetResponse().GetResponseStream();
			if (responseStream != null)
			{
				// Convert JSON to object
				DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(GeoResponse));
				result = (GeoResponse)serializer.ReadObject(responseStream);
			}

			return result;
		}

		/// <summary>
		/// Gets the coordinate.
		/// </summary>
		/// <param name="address">The address that you want to geocode.</param>
		/// <param name="sensor">Indicates whether or not the geocoding request comes from a device with a location sensor.</param>
		/// <param name="apiKey">The API key.</param>
		/// <returns>
		/// The best (first) result from the geocoder; otherwise, <c>GeoCoordinate.Empty</c>.
		/// </returns>
		public static GeoCoordinate GetCoordinate(string address, bool sensor = false, string apiKey = null)
		{
			GeoCoordinate coordinate = GeoCoordinate.Empty;

			try
			{
				if (String.IsNullOrEmpty(apiKey))
				{
					apiKey = ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleGenericApiKey);
				}

				GeoResponse geoResponse = GeoCoder.LookupAddress(address, sensor, apiKey);
				if (geoResponse != null &&
					String.Equals(geoResponse.Status, "OK", StringComparison.OrdinalIgnoreCase) &&
					geoResponse.Results.Length > 0 &&
					geoResponse.Results[0].Geometry != null &&
					geoResponse.Results[0].Geometry.Location != null)
				{
					coordinate = new GeoCoordinate(geoResponse.Results[0].Geometry.Location.Lat, geoResponse.Results[0].Geometry.Location.Lng);
				}
			}
			catch (Exception ex)
			{
				InformationLoggerWeb.LogInformation("Geocoder.GetCoordinate", "Error getting coordinate for address '{0}': {1}", address, ex.ToString());
			}

			return coordinate;
		}
	}
}
