﻿using System.Runtime.Serialization;

namespace Dionysos.Web.Google.AjaxSearch
{
    [DataContract]
    public class Page
    {
        [DataMember(Name = "start")]
        public string Start { get; internal set; }

        [DataMember(Name = "label")]
        public string Label { get; internal set; }

        public override string ToString()
        {
            return string.Format("Start: {0}; Label: {1}",
                string.IsNullOrEmpty(this.Start) ? "No start page!" : this.Start,
                string.IsNullOrEmpty(this.Label) ? "No label!" : this.Label);
        }
    }
}