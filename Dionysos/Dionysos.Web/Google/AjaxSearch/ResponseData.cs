﻿using System.Runtime.Serialization;

namespace Dionysos.Web.Google.AjaxSearch
{
    [DataContract(Name = "responseData")]
    public class ResponseData
    {
        [DataMember(Name = "results")]
        public Result[] Results;

        [DataMember(Name = "cursor")]
        public Cursor Cursor;
    }
}