﻿using System.Runtime.Serialization;

namespace Dionysos.Web.Google.AjaxSearch
{
    [DataContract]
    public class Cursor
    {
        [DataMember(Name = "pages")]
        public Page[] Pages { get; internal set; }

        [DataMember(Name = "estimatedResultCount")]
        public string EstimatedResultCount { get; internal set; }

        [DataMember(Name = "currentPageIndex")]
        public int CurrentPageIndex { get; internal set; }

        [DataMember(Name = "moreResultsUrl")]
        public string MoreResultsUrl { get; internal set; }

        public override string ToString()
        {
            return this.CurrentPageIndex < 0 ? "Invalid current page index!" :
                string.Format("Current page index: {0}; Estimated # results: {1}",
                    this.CurrentPageIndex,
                    this.EstimatedResultCount);
        }
    }
}