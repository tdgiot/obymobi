﻿using System.Runtime.Serialization;

namespace Dionysos.Web.Google.AjaxSearch
{
    [DataContract]
    public class Result
    {
        [DataMember(Name = "GsearchResultClass")]
        public string GSearchResultClass { get; internal set; }

        [DataMember(Name = "unescapedUrl")]
        public string UnescapedUrl { get; internal set; }

        [DataMember(Name = "url")]
        public string Url { get; internal set; }

        [DataMember(Name = "visibleUrl")]
        public string VisibleUrl { get; internal set; }

        [DataMember(Name = "cacheUrl")]
        public string CacheUrl { get; internal set; }

        [DataMember(Name = "title")]
        public string Title { get; internal set; }

        [DataMember(Name = "titleNoFormatting")]
        public string TitleNoFormatting { get; internal set; }

        [DataMember(Name = "content")]
        public string Content { get; internal set; }

        public override string ToString()
        {
            return string.Format("Title: {0}; Url: {1}",
                string.IsNullOrEmpty(this.TitleNoFormatting) ? "# No result title #" : this.TitleNoFormatting,
                string.IsNullOrEmpty(this.Url) ? "No url" : this.Url);
        }
    }
}