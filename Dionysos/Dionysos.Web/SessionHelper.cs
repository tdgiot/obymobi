using Dionysos.Web.Sessions;

namespace Dionysos.Web
{
	/// <summary>
	/// Class which represents a session
	/// </summary>
	public static class SessionHelper
	{
        private static ISessionStore SessionStore { get; set; }

	    static SessionHelper()
	    {
	        SessionStore = new DefaultSessionStore();
	    }

        /// <summary>
        /// Use other type of SessionStore. By default it's using DefaultSessionStore.
        /// Implement ISessionStore to create your own
        /// </summary>
        /// <param name="sessionStore"></param>
	    public static void SetSessionStore(ISessionStore sessionStore)
	    {
	        SessionStore = sessionStore;
	    }

		#region Properties

		/// <summary>
		/// Gets whether HttpContext.Current.Session is not null.
		/// </summary>
		public static bool HasSession
		{
			get { return SessionStore.HasSession; }
		}

	    public static string SessionID
	    {
	        get { return SessionStore.SessionID; }
	    }

		#endregion

		#region Static Methods

		/// <summary>
		/// Tries to get the value from the Session using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the Session value.</param>		
		/// <param name="value">When this method returns true, contains the value retrieved from the Session; otherwise returns the default value for the specified type.</param>
		/// <returns>true if the key and corresponding value exists in the Cache; otherwise, false.</returns>
		public static bool TryGetValue<T>(string key, out T value)
		{
		    return SessionStore.TryGetValue(key, out value);
		}

		/// <summary>
		/// Gets the value from the Session using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the Session value.</param>
		/// <returns>Returns the value, if found and type matches; otherwise returns the default value.</returns>
		public static T GetValue<T>(string key)
		{
		    return SessionStore.GetValue<T>(key);
		}

		/// <summary>
		/// Indicates whether the key exists in the Session and is of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of the value to check.</typeparam>
		/// <param name="key">The key of the Session value.</param>		
		/// <returns>true if the value exists in the Session and is of the specified type; otherwise false.</returns>
		public static bool HasValue<T>(string key)
		{
            return SessionStore.HasValue<T>(key);
		}

		/// <summary>
		/// Tries to set the value to the Session using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="key">The key for the Session value.</param>
		/// <param name="value">The value to set.</param>
		/// <returns>true if Session value is set; false if Session value is not set or removed.</returns>
		public static bool SetValue<T>(string key, T value)
		{
		    return SessionStore.SetValue(key, value);
		}

		/// <summary>
		/// Removes the value with the specified key from the Session.
		/// </summary>
		/// <param name="key">The key for the Session value</param>
		/// <returns>true if Session value is removed; otherwise false.</returns>
		public static bool RemoveValue(string key)
		{
            return SessionStore.RemoveValue(key);
		}

		#endregion

	}
}