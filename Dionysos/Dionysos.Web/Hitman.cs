﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Dionysos.Web
{
	/// <summary>
	/// Your personal hitman always waiting to kill the process!
	/// </summary>
	public class Hitman
	{
        public static StringBuilder Text = new StringBuilder();

		/// <summary>
		/// Kill the process!
		/// </summary>
		/// <param name="message">Message</param>
		/// <param name="args">Arguments</param>
		public static void Kill(string message, params object[] args)
		{
            Kill(string.Format(message, args));
		}

		/// <summary>
		/// Kill the process!
		/// </summary>
		/// <param name="message">Message</param>		
		public static void Kill(string message)
		{
			HttpContext.Current.Response.Clear();
			HttpContext.Current.Response.Write(message);
			HttpContext.Current.Response.End();
		}

        /// <summary>
        /// Kill the process and write the contents of Hitman.Text
        /// </summary>
        public static void Kill()
        {
            Kill(Text.ToString());
        }

        public static void AddToText(string message, params object[] args)
        {
            Hitman.Text.Append(string.Format(message, args) + "<br>");
        }

	}
}
