﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Dionysos.Web.XmlSitemap
{
	[XmlRoot("urlset", Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")]
	public class Sitemap
	{
		#region Fields

		/// <summary>
		/// The map.
		/// </summary>
		private readonly List<Location> map;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the locations.
		/// </summary>
		/// <value>
		/// The locations.
		/// </value>
		[XmlElement("url")]
		public Location[] Locations
		{
			get
			{
				return this.map.ToArray();
			}
			set
			{
				this.map.Clear();

				if (value != null)
				{
					this.map.AddRange(value);
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Sitemap"/> class.
		/// </summary>
		public Sitemap()
		{
			this.map = new List<Location>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		public void Add(Location item)
		{
			this.map.Add(item);
		}

		/// <summary>
		/// Adds the specified URL.
		/// </summary>
		/// <param name="url">The URL.</param>
		public void Add(string url)
		{
			this.Add(new Location(url));
		}

		/// <summary>
		/// Adds the specified URL.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="lastModified">The last modified date.</param>
		public void Add(string url, DateTime? lastModified)
		{
			this.Add(new Location(url, lastModified));
		}

		/// <summary>
		/// Adds the range.
		/// </summary>
		/// <param name="items">The items.</param>
		public void AddRange(IEnumerable<Location> items)
		{
			foreach (Location item in items)
			{
				this.Add(item);
			}
		}

		#endregion
	}

}
