﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.XmlSitemap
{
	public enum ChangeFrequency
	{
		Always,
		Hourly,
		Daily,
		Weekly,
		Monthly,
		Yearly,
		Never
	}
}
