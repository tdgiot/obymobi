﻿using System;
using System.Xml.Serialization;

namespace Dionysos.Web.XmlSitemap
{
	/// <summary>
	/// Represents an XML sitemap location.
	/// </summary>
	public class Location
	{
		#region Properties

		/// <summary>
		/// Gets or sets the URL.
		/// </summary>
		/// <value>
		/// The URL.
		/// </value>
		[XmlElement("loc")]
		public string Url { get; set; }

		/// <summary>
		/// Gets or sets the last modified date.
		/// </summary>
		/// <value>
		/// The last modified date.
		/// </value>
		[XmlElement("lastmod")]
		public DateTime? LastModified { get; set; }

		/// <summary>
		/// Gets or sets the change frequency.
		/// </summary>
		/// <value>
		/// The change frequency.
		/// </value>
		[XmlElement("changefreq")]
		public ChangeFrequency? ChangeFrequency { get; set; }

		/// <summary>
		/// Gets or sets the priority.
		/// </summary>
		/// <value>
		/// The priority.
		/// </value>
		[XmlElement("priority")]
		public double? Priority { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Location" /> class.
		/// </summary>
		public Location()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="Location" /> class.
		/// </summary>
		/// <param name="url">The URL.</param>
		public Location(string url)
			: this(url, null, null, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="Location" /> class.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="lastModified">The last modified date.</param>
		public Location(string url, DateTime? lastModified)
			: this(url, lastModified, null, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="Location" /> class.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="lastModified">The last modified date.</param>
		/// <param name="changeFrequency">The change frequency.</param>
		public Location(string url, DateTime? lastModified, ChangeFrequency? changeFrequency)
			: this(url, lastModified, changeFrequency, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="Location" /> class.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="lastModified">The last modified date.</param>
		/// <param name="changeFrequency">The change frequency.</param>
		/// <param name="priority">The priority.</param>
		public Location(string url, DateTime? lastModified, ChangeFrequency? changeFrequency, double? priority)
		{
			this.Url = url;
			this.LastModified = lastModified;
			this.ChangeFrequency = changeFrequency;
			this.Priority = priority;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Indicates whether the last modified date must be serialized.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the last modified date must be serialized, otherwise; <c>false</c>.
		/// </returns>
		public bool ShouldSerializeLastModified()
		{
			return this.LastModified.HasValue;
		}

		/// <summary>
		/// Indicates whether the change frequency must be serialized.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the change frequency must be serialized, otherwise; <c>false</c>.
		/// </returns>
		public bool ShouldSerializeChangeFrequency()
		{
			return this.ChangeFrequency.HasValue;
		}

		/// <summary>
		/// Indicates whether the priority must be serialized.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the priority must be serialized, otherwise; <c>false</c>.
		/// </returns>
		public bool ShouldSerializePriority()
		{
			return this.Priority.HasValue;
		}

		#endregion
	}
}