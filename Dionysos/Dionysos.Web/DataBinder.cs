using System;
using System.ComponentModel;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Reflection;
using Dionysos.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Interfaces;
using System.Globalization;

namespace Dionysos.Web
{
	/// <summary>
	/// Static class used for the binding of datasources to controls!!!
	/// </summary>
	public class DataBinder
	{
		#region Fields

		private object control;
		private string controlProperty;
		private object dataSource;
		private string dataSourceProperty;

		private PropertyInfo controlPropertyInfo;
		private PropertyInfo dataSourcePropertyInfo;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Lynx_media.Web.DataBinder class
		/// </summary>
		/// <param name="control">The control to databind</param>
		/// <param name="controlProperty">The name of the property of the control to databind</param>
		/// <param name="dataSource">The datasource to databind</param>
		/// <param name="dataSourceProperty">The property of the datasource to databind</param>
		public DataBinder(object control, string controlProperty, object dataSource, string dataSourceProperty)
		{
			this.control = control;
			this.controlProperty = controlProperty;
			this.dataSource = dataSource;
			this.dataSourceProperty = dataSourceProperty;

			System.Type controlType = control.GetType();
			System.Type dataSourceType = dataSource.GetType();

			// Get the PropertyInfo now
			// rather than in the event handler.
			// GK DirtyWorkAround
			if (controlType.GetMember(controlProperty).Length > 1)
			{
				PropertyInfo[] properties = controlType.GetProperties();
				for (int i = 0; i < properties.Length; i++)
				{
					if (properties[i].Name == controlProperty)
					{
						controlPropertyInfo = properties[i];
						break;
					}
				}

			}
			else
			{
				// Get property info from control type
				controlPropertyInfo = controlType.GetProperty(controlProperty, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);
				if (controlPropertyInfo == null)
				{
					// Try inherited members
					controlPropertyInfo = controlType.GetProperty(controlProperty, BindingFlags.Instance | BindingFlags.Public);
				}
			}

			// Get property info from the data source type
			dataSourcePropertyInfo = dataSourceType.GetProperty(dataSourceProperty, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);
			if (dataSourcePropertyInfo == null)
			{
				// Try inherited members
				dataSourcePropertyInfo = dataSourceType.GetProperty(dataSourceProperty, BindingFlags.Instance | BindingFlags.Public);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Binds a System.Web.UI.Control instance to a datasource
		/// </summary>
		/// <param name="control">The System.Object instance to bind</param>
		/// <param name="controlProperty">The property of the control to bind to</param>
		/// <param name="datasource">The datasource instance of type System.Object</param>
		/// <param name="datasourceProperty">The property of the datasource to bind to</param>
		/// <param name="isPostback">Flag indicating whether the page is being loaded in response to a client postback</param>
		public static void Bind(object control, string controlProperty, object datasource, string datasourceProperty, bool isPostback)
		{
			if (Instance.ArgumentIsEmpty(control, "control"))
			{
				// control is empty
			}
			else if (Instance.ArgumentIsEmpty(controlProperty, "controlProperty"))
			{
				// controlProperty is empty
			}
			else if (Instance.ArgumentIsEmpty(datasource, "datasource"))
			{
				// datasource is empty
			}
			else if (Instance.ArgumentIsEmpty(datasourceProperty, "datasourceProperty"))
			{
				// datasourceProperty is empty
			}
			else
			{
				//Dionysos.Diagnostics.Debug.WriteLine(string.Format("Binding controlproperty {0} to datasourceproperty {1}", controlProperty, datasourceProperty));

				// Create the string names of the events
				string controlEvent = controlProperty + "Changed";              // Example: ValueChanged
				string datasourceEvent = "PropertyChanged";

				// Get the types from the binding object combination
				System.Type controlType = control.GetType();
				System.Type datasourceType = datasource.GetType();

				// Get the eventinfo object using the event names
				EventInfo controlEventInfo = controlType.GetEvent(controlEvent);
				EventInfo datasourceEventInfo = datasourceType.GetEvent(datasourceEvent);

				if (Instance.ArgumentIsEmpty(controlEventInfo, "controlEventInfo"))
				{
					// controlEventInfo is empty
				}
				else if (Instance.ArgumentIsEmpty(datasourceEventInfo, "dataSourceEventInfo"))
				{
					// datasourceEventInfo is empty
				}
				else
				{
					// Create and initialize a bindhelper object
					DataBinder dataBinder = new DataBinder(control, controlProperty, datasource, datasourceProperty);

					// The signature of the event
					// delegate must be of the form
					// (object, EventArgs).
					// Although events don't have to be
					// of this signature, this is a good
					// reason to comply with the .NET guidelines.

					// Unfortunately, .NET 1.1 does not
					// handle event signatures that are
					// derived from EventArgs when programatically
					// adding a generic handler.
					// This "bug" is corrected in .NET 2.0!

					// Add an event handler to the ValueChanged event of the control
					controlEventInfo.AddEventHandler(control, new EventHandler(dataBinder.ControlChanged));

					// Add an event handler to the <FieldName>Changed event of the datasource
					datasourceEventInfo.AddEventHandler(datasource, new PropertyChangedEventHandler(dataBinder.DataSourceChanged));

					// Add field length restriction
					// GK Quick Fix
					if (datasource is SD.LLBLGen.Pro.ORMSupportClasses.IEntity && control is Dionysos.Web.UI.WebControls.TextBoxString)
					{
						SD.LLBLGen.Pro.ORMSupportClasses.IEntity datasourceIEntity = datasource as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
						Dionysos.Web.UI.WebControls.TextBoxString tb = control as Dionysos.Web.UI.WebControls.TextBoxString;
						if (datasourceIEntity != null && tb != null)
						{
							if (!Instance.Empty(datasourceIEntity.Fields[datasourceProperty]))
								tb.MaxLength = datasourceIEntity.Fields[datasourceProperty].MaxLength;
						}
					}

					if (!isPostback)
					{
						dataBinder.DataSourceChanged(dataBinder, new PropertyChangedEventArgs(datasourceProperty));
					}
					else
					{
						// GK Quick fix, labels are ReadOnly
						if (control is Dionysos.Web.UI.WebControls.Label)
						{
						}
						else
						{
							dataBinder.ControlChanged(dataBinder, null);
						}
					}

					//if (control is Lynx_media.Web.UI.WebControls.TextBox)
					//{
					//    ((Lynx_media.Web.UI.WebControls.TextBox)control).ValueChanged += new EventHandler(dataBinder.OnLeave);
					//}
					//if (control is Lynx_media.Web.UI.WebControls.TextBox)
					//{
					//    ((Lynx_media.Web.UI.WebControls.TextBox)control).TextChanged += new EventHandler(dataBinder.OnLeave);
					//}
				}
			}
		}

		/// <summary>
		/// Databinds the specified control to the specified datasource
		/// </summary>
		/// <param name="control">Control to databind</param>
		/// <param name="datasource">Datasource to bind the controls to</param>
		/// <param name="isPostBack">Flag which indicates whether the current response is a postback</param>
		public static void DataBindControlRecursively(Control control, object datasource, bool isPostBack)
		{
			// Walk through the child controls of the control to databind
			for (int i = 0; i < control.Controls.Count; i++)
			{
				Control childControl = control.Controls[i];

				// Check if a control has controls it self
				// GK Below  || childCtrl.Controls.Count > 0 has been added because one control had HasControls on false, while it contained controls.
				if (childControl.HasControls() || childControl.Controls.Count > 0)
				{
					// We need special logic for SubPanels as they (might) have another datasource
					if (childControl is SubPanelLLBLGenEntity)
					{
						SubPanelLLBLGenEntity subPanelLLBLGenEntity = childControl as SubPanelLLBLGenEntity;
						if (subPanelLLBLGenEntity.UseDataBinding)
						{
							DataBindControlRecursively(childControl, subPanelLLBLGenEntity.DataSource, isPostBack);
						}
					}
					else
					{
						DataBindControlRecursively(childControl, datasource, isPostBack);
					}
				}

				// Actual binding of the Control it self (we come here after all the recursions for the childs.
				if (childControl is ISelfBindable)
				{
					ISelfBindable bindable = childControl as ISelfBindable;
					if (bindable.UseDataBinding)
					{
						bindable.Bind();
					}
				}
				else if (childControl is IBindable)
				{
					IBindable bindable = childControl as IBindable;
					if (bindable.UseDataBinding)
					{
						// Get the ID of the control without the prefix (remove prefix by removing all lowercase till the first upper)
						// Examples: hlFirstname, tbLastname, rbGender, ddlCategory, etc.
						string controlId = childControl.ID;
						int upperCase = StringUtil.IndexOfFirstUpper(controlId);
						string memberName = string.Empty;

                        object childDatasource = datasource;

						if (upperCase > -1)
						{
							memberName = controlId.Substring(upperCase);
						}

                        // If the memberName contains an underscore use the first part as a "sub-dataSource"
                        if (memberName.Contains("_"))
                        {
                            string[] split = memberName.Split('_');
                            string subEntity = split[0];
                            memberName = split[1];

                            if (!Instance.Empty(subEntity))
                            {
                                if (!subEntity.EndsWith("Entity"))
                                {
                                    subEntity += "Entity";
                                }
                                
                                if (Member.HasMember(datasource, subEntity))
                                {
                                    var dsPropertyInfo = datasource.GetType().GetProperty(subEntity);
                                    if (dsPropertyInfo == null)
                                    {
                                        string datasourceName = datasource == null ? "null" : datasource.ToString();
                                        throw new TechnicalException("Unable to find sub-dataSource '{0}' on main dataSource {1} for control {2} ({3}). Make sure the sub-dataSource naming is correct.", subEntity, datasourceName, controlId, control.GetType().FullName);
                                    }
                                    childDatasource = dsPropertyInfo.GetValue(datasource);
                                }
                            }
                        }

                        // Check whether a property exists on the DataSource
						// according to the name of the control
						try
						{
                            if (childControl.GetType().ToString().Equals("Dionysos.Web.UI.DevExControls.ComboBoxEnum"))
                            {
                                Member.InvokeMethod(childControl, "SetEnumType", new Type[]{typeof(Type)}, new object[] {Member.GetPropertyType(childDatasource, memberName)});
                            }

							if (Instance.Empty(memberName))
							{
								// ID of the control is empty
							}
							else if (Member.HasMember(childDatasource, memberName))
							{
								// If the property exists, data bind the control
								// to the property on the datasource
								DataBinder.Bind(childControl, "Value", childDatasource, memberName, isPostBack);
							}
						}
						catch (Exception ex)
						{
							string controlName = controlId;
							if (childControl.Parent != null)
								controlName = childControl.Parent.GetType().FullName;

							string datasourceName = childDatasource == null ? "null" : childDatasource.ToString();

							throw new Dionysos.TechnicalException(ex, "Problem with binding: {0} on dataSource {1} on control {2}: {3}.\r\n If it's a Subpanel, make sure it's added with a GuiParent BEFORE the OnInit of the page, otherwise the DataSource of the Subpanel doesn't get set. If it's not on a SubPanel make sure the local database you're using is up to date with the MasterEV.", controlId, datasourceName, control.GetType().FullName, ex.Message);
						}
					}
				}
			}
		}

		#endregion

		#region Event handlers

		private void ControlChanged(object sender, EventArgs e)
		{
			// Get the value from the control and the datasource
			object controlValue = controlPropertyInfo.GetValue(control, null);
			object dataSourceValue = dataSourcePropertyInfo.GetValue(dataSource, null);

			if (controlValue != dataSourceValue &&
				dataSourcePropertyInfo.CanWrite)
			{
				if (controlValue == null)
				{
					// No actions on an null value
				}
				else
				{
					// Set the value from the control to the datasource
					if ((controlValue is DateTime || dataSourcePropertyInfo.PropertyType.UnderlyingSystemType == typeof(DateTime?)) &&
						(DateTime)controlValue == DateTime.MinValue)
					{
						controlValue = null;
					}

					if ((control is Dionysos.Web.UI.WebControls.DropDownListLLBLGenEntityCollection || control is Dionysos.Web.UI.WebControls.DropDownListInt) &&
						(int)controlValue <= 0 &&
						dataSourcePropertyInfo.PropertyType.UnderlyingSystemType == typeof(int?))
					{
						controlValue = null;
					}

					if (controlValue is string &&
						dataSourcePropertyInfo.PropertyType.UnderlyingSystemType == typeof(int?) &&
						((string)controlValue).Length == 0)
					{
						controlValue = null;
					}

					//// MDB HACK, should be fixed properly
					//string type = control.GetType().ToString();
					//if (type == "Dionysos.Web.UI.DevExControls.ComboBoxInt" ||
					//	type == "Dionysos.Web.UI.DevExControls.ComboBoxEntityCollection" ||
					//	type == "Dionysos.Web.UI.DevExControls.ComboBoxLLBLGenEntityCollection")
					//{
					//	int? value = (int?)Dionysos.Reflection.Member.InvokeProperty(control, "Value");
					//	if (value.HasValue)
					//	{
					//		if (value == 0)
					//			controlValue = null;
					//	}
					//	else
					//		controlValue = null;

					//	//Dionysos.Web.UI.DevExControls.ComboBoxInt controlAsComboBoxInt = control as Dionysos.Web.UI.DevExControls.ComboBoxInt;
					//	//if (controlAsComboBoxInt.Value.HasValue && controlAsComboBoxInt.Value == 0)
					//	//    controlValue = null;
					//	//else if (!controlAsComboBoxInt.Value.HasValue)
					//	//    controlValue = null;
					//}

                    if (dataSourcePropertyInfo.PropertyType.IsGenericType && dataSourcePropertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) 
                    {
                        Type[] genericTypeArgs = dataSourcePropertyInfo.PropertyType.GetGenericArguments();
                        if (genericTypeArgs != null && genericTypeArgs.Length == 1 && genericTypeArgs[0].IsEnum)
                        {
                            try 
                            { controlValue = Enum.Parse(genericTypeArgs[0], controlValue.ToString()); }
                            catch 
                            { }
                        }
                    }

					if (dataSourcePropertyInfo.PropertyType == typeof(string))
					{
						// Trim text if enabled and control is a TextBox
						if (controlValue is string &&
							control is TextBoxString &&
							ConfigurationManager.GetBool(DionysosWebConfigurationConstants.TrimText))
						{
							controlValue = ((string)controlValue).Trim();
						}

						// Parse control value type to datasource value (string)
						Type controlPropertyType = controlPropertyInfo.PropertyType;
						if (controlPropertyType == typeof(DateTime))
						{
							controlValue = ((DateTime)controlValue).ToString(CultureInfo.InvariantCulture);
						}
						else if (controlPropertyType == typeof(DateTime?))
						{
							controlValue = ((DateTime?)controlValue).GetValueOrDefault().ToString(CultureInfo.InvariantCulture);
						}
						else if (controlPropertyType == typeof(Decimal))
						{
							controlValue = ((Decimal)controlValue).ToString(CultureInfo.InvariantCulture);
						}
						else if (controlPropertyType == typeof(Decimal?))
						{
							controlValue = ((Decimal?)controlValue).GetValueOrDefault().ToString(CultureInfo.InvariantCulture);
						}
						else if (controlPropertyType == typeof(Int32))
						{
							controlValue = ((Int32)controlValue).ToString(CultureInfo.InvariantCulture);
						}
						else if (controlPropertyType == typeof(Int32?))
						{
							controlValue = ((Int32?)controlValue).GetValueOrDefault().ToString(CultureInfo.InvariantCulture);
						}
						else if (controlPropertyType == typeof(Boolean))
						{
							controlValue = ((Boolean)controlValue).ToString(CultureInfo.InvariantCulture);
						}
					}
				}

				try
				{
					// GK HACK ?
					// if null, try if it's an entity
					if (controlValue == null &&
						dataSource is SD.LLBLGen.Pro.ORMSupportClasses.IEntity)
					{
						SD.LLBLGen.Pro.ORMSupportClasses.IEntity data = dataSource as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
						data.SetNewFieldValue(dataSourcePropertyInfo.Name, controlValue);
					}
					else
					{
						dataSourcePropertyInfo.SetValue(dataSource, controlValue, null);
					}
				}
				catch (Exception innerException)
				{
					Control errorCtrl = control as Control;
					string id = "control could not be casted to Control";
					if (errorCtrl != null) id = errorCtrl.ID;
					throw new Exception(String.Format("ControlChanged: Error on control '{0}' with value '{1}'", id, controlValue), innerException);
				}
			}
		}

		private void DataSourceChanged(object sender, PropertyChangedEventArgs e)
		{
			// No need for binding new datasources + this will break the behaviour of Combobox loading their values from the querystring, since it
			// will bind non nullable int fields with 0 (zero).
			//if (this.DataSourceIsNew == null || !this.DataSourceIsNew.Value)
			//{
			if (this.dataSourceProperty == e.PropertyName)
			{
				// Get the value from the control and the datasource
				object controlValue = controlPropertyInfo.GetValue(control, null);
				object dataSourceValue = dataSourcePropertyInfo.GetValue(dataSource, null);

				if (controlValue != dataSourceValue && dataSourceValue != null)
				{
					//Dionysos.Diagnostics.Debug.WriteLine(string.Format("Setting value {0} to controlproperty {1}", dataSourceValue, controlPropertyInfo.Name));
					if ((this.DataSourceIsNew == null || this.DataSourceIsNew.Value) && dataSourceValue is int)
					{
						// GK Don't bind new datasources of the type INT, because they default to 0
					}
					else
					{
						if (dataSourcePropertyInfo.PropertyType == typeof(string))
						{
							string dataSourceValueAsString = (string)dataSourceValue;

							// Trim text if enabled and control is a TextBox
							if (control is TextBoxString &&
								ConfigurationManager.GetBool(DionysosWebConfigurationConstants.TrimText))
							{
								dataSourceValue = dataSourceValueAsString.Trim();
							}

							// Parse datasource value (string) to control value type
							Type controlPropertyType = controlPropertyInfo.PropertyType;
							if (controlPropertyType == typeof(DateTime) || controlPropertyType == typeof(DateTime?))
							{
								DateTime parsed;
								if (DateTime.TryParse(dataSourceValueAsString, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsed))
								{
									dataSourceValue = parsed;
								}
								else
								{
									dataSourceValue = null;
								}
							}
							else if (controlPropertyType == typeof(Decimal) || controlPropertyType == typeof(Decimal?))
							{
								decimal parsed;
								if (Decimal.TryParse(dataSourceValueAsString, NumberStyles.Number, CultureInfo.InvariantCulture, out parsed))
								{
									dataSourceValue = parsed;
								}
								else
								{
									dataSourceValue = null;
								}
							}
							else if (controlPropertyType == typeof(Int32) || controlPropertyType == typeof(Int32?))
							{
								int parsed;
								if (Int32.TryParse(dataSourceValueAsString, NumberStyles.Integer, CultureInfo.InvariantCulture, out parsed))
								{
									dataSourceValue = parsed;
								}
								else
								{
									dataSourceValue = null;
								}
							}
							else if (controlPropertyType == typeof(Boolean))
							{
								bool parsed;
								if (Boolean.TryParse(dataSourceValueAsString, out parsed))
								{
									dataSourceValue = parsed;
								}
								else
								{
									dataSourceValue = null;
								}
							}
						}
                        else if (dataSourcePropertyInfo.PropertyType.IsEnum) // Enum
                        {
                            try
                            {
                                if (dataSourcePropertyInfo.PropertyType.IsEnumDefined(dataSourceValue))
                                    dataSourceValue = (int)dataSourceValue;
                                else
                                    dataSourceValue = null;
                            }
                            catch
                            {
                                dataSourceValue = null;
                            }
                        }
                        else if (dataSourcePropertyInfo.PropertyType.IsGenericType && dataSourcePropertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) // Nullable enum
                        {
                            Type[] genericTypeArgs = dataSourcePropertyInfo.PropertyType.GetGenericArguments();
                            if (genericTypeArgs != null && genericTypeArgs.Length == 1 && genericTypeArgs[0].IsEnum)
                            {
                                dataSourceValue = (int)dataSourceValue;
                            }
                        }

						// Set the value from the datasource to the control
						try
						{
							controlPropertyInfo.SetValue(control, dataSourceValue, null);
						}
						catch (Exception ex)
						{
							throw new TechnicalException(ex, "DataBinding mislukt voor control '{0}', te binden waarde: '{1}' van type '{2}', melding: {3}", ((Control)control).ID, dataSourceValue, dataSourceValue.GetType(), ex.Message);
						}
					}
				}
			}
			//}
		}

		#endregion

		#region Properties

		public bool? DataSourceIsNew
		{
			get
			{
				IEntity dsAsEntity = this.dataSource as IEntity;
				if (dsAsEntity == null)
					return null;
				else
					return dsAsEntity.IsNew;
			}
		}

		#endregion
	}
}

