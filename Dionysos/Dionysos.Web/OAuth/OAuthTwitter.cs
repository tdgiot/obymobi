﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Web;
using System.Net;
using System.IO;

namespace Dionysos.Web.OAuth
{
	/// <summary>
	/// OAuth Twitter requests.
	/// </summary>
	public class OAuthTwitter : OAuthBase
	{
		#region Enumerations

		/// <summary>
		/// The methods available in the Twitter API.
		/// </summary>
		public enum Method
		{
			/// <summary>
			/// Use GET for read-only requests.
			/// </summary>
			GET,
			/// <summary>
			/// Use POST for write requests.
			/// </summary>
			POST
		}

		#endregion

		#region Fields

		/// <summary>
		/// The URI to obtain a request token.
		/// </summary>
		public const string REQUEST_TOKEN = "https://api.twitter.com/oauth/request_token";

		/// <summary>
		/// The URI to authorize a user.
		/// </summary>
        public const string AUTHORIZE = "https://api.twitter.com/oauth/authorize";

		/// <summary>
		/// The URI to obtain a access token.
		/// </summary>
        public const string ACCESS_TOKEN = "https://api.twitter.com/oauth/access_token";

		private string consumerKey = String.Empty;
		private string consumerSecret = String.Empty;
		private string token = String.Empty;
		private string tokenSecret = String.Empty;
        private string userId = String.Empty;
        private string screenName = String.Empty;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the consumer key.
		/// </summary>
		public string ConsumerKey
		{
			get
			{
				return this.consumerKey;
			}
			set
			{
				this.consumerKey = value ?? String.Empty;
			}
		}

		/// <summary>
		/// Gets or sets the consumer secret.
		/// </summary>
		public string ConsumerSecret
		{
			get
			{
				return this.consumerSecret;
			}
			set
			{
				this.consumerSecret = value ?? String.Empty;
			}
		}

		/// <summary>
		/// Gets or sets the access token.
		/// </summary>
		public string Token
		{
			get
			{
				return this.token;
			}
			set
			{
				this.token = value ?? String.Empty;
			}
		}

		/// <summary>
		/// Gets or sets the access token secret.
		/// </summary>
		public string TokenSecret
		{
			get
			{
				return this.tokenSecret;
			}
			set
			{
				this.tokenSecret = value ?? String.Empty;
			}
		}

        /// Gets or sets the user ID
        /// </summary>
        public string UserId
        {
            get
            {
                return this.userId;
            }
            set
            {
                this.userId = value ?? String.Empty;
            }
        }

        /// Gets or sets the screen name
        /// </summary>
        public string ScreenName
        {
            get
            {
                return this.screenName;
            }
            set
            {
                this.screenName = value ?? String.Empty;
            }
        }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new instance of the OAuthTwitter class.
		/// </summary>
		/// <param name="consumerKey">The consumer key to authorise the requests for.</param>
		/// <param name="consumerSecret">The consumer secret to authorise the requests for.</param>
		public OAuthTwitter(string consumerKey, string consumerSecret)
			: this(consumerKey, consumerSecret, null, null)
		{ }

		/// <summary>
		/// Creates a new instance of the OAuthTwitter class.
		/// </summary>
		/// <param name="consumerKey">The consumer key to authorise the requests for.</param>
		/// <param name="consumerSecret">The consumer secret to authorise the requests for.</param>
		/// <param name="token">The access token for the authorised user.</param>
		/// <param name="tokenSecret">The access token secret for the authorised user.</param>
		public OAuthTwitter(string consumerKey, string consumerSecret, string token, string tokenSecret)
		{
			this.ConsumerKey = consumerKey;
			this.ConsumerSecret = consumerSecret;
			this.Token = token;
			this.TokenSecret = tokenSecret;
		}

		#endregion

		/// <summary>
		/// Get the link to Twitter's authorization page for this application.
		/// </summary>
		/// <param name="callbackUrl">The callback url to return when authorised.</param>
		/// <returns>The url with a valid request token or null.</returns>
		public string GetAuthorizationUrl(string callbackUrl)
		{
			string ret = null;

			string response = this.OAuthWebRequest(Method.GET, REQUEST_TOKEN, String.Empty);
			if (!String.IsNullOrEmpty(response))
			{
				// Response contains token and token secret. We only need the token.
				NameValueCollection qs = HttpUtility.ParseQueryString(response);
				if (qs["oauth_token"] != null)
				{
					ret = AUTHORIZE + "?oauth_token=" + qs["oauth_token"];

					if (qs["oauth_token_secret"] != null)
					{
						this.TokenSecret = qs["oauth_token_secret"];
					}

					if (!String.IsNullOrEmpty(callbackUrl))
					{
						ret += "&oauth_callback=" + callbackUrl;
					}
				}
			}

			return ret;
		}

		/// <summary>
		/// Exchange the request token for an access token.
		/// </summary>
		/// <param name="authToken">The oauth_token is supplied by Twitter's authorization page following the callback.</param>
		public void GetAccessToken(string authToken)
		{
			this.Token = authToken;

			string response = this.OAuthWebRequest(Method.GET, ACCESS_TOKEN, String.Empty);

			if (!String.IsNullOrEmpty(response))
			{
				// Store the Token and Token Secret
				NameValueCollection qs = HttpUtility.ParseQueryString(response);
				if (qs["oauth_token"] != null)
				{
					this.Token = qs["oauth_token"];
				}
				if (qs["oauth_token_secret"] != null)
				{
					this.TokenSecret = qs["oauth_token_secret"];
				}
                if (qs["user_id"] != null)
                {
                    this.UserId = qs["user_id"];
                }
                if (qs["screen_name"] != null)
                {
                    this.ScreenName = qs["screen_name"];
                }
			}
		}

        /// <summary>
        /// Exchange the request token and verifier for an access token.
        /// </summary>
        /// <param name="authToken">The oauth_token is supplied by Twitter's authorization page following the callback.</param>
        /// <param name="authToken">The oauth_verifier is supplied by Twitter's authorization page following the callback.</param>
        public void GetAccessToken(string authToken, string verifier)
        {
            this.Token = authToken;

            string response = this.OAuthWebRequest(Method.GET, ACCESS_TOKEN, "&oauth_callback=oob&oauth_verifier=" + verifier);

            if (!String.IsNullOrEmpty(response))
            {
                // Store the Token and Token Secret
                NameValueCollection qs = HttpUtility.ParseQueryString(response);
                if (qs["oauth_token"] != null)
                {
                    this.Token = qs["oauth_token"];
                }
                if (qs["oauth_token_secret"] != null)
                {
                    this.TokenSecret = qs["oauth_token_secret"];
                }
                if (qs["user_id"] != null)
                {
                    this.UserId = qs["user_id"];
                }
                if (qs["screen_name"] != null)
                {
                    this.ScreenName = qs["screen_name"];
                }
            }
        }

		/// <summary>
		/// Submit a web request using OAuth.
		/// </summary>
		/// <param name="method">GET or POST</param>
		/// <param name="url">The full url, including the querystring.</param>
		/// <param name="postData">Data to post (querystring format)</param>
		/// <returns>The web server response.</returns>
		public string OAuthWebRequest(Method method, string url, string postData)
		{
			string outUrl = "";
			string querystring = "";
			string ret = "";

			// Setup postData for signing. Add the postData to the querystring.
			if (method == Method.POST)
			{
				if (!String.IsNullOrEmpty(postData))
				{
					// Decode the parameters and re-encode using the OAuth UrlEncode method.
					NameValueCollection qs = HttpUtility.ParseQueryString(postData);

					postData = "";
					foreach (string key in qs.AllKeys)
					{
						if (!String.IsNullOrEmpty(postData))
						{
							postData += "&";
						}

						qs[key] = HttpUtility.UrlDecode(qs[key]);
						qs[key] = this.UrlEncode(qs[key]);

						postData += key + "=" + qs[key];
					}

					if (url.IndexOf("?") > 0)
					{
						url += "&";
					}
					else
					{
						url += "?";
					}

					url += postData;
				}
			}

			Uri uri = new Uri(url);

			string nonce = this.GenerateNonce();
			string timeStamp = this.GenerateTimeStamp();

			// Generate signature
			string sig = this.GenerateSignature(uri,
				this.ConsumerKey,
				this.ConsumerSecret,
				this.Token,
				this.TokenSecret,
				method.ToString(),
				timeStamp,
				nonce,
				out outUrl,
				out querystring);

			querystring += "&oauth_signature=" + HttpUtility.UrlEncode(sig);

            string headerData = string.Empty;
            if(!string.IsNullOrEmpty(this.Token))
                headerData = string.Format("OAuth oauth_consumer_key=\"{0}\", oauth_nonce=\"{1}\", oauth_signature=\"{2}\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"{3}\", oauth_token=\"{4}\", oauth_version=\"1.0\"", this.ConsumerKey, nonce, HttpUtility.UrlEncode(sig), timeStamp, this.Token);

			// Convert the querystring to postData
			if (method == Method.POST)
			{
				postData = querystring;
				querystring = "";
			}

			if (!String.IsNullOrEmpty(querystring))
			{
				outUrl += "?";
			}

			ret = WebRequest(method, outUrl + querystring, postData, headerData);
  
			return ret;
		}

		/// <summary>
		/// Web Request Wrapper
		/// </summary>
		/// <param name="method">Http Method</param>
		/// <param name="url">Full url to the web resource</param>
		/// <param name="postData">Data to post in querystring format</param>
		/// <returns>The web server response.</returns>
		private string WebRequest(Method method, string url, string postData, string headerData)
		{
            if (method == Method.GET)
            {
                url += postData;
            }

			HttpWebRequest webRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
			webRequest.Method = method.ToString();
			webRequest.ServicePoint.Expect100Continue = false;

			if (method == Method.POST)
			{
				webRequest.ContentType = "application/x-www-form-urlencoded";

                if(!string.IsNullOrEmpty(headerData))
                    webRequest.Headers.Add("Authorization", headerData);

				// POST the data.
				StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
				try
				{
					requestWriter.Write(postData);
				}
				catch
				{
					throw;
				}
				finally
				{
					requestWriter.Close();
					requestWriter = null;
				}
			}

			return this.WebResponseGet(webRequest);
		}

		/// <summary>
		/// Process the web response.
		/// </summary>
		/// <param name="webRequest">The request object.</param>
		/// <returns>The response data.</returns>
		private string WebResponseGet(HttpWebRequest webRequest)
		{
			StreamReader responseReader = null;
			string responseData = "";

			try
			{
				responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
				responseData = responseReader.ReadToEnd();
			}
			catch
			{
				throw;
			}
			finally
			{
				webRequest.GetResponse().GetResponseStream().Close();
				responseReader.Close();
				responseReader = null;
			}

			return responseData;
		}

        public void GenerateAccessToken(String username, String password)
        {
            HttpWebRequest webRequest = (HttpWebRequest)System.Net.WebRequest.Create(GetAuthorizationUrl(null));
            webRequest.Method = OAuthTwitter.Method.GET.ToString();
            webRequest.ServicePoint.Expect100Continue = false;

            String strResponse = WebResponseGet(webRequest);
            String cookie = webRequest.GetResponse().Headers.Get("Set-Cookie");

            String authorizeURL = stringPattern(strResponse, "<form action=\"", "\" id=\"oauth_form\"");
            String authenticityToken = stringPattern(strResponse, "\"authenticity_token\" type=\"hidden\" value=\"", "\" />");
            String oAuthToken = stringPattern(strResponse, "name=\"oauth_token\" type=\"hidden\" value=\"", "\" />");

            String postData = "?Cookie=" + cookie + 
                              "&authenticity_token=" + authenticityToken + 
                              "&oauth_token=" + oAuthToken + 
                              "&session[username_or_email]=" + username + 
                              "&session[password]=" + password;

            strResponse = WebRequest(OAuthTwitter.Method.POST, authorizeURL, postData, string.Empty);
            String pinCode = stringPattern(strResponse, "<code>", "</code>");

            GetAccessToken(oAuthToken, pinCode);
        }

        private static String stringPattern(String body, String before, String after)
        {
            int beforeIndex = body.IndexOf(before);
            int afterIndex = body.IndexOf(after, beforeIndex);
            return body.Substring(beforeIndex + before.Length, afterIndex - (beforeIndex + before.Length));
        }
	}
}
