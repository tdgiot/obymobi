﻿using System;
using System.Reflection;
using Dionysos.Interfaces;
using Dionysos.Reflection;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Web.UI;
using Dionysos.Web.UI.WebControls;

namespace Dionysos.Web
{
	/// <summary>
	/// Class designed to ease databinding in situations where no EntityPage/EntityPanel can be used.
	/// </summary>
	public class SimpleDataBinder
	{
		#region Fields

		/// <summary>
		/// The page that contains the controls to databind.
		/// </summary>
		private readonly PageDefault page;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the page that contains the controls to databind.
		/// </summary>
		public PageDefault Page
		{
			get
			{
				return this.page;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SimpleDataBinder"/> class.
		/// </summary>
		/// <param name="page">The page that contains the controls to databind.</param>
		public SimpleDataBinder(PageDefault page)
		{
			if (page == null) throw new ArgumentNullException("page");

			this.page = page;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the values of the control to the fields on the datasource.
		/// </summary>
		/// <param name="dataSource">The datasource to fill.</param>
		public void SetControlValuesToDataSource(IEntity dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			this.SetControlValuesToDataSource(null, dataSource);
		}

		/// <summary>
		/// Sets the values of the control to the fields on the datasource.
		/// </summary>
		/// <param name="controlPrefix">The prefix used on the controls (eg. Customer to match tbCustomerName and set the datasource Name field).</param>
		/// <param name="dataSource">The datasource to fill.</param>
		public void SetControlValuesToDataSource(string controlPrefix, IEntity dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			var controlList = this.page.ControlList;
			if (controlList != null)
			{
				foreach (Control control in controlList)
				{
					// Skip controls without ID
					if (String.IsNullOrEmpty(control.ID)) continue;

					// Skip non bindable controls
					IBindable bindableControl = control as IBindable;
					if (bindableControl == null || !bindableControl.UseDataBinding) continue;

					// Skip controls with incorrect ID
					string controlId = control.ID.RemoveLeadingLowerCaseCharacters();
					if (String.IsNullOrEmpty(controlId) || (!String.IsNullOrEmpty(controlPrefix) && !controlId.StartsWith(controlPrefix))) continue;

					// Get field name
					string fieldName = String.IsNullOrEmpty(controlPrefix) ? controlId : controlId.RemoveLeft(controlPrefix);

					// Set datasource value if field exists
					if (dataSource.Fields[fieldName] != null)
					{
						this.SetControlValue(fieldName, control, dataSource);
					}
				}
			}
		}

		/// <summary>
		/// Sets the values of the datasource to the controls on the page.
		/// </summary>
		/// <param name="dataSource">The data source.</param>
		public void SetDataSourceValuesToControls(IEntity dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			this.SetDataSourceValuesToControls(null, dataSource);
		}

		/// <summary>
		/// Sets the values of the datasource to the controls on the page.
		/// </summary>
		/// <param name="controlPrefix">The prefix used on the controls (eg. Customer to match tbCustomerName and set it to the datasource Name field).</param>
		/// <param name="dataSource">The datasource to used to set the values.</param>
		public void SetDataSourceValuesToControls(string controlPrefix, IEntity dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			this.SetDataSourceValuesToControls(controlPrefix, true, dataSource);
		}

		/// <summary>
		/// Sets the values of the datasource to the controls on the page.
		/// </summary>
		/// <param name="controlPrefix">The prefix used on the controls (eg. Customer to match tbCustomerName and set it to the datasource Name field).</param>
		/// <param name="setMaxLengths">Set the TextBox.MaxLength properties based on the meta data.</param>
		/// <param name="dataSource">The datasource to used to set the values.</param>
		public void SetDataSourceValuesToControls(string controlPrefix, bool setMaxLengths, IEntity dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			var controlList = this.page.ControlList;
			if (controlList != null)
			{
				foreach (Control control in controlList)
				{
					// Skip controls without ID
					if (String.IsNullOrEmpty(control.ID)) continue;

					// Skip non bindable controls
					IBindable bindableControl = control as IBindable;
					if (bindableControl == null || !bindableControl.UseDataBinding) continue;

					// Skip controls with incorrect ID
					string controlId = control.ID.RemoveLeadingLowerCaseCharacters();
					if (String.IsNullOrEmpty(controlId) || (!String.IsNullOrEmpty(controlPrefix) && !controlId.StartsWith(controlPrefix))) continue;

					// Get field name
					string fieldName = String.IsNullOrEmpty(controlPrefix) ? controlId : controlId.RemoveLeft(controlPrefix);

					// Set control value if field exists
					if (dataSource.Fields[fieldName] != null)
					{
						PropertyInfo propertyInfo = control.GetType().GetProperty("Value", dataSource.Fields[fieldName].GetType()) ??
														control.GetType().GetProperty("Value", typeof(Object));

						if (propertyInfo != null)
						{
							// Use Value property with the same type as the datasource or an unspecified type (object)
							propertyInfo.SetValue(control, dataSource.Fields[fieldName].CurrentValue, null);
						}

						if (setMaxLengths)
						{
							// Set MaxLengths for Text input
							if (control is TextBoxMultiLine)
								((TextBoxMultiLine)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
							else if (control is TextBoxMultiLineBBCode)
								((TextBoxMultiLineBBCode)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
							else if (control is TextBoxString)
								((TextBoxString)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
							else if (control is TextBoxTelephoneNumber)
								((TextBoxTelephoneNumber)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
							else if (control is TextBoxZipcode)
								((TextBoxZipcode)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
							else if (control is TextBoxEmail)
								((TextBoxEmail)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
						}
					}
				}
			}
		}

		/// <summary>
		/// Sets the MaxLength properties of textboxes representing Text input based on meta data
		/// </summary>
		/// <param name="dataSource">The datasource to used to retrieve the meta data from.</param>
		public void SetMaxLengths(IEntity dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			this.SetMaxLengths(null, dataSource);
		}

		/// <summary>
		/// Sets the MaxLength properties of textboxes representing Text input based on meta data
		/// </summary>
		/// <param name="controlPrefix">The prefix used on the controls (eg. Customer to match tbCustomerName and set it to the datasource Name field).</param>
		/// <param name="dataSource">The datasource to used to retrieve the meta data from.</param>
		public void SetMaxLengths(string controlPrefix, IEntity dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			var controlList = this.page.ControlList;
			if (controlList != null)
			{
				foreach (Control control in controlList)
				{
					// Skip controls without ID
					if (String.IsNullOrEmpty(control.ID)) continue;

					// Skip non bindable controls
					IBindable bindableControl = control as IBindable;
					if (bindableControl == null || !bindableControl.UseDataBinding) continue;

					// Skip controls with incorrect ID
					string controlId = control.ID.RemoveLeadingLowerCaseCharacters();
					if (String.IsNullOrEmpty(controlId) || (!String.IsNullOrEmpty(controlPrefix) && !controlId.StartsWith(controlPrefix))) continue;

					// Get field name
					string fieldName = String.IsNullOrEmpty(controlPrefix) ? controlId : controlId.RemoveLeft(controlPrefix);

					// Set control value if field exists
					if (dataSource.Fields[fieldName] != null)
					{
						// Set MaxLengths for Text input
						if (control is TextBoxMultiLine)
							((TextBoxMultiLine)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
						else if (control is TextBoxMultiLineBBCode)
							((TextBoxMultiLineBBCode)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
						else if (control is TextBoxString)
							((TextBoxString)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
						else if (control is TextBoxTelephoneNumber)
							((TextBoxTelephoneNumber)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
						else if (control is TextBoxZipcode)
							((TextBoxZipcode)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
						else if (control is TextBoxEmail)
							((TextBoxEmail)control).MaxLength = dataSource.Fields[fieldName].MaxLength;
					}
				}
			}
		}

		/// <summary>
		/// Not so simple databinding happens here, sets the datasource value to the control value (fills the datasource).
		/// </summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <param name="control">The control.</param>
		/// <param name="dataSource">The data source.</param>
		private void SetControlValue(string fieldName, object control, IEntity dataSource)
		{
			object controlValue = Member.InvokeProperty(control, "Value");
			object dataSourceValue = dataSource.Fields[fieldName].CurrentValue;
			PropertyInfo controlPropertyInfo;

			if (control.GetType().GetMember("Value").Length > 1)
			{
				PropertyInfo[] properties = control.GetType().GetProperties();
				for (int i = 0; i < properties.Length; i++)
				{
					if (properties[i].Name == "Value")
					{
						controlPropertyInfo = properties[i];
						break;
					}
				}

			}

			PropertyInfo dataSourcePropertyInfo = dataSource.GetType().GetProperty(fieldName);

			if (controlValue == null)
			{
				// No actions on an null value
			}
			else
			{
				// Set the value from the control to the datasource
				if ((controlValue is DateTime || dataSourcePropertyInfo.PropertyType.UnderlyingSystemType == typeof(DateTime?)) && (DateTime)controlValue == DateTime.MinValue)
				{
					controlValue = null;
				}

				if (control is DropDownListLLBLGenEntityCollection || control is DropDownListInt)
				{
					if ((int)controlValue <= 0)
					{
						if (dataSourcePropertyInfo.PropertyType.UnderlyingSystemType == typeof(int?))
						{
							controlValue = null;
						}
					}
				}

				if (controlValue is string && dataSourcePropertyInfo.PropertyType.UnderlyingSystemType == typeof(int?) && ((string)controlValue).Length == 0)
				{
					controlValue = null;
				}

				// MDB HACK, should be fixed properly
				string type = control.GetType().ToString();
				if (type == "Dionysos.Web.UI.DevExControls.ComboBoxInt" || type == "Dionysos.Web.UI.DevExControls.ComboBoxEntityCollection" || type == "Dionysos.Web.UI.DevExControls.ComboBoxLLBLGenEntityCollection")
				{
					int? value = (int?)Dionysos.Reflection.Member.InvokeProperty(control, "Value");
					if (value.HasValue)
					{
						if (value == 0)
							controlValue = null;
					}
					else
						controlValue = null;
				}
			}

			try
			{
				// GK HACK ?
				// if null, try if it's an entity
				IEntity entity = dataSource as IEntity;
				if (entity != null && controlValue == null)
				{
					entity.SetNewFieldValue(dataSourcePropertyInfo.Name, controlValue);
				}
				else
				{
					dataSourcePropertyInfo.SetValue(dataSource, controlValue, null);
				}
			}
			catch (Exception innerException)
			{
				Control errorCtrl = control as Control;
				string id = "control could not be casted to Control";
				if (errorCtrl != null) id = errorCtrl.ID;

				throw new Exception(String.Format("ControlChanged: Error on control '{0}' with value '{1}'", id, controlValue.ToString()), innerException);
			}
		}

		#endregion
	}
}
