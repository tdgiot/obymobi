using System;
using System.IO;
using System.Net;
using System.Web;
using Dionysos.Web.HttpModules;
using Dionysos.Web.UI;

namespace Dionysos.Web
{
	/// <summary>
	/// Singleton utility class used for easy access to webapplication operations.
	/// </summary>
	public static class WebShortcuts
	{
		/// <summary>
		/// Get the current application URL.
		/// </summary>
		/// <returns>
		/// The current application URL.
		/// </returns>
		public static string CurrentApplicationUrl()
		{
			return HttpContext.Current.Request.ApplicationPath;
		}

		/// <summary>
		/// Shortcut to ResponseRedirect(url, TRUE).
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void Redirect(string url)
		{
			WebShortcuts.ResponseRedirect(url, true);
		}

		/// <summary>
		/// Shortcut to ResponseRedirect(url, TRUE).
		/// </summary>
		/// <param name="urlFormat">The URL format.</param>
		/// <param name="args">The format arguments.</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void Redirect(string urlFormat, params object[] args)
		{
			WebShortcuts.ResponseRedirect(String.Format(urlFormat, args), true);
		}

		/// <summary>
		/// Permanent redirect to the specified URL and end response (send a 301 Moved Permanently).
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void RedirectPermanent(string url)
		{
			HttpResponse response = HttpContext.Current.Response;
			response.Clear();
			response.StatusCode = (int)HttpStatusCode.MovedPermanently;
			response.RedirectLocation = url;
			response.End();
		}

		/// <summary>
		/// Directly redirects to the RawUrl of the request (brute force reload).
		/// </summary>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void RedirectUsingRawUrl()
		{
			WebShortcuts.RedirectUsingRawUrl(String.Empty);
		}

		/// <summary>
		/// Redirects the to the 404 page.
		/// </summary>
		public static void RedirectTo404Page()
		{
			WebShortcuts.RedirectTo404Page(false);
		}

		/// <summary>
		/// Redirects the to the 404 page.
		/// </summary>
		/// <param name="useTransfer">If set to <c>true</c> uses Transfer instead of RewritePath.</param>
		public static void RedirectTo404Page(bool useTransfer)
		{
			string relativeUrl = ConfigurationManager.GetString(DionysosWebConfigurationConstants.FileNotFoundPageFileName);
			relativeUrl = relativeUrl.StartsWith("~/") ? relativeUrl : "~/" + relativeUrl;

			if (useTransfer)
			{
				WebShortcuts.Set404ResponseCode();
				HttpContext.Current.Server.Transfer(VirtualPathUtility.ToAbsolute(relativeUrl));
			}
			else
			{
				HttpContext.Current.RewritePath(VirtualPathUtility.ToAbsolute(relativeUrl));
			}
		}

		/// <summary>
		/// Writes the stream to response.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="saveAsFile">if set to <c>true</c> [save as file].</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="stream">The stream.</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void WriteStreamToResponse(string fileName, bool saveAsFile, string fileExtension, Stream stream)
		{
			HttpResponse response = HttpContext.Current.Response;
			response.Clear();
			response.Buffer = false;
			response.AppendHeader("Content-Type", "application/" + fileExtension);
			response.AppendHeader("Content-Transfer-Encoding", "binary");
			string disposition = saveAsFile ? "attachment" : "inline";
			response.AppendHeader("Content-Disposition", String.Format("{0}; filename={1}.{2}", disposition, fileName, fileExtension));
			using (stream)
			{
				stream.Copy(response.OutputStream);
			}
			response.End();
		}

		/// <summary>
		/// Retrieve the IP address of the client
		/// </summary>
		/// <returns></returns>
		public static string ClientIp()
		{
			string toReturn = "0.0.0.0";

			try
			{
				toReturn = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
			}
			catch { }

			return toReturn;
		}

		/// <summary>
		/// Directly redirects to the RawUrl of the request (brute force reload)
		/// </summary>
		/// <param name="tabToActivate">Tab to activate after Redirect</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void RedirectUsingRawUrl(string tabToActivate)
		{
			if (GenericRewriteModule.UrlRewritten)
			{
				System.Web.HttpContext.Current.Response.Redirect(GenericRewriteModule.OriginalRawUrl, true);
			}
			else
			{
				QueryStringHelper qs = new QueryStringHelper();
				System.Web.HttpContext.Current.Response.Redirect(qs.MergeQuerystringWithRawUrl(), true);
			}
		}

		/// <summary>
		/// This method should be used instead of Response.Redirect when a site uses globalization
		/// </summary>
		/// <param name="url">Relative URL to redirect to</param>
		/// <param name="args">Arguments to use for formatting the url</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void ResponseRedirect(string url, params object[] args)
		{
			WebShortcuts.ResponseRedirect(String.Format(url, args), true);
		}

		/// <summary>
		/// This method should be used instead of Response.Redirect when a site uses globalization
		/// </summary>
		/// <param name="url">Relative URL to redirect to</param>
		/// <param name="endResponse">End response after redirect</param>
		/// <param name="args">Arguments to use for formatting the url</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void ResponseRedirect(string url, bool endResponse, params object[] args)
		{
			WebShortcuts.ResponseRedirect(String.Format(url, args), endResponse);
		}

		/// <summary>
		/// This method should be used instead of Response.Redirect when a site uses globalization
		/// </summary>
		/// <param name="url">Relative URL to redirect to</param>
		/// <param name="endResponse">End response after redirect</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void ResponseRedirect(string url, bool endResponse)
		{
			WebShortcuts.ResponseRedirect(url, endResponse, String.Empty);
		}

		/// <summary>
		/// This method should be used instead of Response.Redirect when a site uses globalization
		/// </summary>
		/// <param name="url">Relative URL to redirect to</param>
		/// <param name="endResponse">End response after redirect</param>
		/// <param name="tabToActivate">Tab to activate after Redirect</param>
		/// <exception cref="System.Threading.ThreadAbortException">The call to System.Web.HttpResponse.End() has terminated the current request.</exception>
		public static void ResponseRedirect(string url, bool endResponse, string tabToActivate, string target = "_self")
		{
			HttpResponse response = HttpContext.Current.Response;

			if (GlobalizationHelper.UseGlobalization)
			{
				if (!url.Contains("~"))
				{
					//throw new Dionysos.TechnicalException("WebShortcuts.ResponseRedirect is aangeroepen met een niet relatieve URL (Geen '~')");
					url = "~/" + GlobalizationHelper.CurrentUICultureString + "/" + url;
				}
				else
				{
					url = url.Replace("~/", "~/" + GlobalizationHelper.CurrentUICultureString + "/");
				}
			}

		    if (target == "_self")
		    {
		        response.Clear();
		        response.Redirect(url, endResponse);
		    }
		    else
		    {
                System.Web.UI.Page page = (System.Web.UI.Page)HttpContext.Current.Handler;

                if (page == null)
                {
                    throw new InvalidOperationException("Cannot redirect to new window outside Page context.");
                }
                url = page.ResolveClientUrl(url);

                string script = String.Format(@"window.open(""{0}"", ""{1}"");", url, target);
                System.Web.UI.ScriptManager.RegisterStartupScript(page, typeof(System.Web.UI.Page), "Redirect", script, true); 
		    }
		}

		/// <summary>
		/// Retrieve the base url of the application, for example: http://www.company.nl or http://www.company.nl/wms (if it's in a virtual folder)
		/// </summary>
		public static string BaseUrl
		{
			get
			{
				HttpRequest request = HttpContext.Current.Request;

				return new Uri(request.Url, request.ApplicationPath.TrimEnd('/') + '/').ToString();
			}
		}

		/// <summary>
		/// Resolves an Url to a relative Url (with "~/" in front).
		/// </summary>
		/// <param name="url">The Url to resolve.</param>
		/// <returns>
		/// The resolved Url, with "~/" in front (removes ApplicationPath if present).
		/// </returns>
		public static string ResolveToRelativeUrl(string url)
		{
			string relativeUrl = url;

			HttpRequest request = HttpContext.Current.Request;
			if (request.ApplicationPath.Length > 1 &&
				relativeUrl.StartsWith(request.ApplicationPath))
			{
				// Remove ApplicationPath
				relativeUrl = relativeUrl.Remove(0, request.ApplicationPath.Length);
			}

			if (!relativeUrl.StartsWith("~/"))
			{
				// Add relative part
				relativeUrl = StringUtil.CombineWithForwardSlash("~/", url);
			}

			return relativeUrl;
		}

		public static string ResolveToFullUrl(string relativeUrl, params object[] args)
		{
			return ResolveToFullUrl(string.Format(relativeUrl, args));
		}

		public static string ResolveToFullUrl(string relativeUrl)
		{
			HttpRequest request = HttpContext.Current.Request;
			string baseUrl = request.Url.Scheme + @"://" + request.Url.Authority;

			// MDB 20090219
			// Changed line the line below because StringUtil.CombineWithForwardSlash removes double slashes from the url
			//return StringUtil.CombineWithForwardSlash(baseUrl, VirtualPathUtility.ToAbsolute(relativeUrl));
			return (baseUrl + VirtualPathUtility.ToAbsolute(relativeUrl));
		}

		/// <summary>
		/// Maps the path.
		/// </summary>
		/// <param name="virtualPath">The virtual path.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public static string MapPath(string virtualPath, params object[] args)
		{
			return WebShortcuts.MapPath(String.Format(virtualPath, args));
		}

		/// <summary>
		/// Receives the physical path
		/// </summary>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns>
		/// The resolved Url.
		/// </returns>
		public static string MapPath(string virtualPath)
		{
			string path = null;

			HttpServerUtility server = HttpContext.Current.Server;
			if (server != null)
			{
				// Use the Page class
				path = server.MapPath(virtualPath);
			}

			return path;
		}

		/// <summary>
		/// Resolves the relative Url to a absolute Url.
		/// </summary>
		/// <param name="relativeUrl">The relative Url to resolve.</param>
		/// <returns>The resolved Url.</returns>
		public static string ResolveUrl(string relativeUrl)
		{
			string resolvedUrl = null;
			Page handler = HttpContext.Current.Handler as Page;

			if (handler != null)
			{
				// Use the Page class
				resolvedUrl = handler.ResolveUrl(relativeUrl);
			}
			else
			{
				// Use the VirtualPathUtility
				string queryString = null;
				int queryStringIndex = relativeUrl.IndexOf('?');
				if (queryStringIndex != -1)
				{
					queryString = relativeUrl.Substring(queryStringIndex);
					relativeUrl = relativeUrl.Substring(0, queryStringIndex);
				}

				resolvedUrl = VirtualPathUtility.ToAbsolute(relativeUrl) + queryString;
			}

			return resolvedUrl;
		}

		/// <summary>
		/// Resolves the relative Url to a absolute Url.
		/// </summary>
		/// <param name="relativeUrl">The relative Url to resolve.</param>
		/// <param name="args"></param>
		/// <returns>The resolved Url.</returns>
		public static string ResolveUrl(string relativeUrl, params object[] args)
		{
			return WebShortcuts.ResolveUrl(String.Format(relativeUrl, args));
		}

		/// <summary>
		/// Resolves the absolute client URL.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <returns>
		/// The absolute client URL.
		/// </returns>
		public static string ResolveClientUrl(string url)
		{
			if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
			{
				url = WebShortcuts.GetFullUrl(url);
			}

			return url;
		}

		/// <summary>
		/// Gets the full URL.
		/// </summary>
		/// <param name="relativeUrl">The relative URL.</param>
		/// <returns></returns>
		public static string GetFullUrl(string relativeUrl)
		{
			HttpRequest request = HttpContext.Current.Request;

			return request.Url.Scheme + @"://" + request.Url.Authority + WebShortcuts.ResolveUrl(relativeUrl);
		}

		/// <summary>
		/// Set404s the response code.
		/// </summary>
		public static void Set404ResponseCode()
		{
			HttpResponse response = HttpContext.Current.Response;
			response.TrySkipIisCustomErrors = true;
			response.StatusCode = (int)HttpStatusCode.NotFound;
		}
	}
}