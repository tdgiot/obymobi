using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Web;

namespace Dionysos.Web
{
    /// <summary>
    /// Class which represents a application
    /// </summary>
    public class Application
    {
        #region Fields

        static Application instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Web.Application type if the instance has not been initialized yet
        /// </summary>
        static Application()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new Application();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
        }

        /// <summary>
        /// Checks whether a application parameter exists in the application instance
        /// from the specified HttpApplicationState instance
        /// </summary>
        /// <param name="application">The HttpApplicationState instance to get the application instance from</param>
        /// <param name="name">The name of the application parameter</param>
        /// <returns>True if the application parameter exists, False if not</returns>
        public static bool HasParameter(HttpApplicationState application, string name)
        {
            bool hasParameter = true;

            if (Instance.ArgumentIsEmpty(application, "application"))
            {
                // Argument 'application' is empty
            }
            else if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Argument 'name' is empty
            }
            else
            {
                object value = application[name];
                if (Instance.Empty(value))
                {
                    hasParameter = false;
                }
            }

            return hasParameter;
        }

        /// <summary>
        /// Retrieves a application parameter from the application instance 
        /// from the specified HttpApplicationState instance
        /// </summary>
        /// <param name="application">The HttpApplicationState instance to get the application instance from</param>
        /// <param name="name">The name of the application parameter</param>
        /// <returns>An object instance containing the value of the application parameter</returns>
        public static object GetParameter(HttpApplicationState application, string name)
        {
            object value = null;

            if (Instance.ArgumentIsEmpty(application, "application"))
            {
                // Argument 'application' is empty
            }
            else if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Argument 'name' is empty
            }
            else
            {
                value = application[name];
                if (Instance.Empty(value))
                {
                    throw new Dionysos.EmptyException(string.Format("The value of application parameter '{0}' is empty!", name));
                }
            }

            return value;
        }

        /// <summary>
        /// Gets a System.Boolean value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Boolean instance containing the current value</returns>
        public static bool GetParameterAsBoolean(HttpApplicationState application, string name)
        {
            return (bool)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a System.Byte value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Byte instance containing the current value</returns>
        public static byte GetParameterAsByte(HttpApplicationState application, string name)
        {
            return (byte)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a System.Char value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Char instance containing the current value</returns>
        public static char GetParameterAsChar(HttpApplicationState application, string name)
        {
            return (char)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a System.Decimal value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Decimal instance containing the current value</returns>
        public static decimal GetParameterAsDecimal(HttpApplicationState application, string name)
        {
            return (decimal)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a System.Double value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Double instance containing the current value</returns>
        public static double GetParameterAsDouble(HttpApplicationState application, string name)
        {
            return (double)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a System.Single value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Single instance containing the current value</returns>
        public static float GetParameterAsFloat(HttpApplicationState application, string name)
        {
            return (float)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a System.Int32 value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int32 instance containing the current value</returns>
        public static int GetParameterAsInt(HttpApplicationState application, string name)
        {
            return (int)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a System.Int64 value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int64 instance containing the current value</returns>
        public static long GetParameterAsLong(HttpApplicationState application, string name)
        {
            return (long)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a System.Int16 value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int16 instance containing the current value</returns>
        public static short GetParameterAsShort(HttpApplicationState application, string name)
        {
            return (short)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a DateTime value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An DateTime instance containing the current value</returns>
        public static DateTime GetParameterAsDateTime(HttpApplicationState application, string name)
        {
            return (DateTime)GetParameter(application, name);
        }

        /// <summary>
        /// Gets a TimeStamp value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>A TimeStamp instance containing the current value</returns>
        public static TimeStamp GetParameterAsTimeStamp(HttpApplicationState application, string name)
        {
            return TimeStamp.Parse(GetParameterAsString(application, name));
        }

        /// <summary>
        /// Gets a System.Int32 instance of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int32 instance containing the current value</returns>
        public static int GetParameterAsEnumValue(HttpApplicationState application, string name, System.Type enumType)
        {
            Enum temp = (Enum)Enum.Parse(enumType, GetParameterAsString(application, name), true);
            return Convert.ToInt32(temp);
        }

        /// <summary>
        /// Gets a System.String value of a application parameter using the specified
        /// HttpApplicationState instance and the specified parameter name
        /// </summary>
        /// <returns>An System.String instance containing the current value</returns>
        public static string GetParameterAsString(HttpApplicationState application, string name)
        {
            return GetParameter(application, name).ToString();
        }

        #endregion
    }
}