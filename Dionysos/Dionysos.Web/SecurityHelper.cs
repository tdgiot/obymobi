using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

namespace Dionysos.Web
{
    /// <summary>
    /// This class has all methods needed to verify access to pages on this website.
    /// All based on Roles in the HttpContext object.
    /// </summary>
    public class SecurityHelper
    {
        private ArrayList _allowedRoles;

        public SecurityHelper()
        {            
        }

        /// <summary>
        /// Check if the current is allowed to see the page in the siteMapNode (via HttpContext.Current.User.IsInRole())
        /// </summary>
        /// <param name="siteMapNode">A site map node</param>
        /// <returns>Boolean stating if Access is Granted</returns>
        /// <exception cref="MissingFieldException"/>
        public bool AccessCheckForSiteMapItem(XmlNode siteMapNode)
        {
            this._allowedRoles = new ArrayList();
            if (siteMapNode.Attributes["roles"].Value.Length <= 0)
            {
                throw new System.MissingFieldException(string.Format("A page hasn't got any roles."));
            }
            else
            {
                this.AddRoleToAllowedRoles(siteMapNode.Attributes["roles"].Value);
            }

            return this.AccessGranted();
        }

        public void AddRoleToAllowedRoles(string roles)
        {            
            ArrayList rolesToAdd = new ArrayList(roles.Split(','));

            for (int j = 0; j < rolesToAdd.Count; j++)
            {
                bool found = false;
                for (int i = 0; i < this._allowedRoles.Count; i++)
                {
                    if (this._allowedRoles[i] == rolesToAdd[j])
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    this._allowedRoles.Add(rolesToAdd[j]);
                }
            }
        }

        /// <summary>
        /// Checks whether a view is allowed for the specified url
        /// </summary>
        /// <param name="relativeUrl">The relative url to check</param>
        /// <returns>True if a view is allowed, False if not</returns>
        public bool ViewAllowedForUrl(string relativeUrl)
        {
            _allowedRoles = new ArrayList();
            string siteMapFile = HttpContext.Current.Request.PhysicalApplicationPath + "web.sitemap";

            // Open the file
            XmlDocument siteMap = new XmlDocument();
            siteMap.Load(siteMapFile);

            // Add namespace for search options          
            XmlNamespaceManager xmlNs = new XmlNamespaceManager(siteMap.NameTable);
            xmlNs.AddNamespace("x", "http://schemas.microsoft.com/AspNet/SiteMap-File-1.0");

            XmlNode menuItems;
            menuItems = siteMap.SelectSingleNode("/x:siteMap/x:siteMapNode", xmlNs);            

            for (int i = 0; i < menuItems.ChildNodes.Count; i++)
            {
                XmlNode siteMapItem = menuItems.ChildNodes[i];

                if (relativeUrl.ToLower() == siteMapItem.Attributes["url"].Value.ToLower() &&
                    siteMapItem.Attributes["roles"].Value.Length >= 0)
                {
                    this.AddRoleToAllowedRoles(siteMapItem.Attributes["roles"].Value);
                }

                XmlNode subMenuItems = siteMap.SelectSingleNode("/x:siteMap/x:siteMapNode/x:siteMapNode[@description='" + siteMapItem.Attributes["description"].Value + "']", xmlNs);

                for (int j = 0; j < subMenuItems.ChildNodes.Count; j++)
                {
                    XmlNode subMenuItem = subMenuItems.ChildNodes[j];
                    if (relativeUrl.ToLower() == subMenuItem.Attributes["url"].Value.ToLower() &&
                        subMenuItem.Attributes["roles"].Value.Length >= 0)
                    {
                        this.AddRoleToAllowedRoles(subMenuItem.Attributes["roles"].Value);
                    }
                }
            }
         
            return this.AccessGranted();
        }

        /// <summary>
        /// Check if the Current.User in HttpContext has a role valid for this._allowedRoles
        /// </summary>
        /// <returns></returns>
        private bool AccessGranted()
        {
            bool accessGranted = false;
            for (int i = 0; i < this._allowedRoles.Count; i++)
            {
                if (HttpContext.Current.User.IsInRole(this._allowedRoles[i].ToString()))
                {
                    accessGranted = true;
                    break;
                }
            }
            return accessGranted;
        }
    }
}