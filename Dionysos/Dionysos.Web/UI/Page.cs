using Dionysos.Diagnostics;
using Dionysos.Interfaces.Presentation;
using Dionysos.Reflection;
using Dionysos.Web.HttpModules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Represents an .aspx file, also known as a Web Forms page, requested from a server that hosts an ASP.NET Web application.
	/// </summary>
	public class Page : System.Web.UI.Page, IUielement
	{
		#region Fields

		/// <summary>
		/// Indicates whether the DevExpress controls are valid.
		/// </summary>
		private bool? devExpressControlsValid = null;
		private bool isRequestBeingRedirected = false;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the crumbles object containing a set of RawUrls in order visited by the current user.
		/// </summary>
		/// <value>
		/// The crumbles.
		/// </value>
		public Crumbles Crumbles
		{
			get
			{
				return Crumbles.CurrentCrumbles;
			}
		}

		/// <summary>
		/// Gets or sets the crumble title.
		/// </summary>
		/// <value>
		/// The crumble title.
		/// </value>
		public string CrumbleTitle { get; set; }

		/// <summary>
		/// Gets or sets the informators (stored in session, cleared on unload).
		/// </summary>
		/// <value>
		/// The informators.
		/// </value>
		public List<IInformator> Informators
		{
			get
			{
				string key = typeof(Page).FullName + ".Informators";
				List<IInformator> informators;
				if (!SessionHelper.TryGetValue(key, out informators))
				{
					informators = this.Informators = new List<IInformator>();
				}

				return informators;
			}
			set
			{
				string key = typeof(Page).FullName + ".Informators";
				if (value == null)
				{
					SessionHelper.RemoveValue(key);
				}
				else
				{
					SessionHelper.SetValue(key, value);
				}
			}
		}

		/// <summary>
		/// Gets a value indicating whether page validation succeeded.
		/// </summary>
		/// <returns>
		///   <c>true</c> if page validation succeeded; otherwise, <c>false</c>.
		///   </returns>
		/// <exception cref="TechnicalException">IsValid is called without validation the DevExpress controls.</exception>
		public new bool IsValid
		{
			get
			{
				if (this.devExpressControlsValid == null)
				{
					throw new TechnicalException("IsValid is called without validation the DevExpress controls.");
				}

				return base.IsValid && this.devExpressControlsValid.Value;
			}
		}

		/// <summary>
		/// This will add some meta tags and HTTP headers to prevent browser caching.
		/// </summary>
		/// <value>
		/// <c>true</c> if [prevent browser caching]; otherwise, <c>false</c>.
		/// </value>
		public bool PreventBrowserCaching
		{
			get
			{
				return (this.ViewState["PreventBrowserCaching"] as bool?) ?? false;
			}
			set
			{
				this.ViewState["PreventBrowserCaching"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the URL where the page will be redirected to on PreRender (after all events are handled).
		/// </summary>
		/// <value>
		/// The redirect URL configuration preference render complete.
		/// </value>
		public string RedirectUrlOnPreRenderComplete { get; set; }

		/// <summary>
		/// Gets or sets the URL of the page to return to (the previous page).
		/// </summary>
		/// <value>
		/// The return URL.
		/// </value>
		public string ReturnUrl { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Page" /> class.
		/// </summary>
		public Page()
		{
			Debug.WriteLine("Dionysos.Web.UI.Page.Constructor - InformatatorsCount:" + this.Informators.Count);
			this.Load += new EventHandler(Page_Load);
			this.PreRenderComplete += new EventHandler(Page_PreRenderComplete);
			this.Unload += Page_Unload;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
		private void Page_Load(object sender, EventArgs e)
		{
			if (this.PreventBrowserCaching)
			{
				this.Response.CacheControl = "no-cache";
				this.Response.Expires = -120;
				this.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
				this.Response.AddHeader("Pragma", "no-cache");
				this.Response.AddHeader("Cache-Control", "private");
			}
		}

		/// <summary>
		/// Handles the PreRenderComplete event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
		private void Page_PreRenderComplete(object sender, EventArgs e)
		{
			// Check whether a redirect URL has been supplied; if so, redirect to the specified page
			if (!String.IsNullOrEmpty(this.RedirectUrlOnPreRenderComplete))
			{
				this.Response.Redirect(this.RedirectUrlOnPreRenderComplete, true);
			}

			if (!this.IsPostBack &&
				this.Header != null)
			{
				// Add the page to the crumbles
				this.AddToCrumbles();
			}

			Debug.WriteLine("Dionysos.Web.UI.Page.Page_PreRenderComplete - this.isRequestBeingRedirected: " + this.isRequestBeingRedirected);
			this.isRequestBeingRedirected = this.Response.IsRequestBeingRedirected;
		}

		/// <summary>
		/// Handles the Unload event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Unload(object sender, EventArgs e)
		{
			Debug.WriteLine("Dionysos.Web.UI.Page.Page_Unload - this.isRequestBeingRedirected: " + this.isRequestBeingRedirected);

			if (!this.isRequestBeingRedirected)
			{
				Debug.WriteLine("Dionysos.Web.UI.Page.Page_Unload - Clear Informators.");
				this.Informators = null;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Loads the user control with the specified parameters.
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="userControlPath">The user control path.</param>
		/// <param name="constructorParameters">The constructor parameters.</param>
		/// <returns>
		/// The user control.
		/// </returns>
		/// <exception cref="System.MemberAccessException">The requested constructor was not found on :  + ctl.GetType().BaseType.ToString()</exception>
		[Obsolete("Use the LoadControl method of Dionysos.Web.UI.Page or extension method of Dionysos.Web.UserControlHelper.")]
		public static UserControl LoadControlWithParametersStatic(System.Web.UI.Page page, string userControlPath, params object[] constructorParameters)
		{
			List<Type> constParamTypes = new List<Type>();
			foreach (object constParam in constructorParameters)
			{
				constParamTypes.Add(constParam.GetType());
			}

			UserControl ctl = page.LoadControl(userControlPath) as UserControl;

			// Find the relevant constructor
			System.Reflection.ConstructorInfo constructor = ctl.GetType().BaseType.GetConstructor(constParamTypes.ToArray());

			// Call the relevant constructor
			if (constructor == null)
			{
				throw new MemberAccessException("The requested constructor was not found on : " + ctl.GetType().BaseType.ToString());
			}
			else
			{
				constructor.Invoke(ctl, constructorParameters);
			}

			return ctl;
		}

		private int controlCount = 0;

		public new Control LoadControl(string virtualPath)
		{
			Interlocked.Increment(ref controlCount);

			Control control = base.LoadControl(virtualPath);
			control.ID = $"loadControl_{virtualPath.Split('/').LastOrDefault()?.Split('.')[0] ?? "MyUserControl"}_{controlCount}";

			return control;
		}

		/// <summary>
		/// Adds the informator.
		/// </summary>
		/// <param name="informatorType">The type of the informator.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An array containing zero or more objects to format.</param>
		public void AddInformator(InformatorType informatorType, string format, params object[] args)
		{
			this.Informators.Add(new StaticInformator(informatorType, format, args));
		}

		/// <summary>
		/// Adds the info type informator.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An array containing zero or more objects to format.</param>
		public void AddInformatorInfo(string format, params object[] args)
		{
			this.Informators.Add(new StaticInformator(InformatorType.Information, format, args));
		}

		/// <summary>
		/// Redirects to the previous page based on the crumbles.
		/// </summary>
		public virtual void Cancel()
		{
			string returnUrl;
			if (this.Crumbles.Count > 1)
			{
				// GK 080813 Why do we remove the last item? just step back 2
				// GK 080821 Now I know, if  you don't you end in a loop of 2 pages (the current one and the one last visited) if you hint cancel more than once.
				this.Crumbles.RemoveLastItem();
				this.Response.Redirect(this.Crumbles[this.Crumbles.Count - 1].Url, true);
			}
			else if (this.Crumbles.Count == 1)
			{
				// MDB Experimental :)
				this.Response.Redirect(this.Crumbles.GetLastItem().Url, true);
			}
			else if (!String.IsNullOrEmpty(this.ReturnUrl))
			{
				this.Response.Redirect(this.ReturnUrl, true);
			}
			else if (QueryStringHelper.TryGetValue("ReturnUrl", out returnUrl))
			{
				this.Response.Redirect(returnUrl, true);
			}
		}

		/// <summary>
		/// Returns a collection of control validators for a specified validation group.
		/// </summary>
		/// <param name="validationGroup">The validation group to return, or null to return the default validation group.</param>
		/// <returns>
		/// A <see cref="T:System.Web.UI.ValidatorCollection" /> that contains the control validators for the specified validation group.
		/// </returns>
		public new ValidatorCollection GetValidators(string validationGroup)
		{
			if (validationGroup == null)
			{
				validationGroup = String.Empty;
			}

			ValidatorCollection validators = new ValidatorCollection();
			foreach (IValidator validator in this.Validators)
			{
				if (validator is BaseValidator && validationGroup.Equals((validator as BaseValidator).ValidationGroup, StringComparison.Ordinal))
				{
					// ValidationGroup matches using BaseValidator.ValidationGroup
					validators.Add(validator);
				}
				else if (validator is IExtendedValidator && validationGroup.Equals((validator as IExtendedValidator).ValidationGroup, StringComparison.Ordinal))
				{
					// ValidationGroup matches using IExtendedValidator.ValidationGroup
					validators.Add(validator);
				}
				else if (Member.HasMember(validator, "ValidationGroup") && validationGroup.Equals(Member.InvokePropertyAsString(validator, "ValidationGroup"), StringComparison.Ordinal))
				{
					// ValidationGroup matches using reflection
					validators.Add(validator);

					// Add debug message
					Debug.WriteLine(String.Format("Found property ValidationGroup on '{0}' using Refelection, consider implementing IExtendedValidator.", validator.GetType().FullName));
				}
				else if (validationGroup.Length == 0)
				{
					// Validator doesn't have a ValidationGroup property and no validation group is used
					validators.Add(validator);
				}
			}

			return validators;
		}

		/// <summary>
		/// Load a strong typed user control.
		/// </summary>
		/// <typeparam name="UserControlType">The type of user control to load.</typeparam>
		/// <param name="userControlPath">The user control path.</param>
		/// <returns>
		/// The strong typed user control.
		/// </returns>
		public UserControlType LoadControl<UserControlType>(string userControlPath)
			where UserControlType : Control
		{
			return (UserControlType)this.LoadControl(userControlPath);
		}

		/// <summary>
		/// Loads the user control with the specified parameters.
		/// </summary>
		/// <param name="userControlPath">The user control path.</param>
		/// <param name="constructorParameters">The constructor parameters.</param>
		/// <returns>
		/// The user control.
		/// </returns>
		/// <exception cref="System.MemberAccessException">The requested constructor was not found on :  + ctl.GetType().BaseType.ToString()</exception>
		public UserControl LoadControlWithParameters(string userControlPath, params object[] constructorParameters)
		{
			List<Type> constParamTypes = new List<Type>();
			foreach (object constParam in constructorParameters)
			{
				constParamTypes.Add(constParam.GetType());
			}

			UserControl ctl = this.Page.LoadControl(userControlPath) as UserControl;

			// Find the relevant constructor
			ConstructorInfo constructor = ctl.GetType().BaseType.GetConstructor(constParamTypes.ToArray());

			// Call the relevant constructor
			if (constructor == null)
			{
				throw new MemberAccessException("The requested constructor was not found on : " + ctl.GetType().BaseType.ToString());
			}
			else
			{
				constructor.Invoke(ctl, constructorParameters);
			}

			return ctl;
		}

		/// <summary>
		/// Instructs any validation controls included on the page to validate their assigned information.
		/// </summary>
		public override void Validate()
		{
			base.Validate();

			this.devExpressControlsValid = DevExpress.Web.ASPxEdit.ValidateEditorsInContainer(this);
		}

		/// <summary>
		/// Instructs the validation controls in the specified validation group to validate their assigned information.
		/// </summary>
		/// <param name="validationGroup">The validation group name of the controls to validate.</param>
		public override void Validate(string validationGroup)
		{
			base.Validate(validationGroup);

			// Do additional validation on custom validation groups (sadly we cannot override the GetValidators method)
			ValidatorCollection validators = base.GetValidators(validationGroup);
			ValidatorCollection extendedValidators = this.GetValidators(validationGroup);
			if (!String.IsNullOrEmpty(validationGroup) || validators.Count != extendedValidators.Count)
			{
				foreach (IValidator validator in extendedValidators)
				{
					// Skip already validated controls
					if (validators.Contains(validator))
					{
						continue;
					}

					validator.Validate();
				}

				if (!this.devExpressControlsValid.HasValue)
				{
					// This could already be validated, since base method can call Validate
					this.devExpressControlsValid = DevExpress.Web.ASPxEdit.ValidateEditorsInContainer(this, validationGroup);
				}
			}
		}

		/// <summary>
		/// Adds to the current page to the crumbles.
		/// </summary>
		protected virtual void AddToCrumbles()
		{
			this.Crumbles.Add(this);
		}
		/// <summary>
		/// Redirect the page to the current RawUrl and ends the response.
		/// </summary>
		protected void RedirectUsingRawUrl()
		{
			WebShortcuts.RedirectUsingRawUrl();
		}

		/// <summary>
		/// Redirect the page to the current RawUrl using the extra query string parameters (replacing existing ones) and ends the response.
		/// </summary>
		/// <param name="extraQueryStringParameters">The extra query string parameters.</param>
		protected void RedirectUsingRawUrl(string extraQueryStringParameters)
		{
			QueryStringHelper qs = new QueryStringHelper();
			if (extraQueryStringParameters.Contains("&"))
			{
				string[] values = extraQueryStringParameters.Split(char.Parse("&"));
				for (int i = 0; i < values.Length; i++)
				{
					string key = StringUtil.GetAllBeforeFirstOccurenceOf(values[i], char.Parse("="), false);
					string value = StringUtil.GetAllAfterFirstOccurenceOf(values[i], char.Parse("="));
					qs.AddItem(key, value);
				}
			}
			else
			{
				string key = StringUtil.GetAllBeforeFirstOccurenceOf(extraQueryStringParameters, char.Parse("="), false);
				string value = StringUtil.GetAllAfterFirstOccurenceOf(extraQueryStringParameters, char.Parse("="));
				qs.AddItem(key, value);
			}

			this.Response.Redirect(qs.MergeQuerystringWithRawUrl());
		}

		/// <summary>
		/// Redirect the page to the current RawUrl on the PreRenderComplete event (when all events are handled) and ends the response.
		/// </summary>
		protected void RedirectUsingRawUrlOnPreRenderComplete()
		{
			this.RedirectUrlOnPreRenderComplete = this.Request.RawUrl;
		}

		/// <summary>
		/// Initializes the <see cref="T:System.Web.UI.HtmlTextWriter" /> object and calls on the child controls of the <see cref="T:System.Web.UI.Page" /> to render.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> that receives the page content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (GenericRewriteModule.UrlRewritten)
			{
				RewriteFormHtmlTextWriter urlRewriteHandler = new RewriteFormHtmlTextWriter(writer, GenericRewriteModule.OriginalRawUrl);
				base.Render(urlRewriteHandler);
			}
			else
			{
				base.Render(writer);
			}

			// Empty informator only if page is rendered (and not redirected)
			//if (this.Informators.Count > 0)
			//{
			//    // GK Validate on a non-existing group, this will validate the informators, but not the rest
			//    this.Validate("VALIDATE-FOR-INFORMATORS");
			//}
			Debug.WriteLine("Dionysos.Web.UI.Page.Render - this.isRequestBeingRedirected: " + this.isRequestBeingRedirected);
			this.isRequestBeingRedirected = this.Response.IsRequestBeingRedirected;
		}
		#endregion

		#region Classes

		/// <summary>
		/// Static informator.
		/// </summary>
		[Serializable]
		internal class StaticInformator : IInformator
		{
			#region Fields

			/// <summary>
			/// The text.
			/// </summary>
			private string text = String.Empty;

			/// <summary>
			/// The validation group.
			/// </summary>
			private string validationGroup = String.Empty;

			#endregion

			#region Properties

			/// <summary>
			/// Gets or sets the informator type.
			/// </summary>
			public InformatorType InformatorType { get; set; }

			/// <summary>
			/// Gets or sets the text.
			/// </summary>
			public string Text { get; set; }
			#endregion

			#region Constructors

			/// <summary>
			/// Initializes a new instance of the <see cref="StaticInformator"/> class.
			/// </summary>
			/// <param name="informatorType">The type of the informator.</param>
			/// <param name="format">A composite format string.</param>
			/// <param name="args">An array containing zero or more objects to format.</param>
			public StaticInformator(InformatorType informatorType, string format, params object[] args)
			{
				if (args == null || args.Length == 0)
				{
					this.Text = format;
				}
				else
				{
					this.Text = String.Format(format, args);
				}

				this.InformatorType = informatorType;
			}

			#endregion
		}

		#endregion
	}
}
