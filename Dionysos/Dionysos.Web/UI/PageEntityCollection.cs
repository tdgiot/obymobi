using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Dionysos.Interfaces;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Delegates.Web;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Abstract gui class for displaying webpages containing data collections
	/// </summary>
	public abstract class PageEntityCollection : Dionysos.Web.UI.PageDefault, Dionysos.Interfaces.IGuiEntityCollection
	{
		#region Fields

		private object dataSource = null;
		private string entityName = string.Empty;
		private int? pageSize = null;
		private int? pagingItemCount = null;
		private CollectionViewMode? viewMode;
		private List<Dionysos.Web.UI.WebControls.Pager> pagers = new List<Dionysos.Web.UI.WebControls.Pager>();
		private Dionysos.Web.UI.WebControls.GridView mainGridView;
		private Dionysos.Web.UI.WebControls.Repeater mainRepeater;
		private Dionysos.Web.UI.WebControls.DataList mainDataList;
		private Dionysos.Web.UI.WebControls.PlaceHolderPostDataView mainPlaceHolderPostDataView;
		private Dionysos.Web.UI.WebControls.PlaceHolderPreDataView mainPlaceHolderPreDataView;
		private string entityPageUrl = string.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the PageDataCollection class
		/// </summary>
		public PageEntityCollection()
		{
			// Hookup the events
			this.HookupEvents();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Hookup the events to the corresponding event handlers
		/// </summary>
		private void HookupEvents()
		{
			this.Load += new EventHandler(PageEntityCollection_Load);
			this.PreRender += new EventHandler(PageEntityCollection_PreRender);
		}

		/// <summary>
		/// Collection the GridView, Repeater, DataList and Pager controls to their properties on this page
		/// </summary>
		private void CollectGuiControls()
		{
			for (int i = 0; i < this.ControlList.Count; i++)
			{
				// Get a control from the control collection
				// and check whether the control is a data grid
				Control control = this.ControlList[i];
				if (this.mainGridView == null && control is Dionysos.Web.UI.WebControls.GridView)
				{
					this.mainGridView = control as Dionysos.Web.UI.WebControls.GridView;
				}
				if (this.mainRepeater == null && control is Dionysos.Web.UI.WebControls.Repeater)
				{
					this.mainRepeater = control as Dionysos.Web.UI.WebControls.Repeater;
				}
				if (this.mainDataList == null && control is Dionysos.Web.UI.WebControls.DataList)
				{
					this.mainDataList = control as Dionysos.Web.UI.WebControls.DataList;
				}
				if (control is Dionysos.Web.UI.WebControls.Pager)
				{
					if (this.pagers.IndexOf(control as Dionysos.Web.UI.WebControls.Pager) < 0)
						this.pagers.Add(control as Dionysos.Web.UI.WebControls.Pager);
				}
				if (control is Dionysos.Web.UI.WebControls.PlaceHolderPreDataView)
				{
					this.mainPlaceHolderPreDataView = control as Dionysos.Web.UI.WebControls.PlaceHolderPreDataView;
				}
				if (control is Dionysos.Web.UI.WebControls.PlaceHolderPostDataView)
				{
					this.mainPlaceHolderPostDataView = control as Dionysos.Web.UI.WebControls.PlaceHolderPostDataView;
				}
			}
		}


		#region Old seperate Control Searches
		///// <summary>
		///// Get the first GridView of the Page and use it as MainGridView
		///// </summary>
		///// <returns></returns>
		//private Dionysos.Web.UI.WebControls.GridView GetMainGridView()
		//{
		//    // Walk through the control collection of this page
		//    for (int i = 0; i < this.ControlList.Count; i++)
		//    {
		//        // Get a control from the control collection
		//        // and check whether the control is a data grid
		//        Control control = this.ControlList[i];
		//        if (control is Dionysos.Web.UI.WebControls.GridView)
		//        {
		//            return control as Dionysos.Web.UI.WebControls.GridView;
		//        }
		//    }
		//    return null;
		//}

		///// <summary>
		///// Get the first Repeater of the Page and use it as MainRepeater
		///// </summary>
		///// <returns></returns>
		//private Dionysos.Web.UI.WebControls.Repeater GetMainRepeater()
		//{
		//    // Walk through the control collection of this page
		//    for (int i = 0; i < this.ControlList.Count; i++)
		//    {
		//        // Get a control from the control collection
		//        // and check whether the control is a data grid
		//        Control control = this.ControlList[i];

		//    }
		//    return null;
		//}

		///// <summary>
		///// Get the first DataList of the Page and use it as MainDataList
		///// </summary>
		///// <returns></returns>
		//private Dionysos.Web.UI.WebControls.DataList GetMainDataList()
		//{
		//    // Walk through the control collection of this page
		//    for (int i = 0; i < this.ControlList.Count; i++)
		//    {
		//        // Get a control from the control collection
		//        // and check whether the control is a data grid
		//        Control control = this.ControlList[i];
		//        if (control is Dionysos.Web.UI.WebControls.DataList)
		//        {
		//            return control as Dionysos.Web.UI.WebControls.DataList;
		//        }
		//    }
		//    return null;
		//}

		///// <summary>
		///// Get the first Pager of the Page and use it as MainPager
		///// </summary>
		///// <returns></returns>
		//private Dionysos.Web.UI.WebControls.Pager GetMainPager()
		//{
		//    // Walk through the control collection of this page
		//    for (int i = 0; i < this.ControlList.Count; i++)
		//    {
		//        // Get a control from the control collection
		//        // and check whether the control is a data grid
		//        Control control = this.ControlList[i];
		//        if (control is Dionysos.Web.UI.WebControls.Pager)
		//        {
		//            return control as Dionysos.Web.UI.WebControls.Pager;
		//        }
		//    }
		//    return null;
		//}
		#endregion

		/// <summary>
		/// Initializes the page. This is a virtual method and can be overridden in subclasses.
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public virtual bool InitializeGui()
		{
			// Retrieve information from the querystring
			this.ProcessQueryString();

			// Determine entity name
			this.GetEntityName();

			// Initialize Pre DataView Placeholder
			this.InitializePreDataView();

			// Initialize Post DataView Placeholder
			this.InitializePostDataView();

			if (!this.IsPostBack)
			{
				// Initialize the entity collection
				bool preventLoad = false;
				this.OnDataSourceLoad(ref preventLoad);
				if (!preventLoad)
					this.InitializeEntityCollection();
				this.OnDataSourceLoaded();

				// Initialize the databindings
				this.InitializeDataBindings();
			}

			return true;
		}

		private void InitializePreDataView()
		{
			if (this.MainPlaceHolderPreDataView != null)
			{
				// Add pager
				Dionysos.Web.UI.WebControls.Pager pagerTop = new Dionysos.Web.UI.WebControls.Pager();
				pagerTop.PagerNextPrevStyle = Dionysos.Web.UI.WebControls.PagerNextPreviousStyle.AlwaysDisplayFirstAndLast;
				this.MainPlaceHolderPreDataView.Controls.Add(pagerTop);
				this.pagers.Add(pagerTop);
			}
		}

		private void InitializePostDataView()
		{
			if (this.MainPlaceHolderPostDataView != null)
			{
				// Add pager
				Dionysos.Web.UI.WebControls.Pager pagerBottom = new Dionysos.Web.UI.WebControls.Pager();
				pagerBottom.PagerNextPrevStyle = Dionysos.Web.UI.WebControls.PagerNextPreviousStyle.AlwaysDisplayFirstAndLast;
				this.MainPlaceHolderPostDataView.Controls.Add(pagerBottom);
				this.pagers.Add(pagerBottom);
			}
		}

		/// <summary>
		/// Set the Paging properties of the page to each Pager control on the page.
		/// </summary>
		public virtual void SetPagers()
		{
			foreach (Dionysos.Web.UI.WebControls.Pager pgr in this.pagers)
			{
				pgr.CurrentPageIndex = this.PagingCurrentPage;
				pgr.PageSize = this.PagingPageSize;
				if (this.PagingItemCount.HasValue)
					pgr.ItemCount = this.PagingItemCount.Value;
			}
		}

		/// <summary>
		/// Processes the Request.QueryStringHelper instance. This is a virtual method and can be overridden in subclasses.
		/// </summary>
		public virtual void ProcessQueryString()
		{
			int page;
			if (QueryStringHelper.TryGetValue("Page", out page))
			{
				this.PagingCurrentPage = page - 1;
			}
		}

		/// <summary>
		/// Gets the EntityName, if not set in OnInit, from QueryStringHelper and finally via the FileName of the Page
		/// </summary>
		public virtual void GetEntityName()
		{
			// If the EntityName is empty (could be set by
			if (Instance.Empty(this.EntityName))
			{
				string entityName;
				if (!QueryStringHelper.TryGetValue("entity", out entityName))
				{
					entityName = System.IO.Path.GetFileNameWithoutExtension(this.Request.Path);
				}

				this.EntityName = entityName;
			}
		}

		/// <summary>
		/// Initializes the entity collection of the page. This is an abstract method and must be overridden in subclasses.
		/// This should also initialize the PagingItemCount property with the appropriate value based on the collections length
		/// AND the sorting should be applied
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public abstract bool InitializeEntityCollection();

		/// <summary>
		/// Initializes the data data bindings of the controls on the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public abstract bool InitializeDataBindings();

		/// <summary>
		/// Refreshes the data collection of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>True if refreshing was succesful, False if not</returns>
		public abstract bool Refresh();

		/// <summary>
		/// Views an data item from the data collection of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <param name="id">The primary key value of the entity to view</param>
		/// <returns>True if viewing was succesfull, False if not</returns>
		public abstract bool View(object id);

		/// <summary>
		/// Adds an data item to the data collection of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>True if adding was succesfull, False if not</returns>
		public abstract bool Add();

		/// <summary>
		/// Edits the currently selected data item of the data collection of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <param name="id">The primary key value of the entity to edit</param>
		/// <returns>True if editing was succesfull, False if not</returns>
		public abstract bool Edit(object id);

		/// <summary>
		/// Deletes the currently selected data item from the data collection of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <param name="id">The primary key value of the entity to delete</param>
		/// <returns>True if deletion was successfull, False if not</returns>
		public abstract bool Delete(object id);

		/// <summary>
		/// Closes the page and returns to the referring page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		public abstract void Close();

		/// <summary>
		/// Invokes the specified command which has been fired by the main grid view
		/// </summary>
		public abstract void InvokeCommand(string commandName, int index);

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name of the entity for the data collection of this page
		/// </summary>
		[Browsable(true)]
		public string EntityName
		{
			get
			{
				return this.entityName;
			}
			set
			{
				this.entityName = value;
			}
		}

		/// <summary>
		/// Gets or sets the data collection of the form
		/// </summary>
		public object DataSource
		{
			get
			{
				return this.dataSource;
			}
			set
			{
				this.dataSource = value;
			}
		}

		/// <summary>
		/// The PlaceHolder before the dataview contorl that should be populated by the logic of the PageEntityCollection
		/// </summary>
		public Dionysos.Web.UI.WebControls.PlaceHolderPreDataView MainPlaceHolderPreDataView
		{
			get
			{
				if (this.mainPlaceHolderPreDataView == null)
				{
					this.CollectGuiControls();
				}
				return mainPlaceHolderPreDataView;
			}
			set
			{
				mainPlaceHolderPreDataView = value;
			}
		}

		/// <summary>
		/// The PlaceHolder after the dataview control that should be populated by the logic of the PageEntityCollection
		/// </summary>
		public Dionysos.Web.UI.WebControls.PlaceHolderPostDataView MainPlaceHolderPostDataView
		{
			get
			{
				if (this.mainPlaceHolderPostDataView == null)
				{
					this.CollectGuiControls();
				}
				return mainPlaceHolderPostDataView;
			}
			set
			{
				mainPlaceHolderPostDataView = value;
			}
		}

		/// <summary>
		/// The GridView that should be populated and handled by the logic of the PageEntityCollection
		/// </summary>
		public Dionysos.Web.UI.WebControls.GridView MainGridView
		{
			get
			{
				if (this.mainGridView == null)
				{
					this.CollectGuiControls();
				}
				return mainGridView;
			}
			set
			{
				mainGridView = value;
			}
		}

		/// <summary>
		/// The Repeater that should be populated and handled by the logic of the PageEntityCollection
		/// </summary>
		public Dionysos.Web.UI.WebControls.Repeater MainRepeater
		{
			get
			{
				if (this.mainRepeater == null)
				{
					this.CollectGuiControls();
				}
				return mainRepeater;
			}
			set
			{
				mainRepeater = value;
			}
		}

		/// <summary>
		/// The DataList that should be populated and handled by the logic of the PageEntityCollection
		/// </summary>
		public Dionysos.Web.UI.WebControls.DataList MainDataList
		{
			get
			{
				if (this.mainDataList == null)
				{
					this.CollectGuiControls();
				}
				return mainDataList;
			}
			set
			{
				mainDataList = value;
			}
		}

		/// <summary>
		/// Field to sort the DataSource on (first retreived from QueryString[SortExp] then from ViewState[DataSortField])
		/// </summary>
		public string DataSortField
		{
			get
			{
				string sortExp;
				if (QueryStringHelper.TryGetValue("SortExp", out sortExp))
				{
					return sortExp;
				}
				else if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSortField"))
				{
					string field = string.Empty;

					if (!Instance.Empty(this.MainGridView))
					{
						if (this.MainGridView.Columns.Count > 0)
						{
							// If empty take first column of grid
							if (this.MainGridView.Columns[0] is Dionysos.Web.UI.WebControls.BoundField)
							{
								field = ((Dionysos.Web.UI.WebControls.BoundField)this.MainGridView.Columns[0]).DataField;
							}
							else if (this.MainGridView.Columns[0] is Dionysos.Web.UI.WebControls.HyperLinkField)
							{
								field = ((Dionysos.Web.UI.WebControls.HyperLinkField)this.MainGridView.Columns[0]).DataTextField;
							}
						}
					}
					this.ViewState.Add("DataSortField", field);
				}
				return Dionysos.Web.ViewState.GetParameterAsString(this.ViewState, "DataSortField");
			}
			set
			{
				if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSortField"))
				{
					this.ViewState.Add("DataSortField", string.Empty);
				}
				this.ViewState["DataSortField"] = value;
			}
		}

		/// <summary>
		/// Direction in which to sort the DataSource
		/// </summary>
		public ListSortDirection DataSortDirection
		{
			get
			{
				string sortDir;
				if (QueryStringHelper.TryGetValue("SortDir", out sortDir))
				{
					return sortDir.Equals("ASC", StringComparison.OrdinalIgnoreCase) ? ListSortDirection.Ascending : ListSortDirection.Descending;
				}
				else if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSortDirection"))
				{
					this.ViewState.Add("DataSortDirection", ListSortDirection.Ascending);
				}

				return Dionysos.Web.ViewState.GetParameterAsListSortDirection(this.ViewState, "DataSortDirection");
			}
			set
			{
				if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSortDirection"))
				{
					this.ViewState.Add("DataSortDirection", String.Empty);
				}
				this.ViewState["DataSortDirection"] = value;
			}
		}

		/// <summary>
		/// Object to filter the data of the collection on
		/// </summary>
		public object DataSourceFilter
		{
			get
			{
				if (Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSourceFilter"))
				{
					return Dionysos.Web.ViewState.GetParameter(this.ViewState, "DataSourceFilter");
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSourceFilter"))
				{
					this.ViewState.Add("DataSourceFilter", null);
				}
				this.ViewState["DataSourceFilter"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the url of the entity page
		/// </summary>
		[Browsable(true)]
		public virtual string EntityPageUrl
		{
			get
			{
				return this.entityPageUrl;
			}
			set
			{
				this.entityPageUrl = value;
			}
		}

		/// <summary>
		/// PageSize for Paging Purposes
		/// </summary>
		[Browsable(true)]
		public virtual int PagingPageSize
		{
			get
			{
				int pageSize;
				if (!QueryStringHelper.TryGetValue("PageSize", out pageSize) &&
					!SessionHelper.TryGetValue("PageSize", out pageSize))
				{
					pageSize = ConfigurationManager.GetInt(DionysosWebConfigurationConstants.PageSize);
				}

				// Set the pagesize to the session content so it
				// can be used when the querystring is not available
			    SessionHelper.SetValue("PageSize", pageSize);

				return pageSize;
			}
			set
			{
				this.pageSize = value;

				// Set the pagesize to the session content so it
				// can be used when the querystring is not available
                SessionHelper.SetValue("PageSize", value);
			}
		}

		/// <summary>
		/// PagingItemCount the count of items in the datasource
		/// </summary>
		[Browsable(true)]
		public virtual int? PagingItemCount
		{
			get
			{
				return this.pagingItemCount;
			}
			set
			{
				this.pagingItemCount = value;
			}
		}

		/// <summary>
		/// Current page to display, 0-based, So Page 1 for the user is Page 0 in the system
		/// </summary>
		public int PagingCurrentPage
		{
			get
			{
				int page;
				if (QueryStringHelper.TryGetValue("Page", out page))
				{
					return page;
				}
				else if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "CurrentPage"))
				{
					return 0;
				}
				{
					return Dionysos.Web.ViewState.GetParameterAsInt(this.ViewState, "CurrentPage");
				}
			}
			set
			{
				if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "CurrentPage"))
				{
					this.ViewState.Add("CurrentPage", 1);
				}
				this.ViewState["CurrentPage"] = value;
			}
		}

		/// <summary>
		/// Containing the ViewMode to use (which determines which control to show/hide and populate with data
		/// Default is null which is old behaviour and shows all controls and fills them all with data.
		/// </summary>
		[Browsable(true)]
		public virtual CollectionViewMode ViewMode
		{
			get
			{
				if (this.viewMode == null)
					return (CollectionViewMode)Enum.ToObject(typeof(CollectionViewMode), (int)ConfigurationManager.GetInt(DionysosWebConfigurationConstants.CollectionViewMode));
				else
					return this.viewMode.Value;
			}
			set
			{
				this.viewMode = value;
			}
		}

		public List<Dionysos.Web.UI.WebControls.Pager> Pagers
		{
			get
			{
				return this.pagers;
			}
			set
			{
				this.pagers = value;
			}
		}

		#endregion

		#region Event handlers

		private void PageEntityCollection_Load(object sender, EventArgs e)
		{
			this.InitializeGui();

			if (!Instance.Empty(this.MainGridView))
			{
				// Add events, here because GridView has to be availabe
				this.MainGridView.Sorting += new System.Web.UI.WebControls.GridViewSortEventHandler(mainGridView_Sorting);
				this.MainGridView.PageIndexChanging += new System.Web.UI.WebControls.GridViewPageEventHandler(mainGridView_PageIndexChanging);
				this.MainGridView.RowCommand += new System.Web.UI.WebControls.GridViewCommandEventHandler(mainGridView_RowCommand);
			}
		}

		/// <summary>
		/// Handle Sorting from MainGridView
		/// </summary>
		/// <param name="sender">GridView Sender</param>
		/// <param name="e">Arguments</param>
		private void mainGridView_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			// Set the DataSortField and DataSortDirection
			// If the SortExpression stays the same change the SortDirection
			// If the SortExpression if new / different Sort Ascending
			// GK Separate Method
			// Direction
			if (this.DataSortField == string.Empty || this.DataSortField != e.SortExpression)
			{
				this.DataSortDirection = ListSortDirection.Ascending;
			}
			else
			{
				if (this.DataSortDirection == ListSortDirection.Ascending)
				{
					this.DataSortDirection = ListSortDirection.Descending;
				}
				else
				{
					this.DataSortDirection = ListSortDirection.Ascending;
				}
			}

			// Field
			this.DataSortField = e.SortExpression;

			// Update DataSoruce (GK Make method)
			this.PagingCurrentPage = 0;
			this.InitializeEntityCollection();
			this.InitializeDataBindings();

		}

		/// <summary>
		/// Handle Page Changed from MainGridView
		/// </summary>
		/// <param name="sender">GridView Sender</param>
		/// <param name="e">Arguments</param>
		private void mainGridView_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			this.PagingCurrentPage = e.NewPageIndex;

			// Update DataSoruce (GK Make method)
			this.InitializeEntityCollection();
			this.InitializeDataBindings();
		}

		private void mainGridView_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
		{
			// Get the command name from the event args
			// in order to see which button has been clicked
			string commandName = e.CommandName.ToUpper();

			switch (commandName)
			{
				case "VIEW":
				case "EDIT":
				case "DELETE":
					// Convert the row index stored in the CommandArgument
					// property to an Integer.
					int index = Convert.ToInt32(e.CommandArgument);

					// Invoke the command method
					this.InvokeCommand(commandName, index);
					break;
			}
		}

		void PageEntityCollection_PreRender(object sender, EventArgs e)
		{
			this.SetPagers();
		}

		#endregion

		#region Delagets, Delagtes Callers & Events

		public event DataSourceLoadHandler DataSourceLoad;
		public event DataSourceLoadedHandler DataSourceLoaded;

		protected void OnDataSourceLoaded()
		{
			if (this.DataSourceLoaded != null)
				this.DataSourceLoaded(this);
		}


		protected void OnDataSourceLoad(ref bool preventLoad)
		{
			if (this.DataSourceLoad != null)
				this.DataSourceLoad(this, ref preventLoad);
		}

		#endregion
	}
}
