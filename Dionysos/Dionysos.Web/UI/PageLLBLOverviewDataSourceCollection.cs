﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI
{
	public class PageLLBLOverviewDataSourceCollection : PageOverviewDataSourceCollection
	{
		#region Fields
		#endregion

		#region Methods

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			// Initialize the DataSource object			
			LLBLGenProDataSource ds = new LLBLGenProDataSource();
			ds.ID = this.DataSourceId;
			ds.LivePersistence = false; // We want to be able to control it
			this.Controls.Add(ds);
			this.DataSource = ds;		
		}

		public override bool InitializeDataSource()
		{
			// Initalize DataSource
			LLBLGenProDataSource ds = this.DataSource;

			// Initialize some standard values
			ds.ID = this.DataSourceId;
			ds.LivePersistence = true;
			ds.CacheLocation = DataSourceCacheLocation.Session;
			ds.DataContainerType = DataSourceDataContainerType.EntityCollection;
			ds.EnablePaging = true;

			// Retrieve the full type, assembly name of the collection to load
			SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection collection = Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
			Type typeOfCollection = collection.GetType();
			ds.EntityCollectionTypeName = string.Format("{0}, {1}", typeOfCollection.FullName, typeOfCollection.Assembly);

			// Set PK fieldname
			SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = Dionysos.DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
			this.MainGridView.KeyFieldName = entity.PrimaryKeyFields[0].Name;

			return true;
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// DataSource as a LLBLGenProDataSource datatype
		/// </summary>
		public new LLBLGenProDataSource DataSource
		{
			get
			{
				return base.DataSource as LLBLGenProDataSource;
			}
			set
			{
				base.DataSource = value;
			}
		}

		#endregion
		
	}
}
