using System;
using System.ComponentModel;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Interfaces.Data;
using Dionysos.Data;
using System.Web.UI;
using Dionysos.Diagnostics;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Gui class for displaying webpages containing entities
	/// </summary>
	public abstract class PageLLBLGenEntity : PageEntity, IGuiEntity
	{
		#region Fields

		/// <summary>
		/// Indicates whether to use the post redirect get pattern.
		/// </summary>
		private bool? usePostRedirectGet;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the data source.
		/// </summary>
		[Browsable(false)]
		public new SD.LLBLGen.Pro.ORMSupportClasses.IEntity DataSource
		{
			get
			{
				return base.DataSource;
			}
			set
			{
				base.DataSource = value;
			}
		}

        /// <summary>
        /// Overridden new EntityId in int format.
        /// </summary>
        public new int EntityId
		{
			get
			{
				int entityId = -1;
				try
				{
					if (base.EntityId != null)
					{
						entityId = Convert.ToInt32(base.EntityId);
					}
					else
					{
						// Try to get the PrimaryKey Value from the Entity if not Available
						if (this.DataSource != null && this.DataSource.PrimaryKeyFields.Count == 1)
						{
							entityId = Convert.ToInt32(this.DataSource.PrimaryKeyFields[0].CurrentValue);
						}
					}
				}
				catch
				{
					Dionysos.Exceptions.ExceptionManager.HandleException(new InvalidCastException("Non-integer EntityId on PageLLBLGenEntity"));
				}
				return entityId;
			}
			set
			{
				base.EntityId = value;
			}
		}

		/// <summary>
		/// Overriden for LLBLGenEntities, adds "Entity" to base.EntityName is it's not yet in place.
		/// </summary>
		public override string EntityName
		{
			get
			{
				string entityName = base.EntityName;
				if (String.IsNullOrEmpty(entityName) || entityName.EndsWith("Entity"))
				{
					return entityName;
				}
				else
				{
					return entityName + "Entity";
				}
			}
			set
			{
				base.EntityName = value;
			}
		}
		/// <summary>
		/// Gets or sets a value indicating whether this page is dirty (used to alert when leaving without saving).
		/// </summary>
		/// <value>
		///   <c>true</c> if this page is dirty; otherwise, <c>false</c>.
		/// </value>
		public bool IsDirty
		{
			get
			{
				bool isDirty;
				return Boolean.TryParse((string)Dionysos.Web.UI.MasterPage.JavascriptVariables["pageIsDirty"], out isDirty) && isDirty;
			}
			set
			{
				if (value)
				{
					Dionysos.Web.UI.MasterPage.JavascriptVariables["pageIsDirty"] = value;
				}
				else
				{
					Dionysos.Web.UI.MasterPage.JavascriptVariables.Remove("pageIsDirty");
				}
			}
		}

		/// <summary>
		/// Gets or sets whether the page uses the PRG pattern when refreshing data.
		/// </summary>
		/// <value>
		///   <c>true</c> if [use post redirect get]; otherwise, <c>false</c>.
		/// </value>
		public virtual bool UsePostRedirectGet
		{
			get
			{
				return this.usePostRedirectGet ?? (this.usePostRedirectGet = ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UsePostRedirectGet)).Value;
			}
			set
			{
				this.usePostRedirectGet = value;
			}
		}

        #endregion

        #region Methods

        /// <summary>
        /// Adds an data item to the data entity of the page.
        /// </summary>
        /// <returns>
        /// True if adding was succesfull, False if not
        /// </returns>
        public override bool Add()
		{
			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "add");

			Response.Redirect(Request.Path + queryString.Value);

			return true;
		}

		/// <summary>
		/// Saves the current data entity and reloads the page using it's current EntityId
		/// </summary>
		/// <returns>
		/// True if saving was successfull, False if not
		/// </returns>
		public override bool Apply()
		{
			if (this.Save())
			{
				// Redirect to Page in Add mode
				if (this.Request.RawUrl.Contains("mode=add"))
				{
					string url = this.Request.Path + "?mode=edit&id=" + this.EntityId.ToString();

					// Rebuild URL with mode Add and no Id
					for (int i = 0; i < this.Request.QueryString.AllKeys.Length; i++)
					{
						if (this.Request.QueryString.AllKeys[i] != null &&
							this.Request.QueryString.AllKeys[i].ToLower() != "mode" &&
							this.Request.QueryString.AllKeys[i].ToLower() != "id")
						{
							string key = this.Request.QueryString.AllKeys[i];
							url += string.Format("{0}={1}&", key, this.Request.QueryString[key]);
						}
					}
				}
				else
				{
					this.RedirectUsingRawUrl();
				}
			}

			return true;
		}

		/// <summary>
		/// Closes the page and returns to the referring page.
		/// </summary>
		public override void Close()
		{
			if (!Instance.Empty(this.ReturnUrl))
			{
				Response.Redirect(ResolveUrl(this.ReturnUrl));
			}
		}

		/// <summary>
		/// Deletes the current data entity of the page.
		/// </summary>
		/// <returns>
		/// True if deletion was successfull, False if not
		/// </returns>
		/// <exception cref="EmptyException">Variable 'DataSource' is empty.</exception>
		public override bool Delete()
		{
			bool success = false;

			if (Instance.Empty(this.DataSource))
			{
				throw new EmptyException("Variable 'DataSource' is empty.");
			}
			else
			{
				if (this.DataSource.IsNew)
				{
					// If the entity is new
					// the entity cannot be deleted
					// because it does not exist in the database then
				}
				else
				{
					try
					{
						success = this.DataSource.Delete();
					}
					catch (Dionysos.EntityDeleteException deleteException)
					{
						this.MultiValidatorDefault.AddError(deleteException.Message);
						this.Validate();
						success = false;
					}
				}
			}

			// If we have Succces and this is a PostBack (which means the user opened an Entity and wants to delete it)
			// we need to Redirect back like a SaveAndGo via Cancel after the delete
			// Otherwise the user get presented the data of a deleted entity
			if (success)
				this.Cancel();

			return success;
		}

		/// <summary>
		/// Edits the current data entity of the page.
		/// </summary>
		/// <returns>
		/// True if editing was succesfull, False if not
		/// </returns>
		public override bool Edit()
		{
			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "edit");
			queryString.AddItem("id", this.EntityId);

			Response.Redirect(Request.Path + queryString.Value);

			return true;
		}

		/// <summary>
		/// Start the proces of initializing the databindings
		/// </summary>
		/// <returns>
		/// Always true
		/// </returns>
		public override bool InitializeDataBindings()
		{
			DataBinder.DataBindControlRecursively(this, this.DataSource, this.IsPostBack);

			return true;
		}

		/// <summary>
		/// Initializes the entity.
		/// </summary>
		/// <returns>
		/// True if initialization was succesful, False if not
		/// </returns>
		public override bool InitializeEntity()
		{
			if (this.EntityId <= 0 &&
				this.DataSource == null)
			{
				// Get new entity if no entity id and data source are set
				this.DataSource = DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
			}
			else if (this.EntityId > 0 &&
				(this.DataSource == null || this.DataSource.IsNew))
			{
				// Get entity if datasource is not set or new
				this.DataSource = DataFactory.EntityFactory.GetEntity(this.EntityName, this.EntityId) as IEntity;
			}

			return this.DataSource != null;
		}
		/// <summary>
		/// Refreshes the page.
		/// </summary>
		/// <returns>
		/// True if refreshing was succesful, False if not
		/// </returns>
		public override bool Refresh()
		{
			// Redirect after processing the rest of the request
			if (this.IsPostBack &&
				!this.IsCallback &&
				this.UsePostRedirectGet)
			{
				this.Response.Redirect(this.Request.RawUrl, false);
			}

			this.InitializeDataBindings();

			return true;
		}

        public override bool Save()
        {
            return Save(false);
        }

        /// <summary>
        /// Saves the entity after successfully validating.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if the entity was successfully saved; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="Dionysos.EmptyException">Variable 'entity' is empty</exception>
        public bool Save(bool recurse)
		{
            bool success = false;

			// Get validation status
			bool isValid = false;
			try
			{
				isValid = this.IsValid;
			}
			catch
			{
				// Not yet validated
				this.Validate();
				isValid = this.IsValid;

				Debug.WriteLine(String.Format("Validate method not called in time, now using fallback for page: {0}", this.Request.RawUrl));
			}

            if (!this.ValidatePanels())
            {
                return false;
            }

			// Save entity
			if (isValid &&
				this.SetNonBindableData())
			{
				IEntity entity = this.DataSource;
				if (entity == null)
				{
					throw new EmptyException("Variable 'entity' is empty.");
				}

				try
				{
                    success = entity.Save(recurse);

                    if (!success)
					{
						this.MultiValidatorDefault.AddError("", "Niet opgeslagen", "De gegevens zijn niet opgeslagen.");
					}
					else
					{
						entity.Refetch();

						// Update the crumbles
						if (this.PageMode == PageMode.Add)
						{
							// MDB Dirty hack
							try
							{
								IEntityInformation info = EntityInformationUtil.GetEntityInformation(entity);
								this.Crumbles.UpdateLastCrumble(entity.PrimaryKeyFields[0].CurrentValue, EntityInformationUtil.GetShowFieldNameValue(entity));
							}
							catch { }
						}

						// GK Is this a nice place?! Or should we always try to save the panels :S
						if (base.Save())
						{
							// GK 20080516 If we were in Add mode we need to redirect to a edit page, otherwise a 2nd entity is saved... 
							// If we have a save from a Add mode, we should redirect because if the user saves again it will be saved as a new entity
							if (this.PageMode == PageMode.Add)
							{
								QueryStringHelper qs = new QueryStringHelper();
								qs.AddItem("mode", "edit");
								qs.AddItem("id", this.EntityId);

								WebShortcuts.ResponseRedirect(qs.MergeQuerystringWithRawUrl(), false);
							}
							else
							{
								// Refresh page (PRG) or databindings after successful save
								this.Refresh();
							}
						}
						else
						{
							success = false;
							this.Validate();
						}
					}
				}
				catch (EntitySaveException saveException)
				{
					this.MultiValidatorDefault.AddError(saveException.Message);
					this.Validate();
					success = false;
				}
			}

			return success;
		}

        /// <summary>
		/// Saves and clones this entity.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the entity can be saved and cloned; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="System.NotSupportedException">The DataSource does not implement ICloneable.</exception>
		public bool SaveAndClone()
		{
			ICloneable cloneable = this.DataSource as ICloneable;
			if (cloneable != null)
			{
				if (this.IsValid && this.Save())
				{
					IEntity clone = (IEntity)cloneable.Clone();

					// Redirect to page in edit mode
					string url = this.Request.Path + "?mode=edit&id=" + clone.PrimaryKeyFields[0].CurrentValue;
					this.Response.Redirect(url, false);

					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				throw new NotSupportedException("The DataSource does not implement ICloneable.");
			}
		}

		/// <summary>
		/// This function is called just after Validate and just before the datasource is Saved if it returns false the save is not processed
		/// </summary>
		/// <returns></returns>
		public virtual bool SetNonBindableData()
		{
			return true;
		}

		/// <summary>
		/// Custom override for LLBLGen Entities to use Entity and EntityFieldValidation
		/// </summary>
		public override void Validate()
		{
			// Validate entity
			this.DataSource.ValidateEntity();

			// Add errors to multi validator
			IDataErrorInfo dataErrorInfo = this.DataSource as IDataErrorInfo;
			if (dataErrorInfo != null &&
				!String.IsNullOrEmpty(dataErrorInfo.Error))
			{
				string[] errors = dataErrorInfo.Error.Split(';', StringSplitOptions.RemoveEmptyEntries);
				foreach (string error in errors)
				{
					this.MultiValidatorDefault.AddError(error);
				}
			}

			// Validate controls
			base.Validate();
		}

		/// <summary>
		/// Renders the page if no redirection is set, otherwise clears the response.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> that receives the page content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (String.IsNullOrEmpty(this.Response.RedirectLocation))
			{
				// Render the page
				base.Render(writer);
			}
			else
			{
				// Clear the response (and redirect)
				this.Response.ClearContent();
			}
		}

		#endregion
	}
}
