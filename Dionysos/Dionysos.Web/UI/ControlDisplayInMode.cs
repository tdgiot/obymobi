using System;
using System.Web;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// The way a button should be displayed based on a readonly/edit view
    /// </summary>
    public enum ControlDisplayInMode
    {
        Always,
        ReadOnly,
        OnEditable
    }
}
