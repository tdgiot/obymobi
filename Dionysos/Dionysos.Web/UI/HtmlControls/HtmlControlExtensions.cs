﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using Dionysos;
using System.Text.RegularExpressions;

namespace Dionysos.Web.UI.HtmlControls
{
	/// <summary>
	/// Extensions for HtmlControl.
	/// </summary>
	public static class HtmlControlExtensions
	{
		/// <summary>
		/// Adds the class name to the class attribute.
		/// </summary>
		/// <param name="htmlControl">The HtmlControl to add the class name to.</param>
		/// <param name="value">The class name to add.</param>
		/// <returns>true if class name is added; otherwise false (if already set).</returns>
		public static bool AddCssClass(this HtmlControl htmlControl, string value)
		{
			bool added = false;

			string cssClass = htmlControl.Attributes["class"] ?? String.Empty;
			List<string> cssClasses = cssClass.Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();
			if (!cssClasses.Contains(value, StringComparer.OrdinalIgnoreCase))
			{
				// Add class name
				cssClasses.Add(value);
				htmlControl.Attributes["class"] = StringUtil.CombineWithSpace(cssClasses.ToArray());
				added = true;
			}

			return added;
		}

		/// <summary>
		/// Removes the clas name from the class attribute.
		/// </summary>
		/// <param name="htmlControl">The HtmlControl to remove the class name from.</param>
		/// <param name="value">The class name to remove.</param>
		/// <returns>true if class name is removed; otherwise false (if not found).</returns>
		public static bool RemoveCssClass(this HtmlControl htmlControl, string value)
		{
			bool removed = false;

			string cssClass = htmlControl.Attributes["class"] ?? String.Empty;
			List<string> cssClasses = cssClass.Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();
			if (cssClasses.Contains(value, StringComparer.OrdinalIgnoreCase))
			{
				// Remove class name
				cssClasses.RemoveAll(cn => cn.Equals(value, StringComparison.OrdinalIgnoreCase));
				htmlControl.Attributes["class"] = StringUtil.CombineWithSpace(cssClasses.ToArray());
				removed = true;
			}

			return removed;
		}

		/// <summary>
		/// Checks whether the specified class name exists in the class attribute. 
		/// </summary>
		/// <param name="htmlControl">The HtmlControl to check for the class name.</param>
		/// <param name="value">The class name to check.</param>
		/// <returns>true if class name exists; otherwise false.</returns>
		public static bool HasCssClass(this HtmlControl htmlControl, string value)
		{
			string cssClass = htmlControl.Attributes["class"] ?? String.Empty;
			List<string> cssClasses = cssClass.Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();

			return cssClasses.Contains(value, StringComparer.OrdinalIgnoreCase);
		}
	}
}
