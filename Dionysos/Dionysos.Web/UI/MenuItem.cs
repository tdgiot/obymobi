﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Custom implemetation of MenuItem
	/// </summary>
	public class MenuItem
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public MenuItem()
		{
			this.ChildItems = new MenuItemCollection(this);			
			this.Enabled = true;
		}

		#region Properties

		/// <summary>
		/// Gets a MenuItemCollection object that contains the submenu items of the current menu item. 
		/// </summary>
		public MenuItemCollection ChildItems { get; set; }

		/// <summary>
		/// Gets or sets a value that indicates whether the MenuItem object is enabled, allowing the item to display a pop-out image and any child menu items. 
		/// </summary>
		public bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets the URL to an image that is displayed next to the text in a menu item. 
		/// </summary>
		public string ImageUrl { get; set; }

		/// <summary>
		/// Gets or sets the URL to navigate to when the menu item is clicked. 
		/// </summary>
		public string NavigateUrl { get; set; }

		/// <summary>
		/// Gets the parent menu item of the current menu item. 
		/// </summary>
		public MenuItem Parent { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the current menu item is selected in a Menu control. 
		/// </summary>
		public bool Selected { get; set; }

		/// <summary>
		/// Gets or sets the target window or frame in which to display the Web page content associated with a menu item. 
		/// </summary>
		public string Target { get; set; }

		/// <summary>
		/// Gets or sets the text displayed for the menu item in a Menu control. 
		/// </summary>
		public string Text { get; set; }

		/// <summary>
		/// Gets or sets the ToolTip text for the menu item. 
		/// </summary>
		public string ToolTip { get; set; }

		/// <summary>
		/// The object that this MenuItem was derived from
		/// </summary>
		public object DataItem
		{
			get;
			set;
		}

		public string Value { get; set; }

		#endregion
	}
}
