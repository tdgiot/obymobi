﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Defines the properties and methods that objects that participate in Web Forms validation must implement.
	/// </summary>
	public interface IExtendedValidator : IValidator
	{
		/// <summary>
		/// Indicates whether the control uses the validation.
		/// </summary>
		bool UseValidation { get; set; }

		/// <summary>
		/// Gets or sets the name of the validation group to which this validation control belongs.
		/// </summary>
		string ValidationGroup { get; set; }
	}
}
