﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Interfaces;
using System.Web.UI.WebControls;
using System.ComponentModel;
using Dionysos.Delegates.Web;
using Dionysos.Data;
using System.IO;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Class for displaying collections from a datasource
	/// </summary>
	public abstract class PageDataSourceCollection : PageDefault, IGuiEntityCollectionSimple
	{
		#region Fields

		/// <summary>
		/// The entity name.
		/// </summary>
		private string entityName;

		/// <summary>
		/// The entity page URL.
		/// </summary>
		private string entityPageUrl;

		/// <summary>
		/// The view mode.
		/// </summary>
		private CollectionViewMode? viewMode;

		/// <summary>
		/// The show archived records session key.
		/// </summary>
		private static string showArchivedRecordsSessionKey = "PageDataSourceCollection.ShowArchivedRecord";

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the entity name of the data collection of the GUI.
		/// </summary>
		public string EntityName
		{
			get
			{
				if (String.IsNullOrEmpty(this.entityName))
				{
					string typeName = this.GetType().BaseType.Name;
					this.entityName = typeName.Substring(0, typeName.Length - 1);
				}

				return this.entityName;
			}
			set
			{
				this.entityName = value;
			}
		}

		/// <summary>
		/// Gets or sets the data collection of the gui
		/// </summary>
		public object DataSource { get; set; }

		/// <summary>
		/// Gets or sets the data source id.
		/// </summary>
		/// <value>
		/// The data source id.
		/// </value>
		public string DataSourceId { get; set; }

		/// <summary>
		/// Gets or sets the URL of the entity page.
		/// </summary>
		public virtual string EntityPageUrl
		{
			get
			{
				if (String.IsNullOrEmpty(this.entityPageUrl))
				{
					this.entityPageUrl = EntityInformationUtil.GetEntityInformation(this.EntityName).DefaultEntityEditPage;
				}

				return this.entityPageUrl;
			}
			set
			{
				this.entityPageUrl = value;
			}
		}

		/// <summary>
		/// Gets or sets the size of the paging page.
		/// </summary>
		/// <value>
		/// The size of the paging page.
		/// </value>
		public virtual int PagingPageSize
		{
			get
			{
				int pageSize;
				if (!QueryStringHelper.TryGetValue("PageSize", out pageSize) &&
					!SessionHelper.TryGetValue("PageSize", out pageSize))
				{
					pageSize = ConfigurationManager.GetInt(DionysosWebConfigurationConstants.PageSize);
				}
				else
				{
					this.PagingPageSize = pageSize;
				}

				return pageSize;
			}
			set
			{
			    SessionHelper.SetValue("PageSize", value);
			}
		}

		/// <summary>
		/// Containing the ViewMode to use (which determines which control to show/hide and populate with data
		/// Default is null which is old behaviour and shows all controls and fills them all with data.
		/// </summary>
		/// <value>
		/// The view mode.
		/// </value>
		public virtual CollectionViewMode ViewMode
		{
			get
			{
				return (this.viewMode ?? (this.viewMode = ConfigurationManager.GetInt(DionysosWebConfigurationConstants.CollectionViewMode).ToEnum<CollectionViewMode>())).GetValueOrDefault();
			}
			set
			{
				this.viewMode = value;
			}
		}

		/// <summary>
		/// Gets the page mode.
		/// </summary>
		/// <value>
		/// The page mode.
		/// </value>
		public virtual CollectionPageMode PageMode
		{
			get
			{
				CollectionPageMode collectionPageMode;

				string mode;
				if (!QueryStringHelper.TryGetValue("mode", out mode) ||
					!EnumUtil.TryParse<CollectionPageMode>(mode, true, out collectionPageMode))
				{
					collectionPageMode = CollectionPageMode.View;
				}

				return collectionPageMode;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether archived records are shown.
		/// </summary>
		/// <value>
		/// <c>true</c> if archived records are shown; otherwise, <c>false</c>.
		/// </value>
		public static bool ShowArchivedRecords
		{
			get
			{
				bool archivedRecords;
				return SessionHelper.TryGetValue(showArchivedRecordsSessionKey, out archivedRecords) && archivedRecords;
			}
			set
			{
				SessionHelper.SetValue(showArchivedRecordsSessionKey, value);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PageDataSourceCollection"/> class.
		/// </summary>
		public PageDataSourceCollection()
		{
			this.Load += new EventHandler(PageDataSourceCollection_Load);
		}

		#endregion

		#region Event handlers

		/// <summary>
		/// Handles the Load event of the PageDataSourceCollection control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void PageDataSourceCollection_Load(object sender, EventArgs e)
		{
			this.InitializeGui();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the page. This is a virtual method and can be overridden in subclasses.
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public virtual bool InitializeGui()
		{
			// Retrieve information from the querystring
			this.ProcessQueryString();

			// Determine entity name
			this.GetEntityName();

			// Initialize the entity collection
			bool preventLoad = false;
			this.OnDataSourceLoad(ref preventLoad);
			if (!preventLoad)
			{
				this.InitializeDataSource();
			}

			this.OnDataSourceLoaded();

			// Initialize the databindings
			this.InitializeDataBindings();

			return true;
		}

		/// <summary>
		/// The page's PageMode by string
		/// </summary>
		/// <param name="pageMode"></param>
		public void SetPageMode(string pageMode)
		{
			QueryStringHelper qs = new QueryStringHelper();
			qs.AddItem("mode", pageMode);

			this.Response.Redirect(qs.MergeQuerystringWithRawUrl());
		}

		/// <summary>
		/// Processes the Request.QueryStringHelper instance. This is a virtual method and can be overridden in subclasses.
		/// </summary>
		public virtual void ProcessQueryString()
		{ }

		/// <summary>
		/// Gets the EntityName, if not set in OnInit, from QueryStringHelper and finally via the FileName of the Page
		/// </summary>
		public virtual void GetEntityName()
		{
			if (String.IsNullOrEmpty(this.EntityName))
			{
				string entityName;
				if (QueryStringHelper.TryGetValue("entity", out entityName))
				{
					entityName = Path.GetFileNameWithoutExtension(this.Request.Path);
					entityName = entityName.Substring(0, entityName.Length - 1); // Removing trailing "s" for plural
				}

				this.EntityName = entityName;
			}
		}

		/// <summary>
		/// Adds an data item to the data collection of the form
		/// </summary>
		/// <returns>
		/// True if adding was succesfull, False if not
		/// </returns>
		public virtual bool Add()
		{
			string url = string.Format("{0}?mode={1}&entity={2}&id=", this.EntityPageUrl, "add", this.EntityName);
			WebShortcuts.ResponseRedirect(url, true);

			return true;
		}

		/// <summary>
		/// Initializes the entity collection of the page. This is an abstract method and must be overridden in subclasses.
		/// This should also initialize the PagingItemCount property with the appropriate value based on the collections length
		/// AND the sorting should be applied
		/// </summary>
		/// <returns>
		/// True if initialization was succesful, False if not
		/// </returns>
		public abstract bool InitializeDataSource();

		/// <summary>
		/// Initializes the data data bindings of the controls on the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if initialization was succesful, False if not
		/// </returns>
		public abstract bool InitializeDataBindings();

		#endregion

		#region Events

		/// <summary>
		/// Event fired before loading the entity
		/// </summary>
		public event DataSourceLoadHandler DataSourceLoad;

		/// <summary>
		/// Event fired after loading the entity
		/// </summary>
		public event DataSourceLoadedHandler DataSourceLoaded;

		/// <summary>
		/// Called when data source loads.
		/// </summary>
		/// <param name="preventLoad">If set to <c>true</c> prevents load.</param>
		protected void OnDataSourceLoad(ref bool preventLoad)
		{
			if (this.DataSourceLoad != null)
				this.DataSourceLoad(this, ref preventLoad);
		}

		/// <summary>
		/// Called when the data source is loaded.
		/// </summary>
		protected void OnDataSourceLoaded()
		{
			if (this.DataSourceLoaded != null)
				this.DataSourceLoaded(this);
		}

		#endregion
	}
}
