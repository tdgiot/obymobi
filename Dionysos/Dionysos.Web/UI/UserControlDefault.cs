﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI
{
    public class UserControlDefault : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.PreRender += UserControl_PreRender;
        }

        void UserControl_PreRender(object sender, EventArgs e)
        {
            this.CheckPageType();
        }

        public string Translate(string translationKey, string translationValue, bool manualTranslationKey = false)
        { 
           this.CheckPageType();
           string translationKeyPrefix = string.Empty;
           if (manualTranslationKey)
           {
               // No prefix
           }
           else if (this.GetType().FullName.StartsWith("ASP."))
           {
               // Require Base Type
               translationKeyPrefix = this.GetType().BaseType.FullName;
           }
           else
           {
               translationKeyPrefix = this.GetType().FullName;
           }

           if(!manualTranslationKey) translationKey = translationKeyPrefix + "." + translationKey;

           return this.Page.Translate(translationKey, translationValue, true);
        }

        private new PageDefault Page
        {
            get
            {
                return (PageDefault)base.Page;
            }
            set
            {
                base.Page = value;
            }
        }

        private void CheckPageType()
        {
            if (this.Page as PageDefault == null) throw new FunctionalException("Dionysos.Web.UI.WebControls.UserControl can only be used on pages which are based on PageDefault");
        }
    }
}
