using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Globalization;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Gui class which represents an default .aspx page
	/// </summary>
	public class PageDefault : Page
	{
		#region Constants

		/// <summary>
		/// Name for the Multivalidator control on the page
		/// </summary>
		public const string MultiValidatorName = "DefaultMultiValidator";

		#endregion

		#region Fields

		private ControlCollection controlCollection = null;
		private System.Web.UI.WebControls.MenuItemCollection menuItemCollection = null;
		private Dionysos.Web.UI.WebControls.MultiValidator multiValidator = null;
		private bool localizeTitle = true;

		#endregion

		#region Constructors

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init" /> event to initialize the page.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			// Collect the controls on the page (first on init, later on
			this.CollectControls();

			// Add MultiValidator Control
			if (this.multiValidator == null)
			{
				multiValidator = new Dionysos.Web.UI.WebControls.MultiValidator();
				multiValidator.ID = PageDefault.MultiValidatorName;
				this.Controls.Add(multiValidator);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Page.InitComplete" /> event after page initialization.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
		protected override void OnInitComplete(EventArgs e)
		{
			this.CollectControls();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Collects the controls from the page and places them into an Lynx_media.Web.Collections.ControlCollection
		/// </summary>
		public void CollectControls()
		{
			this.menuItemCollection = new System.Web.UI.WebControls.MenuItemCollection();
			this.controlCollection = new ControlCollection();

			for (int i = 0; i < this.Controls.Count; i++)
			{
				Control control = this.Controls[i];

				if (control is Menu)
				{
					this.CollectMenuItems(control as Menu);
				}
				else
				{
					this.CollectControls(control);
				}
			}
		}

		/// <summary>
		/// Gets the subcontrols from the specified control recursively
		/// </summary>
		/// <param name="control">The System.Web.UI.Control to get the subcontrols from</param>
		public virtual void CollectControls(Control control)
		{
			// GK 09092008
			// MDB 10092008 This is not going to work with subclasses
			string controlType = TypeUtil.GetTypeString(control);
			if (controlType == "DevExpress.Web.ASPxGridView.ASPxGridView" ||
				controlType == "Dionysos.Web.UI.DevExControls.GridViewEntityCollection" ||
				controlType == "Dionysos.Web.UI.WebControls.TextBoxRelation")
			{
				this.controlCollection.Add(control);
			}
			else if (control.Controls.Count == 0)
			{
				this.controlCollection.Add(control);
			}
			else
			{
				this.controlCollection.Add(control);

				for (int i = 0; i < control.Controls.Count; i++)
				{
					this.CollectControls(control.Controls[i]);
				}
			}
		}

		/// <summary>
		/// Recursively collects the menu items from a Lynx_media.Web.UI.WebControls.Menu instance
		/// </summary>
		/// <param name="menu">The Lynx_media.Web.UI.WebControls.Menu instance to get the menu items from</param>
		public void CollectMenuItems(Menu menu)
		{
			for (int j = 0; j < menu.Items.Count; j++)
			{
				this.CollectMenuItems(menu.Items[j]);
			}
		}

		/// <summary>
		/// Recursively collects the child menu items from a Lynx_media.Web.UI.WebControls.MenuItem instance
		/// </summary>
		/// <param name="menuItem">The Lynx_media.Web.UI.WebControls.MenuItem instance to get the child menu items from</param>
		public void CollectMenuItems(System.Web.UI.WebControls.MenuItem menuItem)
		{
			this.menuItemCollection.Add(menuItem);

			if (!Instance.Empty(menuItem.ChildItems.Count))
			{
				for (int j = 0; j < menuItem.ChildItems.Count; j++)
				{
					this.CollectMenuItems(menuItem.ChildItems[j]);
				}
			}
		}

		/// <summary>
		/// Get the control which caused the PostBack
		/// As found on: http://www.ryanfarley.com/blog/archive/2005/03/11/1886.aspx
		/// </summary>
		/// <returns></returns>
		public Control GetPostBackControl()
		{
			Control control = null;

			string controlName = this.Request.Params.Get("__EVENTTARGET");
			if (!String.IsNullOrEmpty(controlName))
			{
				control = this.FindControl(controlName);
			}
			else
			{
				foreach (string ctl in this.Request.Form)
				{
					Control c = this.FindControl(ctl);
					if (c is System.Web.UI.WebControls.Button)
					{
						control = c;
						break;
					}
				}
			}

			return control;
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Page.PreRenderComplete" /> event after the <see cref="M:System.Web.UI.Page.OnPreRenderComplete(System.EventArgs)" /> event and before the page is rendered.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
		/// <exception cref="Dionysos.TechnicalException">GlobalizationHelper is not set in Dionysos.Global. Do this in the Global.asax, this is new behaviour (breaking change)</exception>
		protected override void OnPreRenderComplete(EventArgs e)
		{
			base.OnPreRenderComplete(e);

			// Localize if requested
			// 20090804 GK This is NEW!
			if (GlobalizationHelper.UseGlobalization && Dionysos.Global.GlobalizationHelper == null)
				throw new Dionysos.TechnicalException("GlobalizationHelper is not set in Dionysos.Global. Do this in the Global.asax, this is new behaviour (breaking change)");
			else if (Dionysos.Global.GlobalizationHelper != null)
				Dionysos.Global.GlobalizationHelper.LocalizeControls(this);

			//if (GlobalizationHelper.UseGlobalization || /* For backwards compatiblity */
			//    Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.Globalization_Enabled))
			//{
			//    this.CollectControls();
			//    if (Dionysos.ConfigurationManager.GetInt(DionysosWebConfigurationConstants.Globalization_Version) == 1)
			//    {
			//        // Controls:
			//        // Re-load control collection because of dynamicly added controls
			//        GlobalizationHelper.LocalizeControls(this);
			//    }
			//    else if (Dionysos.ConfigurationManager.GetInt(DionysosWebConfigurationConstants.Globalization_Version) == 2)
			//    {
			//        // New
			//    }
			//    else
			//    {
			//        throw new TechnicalException("There's no implementation for Globalization_Version: '{0}'", Dionysos.ConfigurationManager.GetInt(DionysosWebConfigurationConstants.Globalization_Version));
			//    }
			//}
		}

		/// <summary>
		/// Translates the specified translation key.
		/// </summary>
		/// <param name="translationKey">The translation key, a prefix is not required, the PageClass will be used for that.</param>
		/// <param name="translationValue">The translation value.</param>
		/// <returns></returns>
		/// <exception cref="Dionysos.TechnicalException">Dionysos.Global.TranslationProvider == null, method 'PageDefault.Translate' is a shortcust to Dionysos.Global.TranslationProvider and requires Dionysos.Global.TranslationProvider != null.</exception>
		public string Translate(string translationKey, string translationValue, bool manualTranslationKey = false)
		{
			if (Dionysos.Global.TranslationProvider == null)
				throw new Dionysos.TechnicalException("Dionysos.Global.TranslationProvider == null, method 'PageDefault.Translate' is a shortcust to Dionysos.Global.TranslationProvider and requires Dionysos.Global.TranslationProvider != null.");
			else
			{
				string translationKeyPrefix = string.Empty;
                if (manualTranslationKey)
                { 
                    // No prefix
                }
                else if (this.GetType().FullName.StartsWith("ASP."))
                {
                    // Require Base Type
                    translationKeyPrefix = this.GetType().BaseType.FullName;
                }
                else
                    translationKeyPrefix = this.GetType().FullName;

                Translatable translatable;
                if(manualTranslationKey)
				    translatable = new Translatable(translationKey, translationValue);
                else
                    translatable = new Translatable(translationKeyPrefix + "." + translationKey, translationValue);

				return Dionysos.Global.TranslationProvider.GetTranslation(translatable);
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the collection of controls on this page
		/// </summary>
		/// <value>
		/// The control list.
		/// </value>
		public ControlCollection ControlList
		{
			get
			{
				return this.controlCollection;
			}
		}

		/// <summary>
		/// Gets the collection of menu items on this page
		/// </summary>
		/// <value>
		/// The menu item list.
		/// </value>
		public System.Web.UI.WebControls.MenuItemCollection MenuItemList
		{
			get
			{
				return this.menuItemCollection;
			}
		}

		/// <summary>
		/// Control to add custom errors for validation.
		/// </summary>
		/// <value>
		/// The multi validator default.
		/// </value>
		public Dionysos.Web.UI.WebControls.MultiValidator MultiValidatorDefault
		{
			get
			{
				if (this.multiValidator == null)
					return this.ControlList.FindControl(PageDefault.MultiValidatorName) as Dionysos.Web.UI.WebControls.MultiValidator;
				else
					return multiValidator;
			}
		}

		/// <summary>
		/// Gets or sets the localizeTitle, which determines if the Title is localized
		/// </summary>
		/// <value>
		///   <c>true</c> if [localize title]; otherwise, <c>false</c>.
		/// </value>
		public bool LocalizeTitle
		{
			get
			{
				return this.localizeTitle;
			}
			set
			{
				this.localizeTitle = value;
			}
		}

		#endregion
	}
}