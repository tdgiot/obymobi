using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web;
using System.Collections.Specialized;
using System.Collections;
using System.Web.UI.WebControls;
using System.IO;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Gui class which acts as a layoutMarkup and merging container for pages
    /// </summary>
    public class MasterPage : System.Web.UI.MasterPage
    {
        #region Properties

        /// <summary>
        /// FaviconFile to use, if not defined /favicon.ico is used
        /// </summary>
        public static string FaviconFile
        {
            get
            {
                return (string)HttpContext.Current.Items["MasterPageFaviconFile"] ?? String.Empty;
            }
            set
            {
                HttpContext.Current.Items["MasterPageFaviconFile"] = value;
            }
        }

        /// <summary>
        /// Collection of Css files (relative to application folder, prefixed by the ~/ notation) that will be loaded by the MasterPage
        /// </summary>
        public static LinkedItemCollection LinkedCssFiles
        {
            get
            {
                if (HttpContext.Current.Items["MasterPageLinkedCssFiles"] == null)
                {
                    HttpContext.Current.Items["MasterPageLinkedCssFiles"] = new LinkedItemCollection();
                }

                return HttpContext.Current.Items["MasterPageLinkedCssFiles"] as LinkedItemCollection;
            }
        }

        /// <summary>
        /// Gets the linked CSS set attributes.
        /// </summary>
        public static Dictionary<string, StringDictionary> LinkedCssSetAttributes
        {
            get
            {
                if (HttpContext.Current.Items["LinkedCssSetAttributes"] == null)
                {
                    HttpContext.Current.Items["LinkedCssSetAttributes"] = new Dictionary<string, StringDictionary>();
                }

                return HttpContext.Current.Items["LinkedCssSetAttributes"] as Dictionary<string, StringDictionary>;
            }
        }

        /// <summary>
        /// Collection of Js files (relative to application folder, prefixed by the ~/ notation) that will be loaded by the MasterPage
        /// </summary>
        public static LinkedItemCollection LinkedJsFiles
        {
            get
            {
                if (HttpContext.Current.Items["MasterPageLinkedJsFiles"] == null)
                {
                    HttpContext.Current.Items["MasterPageLinkedJsFiles"] = new LinkedItemCollection();
                }

                return HttpContext.Current.Items["MasterPageLinkedJsFiles"] as LinkedItemCollection;
            }
        }

        /// <summary>
        /// Gets the linked JS set attributes.
        /// </summary>
        public static Dictionary<string, StringDictionary> LinkedJsSetAttributes
        {
            get
            {
                if (HttpContext.Current.Items["LinkedJsSetAttributes"] == null)
                {
                    HttpContext.Current.Items["LinkedJsSetAttributes"] = new Dictionary<string, StringDictionary>();
                }

                return HttpContext.Current.Items["LinkedJsSetAttributes"] as Dictionary<string, StringDictionary>;
            }
        }

        /// <summary>
        /// Required to be overwriteable when running (temporarily) without a DB
        /// </summary>
        protected virtual bool CssJsCombining
        {
            get
            {
                bool cssJsCombining = false;
                try
                {
                    cssJsCombining = ConfigurationManager.GetBool(DionysosWebConfigurationConstants.CssJsCombining);
                }
                catch
                {
                    cssJsCombining = false;
                }
                return cssJsCombining;
            }
        }

        /// <summary>
        /// Required to be overwriteable when running (temporarily) without a DB
        /// </summary>
        protected virtual bool CssExplicitlyMediaScreenOnly
        {
            get
            {
                bool cssExplicitlyMediaScreenOnly = false;
                try // Required to run without a DB
                {
                    cssExplicitlyMediaScreenOnly = ConfigurationManager.GetBool(DionysosWebConfigurationConstants.CssExplicitlyMediaScreenOnly);
                }
                catch { cssExplicitlyMediaScreenOnly = false; }
                return cssExplicitlyMediaScreenOnly;
            }
        }

        /// <summary>
        /// Required to be overwriteable when running (temporarily) without a DB
        /// </summary>
        protected virtual string GetGoogleAnalyticsTrackerId()
        {
            return MasterPage.GoogleAnalyticsTrackerId;
        }

        /// <summary>
        /// Gets or sets the Google Analytics tracker id.
        /// </summary>
        public static string GoogleAnalyticsTrackerId
        {
            get
            {
                if (HttpContext.Current.Items["GoogleAnalyticsTrackerId"] == null)
                {
                    HttpContext.Current.Items["GoogleAnalyticsTrackerId"] = ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleAnalyticsTrackerId, true);
                }

                return (string)HttpContext.Current.Items["GoogleAnalyticsTrackerId"];
            }
            set
            {
                HttpContext.Current.Items["GoogleAnalyticsTrackerId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Google Analytics custom variables.
        /// </summary>
        /// <value>
        /// The Google Analytics custom variables.
        /// </value>
        public static List<GACustomVariable> GoogleAnalyticsCustomVariables
        {
            get
            {
                if (HttpContext.Current.Items["GoogleAnalyticsCustomVariables"] == null)
                {
                    HttpContext.Current.Items["GoogleAnalyticsCustomVariables"] = new List<GACustomVariable>();
                }

                return (List<GACustomVariable>)HttpContext.Current.Items["GoogleAnalyticsCustomVariables"];
            }
            set
            {
                HttpContext.Current.Items["GoogleAnalyticsCustomVariables"] = value;
            }
        }

        /// <summary>
        /// Optional setting to set the Google Analytics Page Url that will be tracked
        /// </summary>		
        public static string GoogleAnayticsTrackingPageUrl
        {
            get
            {
                return (string)HttpContext.Current.Items["GoogleAnayticsTrackingPageUrl"] ?? String.Empty;
            }
            set
            {
                HttpContext.Current.Items["GoogleAnayticsTrackingPageUrl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Google Analytics Site Speed is enabled.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if Google Analytics Site Speed is enabled; otherwise, <c>false</c>.
        /// </value>
        public static bool GoogleAnalyticsSiteSpeed
        {
            get
            {
                if (HttpContext.Current.Items["GoogleAnalyticsSiteSpeed"] == null)
                {
                    HttpContext.Current.Items["GoogleAnalyticsSiteSpeed"] = ConfigurationManager.GetBool(DionysosWebConfigurationConstants.GoogleAnalyticsSiteSpeed, true);
                }

                return (bool)HttpContext.Current.Items["GoogleAnalyticsSiteSpeed"];
            }
            set
            {
                HttpContext.Current.Items["GoogleAnalyticsSiteSpeed"] = value;
            }
        }

        /// <summary>
        /// Additional code to be added at the end of the standard tracking code
        /// </summary>
        public static string GoogleAnalyticsAdditionalCode
        {
            get
            {
                return (string)HttpContext.Current.Items["GoogleAnalyticsAdditionalCode"] ?? String.Empty;
            }
            set
            {
                HttpContext.Current.Items["GoogleAnalyticsAdditionalCode"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the javascriptVariables which will be rendered on the Page to be consumed by other scripts these are rendered before any js includes.
        /// </summary>
        public static OrderedDictionary JavascriptVariables
        {
            get
            {
                if (HttpContext.Current.Items["MasterPageJavascriptVariables"] == null)
                {
                    HttpContext.Current.Items["MasterPageJavascriptVariables"] = new OrderedDictionary();
                }

                return HttpContext.Current.Items["MasterPageJavascriptVariables"] as OrderedDictionary;
            }
        }

        /// <summary>
        /// Gets or sets whether favicon.ico must be linked.
        /// </summary>
        public bool LinkFavicon
        {
            get
            {
                return (bool?)this.Context.Items["LinkFavicon"] ?? true;
            }
            set
            {
                this.Context.Items["LinkFavicon"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the canonical URL.
        /// </summary>
        /// <value>
        /// The canonical URL.
        /// </value>
        /// <remarks>
        /// If set, adds a <c><link rel="canonical" href="" /></c> tag to the header.
        /// </remarks>
        public string CanonicalUrl
        {
            get
            {
                return (string)this.Context.Items["CanonicalUrl"];
            }
            set
            {
                this.Context.Items["CanonicalUrl"] = value;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            this.PreRender += new EventHandler(MasterPage_PreRender);

            base.OnInit(e);
        }

        /// <summary>
        /// Handles the PreRender event of the MasterPage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void MasterPage_PreRender(object sender, EventArgs e)
        {
            // To work with nested master pages only the most super masterpage will render:
            if (this.Master == null)
            {
                // Register favicon
                if (this.LinkFavicon)
                {
                    if (!String.IsNullOrEmpty(MasterPage.FaviconFile))
                    {
                        this.Page.Header.Controls.Add(new LiteralControl(String.Format("<link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"{0}\" />", this.ResolveUrl(MasterPage.FaviconFile))));
                    }
                    else
                    {
                        this.Page.Header.Controls.Add(new LiteralControl(String.Format("<link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"{0}\" />", this.ResolveUrl("~/favicon.ico"))));
                    }
                }

                // Add canonical url
                if (!String.IsNullOrEmpty(this.CanonicalUrl))
                {
                    this.Page.Header.Controls.Add(new LiteralControl(String.Format("<link rel=\"canonical\" href=\"{0}\" />", this.ResolveUrl(this.CanonicalUrl))));
                }

                // Register Javascript variables
                JavascriptVariables.Add("baseUrl", WebShortcuts.BaseUrl);

                string variables = "var aspJsVars = {};";
                foreach (DictionaryEntry de in JavascriptVariables)
                {
                    variables += string.Format("aspJsVars[\'{0}\'] = '{1}';", de.Key, de.Value);
                }

                this.Page.Header.Controls.Add(new LiteralControl(String.Format("<script>var baseUrl = '{0}';{1}</script>", WebShortcuts.BaseUrl, variables)));

                // Render added styles and scripts
                this.RenderStyleSheetLinks(this.CssJsCombining);
                this.RenderJavascriptIncludes(this.CssJsCombining);

                // Google analytics code
                string googleAnalyticsKey = this.GetGoogleAnalyticsTrackerId();
                ContentPlaceHolder cph = this.FindControl("cphGoogleAnalytics") as ContentPlaceHolder;
                if (!String.IsNullOrEmpty(googleAnalyticsKey))
                {
                    if (cph != null)
                    {
                        // Constuct cache key
                        string trackerCacheKey = "GoogleAnalytics-" + googleAnalyticsKey;
                        bool trackerCacheEnabled = !this.Request.IsLocal &&
                                                    String.IsNullOrEmpty(GoogleAnayticsTrackingPageUrl) &&
                                                    GoogleAnalyticsCustomVariables.Count == 0 &&
                                                    !(this.Response.StatusCode >= 400 && this.Response.StatusCode < 500);

                        // Get tracker HTML code
                        string trackerCode;
                        if (!trackerCacheEnabled || !CacheHelper.TryGetValue(trackerCacheKey, false, out trackerCode))
                        {
                            StringBuilder sbTrackerCode = new StringBuilder();

                            // Add opening comment when requesting from localhost
                            if (this.Request.IsLocal)
                            {
                                sbTrackerCode.Append("<!-- Google analytics isn't activated on localhost\r\n");
                            }

                            // Add default code
                            sbTrackerCode.Append("<script>");
                            sbTrackerCode.Append("var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");");
                            sbTrackerCode.Append("document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js'%3E%3C/script%3E\"));");
                            sbTrackerCode.Append("</script>");
                            sbTrackerCode.Append("<script>");
                            sbTrackerCode.Append("try {");
                            sbTrackerCode.AppendFormat("var pageTracker = _gat._getTracker(\"{0}\");", googleAnalyticsKey);

                            // Add specific domain (so no cookies will be set on the sub-domains)
                            string preferredDomain = ConfigurationManager.GetString(DionysosWebConfigurationConstants.PreferredDomain, true);
                            if (!String.IsNullOrEmpty(preferredDomain))
                            {
                                sbTrackerCode.AppendFormat("pageTracker._setDomainName(\"{0}\");", preferredDomain);
                            }

                            // Add custom variables
                            foreach (GACustomVariable customVariable in GoogleAnalyticsCustomVariables)
                            {
                                sbTrackerCode.Append(customVariable.ToString());
                            }

                            // Add tacking page url
                            if (!String.IsNullOrEmpty(GoogleAnayticsTrackingPageUrl))
                            {
                                // Specific tracking page url
                                sbTrackerCode.AppendFormat("pageTracker._trackPageview('{0}');", GoogleAnayticsTrackingPageUrl);
                            }
                            else if (this.Response.StatusCode >= 400 && this.Response.StatusCode < 600)
                            {
                                // Custom error tracking page urls
                                if (this.Request.UrlReferrer != null)
                                {
                                    sbTrackerCode.AppendFormat("pageTracker._trackPageview('/errors/{0}?page={1}&from={2}');", this.Response.StatusCode, this.Request.RawUrl.HtmlEncode(), this.Request.UrlReferrer.ToString().HtmlEncode());
                                }
                                else
                                {
                                    sbTrackerCode.AppendFormat("pageTracker._trackPageview('/errors/{0}?page={1}');", this.Response.StatusCode, this.Request.RawUrl.HtmlEncode());
                                }
                            }
                            else
                            {
                                sbTrackerCode.Append("pageTracker._trackPageview();");
                            }

                            // Site Speed
                            if (GoogleAnalyticsSiteSpeed)
                            {
                                sbTrackerCode.Append("pageTracker._trackPageLoadTime();");
                            }

                            sbTrackerCode.Append("} catch(err) {}");
                            sbTrackerCode.Append("</script>");

                            // Add closing comment when requesting from localhost
                            if (this.Request.IsLocal)
                            {
                                sbTrackerCode.Append("\r\n-->");
                            }

                            // Set tracker code
                            trackerCode = sbTrackerCode.ToString();

                            // Cache generated tracker code
                            if (trackerCacheEnabled)
                            {
                                CacheHelper.AddSlidingExpire(false, trackerCacheKey, trackerCode, 60 * 10);
                            }
                        }

                        cph.Controls.Add(new LiteralControl(trackerCode));

                        // Additional Analytics Code
                        if (!String.IsNullOrEmpty(MasterPage.GoogleAnalyticsAdditionalCode))
                        {
                            StringBuilder sbAdditionalCode = new StringBuilder();

                            if (this.Request.IsLocal)
                            {
                                sbAdditionalCode.Append("<!-- Google analytics isn't activated on localhost \r\n");
                            }

                            sbAdditionalCode.AppendFormat("<script>{0}</script>", MasterPage.GoogleAnalyticsAdditionalCode);

                            if (this.Request.IsLocal)
                            {
                                sbAdditionalCode.Append("--> \r\n");
                            }

                            cph.Controls.Add(new LiteralControl(sbAdditionalCode.ToString()));
                        }
                    }
                    else
                    {
                        throw new TechnicalException("Als er gebruik wordt gemaakt van Google Analytics dient er een ContentPlaceHolder met de ID 'cphGoogleAnalytics' aanwezig te zijn.");
                    }
                }
                else if (cph != null && cph.Visible)
                {
                    throw new TechnicalException("Als er gebruik wordt gemaakt van Google Analytics dient er een Google Analytics key aanwezig te zijn in de configuratie.");
                }
            }
        }

        #endregion

        #region Methods

        // GK Place here if needed in another project (couldn't see it when compiling)
        //private void RenderStyleSheetLinks()
        //{
        //    this.RenderStyleSheetLinks(ConfigurationManager.GetBool(DionysosWebConfigurationConstants.CssJsCombining));
        //}

        /// <summary>
        /// Renders the style sheet links.
        /// </summary>
        private void RenderStyleSheetLinks(bool combine)
        {


            if (combine)
            {
                // Add combined CSS files per set
                List<LinkedItemCollection> sets = LinkedCssFiles.GetSets();
                foreach (LinkedItemCollection set in sets)
                {
                    // Sort the files within the set
                    set.Sort();

                    StringBuilder sb = new StringBuilder("~/css/css.axd?files=");

                    // Append files
                    for (int i = 0; i < set.Count; i++)
                    {
                        // Add file
                        sb.Append(set[i].Link.Replace("~/css/", ""));
                        if (i + 1 < set.Count)
                        {
                            sb.Append(",");
                        }
                    }

                    // Add version to link (for caching)
                    sb.AppendFormat("&version={0}", set.VersionId);

                    HtmlLink linkCss = new HtmlLink();
                    linkCss.Attributes.Add("rel", "stylesheet");
                    linkCss.Attributes.Add("type", "text/css");
                    linkCss.Href = this.ResolveUrl(sb.ToString());

                    if (this.CssExplicitlyMediaScreenOnly)
                        linkCss.Attributes.Add("media", "screen");

                    StringDictionary attributes;
                    if (LinkedCssSetAttributes.TryGetValue(set[0].SetName, out attributes))
                    {
                        foreach (DictionaryEntry attribute in attributes)
                        {
                            linkCss.Attributes.Add((string)attribute.Key, (string)attribute.Value);
                        }
                    }

                    this.Page.Header.Controls.Add(linkCss);
                }
            }
            else
            {
                // Add tag for each element
                LinkedCssFiles.Sort();

                for (int i = 0; i < LinkedCssFiles.Count; i++)
                {
                    HtmlLink linkCss = new HtmlLink();
                    linkCss.Attributes.Add("rel", "stylesheet");
                    linkCss.Attributes.Add("type", "text/css");
                    linkCss.Href = String.Format("{0}?version={1}", this.ResolveUrl(LinkedCssFiles[i].Link), LinkedCssFiles[i].VersionId);

                    if (this.CssExplicitlyMediaScreenOnly)
                        linkCss.Attributes.Add("media", "screen");

                    StringDictionary attributes;
                    if (LinkedCssSetAttributes.TryGetValue(LinkedCssFiles[i].SetName, out attributes))
                    {
                        foreach (DictionaryEntry attribute in attributes)
                        {
                            linkCss.Attributes.Add((string)attribute.Key, (string)attribute.Value);
                        }
                    }

                    this.Page.Header.Controls.Add(linkCss);
                }
            }
        }

        // GK Place here if needed in another project (couldn't see it when compiling)
        //private void RenderJavascriptIncludes()        
        //{
        //    this.RenderJavascriptIncludes(ConfigurationManager.GetBool(DionysosWebConfigurationConstants.CssJsCombining));
        //}

        /// <summary>
        /// Renders the javascript includes.
        /// </summary>
        private void RenderJavascriptIncludes(bool combine)
        {
            if (combine)
            {
                // Add combined JavasSript files per set
                List<LinkedItemCollection> sets = LinkedJsFiles.GetSets();
                foreach (LinkedItemCollection set in sets)
                {
                    // Sort the files within the set
                    set.Sort();

                    StringBuilder sb = new StringBuilder("~/js/js.axd?files=");

                    // Append files
                    for (int i = 0; i < set.Count; i++)
                    {
                        // Add file
                        sb.Append(set[i].Link.ToLower().Replace("~/js/", ""));
                        if (i + 1 < set.Count)
                        {
                            sb.Append(",");
                        }
                    }

                    // Add version to link (for caching)
                    sb.AppendFormat("&version={0}", set.VersionId);

                    HtmlGenericControl linkJs = new HtmlGenericControl("script");
                    linkJs.Attributes.Add("src", this.ResolveUrl(sb.ToString()));

                    StringDictionary attributes;
                    if (LinkedJsSetAttributes.TryGetValue(set[0].SetName, out attributes))
                    {
                        foreach (DictionaryEntry attribute in attributes)
                        {
                            linkJs.Attributes.Add((string)attribute.Key, (string)attribute.Value);
                        }
                    }

                    this.Page.Header.Controls.Add(linkJs);
                }
            }
            else
            {
                LinkedJsFiles.Sort();

                for (int i = 0; i < LinkedJsFiles.Count; i++)
                {
                    HtmlGenericControl linkJs = new HtmlGenericControl("script");
                    linkJs.Attributes.Add("src", String.Format("{0}?version={1}", this.ResolveUrl(LinkedJsFiles[i].Link), LinkedJsFiles[i].VersionId));

                    StringDictionary attributes;
                    if (LinkedJsSetAttributes.TryGetValue(LinkedJsFiles[i].SetName, out attributes))
                    {
                        foreach (DictionaryEntry attribute in attributes)
                        {
                            linkJs.Attributes.Add((string)attribute.Key, (string)attribute.Value);
                        }
                    }

                    this.Page.Header.Controls.Add(linkJs);
                }
            }
        }

        #endregion

        #region Classes

        /// <summary>
        /// Represents a Google Analytics Custom Variable.
        /// </summary>
        public class GACustomVariable
        {
            #region Properties

            /// <summary>
            /// Gets the index.
            /// </summary>
            public int Index { get; private set; }

            /// <summary>
            /// Gets the name.
            /// </summary>
            public string Name { get; private set; }

            /// <summary>
            /// Gets the value.
            /// </summary>
            public string Value { get; private set; }

            /// <summary>
            /// Gets the scope.
            /// </summary>
            public GACustomVariableScope Scope { get; private set; }

            #endregion

            #region Constructors

            /// <summary>
            /// Initializes a new instance of the <see cref="GACustomVariable"/> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="name">The name.</param>
            /// <param name="value">The value.</param>
            public GACustomVariable(int index, string name, string value)
            {
                if (index < 1 || index > 5) throw new ArgumentOutOfRangeException("index", index, "Index must be between 1 and 5 (inclusive).");
                if (name == null) throw new ArgumentNullException("name");
                if (String.IsNullOrEmpty(name)) throw new ArgumentException("Name can not be empty.", "name");
                if (value == null) throw new ArgumentNullException("value");
                if (String.IsNullOrEmpty(value)) throw new ArgumentException("Value can not be empty.", "value");

                this.Index = index;
                this.Name = name;
                this.Value = value;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="GACustomVariable"/> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="name">The name.</param>
            /// <param name="value">The value.</param>
            /// <param name="scope">The scope.</param>
            public GACustomVariable(int index, string name, string value, GACustomVariableScope scope)
                : this(index, name, value)
            {
                this.Scope = scope;
            }

            #endregion

            #region Methods

            /// <summary>
            /// Returns a <see cref="System.String"/> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String"/> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                if (this.Scope == GACustomVariableScope.Undefined)
                {
                    return String.Format("pageTracker._setCustomVar({0}, '{1}', '{2}');", this.Index, this.Name.HtmlEncode(), this.Value.HtmlEncode());
                }
                else
                {
                    return String.Format("pageTracker._setCustomVar({0}, '{1}', '{2}', {3});", this.Index, this.Name.HtmlEncode(), this.Value.HtmlEncode(), (int)this.Scope);
                }
            }

            #endregion

            #region Enums

            /// <summary>
            /// The scope of the Google Analytics Custom Variable.
            /// </summary>
            public enum GACustomVariableScope
            {
                /// <summary>
                /// No scope is set (defaults to page-level interaction).
                /// </summary>
                Undefined = 0,
                /// <summary>
                /// Visitor level.
                /// </summary>
                Visitor = 1,
                /// <summary>
                /// Session level.
                /// </summary>
                Session = 2,
                /// <summary>
                /// Page level.
                /// </summary>
                Page = 3
            }

            #endregion
        }

        #endregion
    }
}
