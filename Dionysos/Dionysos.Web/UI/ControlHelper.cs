﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI
{
	public static class ControlHelper
	{
		/// <summary>
		/// Get the namespace of the first relevant parent of this control
		/// </summary>
		/// <param name="control">Control</param>
		public static string GetParentNameSpaceForTranslationKey(this Control control)
		{
			Control parentControl = GetParentForTranslationKey(control);

			string toReturn = string.Empty;
			if (parentControl.GetType().FullName.StartsWith("ASP."))
			{
				// Require Base Type 
				toReturn = parentControl.GetType().BaseType.FullName;
			}
			else
				toReturn = parentControl.GetType().FullName;

			return toReturn;
		}

		/// <summary>
		/// Get the parent Masterpage, Page or UserControl for a control, if a UserControl is availalbe, that's returned, otherwise the Page and otherwise the MasterPage
		/// </summary>
		/// <param name="control"></param>
		/// <returns></returns>
		public static Control GetParentForTranslationKey(this Control control)
		{
			Control parent = null;

			if (Instance.Empty(control.Parent))
			{
				// Control has no parent
			}
			else if (typeof(UserControl).IsInstanceOfType(control.Parent) || typeof(UserControl).IsSubclassOf(control.Parent.GetType()))
			{
				// UserControl
				parent = control.Parent;
			}
			else if (typeof(ContentPlaceHolder).IsInstanceOfType(control.Parent) || typeof(ContentPlaceHolder).IsSubclassOf(control.Parent.GetType()))
			{
				// Page via ContentPlaceHolder (required if it's having a MasterPage) otherwise the MasterPage would be found first
				parent = control.Parent.Page;
			}
			else if (typeof(Page).IsInstanceOfType(control.Parent) || typeof(Page).IsSubclassOf(control.Parent.GetType()))
			{
				// Page as Page, when not contained in a MasterPage
				parent = control.Parent;
			}
			else if (typeof(MasterPage).IsInstanceOfType(control.Parent) || typeof(MasterPage).IsSubclassOf(control.Parent.GetType()))
			{
				// MasterPage
				parent = control.Parent;
			}
			else
			{
				parent = GetParentForTranslationKey(control.Parent);
			}

			return parent;
		}

		/// <summary>
		/// Recursively gets the first parent of the specified control which has the specified type
		/// </summary>
		/// <typeparam name="T">The type of the parent control to find.</typeparam>
		/// <param name="control">The control to get the parent from</param>
		/// <returns>
		/// A System.Web.UI.Control instance representing the parent
		/// </returns>
		public static Control GetParentByType<T>(this Control control)
			where T : Control
		{
			return GetParentByType(control, typeof(T));
		}

		/// <summary>
		/// Recursively gets the first parent of the specified control which has the specified type
		/// </summary>
		/// <param name="control">The control to get the parent from</param>
		/// <param name="parentType">The type of the parent to get</param>
		/// <returns>
		/// A System.Web.UI.Control instance representing the parent
		/// </returns>
		public static Control GetParentByType(this Control control, Type parentType)
		{
			Control parent = null;

			if (Instance.Empty(control.Parent))
			{
				// Control has no parent
			}
			else if (parentType.IsInstanceOfType(control.Parent))
			{
				parent = control.Parent;
			}
			else if (parentType.IsSubclassOf(control.Parent.GetType()))
			{
				parent = control.Parent;
			}
			else
			{
				parent = GetParentByType(control.Parent, parentType);
			}

			return parent;
		}

		/// <summary>
		/// Gets the parent UI element of the specified control
		/// </summary>
		/// <param name="control">The control to get the parent UI element for</param>
		/// <returns>A System.Web.UI.Control instance representing the parent UI element</returns>
		public static Control GetParentUIElement(this Control control)
		{
			Control parent = null;

			if (Instance.Empty(control.Parent))
			{
				// Control has no parent
			}
			else if (control.Parent is Page)
			{
				parent = control.Parent;
			}
			else if (control.Parent is Dionysos.Web.UI.WebControls.SubPanel)
			{
				parent = control.Parent;
			}
			else
			{
				parent = GetParentUIElement(control.Parent);
			}

			return parent;
		}

		/// <summary>
		/// Finds a control recursively on id in the child controls of the specified control
		/// </summary>
		/// <param name="parent">The control which if the parent if the control to find</param>
		/// <param name="id">The id of the control to find</param>
		/// <returns>A System.Web.UI.Control instance</returns>
		public static Control FindControlRecursively(this Control parent, string id)
		{
			Control control = null;

			if (Instance.ArgumentIsEmpty(parent, "parent"))
			{
				// Parameter 'parent' is empty
			}
			else if (Instance.ArgumentIsEmpty(id, "id"))
			{
				// Parameter 'id' is empty
			}
			else
			{
				for (int i = 0; i < parent.Controls.Count; i++)
				{
					Control temp = parent.Controls[i];
					if (temp.ID == id)
					{
						control = temp;
						break;
					}
					else
					{
						control = FindControlRecursively(temp, id);
						if (!Instance.Empty(control))
						{
							break;
						}
					}
				}
			}

			return control;
		}

		/// <summary>
		/// Finds the control recursively.
		/// </summary>
		/// <typeparam name="T">The type of control to find.</typeparam>
		/// <param name="parent">The parent.</param>
		/// <returns>
		/// The child control of the specified type.
		/// </returns>
		public static T FindControlRecursively<T>(this Control parent)
			where T : Control
		{
			return FindControlRecursively<T>(parent, null);
		}

		/// <summary>
		/// Finds the control recursively.
		/// </summary>
		/// <typeparam name="T">The type of control to find.</typeparam>
		/// <param name="parent">The parent.</param>
		/// <param name="id">The id.</param>
		/// <returns>
		/// The child control of the specified type.
		/// </returns>
		public static T FindControlRecursively<T>(this Control parent, string id)
			where T : Control
		{
			if (parent == null)
			{
				throw new ArgumentNullException("parent");
			}

			Control control = null;

			foreach (Control child in parent.Controls)
			{
				if (child is T &&
					(String.IsNullOrEmpty(id) || child.ID == id))
				{
					control = child;
					break;
				}
				else
				{
					control = FindControlRecursively<T>(child);
					if (control != null)
					{
						break;
					}
				}
			}

			return control as T;
		}

		/// <summary>
		/// Determines whether the control is empty (no visible child controls or empty literal controls).
		/// </summary>
		/// <param name="control">The control.</param>
		/// <returns>
		///   <c>true</c> if the control is empty; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsControlEmpty(this Control control)
		{
			bool isEmpty = control.Controls.Count == 0;
			if (!isEmpty)
			{
				isEmpty = true;
				foreach (Control childControl in control.Controls)
				{
					if (!childControl.Visible) continue;
					else if (childControl is LiteralControl && StringUtil.IsNullOrWhiteSpace(((LiteralControl)childControl).Text)) continue;
					else
					{
						isEmpty = false;
						break;
					}
				}
			}

			return isEmpty;
		}

		/// <summary>
		/// Renders a Control to raw HTML.
		/// </summary>
		/// <param name="ctrl">The Control to render.</param>
		/// <returns>The rendered HTML.</returns>
		public static string RenderControl(this Control ctrl)
		{
			StringBuilder sb = new StringBuilder();
			using (StringWriter tw = new StringWriter(sb))
			using (HtmlTextWriter hw = new HtmlTextWriter(tw))
			{
				ctrl.RenderControl(hw);
			}

			return sb.ToString();
		}

		/// <summary>
		/// Sets the ValidationGroup property of all child controls to the specified value.
		/// </summary>
		/// <param name="parentControl">The control to recurse.</param>
		/// <param name="validationGroup">The ValidationGroup to set.</param>
		public static void SetValidationGroup(this Control parentControl, string validationGroup)
		{
			foreach (Control control in parentControl.Controls)
			{
				if (control is BaseValidator)
				{
					(control as BaseValidator).ValidationGroup = validationGroup;
				}
				else if (control is IExtendedValidator)
				{
					(control as IExtendedValidator).ValidationGroup = validationGroup;
				}
				else if (control is ValidationSummary)
				{
					(control as ValidationSummary).ValidationGroup = validationGroup;
				}
				else if (control is IButtonControl)
				{
					(control as IButtonControl).ValidationGroup = validationGroup;
				}

				SetValidationGroup(control, validationGroup);
			}
		}

		/// <summary>
		/// Sets the use validation.
		/// </summary>
		/// <param name="parentControl">The parent control.</param>
		/// <param name="useValidation">The value to set.</param>
		public static void SetUseValidation(this Control parentControl, bool useValidation)
		{
			foreach (Control control in parentControl.Controls)
			{
				if (control is IExtendedValidator)
				{
					(control as IExtendedValidator).UseValidation = useValidation;
				}

				SetUseValidation(control, useValidation);
			}
		}

        /// <summary>
        /// Converts a URL into one that is usable on the requesting client.
        /// </summary>
        /// <param name="control">The control to get the URL from.</param>
        /// <param name="relativeUrlFormat">A composite format string.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        /// <returns>A string containing the resolved URL.</returns>
        public static string ResolveUrl(this Control control, string relativeUrlFormat, params object[] args)
        {
            return control.ResolveUrl(string.Format(relativeUrlFormat, args));
        }
	}
}