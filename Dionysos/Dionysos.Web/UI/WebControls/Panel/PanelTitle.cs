﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    public class PanelTitle : Panel
    {
        #region Fields

        private string headingText = string.Empty;
        private string headingCssClass = string.Empty;
        private string bodyCssClass = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.PanelTitle class
        /// </summary>
        public PanelTitle()
        {
        }

        #endregion

        #region Methods

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            // Create and initialize a table
            Table table = new Table();
            table.CellPadding = 0;
            table.CellSpacing = 0;
            table.Width = new Unit("100%");
            table.Height = new Unit("100%");

            TableRow headingRow = new TableRow();

            TableCell headingCell = new TableCell();
            if (!Instance.Empty(this.headingCssClass))
            {
                headingCell.CssClass = this.headingCssClass;
            }

            Label headingLabel = new Label();
            headingLabel.Text = this.headingText;
            headingCell.Controls.Add(headingLabel);

            headingRow.Cells.Add(headingCell);
            table.Rows.Add(headingRow);

            TableRow bodyRow = new TableRow();

            TableCell bodyCell = new TableCell();
            if (!Instance.Empty(this.bodyCssClass))
            {
                bodyCell.CssClass = this.bodyCssClass;
            }
            bodyRow.Cells.Add(bodyCell);
            table.Rows.Add(bodyRow);

            this.Controls.Add(table);
            
            base.Render(writer);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the heading text of this panel
        /// </summary>
        [Browsable(true)]
        public string HeadingText
        {
            get
            {
                return this.headingText;
            }
            set
            {
                this.headingText = value;
            }
        }

        /// <summary>
        /// Gets or sets the css class of the heading
        /// </summary>
        [Browsable(true)]
        public string HeadingCssClass
        {
            get
            {
                return this.headingCssClass;
            }
            set
            {
                this.headingCssClass = value;
            }
        }

        /// <summary>
        /// Gets or sets the css class of the body
        /// </summary>
        [Browsable(true)]
        public string BodyCssClass
        {
            get
            {
                return this.bodyCssClass;
            }
            set
            {
                this.bodyCssClass = value;
            }
        }

        #endregion
    }
}
