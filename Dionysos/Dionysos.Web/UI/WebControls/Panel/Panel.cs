using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using Dionysos.Globalization;

namespace Dionysos.Web.UI.WebControls
{
	public class Panel : System.Web.UI.WebControls.Panel, ILocalizableControl
	{
		#region Properties

		/// <summary>
		/// Enable/Disable Localization for "Text" when the Parent page has localization
		/// </summary>
		[Category("Localization"), DefaultValue(true)]
		public bool LocalizeText
		{
			get
			{
				return (this.ViewState["LocalizeText"] as bool?) ?? true;
			}
			set
			{
				this.ViewState["LocalizeText"] = value;
			}
		}

		/// <summary>
		/// Get/Set when panel should be displayed (Default: Always)
		/// </summary>
		[Category("Appearance"), DefaultValue(ControlDisplayInMode.Always)]
		public ControlDisplayInMode DisplayInMode
		{
			get
			{
				return (this.ViewState["DisplayInMode"] as ControlDisplayInMode?) ?? ControlDisplayInMode.Always;
			}
			set
			{
				this.ViewState["DisplayInMode"] = value;
			}
		}

        [Category("Localization")]
        public string TranslationTag
        {
            get
            {
                if (ViewState["TranslationTag"] != null)
                {
                    return (string)ViewState["TranslationTag"];
                }
                else
                {
                    return string.Empty;
                }
            }
            set { ViewState["TranslationTag"] = value; }
        }

		#endregion

		#region Methods

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the control content.</param>
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if (this.DisplayInMode != ControlDisplayInMode.Always)
			{
				// Conditional rendering
				bool renderReadOnly = false;
				if (this.Page.GetType().GetProperty("RenderReadOnly") != null)
				{
					renderReadOnly = Convert.ToBoolean(this.Page.GetType().GetProperty("RenderReadOnly").GetValue(this.Page, null));
				}

				if (renderReadOnly)
				{
					if (this.DisplayInMode == ControlDisplayInMode.ReadOnly) base.Render(writer);
				}
				else if (this.DisplayInMode == ControlDisplayInMode.OnEditable)
				{
					base.Render(writer);
				}
			}
			else
			{
				base.Render(writer);
			}
		}

		#endregion

        #region Localization Logic

        protected Dictionary<string, Translatable> localizableProperties = null;
        /// <summary>
        /// Gets / sets all information for the localizable properties
        /// </summary>
        public Dictionary<string, Translatable> TranslatableProperties
        {
            get
            {
                if (this.localizableProperties == null)
                {
                    this.localizableProperties = new Dictionary<string, Translatable>();

                    // TranslationKey Prefix
                    string translationKey = string.Empty;
                    if (this.ID != null && this.ID.Length > 0)
                        translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID;

                    // Text Property
                    if (this.LocalizeText)
                    {
                        Translatable textProperty = new Translatable();
                        textProperty.TranslationValue = this.GroupingText;

                        if (this.TranslationTag.Length > 0)
                            textProperty.TranslationKey = this.TranslationTag;
                        else
                        {
                            if (translationKey.Length == 0)
                                throw new Dionysos.TechnicalException("An .ID property or TranslationTag is required for controls to be localized!, failed for control with .Text: '{0}' - {1}", this.GroupingText, this.ClientID);
                            textProperty.TranslationKey = translationKey + ".GroupingText";
                        }

                        this.localizableProperties.Add("GroupingText", textProperty);
                    }
                }

                return this.localizableProperties;
            }
            set
            {
                this.localizableProperties = value;

                // Set the translated values
                if (this.localizableProperties.ContainsKey("GroupingText"))
                    this.GroupingText = this.localizableProperties["GroupingText"].TranslationValue;
            }
        }

        #endregion
    }
}
