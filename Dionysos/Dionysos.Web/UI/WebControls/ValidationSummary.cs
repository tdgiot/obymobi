using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Custom implementation of the ValidationSummary for the Dionysos Framework
	/// </summary>
	public class ValidationSummary : System.Web.UI.WebControls.ValidationSummary
	{
		/// <summary>
		/// Render is overridden to render the ErrorMessage in another format
		/// </summary>
		/// <param name="writer">writer</param>
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			bool render = false;
			StringBuilder sb = new StringBuilder();

			sb.AppendFormat("<div id=\"{0}\" class=\"{1}\">", this.DivId, this.CssClass.Nullify() ?? "vsummary");

			if (!String.IsNullOrEmpty(this.LeadingText))
			{
				sb.AppendFormat("<p>{0}</p>", this.LeadingText);
			}

			// Validation Information
			ValidatorCollection validators;
			if (this.Page is Dionysos.Web.UI.Page)
			{
				// Get validators using custom logic (IExtendedValidator)
				validators = ((Dionysos.Web.UI.Page)this.Page).GetValidators(this.ValidationGroup);
			}
			else
			{
				// Get validators using default logic (BaseValidator)
				validators = this.Page.GetValidators(this.ValidationGroup);
			}

			// GK: Prepare for some VERY pragmatic solving of the double errors
			List<string> displayedErrors = new List<string>();
			foreach (IValidator validator in validators)
			{
				if (!validator.IsValid)
				{
					if (!render)
					{
						sb.Append("<ul>");
					}
					render = true;

					if (validator.ErrorMessage.Length > 1)
					{
						string message = validator.ErrorMessage;
						string cssClass = String.Empty;

						// Prepare if a prefix is provided (For example: INFO|| or WARNING||
						if (message.Contains("||"))
						{
							int indexOfClosingTag = message.IndexOf("||");

							// Remove leading
							cssClass = message.Substring(0, indexOfClosingTag);
							message = message.Substring(indexOfClosingTag + 2); // +2 because 2 characters are in te indexOf search

							if (!displayedErrors.Contains(message))
							{
								sb.AppendFormat("<li class=\"{0}\">{1}</li>", cssClass, message);
								displayedErrors.Add(message);
							}
						}
						else
						{
							if (!displayedErrors.Contains(message))
							{
								sb.AppendFormat("<li class=\"validation-error\">{0}</li>", message);
								displayedErrors.Add(message);
							}
						}
					}
				}
			}

			// Information/informators
			if (this.Page is Dionysos.Web.UI.Page)
			{
				List<IInformator> informators = ((Dionysos.Web.UI.Page)Page).Informators;
				foreach (IInformator informator in informators)
				{
					if (informator.InformatorType == InformatorType.Hidden) continue;

					if (!render)
					{
						sb.Append("<ul>");
					}
					render = true;

                    if (displayedErrors.Contains(informator.Text))
                    { 
                        // Skip!
                        continue;
                    }
					else if (informator.InformatorType == InformatorType.Information)
					{
						sb.AppendFormat("<li class=\"informator-information\">{0}</li>", informator.Text);
					}
					else
					{
						sb.AppendFormat("<li class=\"informator-warning\">{0}</li>", informator.Text);
					}

                    displayedErrors.Add(informator.Text);
				}
			}

			if (render)
			{
				sb.AppendFormat("</ul>");
			}

			if (!String.IsNullOrEmpty(this.ClosingText))
			{
				sb.AppendFormat("<p>{0}</p>", this.ClosingText);
			}

			sb.AppendFormat("</div>");

			if (render)
			{
				LiteralControl literalControl = new LiteralControl(sb.ToString());
				literalControl.RenderControl(writer);
			}
		}

		private string leadingText = string.Empty;
		/// <summary>
		/// This text is shown above the list of error items (For example use this to display: The following errors occures:)
		/// </summary>
		public string LeadingText
		{
			get
			{
				return this.leadingText;
			}
			set
			{
				this.leadingText = value;
			}
		}

		private string closingText = string.Empty;
		/// <summary>
		/// This text is shown after the list of error items
		/// </summary>
		public string ClosingText
		{
			get
			{
				return this.closingText;
			}
			set
			{
				this.closingText = value;
			}
		}

		private string divId = "validation-summary";
		public string DivId
		{
			get
			{
				return this.divId;
			}
			set
			{
				this.divId = value;
			}
		}
	}
}

