﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Indicates the relationship to the referenced document.
	/// </summary>
	[Flags]
	public enum RelAttribute : long
	{
		/// <summary>
		/// No rel value specified.
		/// </summary>
		None = 0L,
		/// <summary>
		/// The person represented by the current document considers the person represented by the referenced document to be an acquaintance.
		/// </summary>
		Acquaintance = 1L,
		/// <summary>
		/// Gives alternate representations of the current document.
		/// </summary>
		Alternate = 1L << 1,
		/// <summary>
		/// Indicates that the referenced document describes a collection of records, documents, or other materials of historical interest.
		/// </summary>
		Archives = 1L << 2,
		/// <summary>
		/// Gives a link to the current document's author.
		/// </summary>
		Author = 1L << 3,
		/// <summary>
		/// Refers to a bookmark. A bookmark is a link to a key entry point within an extended document. The title attribute may be used, for example, to label the bookmark. Note that several bookmarks may be defined in each document.
		/// </summary>
		Bookmark = 1L << 4,
		/// <summary>
		/// The referenced person is a child of the person represented by the current document.
		/// </summary>
		Child = 1L << 5,
		/// <summary>
		/// The referenced person lives in the same residence as the person represented by the current document.
		/// </summary>
		CoResident = 1L << 6,
		/// <summary>
		/// The referenced person is a co-worker of the person represented by the current document.
		/// </summary>
		CoWorker = 1L << 7,
		/// <summary>
		/// The referenced person is a colleague of the person represented by the current document.
		/// </summary>
		Colleague = 1L << 8,
		/// <summary>
		/// The person represented by the current document considers the person represented by the referenced document to be a contact.
		/// </summary>
		Contact = 1L << 9,
		/// <summary>
		/// This person considers the referenced person to be a crush (i.e. has a crush on the referenced person).
		/// </summary>
		Crush = 1L << 10,
		/// <summary>
		/// This person considers the referenced person to be a date (i.e. is dating the referenced person).
		/// </summary>
		Date = 1L << 11,
		/// <summary>
		/// Indicates that the referenced document is not part of the same site as the current document.
		/// </summary>
		External = 1L << 12,
		/// <summary>
		/// Refers to the first document in a collection of documents.
		/// </summary>
		First = 1L << 13,
		/// <summary>
		/// The person represented by the current document considers the person represented by the referenced document to be a friend.
		/// </summary>
		Friend = 1L << 14,
		/// <summary>
		/// Provides a link to context-sensitive help.
		/// </summary>
		Help = 1L << 15,
		/// <summary>
		/// Refers to a document providing an index for the current document.
		/// </summary>
		Index = 1L << 16,
		/// <summary>
		/// The referenced person is part of the extended family of the person represented by the current document.
		/// </summary>
		Kin = 1L << 17,
		/// <summary>
		/// Refers to the last document in a collection of documents.
		/// </summary>
		Last = 1L << 18,
		/// <summary>
		/// Indicates that the referenced document is a license for the current page.
		/// </summary>
		License = 1L << 19,
		/// <summary>
		/// The referenced document represents the same person as does the current document.
		/// </summary>
		Me = 1L << 20,
		/// <summary>
		/// This person has met the referenced person.
		/// </summary>
		Met = 1L << 21,
		/// <summary>
		/// The referenced person inspires the person represented by the current document.
		/// </summary>
		Muse = 1L << 22,
		/// <summary>
		/// The referenced person lives nearby the person represented by the current document.
		/// </summary>
		Neighbor = 1L << 23,
		/// <summary>
		/// Indicates that the current document is a part of a series, and that the next document in the series is the referenced document.
		/// </summary>
		Next = 1L << 24,
		/// <summary>
		/// Indicates that the destination of that hyperlink SHOULD NOT be afforded any additional weight or ranking by user agents which perform link analysis upon web pages (e.g. search engines).
		/// </summary>
		Nofollow = 1L << 25,
		/// <summary>
		/// Requires that the user agent not send an HTTP Referer (sic) header if the user follows the hyperlink.
		/// </summary>
		Noreferrer = 1L << 26,
		/// <summary>
		/// The referenced person is a parent of the person represented by the current document.
		/// </summary>
		Parent = 1L << 27,
		/// <summary>
		/// Gives the address of the pingback server that handles pingbacks to the current document.
		/// </summary>
		Pingback = 1L << 28,
		/// <summary>
		/// Specifies that the target resource should be preemptively cached.
		/// </summary>
		Prefetch = 1L << 29,
		/// <summary>
		/// Indicates that the current document is a part of a series, and that the previous document in the series is the referenced document.
		/// </summary>
		Prev = 1L << 30,
		/// <summary>
		/// Gives a link to a resource that can be used to search through the current document and its related pages.
		/// </summary>
		Search = 1L << 31,
		/// <summary>
		/// The referenced person is a sibling of the person represented by the current document.
		/// </summary>
		Sibling = 1L << 32,
		/// <summary>
		/// Specifies that the referenced document, if retrieved, is intended to be shown in the browser's sidebar (if it has one).
		/// </summary>
		Sidebar = 1L << 33,
		/// <summary>
		/// The referenced person is a spouse of the person represented by the current document.
		/// </summary>
		Spouse = 1L << 34,
		/// <summary>
		/// This person considers the referenced person to be their sweetheart.
		/// </summary>
		Sweetheart = 1L << 35,
		/// <summary>
		/// Indicates that the referenced document is an author-designated "tag" (or keyword/subject) for the current page.
		/// </summary>
		Tag = 1L << 36,
		/// <summary>
		/// Indicates that the referenced document is the parent of the current document in a hierarchical document collection.
		/// </summary>
		Up = 1L << 37,
		/// <summary>
		/// Indicates that the referenced document must be opened in a lightbox.
		/// </summary>
		Lightbox = 1L << 38
	}
}
