using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Web;
using System.Text.RegularExpressions;
using System.Globalization;
using Dionysos.Globalization;
using System.Linq;

namespace Dionysos.Web.UI.WebControls
{
	public class HyperLink : System.Web.UI.WebControls.HyperLink, ILocalizableControl
	{
		#region Methods

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			// Localize the NavigationUrl
			if (Dionysos.Web.HttpModules.GenericRewriteModule.IsCultureSpecifiedInUrl && this.LocalizeNavigateUrl && this.NavigateUrl.StartsWith("~/"))
			{
				string cultureString = CultureInfo.CurrentUICulture.Name.ToLowerInvariant();

				if (this.NavigateUrl.Equals("~/"))
				{
					// No trailing slash needed
					this.NavigateUrl = this.NavigateUrl.Insert(2, cultureString);
				}
				else
				{
					// Insert culture information
					this.NavigateUrl = this.NavigateUrl.Insert(1, "/" + cultureString);
				}
			}

			// Fix NavigateUrl if we have rewritten Urls
			if (this.NavigateUrl.StartsWith("~/"))
			{
				this.NavigateUrl = this.Page.ResolveUrl(this.NavigateUrl);
			}

			// Render NavigateUrl in lowercase
			if (this.LowerCaseNavigateUrl)
			{
				this.NavigateUrl = this.NavigateUrl.ToLowerInvariant();
			}

			if (this.Rel != RelAttribute.None)
			{
				// Get flagged values
				IEnumerable<RelAttribute> relFlags = EnumUtil.ToArray<RelAttribute>().Where(a => a != RelAttribute.None && this.Rel.HasFlag(a));

				// Get rel values
				string[] relValues = relFlags.Select(a => a.ToString().FormatPascalCase().GenerateSlug()).ToArray();

				// Set rel attribute
				this.Attributes["rel"] = StringUtil.CombineWithSpace(relValues);
			}

			if (this.RenderAsText)
			{
				writer.Write(this.Text);
			}
			else if (this.RenderOnlyContents)
			{
				base.RenderContents(writer);
			}
			else
			{
				base.Render(writer);
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Localize Text when the Parent page has localization
		/// </summary>
		[Category("Localization"), DefaultValue(true)]
		public bool LocalizeText
		{
			get
			{
				if (ViewState["LocalizeText"] != null)
					return (bool)ViewState["LocalizeText"];
				else
					return true;
			}
			set { ViewState["LocalizeText"] = value; }
		}

		/// <summary>
		/// The tag to use for localizing the Text.
		/// </summary>
		[Category("Localization")]
		public string TranslationTag
		{
			get
			{
				if (ViewState["TranslationTag"] != null)
				{
					return (string)ViewState["TranslationTag"];
				}
				else
				{
					return string.Empty;
				}
			}
			set { ViewState["TranslationTag"] = value; }
		}

		/// <summary>
		/// Localize a (relative) NavigateUrl by prefixing the CultrureString.
		/// </summary>
		[Category("Localization"), DefaultValue(false)]
		public bool LocalizeNavigateUrl
		{
			get
			{
				if (ViewState["LocalizeNavigateUrl"] != null)
					return (bool)ViewState["LocalizeNavigateUrl"];
				else
					return false;
			}
			set
			{
				ViewState["LocalizeNavigateUrl"] = value;
			}
		}

		/// <summary>
		/// Renders the NavigateUrl in lowercase.
		/// </summary>
		[DefaultValue(false)]
		public bool LowerCaseNavigateUrl
		{
			get
			{
				if (ViewState["LowerCaseNavigateUrl"] != null)
					return (bool)ViewState["LowerCaseNavigateUrl"];
				else
					return false;
			}
			set
			{
				ViewState["LowerCaseNavigateUrl"] = value;
			}
		}

		/// <summary>
		/// Only render text.
		/// </summary>
		public bool RenderAsText
		{
			get
			{
				if (ViewState["RenderAsText"] != null)
					return (bool)ViewState["RenderAsText"];
				else
					return false;
			}
			set
			{
				ViewState["RenderAsText"] = value;
			}
		}

		/// <summary>
		/// Only render contents.
		/// </summary>
		public bool RenderOnlyContents
		{
			get
			{
				if (ViewState["RenderOnlyContents"] != null)
					return (bool)ViewState["RenderOnlyContents"];
				else
					return false;
			}
			set
			{
				ViewState["RenderOnlyContents"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the relationship of the referenced document.
		/// </summary>
		/// <value>
		/// The relationship to the referenced document.
		/// </value>
		[DefaultValue(RelAttribute.None)]
		public RelAttribute Rel
		{
			get
			{
				return (this.ViewState["Rel"] as RelAttribute?) ?? RelAttribute.None;
			}
			set
			{
				this.ViewState["Rel"] = value;
			}
		}

		#endregion

		#region Localization Logic

		//private bool localizeText = true;
		///// <summary>
		///// Gets / Sets if the control should be localized
		///// </summary>
		//[Category("Localization"), DefaultValue(true)]		
		//public bool LocalizeText
		//{
		//    get	{ return this.localizeText; }
		//    set { this.localizeText = value; }
		//}

		//private string translationTagText = string.Empty;
		///// <summary>
		///// Gets / sets the control's TranslationTag for the Text property, when set this will be used a the TranslationKey instead of the Page's Namespace + Control.ID
		///// 
		///// </summary>
		//[Category("Localization"), DefaultValue(string.Empty)]
		//public string TranslationTagText
		//{
		//    get { return this.translationTagText; }
		//    set { this.translationTagText = value; }
		//}

		protected Dictionary<string, Translatable> localizableProperties = null;
		/// <summary>
		/// Gets / sets all information for the localizable properties
		/// </summary>
		public Dictionary<string, Translatable> TranslatableProperties
		{
			get
			{
				if (this.localizableProperties == null)
				{
					this.localizableProperties = new Dictionary<string, Translatable>();

					// TranslationKey Prefix
					string translationKey = string.Empty;
					if (this.ID != null && this.ID.Length > 0)
						translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID;

					// Text Property
					if (this.LocalizeText)
					{
						Translatable textProperty = new Translatable();
						textProperty.TranslationValue = this.Text;

						if (this.TranslationTag.Length > 0)
							textProperty.TranslationKey = this.TranslationTag;
						else
						{
							if (translationKey.Length == 0)
								throw new Dionysos.TechnicalException("An .ID property or TranslationTag is required for controls to be localized!, failed for control with .Text: '{0}' - {1}", this.Text, this.ClientID);
							textProperty.TranslationKey = translationKey + ".Text";
						}

						this.localizableProperties.Add("Text", textProperty);
					}

					// NavigateUrl Property
					if (this.LocalizeNavigateUrl)
					{
						Translatable navigateUrlProperty = new Translatable();
						navigateUrlProperty.TranslationValue = this.NavigateUrl;

						if (translationKey.Length == 0)
							throw new Dionysos.TechnicalException("An .ID property or TranslationTag is required for controls to be localized!, failed for control with .Text: '{0}' - {1}", this.Text, this.ClientID);
						navigateUrlProperty.TranslationKey = translationKey + ".NavigateUrl";

						this.localizableProperties.Add("NavigateUrl", navigateUrlProperty);
					}
				}

				return this.localizableProperties;
			}
			set
			{
				this.localizableProperties = value;

				// Set the translated values
				if (this.localizableProperties.ContainsKey("Text"))
					this.Text = this.localizableProperties["Text"].TranslationValue;
				if (this.localizableProperties.ContainsKey("NavigateUrl"))
					this.NavigateUrl = this.localizableProperties["NavigateUrl"].TranslationValue;
			}
		}

		#endregion
	}
}
