using System;
using Dionysos.Interfaces;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;
using System.ComponentModel;
using System.Web.UI;
using Dionysos.Reflection;
using Dionysos.Globalization;
using Dionysos.Web.Resources;
using System.Reflection;

namespace Dionysos.Web.UI.WebControls
{
	public class CheckBox : System.Web.UI.WebControls.CheckBox, IBindable, IExtendedValidator, ILocalizableControl
	{
		#region Fields

		private string friendlyName = String.Empty;
		private string saveAndRetrieveValueCookieName = String.Empty;
		private bool useDataBinding = true;
		private bool isValid = true;
		private string errorMessage = String.Empty;
		private bool useValidation = true;
		private Dictionary<string, Translatable> localizableProperties = null;

		/// <summary>
		/// Occurs when the value has changed.
		/// </summary>
		public event EventHandler ValueChanged;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		public bool Value
		{
			get
			{
				return this.Checked;
			}
			set
			{
				this.Checked = value;
			}
		}

		/// <summary>
		/// Gets or sets the friendly name.
		/// </summary>
		public string FriendlyName
		{
			get
			{
				if (String.IsNullOrEmpty(this.friendlyName))
				{
					// Try to find a friendly name if not set
					string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					ITextControl textControl = this.Parent.FindControl("lbl" + strippedId) as ITextControl;
					if (textControl != null)
					{
						this.friendlyName = textControl.Text;
					}
					else
					{
						this.friendlyName = strippedId;
					}
				}

				return this.friendlyName;
			}
			set
			{
				this.friendlyName = value;
			}
		}

		/// <summary>
		/// Gets or sets whether a value is required.
		/// </summary>
		public bool IsRequired { get; set; }

		/// <summary>
		/// Gets or sets the cookie name to save and retrieve the current value to (if empty, doesn't save or retrieves the value).
		/// </summary>
		public string SaveAndRetrieveValueCookieName
		{
			get
			{
				return this.saveAndRetrieveValueCookieName;
			}
			set
			{
				this.saveAndRetrieveValueCookieName = value ?? String.Empty;
			}
		}

		/// <summary>
		/// Gets or sets whether a label should be rendered.
		/// </summary>
		public bool RenderLabel { get; set; }

		#endregion

		#region Localization Properties

		/// <summary>
		/// Gets or sets whether the text is localized.
		/// </summary>
		public bool LocalizeText
		{
			get
			{
				return this.ViewState["LocalizeText"] == null || (bool)this.ViewState["LocalizeText"];
			}
			set
			{
				this.ViewState["LocalizeText"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the translation tag.
		/// </summary>
		public string TranslationTag
		{
			get
			{
				return this.ViewState["TranslationTag"] == null ? String.Empty : (string)this.ViewState["TranslationTag"];
			}
			set
			{
				this.ViewState["TranslationTag"] = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CheckBox class.
		/// </summary>
		public CheckBox()
		{
			this.Load += new EventHandler(CheckBox_Load);
			this.CheckedChanged += new EventHandler(CheckBox_CheckedChanged);
		}

		#endregion

		#region Methods

		protected override void Render(HtmlTextWriter writer)
		{
			bool renderReadOnly = false;
			PropertyInfo propertyInfo = this.Page.GetType().GetProperty("RenderReadOnly");
			if (propertyInfo != null)
			{
				renderReadOnly = Convert.ToBoolean(propertyInfo.GetValue(this.Page, null));
			}

			if (renderReadOnly || this.RenderLabel)
			{
				// Render label
				Label label = new Label();
				label.LocalizeText = false;
				label.Text = this.Checked ? "Ja" : "Nee";
				label.RenderControl(writer);
			}
			else
			{
				// Render checkbox
				base.Render(writer);
			}
		}

		#endregion

		#region Event Handlers

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			// Load value from Cookie
			if (this.SaveAndRetrieveValueCookieName != String.Empty)
			{
				HttpCookie cookie = HttpContext.Current.Request.Cookies[this.SaveAndRetrieveValueCookieName];
				if (cookie != null && !String.IsNullOrEmpty(cookie.Value))
				{
					bool isChecked;
					if (Boolean.TryParse(cookie.Value, out isChecked))
					{
						this.Checked = isChecked;
					}
				}
			}
		}

		protected void CheckBox_Load(object sender, EventArgs e)
		{
			if (this.UseValidation)
			{
				if (this.IsRequired)
				{
					this.Page.Validators.Add(this);
					this.CssClass += " required";
				}
			}
		}

		public void OnValueChanged(object sender, EventArgs e)
		{
			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

		protected void CheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.SaveAndRetrieveValueCookieName != String.Empty)
			{
				HttpCookie cookie = HttpContext.Current.Request.Cookies[this.SaveAndRetrieveValueCookieName];
				if (cookie != null)
				{
					cookie.Value = this.Checked.ToString();
					cookie.Expires = DateTime.Now.AddDays(30);
				}
				else
				{
					cookie = new HttpCookie(this.SaveAndRetrieveValueCookieName, this.Checked.ToString());
					cookie.Expires = DateTime.Now.AddDays(30);
					HttpContext.Current.Response.Cookies.Add(cookie);
				}
			}

			this.OnValueChanged(sender, e);
		}

		#endregion

		#region IBindable Members

		/// <summary>
		/// Gets or sets whether databinding is used.
		/// </summary>
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		#endregion
		
		#region IExtendedValidator Members

		/// <summary>
		/// Validates this control.
		/// </summary>
		public void Validate()
		{
			if (this.UseValidation)
			{
				// Validate required
				if (this.IsRequired && !this.Value)
				{
					this.ErrorMessage = String.Format(Dionysos_Web_UI_WebControls.CheckBox_ErrorMessage_Required, this.FriendlyName);
					this.IsValid = false;
				}

				// Add or remove css class
				if (!this.IsValid)
				{
					this.CssClass += " ivld";
				}
				else
				{
					this.CssClass = this.CssClass.Replace("ivld", String.Empty);
				}
			}
		}

		/// <summary>
		/// Gets or sets whether this control is valid.
		/// </summary>
		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
			}
		}

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		/// <summary>
		/// Gets or sets whether this control is validated.
		/// </summary>
		public bool UseValidation
		{
			get
			{
				return this.useValidation;
			}
			set
			{
				this.useValidation = value;
			}
		}

		#endregion

		#region ILocalizableControl Members

		/// <summary>
		/// Gets or sets all information for the localizable properties.
		/// </summary>
		public Dictionary<string, Translatable> TranslatableProperties
		{
			get
			{
				if (this.localizableProperties == null)
				{
					this.localizableProperties = new Dictionary<string, Translatable>();

					// TranslationKey prefix
					string translationKey = String.Empty;
					if (this.ID != null && this.ID.Length > 0)
						translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID;

					// Text property
					if (this.LocalizeText)
					{
						Translatable textProperty = new Translatable();
						textProperty.TranslationValue = this.Text;

						if (this.TranslationTag.Length > 0)
							textProperty.TranslationKey = this.TranslationTag;
						else
						{
							if (translationKey.Length == 0)
								throw new Dionysos.TechnicalException("An .ID property or TranslationTag is required for controls to be localized!, failed for control with .Text: '{0}' - {1}", this.Text, this.ClientID);
							textProperty.TranslationKey = translationKey + ".Text";
						}

						this.localizableProperties.Add("Text", textProperty);
					}
				}

				return this.localizableProperties;
			}
			set
			{
				this.localizableProperties = value;

				// Set the translated values
				if (this.localizableProperties.ContainsKey("Text"))
					this.Text = this.localizableProperties["Text"].TranslationValue;
			}
		}

		#endregion
	}
}
