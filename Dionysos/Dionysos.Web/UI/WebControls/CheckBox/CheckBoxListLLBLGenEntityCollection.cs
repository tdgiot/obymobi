using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Globalization;
using Dionysos.Interfaces;
using System.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Check box list data bound via a LLBLGenEntityCollection.
	/// </summary>
	public class CheckBoxListLLBLGenEntityCollection : CheckBoxListEntityCollection
	{
		#region Fields

		private string fKfieldForPageDataSourceOnLinkCollection = string.Empty;
		private string fKFieldForBoundCollectionOnLinkCollection = string.Empty;
		private string linkCollectionPropertyOnParentDataSource = string.Empty;
		private string linkEntityName = string.Empty;

		#endregion

		#region Properties

		/// <summary>
		/// The name of the property on the Page's DataSource containing the Collection of items that Link the Page.DataSource and the CheckboxList.DataSource
		/// </summary>
		/// <value>
		/// The link collection property on parent data source.
		/// </value>
		public string LinkCollectionPropertyOnParentDataSource
		{
			get
			{
				if (this.linkCollectionPropertyOnParentDataSource.IsNullOrWhiteSpace())
					this.linkCollectionPropertyOnParentDataSource = this.LinkEntityName + "Collection";

				return this.linkCollectionPropertyOnParentDataSource;
			}
			set
			{
				this.linkCollectionPropertyOnParentDataSource = value;
			}
		}

		/// <summary>
		/// Name of the Entity thats links the Page.DataSoruce and the CheckboxList.DataSource
		/// </summary>
		/// <value>
		/// The name of the link entity.
		/// </value>
		public string LinkEntityName
		{
			get
			{
				if (this.linkEntityName.IsNullOrWhiteSpace())
					this.linkEntityName = LLBLGenUtil.GetEntityName(this.PageAsPageLLBLGenEntity.EntityName) + this.EntityName;

				return this.linkEntityName;
			}
			set
			{
				this.linkEntityName = value;
			}
		}

		/// <summary>
		/// Gets or sets the the name of the field on the link collection in which to put the vlaue of the primary key of the selected item in the check box list.
		/// </summary>
		/// <value>
		/// The foreign key field for the bound collection on the link collection.
		/// </value>
		public string FKFieldForBoundCollectionOnLinkCollection
		{
			get
			{
				if (this.fKFieldForBoundCollectionOnLinkCollection.IsNullOrWhiteSpace())
					return this.PKfieldOfBoundCollection;
				else
					return this.fKFieldForBoundCollectionOnLinkCollection;
			}
			set
			{
				this.fKFieldForBoundCollectionOnLinkCollection = value;
			}
		}

		/// <summary>
		/// Gets or sets the the name of the field on the link collection in which to put the vlaue of the primary key of the data source of the page on which the check box list is placed.
		/// </summary>
		/// <value>
		/// The foreign key field for the page data source on the link collection.
		/// </value>
		public string FKfieldForPageDataSourceOnLinkCollection
		{
			get
			{
				if (fKfieldForPageDataSourceOnLinkCollection.IsNullOrWhiteSpace())
					return this.PKfieldOfPageDataSource;
				else
					return this.fKfieldForPageDataSourceOnLinkCollection;
			}
			set
			{
				this.fKfieldForPageDataSourceOnLinkCollection = value;
			}
		}

		/// <summary>
		/// Gets the primary key field of the bound collection.
		/// </summary>
		private string PKfieldOfBoundCollection
		{
			get
			{
				return this.EntityName + "Id";
			}
		}

		/// <summary>
		/// Gets the primary key field of the page data source.
		/// </summary>
		private string PKfieldOfPageDataSource
		{
			get
			{
				return ((IEntity)this.ParentGuiEntity.DataSource).PrimaryKeyFields[0].Name;
			}
		}

		/// <summary>
		/// Gets the link collection.
		/// </summary>
		private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection LinkCollection
		{
			get
			{
				try
				{
					return Dionysos.Reflection.Member.InvokeProperty(base.ParentGuiEntity.DataSource, this.LinkCollectionPropertyOnParentDataSource) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
				}
				catch (Exception ex)
				{
					throw new Dionysos.TechnicalException(ex, "De LinkCollection {0} kan niet worden gevonden op ParentGuiDataSource (Type: {1}), check ook de inner exception", this.LinkCollectionPropertyOnParentDataSource, base.ParentGuiEntity.DataSource.GetType().ToString());
				}
			}
		}

		/// <summary>
		/// Gets the page as PageLLBLGenEntity.
		/// </summary>
		private PageLLBLGenEntity PageAsPageLLBLGenEntity
		{
			get
			{
				return this.Page as PageLLBLGenEntity;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initialize the EntityCollection
		/// </summary>
		/// <returns>If it was succesful.</returns>
		public override bool InitializeEntityCollection()
		{
			SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entities = (SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection)Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName);
			entities.GetMulti(null);

			if (entities.Count > 0)
			{
				IEntityField field = entities[0].Fields[this.DataSortField];
				if (field != null)
				{
					entities.Sort(field.FieldIndex, ListSortDirection.Ascending);
				}
				else
				{
					entities.Sort(this.DataSortField, ListSortDirection.Ascending, null);
				}
			}

			this.EntityCollection = entities;

			return !Instance.Empty(this.EntityCollection);
		}

		/// <summary>
		/// Logic to select the correct checkboxes
		/// </summary>
		public override void Bind()
		{
			if (!this.Page.IsPostBack)
			{
				for (int i = 0; i < this.LinkCollection.Count; i++)
				{
					System.Web.UI.WebControls.ListItem li = this.Items.FindByValue(this.LinkCollection[i].Fields[this.FKFieldForBoundCollectionOnLinkCollection].CurrentValue.ToString());
					if (li != null)
					{
						li.Selected = true;
					}
				}
			}
		}

		public override bool Save()
		{
			// Only do this when databinding is true
			if (base.UseDataBinding)
			{
				object parentPkValue = ((IEntity)this.ParentGuiEntity.DataSource).PrimaryKeyFields[0].CurrentValue;

				// Loop through all items of the checkboxlist, their values represent the primary key of the Collection that is bound to this CbList
				for (int i = 0; i < this.Items.Count; i++)
				{
					System.Web.UI.WebControls.ListItem li = this.Items[i];

                    // Get the primary id                    
				    int selectedItemId;
				    if (Int32.TryParse(li.Value, out selectedItemId))
				    {
                        // Try to find the Entity in the retrieved collection, if not found return null
                        SD.LLBLGen.Pro.ORMSupportClasses.IEntity item = this.FindEntity(selectedItemId);
                        if (li.Selected)
                        {
                            // Add if not found 
                            if (item == null)
                            {
                                // Dynamicly create an entity based on the LinkEntityName
                                item = DataFactory.EntityFactory.GetEntity(this.LinkEntityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
                                // Populate it's fields 
                                item.SetNewFieldValue(this.FKFieldForBoundCollectionOnLinkCollection, selectedItemId);

                                item.SetNewFieldValue(this.FKfieldForPageDataSourceOnLinkCollection, parentPkValue);
                                // Save
                                item.Save();
                            }
                        }
                        else
                        {
                            // If found but not selected removed the Entity.
                            if (item != null)
                                item.Delete();
                        }
				    }
				}
			}
			return true;
		}

		/// <summary>
		/// Find the Entity on the Collection based on the PrimaryKey of the BoundEntity
		/// </summary>
		/// <param name="selectedItemId">The selected item id.</param>
		/// <returns></returns>
		private SD.LLBLGen.Pro.ORMSupportClasses.IEntity FindEntity(int selectedItemId)
		{
			SD.LLBLGen.Pro.ORMSupportClasses.IEntity retval = null;
			for (int i = 0; i < this.LinkCollection.Count; i++)
			{
				if ((int)this.LinkCollection[i].Fields[this.FKFieldForBoundCollectionOnLinkCollection].CurrentValue == selectedItemId)
				{
					retval = this.LinkCollection[i];
					break;
				}
			}

			return retval;
		}

		#endregion
	}
}
