using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;
using System.Web.UI;
using System.ComponentModel;
using Dionysos.Web.Resources;

namespace Dionysos.Web.UI.WebControls
{
	public class CheckBoxList : System.Web.UI.WebControls.CheckBoxList, IBindable, IExtendedValidator
	{
		#region Fields

		private IGuiEntitySimple parentGuiEntity = null;
		private string friendlyName = String.Empty;
		private bool isValid = true;
		private string errorMessage = String.Empty;
		private bool useValidation = true;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the parent gui of the subpanel.
		/// </summary>
		public IGuiEntitySimple ParentGuiEntity
		{
			get
			{
				if (this.parentGuiEntity == null)
					this.parentGuiEntity = this.GetIGuiEntitySimpleParent(this);

				if (this.parentGuiEntity == null)
				{
					// Still NULL, invalid
					throw new Dionysos.TechnicalException("Control '{0}' behoort op een IGuiEntitySimple te worden geplaatst", this.GetType().ToString());
				}

				return this.parentGuiEntity;
			}
			set
			{
				this.parentGuiEntity = value;
			}
		}

		/// <summary>
		/// Gets or sets the friendly name.
		/// </summary>
		public string FriendlyName
		{
			get
			{
				if (String.IsNullOrEmpty(this.friendlyName))
				{
					// Try to find a friendly name if not set
					string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					ITextControl textControl = this.Parent.FindControl("lbl" + strippedId) as ITextControl;
					if (textControl != null)
					{
						this.friendlyName = textControl.Text;
					}
					else
					{
						this.friendlyName = strippedId;
					}
				}

				return this.friendlyName;
			}
			set
			{
				this.friendlyName = value;
			}
		}

		/// <summary>
		/// Gets or sets whether a value is required.
		/// </summary>
		public bool IsRequired { get; set; }

		/// <summary>
		/// Gets the number of selected items.
		/// </summary>
		/// <returns>The number of selected items.</returns>
		public int SelectedItemsCount
		{
			get
			{
				int count = 0;
				for (int i = 0; i < this.Items.Count; i++)
				{
					if (this.Items[i].Selected)
					{
						count++;
					}
				}
				return count;
			}
		}

		/// <summary>
		/// Gets the selected items.
		/// </summary>
		public ListItemCollection SelectedItems
		{
			get
			{
				ListItemCollection selectedItems = new ListItemCollection();
				foreach (ListItem item in this.Items)
				{
					if (item.Selected) selectedItems.Add(item);
				}

				return selectedItems;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CheckBoxList class.
		/// </summary>
		public CheckBoxList()
		{
            if (this.CssClass.IsNullOrWhiteSpace())
                this.CssClass = "default-checkboxlist";
			this.Load += new EventHandler(CheckBoxList_Load);            
		}

		#endregion

		#region Event Handlers

		protected void CheckBoxList_Load(object sender, EventArgs e)
		{
			if (this.UseValidation)
			{
				if (this.IsRequired)
				{
					this.Page.Validators.Add(this);
					this.CssClass += " required";
				}
			}
		}

		#endregion

		#region Methods

		protected IGuiEntitySimple GetIGuiEntitySimpleParent(Control search)
		{
			IGuiEntitySimple control = search as IGuiEntitySimple;
			if (control != null)
				return control;
			else if (search.Parent != null)
				return this.GetIGuiEntitySimpleParent(search.Parent);
			else
				return null;
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			bool renderReadOnly = false;
			if (this.Page.GetType().GetProperty("RenderReadOnly") != null)
			{
				object val = this.Page.GetType().GetProperty("RenderReadOnly").GetValue(this.Page, null);
				if (Convert.ToBoolean(val))
				{
					renderReadOnly = true;
				}
			}

			if (renderReadOnly)
			{
				string itemsText = "";
				foreach (ListItem li in this.Items)
				{
					if (li.Selected)
						itemsText += li.Text + "; ";
				}
				// Strip last "; "
				if (itemsText.Length > 0)
				{
					itemsText = itemsText.Substring(0, itemsText.Length - 2);
				}
				writer.Write(itemsText);
			}
			else
			{
				//Perform the normal rendering
				base.Render(writer);
			}
		}

		/// <summary>
		/// Select items in the CheckBoxList by their values.
		/// </summary>
		/// <param name="list">List containing the values to select.</param>
		public void SelectMultipleItems(List<int> list)
		{
			for (int i = 0; i < list.Count; i++)
			{
				ListItem li = this.Items.FindByValue(list[i].ToString());
				if (li != null)
				{
					li.Selected = true;
				}
			}
		}

		/// <summary>
		/// Select items in the CheckBoxList by their values.
		/// </summary>
		/// <param name="valuesToSelect">String containing the values.</param>
		/// <param name="seperator">Character seperating the values in the string.</param>
		public void SelectMultipleItems(string valuesToSelect, char seperator)
		{
			string[] values = valuesToSelect.Split(seperator);

			// First set all to false
			foreach (ListItem item in this.Items)
			{
				item.Selected = false;
			}

			for (int i = 0; i < values.Length; i++)
			{
				ListItem li = this.Items.FindByValue(values[i]);
				if (li != null)
				{
					li.Selected = true;
				}
			}
		}

		/// <summary>
		/// Selects all items.
		/// </summary>
		public void SelectAll()
		{
			foreach (ListItem item in this.Items)
			{
				item.Selected = true;
			}
		}

		/// <summary>
		/// Get all selected values in one string, seperated by the supplied seperator character.
		/// </summary>
		/// <param name="seperator">The seperator character.</param>
		/// <returns>
		/// The string of selected values.
		/// </returns>
		public string SelectedValues(char seperator)
		{
			return this.SelectedValues(seperator.ToString());
		}

		/// <summary>
		/// Get all selected values in one string, seperated by the supplied seperator.
		/// </summary>
		/// <param name="seperator">The seperator.</param>
		/// <returns>
		/// The string of selected values.
		/// </returns>
		public string SelectedValues(string seperator)
		{
			// Save the ContentTypes
			string values = "";
			for (int i = 0; i < this.Items.Count; i++)
			{
				if (this.Items[i].Selected)
				{
					if (!String.IsNullOrEmpty(values))
					{
						values += seperator;
					}

					values += this.Items[i].Value;
				}
			}

			return values;
		}

		/// <summary>
		/// Gets the selected values as a list.
		/// </summary>
		/// <returns>The list of selected values.</returns>
		public List<int> SelectedValuesAsIntList()
		{
			List<int> toReturn = new List<int>();

			for (int i = 0; i < this.Items.Count; i++)
			{
				if (this.Items[i].Selected)
				{
					toReturn.Add(Convert.ToInt32(this.Items[i].Value));
				}
			}

			return toReturn;
		}        

		#endregion

		#region IBindable Members

		/// <summary>
		/// Gets or sets the flag which indicated whether databinding should be used
		/// </summary>
		public virtual bool UseDataBinding { get; set; }

		#endregion

		#region IExtendedValidator Members

		/// <summary>
		/// Validates this control.
		/// </summary>
		public void Validate()
		{
			if (this.UseValidation)
			{
				// Validate required
				if (this.IsRequired && this.SelectedItemsCount == 0)
				{
					this.ErrorMessage = String.Format(Dionysos_Web_UI_WebControls.CheckBoxList_ErrorMessage_Required, this.FriendlyName);
					this.IsValid = false;
				}

				// Add or remove css class
				if (!this.IsValid)
				{
					this.CssClass += " ivld";
				}
				else
				{
					this.CssClass = this.CssClass.Replace("ivld", String.Empty);
				}
			}
		}

		/// <summary>
		/// Gets or sets whether this control is valid.
		/// </summary>
		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
			}
		}

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		/// <summary>
		/// Gets or sets whether this control is validated.
		/// </summary>
		public bool UseValidation
		{
			get
			{
				return this.useValidation;
			}
			set
			{
				this.useValidation = value;                
			}
		}

		#endregion
	}
}
