using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Checkbox List using a EntityColleciton
	/// </summary>
	public abstract class CheckBoxListEntityCollection : CheckBoxList, ISaveableControl, ISelfBindable
	{
		#region Fields

		private bool useDataBinding = true;
		private string dataSortField = null;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name of the entity.
		/// </summary>
		/// <value>The name of the entity.</value>
		public string EntityName { get; set; }

		/// <summary>
		/// Gets or sets the entity collection.
		/// </summary>
		/// <value>The entity collection.</value>
		public object EntityCollection { get; set; }

		/// <summary>
		/// Gets or sets the data sort field.
		/// </summary>
		/// <value>The data sort field.</value>
		public string DataSortField
		{
			get
			{
				if (!String.IsNullOrEmpty(this.dataSortField))
				{
					return this.dataSortField;
				}
				else
				{
					return this.DataTextField;
				}
			}
			set
			{
				this.dataSortField = value;
			}
		}

		/// <summary>
		/// Gets or sets the field of the data source that provides the text content of the list items.
		/// </summary>
		/// <value>A <see cref="T:System.String"/> that specifies the field of the data source that provides the text content of the list items. The default is <see cref="F:System.String.Empty"/>.</value>
		public override string DataTextField
		{
			get
			{
				if (String.IsNullOrEmpty(base.DataTextField))
				{
					return "Name";
				}
				else
				{
					return base.DataTextField;
				}
			}
			set
			{
				base.DataTextField = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to use databinding.
		/// </summary>
		/// <value><c>true</c> if databinding is used; otherwise, <c>false</c>.</value>
		public override bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to prevent entity collection initialization.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if entity collection initialization is prevented; otherwise, <c>false</c>.
		/// </value>
		public bool PreventEntityCollectionInitialization { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CheckBoxListEntityCollection"/> class.
		/// </summary>
		public CheckBoxListEntityCollection()
		{
			this.Init += new EventHandler(CheckBoxListEntityCollection_Init);
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the Init event of the CheckBoxListEntityCollection control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CheckBoxListEntityCollection_Init(object sender, EventArgs e)
		{
			this.BindDataSource();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Binds the collection based on the EntityName to the DropDownList
		/// On default the DataValueField = EntityName + "Id" and DataTextField "Name" (only loaded with empty DataValueField and DataTextField properties
		/// </summary>
		protected void BindDataSource()
		{
			if (!this.PreventEntityCollectionInitialization)
			{
				this.InitializeEntityCollection();
			}

			if (this.EntityCollection != null)
			{
				this.DataSource = this.EntityCollection;

				if (String.IsNullOrEmpty(this.DataValueField))
				{
					this.DataValueField = this.EntityName + "Id";
				}

				this.DataBind();
			}
			else if (this.PreventEntityCollectionInitialization)
			{
				// Nothing todo, manual entity collection
			}
			else
			{
				this.Items.Clear();
				this.Items.Add("*Databinding failed*");
			}
		}

		/// <summary>
		/// Initializes the entity collection.
		/// </summary>
		/// <returns><c>true</c> if the initialization was succesfull; otherwise, <c>false</c>.</returns>
		public abstract bool InitializeEntityCollection();

		/// <summary>
		/// Binds the 
		/// Selects the correct checkboxes in the checkboxlist based on the datasource
		/// </summary>
		public abstract void Bind();

		/// <summary>
		/// Save the Changes
		/// </summary>
		/// <returns>True if save was succesful, False if not</returns>
		public abstract bool Save();

		#endregion
	}
}
