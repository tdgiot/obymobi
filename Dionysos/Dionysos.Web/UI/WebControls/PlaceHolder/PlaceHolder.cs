﻿using System;
using System.Web.UI;

namespace Dionysos.Web.UI.WebControls
{
    public class PlaceHolder : System.Web.UI.WebControls.PlaceHolder
    {
        #region Methods

        /// <summary>
        /// Inserts the HTML.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        public void InsertHtml(string format, params object[] args)
        {
            this.InsertHtml(null, format, args);
        }

        /// <summary>
        /// Inserts the HTML.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        public void InsertHtml(IFormatProvider provider, string format, params object[] args)
        {
            this.InsertHtml(String.Format(provider, format, args));
        }

        /// <summary>
        /// Inserts the HTML.
        /// </summary>
        /// <param name="text">The text.</param>
        public void InsertHtml(string text)
        {
            this.Controls.AddAt(0, new LiteralControl(text + Environment.NewLine));
        }

        /// <summary>
        /// Adds the HTML.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        public void AddHtml(string format, params object[] args)
        {
            this.AddHtml(null, format, args);
        }

        /// <summary>
        /// Adds the HTML.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        public void AddHtml(IFormatProvider provider, string format, params object[] args)
        {
            this.AddHtml(StringUtil.FormatSafeWithProvider(format, provider, args));
        }

        /// <summary>
        /// Adds the HTML.
        /// </summary>
        /// <param name="text">The text.</param>
        public void AddHtml(string text)
        {
            this.Controls.Add(new LiteralControl(text + Environment.NewLine));
        }

        /// <summary>
        /// Adds the javascript.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="addScriptTags">If set to <c>true</c> adds script tags.</param>
        public void AddJavascript(string text, bool addScriptTags)
        {
            if (addScriptTags)
            {
                text = "<script type=\"text/javascript\">" + text + "</script>";
            }

            this.AddHtml(text);
        }

        public override System.Web.UI.ControlCollection Controls
        {
            get
            {                
                return base.Controls;
            }
        }
        
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
        }

        #endregion
    }
}
