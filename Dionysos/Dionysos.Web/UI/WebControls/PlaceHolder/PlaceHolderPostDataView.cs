﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// This placeholder should be used on EntityCollectionPages. This placeholder is used for adding controls after the dataview (for example the maingridview)
    /// </summary>
    public class PlaceHolderPreDataView : System.Web.UI.WebControls.PlaceHolder
    {
    }
}
