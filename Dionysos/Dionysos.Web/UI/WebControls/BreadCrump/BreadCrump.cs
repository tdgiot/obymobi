﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    public class BreadCrump : ListControl
    {
        #region Fields

        private string separatorText = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls type
        /// </summary>
        public BreadCrump()
        {
        }

        #endregion

        #region Methods

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            //base.Render(writer);

            if (this.Items.Count == 0)
            {
                // No items in this breadcrump instance
            }
            else
            {
                Table breadcrump = new Table();
                breadcrump.CellPadding = 0;
                breadcrump.CellSpacing = 0;
                breadcrump.CssClass = this.CssClass;

                TableRow breadcrumpRow = new TableRow();

                for (int i = 0; i < this.Items.Count; i++)
                {
                    if (i < (this.Items.Count - 1))
                    {
                        TableCell breadcrumpCell = new TableCell();

                        HyperLink hlBreadCrumpItem = new HyperLink();
                        hlBreadCrumpItem.CssClass = "breadcrumpItem";
                        hlBreadCrumpItem.Text = this.Items[i].Text;
                        hlBreadCrumpItem.NavigateUrl = this.Items[i].Value;
                        breadcrumpCell.Controls.Add(hlBreadCrumpItem);

                        breadcrumpRow.Cells.Add(breadcrumpCell);

                        TableCell separatorCell = new TableCell();
                        separatorCell.Text = this.separatorText.Length > 0 ? this.separatorText : "&nbsp;>&nbsp;";

                        breadcrumpRow.Cells.Add(separatorCell);
                    }
                    else
                    {
                        TableCell breadcrumpCell = new TableCell();

                        string literal = string.Format("<span>{0}</span>", this.Items[i].Text);
                        breadcrumpCell.Controls.Add(new LiteralControl(literal));

                        breadcrumpRow.Cells.Add(breadcrumpCell);
                    }
                }

                breadcrump.Rows.Add(breadcrumpRow);
                breadcrump.RenderControl(writer);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text which is displayed between breadcrump items
        /// </summary>
        public string SeparatorText
        {
            get
            {
                return this.separatorText;
            }
            set
            {
                this.separatorText = value;
            }
        }

        #endregion
    }
}
