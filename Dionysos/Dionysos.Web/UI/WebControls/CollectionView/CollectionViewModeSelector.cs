﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Reflection;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a web control for specifying the collection view mode
    /// </summary>
    public class CollectionViewModeSelector : WebControl
    {
        #region Fields

        private string tilesViewText = string.Empty;
        private string detailsViewText = string.Empty;
        private string listViewText = string.Empty;
        private string separatorText = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.CollectionViewModeSelector type
        /// </summary>
        public CollectionViewModeSelector()
        {
        }

        #endregion

        #region Methods

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);

            // Initialize an empty string to construct the literal
            // and initialize a CollectionViewMode instance
            CollectionViewMode viewMode = CollectionViewMode.Tiles;

            // Check whether the page has a property ViewMode
            if (!Instance.Empty(this.Page) && Member.HasMember(this.Page, "ViewMode"))
            {
                viewMode = (CollectionViewMode)Member.InvokeProperty(this.Page, "ViewMode");
            }

            Table collectionViewModeSelector = new Table();
            collectionViewModeSelector.CellPadding = 0;
            collectionViewModeSelector.CellSpacing = 0;
            collectionViewModeSelector.CssClass = this.CssClass;

            TableRow collectionViewModeSelectorRow = new TableRow();

            // Add the tiles view
            TableCell tilesTableCell = new TableCell();
            if (viewMode == CollectionViewMode.Tiles)
            {
                string literal = string.Format("<span>{0}</span>", this.tilesViewText.Length > 0 ? this.tilesViewText : "Tegels");
                tilesTableCell.Controls.Add(new LiteralControl(literal));
            }
            else
            {
                HyperLink hlViewModeItem = new HyperLink();
                hlViewModeItem.CssClass = "collectionViewModeItem";
                hlViewModeItem.Text = this.tilesViewText.Length > 0 ? this.tilesViewText : "Tegels";

                QueryStringHelper qs = new QueryStringHelper();
                qs.AddItem("ViewMode", (int)CollectionViewMode.Tiles);

                hlViewModeItem.NavigateUrl = qs.MergeQuerystringWithRawUrl(); // This make sure other selections in the query string are also preserved
                tilesTableCell.Controls.Add(hlViewModeItem);
            }
            collectionViewModeSelectorRow.Cells.Add(tilesTableCell);

            // Check whether a separator should be added
            if (this.separatorText.Length > 0)
            {
                TableCell separatorTableCell = new TableCell();
                separatorTableCell.Text = this.separatorText;
                collectionViewModeSelectorRow.Cells.Add(separatorTableCell);
            }

            // Add the details view
            TableCell detailsTableCell = new TableCell();
            if (viewMode == CollectionViewMode.Details)
            {
                string literal = string.Format("<span>{0}</span>", this.detailsViewText.Length > 0 ? this.detailsViewText : "Details");
                detailsTableCell.Controls.Add(new LiteralControl(literal));
            }
            else
            {
                HyperLink hlViewModeItem = new HyperLink();
                hlViewModeItem.CssClass = "collectionViewModeItem";
                hlViewModeItem.Text = this.detailsViewText.Length > 0 ? this.detailsViewText : "Details";

                QueryStringHelper qs = new QueryStringHelper();
                qs.AddItem("ViewMode", (int)CollectionViewMode.Details);

                hlViewModeItem.NavigateUrl = qs.MergeQuerystringWithRawUrl(); // This make sure other selections in the query string are also preserved
                detailsTableCell.Controls.Add(hlViewModeItem);
            }
            collectionViewModeSelectorRow.Cells.Add(detailsTableCell);

            // Check whether a separator should be added
            if (this.separatorText.Length > 0)
            {
                TableCell separatorTableCell = new TableCell();
                separatorTableCell.Text = this.separatorText;
                collectionViewModeSelectorRow.Cells.Add(separatorTableCell);
            }

            // Add the list view
            TableCell listTableCell = new TableCell();
            if (viewMode == CollectionViewMode.List)
            {
                string literal = string.Format("<span>{0}</span>", this.listViewText.Length > 0 ? this.listViewText : "Lijst");
                listTableCell.Controls.Add(new LiteralControl(literal));
            }
            else
            {
                HyperLink hlViewModeItem = new HyperLink();
                hlViewModeItem.CssClass = "collectionViewModeItem";
                hlViewModeItem.Text = this.listViewText.Length > 0 ? this.listViewText : "Lijst";

                QueryStringHelper qs = new QueryStringHelper();
                qs.AddItem("ViewMode", (int)CollectionViewMode.List);

                hlViewModeItem.NavigateUrl = qs.MergeQuerystringWithRawUrl(); // This make sure other selections in the query string are also preserved
                listTableCell.Controls.Add(hlViewModeItem);
            }
            collectionViewModeSelectorRow.Cells.Add(listTableCell);

            collectionViewModeSelector.Rows.Add(collectionViewModeSelectorRow);
            collectionViewModeSelector.RenderControl(writer);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text which is being displayed for the tiles view
        /// </summary>
        public string TilesViewText
        {
            get
            {
                return this.tilesViewText;
            }
            set
            {
                this.tilesViewText = value;
            }
        }

        /// <summary>
        /// Gets or sets the text which is being displayed for the details view
        /// </summary>
        public string DetailsViewText
        {
            get
            {
                return this.detailsViewText;
            }
            set
            {
                this.detailsViewText = value;
            }
        }

        /// <summary>
        /// Gets or sets the text which is being displayed for the list view
        /// </summary>
        public string ListViewText
        {
            get
            {
                return this.listViewText;
            }
            set
            {
                this.listViewText = value;
            }
        }

        /// <summary>
        /// Gets or sets the text which is being displayed between the items
        /// </summary>
        public string SeparatorText
        {
            get
            {
                return this.separatorText;
            }
            set
            {
                this.separatorText = value;
            }
        }

        #endregion
    }
}
