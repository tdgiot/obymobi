﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Collections;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.Drawing.Design;
using System.ComponentModel.Design;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class using for displaying collections of data
    /// </summary>
    [ParseChildren(true)]
    [Designer(typeof(CollectionViewControlDesigner))]
    [ToolboxData("<{0}:CollectionView runat=\"server\" width=\"100%\" height=\"100%\" ></{0}:CollectionView>")]
    public class CollectionView : CompositeControl
    {
        #region Fields

        internal List<IAttributeAccessor> regions;
        private ITemplate headerTemplate;
        private ITemplate listViewTemplate;
        private ITemplate tilesViewTemplate;
        private ITemplate detailsViewTemplate;

        private Style styleHeader;
        private Style styleListView;
        private Style styleTilesView;
        private Style styleDetailsView;

        private Panel panelMain;

        private Panel panelHeader;

        private Panel panelListView;
        private Panel panelTilesView;
        private Panel panelDetailsView;

        private CollectionViewMode viewMode = CollectionViewMode.List;

        private object datasource = null;

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            regions = new List<IAttributeAccessor>();

            panelMain = new Panel();

            // Instantiate the header panel
            panelHeader = new Panel();
            if (headerTemplate != null)
            {
                headerTemplate.InstantiateIn(panelHeader);
            }
            panelMain.Controls.Add(panelHeader);
            regions.Add(panelHeader);

            panelListView = new Panel();
            if (listViewTemplate != null)
            {
                listViewTemplate.InstantiateIn(panelListView);
            }
            panelMain.Controls.Add(panelListView);
            regions.Add(panelListView);

            panelTilesView = new Panel();
            if (tilesViewTemplate != null)
            {
                tilesViewTemplate.InstantiateIn(panelTilesView);
            }
            panelMain.Controls.Add(panelTilesView);
            regions.Add(panelTilesView);

            panelDetailsView = new Panel();
            if (detailsViewTemplate != null)
            {
                detailsViewTemplate.InstantiateIn(panelDetailsView);
            }
            panelMain.Controls.Add(panelDetailsView);
            regions.Add(panelDetailsView);
   
            Controls.Add(panelMain);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            try
            {
                if (panelMain == null)
                    CreateChildControls();

                // Initialize header

                // Header panel
                panelHeader.Width = Width;
                panelHeader.Height = HeaderHeight;
                panelHeader.Wrap = true;

                if (styleHeader != null)
                    panelHeader.ApplyStyle(StyleHeader);

                        // Initialize gridview

                        // List view panel
                        panelListView.Width = Width;
                        panelListView.Height = Height;
                        panelListView.Wrap = true;

                        //panelListView.Controls.Add(gridViewListView);
                        if (styleListView != null)
                            panelListView.ApplyStyle(StyleListView);

                        panelListView.ScrollBars = ScrollBarsListView;
                        panelListView.HorizontalAlign = HorizontalAlignListView;

                        // Initialize repeater

                        // Tiles view panel
                        panelTilesView.Width = Width;
                        panelTilesView.Height = Height;
                        panelTilesView.Wrap = true;

                        //panelTilesView.Controls.Add(repeaterTilesView);
                        if (styleTilesView != null)
                            panelTilesView.ApplyStyle(StyleTilesView);

                        panelTilesView.ScrollBars = ScrollBarsTilesView;
                        panelTilesView.HorizontalAlign = HorizontalAlignTilesView;

                        // Initialize repeater

                        // Details view panel
                        panelDetailsView.Width = Width;
                        panelDetailsView.Height = Height;
                        panelDetailsView.Wrap = true;

                        //panelDetailsView.Controls.Add(repeaterDetailsView);
                        if (styleDetailsView != null)
                            panelDetailsView.ApplyStyle(StyleDetailsView);

                        panelDetailsView.ScrollBars = ScrollBarsDetailsView;
                        panelDetailsView.HorizontalAlign = HorizontalAlignDetailsView;


                panelMain.Width = Width;
                panelMain.Height = Height;

                panelMain.RenderControl(writer);
            }
            catch (Exception e)
            {
                writer.Write("Render Error: <br >" + e.Message);
            }
        }

        protected override void LoadViewState(object savedState)
        {
            if (savedState == null)
            {
                base.LoadViewState(null);
                return;
            }
            else
            {
                object[] myState = (object[])savedState;
                if (myState.Length !=3)
                {
                    throw new ArgumentException("Invalid view state");
                }
                base.LoadViewState(myState[0]);
                ((IStateManager)StyleHeader).LoadViewState(myState[1]);
                ((IStateManager)StyleListView).LoadViewState(myState[2]);
                ((IStateManager)StyleTilesView).LoadViewState(myState[3]);
                ((IStateManager)StyleDetailsView).LoadViewState(myState[4]);
            }
        }

        protected override object SaveViewState()
        {
            // Customized state management to save the state of styles.
            object[] myState = new object[5];

            myState[0] = base.SaveViewState();
            if (styleHeader != null)
                myState[1] = ((IStateManager)styleHeader).SaveViewState();
            if (styleListView != null)
                myState[2] = ((IStateManager)styleListView).SaveViewState();
            if (styleTilesView != null)
                myState[2] = ((IStateManager)styleTilesView).SaveViewState();
            if (styleDetailsView != null)
                myState[2] = ((IStateManager)styleDetailsView).SaveViewState();

            return myState;
        }

        protected override void TrackViewState()
        {
            // Customized state management to track the state 
            // of styles.
            base.TrackViewState();

            if (styleHeader != null)
                ((IStateManager)styleHeader).TrackViewState();
            if (styleListView != null)
                ((IStateManager)styleListView).TrackViewState();
            if (styleTilesView != null)
                ((IStateManager)styleTilesView).TrackViewState();
            if (styleDetailsView != null)
                ((IStateManager)styleDetailsView).TrackViewState();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the view mode
        /// </summary>
        [Browsable(true)]
        public CollectionViewMode ViewMode
        {
            get
            {
                return this.viewMode;
            }
            set
            {
                this.viewMode = value;
            }
        }

        /// <summary>
        /// Gets or sets the height of the header
        /// </summary>        
        [Category("Appearance"), Bindable(true), DefaultValue("25px"), Description("Height of the heading")]
        public Unit HeaderHeight
        {
            get
            {
                object o = ViewState["HeaderHeight"];
                return (o == null) ? Unit.Parse("20px") : (Unit)o;
            }
            set 
            { 
                ViewState["HeaderHeight"] = value; 
            }
        }

        /// <summary>
        /// Gets or sets the scrollbar mode for the listview region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(ScrollBars.None), Description("The scrollbar mode for the listview region of the pane")]
        public ScrollBars ScrollBarsListView
        {
            get
            {
                object o = ViewState["ScrollBarsListView"];
                return (o == null) ? ScrollBars.None : (ScrollBars)ViewState["ScrollBarsListView"];
            }
            set 
            { 
                ViewState["ScrollBarsListView"] = value; 
            }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment for the listview region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(HorizontalAlign.NotSet), Description("The horizontal alignment for the listview region of the pane")]
        public HorizontalAlign HorizontalAlignListView
        {
            get
            {
                object o = ViewState["HorizontalAlignListView"];
                return (o == null) ? HorizontalAlign.NotSet : (HorizontalAlign)ViewState["HorizontalAlignListView"];
            }
            set 
            { 
                ViewState["HorizontalAlignListView"] = value; 
            }
        }

        /// <summary>
        /// Gets or sets the scrollbar mode for the tiles view region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(ScrollBars.None), Description("The scrollbar mode for the tiles view region of the pane")]
        public ScrollBars ScrollBarsTilesView
        {
            get
            {
                object o = ViewState["ScrollBarsTilesView"];
                return (o == null) ? ScrollBars.None : (ScrollBars)ViewState["ScrollBarsTilesView"];
            }
            set
            {
                ViewState["ScrollBarsTilesView"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment for the tiles view region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(HorizontalAlign.NotSet), Description("The horizontal alignment for the tiles view region of the pane")]
        public HorizontalAlign HorizontalAlignTilesView
        {
            get
            {
                object o = ViewState["HorizontalAlignTilesView"];
                return (o == null) ? HorizontalAlign.NotSet : (HorizontalAlign)ViewState["HorizontalAlignTilesView"];
            }
            set
            {
                ViewState["HorizontalAlignTilesView"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the scrollbar mode for the details view region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(ScrollBars.None), Description("The scrollbar mode for the details view region of the pane")]
        public ScrollBars ScrollBarsDetailsView
        {
            get
            {
                object o = ViewState["ScrollBarsDetailsView"];
                return (o == null) ? ScrollBars.None : (ScrollBars)ViewState["ScrollBarsDetailsView"];
            }
            set
            {
                ViewState["ScrollBarsDetailsView"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment for the details view region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(HorizontalAlign.NotSet), Description("The horizontal alignment for the details view region of the pane")]
        public HorizontalAlign HorizontalAlignDetailsView
        {
            get
            {
                object o = ViewState["HorizontalAlignDetailsView"];
                return (o == null) ? HorizontalAlign.NotSet : (HorizontalAlign)ViewState["HorizontalAlignDetailsView"];
            }
            set
            {
                ViewState["HorizontalAlignDetailsView"] = value;
            }
        }

        [Browsable(false)]
        [MergableProperty(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(Pane))]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate HeaderTemplate
        {
            get
            {
                return headerTemplate;
            }
            set
            {
                headerTemplate = value;
            }
        }

        [Browsable(false)]
        [MergableProperty(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(Pane))]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate ListViewTemplate
        {
            get
            {
                return listViewTemplate;
            }
            set
            {
                listViewTemplate = value;
            }
        }

        [Browsable(false)]
        [MergableProperty(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(Pane))]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate TilesViewTemplate
        {
            get
            {
                return tilesViewTemplate;
            }
            set
            {
                tilesViewTemplate = value;
            }
        }

        [Browsable(false)]
        [MergableProperty(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(Pane))]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate DetailsViewTemplate
        {
            get
            {
                return detailsViewTemplate;
            }
            set
            {
                detailsViewTemplate = value;
            }
        }

        [Category("Appearance"), DefaultValue(null), PersistenceMode(PersistenceMode.InnerProperty), Description("Style of the header area"),]
        public virtual Style StyleHeader
        {
            get
            {
                if (styleHeader == null)
                {
                    styleHeader = new Style();
                    if (IsTrackingViewState)
                        ((IStateManager)styleHeader).TrackViewState();
                }
                return styleHeader;
            }
        }

        [Category("Appearance"), DefaultValue(null), PersistenceMode(PersistenceMode.InnerProperty), Description("Style of the list view area"),]
        public virtual Style StyleListView
        {
            get
            {
                if (styleListView == null)
                {
                    styleListView = new Style();
                    if (IsTrackingViewState)
                        ((IStateManager)styleListView).TrackViewState();
                }
                return styleListView;
            }
        }

        [Category("Appearance"), DefaultValue(null), PersistenceMode(PersistenceMode.InnerProperty), Description("Style of the tiles view area"),]
        public virtual Style StyleTilesView
        {
            get
            {
                if (styleTilesView == null)
                {
                    styleTilesView = new Style();
                    if (IsTrackingViewState)
                        ((IStateManager)styleTilesView).TrackViewState();
                }
                return styleTilesView;
            }
        }

        [Category("Appearance"), DefaultValue(null), PersistenceMode(PersistenceMode.InnerProperty), Description("Style of the details view area"),]
        public virtual Style StyleDetailsView
        {
            get
            {
                if (styleDetailsView == null)
                {
                    styleDetailsView = new Style();
                    if (IsTrackingViewState)
                        ((IStateManager)styleDetailsView).TrackViewState();
                }
                return styleDetailsView;
            }
        }

        /// <summary>
        /// Gets or sets the datasource
        /// </summary>
        [Browsable(false)]
        public object DataSource
        {
            get
            {
                return this.datasource;
            }
            set
            {
                this.datasource = value;
            }
        }

        #endregion
    }

    /// <summary>
    /// Designer class which is being used at design time for CollectionView instances 
    /// </summary>    
    public class CollectionViewControlDesigner : CompositeControlDesigner
    {
        #region Fields

        private CollectionView collectionView;
        private int currentRegion = -1;
        private int nbRegions = 0;

        #endregion

        #region Methods

        public override void Initialize(IComponent component)
        {
            this.collectionView = (CollectionView)component;
            base.Initialize(component);
            SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
            SetViewFlags(ViewFlags.TemplateEditing, true);
        }

        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                TemplateGroupCollection collection = new TemplateGroupCollection();

                TemplateGroup groupHeader = new TemplateGroup("HeaderTemplate");
                TemplateDefinition definitionHeader = new TemplateDefinition(this, "HeaderTemplate", collectionView, "HeaderTemplate", false);
                groupHeader.AddTemplateDefinition(definitionHeader);
                collection.Add(groupHeader);

                TemplateGroup groupListView = new TemplateGroup("ListViewTemplate");
                TemplateDefinition definitionListView = new TemplateDefinition(this, "ListViewTemplate", collectionView, "ListViewTemplate", false);
                groupListView.AddTemplateDefinition(definitionListView);
                collection.Add(groupListView);

                TemplateGroup groupTilesView = new TemplateGroup("ListViewTemplate");
                TemplateDefinition definitionTilesView = new TemplateDefinition(this, "TilesViewTemplate", collectionView, "TilesViewTemplate", false);
                groupTilesView.AddTemplateDefinition(definitionTilesView);
                collection.Add(groupTilesView);

                TemplateGroup groupDetailsView = new TemplateGroup("DetailsViewTemplate");
                TemplateDefinition definitionDetailsView = new TemplateDefinition(this, "DetailsViewTemplate", collectionView, "DetailsViewTemplate", false);
                groupDetailsView.AddTemplateDefinition(definitionDetailsView);
                collection.Add(groupDetailsView);
                
                return collection;
            }
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            if (this.collectionView.regions != null)
            {
                nbRegions = this.collectionView.regions.Count;
                for (int i = 0; i < nbRegions; i++)
                {
                    this.collectionView.regions[i].SetAttribute(DesignerRegion.DesignerRegionAttributeName, i.ToString());
                }
            }
        }

        public override string GetDesignTimeHtml(DesignerRegionCollection regions)
        {
            this.CreateChildControls();

            for (int i = 0; i < nbRegions; i++)
            {
                DesignerRegion r;
                if (currentRegion == i)
                    r = new EditableDesignerRegion(this, i.ToString());
                else
                    r = new DesignerRegion(this, i.ToString());
                regions.Add(r);
            }

            if ((currentRegion >= 0) && (currentRegion < nbRegions))
                regions[currentRegion].Highlight = true;
            return base.GetDesignTimeHtml(regions);
        }

        protected override void OnClick(DesignerRegionMouseEventArgs e)
        {
            base.OnClick(e);
            currentRegion = -1;
            if (e.Region != null)
            {
                for (int i = 0; i < nbRegions; i++)
                {
                    if (e.Region.Name == i.ToString())
                    {
                        currentRegion = i;
                        break;
                    }
                }
                UpdateDesignTimeHtml();
            }
        }

        public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
        {
            IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
            if (host != null)
            {
                ITemplate headerTemplate;
                if (currentRegion == 0)
                {
                    headerTemplate = this.collectionView.HeaderTemplate;
                    return ControlPersister.PersistTemplate(headerTemplate, host);
                }

                ITemplate listViewTemplate;
                if (currentRegion == 1)
                {
                    listViewTemplate = this.collectionView.ListViewTemplate;
                    return ControlPersister.PersistTemplate(listViewTemplate, host);
                }

                ITemplate tilesViewTemplate;
                if (currentRegion == 2)
                {
                    tilesViewTemplate = this.collectionView.TilesViewTemplate;
                    return ControlPersister.PersistTemplate(tilesViewTemplate, host);
                }

                ITemplate detailsViewTemplate;
                if (currentRegion == 3)
                {
                    detailsViewTemplate = this.collectionView.DetailsViewTemplate;
                    return ControlPersister.PersistTemplate(detailsViewTemplate, host);
                }
            }
            return String.Empty;
        }

        public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
        {
            if (content == null)
                return;
            IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
            if (host != null)
            {
                ITemplate template = ControlParser.ParseTemplate(host, content);
                if (template != null)
                {
                    if (currentRegion == 0)
                    {
                        this.collectionView.HeaderTemplate = template;
                    }
                    else if (currentRegion == 1)
                    {
                        this.collectionView.ListViewTemplate = template;
                    }
                    else if (currentRegion == 2)
                    {
                        this.collectionView.TilesViewTemplate = template;
                    }
                    else if (currentRegion == 3)
                    {
                        this.collectionView.DetailsViewTemplate = template;
                    }
                }
            }
        }

        #endregion
    }
}
