﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Text.RegularExpressions;

namespace Dionysos.Web.UI.WebControls
{
	public class ValidationMessage : System.Web.UI.WebControls.Label
	{
		#region Fields

		/// <summary>
		/// The field name.
		/// </summary>
		protected string fieldName;

		/// <summary>
		/// The text.
		/// </summary>
		protected string text;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the HTML tag that is used to render the <see cref="T:System.Web.UI.WebControls.Label"/> control.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// The <see cref="T:System.Web.UI.HtmlTextWriterTag"/> value used to render the <see cref="T:System.Web.UI.WebControls.Label"/>.
		/// </returns>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Span;
			}
		}

		/// <summary>
		/// Gets or sets the text content of the <see cref="T:System.Web.UI.WebControls.Label"/> control.
		/// </summary>
		/// <returns>
		/// The text content of the control. The default value is <see cref="F:System.String.Empty"/>.
		///   </returns>
		public override string Text
		{
			get
			{
				if (String.IsNullOrEmpty(this.text) &&
					!String.IsNullOrEmpty(this.AssociatedControlID))
				{
					IValidator associatedControl = this.FindControl(this.AssociatedControlID) as IValidator;
					if (associatedControl != null &&
						!associatedControl.IsValid)
					{
						this.text = Regex.Replace(associatedControl.ErrorMessage, @"<strong>(.*?)</strong>", "", RegexOptions.Compiled | RegexOptions.IgnoreCase).Trim().TurnFirstToUpper(false);
					}
				}

				return this.text;
			}
			set
			{
				this.text = value;
			}
		}

		/// <summary>
		/// Gets the Page as PageDefault.
		/// </summary>
		/// <value>The page as page default.</value>
		protected PageDefault PageAsPageDefault
		{
			get
			{
				return this.Page as PageDefault;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
		/// </summary>
		/// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnLoad(EventArgs e)
		{
			// Set field name
			this.fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);

			// Set CSS class
			this.CssClass = "vmessage";

			// Reset to visible
			this.Visible = true;

			base.OnLoad(e);
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.PreRender"/> event.
		/// </summary>
		/// <param name="e">A <see cref="T:System.EventArgs"/> that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			if (String.IsNullOrEmpty(this.AssociatedControlID) &&
				this.PageAsPageDefault != null &&
				!String.IsNullOrEmpty(this.fieldName))
			{
				// Get valid associated controls
				List<Control> associatedControls = this.PageAsPageDefault.ControlList.Where(c =>
					!String.IsNullOrEmpty(c.ID) &&
					!c.ID.Equals(this.ID) &&
					(c is ITextControl || c is ICheckBoxControl || c is FileUpload) &&
					this.fieldName.Equals(c.ID.RemoveLeadingLowerCaseCharacters())).ToList();

				// Only associate if exactly one is found
				if (associatedControls.Count == 1)
				{
					this.AssociatedControlID = associatedControls[0].ID;
				}
			}

			this.Visible = !String.IsNullOrEmpty(this.Text);
		}

		/// <summary>
		/// Adds the HTML attributes and styles of a <see cref="T:System.Web.UI.WebControls.Label"/> control to render to the specified output stream.
		/// </summary>
		/// <param name="writer">An <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		/// <exception cref="T:System.Web.HttpException">
		/// The control specified in the <see cref="P:System.Web.UI.WebControls.Label.AssociatedControlID"/> property cannot be found.
		///   </exception>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			// Remove associated control, so no for attribute is rendered
			string associatedControlId = this.AssociatedControlID;
			this.AssociatedControlID = null;

			// Render attributes
			base.AddAttributesToRender(writer);

			// Restore associated control
			this.AssociatedControlID = associatedControlId;
		}

		#endregion
	}
}
