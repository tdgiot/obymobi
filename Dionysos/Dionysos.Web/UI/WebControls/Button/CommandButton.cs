using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using Dionysos.Reflection;
using System.Web.UI;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class for displaying buttons on a webform
    /// </summary>
    public class CommandButton : Button
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Web.UI.WebControls.CommandButton type
        /// </summary>
        public CommandButton()
        {
            // Set the defaults
            this.CssClass = "button";
        }

        #endregion

        #region Methods

		private bool ExecuteCommand(Control parent)
		{ 
			bool toReturn = false;
			if (Member.HasMember(parent, this.CommandName))
			{
				toReturn = true;
				Member.InvokeMethod(parent, this.CommandName, new object[] { });
			}
			else if (parent.Parent != null)
				toReturn = ExecuteCommand(parent.Parent);
			else
				toReturn = false;
			return toReturn;
		}

        /// <summary>
        /// Raises the System.Web.UI.WebControls.Button.Command event from the System.Web.UI.WebControls.Button instance
        /// </summary>
        /// <param name="e">The System.Web.UI.WebControls.CommandEventArgs instance</param>
        protected override void OnCommand(System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (Instance.Empty(this.CommandName))
            {
                // No command name specified
				throw new Dionysos.TechnicalException("No CommandName defined for '{0}'", this.ID);
            }
            else
            {
				if (!ExecuteCommand(this.Parent))
				{
					throw new Dionysos.TechnicalException("CommandButton '{0}' fired unhandled Command", this.ID, this.CommandName);
				}
            }
        }

        #endregion
    }
}
