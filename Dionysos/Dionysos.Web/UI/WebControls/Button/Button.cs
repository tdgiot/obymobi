using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using Dionysos.Globalization;

namespace Dionysos.Web.UI.WebControls
{
	public class Button : System.Web.UI.WebControls.Button, ILocalizableControl
	{
		#region Fields

		private string cssClassDefault = "button";

		#endregion

		#region Properties
		/// <summary>
		/// Enable/Disable Localization for "Text" when the Parent page has localization
		/// </summary>
		[Category("Localization"), DefaultValue(true)]
		public bool LocalizeText
		{
			get
			{
				if (ViewState["LocalizeText"] != null)
					return (bool)ViewState["LocalizeText"];
				else
					return true;
			}
			set { ViewState["LocalizeText"] = value; }
		}

		/// <summary>
		/// Get/Set Pre-Submit warning. Will Show a JavaDialog asking for Yes/No when Clicked
		/// </summary>
		public string PreSubmitWarning
		{
			get
			{
				if (ViewState["PreSubmitWarning"] != null)
					return (string)ViewState["PreSubmitWarning"];
				else
					return "";
			}
			set { ViewState["PreSubmitWarning"] = value; }
		}

		/// <summary>
		/// Get/Set when button should be displayed (Default: Always)
		/// </summary>
		[Category("Appearance"), DefaultValue(ControlDisplayInMode.Always)]
		public ControlDisplayInMode DisplayInMode
		{
			get
			{
				if (ViewState["DisplayInMode"] != null)
					return (ControlDisplayInMode)ViewState["DisplayInMode"];
				else
					return ControlDisplayInMode.Always;
			}
			set { ViewState["DisplayInMode"] = value; }
		}

		#endregion

		#region Methods

		public Button()
		{
			this.PreRender += new EventHandler(ButtonExtended_PreRender);
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			// Check for empty CssClass and fill it
			if (Instance.Empty(this.CssClass))
			{
				this.CssClass = cssClassDefault;
			}

			if (this.DisplayInMode != ControlDisplayInMode.Always)
			{
				bool renderReadOnly = false;
				if (this.Page.GetType().GetProperty("RenderReadOnly") != null)
				{
					object val = this.Page.GetType().GetProperty("RenderReadOnly").GetValue(this.Page, null);
					if (Convert.ToBoolean(val))
					{
						renderReadOnly = true;
					}
				}

				if (renderReadOnly)
				{
					if (this.DisplayInMode == ControlDisplayInMode.ReadOnly)
						base.Render(writer);
				}
				else
				{
					if (this.DisplayInMode == ControlDisplayInMode.OnEditable)
						base.Render(writer);
				}
			}
			else
			{
				base.Render(writer);
			}
		}

		#endregion

		#region Event Handlers

		void ButtonExtended_PreRender(object sender, EventArgs e)
		{
			if (this.PreSubmitWarning.Length > 0)
			{
				string confirmScript = string.Format("return confirm('{0}');", this.PreSubmitWarning);
				this.Attributes.Add("OnClick", confirmScript);
			}
		}

		#endregion

		#region Localization Logic

		private bool localizePreSubmitWarning = true;

		/// <summary>
		/// Gets / Sets if the control should be localized
		/// </summary>
		[Category("Localization"), DefaultValue(true)]
		public bool LocalizePreSubmitWarning
		{
			get { return this.localizePreSubmitWarning; }
			set { this.localizePreSubmitWarning = value; }
		}

		private string translationTagText = string.Empty;

		/// <summary>
		/// Gets / sets the control's TranslationTag for the Text property, when set this will be used a the TranslationKey instead of the Page's Namespace + Control.ID
		/// </summary>
		[Category("Localization"), DefaultValue("")]
		public string TranslationTagText
		{
			get { return this.translationTagText; }
			set { this.translationTagText = value; }
		}

		private string translationTagPreSubmitWarning = string.Empty;

		/// <summary>
		/// Gets / sets the control's TranslationTag for the PreSubmitWarning property, when set this will be used a the TranslationKey instead of the Page's Namespace + Control.ID
		/// 
		/// </summary>
		[Category("Localization"), DefaultValue("")]
		public string TranslationTagPreSubmitWarning
		{
			get { return this.translationTagPreSubmitWarning; }
			set { this.translationTagPreSubmitWarning = value; }
		}

		/// <summary>
		/// The localizable properties.
		/// </summary>
		protected readonly Dictionary<string, Translatable> localizableProperties = new Dictionary<string, Translatable>();

		/// <summary>
		/// Gets or sets all information for the localizable properties.
		/// </summary>
		/// <value>
		/// The translatable properties.
		/// </value>
		/// <exception cref="TechnicalException">An TranslationTag or ID is required for controls to be localized, failed for control with Text: '{0}' ({1}).</exception>
		public Dictionary<string, Translatable> TranslatableProperties
		{
			get
			{
				// Text property
				if (this.LocalizeText)
				{
					Translatable translatableText;
					if (!this.localizableProperties.TryGetValue("Text", out translatableText))
					{
						// Get translation key
						string translationKey = this.TranslationTagText;
						if (String.IsNullOrEmpty(translationKey) &&
							!String.IsNullOrEmpty(this.ID))
						{
							translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID + ".Text";
						}

						if (String.IsNullOrEmpty(translationKey))
						{
							throw new TechnicalException("An TranslationTagText or ID is required for controls to be localized, failed for control with Text: '{0}' ({1}).", this.Text, this.ClientID);
						}

						translatableText = new Translatable();
						translatableText.TranslationKey = translationKey;
					}

					// Set translatable text
					translatableText.TranslationValue = this.Text;
					this.localizableProperties["Text"] = translatableText;
				}

				// PreSubmitWarning property
				if (this.LocalizePreSubmitWarning)
				{
					Translatable translatablePreSubmitWarning;
					if (!this.localizableProperties.TryGetValue("PreSubmitWarning", out translatablePreSubmitWarning))
					{
						// Get translation key
						string translationKey = this.TranslationTagPreSubmitWarning;
						if (String.IsNullOrEmpty(translationKey) &&
							!String.IsNullOrEmpty(this.ID))
						{
							translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID + ".PreSubmitWarning";
						}

						if (String.IsNullOrEmpty(translationKey))
						{
							throw new TechnicalException("An TranslationTagPreSubmitWarning or ID is required for controls to be localized, failed for control with PreSubmitWarning: '{0}' ({1}).", this.PreSubmitWarning, this.ClientID);
						}

						translatablePreSubmitWarning = new Translatable();
						translatablePreSubmitWarning.TranslationKey = translationKey;
					}

					// Set translatable text
					translatablePreSubmitWarning.TranslationValue = this.PreSubmitWarning;
					this.localizableProperties["PreSubmitWarning"] = translatablePreSubmitWarning;
				}

				return this.localizableProperties;
			}
			set
			{
				// Set the translated values
				Translatable translatableText;
				if (value.TryGetValue("Text", out translatableText))
				{
					this.Text = translatableText.TranslationValue;
				}

				Translatable translatablePreSubmitWarning;
				if (value.TryGetValue("PreSubmitWarning", out translatablePreSubmitWarning))
				{
					this.PreSubmitWarning = translatablePreSubmitWarning.TranslationValue;
				}
			}
		}

		#endregion
	}
}
