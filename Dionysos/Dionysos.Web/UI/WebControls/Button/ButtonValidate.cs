using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using Dionysos.Reflection;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class for displaying buttons on a webform
    /// </summary>
    public class ButtonValidate : Button
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Web.UI.WebControls.CommandButton type
        /// </summary>
        public ButtonValidate()
        {
            // Set the defaults
            this.CausesValidation = true;
        }

        #endregion

        #region Methods



        #endregion
    }
}
