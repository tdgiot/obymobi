using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Dionysos.Interfaces;
using System.Resources;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Class used to create a DropDownList filled with a EntityCollection based on an EntityName using the Dionysos.DataFactory
    /// </summary>
    public abstract class DropDownListEntityCollection : Dionysos.Web.UI.WebControls.DropDownList, IBindable
    {
        #region Fields

        private string entityName;
        private bool useDataBinding = true;
        private object entityCollection;
        private string dataSortField = string.Empty;
           
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor of DropDownListLLBLGenEntityCollection class
        /// </summary>
        public DropDownListEntityCollection()
        {
            // Set DefaultItemValue
            this.DefaultListItemText = Resources.Dionysos_Web_UI_WebControls.DropDownListEntityCollection_Select;
            this.DefaultListItemValue = "-1";
           
            this.HookUpEvents();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Hook up the classes internal events.
        /// </summary>
        private void HookUpEvents()
        {
            this.Init += new EventHandler(DropDownListEntityCollection_Init);
        }

        /// <summary>
        /// Initializes the properly sorted entity collection of the dropdownlist. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <returns>True if initialization was succesful, False if not</returns>
        public abstract bool InitializeEntityCollection();

        /// <summary>
        /// Binds the collection based on the EntityName to the DropDownList
        /// On default the DataValueField = EntityName + "Id" and DataTextField "Name" (only loaded with empty DataValueField and DataTextField properties
        /// </summary>
        void BindDataSource()
        {
            if (this.EntityCollection != null)
            {
                this.DataSource = this.EntityCollection;

                if (Instance.Empty(this.DataValueField))
                {
                    this.DataValueField = this.entityName + "Id";
                }

                this.DataBind();

                // Add "Make Choice"
                // GK Removed: It available in parent > DefaultListItemText & DefaultListItemValue				
				// GK: Not needed, is taken care of by DropDownList
                // ListItem emptyValue = new ListItem("Maak uw keuze", "-1");
                // this.Items.Insert(0, emptyValue);                
            }
            else
            {
                this.Items.Clear();
                this.Items.Add("*Databinding Failed*");
            }

            // Reset Selected Index
            this.SelectedIndex = -1;
            this.SelectedItem.Selected = false;

            // Set selected index based on QuerySTring
            this.SelectItemBasedOnQueryString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// EntityName of which to create the collection to fill the dropdownlist with.
        /// </summary>
        public string EntityName
        {
            get { return entityName; }
            set { entityName = value; }
        }

        /// <summary>
        /// Gets or sets the value of this TextBox
        /// </summary>
        [Browsable(false)]
        public new int Value
        {
            get
            {                
                return this.ValidId;
            }
            set
            {
                this.SelectByItemValue(value);
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether databinding should be used
        /// </summary>
        [Browsable(true)]
        public new bool UseDataBinding
        {
            get
            {
                return this.useDataBinding;
            }
            set
            {
                this.useDataBinding = value;
            }
        }

        #endregion

        #region EventHandlers

        void DropDownListEntityCollection_Init(object sender, EventArgs e)
        {
            // Done in the Init because databinding is in Load an
            // GK Seems to get lost in PostBAck, so no !POstBAck check
            this.InitializeEntityCollection();
            this.BindDataSource();            
        }

        /// <summary>
        /// EntityCollection thas should be used for DataBinding
        /// </summary>
        public object EntityCollection
        {
            get
            { 
                return this.entityCollection; 
            }
            set 
            {
                this.entityCollection = value; 
            }
        }

        /// <summary>
        /// Field to sort the collection on, default DataTextField is used
        /// </summary>
        public string DataSortField
        {
            get
            {
                if (!Instance.Empty(this.dataSortField))
                {
                    return dataSortField;
                }
                else
                {
                    return this.DataTextField;
                }
            }
            set
            {
                dataSortField = value;
            }
        }

        /// <summary>
        /// Gets or sets the field of the datasource that provides the text content for the items
        /// </summary>
        public override string DataTextField
        {
            get
            {
                if (Instance.Empty(base.DataTextField))
                {
                    return "Name";
                }
                else
                {
                    return base.DataTextField;
                }
            }
            set
            {
                base.DataTextField = value;
            }
        }

        #endregion
    }
}
