using System;
using System.Text;
using System.Web;
using System.Collections;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Dionysos.Reflection;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Bindable DropDownList with an Int Value property
	/// </summary>
	public class DropDownListInt : DropDownList, IBindable
	{
		/// <summary>
		/// Gets or sets the value of this TextBox
		/// </summary>
		[Browsable(false)]
		public new int Value
		{
			get
			{
				return this.ValidId;
			}
			set
			{
				this.SelectByItemValue(value);
			}
		}

		/// <summary>
		/// Validate the contorl
		/// </summary>
		public override void Validate()
		{
			if (this.PageMode == PageMode.View)
			{
				// No validation in view page mode
			}
			else
			{
				// Check whether input is required and if so, check whether input is entered
				base.Validate();

				if (this.IsValid &&
					this.IsRequired &&
					(this.AllowZero ? this.ValidId < 0 : this.ValidId <= 0))
				{
					this.IsValid = false;
					this.ErrorMessage = string.Format("Veld '{0}' is verplicht.", this.FriendlyName);
				}
			}
		}
	}
}
