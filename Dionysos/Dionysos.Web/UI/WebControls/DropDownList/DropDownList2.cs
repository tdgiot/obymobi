using System;
using System.Text;
using System.Web;
using System.Collections;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Dionysos.Reflection;
using Dionysos.Interfaces;
using System.Globalization;

namespace Dionysos.Web.UI.WebControls
{
	public class DropDownList2 : System.Web.UI.WebControls.DropDownList, IBindable, IExtendedValidator
	{
		#region Fields

		private PageMode pageMode = PageMode.None;
		private bool renderLabel = true;
		private bool isValid = true;
		private string errorMessage = String.Empty;
		private string friendlyName = String.Empty;
		private string preSubmitWarning = String.Empty;
		private char delimiter = '|';
        protected bool valueSet = false;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether AJAX item management is enabled.
		/// </summary>
		/// <value>
		///   <c>true</c> if AJAX item management is enabled; otherwise, <c>false</c>.
		/// </value>
        public bool AjaxItemManagement { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to localize the default item.
		/// </summary>
		/// <value>
		///   <c>true</c> if the default item is localized; otherwise, <c>false</c>.
		/// </value>
		public bool LocalizeDefaultItem {get; set;}
		/// <summary>
		/// Gets or sets the translation tag.
		/// </summary>
		/// <value>
		/// The translation tag.
		/// </value>
		public string TranslationTag {get; set;}

		/// <summary>
		/// Gets or sets a value indicating whether the last rendered as read only.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the last rendered as read only; otherwise, <c>false</c>.
		/// </value>
		private bool LastRenderedAsReadOnly {get;set;}

		/// <summary>
		/// Gets or sets a value indicating whether all items are localized.
		/// </summary>
		/// <value>
		///   <c>true</c> if all items are localized; otherwise, <c>false</c>.
		/// </value>
		public bool LocalizeAllItems {get; set;}	

		/// <summary>
		/// Gets or sets a value indicating whether zero value is allowed.
		/// </summary>
		/// <value>
		///   <c>true</c> if zero value is allowed; otherwise, <c>false</c>.
		/// </value>
		public bool AllowZero { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance has valid id.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has valid id; otherwise, <c>false</c>.
		/// </value>
		public bool HasValidId
		{
			get
			{
				int validId;
				return !String.IsNullOrEmpty(this.SelectedValue) && // Not empty
						Int32.TryParse(this.SelectedValue, NumberStyles.None, CultureInfo.InvariantCulture, out validId) && // Valid integer
						validId >= 0 && // Positive integer
						(this.AllowZero || validId > 0); // Zero allowed or not
			}
		}

		/// <summary>
		/// Gets the valid id (or -1 if not valid).
		/// </summary>
		public int ValidId
		{
			get
			{
				int validId = -1;
				if (this.HasValidId)
				{
					validId = Int32.Parse(this.SelectedValue);
				}

				return validId;
			}
		}

		/// <summary>
		/// Gets the valid id as byte.
		/// </summary>
		public byte ValidIdAsByte
		{
			get
			{
				byte validIdAsByte = 0;
				if (this.ValidId >= Byte.MinValue &&
					this.ValidId <= Byte.MaxValue)
				{
					validIdAsByte = (byte)this.ValidId;
				}

				return validIdAsByte;
			}
		}

		/// <summary>
		/// Gets/Sets a Default ListItem.Text, this will be appended to the top.
		/// (If not set/length = 0 nothing will be appended)
		/// </summary>
		public string DefaultListItemText {get; set;}

		/// <summary>
		/// Gets/Sets the Url to Link to with the Selected Value / Selected Text if rendered as Read Only
		/// (If not set/length = 0 nothing will happen)
		/// </summary>
		public string LinkToItemUrl {get; set;}
		/// <summary>
		/// Gets/Sets ListItem.Value for the DefaultListItem
		/// (If not set/length = 0 nothing will be appended)
		/// </summary>
		public string DefaultListItemValue {get; set;}

		/// <summary>
		/// Gets or sets the flag indicating whether databinding should be used
		/// </summary>
		[Browsable(true)]
		public bool UseDataBinding { get; set; }

		/// <summary>
		/// Gets or sets the page mode for this control
		/// </summary>
		[Browsable(false)]
		public PageMode PageMode
		{
			get
			{
				return this.pageMode;
			}
			set
			{
				this.pageMode = value;
			}
		}

		/// <summary>
		/// Gets or sets the value of this TextBox
		/// </summary>
		[Browsable(false)]
		public object Value
		{
			get
			{
				return this.SelectedValue;
			}
			set
			{
				this.SelectByItemValue(value != null ? value.ToString() : String.Empty);

				// Fire the value changed event
				//this.OnValueChanged(null, null);
			}
		}

		/// <summary>
		/// Gets or sets the flag if a label should be rendered
		/// </summary>
		[Browsable(true)]
		public bool RenderLabel
		{
			get
			{
				return this.renderLabel;
			}
			set
			{
				this.renderLabel = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag if input is required or not
		/// </summary>
		[Browsable(true)]
		public bool IsRequired { get; set; }

		/// <summary>
		/// ACTUAL JAVASCRIPT NOT YET IMPLEMENTED!!!
		/// Message to show in a popup box before a postback is performed with AutoPostBack.True
		/// ACTUAL JAVASCRIPT NOT YET IMPLEMENTED!!!
		/// </summary>
		[Browsable(true)]
		public string PreSubmitWarning
		{
			get
			{
				return this.preSubmitWarning;
			}
			set
			{
				this.preSubmitWarning = value;
			}
		}

		/// <summary>
		/// Gets or sets the error message which is being set after validation
		/// </summary>
		[Browsable(false)]
		public string FriendlyName
		{
			get
			{
				if (Instance.Empty(this.friendlyName))
				{
					// Try to find a friendly name if not available, via a corresponding lbl
					string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					Label textLabel = this.Parent.FindControl("lbl" + strippedId) as Label;
					if (textLabel != null)
					{
						this.friendlyName = textLabel.Text;
					}
					else
					{
						this.friendlyName = strippedId;
					}
				}
				return this.friendlyName;
			}
			set
			{
				this.friendlyName = value;
			}
		}

		/// <summary>
		/// Get/set delimiter for data persistence in the client side
		/// </summary>
		[Category("AJAX"),
		Description("Delimiter for data persistence in the client side")]
		public char Delimiter
		{
			get { return delimiter; }
			set { delimiter = value; }
		}

		#endregion

		#region Constructors

		public DropDownList2()
		{
			this.HookUpEvents();
			this.CssClass = "input";
            this.TranslationTag = string.Empty;
            this.DefaultListItemText = "None - Select ...";
		}

		#endregion

		#region Event Handlers

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			Page.Validators.Add(this);
		}

		protected override void OnLoad(EventArgs e)
		{
			string uniqueIdUnderscored = this.UniqueID.Replace(":", "_");
			if (Page.IsPostBack && this.AjaxItemManagement)
			{
                // get selected value from form data
			    ArrayList ajaxitems;
			    if (SessionHelper.TryGetValue(uniqueIdUnderscored, out ajaxitems))
			    {
                    string selectedValue = null;
                    if (HttpContext.Current.Request.Params[this.UniqueID] != null)
                    {
                        selectedValue = HttpContext.Current.Request.Params[this.UniqueID];
                    }

                    this.Items.Clear();
                    foreach (object item in ajaxitems)
                    {
                        string[] valueAndId = item.ToString().Split(Char.Parse(";"));
                        this.Items.Add(new ListItem(valueAndId[0], valueAndId[1]));
                    }

			        if (selectedValue != null)
			        {
			            ListItem selectedListItem = this.Items.FindByValue(selectedValue);
			            if (selectedListItem != null)
			            {
			                selectedListItem.Selected = true;
			            }
			        }
			    }
			}
			else
			{
				SessionHelper.RemoveValue(uniqueIdUnderscored);
			}

			base.OnLoad(e);

			// Set the page mode for this control
			if (Member.HasMember(Page, "PageMode"))
			{
				PageMode pageMode = (PageMode)Member.InvokeProperty(Page, "PageMode");
				if (Instance.Empty(pageMode))
				{
					throw new EmptyException("Variable 'pageMode' is empty.");
				}
				else
				{
					this.pageMode = pageMode;
				}
			}
		}

		protected void DropDownList_DataBound(object sender, EventArgs e)
		{
			// Hanlde default Item
			if (this.Items.Count > 0 && this.Items[0].Value == this.DefaultListItemValue && this.Items[0].Text == this.DefaultListItemText)
			{
			}
			else
			{
				if (this.DefaultListItemText.Length > 0)
				{
					ListItem defaultItem = new ListItem(this.DefaultListItemText, this.DefaultListItemValue);
					this.Items.Insert(0, defaultItem);
				}
			}

			this.SelectItemBasedOnQueryString();
		}

		/// <summary>
		/// Helper method to fire up the value changed event
		/// </summary>
		/// <param name="sender">The sender</param>
		/// <param name="e">The EventArgs</param>
		public void OnValueChanged(object sender, EventArgs e)
		{
			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Add standard events to the control
		/// </summary>
		private void HookUpEvents()
		{
			this.DataBound += new EventHandler(DropDownList_DataBound);
		}

		public void SelectItemBasedOnQueryString()
		{
			// Check if the QueryString Contains a value for this DropDownlist if it's not selected yet
			string fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
			// Changed: if (this.pageMode == PageMode.Add && this.ValidId <= 0 && QueryStringHelper.HasParameter(fieldName) && !this.Page.IsPostBack)
			// Removed: this.pageMode == PageMode.Add &&
			string fieldValue;
			if (this.ValidId <= 0 &&
				QueryStringHelper.TryGetValue(fieldName, out fieldValue) &&
				!this.Page.IsPostBack)
			{
				int fieldValueInt;
				if (Int32.TryParse(fieldValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out fieldValueInt))
				{
					this.SelectByItemValue(fieldValueInt);
				}

				if (this.ValidId > 0)
				{
					this.SelectByItemValue(fieldValue);
					//this.Enabled = false;
					this.ToolTip = "Deze waarde is vanaf een andere pagina meegenomen en kan daarom niet worden gewijzigd";
				}
			}
		}

		/// <summary>
		/// Selects the item by value.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		///   <c>true</c> if the value is selected; otherwise, <c>false</c>.
		/// </returns>
		public bool SelectByItemValue(string value)
		{
			bool isSelected = false;

			ListItem li = this.Items.FindByValue(value);
			if (li != null)
			{
				// Set selected item and index
				li.Selected = true;
				this.SelectedIndex = this.Items.IndexOf(li);

				isSelected = true;
			}

			return isSelected;
		}

		/// <summary>
		/// Selects the item by value.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		///   <c>true</c> if the value is selected; otherwise, <c>false</c>.
		/// </returns>
		public bool SelectByItemValue(int value)
		{
			return this.SelectByItemValue(value.ToString());
		}

		/// <summary>
		/// Selects the item by value.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		///   <c>true</c> if the value is selected; otherwise, <c>false</c>.
		/// </returns>
		public bool SelectByItemValue(byte value)
		{
			return this.SelectByItemValue(value.ToString());
		}

		public void FillWithNumericRange(int start, int end)
		{
			this.Items.Clear();
			for (int i = start; i <= end; i++)
			{
				this.Items.Add(new ListItem(i.ToString(), i.ToString()));
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			bool renderReadOnly = false;           

            // Put GetProperty result in new variable because reflection is slow.
            // Don't want to execute it twice
		    var propertyInfo = this.Page.GetType().GetProperty("RenderReadOnly");
			if (propertyInfo != null)
			{
				object val = propertyInfo.GetValue(this.Page, null);
				if (Convert.ToBoolean(val))
				{
					renderReadOnly = true;
				}
			}            

			this.LastRenderedAsReadOnly = renderReadOnly;

			// Render the control as read only when the associated flag is set
			// or the pagemode is set to view
			if ((renderReadOnly && this.renderLabel) || (this.pageMode == PageMode.View && this.renderLabel))
			{
				if (this.SelectedValue == "0" || this.SelectedValue == "-1" ||
					this.SelectedItem == null || this.SelectedItem.Text == null)
					writer.Write("[N/A]");
				else
				{
					if (this.LinkToItemUrl.Length > 0)
					{
						HyperLink hl = new HyperLink();
						hl.Text = this.SelectedItem.Text;
						hl.NavigateUrl = this.LinkToItemUrl + this.SelectedValue;
						hl.Target = "_blank";
						hl.RenderControl(writer);
					}
					else
					{
						//writer.Write(this.SelectedItem.Text);
						Dionysos.Web.UI.WebControls.Label lbl = new Dionysos.Web.UI.WebControls.Label();
						lbl.LocalizeText = false;
						lbl.Text = this.SelectedItem.Text;
						lbl.RenderControl(writer);
					}
				}
			}
			else
			{
				if (this.IsRequired)
				{
					this.CssClass += " required ";
				}

				//Perform the normal rendering
				if (this.isValid)
				{
					this.CssClass.Replace(" ivld ", "");
				}
				else
				{
					this.CssClass += " ivld ";
				}

				base.Render(writer);
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Occurs when the value changed between posts to the server
		/// </summary>
		public event EventHandler ValueChanged;

		#endregion

		#region IExtendedValidator Members

		/// <summary>
		/// Indicates whether this control is valid after validation
		/// </summary>
		[Browsable(false)]
		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
			}
		}

		/// <summary>
		/// Gets or sets the error message which is being set after validation
		/// </summary>
		[Browsable(false)]
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		/// <summary>
		/// Validate the contorl
		/// </summary>
		public virtual void Validate()
		{
			if (this.pageMode == PageMode.View)
			{
				// No validation in view page mode
			}
			else
			{
				// Check whether input is required
				// and if so, check whether input is entered

				// First Check Entity Validation
				if (this.Page is Dionysos.Web.UI.PageLLBLGenEntity)
				{
					IDataErrorInfo errorInfo = ((Dionysos.Web.UI.PageLLBLGenEntity)this.Page).DataErrorInfo;
					string fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					if (errorInfo[fieldName] != null && errorInfo[fieldName].Length > 0)
					{
						this.isValid = false;
						this.errorMessage = String.Format("<strong>{0}</strong>: {1}", this.FriendlyName, errorInfo[fieldName].Replace(fieldName, this.FriendlyName));
					}
				}

				// Only check if no LLBL errors are found
				if (this.isValid)
				{
					// Check whether input is required
					// and if so, check whether input is entered
					if (this.IsRequired &&
						(String.IsNullOrEmpty(this.SelectedValue) || this.SelectedValue == "-1" || (!String.IsNullOrEmpty(this.DefaultListItemValue) && this.SelectedValue == this.DefaultListItemValue && (!this.AllowZero || this.SelectedValue != "0"))))
					{
						this.isValid = false;
						this.errorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.DropDownList_ErrorMessage_Required, this.FriendlyName);
					}
				}
			}
		}

		public bool UseValidation { get; set; }

		#endregion

		#region Option Group Support

		/// <summary>
		/// Renders the items in the <see cref="T:System.Web.UI.WebControls.ListControl"/> control.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream used to write content to a Web page.</param>
		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
		{
			ListItemCollection items = this.Items;
			int count = items.Count;
			string optionTag = "option", optGroupTag = "optgroup";
			string optGroup;
			if (count > 0)
			{
				bool flag = false;
				string prevOptGroup = null;
				for (int i = 0; i < count; i++)
				{
					optGroup = null;
					ListItem item = items[i];
					if (item.Enabled)
					{
						if (item.Attributes != null &&
							item.Attributes["optgroup"] != null)
						{
							// Write optgroup begin tag
							optGroup = item.Attributes[optGroupTag];
							if (prevOptGroup != optGroup)
							{
								if (prevOptGroup != null)
								{
									writer.WriteEndTag(optGroupTag);
								}
								writer.WriteBeginTag(optGroupTag);
								if (!String.IsNullOrEmpty(optGroup))
								{
									writer.WriteAttribute("label", optGroup);
								}
								writer.Write('>');
							}

							item.Attributes.Remove("optgroup");
							prevOptGroup = optGroup;
						}
						else
						{
							if (prevOptGroup != null)
							{
								writer.WriteEndTag(optGroupTag);
							}
							prevOptGroup = null;
						}

						writer.WriteBeginTag(optionTag);
						if (item.Selected)
						{
							if (flag)
							{
								this.VerifyMultiSelect();
							}
							flag = true;
							writer.WriteAttribute("selected", "selected");
						}
						writer.WriteAttribute("value", item.Value, true);
						if (item.Attributes != null &&
							item.Attributes.Count > 0)
						{
							item.Attributes.Render(writer);
						}
						if (optGroup != null)
						{
							item.Attributes.Add("optgroup", optGroup);
						}
						if (this.Page != null)
						{
							this.Page.ClientScript.RegisterForEventValidation(this.UniqueID, item.Value);
						}

						writer.Write('>');
						HttpUtility.HtmlEncode(item.Text, writer);
						writer.WriteEndTag(optionTag);
						writer.WriteLine();
						if (i == count - 1)
						{
							if (prevOptGroup != null)
							{
								writer.WriteEndTag(optGroupTag);
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Saves the current view state of the <see cref="T:System.Web.UI.WebControls.ListControl"/> -derived control and the items it contains.
		/// </summary>
		/// <returns>
		/// An <see cref="T:System.Object"/> that contains the saved state of the <see cref="T:System.Web.UI.WebControls.ListControl"/> control.
		/// </returns>
		protected override object SaveViewState()
		{
			object[] state = new object[this.Items.Count + 1];
			object baseState = base.SaveViewState();
			state[0] = baseState;
			bool itemHasAttributes = false;

			for (int i = 0; i < this.Items.Count; i++)
			{
				if (this.Items[i].Attributes.Count > 0)
				{
					itemHasAttributes = true;
					object[] attributes = new object[this.Items[i].Attributes.Count * 2];
					int k = 0;

					foreach (string key in this.Items[i].Attributes.Keys)
					{
						attributes[k] = key;
						k++;
						attributes[k] = this.Items[i].Attributes[key];
						k++;
					}
					state[i + 1] = attributes;
				}
			}

			if (itemHasAttributes)
				return state;
			return baseState;
		}

		/// <summary>
		/// Loads the previously saved view state of the <see cref="T:System.Web.UI.WebControls.DetailsView"/> control.
		/// </summary>
		/// <param name="savedState">An <see cref="T:System.Object"/> that represents the state of the <see cref="T:System.Web.UI.WebControls.ListControl"/> -derived control.</param>
		protected override void LoadViewState(object savedState)
		{
			if (savedState == null)
				return;

			if (!(savedState.GetType().GetElementType() == null) &&
				(savedState.GetType().GetElementType().Equals(typeof(object))))
			{
				object[] state = (object[])savedState;
				base.LoadViewState(state[0]);

				for (int i = 1; i < state.Length; i++)
				{
					if (state[i] != null)
					{
						object[] attributes = (object[])state[i];
						for (int k = 0; k < attributes.Length; k += 2)
						{
							this.Items[i - 1].Attributes.Add
								(attributes[k].ToString(), attributes[k + 1].ToString());
						}
					}
				}
			}
			else
			{
				base.LoadViewState(savedState);
			}
		}

		#endregion
	}
}
