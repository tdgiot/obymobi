using System;
using System.Text;
using System.Web;
using System.Collections;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Dionysos.Reflection;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Bindable DropDownList with an Int Value property
	/// </summary>
    public class DropDownListInt2 : DropDownList2, IBindable, IUltraControl
	{
		/// <summary>
		/// Gets or sets the value of this TextBox
		/// </summary>
		[Browsable(false)]
		public new int? Value
		{
			get
			{
                if (this.ValidId >= 0)
                    return this.ValidId;
                else
                    return null;				
			}
			set
			{
				this.SelectByItemValue(value ?? -1);
                this.valueSet = true;
			}
		}

		/// <summary>
		/// Validate the contorl
		/// </summary>
		public override void Validate()
		{
			if (this.PageMode == PageMode.View)
			{
				// No validation in view page mode
			}
			else
			{
				// Check whether input is required and if so, check whether input is entered
				base.Validate();

				if (this.IsValid &&
					this.IsRequired &&
					(this.AllowZero ? this.ValidId < 0 : this.ValidId <= 0))
				{
					this.IsValid = false;
					this.ErrorMessage = string.Format("Veld '{0}' is verplicht.", this.FriendlyName);
				}
			}
		}
	}
}
