using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Class used to create a DropDownList filled with a EntityCollection based on an EntityName using the Dionysos.DataFactory
    /// </summary>
    public class DropDownListLLBLGenEntityCollection : Dionysos.Web.UI.WebControls.DropDownListEntityCollection, IBindable
    {
        #region Methods

        /// <summary>
        /// Initialize the EntityCollection
        /// </summary>
        /// <returns>If it was succesful.</returns>
        public override bool InitializeEntityCollection()
        {
            SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entities = (SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection)Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName);
            entities.GetMulti(null);

            if (entities.Count > 0)
            {
                if (entities[0].Fields[this.DataSortField] != null)
                {
                    entities.Sort(this.DataSortField, ListSortDirection.Ascending, null);
                }
            }

            if (this.DataSortField.Length > 0)
                entities.Sort(this.DataSortField, ListSortDirection.Ascending, null);

            this.EntityCollection = entities;

            
            
            return !Instance.Empty(this.EntityCollection);
        }

        #endregion

        public override void Validate()
        {
            if (this.IsRequired)
            { 

            }
            base.Validate();
        }
    }
}
