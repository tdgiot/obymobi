﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Represents a list control that encapsulates a group of radio button controls.
	/// </summary>
	public class RadioButtonListString : RadioButtonList
	{
		/// <summary>
		/// Gets or sets the selected value.
		/// </summary>
		public new string Value
		{
			get
			{
				return this.SelectedValue;
			}
			set
			{
				this.SelectedValue = value;
			}
		}
	}
}
