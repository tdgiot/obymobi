﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Radio button list for bit(null) values, true, false, null
    /// </summary>
    public class RadioTrueFalseNaNull : RadioButtonList
    {
        private bool isRequired = false;

        /// <summary>
        /// Radiobutton list with Yes, No, Unknown, Null
        /// </summary>
        public RadioTrueFalseNaNull()
        {
            // Add values
            this.StartValue = 1;
            this.UseValidation = true;
            this.Items.Add(new ListItem(Dionysos.Resources.DionysosResources.Yes, "1"));
            this.Items.Add(new ListItem(Dionysos.Resources.DionysosResources.No, "2"));
            this.Items.Add(new ListItem(Dionysos.Resources.DionysosResources.NotApplicable, "3"));
            this.Items.Add(new ListItem(Dionysos.Resources.DionysosResources.Unknown, "4"));
            base.Value = 4;
            this.RepeatDirection = RepeatDirection.Horizontal;
        }

        public override void Validate()
        {
            this.IsValid = true;
            if (this.IsRequired)
            {
                if (!this.Value.HasValue)
                {
                    this.IsValid = false;
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.RadioTrueFalseNull_ErrorMessage_Required, this.FriendlyName);
                }
            }
        }

        public new int? Value
        {
            get
            {
                if (base.Value == 1)
                    return 1;
                else if (base.Value == 2)
                    return 2;
                else if (base.Value == 3)
                    return 3;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    base.Value = 4;
                else
                    base.Value = value.Value;
            }
        }

        /// <summary>
        /// Gets or sets the IsRequired
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return this.isRequired;
            }
            set
            {
                this.isRequired = value;
            }
        }

    }
}
