﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which is used to display lists of radiobutton controls
	/// </summary>
	public class RadioButtonList : System.Web.UI.WebControls.RadioButtonList, IBindable, IExtendedValidator
	{
		#region Fields

		private int value = 0;
		private int startValue = 0;
		private bool useDataBinding = true;
		private bool useValidation = false;
		private bool isValid = true;
		private string errorMessage = String.Empty;
		private string friendlyName = String.Empty;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public virtual int Value
		{
			get
			{
				this.GetValue();
				return this.value;
			}
			set
			{
				this.value = value;
				this.SetValue();
			}
		}

		/// <summary>
		/// Gets or sets the start value.
		/// </summary>
		/// <value>The start value.</value>
		public int StartValue
		{
			get
			{
				return this.startValue;
			}
			set
			{
				this.startValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag which indicated whether databinding should be used.
		/// </summary>
		/// <value><c>true</c> if databinding is used; otherwise, <c>false</c>.</value>
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to use validation.
		/// </summary>
		/// <value><c>true</c> if validation is used; otherwise, <c>false</c>.</value>
		[Browsable(true)]
		public bool UseValidation
		{
			get
			{
				return this.useValidation;
			}
			set
			{
				this.useValidation = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the input is valid.
		/// </summary>
		/// <value><c>true</c> if the input is valid; otherwise, <c>false</c>.</value>
		[Browsable(false)]
		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
			}
		}

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>The error message.</value>
		[Browsable(false)]
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		/// <summary>
		/// Gets or sets the friendly name.
		/// </summary>
		/// <value>The friendly name.</value>
		[Browsable(true)]
		public string FriendlyName
		{
			get
			{
				// Try to find a friendly name if not available, via a corresponding lbl
				if (String.IsNullOrEmpty(this.friendlyName))
				{
					string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					Label textLabel = this.Parent.FindControl("lbl" + strippedId) as Label;
					if (textLabel != null)
					{
						this.friendlyName = textLabel.Text;
					}
					else
					{
						this.friendlyName = strippedId;
					}
				}

				return this.friendlyName;
			}
			set
			{
				this.friendlyName = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="RadioButtonList"/> class.
		/// </summary>
		public RadioButtonList()
		{
			this.SelectedIndexChanged += new EventHandler(RadioButtonList_SelectedIndexChanged);
		}

		#endregion

		#region Events

		/// <summary>
		/// Occurs when the value is changed.
		/// </summary>
		public event EventHandler ValueChanged;

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.Page.Validators.Add(this);
			this.EnableViewState = true;
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the RadioButtonList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void RadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.OnValueChanged(sender, e);
		}

		/// <summary>
		/// Called when the value is changed.
		/// </summary>
		/// <param name="sender">The sender</param>
		/// <param name="e">The EventArgs</param>
		public void OnValueChanged(object sender, EventArgs e)
		{
			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

		#endregion

		#region Methods

        /// <summary>
        /// Data binds the enum using the default string value as text.
        /// </summary>
        /// <typeparam name="T">The enum type.</typeparam>
        public void DataBindEnum<T>()
            where T : struct, IComparable, IFormattable, IConvertible
        {
            this.DataBindEnum<T>(e => EnumUtil.GetStringValue(e as Enum, null), false);
        }

		/// <summary>
		/// Data binds the enum.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="textSelector">The text selector.</param>
		/// <param name="orderByText">If set to <c>true</c> orders the items by text.</param>
		public void DataBindEnum<T>(Func<T, string> textSelector, bool orderByText)
            where T : struct, IComparable, IFormattable, IConvertible
        {
            Dictionary<int, string> dataSource = EnumUtil.ToArray<T>().ToDictionary(e => e.ToInt32(CultureInfo.InvariantCulture), textSelector);
            if (orderByText)
            {
                dataSource = dataSource.OrderBy(kv => kv.Value).ToDictionary(kv => kv.Key, kv => kv.Value);
            }

            this.DataSource = dataSource;
            this.DataValueField = "Key";
			this.DataTextField = "Value";
            this.DataBind();
        }

        /// <summary>
		/// Gets the value.
		/// </summary>
		private void GetValue()
		{
			for (int i = 0; i < this.Items.Count; i++)
			{
				if (this.Items[i].Selected)
				{
					this.value = i + this.StartValue;
					break;
				}
			}
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		private void SetValue()
		{
			if (this.Items.Count >= this.value)
			{
				for (int i = 0; i < this.Items.Count; i++)
				{
					if (i == (this.value - this.StartValue))
					{
						this.Items[i].Selected = true;
					}
					else
					{
						this.Items[i].Selected = false;
					}
				}
			}
		}

		/// <summary>
		/// Validates the input of the control (if required).
		/// </summary>
		public virtual void Validate()
		{
			this.IsValid = true;

			if (this.UseValidation)
			{
				this.IsValid = false;

				for (int i = 0; i < this.Items.Count; i++)
				{
					if (this.Items[i].Selected)
					{
						this.IsValid = true;
						break;
					}
				}

				if (!this.IsValid)
				{
					this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.RadioButtonList_ErrorMessage_Required, this.FriendlyName);
				}
			}
		}

		#endregion
	}
}
