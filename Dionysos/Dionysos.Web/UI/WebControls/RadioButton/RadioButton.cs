using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace Dionysos.Web.UI.WebControls
{
    public class RadioButton : System.Web.UI.WebControls.RadioButton
    {
        #region Fields
        #endregion

        #region Properties
        /// <summary>
        /// Enable/Disable Localization for "Text" when the Parent page has localization
        /// </summary>
        [Category("Localization"), DefaultValue(true)]
        public bool LocalizeText
        {
            get
            {
                if (ViewState["LocalizeText"] != null)
                    return (bool)ViewState["LocalizeText"];
                else
                    return true;
            }
            set { ViewState["LocalizeText"] = value; }
        }
        #endregion

        #region Methods

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            bool renderReadOnly = false;
            if (this.Page.GetType().GetProperty("RenderReadOnly") != null)
            {
                object val = this.Page.GetType().GetProperty("RenderReadOnly").GetValue(this.Page, null);
                if (Convert.ToBoolean(val))
                {
                    renderReadOnly = true;
                }
            }

            // TODO: Use CSS
            if (renderReadOnly)
            {
                if (this.Checked)
                    writer.Write("<font style=\"color:green\"><strong>V</strong></font>");
                else
                    writer.Write("<font style=\"color:red\"><strong>X</strong></font>");
            }
            else
            {
                //Perform the normal rendering
                base.Render(writer);
            }
        }

        #endregion

        #region Event Handlers

        #endregion
    }
}
