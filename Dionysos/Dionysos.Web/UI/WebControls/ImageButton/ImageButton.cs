using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Dionysos implementation of the ImageButton
	/// </summary>
	public class ImageButton : System.Web.UI.WebControls.ImageButton
	{
		#region Fields

		private string preSubmitWarning = string.Empty;
		private string text = string.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor of ImageButton
		/// </summary>
		public ImageButton()
		{
			// Hookup the events
			this.HookupEvents();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Hookup the events to the corresponding event handlers
		/// </summary>
		private void HookupEvents()
		{
			this.PreRender += new EventHandler(ImageButton_PreRender);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the pre-submit warning. Will Show a JavaDialog asking for Yes/No when Clicked
		/// </summary>
		public string PreSubmitWarning
		{
			get
			{
				return this.preSubmitWarning;
			}
			set
			{
				this.preSubmitWarning = value;
			}
		}

		/// <summary>
		/// Gets or sets the text of the button
		/// </summary>
		public new string Text
		{
			get
			{
				return this.text;
			}
			set
			{
				this.text = value;
			}
		}

		#endregion

		#region EventHandlers

		private void ImageButton_PreRender(object sender, EventArgs e)
		{
			if (this.preSubmitWarning != string.Empty)
			{
				this.Attributes.Add("onClick", string.Format("if(!confirm('{0}')){{return false;}}", this.preSubmitWarning));
			}

			if (this.ImageUrl != null)
			{
				this.ImageUrl = this.ResolveUrl(this.ImageUrl);
			}
		}

		#endregion
	}
}
