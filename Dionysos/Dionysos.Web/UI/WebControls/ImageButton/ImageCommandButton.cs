using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Reflection;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class for displaying buttons on a webform
    /// </summary>
    public class ImageCommandButton : Dionysos.Web.UI.WebControls.ImageButton, INamingContainer
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.CommandButton type
        /// </summary>
        public ImageCommandButton()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the System.Web.UI.WebControls.Button.Command event from the System.Web.UI.WebControls.Button instance
        /// </summary>
        /// <param name="e">The System.Web.UI.WebControls.CommandEventArgs instance</param>
        protected override void OnCommand(System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (Instance.Empty(this.CommandName))
            {
                // No command name specified
            }
            else
            {
                // GK Maybe we can even do with Parent only... more generic
                object[] parameters;
                if (Instance.Empty(e.CommandArgument))
                {
                    parameters = new object[] { e.CommandArgument };
                }
                else
                {
                    parameters = new object[] { };
                }

                Control parent = ControlHelper.GetParentUIElement(this);
                if (Instance.Empty(parent))
                {
                    parent = this.Page;
                }

                if (Instance.Empty(parent))
                {
                    throw new EmptyException("Variable 'parent' is empty.");
                }
                else
                {
                    if (Member.HasMember(parent, this.CommandName))
                    {
                        Member.InvokeMethod(parent, this.CommandName, parameters);
                    }
                }
            }
        }

        /// <summary>
        /// Creates the child controls of this ImageCommandButton instance
        /// </summary>
        protected override void CreateChildControls()
        {
            if (Instance.Empty(this.Text))
            {
                base.CreateChildControls();
            }
            else
            {
                Table table = new Table();
                table.ID = string.Format("tbl{0}", this.ID);
                table.CellPadding = 0;
                table.CellSpacing = 0;
                table.Style.Add("display", "inline");
                table.Style.Add("vertical-align", "middle");

                TableRow row = new TableRow();
                row.ID = string.Format("tr{0}", this.ID);

                TableCell imgCell = new TableCell();
                imgCell.ID = string.Format("tdImg{0}", this.ID);

                ImageCommandButton imgButton = new ImageCommandButton();
                imgButton.ID = string.Format("imgButton{0}", this.ID);
                imgButton.ImageUrl = this.ImageUrl;
                imgButton.CommandName = this.CommandName;
                imgButton.ToolTip = this.ToolTip;

                imgCell.Controls.Add(imgButton);

                TableCell txtCell = new TableCell();
                txtCell.ID = string.Format("tdTxt{0}", this.ID);

                Label lbl = new Label();
                lbl.ID = string.Format("lbl{0}", this.ID);
                lbl.Text = this.Text;

                txtCell.Controls.Add(lbl);

                row.Cells.Add(imgCell);
                row.Cells.Add(txtCell);

                table.Rows.Add(row);

                this.Controls.Add(table);
            }
        }

        #endregion 
       
        #region EventHandlers

        #endregion
    }
}
