using System;
using System.Web;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Reflection;
using System.Threading;
using System.Resources;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;
using Dionysos.Globalization;

namespace Dionysos.Web.UI.WebControls
{
	public class MultiValidatorDesigner : System.Web.UI.Design.ControlDesigner
	{
		// Whether to display the html of the associated 
		// control using a large heading text size.		

		public MultiValidatorDesigner()
			: base()
		{

		}

		// Returns the html to use to represent the control at design time.
		public override string GetDesignTimeHtml()
		{
			return "[Multi Validator]";
		}
	}

	/// <summary>
	/// Summary description for MultiValidator.
	/// </summary>
	[DesignerAttribute(typeof(MultiValidatorDesigner), typeof(IDesigner))]
	public class MultiValidator : WebControl, IValidator
	{
		#region Members

		private bool isValid = true;
		private string errorMessage = "";
		private List<Error> errors = new List<Error>();
		private ArrayList validators = new ArrayList();
		private string validationGroup = string.Empty;

		#endregion

		#region Methods

		/// <summary>
		/// Add an error like: <strong>titleInBold</strong>: message 
		/// </summary>
		/// <param name="errorType">Prefix to set css class in D.ValidationSummary</param>
		/// <param name="titleInBold">Title (first part in bold)</param>
		/// <param name="message">message</param>
		/// <param name="validationGroup">The validation group for the error</param>
		public void AddError(string errorType, string titleInBold, string message, string validationGroup)
		{
			// Check if error is already in place
			bool found = false;
			for (int i = 0; i < this.errors.Count; i++)
			{
				Error error = this.errors[i];
				if (error.Message == message)
				{
					found = true;
					break;
				}
			}

			if (!found)
			{
				Error error = new Error();
				if (errorType.Length > 0 && titleInBold.Length > 0)
					error.Message = string.Format("{0}<strong>{1}:</strong> {2}", errorType, titleInBold, message);
				else if (titleInBold.Length > 0)
					error.Message = string.Format("<strong>{0}:</strong> {1}", titleInBold, message);
				else
					error.Message = message;

				if (validationGroup.Length > 0)
					error.ValidationGroup = validationGroup;

				this.errors.Add(error);
			}
		}

		/// <summary>
		/// Add an error like: <strong>titleInBold</strong>: message 
		/// </summary>
		/// <param name="errorType">Prefix to set css class in D.ValidationSummary</param>
		/// <param name="titleInBold">Title (first part in bold)</param>
		/// <param name="message">message</param>
		public void AddError(string errorType, string titleInBold, string message)
		{
			AddError(errorType, titleInBold, message, string.Empty);
		}

		/// <summary>
		/// Add an error like: <strong>titleInBold</strong>: message 
		/// </summary>
		/// <param name="message">message</param>
		/// <param name="validationGroup">The validation group for the error</param>
		public void AddError(string message, string validationGroup)
		{
			AddError(string.Empty, string.Empty, message, validationGroup);
		}

		/// <summary>
		/// Add an information like: <strong>titleInBold</strong>: message 
		/// </summary>
		/// <param name="message">message</param>
		public void AddInformation(string message)
		{
			AddError(message, string.Empty);
		}

		/// <summary>
		/// Add an warning like: <strong>titleInBold</strong>: message 
		/// </summary>
		/// <param name="message">message</param>
		public void AddWarning(string message)
		{
			AddError(message, string.Empty);
		}

		/// <summary>
		/// Add an error like: <strong>titleInBold</strong>: message 
		/// </summary>
		/// <param name="message">message</param>
		public void AddError(string message)
		{
			AddError(message, string.Empty);
		}

		/// <summary>
		/// Remove all set Errors from the Error Array
		/// </summary>
		public void ClearErrors()
		{
			this.errors.Clear();
		}

		/// <summary>
		/// Empty Render method (override), so no HTML will be rendered to the page.
		/// </summary>
		/// <param name="writer"></param>
		protected override void Render(HtmlTextWriter writer)
		{
			// Do not render this control
			// base.Render(writer);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Error message
		/// </summary>
		[Bindable(true)]
		[Category("Appearance")]
		[DefaultValue("")]
		public string ErrorMessage
		{
			get
			{
				return errorMessage;
			}
			set
			{
				errorMessage = value;
			}
		}

		/// <summary>
		/// Gets or sets the validation group
		/// </summary>
		public string ValidationGroup
		{
			get
			{
				return this.validationGroup;
			}
			set
			{
				this.validationGroup = value;
			}
		}

		/// <summary>
		/// Returns true/false if there are any errors
		/// </summary>
		public bool HasErrors
		{
			get { return this.errors.Count > 0; }
		}

		#endregion

		#region Event handlers

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			Page.Validators.Add(this);
		}

		protected override void OnUnload(EventArgs e)
		{
			if (Page != null)
			{
				Page.Validators.Remove(this);
				foreach (IValidator validator in this.validators)
					Page.Validators.Remove(validator);
			}

			base.OnUnload(e);
		}

		#endregion

		#region IValidator

		void IValidator.Validate()
		{
			isValid = (this.errors.Count == 0);
			foreach (Error error in this.errors)
			{
				StaticValidator validator = new StaticValidator();
				validator.ErrorMessage = error.Message;
				//if (error.ValidationGroup.Length > 0)
				//    validator.ValidationGroup = error.ValidationGroup;
				Page.Validators.Add(validator);
				this.validators.Add(validator);
			}

			// GK Tricky solution to prevent errors double displayed if this.validate() is requested twice on a page... 
			this.errors.Clear();
		}

		/// <summary>
		/// Flag which indicates whether this multivalidator is valid or not
		/// </summary>
		bool IValidator.IsValid
		{
			get
			{
				return isValid;
			}
			set
			{
				isValid = value;
			}
		}

		#endregion
	}

	//[ToolboxItem(false)]
	//internal class StaticValidator : IValidator
	//{
	//    private string errorMessage;

	//    #region IValidator
	//    void IValidator.Validate()
	//    {
	//        System.Diagnostics.Trace.WriteLine("StaticValidator.Validate()");
	//    }//Validate

	//    bool IValidator.IsValid
	//    {
	//        get { return false; }
	//        set { }
	//    }//IsValid
	//    #endregion

	//    public string ErrorMessage
	//    {
	//        get { return errorMessage; }
	//        set { errorMessage = value; }
	//    }//ErrorMessage
	//}

	[ToolboxItem(false)]
	internal class StaticValidator : IValidator
	{
		private string errorMessage = string.Empty;
		private string validationGroup = string.Empty;

		#region IValidator

		void IValidator.Validate()
		{
			System.Diagnostics.Trace.WriteLine("StaticValidator.Validate()");
		}

		bool IValidator.IsValid
		{
			get { return false; }
			set { }
		}

		#endregion

		public string ErrorMessage
		{
			get { return errorMessage; }
			set { errorMessage = value; }
		}

		public string ValidationGroup
		{
			get { return validationGroup; }
			set { validationGroup = value; }
		}
	}

	[ToolboxItem(false)]
	internal class Error
	{
		#region Members

		private string message = string.Empty;
		private string validationGroup = string.Empty;

		#endregion

		#region Constructors

		public Error()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the error message
		/// </summary>
		public string Message
		{
			get
			{
				return this.message;
			}
			set
			{
				this.message = value;
			}
		}

		/// <summary>
		/// Gets or sets the validation group for this error
		/// </summary>
		public string ValidationGroup
		{
			get
			{
				return this.validationGroup;
			}
			set
			{
				this.validationGroup = value;
			}
		}

		#endregion
	}
}

