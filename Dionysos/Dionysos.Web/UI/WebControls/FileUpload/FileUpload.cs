using System;
using System.Collections.Generic;
using System.ComponentModel;
using Dionysos.Interfaces;
using System.Web.Configuration;
using Dionysos.Collections;
using System.Web;
using System.Linq;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which represents a file upload control
	/// </summary>
	public class FileUpload : System.Web.UI.WebControls.FileUpload, IExtendedValidator, IBindable
	{
		#region Fields

		private bool isRequired = false;
		private bool useDataBinding = false;
		private string value = string.Empty;
		private bool useValidation = false;
		private bool isValid = true;
		private string errorMessage = string.Empty;
		private string friendlyName = string.Empty;
		private string accept = string.Empty;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name of the validation group to which this validation control belongs.
		/// </summary>
		public string ValidationGroup { get; set; }

		/// <summary>
		/// Gets or sets the flag if input is required or not
		/// </summary>
		[Browsable(true)]
		public bool IsRequired
		{
			get
			{
				return this.isRequired;
			}
			set
			{
				this.isRequired = value;
			}
		}

        /// <summary> 
        /// The accept attribute specifies a filter for what file types the user can pick from the file input dialog box. 
        /// </summary> 
        [Browsable(true)]
        public string Accept
        {
            get
            {
                return accept;
            }
            set
            {
                this.Attributes["accept"] = value;
                accept = value;
            }
        }

		/// <summary>
		/// Gets or sets the size of the max file (in kB).
		/// </summary>
		/// <value>
		/// The size of the max file (in kB).
		/// </value>
		public int AllowedFileSize { get; set; }

		/// <summary>
		/// Gets or sets the allowed content types (eg. image/png or image, seperated by ;).
		/// </summary>
		/// <value>
		/// The allowed content types (eg. image/png or image).
		/// </value>
		public string AllowedContentTypes { get; set; }

		/// <summary>
		/// Gets or sets the flag which indicates if databinding should be used
		/// </summary>
		[Browsable(true)]
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		/// <summary>
		/// Gets or sets the file name
		/// </summary>
		public string Value
		{
			get
			{
				return this.FileName;
			}
			set
			{
				//this.FileName = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag which indicates if validation should be used
		/// </summary>
		public bool UseValidation
		{
			get
			{
				return this.useValidation;
			}
			set
			{
				this.useValidation = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag which indicates whether the input is succesfully validated
		/// </summary>
		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
			}
		}

		/// <summary>
		/// Gets or sets the validation error message
		/// </summary>
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		/// <summary>
		/// Gets or sets the friendly name for this control
		/// </summary>
		[Browsable(true)]
		public string FriendlyName
		{
			get
			{
				// Try to find a friendly name if not available, via a corresponding lbl
				if (Instance.Empty(this.friendlyName))
				{
					string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					Label textLabel = this.Parent.FindControl("lbl" + strippedId) as Label;
					if (textLabel != null)
					{
						this.friendlyName = textLabel.Text;
					}
					else
					{
						this.friendlyName = strippedId;
					}
				}
				return this.friendlyName;
			}
			set
			{
				this.friendlyName = value;
			}
		}

		/// <summary>
		/// Gets the size of the max file (in kB).
		/// </summary>
		/// <value>The size of the max file (in kB).</value>
		public static int MaxFileSize
		{
			get
			{
				System.Configuration.Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
				HttpRuntimeSection section = config.GetSection("system.web/httpRuntime") as HttpRuntimeSection;

				return section.MaxRequestLength;
			}
		}

		/// <summary>
		/// Gets or sets a value that specifies whether multiple files can be selected for upload.
		/// </summary>
		/// <value>
		///   <c>true</c> if multiple files can be selected; otherwise, <c>false</c>.
		/// </value>
		public bool AllowMultiple { get; set; }

		/// <summary>
		/// Gets a value that indicates whether any files have been uploaded.
		/// </summary>
		/// <value>
		///   <c>true</c> if any files have been uploaded; otherwise, <c>false</c>.
		/// </value>
		public bool HasFiles
		{
			get
			{
				return this.PostedFiles.Any(f => f.ContentLength > 0);
			}
		}

		/// <summary>
		/// Gets the collection of uploaded files.
		/// </summary>
		/// <value>
		/// The collection of uploaded files.
		/// </value>
		public IList<HttpPostedFile> PostedFiles
		{
			get
			{
				List<HttpPostedFile> postedFiles = new List<HttpPostedFile>();
				if (this.Page != null &&
					this.Page.IsPostBack)
				{
					HttpFileCollection files = this.Context.Request.Files;
					for (int i = 0; i < files.Count; i++)
					{
						if (String.Equals(this.UniqueID, files.GetKey(i), StringComparison.InvariantCultureIgnoreCase))
						{
							postedFiles.Add(files[i]);
						}
					}
				}

				return postedFiles;
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Occurs when the value changed between posts to the server
		/// </summary>
		public event EventHandler ValueChanged;

		#endregion

		#region Event Handlers

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.Page.Validators.Add(this);
		}

		/// <summary>
		/// Helper method to fire up the value changed event
		/// </summary>
		/// <param name="sender">The sender</param>
		/// <param name="e">The EventArgs</param>
		public void OnValueChanged(object sender, EventArgs e)
		{
			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.PreRender" /> event for the <see cref="T:System.Web.UI.WebControls.FileUpload" /> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			if (this.AllowMultiple) this.Attributes["multiple"] = "multiple";

			base.OnPreRender(e);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Validates the input of the control (if required)
		/// and checks whether the control has input
		/// </summary>
		public void Validate()
		{
			this.IsValid = true;
			if (this.UseValidation)
			{
				// Check whether input is required
				// and if so, check whether input is entered
				if (this.IsRequired && !this.HasFiles)
				{
					this.IsValid = false;
					this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.FileUpload_ErrorMessage_Required, this.FriendlyName);
				}
				else if (this.HasFiles)
				{
					IEnumerable<HttpPostedFile> postedFiles = this.PostedFiles.Where(f => f.ContentLength > 0);

					if (this.AllowedFileSize > 0 &&
						(postedFiles.Sum(f => f.ContentLength) / 1024m) > this.AllowedFileSize)
					{
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.FileUpload_ErrorMessage_AllowedFileSize, this.FriendlyName, this.AllowedFileSize);
					}
					else if (!String.IsNullOrEmpty(this.AllowedContentTypes) &&
						postedFiles.All(f => !String.IsNullOrEmpty(f.ContentType)))
					{
						bool allowed = true;

						foreach (HttpPostedFile postedFile in postedFiles)
						{
							bool postedFileAllowed = false;

							string contentType = postedFile.ContentType.GetAllBeforeFirstOccurenceOf(';', false).Trim();
							foreach (string allowedContentType in this.AllowedContentTypes.Split(';', StringSplitOptions.RemoveEmptyEntries).Trim())
							{
								if (contentType.Equals(allowedContentType, StringComparison.OrdinalIgnoreCase) ||
									(!allowedContentType.Contains("/") && contentType.StartsWith(allowedContentType + "/", StringComparison.OrdinalIgnoreCase)))
								{
									postedFileAllowed = true;
									break;
								}
							}

							if (!postedFileAllowed)
							{
								allowed = false;
								break;
							}
						}

						if (!allowed)
						{
							this.IsValid = false;
							this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.FileUpload_ErrorMessage_AllowedContentTypes, this.FriendlyName);
						}
					}
				}
			}
		}

		#endregion
	}
}
