using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Configuration;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// This Class Will Contain an Custom Collection with all required translations
	/// this collection can than be translated in a class of the application using 
	/// this library. 
	/// So the steps in a application would be:
	/// 1.  Supply a control (with possibly more controls for recursive mode)
	/// (2.)Make the WebControlLocalizer Create he custom collection with all info (maybe autmotic when assigned the control)
	/// 3.  From the application manipulatie the Custom Collection exposed by this class
	/// 4.  Run some sort of Apply function on the WebControlLocalizer which will update all controls
	///     with the correct translation. 
	/// This way any kind of translation can be used per application
	/// </summary>
	public class WebControlLocalizer : Control
	{
		#region Fields

		private string _stripTypeNameBy;
		private string _pageName;
		private string _masterPageName;
		private Page _parentPage;
		private WebLocalizationItemCollection _webControlLocalizationItems;
		private bool _runningForPageSpecificControls; // Relevant voor Masterpages
		private bool _runningForPageSpecificControlsForced; // Relevant voor Masterpages
		private bool _datagridHeaderFound = false; // Used when Setting datagrid
		private DataGridItem _datagridHeader = null; // Used when Setting datagrid

		#endregion

		#region Properties

		/// <summary>
		/// PageName (After possible NameSpace stripping done by this control)
		/// </summary>
		public string PageName
		{
			get { return this._pageName; }
		}

		/// <summary>
		/// MasterPageName (After possible NameSpace stripping done by this control)
		/// </summary>
		public string MasterPageName
		{
			get { return _masterPageName; }
		}

		/// <summary>
		/// Collection of all Items for translation
		/// </summary>
		public bool RunningForPageSpecificControlsForced
		{
			get { return _runningForPageSpecificControlsForced; }
			set { _runningForPageSpecificControlsForced = value; }
		}

		/// <summary>
		/// Collection of all Items for translation
		/// </summary>
		public WebLocalizationItemCollection WebControlLocalizationItems
		{
			get { return _webControlLocalizationItems; }
			set { _webControlLocalizationItems = value; }
		}

		/// <summary>
		/// Get/Sets the StripTypeNameBy for striping the identification type of the Page
		/// (Gets filled by WebControlLocalizer.StripTypeNameBy from AppSettings, if available and not custom set)
		/// </summary>
		public string StripTypeNameBy
		{
			get
			{
				if (this._stripTypeNameBy == string.Empty)
				{
					// Get the appSettings.
					AppSettingsReader configReader = new AppSettingsReader();
					this._stripTypeNameBy = configReader.GetValue("WebControlLocalizer.StripTypeNameBy", typeof(string)).ToString();
				}
				return this._stripTypeNameBy;
			}
			set
			{
				this._stripTypeNameBy = value;
			}
		}

		#endregion

		#region Methods

		public WebControlLocalizer(Page page)
		{
			this._parentPage = page;
			this._stripTypeNameBy = string.Empty;
			this._webControlLocalizationItems = new WebLocalizationItemCollection();

			if (this._parentPage == null)
			{
				throw new ApplicationException("The WebControlLocalizer.GetWebLocalizationItemsFromPage() was called and the Page property was Empty");
			}
			else if (this._parentPage.Master == null)
			{
				throw new ApplicationException("The WebControlLocalizer.GetWebLocalizationItemsFromPage() was called and the MasterPage property of Page was Empty");
			}
			else
			{
				// First set the page Name
				this._pageName = this._parentPage.GetType().BaseType.FullName;
				this._pageName = this._pageName.Replace(this.StripTypeNameBy, "");
				this._masterPageName = this._parentPage.Master.GetType().BaseType.FullName;
				this._masterPageName = this._masterPageName.Replace(this.StripTypeNameBy, "");
			}

		}

		/// <summary>
		/// Get all WebLocalizationItems from this controls Page property
		/// (If the page has a MasterPage, only controls in the ContentPlaceHolders will be localized)
		/// </summary>
		public void GetWebLocalizationItemsFromPage()
		{
			// Now run through controls and add them to the WebControlLocalizationItems
			this.GetWebLocalizationItems(this._parentPage);
		}

		/// <summary>
		/// Set all WebLocalizationItems from this controls Page property
		/// (If the page has a MasterPage, only controls in the ContentPlaceHolders will be localized)
		/// </summary>
		public void SetWebLocalizationItemsFromPage()
		{
			if (this._parentPage != null)
			{
				// Now run through controls and add them to the WebControlLocalizationItems
				this.SetWebLocalizationItems(this._parentPage);
			}
			else
			{
				throw new ApplicationException("The WebControlLocalizer.SetWebLocalizationItemsFromPage() was called and the Page property was Empty");
			}
		}

		/// <summary>
		/// Fetch All Enitities into the WebControlLocalizationItems Property           
		/// </summary>
		public void GetWebLocalizationItems(Control parentCtrl)
		{
			#region Find All Generic Controls on the Page & MasterPage
			for (int i = 0; i < parentCtrl.Controls.Count; i++)
			{
				Control childCtrl = parentCtrl.Controls[i];

				// Check if a control has controls it self
				// GK Below  || childCtrl.Controls.Count > 0 has been added because one control had HasControls on false, while it contained controls.
				// It was the problem of the mysettings.aspx page not translating the labels inside the tabpages.
				if (childCtrl.HasControls() || childCtrl.Controls.Count > 0)
				{
					// Check if we're in the masterpage, or its placeholder.
					// Setting below CAN BE OVERRIDDEN VIA this.RunningForPageSpecificControlsForced
					if (childCtrl.GetType() == typeof(System.Web.UI.WebControls.ContentPlaceHolder))
					{
						// Switch ContentPlaceHolder On
						this._runningForPageSpecificControls = true;
						this.GetWebLocalizationItems(childCtrl);
						this._runningForPageSpecificControls = false;
					}
					else
					{
						// Run Regulary
						this.GetWebLocalizationItems(childCtrl);
					}
				}

				System.Type ctrlType = childCtrl.GetType();
				#region Add The Item to the Items
				if (this._parentPage.IsPostBack && !(ctrlType == typeof(DataGrid) || ctrlType == typeof(HtmlTitle)))
				{
					// Do nothing
				}
				if (ctrlType == typeof(Button))
				{
					#region Button
					Button bt = (Button)childCtrl;
					if (bt.LocalizeText)
					{
						this.AddWebLocalizationItem(bt.ID, bt.Text);
						if (bt.PreSubmitWarning.Length > 0)
						{
							this.AddWebLocalizationItem(bt.ID + "_PreSubmitWarning", bt.PreSubmitWarning);
						}
					}

					#endregion
				}
				else if (ctrlType == typeof(CheckBox))
				{
					#region CheckBox
					CheckBox cb = (CheckBox)childCtrl;
					if (cb.LocalizeText)
						this.AddWebLocalizationItem(cb.ID, cb.Text, string.Empty, string.Empty, cb.TranslationTag);
					#endregion
				}
				else if (ctrlType == typeof(RadioButton))
				{
					#region RadioButton
					RadioButton rdb = (RadioButton)childCtrl;
					if (rdb.LocalizeText)
						this.AddWebLocalizationItem(rdb.ID, rdb.Text);
					#endregion
				}
				else if (ctrlType == typeof(DropDownList))
				{
					#region DropDownList
					DropDownList ddl = (DropDownList)childCtrl;
					if (ddl.LocalizeAllItems)
					{
						for (int j = 0; j < ddl.Items.Count; j++)
						{
							this.AddWebLocalizationItem(ddl.ID, ddl.Items[j].Text, ddl.Items[j].Value);
						}
					}

					if (ddl.LocalizeDefaultItem)
					{
						// Gets treated like a .Text property from "normal" controls
						this.AddWebLocalizationItem(ddl.ID, ddl.DefaultListItemText);
					}
					#endregion
				}
				else if (ctrlType == typeof(HyperLink))
				{
					#region HyperLink
					HyperLink hl = (HyperLink)childCtrl;
					if (hl.LocalizeText)
						this.AddWebLocalizationItem(hl.ID, hl.Text);
					#endregion
				}
				else if (ctrlType == typeof(Label))
				{
					#region Label
					Label lbl = (Label)childCtrl;
					if (lbl.LocalizeText)
						this.AddWebLocalizationItem(lbl.ID, lbl.Text, string.Empty, string.Empty, lbl.TranslationTag);
					#endregion
				}
				else if (ctrlType == typeof(TextBox))
				{
					#region TextBox (nothing at the moment)
					// Nothing for now
					#endregion
				}
				//else if (ctrlType == typeof(CollapsablePanelExtended))
				//{
				//    #region CollapsablePanel
				//    CollapsablePanelExtended cpnl = (CollapsablePanelExtended)childCtrl;
				//    if(cpnl.LocalizeTitle)
				//        this.AddWebLocalizationItem(cpnl.ID, cpnl.TitleText);
				//    #endregion
				//}
				else if (ctrlType == typeof(DataGrid))
				{
					#region DataGrid
					DataGrid dg = (DataGrid)childCtrl;
					if (dg.LocalizeHeadings)
					{
						for (int j = 0; j < dg.Columns.Count; j++)
						{
							if (dg.Columns[j].FooterText == "TRANSLATION_TRUE")
							{
								string colFieldName = dg.Columns[j].HeaderText.Replace(" ", "");
								this.AddWebLocalizationItem(dg.ID + "_col_" + colFieldName, dg.Columns[j].HeaderText, string.Empty, string.Empty, "field" + colFieldName);
							}
						}
					}
					#endregion
				}
				else if (ctrlType == typeof(HtmlTitle))
				{
					#region HtmlTitle Always!
					HtmlTitle hTitle = (HtmlTitle)childCtrl;
					this._runningForPageSpecificControls = true;
					this.AddWebLocalizationItem(hTitle.ID, hTitle.Text);
					this._runningForPageSpecificControls = false;
					#endregion
				}
				else
				{
					#region Else?!?!?!?
					#endregion
				}
				#endregion
			}
			#endregion
		}

		/// <summary>
		/// Fetch All Enitities into the WebControlLocalizationItems Property           
		/// </summary>
		public void SetWebLocalizationItems(Control parentCtrl)
		{
			for (int i = 0; i < parentCtrl.Controls.Count; i++)
			{
				Control childCtrl = parentCtrl.Controls[i];
				// Check if a control has controls it self
				if (childCtrl.HasControls())
				{
					// Check if we're in the masterpage, or its placeholder.
					// Setting below CAN BE OVERRIDDEN VIA this.RunningForPageSpecificControlsForced
					if (childCtrl.GetType() == typeof(System.Web.UI.WebControls.ContentPlaceHolder))
					{
						// Switch ContentPlaceHolder On
						this._runningForPageSpecificControls = true;
						this.SetWebLocalizationItems(childCtrl);
						this._runningForPageSpecificControls = false;
					}
					else
					{
						// Run Regulary
						this.SetWebLocalizationItems(childCtrl);
					}
				}

				System.Type ctrlType = childCtrl.GetType();
				WebLocalizationItem wlItem = null;
				#region Add The Item to the Items (on Postback ONLY do DataGrids)
				if (this._parentPage.IsPostBack && !(ctrlType == typeof(DataGrid) || ctrlType == typeof(HtmlTitle)))
				{
					// Do nothing
				}
				else if (ctrlType == typeof(Button))
				{
					#region Button
					Button bt = (Button)childCtrl;
					wlItem = this.FindWebLocalizationItem(bt.ID);
					if (wlItem != null)
					{
						bt.Text = wlItem.LocalizedString;
					}

					wlItem = this.FindWebLocalizationItem(bt.ID + "_PreSubmitWarning");
					if (wlItem != null)
					{
						bt.PreSubmitWarning = wlItem.LocalizedString;
					}
					#endregion
				}
				else if (ctrlType == typeof(CheckBox))
				{
					#region CheckBox
					CheckBox cb = (CheckBox)childCtrl;
					wlItem = this.FindWebLocalizationItem(cb.ID);
					if (wlItem != null)
					{
						cb.Text = wlItem.LocalizedString + wlItem.EndingCharacter;
					}
					#endregion
				}
				else if (ctrlType == typeof(RadioButton))
				{
					#region Radio Button
					RadioButton rdb = (RadioButton)childCtrl;
					wlItem = this.FindWebLocalizationItem(rdb.ID);
					if (wlItem != null)
					{
						rdb.Text = wlItem.LocalizedString;
					}
					#endregion
				}
				else if (ctrlType == typeof(DropDownList))
				{
					#region DropDownList
					DropDownList ddl = (DropDownList)childCtrl;
					if (ddl.LocalizeAllItems)
					{
						for (int j = 0; j < ddl.Items.Count; j++)
						{
							wlItem = this.FindWebLocalizationItem(ddl.ID, ddl.Items[j].Value);
							if (wlItem != null)
							{
								ddl.Items[j].Text = wlItem.LocalizedString;
							}
						}
					}

					if (ddl.LocalizeDefaultItem)
					{
						// Gets treated like a .Text property from "normal" controls
						wlItem = this.FindWebLocalizationItem(ddl.ID);
						if (wlItem != null)
						{
							ddl.DefaultListItemText = wlItem.LocalizedString;
						}
					}
					#endregion
				}
				else if (ctrlType == typeof(HyperLink))
				{
					#region HyperLink
					HyperLink hl = (HyperLink)childCtrl;
					wlItem = this.FindWebLocalizationItem(hl.ID);
					if (wlItem != null)
					{
						hl.Text = wlItem.LocalizedString;
					}
					#endregion
				}
				else if (ctrlType == typeof(Label))
				{
					#region Label
					Label lbl = (Label)childCtrl;
					wlItem = this.FindWebLocalizationItem(lbl.ID);
					if (wlItem != null)
					{
						lbl.Text = wlItem.LocalizedString + wlItem.EndingCharacter;
					}
					#endregion
				}
				//else if (ctrlType == typeof(CollapsablePanelExtended))
				//{
				//    #region CollapsablePanel
				//    CollapsablePanelExtended cpnl = (CollapsablePanelExtended)childCtrl;
				//    wlItem = this.FindWebLocalizationItem(cpnl.ID);
				//    if (wlItem != null)
				//    {
				//        cpnl.TitleText = wlItem.LocalizedString;
				//    }
				//    #endregion
				//}
				else if (ctrlType == typeof(DataGrid))
				{
					#region DataGrid
					DataGrid dg = (DataGrid)childCtrl;
					if (dg.LocalizeHeadings)
					{
						for (int j = 0; j < dg.Columns.Count; j++)
						{
							wlItem = this.FindWebLocalizationItem(dg.ID + "_col_" + dg.Columns[j].HeaderText.Replace(" ", ""));
							this.LoadDataGridHeaderItem(dg);
							if (wlItem != null)
							{
								// GK TODO werkt waarschijnljik niet altijd zo
								this.SetHeaderTableCell(dg, wlItem.OriginalString, wlItem.LocalizedString);
								try
								{
									//this._datagridHeader.Cells[j].Text = wlItem.LocalizedString;
									//((System.Web.UI.WebControls.TableCell)dg.Controls[0].Controls[0].Controls[j]).Text = wlItem.LocalizedString;                                        
								}
								catch { }
							}
						}
					}
					#endregion
				}
				else if (ctrlType == typeof(HtmlTitle))
				{
					#region HtmlTitle
					HtmlTitle hTitle = (HtmlTitle)childCtrl;
					this._runningForPageSpecificControls = true;
					wlItem = this.FindWebLocalizationItem(hTitle.ID);
					this._runningForPageSpecificControls = false;
					if (wlItem != null)
					{
						hTitle.Text = wlItem.LocalizedString;
					}
					#endregion
				}
				else if (ctrlType == typeof(TextBox))
				{
					#region TextBox (nothing at the moment)
					// Nothing for now
					#endregion
				}
				else
				{
					#region Else?!?!?!?
					#endregion
				}
				#endregion
			}
		}

		/// <summary>
		/// Search for the DataGrid header item and put it in field _datagridHeader        
		/// </summary>
		/// <param name="parentCtrl">Control to Search in</param>
		private void LoadDataGridHeaderItem(Control parentCtrl)
		{
			if (!this._datagridHeaderFound)
			{
				for (int i = 0; i < parentCtrl.Controls.Count; i++)
				{
					Control childCtrl = parentCtrl.Controls[i];

					if (childCtrl.HasControls())
					{
						this.LoadDataGridHeaderItem(childCtrl);
					}

					if (childCtrl.GetType().ToString() == "System.Web.UI.WebControls.DataGridItem")
					{
						DataGridItem gridItem = (System.Web.UI.WebControls.DataGridItem)childCtrl;

						if (gridItem.ItemType == ListItemType.Header)
						{
							this._datagridHeader = gridItem;
							this._datagridHeaderFound = true;
						}
					}
				}
			}
		}

		/// <summary>
		/// Search for the DataGrid header item and put it in field _datagridHeader
		/// </summary>
		/// <param name="parentCtrl">Control to Search in</param>
		/// <param name="originalHeaderText">The original header text.</param>
		/// <param name="localizedHeaderText">The localized header text.</param>
		private void SetHeaderTableCell(Control parentCtrl, string originalHeaderText, string localizedHeaderText)
		{
			for (int i = 0; i < parentCtrl.Controls.Count; i++)
			{
				Control childCtrl = parentCtrl.Controls[i];

				if (childCtrl.HasControls())
				{
					this.SetHeaderTableCell(childCtrl, originalHeaderText, localizedHeaderText);
				}

				if (childCtrl.GetType().GetProperty("Text") != null)
				{
					object val = childCtrl.GetType().GetProperty("Text").GetValue(childCtrl, null);
					if (val.ToString() == originalHeaderText)
					{
						childCtrl.GetType().GetProperty("Text").SetValue(childCtrl, localizedHeaderText, null);
					}
				}

			}
		}

		/// <summary>
		/// Find a Item in the WebLocalizationItems. This function automaticly adds the Page/MasterPage name
		/// </summary>
		/// <param name="controlId">Control to Search For</param>
		/// <returns>The Item for the Control or null</returns>
		private WebLocalizationItem FindWebLocalizationItem(string controlId)
		{
			if (this._runningForPageSpecificControls || this._runningForPageSpecificControlsForced)
				return this._webControlLocalizationItems.FindByControlID(this._pageName, controlId);
			else
				return this._webControlLocalizationItems.FindByControlID(this._masterPageName, controlId);
		}

		/// <summary>
		/// Find a Item in the WebLocalizationItems. This function automaticly adds the Page/MasterPage name
		/// </summary>
		/// <param name="controlId">Control to Search For</param>
		/// <param name="optionValue">Options Value of the Item in the Control</param>
		/// <returns>The Item for the Control or null</returns>
		private WebLocalizationItem FindWebLocalizationItem(string controlId, string optionValue)
		{
			// Setting below CAN BE OVERRIDDEN VIA this.RunningForPageSpecificControlsForced
			if (this._runningForPageSpecificControls || this._runningForPageSpecificControlsForced)
				return this._webControlLocalizationItems.FindByControlIDAndValue(this._pageName, controlId, optionValue);
			else
				return this._webControlLocalizationItems.FindByControlIDAndValue(this._masterPageName, controlId, optionValue);
		}

		/// <summary>
		/// Add a WebLocalizationItem to the Collection
		/// And PageName, based on _runningInContentPlaceHolder switch. (Page or MasterPage)
		/// </summary>
		/// <param name="controlId">ID of the control on the Page</param>
		/// <param name="originalString">String to be localized</param>
		/// <param name="objectType">Object Type (UI / DB)</param>
		/// <param name="optionValue">Option Value (DropDownList Items, etc)</param>
		private void AddWebLocalizationItem(string controlId, string originalString, string objectType, string optionValue)
		{
			this.AddWebLocalizationItem(controlId, originalString, objectType, optionValue, string.Empty);
		}

		/// <summary>
		/// Add a WebLocalizationItem to the Collection
		/// And PageName, based on _runningInContentPlaceHolder switch. (Page or MasterPage)
		/// </summary>
		/// <param name="controlId">ID of the control on the Page</param>
		/// <param name="originalString">String to be localized</param>
		/// <param name="objectType">Object Type (UI / DB)</param>
		/// <param name="optionValue">Option Value (DropDownList Items, etc)</param>
		/// <param name="translationTag">TranslationTag</param>
		private void AddWebLocalizationItem(string controlId, string originalString, string objectType, string optionValue, string translationTag)
		{
			WebLocalizationItem item = new WebLocalizationItem();
			item.ObjectType = objectType;
			item.OriginalString = originalString;
			item.OptionValue = optionValue;
			item.TranslationTag = translationTag;

			// Setting below CAN BE OVERRIDDEN VIA this.RunningForPageSpecificControlsForced
			if (this._runningForPageSpecificControls || this._runningForPageSpecificControlsForced)
			{
				item.PageName = this._pageName;
			}
			else
			{
				item.PageName = this._masterPageName;
			}
			item.ControlID = controlId;
			this._webControlLocalizationItems.Add(item);
		}

		/// <summary>
		/// Add a WebLocalizationItem to the Collection with ObjectType "UI"
		/// And PageName, based on _runningInContentPlaceHolder switch. (Page or MasterPage)
		/// </summary>
		/// <param name="controlId">ID of the control on the Page</param>
		/// <param name="originalString">String to be localized</param>
		private void AddWebLocalizationItem(string controlId, string originalString)
		{
			this.AddWebLocalizationItem(controlId, originalString, "UI", string.Empty);
		}

		/// <summary>
		/// Add a WebLocalizationItem to the Collection with ObjectType "UI"
		/// And PageName, based on _runningInContentPlaceHolder switch. (Page or MasterPage)
		/// </summary>
		/// <param name="controlId">ID of the control on the Page</param>
		/// <param name="originalString">String to be localized</param>
		/// <param name="optionValue">Option Value (DropDownList Items, etc)</param>
		private void AddWebLocalizationItem(string controlId, string originalString, string optionValue)
		{
			this.AddWebLocalizationItem(controlId, originalString, "UI", optionValue);
		}

		#endregion
	}

	public class WebLocalizationItem
	{
		#region Fields
		private string _objectType;
		private string _pageName;
		private string _endingCharacter;
		private string _controlID;
		private string _translationTag;
		private string _originalString;
		private string _localizedString;
		private string _optionValue;

		#endregion

		#region Methods
		public WebLocalizationItem()
		{
			this._optionValue = string.Empty;
			this._localizedString = string.Empty;
			this._endingCharacter = string.Empty;
		}
		#endregion

		#region Properties
		/// <summary>
		/// If the TranslationTag is having a NonDigitNonCharacter last character is that character will be set here.
		/// </summary>
		public string EndingCharacter
		{
			get { return this._endingCharacter; }
		}

		/// <summary>
		/// Value of the Option (In DropDownList, RadioButtonList, etc)
		/// </summary>
		public string OptionValue
		{
			get { return this._optionValue; }
			set { this._optionValue = value; }
		}

		/// <summary>
		/// Ojbect Type (DB or UI, Runtime / Design Time)
		/// </summary>
		public string ObjectType
		{
			get { return this._objectType; }
			set { this._objectType = value; }
		}

		/// <summary>
		/// Name of the (Master)Page the WebControl resides in
		/// </summary>
		public string PageName
		{
			get { return this._pageName; }
			set { this._pageName = value; }
		}

		/// <summary>
		/// ID/TranslationTag of the WebControl used for finding the control and assigning it's new value.
		/// 
		/// </summary>
		public string ControlID
		{
			get { return this._controlID; }
			set { this._controlID = value; }
		}

		/// <summary>
		/// TranslationTag of the WebControl used for retrieving translation, if not available ControlId is used
		/// 
		/// </summary>
		public string TranslationTag
		{
			get
			{
				if (this._translationTag == string.Empty)
				{
					return this.ControlID;
				}
				else
				{
					return this._translationTag;
				}
			}
			set
			{
				int nameLength = value.Trim().Length;
				if (nameLength > 3)
				{
					char lastChar = Char.Parse(value.Substring(nameLength - 1));
					if (!Char.IsLetterOrDigit(lastChar))
					{
						this._endingCharacter = lastChar.ToString();
						this._translationTag = value.Substring(0, nameLength - 1);
					}
					else
					{
						this._translationTag = value;
					}
				}
				else
				{
					this._translationTag = value;
				}
			}
		}

		/// <summary>
		/// Original String, the string which should be localized       
		/// </summary>
		public string OriginalString
		{
			get { return this._originalString; }
			set
			{
				this._originalString = value;
				if (this._localizedString == string.Empty)
				{
					this._localizedString = value + " (-)";
				}
			}
		}

		/// <summary>
		/// Localized String, the localized string of the original
		/// (After retrieval) Will append the EndingCharacter if set.
		/// </summary>
		public string LocalizedString
		{
			get { return _localizedString; }
			set
			{
				_localizedString = value;
			}
		}
		#endregion
	}

	public class WebLocalizationItemCollection : CollectionBase
	{
		#region Methods
		public int Add(WebLocalizationItem item)
		{
			return this.List.Add(item);
		}
		public void Insert(int index, WebLocalizationItem item)
		{
			this.List.Insert(index, item);
		}
		public void Remove(WebLocalizationItem item)
		{
			this.List.Remove(item);
		}
		public bool Contains(WebLocalizationItem item)
		{
			return this.List.Contains(item);
		}
		public int IndexOf(WebLocalizationItem item)
		{
			return this.List.IndexOf(item);
		}
		public void CopyTo(WebLocalizationItem[] array, int index)
		{
			this.List.CopyTo(array, index);
		}

		/// <summary>
		/// Find the Item for a Control by its ID
		/// </summary>
		/// <param name="pagename">The pagename.</param>
		/// <param name="controlID">The control ID.</param>
		/// <returns></returns>
		public WebLocalizationItem FindByControlID(string pagename, string controlID)
		{
			WebLocalizationItem item = null;
			for (int i = 0; i < this.List.Count; i++)
			{
				WebLocalizationItem checkItem = (WebLocalizationItem)this.List[i];
				if (checkItem.PageName == pagename && checkItem.ControlID == controlID)
				{
					item = checkItem;
					break;
				}
			}
			return item;
		}

		/// <summary>
		/// Find the Item for a Control + Value (Dropdownlist, RadioButtonList, etc)
		/// </summary>
		/// <param name="pagename">The pagename.</param>
		/// <param name="controlID">Control ID</param>
		/// <param name="value">Option Value</param>
		/// <returns>
		/// Item with details for the Control
		/// </returns>
		public WebLocalizationItem FindByControlIDAndValue(string pagename, string controlID, string value)
		{
			WebLocalizationItem item = null;
			for (int i = 0; i < this.List.Count; i++)
			{
				WebLocalizationItem checkItem = (WebLocalizationItem)this.List[i];
				if (checkItem.PageName == pagename && checkItem.ControlID == controlID && checkItem.OptionValue == value)
				{
					item = checkItem;
					break;
				}
			}
			return item;
		}
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the element at the specified index.
		/// </summary>
		/// <returns>
		/// The element at the specified index.
		/// </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.IList"/>.
		/// </exception>
		/// <exception cref="T:System.NotSupportedException">
		/// The property is set and the <see cref="T:System.Collections.IList"/> is read-only.
		/// </exception>
		public WebLocalizationItem this[int index]
		{
			get { return (WebLocalizationItem)this.List[index]; }
			set { this.List[index] = value; }
		}
		#endregion
	}

}
