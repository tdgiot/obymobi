﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Dionysos implementation of the Image control (generating XHTML image element).
	/// </summary>
	public class Image : System.Web.UI.WebControls.Image
	{
		protected override void Render(HtmlTextWriter writer)
		{
			// Required attributes
			writer.AddAttribute(HtmlTextWriterAttribute.Src, this.ResolveUrl(this.ImageUrl), true);
			writer.AddAttribute(HtmlTextWriterAttribute.Alt, this.AlternateText, true);

			// Optional attributes
			if (!this.Width.IsEmpty)
				writer.AddAttribute(HtmlTextWriterAttribute.Width, this.Width.Value.ToString());
			if (!this.Height.IsEmpty)
				writer.AddAttribute(HtmlTextWriterAttribute.Height, this.Height.Value.ToString());
			if (!String.IsNullOrEmpty(this.DescriptionUrl))
				writer.AddAttribute(HtmlTextWriterAttribute.Longdesc, this.DescriptionUrl, true);

			//Write standard attributes
			if (!String.IsNullOrEmpty(this.CssClass))
				writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CssClass);
			if (!String.IsNullOrEmpty(this.Attributes["usemap"]))
				writer.AddAttribute("usemap", this.Attributes["usemap"]);
			if (this.Style.Count > 0)
				writer.AddAttribute(HtmlTextWriterAttribute.Style, this.Style.Value);
			//if (!String.IsNullOrEmpty(this.ClientID))
			//    writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);

			writer.RenderBeginTag(HtmlTextWriterTag.Img);
			writer.RenderEndTag();
			writer.WriteLine();
		}
	}
}
