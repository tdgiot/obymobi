﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is being used as a ToolBarButton control
    /// </summary>
    public class ToolBarButtonDesigner : System.Web.UI.Design.ControlDesigner
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarButtonDesigner class
        /// </summary>
        public ToolBarButtonDesigner() : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions
        /// </summary>
        /// <returns></returns>
        public override string GetDesignTimeHtml()
        {
            return "[ToolBar]";
        }

        #endregion
    }

    /// <summary>
    /// Gui class which represents a button on a toolbar control
    /// </summary>
    [DesignerAttribute(typeof(ToolBarButtonDesigner), typeof(IDesigner))]
    public class ToolBarButton : CompositeControl
    {
        #region Fields

        private ImageCommandButton imageCommandButton = null;
        private LinkCommandButton linkCommandButton = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarButton class
        /// </summary>
        public ToolBarButton()
        {
            // Set the defaults
            this.CssClass = "toolbarButton";

            // Initialize the child controls
            this.imageCommandButton = new ImageCommandButton();
            this.imageCommandButton.ID = string.Format("imgBtn{0}", this.ID);
            this.imageCommandButton.CssClass = "toolbarButtonImage";
            this.linkCommandButton = new LinkCommandButton();
            this.linkCommandButton.ID = string.Format("lnkBtn{0}", this.ID);
            this.linkCommandButton.CssClass = "toolbarButtonText";
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the child controls of this ImageCommandButton instance
        /// </summary>
        protected override void CreateChildControls()
        {
            if (Instance.Empty(this.Text))
            {
                base.CreateChildControls();
            }
            else
            {
                Table table = new Table();
                table.ID = string.Format("tbl{0}", this.ID);
                table.CellPadding = 0;
                table.CellSpacing = 0;
                table.Style.Add("display", "inline");
                table.Style.Add("vertical-align", "middle");

                TableRow row = new TableRow();
                row.ID = string.Format("tr{0}", this.ID);

                TableCell imgCell = new TableCell();
                imgCell.ID = string.Format("tdImg{0}", this.ID);

                imgCell.Controls.Add(this.imageCommandButton);

                TableCell spaceCell = new TableCell();
                spaceCell.ID = string.Format("tdSpace{0}", this.ID);

                spaceCell.Controls.Add(new LiteralControl("&nbsp;"));

                TableCell txtCell = new TableCell();
                txtCell.ID = string.Format("tdTxt{0}", this.ID);

                txtCell.Controls.Add(this.linkCommandButton);

                row.Cells.Add(imgCell);
                row.Cells.Add(spaceCell);
                row.Cells.Add(txtCell);

                table.Rows.Add(row);

                this.Controls.Add(table);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text of the toolbar button
        /// </summary>
        public string Text
        {
            get
            {
                return this.linkCommandButton.Text;
            }
            set
            {
                this.linkCommandButton.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the url to the image
        /// </summary>
        public string ImageUrl
        {
            get
            {
                return this.imageCommandButton.ImageUrl;
            }
            set
            {
                this.imageCommandButton.ImageUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the command
        /// </summary>
        public string CommandName
        {
            get
            {
                return this.imageCommandButton.CommandName;
            }
            set
            {
                this.imageCommandButton.CommandName = value;
                this.linkCommandButton.CommandName = value;
            }
        }

        /// <summary>
        /// Gets or sets the tooltip
        /// </summary>
        public new string ToolTip
        {
            get
            {
                return this.imageCommandButton.ToolTip;
            }
            set
            {
                this.imageCommandButton.ToolTip = value;
            }
        }

        /// <summary>
        /// Gets or sets the warning which should be displayed before submitting
        /// </summary>
        public string PreSubmitWarning
        {
            get
            {
                return this.imageCommandButton.PreSubmitWarning;
            }
            set
            {
                this.imageCommandButton.PreSubmitWarning = value;
                this.linkCommandButton.PreSubmitWarning = value;
            }
        }


        #endregion
    }
}
