﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is being used as a ToolBarPage control
    /// </summary>
    public class ToolBarPageDesigner : System.Web.UI.Design.ControlDesigner
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarPageDesigner class
        /// </summary>
        public ToolBarPageDesigner() : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions
        /// </summary>
        /// <returns></returns>
        public override string GetDesignTimeHtml()
        {
            return "[ToolBarPage]";
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [DesignerAttribute(typeof(ToolBarPageDesigner), typeof(IDesigner))]
    public class ToolBarPage : CompositeControl
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarPage class
        /// </summary>
        public ToolBarPage()
        {
            this.CssClass = "toolbar";
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            ToolBarButton btnCancel = new ToolBarButton();
            btnCancel.ImageUrl = ResolveUrl("~/Images/Icons/cancel.png");
            btnCancel.CommandName = "Cancel";
            btnCancel.ToolTip = "Cancel";
            btnCancel.Text = "Cancel";
            this.Controls.Add(btnCancel);
        }

        #endregion
    }
}
