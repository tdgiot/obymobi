﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Collection class used for storing Dionysos.Web.UI.WebControls.ToolBarButton instances in
    /// </summary>
    public class ToolBarButtonCollection : ICollection<Dionysos.Web.UI.WebControls.ToolBarButton>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ToolBarButtonCollection class
        /// </summary>	
        public ToolBarButtonCollection()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Dionysos.Web.UI.WebControls.ToolBarButton instance to the collection
        /// </summary>
        /// <param name="toolBarButton">The Dionysos.Web.UI.WebControls.ToolBarButton instance to add to the collection</param>
        public void Add(Dionysos.Web.UI.WebControls.ToolBarButton toolBarButton)
        {
            this.items.Add(toolBarButton);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Dionysos.Web.UI.WebControls.ToolBarButton instance is already in the collection
        /// </summary>
        /// <param name="toolBarButton">The Dionysos.Web.UI.WebControls.ToolBarButton instance to check</param>
        /// <returns>True if the Dionysos.Web.UI.WebControls.ToolBarButton instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Web.UI.WebControls.ToolBarButton toolBarButton)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Web.UI.WebControls.ToolBarButton) == toolBarButton)
                {
                    contains = true;
                    break;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Web.UI.WebControls.ToolBarButton[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Dionysos.Web.UI.WebControls.ToolBarButton instance from this collection
        /// </summary>
        /// <param name="toolBarButton">The Dionysos.Web.UI.WebControls.ToolBarButton instance to remove</param>
        public bool Remove(Dionysos.Web.UI.WebControls.ToolBarButton toolBarButton)
        {
            this.items.Remove(toolBarButton);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.ToolBarButtonCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Web.UI.WebControls.ToolBarButton> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Web.UI.WebControls.ToolBarButton>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.ToolBarButtonCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Dionysos.Web.UI.WebControls.ToolBarButton instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Dionysos.Web.UI.WebControls.ToolBarButton instance to get</param>
        /// <returns>An Dionysos.Web.UI.WebControls.ToolBarButton instance</returns>
        public Dionysos.Web.UI.WebControls.ToolBarButton this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Web.UI.WebControls.ToolBarButton;
            }
        }

        #endregion
    }
}