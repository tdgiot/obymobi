﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is being used as a ToolBar control
    /// </summary>
    public class ToolBarDesigner : System.Web.UI.Design.ControlDesigner
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarDesigner class
        /// </summary>
        public ToolBarDesigner() : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions
        /// </summary>
        /// <returns></returns>
        public override string GetDesignTimeHtml()
        {
            return "[ToolBar]";
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [DesignerAttribute(typeof(ToolBarDesigner), typeof(IDesigner))]
    public class ToolBar : CompositeControl
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBar class
        /// </summary>
        public ToolBar()
        {
            this.CssClass = "toolbar";
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            Control parent = ControlHelper.GetParentUIElement(this);
            if (Instance.Empty(parent))
            {
                parent = this.Page;
            }

            if (parent is PageEntity)
            {
                PageEntity entityPage = this.Page as PageEntity;
                switch (entityPage.PageMode)
                {
                    case PageMode.View:

                        this.Controls.Add(new LiteralControl("&nbsp;"));

                        ToolBarButton btnEdit = new ToolBarButton();
                        btnEdit.ImageUrl = ResolveUrl("~/Images/Icons/ico_edit.gif");
                        btnEdit.CommandName = "Edit";
                        btnEdit.ToolTip = "Wijzigen";
                        btnEdit.Text = "Wijzigen";
                        this.Controls.Add(btnEdit);

                        this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

                        ToolBarButton btnCancel = new ToolBarButton();
                        btnCancel.ImageUrl = ResolveUrl("~/Images/Icons/cancel.png");
                        btnCancel.CommandName = "Cancel";
                        btnCancel.ToolTip = "Cancel";
                        btnCancel.Text = "Cancel";
                        this.Controls.Add(btnCancel);

                        break;

                    default:

                        this.Controls.Add(new LiteralControl("&nbsp;"));

                        ToolBarButton btnSave = new ToolBarButton();
                        btnSave.ImageUrl = ResolveUrl("~/Images/Icons/disk.png");
                        btnSave.CommandName = "Save";
                        btnSave.ToolTip = "Opslaan (blijf op huidige pagina)";
                        btnSave.Text = "Opslaan";
                        this.Controls.Add(btnSave);

                        this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

                        ToolBarButton btnSaveAndGo = new ToolBarButton();
                        btnSaveAndGo.ImageUrl = ResolveUrl("~/Images/Icons/disk_go.png");
                        btnSaveAndGo.CommandName = "SaveAndGo";
                        btnSaveAndGo.ToolTip = "Opslaan en Sluiten";
                        btnSaveAndGo.Text = "Opslaan en Sluiten";
                        this.Controls.Add(btnSaveAndGo);

                        this.Controls.Add(new LiteralControl("&nbsp;"));

                        ToolBarButton btnSaveAndNew = new ToolBarButton();
                        btnSaveAndNew.ImageUrl = ResolveUrl("~/Images/Icons/disk_add.png");
                        btnSaveAndNew.CommandName = "SaveAndNew";
                        btnSaveAndNew.ToolTip = "Opslaan en Nieuw";
                        btnSaveAndNew.Text = "Opslaan en Nieuw";
                        this.Controls.Add(btnSaveAndNew);

                        this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

                        ToolBarButton btnDelete = new ToolBarButton();
                        btnDelete.PreSubmitWarning = "Weet u zeker dat u dit item wilt verwijderen?";
                        btnDelete.ImageUrl = ResolveUrl("~/Images/Icons/delete.png");
                        btnDelete.CommandName = "Delete";
                        btnDelete.ToolTip = "Verwijderen";
                        btnDelete.Text = "Verwijderen";
                        this.Controls.Add(btnDelete);

                        this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

                        ToolBarButton btnCancel2 = new ToolBarButton();
                        btnCancel2.ImageUrl = ResolveUrl("~/Images/Icons/cancel.png");
                        btnCancel2.CommandName = "Cancel";
                        btnCancel2.ToolTip = "Cancel";
                        btnCancel2.Text = "Cancel";
                        this.Controls.Add(btnCancel2);

                        break;
                }
            }
            else if (parent is PageEntityCollection)
            {
                this.Controls.Add(new LiteralControl("&nbsp;"));

                ToolBarButton btnAdd = new ToolBarButton();
                btnAdd.ImageUrl = ResolveUrl("~/Images/Icons/add.png");
                btnAdd.CommandName = "Add";
                btnAdd.ToolTip = "Nieuw";
                btnAdd.Text = "Nieuw";
                this.Controls.Add(btnAdd);

                this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

                ToolBarButton btnCancel = new ToolBarButton();
                btnCancel.ImageUrl = ResolveUrl("~/Images/Icons/cancel.png");
                btnCancel.CommandName = "Cancel";
                btnCancel.ToolTip = "Cancel";
                btnCancel.Text = "Cancel";
                this.Controls.Add(btnCancel);
            }
            else if (parent is SubPanelEntityCollection)
            {
                this.Controls.Add(new LiteralControl("&nbsp;"));

                ToolBarButton btnAdd = new ToolBarButton();
                btnAdd.ImageUrl = ResolveUrl("~/Images/Icons/add.png");
                btnAdd.CommandName = "Add";
                btnAdd.ToolTip = "Nieuw";
                btnAdd.Text = "Nieuw";
                this.Controls.Add(btnAdd);
            }
        }

        #endregion
    }
}

//using System;
//using System.ComponentModel;
//using System.ComponentModel.Design;
//using System.Collections.Generic;
//using System.Text;
//using System.Web.UI;
//using System.Web.UI.WebControls;

//namespace Dionysos.Web.UI.WebControls
//{
//    ///// <summary>
//    ///// Gui class which is being used as a ToolBar control
//    ///// </summary>
//    //public class ToolBarDesigner : System.Web.UI.Design.ControlDesigner
//    //{
//    //    #region Constructors

//    //    /// <summary>
//    //    /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarDesigner class
//    //    /// </summary>
//    //    public ToolBarDesigner() : base()
//    //    {
//    //    }

//    //    #endregion

//    //    #region Methods

//    //    /// <summary>
//    //    /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions
//    //    /// </summary>
//    //    /// <returns></returns>
//    //    public override string GetDesignTimeHtml()
//    //    {
//    //        return "[ToolBar]";
//    //    }

//    //    #endregion
//    //}

//    ///// <summary>
//    ///// 
//    ///// </summary>
//    //[DesignerAttribute(typeof(ToolBarDesigner), typeof(IDesigner))]
//    public class ToolBar : ListControl
//    {
//        #region Fields

//        private ToolBarButtonCollection items = new ToolBarButtonCollection();

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBar class
//        /// </summary>
//        public ToolBar()
//        {
//            this.CssClass = "toolbar";
//        }

//        #endregion

//        #region Methods

//        //protected override void CreateChildControls()
//        //{
//        //    Control parent = ControlHelper.GetParentUIElement(this);
//        //    if (Instance.Empty(parent))
//        //    {
//        //        //throw new EmptyException("Variable 'parent' is empty!");
//        //        parent = this.Page;
//        //    }
//        //    //else
//        //    //{
//        //        if (parent is PageEntity)
//        //        {
//        //            PageEntity entityPage = this.Page as PageEntity;
//        //            switch (entityPage.PageMode)
//        //            {
//        //                case PageMode.View:

//        //                    this.Controls.Add(new LiteralControl("&nbsp;"));

//        //                    ToolBarButton btnEdit = new ToolBarButton();
//        //                    btnEdit.ImageUrl = ResolveUrl("~/Images/Icons/ico_edit.gif");
//        //                    btnEdit.CommandName = "Edit";
//        //                    btnEdit.ToolTip = "Wijzigen";
//        //                    btnEdit.Text = "Wijzigen";
//        //                    this.Controls.Add(btnEdit);

//        //                    this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

//        //                    ToolBarButton btnCancel = new ToolBarButton();
//        //                    btnCancel.ImageUrl = ResolveUrl("~/Images/Icons/cancel.png");
//        //                    btnCancel.CommandName = "Cancel";
//        //                    btnCancel.ToolTip = "Annuleren";
//        //                    btnCancel.Text = "Annuleren";
//        //                    this.Controls.Add(btnCancel);

//        //                    break;

//        //                default:

//        //                    this.Controls.Add(new LiteralControl("&nbsp;"));

//        //                    ToolBarButton btnSave = new ToolBarButton();
//        //                    btnSave.ImageUrl = ResolveUrl("~/Images/Icons/disk.png");
//        //                    btnSave.CommandName = "Save";
//        //                    btnSave.ToolTip = "Opslaan (blijf op huidige pagina)";
//        //                    btnSave.Text = "Opslaan";
//        //                    this.Controls.Add(btnSave);

//        //                    this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

//        //                    ToolBarButton btnSaveAndGo = new ToolBarButton();
//        //                    btnSaveAndGo.ImageUrl = ResolveUrl("~/Images/Icons/disk_go.png");
//        //                    btnSaveAndGo.CommandName = "SaveAndGo";
//        //                    btnSaveAndGo.ToolTip = "Opslaan en Sluiten";
//        //                    btnSaveAndGo.Text = "Opslaan en Sluiten";
//        //                    this.Controls.Add(btnSaveAndGo);

//        //                    this.Controls.Add(new LiteralControl("&nbsp;"));

//        //                    ToolBarButton btnSaveAndNew = new ToolBarButton();
//        //                    btnSaveAndNew.ImageUrl = ResolveUrl("~/Images/Icons/disk_add.png");
//        //                    btnSaveAndNew.CommandName = "SaveAndNew";
//        //                    btnSaveAndNew.ToolTip = "Opslaan en Nieuw";
//        //                    btnSaveAndNew.Text = "Opslaan en Nieuw";
//        //                    this.Controls.Add(btnSaveAndNew);

//        //                    this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

//        //                    ToolBarButton btnDelete = new ToolBarButton();
//        //                    btnDelete.PreSubmitWarning = "Weet u zeker dat u dit item wilt verwijderen?";
//        //                    btnDelete.ImageUrl = ResolveUrl("~/Images/Icons/delete.png");
//        //                    btnDelete.CommandName = "Delete";
//        //                    btnDelete.ToolTip = "Verwijderen";
//        //                    btnDelete.Text = "Verwijderen";
//        //                    this.Controls.Add(btnDelete);

//        //                    this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

//        //                    ToolBarButton btnCancel2 = new ToolBarButton();
//        //                    btnCancel2.ImageUrl = ResolveUrl("~/Images/Icons/cancel.png");
//        //                    btnCancel2.CommandName = "Cancel";
//        //                    btnCancel2.ToolTip = "Annuleren";
//        //                    btnCancel2.Text = "Annuleren";
//        //                    this.Controls.Add(btnCancel2);

//        //                    break;
//        //            }
//        //        }
//        //        else if (parent is PageEntityCollection)
//        //        {
//        //            this.Controls.Add(new LiteralControl("&nbsp;"));

//        //            ToolBarButton btnAdd = new ToolBarButton();
//        //            btnAdd.ImageUrl = ResolveUrl("~/Images/Icons/add.png");
//        //            btnAdd.CommandName = "Add";
//        //            btnAdd.ToolTip = "Nieuw";
//        //            btnAdd.Text = "Nieuw";
//        //            this.Controls.Add(btnAdd);

//        //            this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

//        //            ToolBarButton btnCancel = new ToolBarButton();
//        //            btnCancel.ImageUrl = ResolveUrl("~/Images/Icons/cancel.png");
//        //            btnCancel.CommandName = "Cancel";
//        //            btnCancel.ToolTip = "Annuleren";
//        //            btnCancel.Text = "Annuleren";
//        //            this.Controls.Add(btnCancel);
//        //        }
//        //        else if (parent is SubPanelEntityCollection)
//        //        {
//        //            this.Controls.Add(new LiteralControl("&nbsp;"));

//        //            ToolBarButton btnAdd = new ToolBarButton();
//        //            btnAdd.ImageUrl = ResolveUrl("~/Images/Icons/add.png");
//        //            btnAdd.CommandName = "Add";
//        //            btnAdd.ToolTip = "Nieuw";
//        //            btnAdd.Text = "Nieuw";
//        //            this.Controls.Add(btnAdd);
//        //        }
//        //    //}
//        //}

//        #endregion

//        #region Properties

//        /// <summary>
//        /// Gets or sets the toolbar buttons
//        /// </summary>
//        public new ToolBarButtonCollection Items
//        {
//            get
//            {
//                return this.items;
//            }
//            set
//            {
//                this.items = value;
//            }
//        }

//        #endregion
//    }
//}
