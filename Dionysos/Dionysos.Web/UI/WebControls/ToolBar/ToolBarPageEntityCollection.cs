﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is being used as a ToolBar control on a entity collection page
    /// </summary>
    public class ToolBarPageEntityCollectionDesigner : System.Web.UI.Design.ControlDesigner
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarPageEntityCollectionDesigner class
        /// </summary>
        public ToolBarPageEntityCollectionDesigner() : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions
        /// </summary>
        /// <returns></returns>
        public override string GetDesignTimeHtml()
        {
            return "[ToolBarPageEntityCollection]";
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [DesignerAttribute(typeof(ToolBarPageEntityCollectionDesigner), typeof(IDesigner))]
    public class ToolBarPageEntityCollection : CompositeControl
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarPageEntityCollection class
        /// </summary>
        public ToolBarPageEntityCollection()
        {
            this.CssClass = "toolbar";
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            Control parent = ControlHelper.GetParentUIElement(this);
            if (Instance.Empty(parent))
            {
                parent = this.Page;
            }

            if (parent is PageEntityCollection)
            {
                ToolBarButton btnAdd = new ToolBarButton();
                btnAdd.ImageUrl = ResolveUrl("~/Images/Icons/add.png");
                btnAdd.CommandName = "Add";
                btnAdd.ToolTip = "Nieuw";
                btnAdd.Text = "Nieuw";
                this.Controls.Add(btnAdd);

                this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));

                ToolBarButton btnCancel = new ToolBarButton();
                btnCancel.ImageUrl = ResolveUrl("~/Images/Icons/cancel.png");
                btnCancel.CommandName = "Cancel";
                btnCancel.ToolTip = "Cancel";
                btnCancel.Text = "Cancel";
                this.Controls.Add(btnCancel);
            }
            else
            {
                throw new TechnicalException("A ToolBarPageEntityCollection can only be placed on a page which inherits PageEntityCollection.");
            }
        }

        #endregion
    }
}
