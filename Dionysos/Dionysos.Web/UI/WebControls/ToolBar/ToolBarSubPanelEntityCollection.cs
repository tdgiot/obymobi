﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is being used as a ToolBar control on entity collection subpanels
    /// </summary>
    public class ToolBarSubPanelEntityCollectionDesigner : System.Web.UI.Design.ControlDesigner
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarSubPanelEntityCollectionDesigner class
        /// </summary>
        public ToolBarSubPanelEntityCollectionDesigner() : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions
        /// </summary>
        /// <returns></returns>
        public override string GetDesignTimeHtml()
        {
            return "[ToolBarSubPanelEntityCollection]";
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [DesignerAttribute(typeof(ToolBarSubPanelEntityCollectionDesigner), typeof(IDesigner))]
    public class ToolBarSubPanelEntityCollection : CompositeControl
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.ToolBarSubPanelEntityCollection class
        /// </summary>
        public ToolBarSubPanelEntityCollection()
        {
            this.CssClass = "toolbar";
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            Control parent = ControlHelper.GetParentUIElement(this);
            if (Instance.Empty(parent))
            {
                parent = this.Page;
            }

            if (parent is SubPanelEntityCollection)
            {
                ToolBarButton btnAdd = new ToolBarButton();
                btnAdd.ImageUrl = ResolveUrl("~/Images/Icons/add.png");
                btnAdd.CommandName = "Add";
                btnAdd.ToolTip = "Nieuw";
                btnAdd.Text = "Nieuw";
                this.Controls.Add(btnAdd);
            }
        }

        #endregion
    }
}
