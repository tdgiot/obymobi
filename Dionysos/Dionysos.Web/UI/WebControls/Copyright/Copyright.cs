﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Outputs text used in copyright notes (eg. 2009 - 2010)
	/// </summary>
	[DefaultProperty("InitialYear")]
	[ToolboxData("<{0}:Copyright runat=\"server\" />")]
	public class Copyright : Control
	{
		#region Properties

		/// <summary>
		/// Gets or sets the initial year to use for the copyright text.
		/// </summary>
		/// <value>
		/// The initial year.
		/// </value>
		[Bindable(true)]
		[Category("Appearance")]
		[DefaultValue("")]
		[Localizable(false)]
		public int InitialYear
		{
			get
			{
				return (this.ViewState["InitialYear"] as int?) ?? DateTime.Today.Year;
			}
			set
			{
				this.ViewState["InitialYear"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the display format text, where {0} is the year span, {1} the initial year and {2} the current year.
		/// </summary>
		/// <value>
		/// The display format text.
		/// </value>
		[Bindable(true)]
		[Category("Appearance")]
		[DefaultValue("")]
		[Localizable(false)]
		public string DisplayFormat
		{
			get
			{
				return this.ViewState["DisplayFormat"] as string;
			}
			set
			{
				this.ViewState["DisplayFormat"] = value;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Renders the text.
		/// </summary>
		/// <param name="output">The HtmlTextWriter to write the text output to.</param>
		protected override void Render(HtmlTextWriter output)
		{
			int initialYear = this.InitialYear,
				currentYear = DateTime.Today.Year;

			string yearSpan;
			if (initialYear == currentYear)
			{
				yearSpan = initialYear.ToString();
			}
			else
			{
				yearSpan = initialYear + " - " + currentYear;
			}

			string displayFormat = this.DisplayFormat;
			if (!String.IsNullOrEmpty(displayFormat))
			{
				output.Write(String.Format(displayFormat, yearSpan, initialYear, currentYear));
			}
			else
			{
				output.Write(yearSpan);
			}
		}

		#endregion
	}
}
