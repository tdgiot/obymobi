using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    public enum StatusType
    {
        Idle,
        Running,
        Pausing,
        Paused,
        Stopping
    }
}
