using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

namespace Dionysos.Web.UI.WebControls
{
    public class JobQueueEventArgs : EventArgs
    {
        #region Fields

        private string _queueName;
        private StatusType _status = StatusType.Idle;
        private int _threads;
        private int _threadsRunning;
        private int _jobsCompleted;
        private int _jobsRemaining;
        private TimeSpan _timeCompleted = TimeSpan.Zero;
        private TimeSpan _timeRemaining = TimeSpan.Zero;
        private string _queueMessage;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.JobQueueEventArgs class
        /// </summary>
        /// <param name="queueName"></param>
        public JobQueueEventArgs(string queueName)
        {
            _queueName = queueName;
        }

        #endregion

        #region Properties

        public string QueueName
        {
            get 
            { 
                return _queueName; 
            }
			set
			{
				_queueMessage = value;
			}
        }

        public StatusType Status
        {
            get 
            { 
                return _status; 
            }
            set 
            { 
                _status = value; 
            }
        }

        public int Threads
        {
            get 
            { 
                return _threads; 
            }
            set 
            { 
                _threads = value; 
            }
        }

        public int ThreadsRunning
        {
            get 
            { 
                return _threadsRunning; 
            }
            set 
            { 
                _threadsRunning = value; 
            }
        }

        public int JobsCompleted
        {
            get 
            { 
                return _jobsCompleted; 
            }
            set 
            { 
                _jobsCompleted = value; 
            }
        }

        public int JobsRemaining
        {
            get 
            { 
                return _jobsRemaining; 
            }
            set 
            { 
                _jobsRemaining = value; 
            }
        }

        public TimeSpan TimeCompleted
        {
            get 
            { 
                return _timeCompleted; 
            }
            set
            {
                // MT: Obviously, this can get a little off
                _timeCompleted = value;
                _timeRemaining = new TimeSpan(0, 0, (int)((_timeCompleted.TotalSeconds * _jobsRemaining)
                                                           / _jobsCompleted) / _threadsRunning);
            }
        }

        public TimeSpan TimeRemaining
        {
            get 
            { 
                return _timeRemaining; 
            }
            set 
            { 
                _timeRemaining = value; 
            }
        }

        public string QueueMessage
        {
            get 
            { 
                return _queueMessage; 
            }
            set 
            { 
                _queueMessage = value; 
            }
        }

        public string StatusMessage
        {
            get
            {
                TimeSpan ts = Dionysos.Diagnostics.Debug.GetTimeSpanSinceStartingPoint();
                long mem = Dionysos.Diagnostics.Debug.GetMemoryUseSinceStartingPoint();
                long maxMem = Dionysos.Diagnostics.Debug.MaxMemoryUsageFromResetPoint / (1024*1024);
                string time = string.Format("{0}:{1}:{2}.{3}", ts.Hours, ts.Minutes.ToString("00"), ts.Seconds.ToString("00"), ts.Milliseconds.ToString("000"));
                switch (Status)
                {
                    case StatusType.Idle:
                        return string.Format("{0} is gestopt. ({0}MB (Max. {2}MB) / {1})", QueueName, mem, time, maxMem);
                    case StatusType.Running:
                        return string.Format("{0}. {1} van {2} taken afgerond. ({3}MB / {4}).", QueueName, JobsCompleted, JobsCompleted + JobsRemaining, mem, time);
                    case StatusType.Pausing:
                        return string.Format("{0} wordt gepauzeerd.", QueueName);
                    case StatusType.Paused:
                        return string.Format("{0} is gepauzeerd. {1} of {2} taken afgerond.", QueueName, JobsCompleted, JobsCompleted + JobsRemaining);
                    case StatusType.Stopping:
                        return string.Format("{0} wordt gestopt.", QueueName);
                }
                Debug.Assert(false);
                return "";
            }
        }

        public string TimeRemainingMessage
        {
            get
            {
                //if (Status == StatusType.Idle)
                //    return "";

                //if (TimeRemaining == TimeSpan.Zero)
                //    return "";

                //if (TimeRemaining.TotalMinutes < 1)
                //    return "<1 Minute";

                //return string.Format("Time Remaining: {0:00}:{1:00}:{2:00}", TimeRemaining.Days * 24 + TimeRemaining.Hours, TimeRemaining.Minutes, TimeRemaining.Seconds);
                return "";
            }
        }

        #endregion
    }
}
