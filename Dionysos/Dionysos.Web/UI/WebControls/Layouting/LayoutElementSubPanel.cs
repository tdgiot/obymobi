﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Layouting;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI.WebControls.Layouting
{
	/// <summary>
	/// Class for UserControls for editing TemplateElements
	/// </summary>
	public abstract class LayoutElementSubPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
	{
		#region Fields

		private ILayoutElement layoutElement = null;
		private string parentEntityPrimaryKeyFieldName = String.Empty;
		private ILayoutValue layoutValue = null;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the layout element.
		/// </summary>
		/// <value>
		/// The layout element.
		/// </value>
		public ILayoutElement LayoutElement
		{
			get
			{
				return this.layoutElement;
			}
			set
			{
				this.layoutElement = value;
				this.SetLabelValue();
				this.SetControlValue();
				this.SetControlId();
				this.SetInputControlId();
			}
		}

		/// <summary>
		/// Gets or sets the layout value.
		/// </summary>
		/// <value>
		/// The layout value.
		/// </value>
		public ILayoutValue LayoutValue
		{
			get
			{
				return this.layoutValue;
			}
			set
			{
				this.layoutValue = value;
				this.SetControlValue();
				this.SetControlId();
				this.SetInputControlId();
			}
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public abstract string Value
		{
			get;
			set;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Load" /> event.
		/// </summary>
		/// <param name="e">The <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		/// <exception cref="EmptyException">TemplateElementSubPanel kan niet worden geladen: TemplateElement == null</exception>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (this.LayoutElement == null)
			{
				throw new EmptyException("TemplateElementSubPanel kan niet worden geladen: TemplateElement == null");
			}

			// First try to get the Parent datasource from the page, if not, is has to have been manually set
			if (!this.IsPostBack)
			{
				this.SetLabelValue();

				this.SetControlValue();
				this.SetControlId();

				this.SetInputControlTabIndex((short)((this.LayoutValue.SortOrder + 1) * 1000));
				this.SetInputControlId();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Save the TemplateElement.
		/// </summary>
		/// <returns>
		/// If save was succesful.
		/// </returns>
		/// <exception cref="Dionysos.TechnicalException">Er is geen LayoutValue gezet op de LayoutElementSubPanel</exception>
		public virtual bool Save()
		{
			ILayoutValue layoutValue = this.LayoutValue;
			if (layoutValue == null) throw new TechnicalException("Er is geen LayoutValue gezet op de LayoutElementSubPanel.");
			if (layoutValue.IsNew) throw new TechnicalException("De LayoutValue is nog niet opgeslagen, waarschijnlijk is deze niet goed toegevoegd aan de layout.");

			layoutValue.Value = this.Value;

			return layoutValue.Save();
		}

		/// <summary>
		/// Sets the Label.Text with the FriendlyName of the TemplateElement.
		/// </summary>
		/// <exception cref="System.NotImplementedException">Op de LayoutElementSubPanel dient een Dionysos.Web.UI.WebControl.Label aanwezig te zijn met ID \lblName\, mocht dit niet kunnen is het mogelijk om SetLabelValue te overriden.</exception>
		protected virtual void SetLabelValue()
		{
			Label lbl = this.FindControl("lblName") as Label;
			if (lbl == null)
			{
				throw new NotImplementedException("Op de LayoutElementSubPanel dient een Dionysos.Web.UI.WebControl.Label aanwezig te zijn met ID \"lblName\", mocht dit niet kunnen is het mogelijk om SetLabelValue te overriden.");
			}
			else
			{
				lbl.Text = this.LayoutElement.FriendlyName;
			}
		}

		/// <summary>
		/// Sets the control value.
		/// </summary>
		protected virtual void SetControlValue()
		{
			if (this.LayoutValue != null && this.LayoutValue.Value.Length > 0)
				this.Value = this.LayoutValue.Value;
			else
				this.Value = this.LayoutElement.DefaultValue;
		}

		/// <summary>
		/// Sets the control ID.
		/// </summary>
		protected virtual void SetControlId()
		{
			if (this.LayoutValue != null && this.LayoutValue.SystemName.Length > 0 && !this.LayoutValue.IsNew)
			{
				this.ID = this.LayoutValue.SystemName + this.LayoutValue.InstanceNo.ToString();
			}
			else if (this.LayoutValue != null && this.LayoutValue.SystemName.Length > 0 && this.LayoutValue.IsNew)
			{
				this.ID = this.LayoutValue.SystemName + "0";
			}
			else
			{
				this.ID = this.LayoutElement.SystemName;
			}
		}

		/// <summary>
		/// Sets the input control tab index.
		/// </summary>
		/// <param name="tabIndex">The tab index.</param>
		/// <exception cref="System.NotImplementedException">Op de LayoutElementSubPanel dient een Dionysos.Web.UI.WebControl.TextBox aanwezig te zijn met ID \tbValue\, mocht dit niet kunnen is het mogelijk om SetInputControlTabIndex te overriden.</exception>
		public virtual void SetInputControlTabIndex(short tabIndex)
		{
			TextBox tb = this.FindControl("tbValue") as TextBox;
			if (tb == null)
			{
				throw new NotImplementedException("Op de LayoutElementSubPanel dient een Dionysos.Web.UI.WebControl.TextBox aanwezig te zijn met ID \"tbValue\", mocht dit niet kunnen is het mogelijk om SetInputControlTabIndex te overriden.");
			}
			else
			{
				tb.TabIndex = tabIndex;
			}
		}

		/// <summary>
		/// Sets the input control ID.
		/// </summary>
		public abstract void SetInputControlId();

		#endregion
	}
}
