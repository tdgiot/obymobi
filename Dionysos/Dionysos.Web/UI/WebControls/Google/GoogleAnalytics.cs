﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls.Google
{
    /// <summary>
    /// Emits Google Analytics JavaScript. Be sure to include this control on your site's MasterPage or a base page
    /// that will ensure this JavaScript gets emitted inside the HEAD element on every rendered page. 
    /// </summary>
    [DefaultProperty("AccountId")]
    [ToolboxData("<{0}:GoogleAnalytics runat=server></{0}:GoogleAnalytics>")]
    public class GoogleAnalytics : WebControl
    {
        private static string googleAnalyticsJavaScript = String.Empty;
        private static readonly object LockObject = new object();

        public GoogleAnalytics()
        {
            TrackingId = "";
        }

        /// <summary>
        /// Google Analytics Account Id.
        /// </summary>
        public string TrackingId
        {
            get { return ((string)ViewState["GoogleAnalyticsTrackingId"]); }
            set { ViewState["GoogleAnalyticsTrackingId"] = value; }
        }

        /// <summary>
        /// Renders control's begin HTML tag.
        /// </summary>
        /// <remarks>
        /// Pass-through call to ignore the default SPAN tag rendered by WebControl base class. 
        /// </remarks>
        /// <param name="writer"><see cref="HtmlTextWriter"/></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
        }

        /// <summary>
        /// Renders control's end HTML tag.
        /// </summary>
        /// <remarks>
        /// Pass-through call to ignore the default SPAN tag rendered by WebControl base class.
        /// </remarks>
        /// <param name="writer"><see cref="HtmlTextWriter"/></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
        }

        /// <summary>
        /// Renders control's contents.
        /// </summary>
        /// <param name="output"><see cref="HtmlTextWriter"/></param>
        protected override void RenderContents(HtmlTextWriter output)
        {
            if (TrackingId.Length > 0)
                output.Write(GetGoogleAnalyticsJavaScript());
        }

        /// <summary>
        /// Builds the Google Analytics JavaScript block.
        /// </summary>
        /// <returns>Google Analytics JavaScript block.</returns>
        protected string GetGoogleAnalyticsJavaScript()
        {
            if (googleAnalyticsJavaScript.Length == 0)
            {
                var bldr = new StringBuilder();

                bldr.AppendLine(@"<script>");
                bldr.Append(@"    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){");
                bldr.Append(@"    (i[r].q=i[r].q||[]).push(arguments);},i[r].l=1*new Date();a=s.createElement(o),");
                bldr.Append(@"    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);");
                bldr.Append(@"    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');");
                bldr.Append(@"    ga('create', '" + TrackingId + "', 'crave-emenu.com');");
                bldr.AppendLine(@"    ga('send', 'pageview');");
                bldr.AppendLine(@"</script>");

                lock (LockObject) { googleAnalyticsJavaScript = bldr.ToString(); }
            }

            return googleAnalyticsJavaScript;
        }
    }
}
