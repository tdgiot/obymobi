﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using Dionysos.Web.Security;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is used to implement signon user controls
    /// </summary>
    public class SignOnControl : UserControl
    {
        #region Fields

        private string defaultPageAfterSignOn = "~/Default.aspx";
        private string signOnPageRelativeUrl = "~/SignOn.aspx";
        private string errorMessage = string.Empty;
        private IManagableUser user = null;
        private AuthenticateResult result;

        private System.Web.UI.WebControls.Panel panelLogin = null; 
        private TextBoxString tbLogin = null;
        private TextBoxPassword tbPassword = null;
        private CheckBox cbRememberLogin = null;
        private LinkButton lbLogin = null;
        private Button btLogin = null;
		private Dionysos.Web.UI.DevExControls.Button btLoginDevEx = null;

        private System.Web.UI.WebControls.Panel panelLoggedIn = null;
        private Label lblUserName = null;
        private LinkButton btSignOut = null;       

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.SignOnControl type
        /// </summary>
        public SignOnControl()
        {
        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Check if this is a SignOutAction
            if (QueryStringHelper.HasParameter("SignOut"))
            {
                this.ProcessSignOut();
            }

            this.SetLoginControls();

            if (!this.IsPostBack)
            {
                this.InitializeAuthentication();
            }
            
            this.HookupEvents();
        }

        private void ProcessSignOut()
        {
            FormsAuthentication.SignOut();
            QueryStringHelper qs = new QueryStringHelper();
            qs.AddItem("SignOut", "");
            this.Response.Redirect(qs.MergeQuerystringWithRawUrl(), true);
        }

        private void SetLoginControls()
        {            
            this.panelLogin = ControlHelper.FindControlRecursively(this, "panelLogin") as System.Web.UI.WebControls.Panel;
            this.tbLogin = ControlHelper.FindControlRecursively(this, "tbLogin") as TextBoxString;
            this.tbPassword = ControlHelper.FindControlRecursively(this, "tbPassword") as TextBoxPassword;
            this.cbRememberLogin = ControlHelper.FindControlRecursively(this, "cbRememberLogin") as CheckBox;
            this.lbLogin = ControlHelper.FindControlRecursively(this, "lbLogin") as LinkButton;
            this.btLogin = ControlHelper.FindControlRecursively(this, "btLogin") as Button;
			this.btLoginDevEx = ControlHelper.FindControlRecursively(this, "btLoginDevEx") as Dionysos.Web.UI.DevExControls.Button;

            this.panelLoggedIn = ControlHelper.FindControlRecursively(this, "panelLoggedIn") as System.Web.UI.WebControls.Panel;
            this.lblUserName = ControlHelper.FindControlRecursively(this, "lblUsername") as Label;
            this.btSignOut = ControlHelper.FindControlRecursively(this, "btSignOut") as LinkButton;
        }

        private void InitializeAuthentication()
        {
            if (this.IsAuthenticated)
            {
                this.panelLogin.Visible = false;
                this.panelLoggedIn.Visible = true;
                this.lblUserName.Text = (Dionysos.Reflection.Member.HasMember(UserManager.CurrentUser, UserManager.Instance.FieldMappings.Username) ? Dionysos.Reflection.Member.InvokePropertyAsString(UserManager.CurrentUser, UserManager.Instance.FieldMappings.Username) : UserManager.CurrentUser.ShortDisplayName);
            }
            else
            {
                this.panelLogin.Visible = true;
                this.panelLoggedIn.Visible = false;

                if (CookieHelper.HasParameter(this.userNameCookieId))
                {
                    this.tbLogin.Text = CookieHelper.GetParameterAsString(this.userNameCookieId);
                }
            }
        }

        private void HookupEvents()
        {
            if (!Instance.Empty(this.lbLogin))
                this.lbLogin.Click += new EventHandler(btLogin_Click);
            if (!Instance.Empty(this.btLogin))
                this.btLogin.Click += new EventHandler(btLogin_Click);
			if(!Instance.Empty(this.btLoginDevEx))
				this.btLoginDevEx.Click += new EventHandler(btLogin_Click);
            if (!Instance.Empty(this.btSignOut))
                this.btSignOut.Click += new EventHandler(btSignOut_Click);
        }

        /// <summary>
        /// Signs on the user for the specified username and password
        /// </summary>
        /// <param name="username">The username of the user to sign on</param>
        /// <param name="password">The password of the user to sign on</param>
        public virtual void SignOn(string username, string password)
        {
            // Authenticate user
            this.result = UserManager.Instance.AuthenticateUserAndRetrieve(username, password, out this.user);

            if (this.result == AuthenticateResult.Succes)
            {
                if (this.cbRememberLogin.Checked)
                {
                    this.AddLoginCookie(this.tbLogin.Text);
                }
                else
                {
                    this.Response.Cookies[this.userNameCookieId].Expires = DateTime.Now.AddDays(-10);
                }                
            }

            this.ProcessAuthenticateResult();
            if(this.result == AuthenticateResult.Succes)
                FormsAuthentication.SetAuthCookie(this.User.UserId.ToString(), true);

            this.AfterSignOn();
        }

        /// <summary>
        /// Performs some operations after the user tried to sign on
        /// It also redirects to another page, so code after this method is NEVER executed
        /// </summary>
        public virtual void AfterSignOn()
        {
            this.AfterSignOn(string.Empty);
        }

        /// <summary>
        /// Performs some operations after the user tried to sign on
        /// It also redirects to another page, so code after this method is NEVER executed
        /// </summary>
        /// <param name="redirectUrl">Url to redirect to after signon, if empty default behaviour is used (ReturnUrl / Default.aspx)</param>       
        public virtual void AfterSignOn(string redirectUrl)
        {
            if (!Instance.Empty(this.User))
            {
                // Check whether the user wants to remember the username
                if (redirectUrl.Length > 0)
                {
                    HttpContext.Current.Response.Redirect(redirectUrl, true);
                }
                if (QueryStringHelper.HasParameter("ReturnUrl"))
                {
                    FormsAuthentication.RedirectFromLoginPage(this.User.UserId.ToString(), true);
                }
                else
                {
                    HttpContext.Current.Response.Redirect(this.DefaultPageAfterSignOn, true);
                }
            }
            else
            {
                if (this.ErrorMessage.Length > 0)
                    HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=" + this.ErrorMessage));
                else
                    HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=Het inloggen is mislukt, probeer het opnieuw."));
            }
        }

        /// <summary>
        /// Processes the authentication result. Default behaviour is settings an error message.
        /// Difference behaviour can be achieved by overriding this class in a subclass.
        /// </summary>
        public virtual void ProcessAuthenticateResult()
        {
            this.errorMessage = string.Empty;
            switch (this.result)
            {
                case AuthenticateResult.Succes:
                    break;
                case AuthenticateResult.LoginUnkown:
                    this.errorMessage = "Gebruikersnaam is onbekend";
                    break;
                case AuthenticateResult.PasswordIncorrect:
                    this.errorMessage = "Wachtwoord onjuist.";
                    break;
                case AuthenticateResult.UserIsLockedOut:
                    this.errorMessage = "Gebruiker is geblokkeerd";
                    break;
                case AuthenticateResult.UserIsNotActivated:
                    this.errorMessage = "Gebruiker is niet geactiveerd";
                    break;
                case AuthenticateResult.TechnicalFailure:
                    this.errorMessage = "Technische fout, probeer later opnieuw of neem contact op.";
                    break;
                case AuthenticateResult.UsernameEmpty:
                    this.errorMessage = "Er is geen gebruikersnaam ingegeven.";
                    break;
                case AuthenticateResult.PasswordEmpty:
                    this.errorMessage = "Er is geen wachtwoord ingegeven.";
                    break;
                default:
                    this.errorMessage = "Onbekende fout, probeer later opnieuw of neem contact op.";
                    break;
            }
        }

        /// <summary>
        /// Signs out the current user by redirecting to the signout page
        /// </summary>
        public virtual void SignOut()
        {
            HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?SignOut=true"));
        }

        /// <summary>
        /// Adds a cookie containing the username of the current user
        /// </summary>
        /// <param name="login">The username of the current user</param>
        public void AddLoginCookie(string login)
        {
            CookieHelper.AddCookie(this.userNameCookieId, login, DateTime.Now.AddDays(60));
        }

        #endregion

        #region Event handlers

        private void btLogin_Click(object sender, EventArgs e)
        {
            this.SignOn(this.tbLogin.Text, this.tbPassword.Text);
        }

        private void btSignOut_Click(object sender, EventArgs e)
        {
            this.SignOut();
        }

        #endregion

        #region Properties

        private string userNameCookieId
        {
            get
            {
                return string.Format("{0}-{1}", this.Request.ApplicationPath.Replace("/", string.Empty), "UserName");
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether the current user is authenticated
        /// </summary>
        public bool IsAuthenticated
        {
            get
            {
                return HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }

        /// <summary>
        /// Gets or sets the authenticated user 
        /// </summary>
        public IManagableUser User
        {
            get
            {
                return this.user;
            }
            set
            {
                this.user = value;
            }
        }

        /// <summary>
        /// Gets or sets the PropertyName
        /// </summary>
        public AuthenticateResult Result
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        /// <summary>
        /// Gets or sets the authentication error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        
        /// <summary>
        /// Gets or sets the signOnPageRelativeUrl
        /// </summary>
        public string SignOnPageRelativeUrl
        {
            get
            {
                return this.signOnPageRelativeUrl;
            }
            set
            {
                this.signOnPageRelativeUrl = value;
            }
        }        

        /// <summary>
        /// Gets or sets the defaultPageAfterSignOn
        /// </summary>
        public string DefaultPageAfterSignOn
        {
            get
            {
                return this.defaultPageAfterSignOn;
            }
            set
            {
                this.defaultPageAfterSignOn = value;
            }
        }



        #endregion
    }
}
