﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    public class SignOnView : LoginView
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.SignOnView type
        /// </summary>
        public SignOnView()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the template to display the Web users who are logged in into the Web site
        /// </summary>
        public ITemplate SignedOnTemplate
        {
            get
            {
                return this.LoggedInTemplate;
            }
            set
            {
                this.LoggedInTemplate = value;
            }
        }

        #endregion
    }
}
