﻿using System;
using Dionysos.Data;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Label used for Entity Pages as a label next to a control
	/// </summary>
	public class LabelEntityFieldInfo : LabelAssociated
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="LabelEntityFieldInfo"/> class.
		/// </summary>
		public LabelEntityFieldInfo()
		{
			this.LocalizeText = false;
		}

		#endregion

		#region Methods

		/// <summary>
		/// OnPreRender overwrite arranges the this.Text from the EntityInformation
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		/// <exception cref="Dionysos.TechnicalException">A LabelEntityFieldInfo requires a PageEntity as it's Page, the current page '{0}' is not a PageEntity or a subclass of that</exception>
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			
            if (String.IsNullOrEmpty(EntityName))
            {
                throw new TechnicalException("A LabelEntityFieldInfo requires a PageEntity as it's Page, the current page '{0}' is not a PageEntity or a subclass of that", this.Page.GetType().BaseType.ToString());
            }

			// Get friendly name
			string friendlyName = EntityInformationUtil.GetFieldNameFriendlyName(EntityName, this.fieldName);
			if (!String.IsNullOrEmpty(friendlyName))
			{
				this.Text = friendlyName;
			}
		}

        #endregion
	}
}
