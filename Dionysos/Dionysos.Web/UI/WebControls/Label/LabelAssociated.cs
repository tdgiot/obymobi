﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// A label that automatically connects to a control with the same id when the leadlowercase characters are stripped from the ID
	/// </summary>
	public class LabelAssociated : Label
	{
		#region Fields

		/// <summary>
		/// The field name.
		/// </summary>
		protected string fieldName = String.Empty;

		/// <summary>
		/// Whether this control has an associated control.
		/// </summary>
		private bool hasAssociatedControl = false;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the HTML tag that is used to render the <see cref="T:System.Web.UI.WebControls.Label"/> control.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// The <see cref="T:System.Web.UI.HtmlTextWriterTag"/> value used to render the <see cref="T:System.Web.UI.WebControls.Label"/>.
		/// </returns>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				if (this.hasAssociatedControl)
				{
					return HtmlTextWriterTag.Label;
				}
				else
				{
					return base.TagKey;
				}
			}
		}

		/// <summary>
		/// Gets the Page as PageDefault.
		/// </summary>
		/// <value>The page as page default.</value>
		private PageDefault PageAsPageDefault
		{
			get
			{
				return this.Page as PageDefault;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Raises the System.Web.UI.Control.Load event.
		/// </summary>
		/// <param name="e">The System.EventArgs object that contains the event data.</param>
		protected override void OnLoad(EventArgs e)
		{
			// Set field name
			this.fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);

			base.OnLoad(e);
		}

		/// <summary>
		/// Adds the HTML attributes and styles of a <see cref="T:System.Web.UI.WebControls.Label"/> control to render to the specified output stream.
		/// </summary>
		/// <param name="writer">An <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		/// <exception cref="T:System.Web.HttpException">
		/// The control specified in the <see cref="P:System.Web.UI.WebControls.Label.AssociatedControlID"/> property cannot be found.
		/// </exception>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);

			// Automatic control association
			if (this.PageAsPageDefault == null)
			{
				throw new TechnicalException("Using LabelAssociated requires the Page to inherit PageDefault.");
			}

			if (String.IsNullOrEmpty(this.AssociatedControlID))
			{
				// Get valid associated controls
				List<Control> associatedControls = this.PageAsPageDefault.ControlList.Where(c =>
					!String.IsNullOrEmpty(c.ID) &&
					!c.ID.Equals(this.ID) &&
					(c is ITextControl || c is ICheckBoxControl || c is FileUpload) &&
					this.fieldName.Equals(c.ID.RemoveLeadingLowerCaseCharacters())).ToList();

				// Only associate if exactly one is found
				if (associatedControls.Count == 1)
				{
					// Manually set For attribute
					writer.AddAttribute(HtmlTextWriterAttribute.For, associatedControls[0].ClientID);
					this.hasAssociatedControl = true;
				}
			}
		}

		#endregion
	}
}
