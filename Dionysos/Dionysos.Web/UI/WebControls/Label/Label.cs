using System;
using System.Collections.Generic;
using System.ComponentModel;
using Dionysos.Text.RegularExpressions;
using Dionysos.Globalization;
using System.Web.UI;
using Dionysos.Data;
using Dionysos.Interfaces.Data;
using Dionysos.Web.Extensions;

namespace Dionysos.Web.UI.WebControls
{
	public class Label : System.Web.UI.WebControls.Label, Dionysos.Interfaces.IBindable, ILocalizableControl
	{
		#region Fields

		private bool useDataBinding = false;
		private bool renderTextOnly = false;
        private string valueFormatString = string.Empty;
        private string entityName = string.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Construct an instance of Label
		/// </summary>
		public Label()
		{

		}

		/// <summary>
		/// Construct an instance of Label with and assign values to it's text and ID properties.
		/// </summary>
		/// <param name="id">Value to assigned to the ID property</param>
		/// <param name="text">Value to be assigned tot the Text property</param>        
		public Label(string id, string text)
		{
			this.ID = id;
			this.Text = text;
		}

		#endregion

		#region Properties
		/// <summary>
		/// Enable/Disable Localization for "Text" when the Parent page has localization
		/// </summary>
		[Category("Localization"), DefaultValue(true)]
		public bool LocalizeText
		{
			get
			{
				if (ViewState["LocalizeText"] != null)
					return (bool)ViewState["LocalizeText"];
				else
					return true;
			}
			set { ViewState["LocalizeText"] = value; }
		}

		/// <summary>
		/// Render as Link if Linkable (Default: true)
		/// </summary>
		[Category("Localization"), DefaultValue(true)]
		public bool RenderAsLinkIfLinkable
		{
			get
			{
				if (ViewState["RenderAsLinkIfLinkable"] != null)
				{
					bool result = true;
					// GK HACK Otherwise without try catch problems in Design View
					try
					{
						result = (bool)ViewState["RenderAsLinkIfLinkable"];
					}
					catch
					{ }
					return result;
				}
				else
					return true;
			}
			set { ViewState["RenderAsLinkIfLinkable"] = value; }
		}

		[Category("Localization")]
		public string TranslationTag
		{
			get
			{
				if (ViewState["TranslationTag"] != null)
				{
					return (string)ViewState["TranslationTag"];
				}
				else
				{
					return string.Empty;
				}
			}
			set { ViewState["TranslationTag"] = value; }
		}

		/// <summary>
		/// Set a format string that has to be applied when .Value of this control is assigned with a value
		/// </summary>
		public string ValueFormatString
		{
			get
			{
				return this.valueFormatString;
			}
			set
			{
				this.valueFormatString = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag indicating whether databinding should be used
		/// </summary>
		[Browsable(true)]
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		/// <summary>
		/// Renders the Text only, doesn't add SPAN elements to the label
		/// also prevents rendering as Url/Mailto if set True
		/// </summary>
		[Browsable(true)]
		public bool RenderTextOnly
		{
			get
			{
				return this.renderTextOnly;
			}
			set
			{
				this.renderTextOnly = value;
			}
		}

		/// <summary>
		/// Value of the CheckBox.Checked as boolean
		/// </summary>
		public object Value
		{
			get
			{
				return this.Text;
			}
			set
			{
				if (value is DateTime)
				{
					if (this.ValueFormatString != null)
					{
						this.Text = ((DateTime)value).ToString(this.ValueFormatString);
					}
					else
					{
						this.Text = ((DateTime)value).ToString();
					}
				}
				else
				{
					this.Text = value.ToString();
				}
			}
		}

        /// <summary>
        /// Manually specify the EntityName.
        /// </summary>
		public string EntityName 
        {
            get
            {
                // Get entity name from sub panel
                if (String.IsNullOrEmpty(entityName) && this.GetParentByType<SubPanelLLBLGenEntity>() is SubPanelLLBLGenEntity subPanelLlblGenEntity)
                {
					entityName = subPanelLlblGenEntity.EntityName;
				}

                // Get entity name from page
                if (String.IsNullOrEmpty(entityName) && this.Page is PageEntity pageEntity)
                {
					entityName = pageEntity.EntityName;
				}

				return this.entityName;
            }
            set
            {
                this.entityName = value;
            }
        }

		#endregion

		#region Methods

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if (this.renderTextOnly)
			{
				writer.Write(this.Text);
			}
			else if (!this.RenderAsLinkIfLinkable)
			{
				base.Render(writer);
			}
            else if (!this.EntityName.IsNullOrWhiteSpace())
            {
                base.Render(writer);

                string navigateUrl = GetEntityEditPageLink();
                if (!navigateUrl.IsNullOrWhiteSpace())
                {
                    HyperLink ddLink = new HyperLink();
                    ddLink.NavigateUrl = navigateUrl;
                    ddLink.CssClass = "ddLink";
                    ddLink.Text = "&raquo;";
                    ddLink.Target = "_blank";
                    ddLink.RenderControl(writer);
                }
            }
			else
			{
				if (RegEx.IsEmail(this.Text))
				{
					HyperLink hlEmail = new HyperLink();
					hlEmail.NavigateUrl = "mailto:" + this.Text;
					hlEmail.Text = this.Text;
					hlEmail.Target = "_blank";
					hlEmail.RenderControl(writer);
				}
				else if (RegEx.IsUrl(this.Text))
				{
					HyperLink hlUrl = new HyperLink();
					hlUrl.NavigateUrl = this.Text;
					hlUrl.Text = this.Text;
					hlUrl.Target = "_blank";
					hlUrl.RenderControl(writer);
				}
				else
				{
					base.Render(writer);
				}
			}
		}

        private string GetEntityEditPageLink()
        {
            string propertyName = ID.RemoveLeadingLowerCaseCharacters();

            if (!TryGetEntityInformation(this.EntityName, out IEntityInformation entityInformation))
            {
                return string.Empty;
            }

            if (!entityInformation.ForeignKeyFields.ContainsKey(propertyName))
            {
                return string.Empty;
            }

			string relatedEntityName = entityInformation.ForeignKeyFields[propertyName]?.EntityRelationInformation?.RelatedEntityName;
            if (!TryGetEntityInformation(relatedEntityName, out IEntityInformation relatedEntityInformation))
            {
                return string.Empty;
            }

            if (relatedEntityInformation.DefaultEntityEditPage.IsNullOrWhiteSpace())
            {
                return string.Empty;
			}
			
            object value = GetControlValue(propertyName);
            if (value == null || value.GetType() != typeof(int))
            {
                return string.Empty;
            }

			return $"{ResolveUrl(relatedEntityInformation.DefaultEntityEditPage)}?id={value}";
        }

        private static bool TryGetEntityInformation(string entityName, out IEntityInformation entityInformation)
        {
			try
            {
                entityInformation = EntityInformationUtil.GetEntityInformation(entityName);
			}
            catch (EmptyException)
            {
				entityInformation = null;
            }

            return entityInformation != null;
        }

        private object GetControlValue(string propertyName)
        {
            Control control = null;
            if (Page is PageDefault pageDefault)
            {
                foreach (Control childControl in pageDefault.ControlList)
                {
                    if (childControl.ID != null &&
                        childControl.ID.Contains(propertyName) &&
                        !childControl.ID.Equals(ID))
                    {
                        control = childControl;
                        break;
                    }
                }
            }

            if (control == null)
            {
                return null;
            }

            if (control.TryGetPropertyValue("Value", out object value))
            {
                return value;
            }

            return null;
        }

		#endregion

		#region Event Handlers

		/// <summary>
		/// Helper method to fire up the value changed event
		/// </summary>
		/// <param name="sender">The sender</param>
		/// <param name="e">The EventArgs</param>
		public void OnValueChanged(object sender, EventArgs e)
		{
			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

		/// <summary>
		/// Occurs when the value changed between posts to the server
		/// </summary>
		public event EventHandler ValueChanged;

		#endregion

		#region Localization Logic

		/// <summary>
		/// The localizable properties.
		/// </summary>
		protected readonly Dictionary<string, Translatable> localizableProperties = new Dictionary<string, Translatable>();

		/// <summary>
		/// Gets or sets all information for the localizable properties.
		/// </summary>
		/// <value>
		/// The translatable properties.
		/// </value>
		/// <exception cref="TechnicalException">An TranslationTag or ID is required for controls to be localized, failed for control with Text: '{0}' ({1}).</exception>
		public Dictionary<string, Translatable> TranslatableProperties
		{
			get
			{
				// Text property
				if (this.LocalizeText)
				{
					Translatable translatableText;
					if (!this.localizableProperties.TryGetValue("Text", out translatableText))
					{
						// Get translation key
						string translationKey = this.TranslationTag;
						if (String.IsNullOrEmpty(translationKey) &&
							!String.IsNullOrEmpty(this.ID))
						{
							translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID + ".Text";
						}

						if (String.IsNullOrEmpty(translationKey))
						{
							throw new TechnicalException("An TranslationTag or ID is required for controls to be localized, failed for control with Text: '{0}' ({1}).", this.Text, this.ClientID);
						}

						translatableText = new Translatable();
						translatableText.TranslationKey = translationKey;
					}

					// Set translatable text
					translatableText.TranslationValue = this.Text;
					this.localizableProperties["Text"] = translatableText;
				}

				return this.localizableProperties;
			}
			set
			{
				// Set the translated values
				Translatable translatableText;
				if (value.TryGetValue("Text", out translatableText))
				{
					this.Text = translatableText.TranslationValue;
				}
			}
		}

		#endregion
	}
}
