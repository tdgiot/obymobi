using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.ComponentModel.Design;
using Dionysos.Text.RegularExpressions;

namespace Dionysos.Web.UI.WebControls
{
    public class LabelTextOnly : Dionysos.Web.UI.WebControls.Label
    {
        #region Fields

        #endregion

        #region Constructors

        /// <summary>
        /// Construct an instance of Label
        /// </summary>
        public LabelTextOnly()
        {            
            this.PreRender+=new EventHandler(LabelTextOnly_PreRender);
        }

        #endregion

        #region Properties

        #endregion

        #region Methods

  

        #endregion

        #region Event Handlers

        void  LabelTextOnly_PreRender(object sender, EventArgs e)
        {
 	        base.RenderTextOnly = true;
        }

        #endregion
    }
}
