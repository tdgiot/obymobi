using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
	public class Lookup : System.Web.UI.WebControls.TextBox
	{
		private string scriptFile = "";
		private string callBackFunction = "";
		private string backgroundColor = "#EEE";
		private string highlightColor = "#CCC";
		private string font = "Verdana";
		private string divPadding = "2px";
		private string divBorder = "1px solid #CCC";

		public string ScriptFile
		{
			get { return scriptFile; }
			set { scriptFile = value; }
		}

		public string CallBackFunction
		{
			get { return callBackFunction; }
			set { callBackFunction = value; }
		}

		public string BackgroundColor
		{
			get { return backgroundColor; }
			set { backgroundColor = value; }
		}

		public string HighlightColor
		{
			get { return highlightColor; }
			set { highlightColor = value; }
		}

		public string DivFont
		{
			get { return font; }
			set { font = value; }
		}

		public string DivPadding
		{
			get { return divPadding; }
			set { divPadding = value; }
		}

		public string DivBorder
		{
			get { return divBorder; }
			set { divBorder = value; }
		}

		public Lookup()
		{
			this.Attributes.Add("autocomplete", "off");
		}

		protected override void Render(HtmlTextWriter writer)
		{
			base.Render(writer);

			// bind script that contains almost all logic
			Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadScript", "<script language='JavaScript' src='" + ScriptFile + "'></script>");

			// include UI settings
			string styles = String.Format(
				@"<script language='JavaScript'>
					var DIV_BG_COLOR = '{0}';
					var DIV_HIGHLIGHT_COLOR = '{1}';
					var DIV_FONT = '{2}';
					var DIV_PADDING = '{3}';
					var DIV_BORDER = '{4}';
				</script>",
				BackgroundColor, HighlightColor, DivFont, DivPadding, DivBorder);

			Page.ClientScript.RegisterStartupScript(this.GetType(), "LookupStyles", styles);

			// initialize postback handling 
			Page.ClientScript.RegisterStartupScript(this.GetType(), "RegisterScript", "<script language='JavaScript'>InitQueryCode('" + this.ClientID + "')</script>");

			// set correct calllback function
			Page.ClientScript.RegisterStartupScript(this.GetType(), "RegisterCallBack", @"<script language='JavaScript'>
				
				mainLoop = function() {

					val = escape(queryField.value);
					  
					if(lastVal != val && searching == false){
						
						var response = " + CallBackFunction + @"(val);
						showQueryDiv('smi', response.value); lastVal = val;
					}
					  
					setTimeout('mainLoop()', 100);
					return true;};
				</script>");
		}
	}
}


//using System;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Resources;
//using System.Collections;
//using System.ComponentModel;
//using System.ComponentModel.Design;

//namespace Dionysos.Web.UI.WebControls
//{
//    /// <summary>
//    /// Summary description for TimePicker.
//    /// </summary>
//    [DesignerAttribute(typeof(LookupDesigner), typeof(IDesigner))]
//    public class Lookup : Control, INamingContainer, System.Web.UI.IValidator
//    {
//        #region Fields

//        private bool populateAfterReadOnly = false;
//        private bool isValid = true;
//        private string errorMessage = "";
//        private string friendlyName = "Lookup Control";
//        public bool isRequired = true;
//        public bool useValidation = false;
//        public string TextBoxCssClass = "";
//        public string TextBoxWidth = "";

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Constructs an instance of the Dionysos.Web.UI.WebControls.Lookup type
//        /// </summary>
//        public Lookup()
//        {
//        }

//        #endregion

//        #region Methods

//        protected override void OnInit(EventArgs e) 
//        {
//            base.OnInit(e);
//            Page.Validators.Add(this);
//            this.EnableViewState=true;
//            this.EnsureChildControls();

//            if (ViewState["SelectedText"] == null)
//            {
//                ViewState["SelectedText"] = "";
//                ViewState["SelectedId"] = "-2";
//            }
//        }		

//        protected override void OnUnload(EventArgs e) 
//        {
//            if (Page != null) 
//            {
//                Page.Validators.Remove(this);
//            }
//            base.OnUnload(e);
//        }	

//        public virtual void Validate()
//        {
//            // Check 
//            if (this.useValidation || !this.renderReadOnly)
//            {
//                EnsureChildControls();
//                if (this.isRequired && this.SelectedText.Length <= 0)
//                {
//                    // No blank allowed
//                    this.isValid = false;
//                    this.ErrorMessage = String.Format("'{0}'must contain a value.", this.FriendlyName);
//                }
//                else if (!this.isRequired && this.SelectedText.Length == 0)
//                {
//                    this.GetHiddenField().Value = "-1";
//                }
//                else
//                {
//                    //if (!this.findEntityByText(this.SelectedText))
//                    //{
//                    //    this._valid = false;
//                    //    this.ErrorMessage = String.Format("'{0}' no result could be found for '{1}'.", this.FriendlyName, this.SelectedText);
//                    //}
//                }
//            }
//        }

//        private HiddenField GetHiddenFieldNextControl()
//        {
//            this.EnsureChildControls();
//            HiddenField result = null;
//            result = (HiddenField)this.FindControl("nextControlId");
//            return result;
//        }

//        private TextBox GetTextBox()
//        {
//            this.EnsureChildControls();
//            TextBox result = null;
//            result = (TextBox)this.FindControl("TextBox");
//            return result;
//        }

//        private HiddenField GetHiddenField()
//        {
//            this.EnsureChildControls();
//            HiddenField result = null;
//            result = (HiddenField)this.FindControl("foundId");
//            return result;
//        }

//        private Image GetImage()
//        {
//            Image result = null;
//            foreach (Control ctrl in this.Controls)
//            {

//                // TODO: Optimize, no loop but direct call                    
//                if (ctrl.GetType().ToString() == "System.Web.UI.WebControls.Image")
//                {
//                    result = (Image)ctrl;
//                }
//            }
//            return result;
//        }

//        #endregion

//        #region Properties

//        /// <summary>
//        /// Gets or sets the flag indicating if the control is valid
//        /// </summary>
//        public bool IsValid 
//        {
//            get 
//            { 
//                return isValid; 
//            }
//            set 
//            { 
//                isValid = value; 
//            }
//        }

//        /// <summary>
//        /// Gets or sets the error message after validation
//        /// </summary>
//        public string ErrorMessage 
//        {
//            get 
//            { 
//                return errorMessage; 
//            }
//            set 
//            { 
//                errorMessage = value; 
//            }
//        }

//        /// <summary>
//        /// Gets the TextBox control
//        /// </summary>
//        public TextBox TextBoxField
//        {
//            get
//            {
//                return this.GetTextBox();
//            }
//        }

//        /// <summary>
//        /// After the DIV is closed, focus is set to this control
//        /// </summary>
//        public String NextControlId
//        {
//            set
//            {
//                this.GetHiddenFieldNextControl().Value = value;
//            }
//        }

//        /// <summary>
//        /// Gets or sets the friendly name of the lookup control
//        /// </summary>
//        public string FriendlyName 
//        {
//            get 
//            { 
//                return friendlyName; 
//            }
//            set 
//            { 
//                friendlyName = value; 
//            }
//        }

//        /// <summary>
//        /// Gets or sets the flag indicating whether 
//        /// </summary>
//        public bool IsRequired
//        {
//            get 
//            { 
//                return this.isRequired; 
//            }
//            set 
//            { 
//                this.isRequired = value; 
//            }
//        }

//        /// <summary>
//        /// Don't use validation for the Lookup Control
//        /// </summary>
//        public bool UseValidation
//        {
//            get
//            {
//                object o = ViewState["UseValidation"];
//                if (o != null)
//                {
//                    return (bool)o;
//                }

//                return false;
//            }
//            set
//            {
//                ViewState["UseValidation"] = value;
//            }
//        }

//        /// <summary>
//        /// Javascript to be run after focus is lost
//        /// </summary>
//        public string OnBlurJavaScript
//        {
//            get
//            {
//                object o = ViewState["OnBlurJavaScript"];
//                if (o != null)
//                {
//                    return (string)o;
//                }

//                return "";
//            }
//            set
//            {
//                ViewState["OnBlurJavaScript"] = value;
//            }
//        }

//        /// <summary>
//        /// Don't check for ReadOnly Flag on Page
//        /// </summary>
//        public bool IgnoreReadOnlyRendering
//        {
//            get
//            {
//                object o = ViewState["IgnoreReadOnlyRendering"];
//                if (o != null)
//                {
//                    return (bool)o;
//                }

//                return false;
//            }
//            set
//            {
//                ViewState["IgnoreReadOnlyRendering"] = value;
//            }
//        }


//        private bool renderReadOnly
//        {
//            get
//            {
//                bool readOnly = false;
//                if (this.Page.GetType().GetProperty("RenderReadOnly") != null)
//                {
//                    object val = this.Page.GetType().GetProperty("RenderReadOnly").GetValue(this.Page, null);
//                    if (Convert.ToBoolean(val))
//                    {
//                        readOnly = true;
//                    }
//                }
//                return readOnly;
//            }
//        }

//        [Browsable(false)]
//        public string SelectedText
//        {
//            get
//            {
//                EnsureChildControls();
//                string returnText = "{ERROR}";
//                foreach (Control ctrl in this.Controls)
//                {
//                    // TODO: Optimize, no loop but direct call                    
//                    if (ctrl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
//                    {
//                        returnText = ((TextBox)ctrl).Text;
//                    }
//                }
//                return returnText;
//            }
//            set
//            {
//                EnsureChildControls();
//                ViewState["SelectedText"] = value;
//                foreach (Control ctrl in this.Controls)
//                {
//                    // TODO: Optimize, no loop but direct call
//                    if (ctrl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
//                    {
//                        ((TextBox)ctrl).Text = value.ToString();
//                    }
//                }
//            }
//        }

//        [Browsable(false)]
//        public int SelectedId
//        {
//            get
//            {
//                EnsureChildControls();
//                return Convert.ToInt32(this.GetHiddenField().Value);
//            }
//            set
//            {
//                EnsureChildControls();
//                ViewState["SelectedId"] = value;
//                this.GetHiddenField().Value = value.ToString();
//            }
//        }

//        protected void ValidateCallBackFunction()
//        {
//            // not yet
//        }


//        protected override void CreateChildControls()
//        {
//            // For check if we come from a ReadOnly State
//            if (this.Page.IsPostBack && this.Controls.Count <= 1)
//            {
//                this.populateAfterReadOnly = true;
//            }

//            TextBox tbName = new TextBox();
//            if (this.TextBoxCssClass.Length > 0)
//                tbName.CssClass = this.TextBoxCssClass;
//            else
//                tbName.CssClass = "input";
//            tbName.ID = "TextBox";

//            if (this.TextBoxWidth.Length > 0)
//                tbName.Style.Add("width", this.TextBoxWidth);

//            tbName.Attributes.Add("autocomplete", "off");
//            tbName.Attributes.Add("onFocus", "InitQueryCode('" + this.ClientID + "_" + tbName.ID + "');lastVal = escape(this.value);setMainLoopFor" + this.ClientID + "();");

//            Controls.Add(tbName);

//            HiddenField hdn = new HiddenField();
//            hdn.ID = "foundId";
//            hdn.Value = "-1";
//            Controls.Add(hdn);

//            HiddenField hdn2 = new HiddenField();
//            hdn2.ID = "nextControlId";
//            Controls.Add(hdn2);
//        }

//        protected override void OnPreRender(EventArgs e)
//        {
//            if (this.OnBlurJavaScript.Length > 0)
//                this.GetTextBox().Attributes.Add("onBlur", "hideDiv();" + this.OnBlurJavaScript);
//            else
//                this.GetTextBox().Attributes.Add("onBlur", "hideDiv();");


//        }

//        /// <summary>
//        /// Empty the Text & Id Value
//        /// </summary>
//        public void ClearSelection()
//        {
//            this.SelectedId = -1;
//            this.SelectedText = "";
//            this.GetHiddenField().Value = "-1";
//            this.GetTextBox().Text = "";
//        }

//        #endregion

//        #region AJAX Code		
//        private string scriptFile = "";
//        private string callBackFunction = "";        
//        private string backgroundColor = "#EEE";
//        private string highlightColor = "#CCC";
//        private string font = "Verdana";
//        private string divPadding = "2px";
//        private string divBorder = "1px solid #CCC";

//        public string ScriptFile
//        {
//            get { return scriptFile; }
//            set { scriptFile = value; }
//        }

//        public string CallBackFunction
//        {
//            get { return callBackFunction; }
//            set { callBackFunction = value; }
//        }

//        public string BackgroundColor
//        {
//            get { return backgroundColor; }
//            set { backgroundColor = value; }
//        }

//        public string HighlightColor
//        {
//            get { return highlightColor; }
//            set { highlightColor = value; }
//        }

//        public string DivFont
//        {
//            get { return font; }
//            set { font = value; }
//        }

//        public string DivPadding
//        {
//            get { return divPadding; }
//            set { divPadding = value; }
//        }

//        public string DivBorder
//        {
//            get { return divBorder; }
//            set { divBorder = value; }
//        }

//        protected override void Render(HtmlTextWriter writer)
//        {
//            if (this.renderReadOnly && !this.IgnoreReadOnlyRendering)
//            {
//                if (this.SelectedId <= 0)
//                    writer.Write("[N/A]");
//                else
//                    writer.Write(this.SelectedText);
//            }
//            else
//            {
//                if (this.populateAfterReadOnly)
//                {
//                    this.GetTextBox().Text = ViewState["SelectedText"].ToString();
//                    this.GetHiddenField().Value = ViewState["SelectedId"].ToString();
//                }

//                base.Render(writer);

//                // bind script that contains almost all logic
//                //Page.RegisterStartupScript("LookUpScript", 
//                //	"<script language='JavaScript' src='" + ScriptFile + "'>" + 
//                //	"</script>");

//                // include UI settings
//                string styles = String.Format(
//                    @"<script language='JavaScript'>
//                    var DIV_BG_COLOR = '{0}';
//                    var DIV_HIGHLIGHT_COLOR = '{1}';
//                    var DIV_FONT = '{2}';
//                    var DIV_PADDING = '{3}';
//                    var DIV_BORDER = '{4}';
//                </script>",
//                    BackgroundColor, HighlightColor, DivFont,
//                    DivPadding, DivBorder);

//                this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "LookupStyles", styles);

//                #region Old for 1 control only
////                 //initialize postback handling 
////                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "RegisterScript", "<script language='JavaScript'>InitQueryCode('" + this.ClientID+":_ctl0" + "');lastVal = '"+this.SelectedText+"';</script>");

////                // set correct calllback function
////                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "RegisterCallBack", 
////                    @"<script language='JavaScript'>                
////                    mainLoop = function() {
////
////                        val = escape(queryField.value);
////                      
////                        if(lastVal != val && searching == false){
////                        
////                            var response = " + CallBackFunction + @"(val);
////                            showQueryDiv('smi', response.value); lastVal = val;
////                        }
////                      
////                        setTimeout('mainLoop()', 100);
////                        return true;};
////                    </script>");
//                #endregion

//                this.ValidateCallBackFunction();

//                this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "RegisterCallBack" + this.ClientID,
//                    @"<script language='JavaScript'>                   
//				function setMainLoopFor" + this.ClientID + @"()
//				{   
//					mainLoop = function() 
//                    {
//						val = escape(queryField.value);                        
//						if(lastVal != val && searching == false)
//                        {	                                             
//							var response = " + CallBackFunction + @"(val, callBack_" + this.ClientID + @");							
//						}	                      
//                        else
//                        {
//						    lutimer = setTimeout('mainLoop()', 100);
//                        }
//						return true;
//                    };
//				}
//                function callBack_" + this.ClientID + @"(response)
//                {
//                    showQueryDiv('smi', response.value); lastVal = val;
//                    lutimer = setTimeout('mainLoop()', 100);
//                }
//
//                </script>");
//                /*                 var nextControl" + this.ClientID + @" = '" + this.NextControlID + @"'; */
//            }
//        }
//        #endregion
//    }

//    #region LookupDesigner class

//    public class LookupDesigner : System.Web.UI.Design.ControlDesigner
//    {
//        // Whether to display the html of the associated 
//        // control using a large heading text size.		

//        public LookupDesigner() : base()
//        {
//        }

//        // Returns the html to use to represent the control at design time.
//        public override string GetDesignTimeHtml()
//        {
//            return "[Lookup Control]";
//        }
//    }

//    #endregion
//}