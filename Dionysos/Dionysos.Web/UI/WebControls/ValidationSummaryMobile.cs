using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Custom implementation of the ValidationSummary for the Dionysos Framework
    /// </summary>
	public class ValidationSummaryMobile : ValidationSummary
	{
		public ValidationSummaryMobile()
		{
			this.EnableClientScript = false;
		}
	}
}
