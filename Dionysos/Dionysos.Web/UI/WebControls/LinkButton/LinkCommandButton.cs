﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Dionysos.Reflection;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is being used for link button controls
    /// </summary>
    public class LinkCommandButton : LinkButton
    {
        #region Fields

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.LinkCommandButton class
        /// </summary>
        public LinkCommandButton()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the System.Web.UI.WebControls.Button.Command event from the System.Web.UI.WebControls.Button instance
        /// </summary>
        /// <param name="e">The System.Web.UI.WebControls.CommandEventArgs instance</param>
        protected override void OnCommand(System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (Instance.Empty(this.CommandName))
            {
                // No command name specified
            }
            else
            {
                Control parent = ControlHelper.GetParentUIElement(this);
                if (Instance.Empty(parent))
                {
                    parent = this.Page;
                }
                if (Instance.Empty(parent))
                {
                    throw new EmptyException("Variable 'parent' is empty.");
                }
                else
                {
                    // GK Maybe we can even do with Parent only... more generic
                    object[] parameters;
                    if (!Instance.Empty(e.CommandArgument.ToString()))
                    {
                        parameters = new object[] { e.CommandArgument };
                        if (Member.HasMember(parent, this.CommandName))
                        {
                            Member.InvokeMethod(parent, this.CommandName, parameters);
                        }
                    }
                    else
                    {
                        if (Member.HasMember(parent, this.CommandName))
                        {
                            Member.InvokeMethod(parent, this.CommandName);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
