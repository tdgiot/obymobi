using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Dionysos.Globalization;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Dionysos Implementation of LinkButton
    /// </summary>
    public class LinkButton : System.Web.UI.WebControls.LinkButton, ILocalizableControl
    {
        #region Fields

        private string preSubmitWarning = string.Empty;
        private const string _addClickScript = "addClickFunction('{0}');";

        #endregion

        #region Methods

        /// <summary>
        /// Constructor of ImageButton
        /// </summary>
        public LinkButton()
        {
            // Set the defaults
            this.CausesValidation = false;

            // Hookup the events                        
            this.PreRender += new EventHandler(LinkButton_PreRender);
        }



        protected override void OnLoad(System.EventArgs e)
        {
            string _addClickFunctionScript = @"  function addClickFunction(id) {{
            var b = document.getElementById(id);
            if (b && typeof(b.click) == 'undefined') b.click = function() {{
                var result = true; if (b.onclick) result = b.onclick();
                if (typeof(result) == 'undefined' || result) {{ eval(b.getAttribute('href')); }}
            }}}};";

            this.Page.ClientScript.RegisterStartupScript(GetType(), "addClickFunctionScript", _addClickFunctionScript, true);
            string script = String.Format(_addClickScript, ClientID); Page.ClientScript.RegisterStartupScript(GetType(), "click_" + ClientID, script, true);

            base.OnLoad(e);
        }

        #endregion

        #region Properties
        /// <summary>
        /// Get/Set Pre-Submit warning. Will Show a JavaDialog asking for Yes/No when Clicked
        /// </summary>
        public string PreSubmitWarning
        {
            get
            {
                return this.preSubmitWarning;
            }
            set
            {
                this.preSubmitWarning = value;
            }
        }
        #endregion

        #region EventHandlers

        void LinkButton_PreRender(object sender, EventArgs e)
        {
            if (this.preSubmitWarning != string.Empty)
            {
                this.Attributes.Add("onClick", string.Format("if(!confirm('{0}')){{return false;}}", this.preSubmitWarning));
            }
        }

        #endregion

        #region Localization Logic

        private bool localizePreSubmitWarning = true;
        /// <summary>
        /// Gets / Sets if the control should be localized
        /// </summary>
        [Category("Localization"), DefaultValue(true)]
        public bool LocalizePreSubmitWarning
        {
            get { return this.localizePreSubmitWarning; }
            set { this.localizePreSubmitWarning = value; }
        }

        private bool localizeText = true;
        /// <summary>
        /// Gets / Sets if the control should be localized
        /// </summary>
        [Category("Localization"), DefaultValue(true)]
        public bool LocalizeText
        {
            get { return this.localizeText; }
            set { this.localizeText = value; }
        }

        private string translationTagText = string.Empty;
        /// <summary>
        /// Gets / sets the control's TranslationTag for the Text property, when set this will be used a the TranslationKey instead of the Page's Namespace + Control.ID
        /// 
        /// </summary>
        [Category("Localization"), DefaultValue("")]
        public string TranslationTagText
        {
            get { return this.translationTagText; }
            set { this.translationTagText = value; }
        }

        private string translationTagPreSubmitWarning = string.Empty;
        /// <summary>
        /// Gets / sets the control's TranslationTag for the PreSubmitWarning property, when set this will be used a the TranslationKey instead of the Page's Namespace + Control.ID
        /// 
        /// </summary>
        [Category("Localization"), DefaultValue("")]
        public string TranslationTagPreSubmitWarning
        {
            get { return this.translationTagPreSubmitWarning; }
            set { this.translationTagPreSubmitWarning = value; }
        }

        protected Dictionary<string, Translatable> localizableProperties = null;
        /// <summary>
        /// Gets / sets all information for the localizable properties
        /// </summary>
        public Dictionary<string, Translatable> TranslatableProperties
        {
            get
            {
                if (this.localizableProperties == null)
                {
                    this.localizableProperties = new Dictionary<string, Translatable>();

                    // TranslationKey Prefix
                    string translationKey = string.Empty;
                    if (this.ID != null && this.ID.Length > 0)
                        translationKey = ControlHelper.GetParentNameSpaceForTranslationKey(this) + "." + this.ID;

                    // Text Property
                    if (this.LocalizeText)
                    {
                        Translatable textProperty = new Translatable();
                        textProperty.TranslationValue = this.Text;

                        if (this.TranslationTagText.Length > 0)
                            textProperty.TranslationKey = this.TranslationTagText;
                        else
                        {
                            if (translationKey.Length == 0)
                                throw new Dionysos.TechnicalException("An .ID property or TranslationTag is required for controls to be localized!, failed for control with .Text: '{0}' - {1}", this.Text, this.ClientID);
                            textProperty.TranslationKey = translationKey + ".Text";
                        }

                        this.localizableProperties.Add("Text", textProperty);
                    }

                    // PreSubmitWarning Property
                    if (this.LocalizePreSubmitWarning)
                    {
                        Translatable submitWarningProperty = new Translatable();
                        submitWarningProperty.TranslationValue = this.PreSubmitWarning;

                        if (this.TranslationTagPreSubmitWarning.Length > 0)
                            submitWarningProperty.TranslationKey = this.TranslationTagPreSubmitWarning;
                        else
                        {
                            if (translationKey.Length == 0)
                                throw new Dionysos.TechnicalException("An .ID property or TranslationTag is required for controls to be localized!, failed for control with .Text: '{0}' - {1}", this.Text, this.ClientID);
                            submitWarningProperty.TranslationKey = translationKey + ".PreSubmitWarning";
                        }

                        this.localizableProperties.Add("PreSubmitWarning", submitWarningProperty);
                    }
                }

                return this.localizableProperties;
            }
            set
            {
                this.localizableProperties = value;

                // Set the translated values
                if (this.localizableProperties.ContainsKey("Text"))
                    this.Text = this.localizableProperties["Text"].TranslationValue;
                if (this.localizableProperties.ContainsKey("PreSubmitWarning"))
                {
                    this.PreSubmitWarning = this.localizableProperties["PreSubmitWarning"].TranslationValue;
                    if (this.PreSubmitWarning.Length > 0)
                    {
                        this.Attributes.Remove("onClick");
                        this.Attributes.Add("onClick", string.Format("if(!confirm('{0}')){{return false;}}", this.PreSubmitWarning));
                    }
                }
            }
        }

        #endregion
    }
}
