﻿function intInputValidator(tBox, minValue, maxValue, allowNegative, thousandsSeperators, numberOfDecimals, thousandsSeperator) {

    var startsWithMinus = false;
    var value = tBox.value;
    if (allowNegative) {
        if (value.substring(0, 1) == "-") {
            startsWithMinus = true;
            value = value.substring(1);
        }
    }

    value = value.replace(/\D/g, "");
    regExMatch = new RegExp(/[\d]+/);
    var m = regExMatch.exec(value);
    if (m != null) {
        value = m[0];
        numericValue = value.match(/[\d]+/)[0];
        if (startsWithMinus)
            numericValue = -1 * numericValue;
    }
    else {
        value = "";
    }

    if (startsWithMinus && value.length > 0) {
        value = "-" + value;        
    }

    if (value.length > 0) {
        if (minValue != null && numericValue < minValue) {
            numericValue = minValue;
        }
        if (maxValue != null && numericValue > maxValue) {
            numericValue = maxValue;
        }

        if (thousandsSeperators) {
            tBox.value = numericValue.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/img, '$1' + thousandsSeperator).split('').reverse().join('');
            if (tBox.value.substring(0, 1) == thousandsSeperator)
                tBox.value = tBox.value.substring(1);
        }
    }
    else
        tBox.value = value;    
}