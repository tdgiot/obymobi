﻿function grow(control, minimumRows) {
    control.rows = minimumRows || 1;
    if(control.scrollHeight > control.clientHeight) {
        // Grow till it fits
        while(control.scrollHeight > control.clientHeight) {
            control.rows += 1;
        }
    }
}
function checkTextAreaMaxLength(textBox, event, length) {
    var mLen = textBox["MaxLength"];
    if (null == mLen)
        mLen = length;

    var maxLength = parseInt(mLen);
    if (!checkSpecialKeys(event)) {
        if (textBox.value.length > maxLength - 1) {
            if (window.event) {
                event.returnValue = false;
            } else {
                event.preventDefault();
            }
        }
    }
}

function checkSpecialKeys(e) {
    if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 35 && e.keyCode != 36 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
        return false;
    else
        return true;
}

function prettyPrintJson(el) {
    var ugly = el.value;
    if (!ugly) return;

	var obj = JSON.parse(ugly);
    var pretty = JSON.stringify(obj, undefined, 4);
    el.value = pretty;
    el.scrollTop = el.scrollHeight;
}
