﻿function inputValidator(tBox) {        
    var value = tBox.value;
    var ch;       

    for (var i = 0; i < value.length; i++) {
        ch = value.charCodeAt(i);        

        // http://www.utf8-chartable.de/unicode-utf8-table.pl?start=61440&unicodeinhtml=dec
        if ((ch > 42892 && ch < 44032) ||
            (ch > 55291 && ch < 63744) ||
            (ch > 65528 && ch < 66304) ||
            (ch > 66729 && ch < 68608) || 
            (ch > 27 && ch < 32) || 
            ch == 3)
        {
            value = value.replace(value[i], '');
        }
    }    

    if (value != tBox.value)
    {        
        tBox.value = value;
    }
}