﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    public class TextBoxMultiLineBBCode : TextBoxMultiLine
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.TextBoxMultiLineBBCode
        /// </summary>
        public TextBoxMultiLineBBCode()
        {
        }

        #endregion

        #region Methods

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // Add some JavaScript
            StringBuilder js = new StringBuilder();
            js.Append("function InsertTagAroundSelection(sStartTagToInsert, sEndTagToInsert)\r\n");
            js.Append("{\r\n");
            js.Append("    var tbxMessage = document.getElementById('" + this.ClientID + "');\r\n");
            js.Append("    var sSelectedText;\r\n");
            js.Append("    if (tbxMessage)\r\n");
            js.Append("    {\r\n");
            js.Append("       if (typeof tbxMessage.cursorPos != 'undefined')\r\n");
            js.Append("       {\r\n");
            js.Append("	         var cursorPos = tbxMessage.cursorPos;\r\n");
            js.Append("	         sSelectedText = cursorPos.text;\r\n");
            js.Append("	         cursorPos.text = sStartTagToInsert + sSelectedText + sEndTagToInsert;\r\n");
            js.Append("       }\r\n");
            js.Append("       else\r\n");
            js.Append("       {\r\n");
            js.Append("	         if (typeof tbxMessage.selectionStart != 'undefined')\r\n");
            js.Append("	         {\r\n");
            js.Append("	         // remember scrollposition\r\n");
            js.Append("	         var scrollTop = tbxMessage.scrollTop;\r\n");
            js.Append("	         var sStart = tbxMessage.selectionStart;\r\n");
            js.Append("	         var sEnd = tbxMessage.selectionEnd;\r\n");
            js.Append("	         sSelectedText = tbxMessage.value.substring(sStart, sEnd);\r\n");
            js.Append("	         var newText = sStartTagToInsert + sSelectedText + sEndTagToInsert;\r\n");
            js.Append("	         tbxMessage.value = tbxMessage.value.substr(0, sStart) + newText + tbxMessage.value.substr(sEnd);\r\n");
            js.Append("	         var nStart = sStart == sEnd ? sStart + newText.length : sStart;\r\n");
            js.Append("	         var nEnd = sStart + newText.length;\r\n");
            js.Append("	         tbxMessage.setSelectionRange(nStart, nEnd);\r\n");
            js.Append("	         tbxMessage.scrollTop = scrollTop;\r\n");
            js.Append("	         }\r\n");
            js.Append("	         else\r\n");
            js.Append("	         {\r\n");
            js.Append("	            tbxMessage.value += sStartTagToInsert + sEndTagToInsert;\r\n");
            js.Append("	         }\r\n");
            js.Append("       }\r\n");
            js.Append("       tbxMessage.focus();\r\n");
            js.Append("       if (typeof tbxMessage.cursorPos != 'undefined')\r\n");
            js.Append("       {\r\n");
            js.Append("	         tbxMessage.onselect();\r\n");
            js.Append("       }\r\n");
            js.Append("   }\r\n");
            js.Append("   return false;\r\n");
	        js.Append("}");

            if (!Page.ClientScript.IsClientScriptBlockRegistered("InsertTagAroundSelection"))
                this.Page.ClientScript.RegisterClientScriptBlock(GetType(), "InsertTagAroundSelection", js.ToString(), true);
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);

            writer.Write("<br />");
            writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertTagAroundSelection('[b]','[/b]');\" name=\"btnBold\" >", ResolveUrl("~/Images/Icons/text_bold.png")));
            writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertTagAroundSelection('[i]','[/i]');\" name=\"btnItalic\" >", ResolveUrl("~/Images/Icons/text_italic.png")));
            writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertTagAroundSelection('[s]','[/s]');\" name=\"btnStriked\" >", ResolveUrl("~/Images/Icons/text_strikethrough.png")));
            writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertTagAroundSelection('[u]','[/u]');\" name=\"btnUnderline\" >", ResolveUrl("~/Images/Icons/text_underline.png")));
            writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertTagAroundSelection('[list][*]','[/list]');\" name=\"btnList\" >", ResolveUrl("~/Images/Icons/text_list_bullets.png")));
            writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertTagAroundSelection('[list=1][*]','[/list]');\" name=\"btnListNumbers\" >", ResolveUrl("~/Images/Icons/text_list_numbers.png")));
            writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertTagAroundSelection('[url]','[/url]');\" name=\"btnUrl\" >", ResolveUrl("~/Images/Icons/link.png")));
        }

        #endregion
    }
}
