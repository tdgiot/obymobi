using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Dionysos;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing time values in
    /// </summary>
    public class TextBoxTime : TextBox
    {
        #region Fields

        private string value = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxTime type
        /// </summary>
        public TextBoxTime()
        {
            // Set the defaults
            this.IconUrl = "~/Images/Icons/clock.gif";
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the input of the control (if required)
        /// and checks whether the input is a time value
        /// </summary>
        public override void Validate()
        {
            this.IsValid = true;
            if (this.UseValidation)
            {
                // Check whether input is required
                // and if so, check whether input is entered
                if (this.IsRequired && Instance.Empty(this.Text.Trim()))
                {
                    this.IsValid = false;
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Required, this.FriendlyName);
                }

                // Check whether the input is a time value
                Time temp;
                if (this.IsValid && !Time.TryParse(this.Text, out temp))
                {
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBoxTime_ErrorMessage, this.FriendlyName);
                    this.IsValid = false;
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the System.String time value of the input
        /// </summary>
        public new string Value
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Text))
                {
                    Time temp;
                    if (Time.TryParse(this.Text, out temp))
                    {
                        this.value = this.Text;
                    }
                    else
                    {
                        this.value = string.Empty;
                    }
                }
                return this.value;
            }
            set
            {
                this.value = value;
                if (!Instance.Empty(this.value))
                {
                    base.Value = this.value;
                }
            }
        }

        #endregion
    }
}
