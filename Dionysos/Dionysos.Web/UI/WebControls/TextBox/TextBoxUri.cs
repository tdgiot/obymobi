using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing email addresses in
    /// </summary>
    public class TextBoxUri : TextBox
    {
        #region Fields

        private string value = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxEmail type
        /// </summary>
        public TextBoxUri()
        {
            // Set the defaults            
            this.UseValidation = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the input of the control (if required)
        /// and checks whether the input is a email address
        /// </summary>
        public override void Validate()
        {
            this.IsValid = true;
            if (this.UseValidation)
            {
                // Check whether input is required
                // and if so, check whether input is entered
                if (this.IsRequired && Instance.Empty(this.Text.Trim()))
                {
                    this.IsValid = false;
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Required, this.FriendlyName);
                }

                // Check whether the input is a email address                
                if (this.IsValid && !Instance.Empty(this.Text.Trim()) && !UrlUtil.ValidateUrl(this.Text))
                {
                    this.IsValid = false;
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBoxUri_ErrorMessage, this.FriendlyName);
                }
            }
        }

        #endregion

        #region Properties
        #endregion
    }
}
