using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Text.RegularExpressions;
using System.Globalization;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which represents a TextBox for storing zipcodes in
	/// </summary>
	public class TextBoxZipcode : TextBox
	{
		#region Fields

		private string value = String.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TextBoxZipcode class.
		/// </summary>
		public TextBoxZipcode()
		{
			this.UseValidation = true;

			// Set the MaxLength property
			this.SetMaxLength(this.ValidationRegion);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the MaxLength property based on the specified region. If the region is not supported, sets MaxLength to 0 (not set).
		/// </summary>
		/// <param name="regionInfo">The region to base the length on.</param>
		private void SetMaxLength(RegionInfo regionInfo)
		{
			switch (regionInfo.Name)
			{
				case "NL":
					this.MaxLength = 7;
					break;
				case "BE":
					this.MaxLength = 4;
					break;
				case "FR":
				case "ES":
					this.MaxLength = 5;
					break;
				default:
					this.MaxLength = 0;
					break;
			}
		}

		/// <summary>
		/// Validates the input of the control (if required) and checks whether the input is a zipcode.
		/// </summary>
		public override void Validate()
		{
			this.IsValid = true;

			if (this.UseValidation)
			{
				// Check whether input is required and if so, check whether input is entered
				if (this.IsRequired && StringUtil.IsNullOrWhiteSpace(this.Text))
				{
					this.IsValid = false;
					this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Required, this.FriendlyName);
				}

				// Check whether the input is a zipcode
				bool supported;
				if (this.IsValid && !StringUtil.IsNullOrWhiteSpace(this.Text) && (!RegEx.IsZipcode(this.Text.Trim(), this.ValidationRegion, out supported) && supported))
				{
					this.IsValid = false;
					this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBoxZipcode_ErrorMessage, this.FriendlyName, this.ValidationRegion.NativeName);
				}
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		public new string Value
		{
			get
			{
				if (!RegEx.IsZipcode(base.Value.ToString(), this.ValidationRegion))
				{
					this.value = String.Empty;
				}

				return base.Value.ToString();
			}
			set
			{
				this.value = value;

				if (!String.IsNullOrEmpty(this.value))
				{
					base.Value = this.value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the region to validate against.
		/// </summary>
		public RegionInfo ValidationRegion
		{
			get
			{
				return this.ViewState["ValidationRegion"] as RegionInfo ?? new RegionInfo(CultureInfo.CurrentUICulture.LCID);
			}
			set
			{
				this.ViewState["ValidationRegion"] = value;

				// Update the MaxLength property
				this.SetMaxLength(this.ValidationRegion);
			}
		}

		#endregion
	}
}
