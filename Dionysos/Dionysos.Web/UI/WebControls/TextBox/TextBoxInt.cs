using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Globalization;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which represents a TextBox for storing integer values in
	/// </summary>
	public class TextBoxInt : TextBox
	{
		#region Fields

		private int value = 0;
		private bool allowZero = false;
		private bool allowNegative = true;
		private int minimalValue = -1;
		private int maximalValue = -1;
		private bool filterKeyInputByJavascript = true;
		private bool thousandsSeperators = true;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxInt type
		/// </summary>
		public TextBoxInt()
		{
			this.Load += new EventHandler(TextBoxInt_Load);
		}

		void TextBoxInt_Load(object sender, EventArgs e)
		{
			if (base.IsRequired)
				this.UseValidation = true;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Validates the input of the control (if required)
		/// and checks whether the input is an integer value
		/// </summary>
		public override void Validate()
		{
			this.IsValid = true;
			if (this.UseValidation)
			{
				// Check whether input is required
				// and if so, check whether input is entered
				base.Validate();
				if (this.IsValid)
				{
					// Check whether the input is an integer value
					if (this.IsValid && this.Text.Length > 0 && this.Value == null)
					{
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_NonNummeric, this.FriendlyName);
					}

					// Check whether the input is an integer value
					if (this.IsValid && !this.allowNegative && (this.value < 0))
					{
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Negative, this.FriendlyName);
					}

					// Check whether the input is an integer value
					if (this.IsValid && !this.allowZero && (this.value == 0))
					{
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Zero, this.FriendlyName);
					}

					// Check whether the input is greater than the minimal value
					if (this.IsValid && this.minimalValue != -1 && this.value < this.minimalValue)
					{
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_LowerThanMinimalValue, this.FriendlyName, this.MinimalValue);
					}

					// Check whether the input is smaller than the maximal value
					if (this.IsValid && this.maximalValue != -1 && this.value > this.maximalValue)
					{
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_GreaterThanMaximalValue, this.FriendlyName, this.MaximalValue);
					}
				}
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// Add Javascript if required
			Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "intInputValidator", EmbeddedResourceUtil.UnpackEmbeddedResourceToString("Dionysos.Web.UI.WebControls.TextBox.TextBoxInt.js"), true);
			//Page.ClientScript.RegisterClientScriptResource(typeof(TextBoxInt), "Dionysos.Web.UI.WebControls.TextBox.TextBoxInt.js");

			string parameterMinValue = MinimalValue == -1 ? "null" : MinimalValue.ToString();
			string parameterMaxValue = MaximalValue == -1 ? "null" : MaximalValue.ToString();
			string parameterAllowNegative = this.AllowNegative.ToString().ToLower();

			string functionCall = string.Format("intInputValidator(this, {0}, {1}, {2}, {3}, 0, '{4}');",
							parameterMinValue, parameterMaxValue, parameterAllowNegative, thousandsSeperators.ToString().ToLower(),
							CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator);

			this.Attributes["onkeyup"] = functionCall;

			if (this.Value.HasValue && this.thousandsSeperators)
				this.Text = this.Value.Value.ToString("N0");
		}

		#endregion

		#region Properties

		/// <summary>
		/// Returns a non-nullable INT, returns 0 for null
		/// </summary>
		public int ValueNonNullable
		{
			get
			{
				if (this.Value.HasValue)
					return this.Value.Value;
				else
					return 0;
			}
		}

		/// <summary>
		/// Gets or sets the System.Int32 value of the input
		/// </summary>
		[Browsable(false)]
		public new int? Value
		{
			get
			{
				if (this.Text == String.Empty)
				{
					return null;
				}
				else
				{
					string numberGroupSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;
					string parseValue = String.IsNullOrEmpty(numberGroupSeparator) ? this.Text : this.Text.Replace(numberGroupSeparator, String.Empty);
					if (Int32.TryParse(parseValue, out this.value))
					{
						return this.value;
					}
					else
					{
						return null;
					}
				}
			}
			set
			{
				if (value.HasValue)
				{
					this.value = value.Value;
				}

				if (this.value == 0)
				{
					if (this.allowZero)
					{
						base.Value = this.value;
					}
					else
					{
						base.Value = String.Empty;
					}
				}
				else
				{
					base.Value = this.value;
				}
			}
		}

		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				string numberGroupSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;
				string parseValue = String.IsNullOrEmpty(numberGroupSeparator) ? this.Text : this.Text.Replace(numberGroupSeparator, String.Empty);

				int parsedValue;
				if (this.ThousandsSeperators &&
					Int32.TryParse(parseValue, out parsedValue))
				{
					base.Text = parsedValue.ToString("N0");
				}
				else base.Text = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag which indicated whether zero is allowed
		/// </summary>
		[Browsable(true)]
		public bool AllowZero
		{
			get
			{
				return this.allowZero;
			}
			set
			{
				this.allowZero = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag which indicated whether negative values are allowed
		/// </summary>
		[Browsable(true)]
		public bool AllowNegative
		{
			get
			{
				return this.allowNegative;
			}
			set
			{
				this.allowNegative = value;
			}
		}

		/// <summary>
		/// Gets or sets the minimal value
		/// </summary>
		[Browsable(true)]
		public int MinimalValue
		{
			get
			{
				return this.minimalValue;
			}
			set
			{
				this.minimalValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the PropertyName
		/// </summary>
		[Browsable(true)]
		public int MaximalValue
		{
			get
			{
				return this.maximalValue;
			}
			set
			{
				this.maximalValue = value;
			}
		}

		/// <summary>
		/// Let the textbox input be validated by a javascript to prevent other input then numbers.
		/// </summary>
		[Browsable(true)]
		public bool FilterKeyInputByJavascript
		{
			get
			{
				return this.filterKeyInputByJavascript;
			}
			set
			{
				this.filterKeyInputByJavascript = value;
			}
		}

		/// <summary>
		/// Gets or sets the useThousandsSeperators
		/// </summary>
		public bool ThousandsSeperators
		{
			get
			{
				return this.thousandsSeperators;
			}
			set
			{
				this.thousandsSeperators = value;
			}
		}

		#endregion
	}
}
