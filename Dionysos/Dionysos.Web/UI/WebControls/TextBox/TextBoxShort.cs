using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing short values in
    /// </summary>
    public class TextBoxShort : TextBox
    {
        #region Fields

        private short value = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxShort type
        /// </summary>
        public TextBoxShort()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the input of the control (if required)
        /// and checks whether the input is a short value
        /// </summary>
        public override void Validate()
        {
            this.IsValid = true;
            if (this.UseValidation)
            {
                // Check whether input is required
                // and if so, check whether input is entered
                if (this.IsRequired && Instance.Empty(this.Text.Trim()))
                {
                    this.IsValid = false;
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Required, this.FriendlyName);
                }

                // Check whether the input is a short value
                if (this.IsValid && !Int16.TryParse(this.Text, out this.value))
                {
                    this.IsValid = false;
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_NonNummeric, this.FriendlyName);
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the System.Int16 value of the input
        /// </summary>
        [Browsable(false)]
        public new short Value
        {
            get
            {
                Int16.TryParse(this.Text, out this.value);
                return this.value;
            }
            set
            {
                this.value = value;
                if (!Instance.Empty(this.value))
                {
                    base.Value = this.value;
                }
            }
        }

        #endregion
    }
}
