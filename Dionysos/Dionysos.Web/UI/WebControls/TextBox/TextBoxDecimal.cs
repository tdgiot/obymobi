using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which represents a TextBox for storing decimal values in
	/// </summary>
	public class TextBoxDecimal : TextBox
	{
		#region Fields

		private int amountOfDecimals = 2;
		private bool allowZero = true;
		private bool allowNegative = true;
		private bool allowPeriodAndCommaAsDecimalSeperator = false;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the decimal value.
		/// </summary>
		/// <value>The decimal value.</value>
		[Browsable(false)]
		public new Decimal? Value
		{
			get
			{
				decimal? value;

				if (String.IsNullOrEmpty(this.Text))
				{
					value = null;
				}
				else
				{
					NumberFormatInfo nfi = new NumberFormatInfo()
					{
						NumberDecimalSeparator = "."
					};

					decimal parsedValue;
					if ((this.AllowPeriodAndCommaAsDecimalSeperator && Decimal.TryParse(this.Text.Replace(',', '.'), NumberStyles.Any, nfi, out parsedValue)) ||
						Decimal.TryParse(this.Text, out parsedValue))
					{
						value = parsedValue;
					}
					else
					{
						value = null;
					}
				}

				return value;
			}
			set
			{
				if (value == null)
				{
					this.Text = String.Empty;
				}
				else
				{
					if (this.AmountOfDecimals >= 0)
					{
						this.Text = value.Value.ToString("N" + this.AmountOfDecimals);
					}
					else
					{
						this.Text = value.Value.ToString();
					}
				}

			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether zero is allowed.
		/// </summary>
		/// <value><c>true</c> if zero is allowed; otherwise, <c>false</c>.</value>
		[Browsable(true)]
		public bool AllowZero
		{
			get
			{
				return this.allowZero;
			}
			set
			{
				this.allowZero = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether negative values are allowed.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if negative values are allowed; otherwise, <c>false</c>.
		/// </value>
		public bool AllowNegative
		{
			get
			{
				return this.allowNegative;
			}
			set
			{
				this.allowNegative = value;
			}
		}

		/// <summary>
		/// Gets or sets the amount of decimals.
		/// </summary>
		/// <value>The amount of decimals.</value>
		public int AmountOfDecimals
		{
			get
			{
				return this.amountOfDecimals;
			}
			set
			{
				this.amountOfDecimals = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to allow a period and comma as decimal seperator.
		/// </summary>
		/// <value><c>true</c> if a period and comma are allowed as decimal seperator; otherwise, <c>false</c>.</value>
		public bool AllowPeriodAndCommaAsDecimalSeperator
		{
			get
			{
				return this.allowPeriodAndCommaAsDecimalSeperator;
			}
			set
			{
				this.allowPeriodAndCommaAsDecimalSeperator = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TextBoxDecimal"/> class.
		/// </summary>
		public TextBoxDecimal()
		{
			this.AllowPeriodAndCommaAsDecimalSeperator = ConfigurationManager.GetBool(DionysosWebConfigurationConstants.TextBoxDecimalAllowPeriodAndCommaAsDecimalSeperator, true);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Validates the input of the control (if required).
		/// </summary>
		public override void Validate()
		{
			this.IsValid = true;

			if (this.UseValidation)
			{
				// Check whether input is required and if so, check whether input is entered
				base.Validate();

				if (this.IsValid)
				{
					if (!String.IsNullOrEmpty(this.Text) && this.Value == null)
					{
						// Check whether the input is decimal value
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_NonDecimal, this.FriendlyName);
					}
					else if (!this.AllowZero && this.Value == 0)
					{
						// Check whether the input is not zero
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Zero, this.FriendlyName);
					}
					else if (!this.AllowNegative && this.Value < 0)
					{
						// Check whether the input is an integer value
						this.IsValid = false;
						this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Negative, this.FriendlyName);
					}
				}
			}
		}

		#endregion
	}
}
