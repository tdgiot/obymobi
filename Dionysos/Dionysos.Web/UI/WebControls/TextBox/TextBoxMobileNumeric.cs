using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing string values in
    /// </summary>
    public class TextBoxMobileNumeric: TextBoxMobile		
    {
        #region Fields

		private string value = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxString type
        /// </summary>
		public TextBoxMobileNumeric()
        {
			// 
            //base.WapInputFormat = "*N";
			//this.Attributes.Add("inputmode", "digits");
			this.PreRender += new EventHandler(TextBoxMobileNumeric_PreRender);
        }

		void TextBoxMobileNumeric_PreRender(object sender, EventArgs e)
		{
			this.CssClass += " digitsOnly";
		}

		#endregion

        #region Methods

		/// <summary>
		/// Validates the input of the control (if required)
		/// and checks whether the input is a email address
		/// </summary>
		public override void Validate()
		{
			this.IsValid = true;
			if (this.UseValidation)
			{
				long numberOut;

				// Check whether input is required
				// and if so, check whether input is entered
				if (this.IsRequired && Instance.Empty(this.Text.Trim()))
				{
					this.IsValid = false;
					this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Required, this.FriendlyName);
				}
				// Check whether the input is a mobile number
				else if (this.IsValid && !long.TryParse(this.Text, out numberOut))
				{
					this.IsValid = false;
					this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBoxMobileNumeric_ErrorMessage, this.FriendlyName);
				}
			}
		}

        #endregion

        #region Properties	

		/// <summary>
		/// Gets or sets the mobile number value of the input
		/// </summary>
		[Browsable(false)]
		public new string Value
		{
			get
			{
				if (!string.IsNullOrEmpty(this.Text))
				{
					long numberOut;
					if (long.TryParse(this.Text, out numberOut))
					{
						this.value = this.Text;
					}
					else
					{
						this.value = string.Empty;
					}
				}
				return this.value;
			}
			set
			{
				this.value = value;
				if (!Instance.Empty(this.value))
				{
					long numberOut;
					if (long.TryParse(this.value, out numberOut))
					{
						base.Value = this.value;
					}
				}
			}
		}

        #endregion
    }
}
