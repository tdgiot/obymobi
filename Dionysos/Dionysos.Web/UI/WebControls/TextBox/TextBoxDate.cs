using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing date values in
    /// </summary>
    public class TextBoxDate : TextBox
    {
        #region Fields

        private System.DateTime value = DateTime.MinValue;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.TextBoxDate type
        /// </summary>
        public TextBoxDate()
        {
            // Set the defaults
            this.IconUrl = "~/Images/Icons/calendar.gif";
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the input of the control (if required)
        /// and checks whether the input is a decimal value
        /// </summary>
        public override void Validate()
        {
            this.IsValid = true;
            if (this.UseValidation)
            {
                // Check whether input is required
                // and if so, check whether input is entered
                base.Validate();
                if (this.IsValid)
                {
                    // Check whether the input is a decimal value
                    if (!this.IsRequired && this.Text.Length == 0)
                    {
                        // Nothing to do here.. 
                    }
                    else if (this.IsValid && !System.DateTime.TryParse(this.Text, out this.value))
                    {
                        this.IsValid = false;
                        this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBoxDate_ErrorMessage, this.FriendlyName);
                    }
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the System.DateTime value of the input
        /// </summary>
        [Browsable(false)]
        public new System.DateTime Value
        {
            get
            {
                if (Instance.Empty(this.Text))
                {
                    return DateTime.MinValue;
                }
                else
                {
                    System.DateTime.TryParse(this.Text, out this.value);
                }
                return this.value;
            }
            set
            {
                this.value = value;
                if (!Instance.Empty(this.value))
                {
                    base.Value = this.value.ToShortDateString();
                }
            }
        }

        #endregion
    }
}
