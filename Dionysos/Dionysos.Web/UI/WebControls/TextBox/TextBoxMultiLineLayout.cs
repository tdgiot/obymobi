﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace Dionysos.Web.UI.WebControls
//{
//    public class TextBoxMultiLineLayout : TextBoxMultiLine
//    {
//        #region Constructors

//        /// <summary>
//        /// Constructs an instance of the Dionysos.Web.UI.WebControls.TextBoxMultiLineLayout
//        /// </summary>
//        public TextBoxMultiLineLayout()
//        {
//        }

//        #endregion

//        #region Methods

//        protected override void OnPreRender(EventArgs e)
//        {
//            base.OnPreRender(e);

//            // Add some JavaScript
//            StringBuilder js = new StringBuilder();
//            js.Append("function InsertLayoutElement(sStartTagToInsert)\r\n");
//            js.Append("{\r\n");
//            js.Append("    var sEndTagToInsert = \"<!-- EndLayoutElement -->\";\r\n");
//            js.Append("    var tbxMessage = document.getElementById('" + this.ClientID + "');\r\n");
//            js.Append("    var sSelectedText;\r\n");
//            js.Append("    if (tbxMessage)\r\n");
//            js.Append("    {\r\n");
//            js.Append("       if (typeof tbxMessage.cursorPos != 'undefined')\r\n");
//            js.Append("       {\r\n");
//            js.Append("	         var cursorPos = tbxMessage.cursorPos;\r\n");
//            js.Append("	         sSelectedText = cursorPos.text;\r\n");
//            js.Append("	         cursorPos.text = sStartTagToInsert + sSelectedText + sEndTagToInsert;\r\n");
//            js.Append("       }\r\n");
//            js.Append("       else\r\n");
//            js.Append("       {\r\n");
//            js.Append("	         if (typeof tbxMessage.selectionStart != 'undefined')\r\n");
//            js.Append("	         {\r\n");
//            js.Append("	         // remember scrollposition\r\n");
//            js.Append("	         var scrollTop = tbxMessage.scrollTop;\r\n");
//            js.Append("	         var sStart = tbxMessage.selectionStart;\r\n");
//            js.Append("	         var sEnd = tbxMessage.selectionEnd;\r\n");
//            js.Append("	         sSelectedText = tbxMessage.value.substring(sStart, sEnd);\r\n");
//            js.Append("	         var newText = sStartTagToInsert + sSelectedText + sEndTagToInsert;\r\n");
//            js.Append("	         tbxMessage.value = tbxMessage.value.substr(0, sStart) + newText + tbxMessage.value.substr(sEnd);\r\n");
//            js.Append("	         var nStart = sStart == sEnd ? sStart + newText.length : sStart;\r\n");
//            js.Append("	         var nEnd = sStart + newText.length;\r\n");
//            js.Append("	         tbxMessage.setSelectionRange(nStart, nEnd);\r\n");
//            js.Append("	         tbxMessage.scrollTop = scrollTop;\r\n");
//            js.Append("	         }\r\n");
//            js.Append("	         else\r\n");
//            js.Append("	         {\r\n");
//            js.Append("	            tbxMessage.value += sStartTagToInsert + sEndTagToInsert;\r\n");
//            js.Append("	         }\r\n");
//            js.Append("       }\r\n");
//            js.Append("       tbxMessage.focus();\r\n");
//            js.Append("       if (typeof tbxMessage.cursorPos != 'undefined')\r\n");
//            js.Append("       {\r\n");
//            js.Append("	         tbxMessage.onselect();\r\n");
//            js.Append("       }\r\n");
//            js.Append("   }\r\n");
//            js.Append("   return false;\r\n");
//            js.Append("}");

//            if (!Page.ClientScript.IsClientScriptBlockRegistered("InsertLayoutElement"))
//                this.Page.ClientScript.RegisterClientScriptBlock(GetType(), "InsertLayoutElement", js.ToString(), true);
//        }

//        protected override void Render(System.Web.UI.HtmlTextWriter writer)
//        {
//            base.Render(writer);

//            writer.Write("<br />");
//            writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertLayoutElement({1});\" name=\"btnSingleLine\" >", ResolveUrl("~/Images/Icons/textfield.png"), "<!-- BeginLayoutElement SystemName=\"SingleLine\" DataType=\"SingleLine\" FriendlyName=\"Tekstregel\" SortOrder=\"1000\" -->"));
//            //writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertLayoutElement(\"<!-- BeginLayoutElement SystemName=\"MultiLine\" DataType=\"MultiLine\" FriendlyName=\"Tekstvak\" SortOrder=\"1000\" -->\");\" name=\"btnMultiLine\" >", ResolveUrl("~/Images/Icons/textfield.png")));
//            //writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertLayoutElement(\"<!-- BeginLayoutElement SystemName=\"Image\" DataType=\"Image-100,100\" FriendlyName=\"Afbeelding\" SortOrder=\"1000\" --><img src=\"dummy\" width=\"100\" height=\"100\" />\");\" name=\"btnImage\" >", ResolveUrl("~/Images/Icons/image.png")));
//            //writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertLayoutElement(\"<!-- BeginLayoutElement SystemName=\"Date\" DataType=\"Date\" FriendlyName=\"Datum\" SortOrder=\"1000\" -->\");\" name=\"btnDate\" >", ResolveUrl("~/Images/Icons/date.png")));
//            //writer.Write(string.Format("<img src={0} class=\"bbButton\" onclick=\"InsertLayoutElement(\"<!-- BeginLayoutElement SystemName=\"Table\" DataType=\"Table\" FriendlyName=\"Tabel\" SortOrder=\"1000\" -->\");\" name=\"btnTable\" >", ResolveUrl("~/Images/Icons/table.png")));
//        }

//        #endregion
//    }
//}
