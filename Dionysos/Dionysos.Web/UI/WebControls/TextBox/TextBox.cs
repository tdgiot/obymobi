using System;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Reflection;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which represents a TextBox
	/// </summary>
	public class TextBox : System.Web.UI.WebControls.TextBox, IExtendedValidator, IBindable
	{
		#region Fields

		private const string ScriptKeyTextBoxExtendedKeyDownHandler = "ScriptKeyTextBoxExtendedKeyDownHandler";
		private const string ScriptKeyTextBoxExtendedKeyDownHandler2 = "ScriptKeyTextBoxExtendedKeyDownHandler2";
		private const string ScriptTextBoxExtenderKeyDownNumericHandler = "ScriptTextBoxExtenderKeyDownNumericHandler";
		private const string ScriptTextBoxExtenderDefaultValue = "ScriptTextBoxExtenderDefaultValue";
		private const string ScriptTextBoxExtenderOnChangeHandler = "ScriptTextBoxExtenderOnChangeHandler";
        private const string ScriptTextBoxExtenderOnKeyUpHandler = "ScriptTextBoxExtenderOnKeyUpHandler";

		#region Obsolete fields

		private bool _timeRequired = false;
		private bool _numericRequired = false;
		private bool _numericRequiredPositive = false;
		private bool _preventValidation = false;
		private bool _moneyRequired = false;
		private bool _urlRequired = false;
		private bool _emailAddressRequired = false;
		private int _maxNumericLength = 20;

		#endregion

		private string eventArgumentExtra = string.Empty;
		private bool useValidation = false;
		private bool isValid = true;
		private string errorMessage = string.Empty;
		private bool isRequired = false;
		private string friendlyName = string.Empty;
		private PageMode pageMode = PageMode.None;
		private bool useDataBinding = true;
		private bool renderReadOnlyOverwrite = false;
		private string defaultButton;
		private bool javascriptNumericsEnabled = false;
		private bool renderIcon = false;
		private string iconUrl = string.Empty;
		private bool renderLabel = true;
		private string defaultValue = string.Empty;
		private bool removeDefaultValueOnFocus = true;
		private string emptyText = string.Empty;
		private string onChangeClientFunction = string.Empty;
		private string onChangeClientControlId = string.Empty;
		private bool localizeDefaultValue = false;

		// TODO: Public members?
		public string AmountPlusImageUrl = string.Empty;
		public string AmountMinusImageUrl = string.Empty;
		public bool IndicateChangedValue = false;
		public string IndicateChangedColor = "#D5EBF4";
		public string SaveGroup = string.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Dionysos.Web.UI.WebControls.TextBox type
		/// </summary>
		public TextBox()
		{
			// Hookup the events
			this.HookupEvents();
		}

		#endregion

		#region Properties

		/// <summary>
		/// Indicates whether the text value has been changed.
		/// </summary>
		public bool HasTextChanged { get; private set; } = false;

        #region Obsolete code

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public System.DateTime GetDateTime()
		{
			return System.DateTime.Parse(this.Text);
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public int IntValue
		{
			get
			{
				if (this.Text.Trim().Length == 0)
				{
					return Convert.ToInt32(-1);
				}
				else
				{
					try
					{
						int result;
						if (this.Text.StartsWith("-") || this.Text.StartsWith("-"))
						{
							result = Convert.ToInt32(this.Text);
						}
						else
						{
							result = Convert.ToInt32("0" + this.Text);
						}
						return result;
					}
					catch
					{
						return -1;
					}
				}
			}
			set
			{
				if (value.ToString().Trim().Length <= 0)
				{
					this.Text = "";
				}
				else
				{
					if (value >= 0)
					{
						this.Text = value.ToString();
					}
					else
					{
						this.Text = "";
					}
				}
			}
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public decimal DecimalValue
		{
			get
			{
				if (this.Text.Trim().Length == 0)
				{
					return Convert.ToDecimal(-1);
				}
				else
				{
					try
					{

						decimal result = Convert.ToDecimal("0" + this.Text);
						if (result >= 0)
						{
							return result;
						}
						else
						{
							return Convert.ToDecimal(-1);
						}
					}
					catch
					{
						return Convert.ToDecimal(-1);
					}
				}
			}
			set
			{
				if (value.ToString().Trim().Length <= 0)
				{
					this.Text = "";
				}
				else
				{
					if (value >= 0)
					{
						this.Text = value.ToString();
					}
					else
					{
						this.Text = "";
					}
				}
			}
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public bool RequireTime
		{
			get { return _timeRequired; }
			set { _timeRequired = value; }
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public bool RequireNumeric
		{
			get { return _numericRequired; }
			set { _numericRequired = value; }
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public bool RequireNumericPositive
		{
			get { return _numericRequiredPositive; }
			set { _numericRequiredPositive = value; }
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public int MaxNumericLength
		{
			get { return _maxNumericLength; }
			set { _maxNumericLength = value; }
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public bool RequireEmailAddress
		{
			get { return _emailAddressRequired; }
			set { _emailAddressRequired = value; }
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public bool RequireMoney
		{
			get { return _moneyRequired; }
			set { _moneyRequired = value; }
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public bool AllowBlank
		{
			get
			{
				object o = ViewState[this.ID + "AllowBlank"];
				if (o != null)
				{
					return (bool)o;
				}

				return true;
			}
			set
			{
				ViewState[this.ID + "AllowBlank"] = value;
			}
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public bool RequireUrl
		{
			get { return _urlRequired; }
			set { _urlRequired = value; }
		}

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public bool PreventValidation
		{
			get { return _preventValidation; }
			set { _preventValidation = value; }
		}

		#endregion

		/// <summary>
		/// Gets or sets the value of this TextBox (Replaces TextBox.DefaultValue with string.Empty on get)
		/// </summary>
		[Browsable(false)]
		public virtual object Value
		{
			get
			{
				string toReturn = this.Text;

				// GK Bit of tricky... but required since ToolTip in Obymobi site does some kind of truncate
				if (this.ToolTip.Length > 0 && this.ToolTip.StartsWith(this.Text))
					this.Text = this.Text.Replace(this.ToolTip, string.Empty);

				if (this.defaultValue != string.Empty)
					return this.Text.Replace(this.defaultValue, string.Empty);
				else
					return this.Text;
			}
			set
			{
				this.Text = (value ?? String.Empty).ToString();

				// Fire the value changed event
				//this.OnValueChanged(null, null);
			}
		}

		/// <summary>
		/// Gets or sets the flag indicating whether databinding should be used
		/// </summary>
		[Browsable(true)]
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag if input is required or not
		/// </summary>
		[Browsable(true)]
		public bool IsRequired
		{
			get
			{
				return this.isRequired;
			}
			set
			{
				this.isRequired = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag which determines if validation 
		/// is required for this control
		/// </summary>
		[Browsable(true)]
		public bool UseValidation
		{
			get
			{
				return this.useValidation;
			}
			set
			{
				this.useValidation = value;
			}
		}

		/// <summary>
		/// Indicates whether this control is valid after validation
		/// </summary>
		[Browsable(false)]
		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
			}
		}

		/// <summary>
		/// Gets or sets the error message which is being set after validation
		/// </summary>
		[Browsable(false)]
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		/// <summary>
		/// If true the TextBox will render as a editable TextBox even when page is set as RenderReadOnly (Default: false);
		/// </summary>
		[Browsable(true)]
		public bool RenderReadOnlyOverwrite
		{
			get
			{
				return renderReadOnlyOverwrite;
			}
			set
			{
				renderReadOnlyOverwrite = value;
			}
		}

		/// <summary>
		/// If no value is assigned to this textbox instance, this value will be rendered
		/// </summary>
		[Browsable(true)]
		public string EmptyText
		{
			get
			{
				return this.emptyText;
			}
			set
			{
				this.emptyText = value;
			}
		}

		/// <summary>
		/// If no value is assigned in on the moment of PreRender this value is assigned
		/// This value will also be removed / returned on mouse click (for example: Type search query...)
		/// </summary>
		[Browsable(true)]
		public string DefaultValue
		{
			get
			{
				return this.defaultValue;
			}
			set
			{
				this.defaultValue = value;
			}
		}

		/// <summary>
		/// Determines if the value set in 'DefaultValue' should be removed when the user clicks the control (default is true)
		/// </summary>
		[Browsable(true)]
		public bool RemoveDefaultValueOnFocus
		{
			get
			{
				return removeDefaultValueOnFocus;
			}
			set
			{
				this.removeDefaultValueOnFocus = value;
			}
		}

		/// <summary>
		/// Property for assigning a default button to this textbox. 
		/// When pressing the 'enter' key the default button will be pressed
		/// </summary>
		[Themeable(false), TypeConverter(typeof(ControlIDConverter)), DefaultValue(""), IDReferenceProperty]
		public string DefaultButton
		{
			get
			{
				return defaultButton;
			}
			set
			{
				defaultButton = value;
			}
		}

		/// <summary>
		/// Gets or sets a flag which indicated whether JavaScript numerics are enabled
		/// </summary>
		[Browsable(true)]
		public bool JavascriptNumericsEnabled
		{
			get
			{
				return javascriptNumericsEnabled;
			}
			set
			{
				javascriptNumericsEnabled = value;
			}
		}

		/// <summary>
		/// Gets or sets the friendly name for this control
		/// </summary>
		[Browsable(true)]
		public string FriendlyName
		{
			get
			{
				// Try to find a friendly name if not available, via a corresponding lbl
				if (Instance.Empty(this.friendlyName))
				{
					string strippedId = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
					Label textLabel = this.Parent.FindControl("lbl" + strippedId) as Label;
					if (textLabel != null)
					{
						this.friendlyName = textLabel.Text;
					}
					else
					{
						this.friendlyName = strippedId;
					}
				}
				return this.friendlyName;
			}
			set
			{
				this.friendlyName = value;
			}
		}

		/// <summary>
		/// Gets or sets the page mode for this control
		/// </summary>
		[Browsable(false)]
		public PageMode PageMode
		{
			get
			{
				return this.pageMode;
			}
			set
			{
				this.pageMode = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag if an icon should be rendered
		/// </summary>
		[Browsable(true)]
		public bool RenderIcon
		{
			get
			{
				return this.renderIcon;
			}
			set
			{
				this.renderIcon = value;
			}
		}

		/// <summary>
		/// Gets or sets the url to the icon
		/// </summary>
		public string IconUrl
		{
			get
			{
				return this.iconUrl;
			}
			set
			{
				this.iconUrl = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag if a label should be rendered even if the page is readonly (for example login controls)
		/// </summary>
		[Browsable(true)]
		public bool RenderLabel
		{
			get
			{
				return this.renderLabel;
			}
			set
			{
				this.renderLabel = value;
			}
		}

		/// <summary>
		/// Gets or sets an extra value on the TextBox which can be used to identityf the TextBox
		/// For Example in a OnTextchanged Vevent this proprty could contain information about what to update. 
		/// </summary>
		[Browsable(true), Bindable(true), DefaultValue("")]
		public string EventArgumentExtra
		{
			get
			{
				return this.eventArgumentExtra;
			}
			set
			{
				this.eventArgumentExtra = value;
			}
		}

		private string eventArgument = string.Empty;

		/// <summary>
		/// Gets or sets the name of the client function which has to be called when a key is pressed
		/// </summary>
		public string OnChangeClientFunction
		{
			get
			{
				return this.onChangeClientFunction;
			}
			set
			{
				this.onChangeClientFunction = value;
			}
		}

		/// <summary>
		/// Gets or sets the id of the control to set a new value when a key is pressed
		/// </summary>
		public string OnChangeClientControlId
		{
			get
			{
				return this.onChangeClientControlId;
			}
			set
			{
				this.onChangeClientControlId = value;
			}
		}

		/// <summary>
		/// Gets or sets the LocalizeDefaultValue
		/// </summary>
		public bool LocalizeDefaultValue
		{
			get
			{
				return this.localizeDefaultValue;
			}
			set
			{
				this.localizeDefaultValue = value;
			}
		}

        [Browsable(true)]
	    public string Placeholder { get; set; }

		#endregion

		#region Methods

		#region Obsolete methods

		[Obsolete("This property is obsolete. Support may be discontinued in next version")]
		public void SetDateTime(System.DateTime dateTime)
		{
			this.Text = dateTime.ToShortTimeString();
		}

		#endregion

		/// <summary>
		/// Hookup the events to the corresponding event handlers
		/// </summary>
		private void HookupEvents()
		{
			this.TextChanged += new EventHandler(TextBox_TextChanged);
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (!string.IsNullOrEmpty(this.defaultButton))
			{
				// Default button on Enter script;
				if (!this.Page.ClientScript.IsClientScriptBlockRegistered(ScriptKeyTextBoxExtendedKeyDownHandler))
				{
					StringBuilder script = new StringBuilder();
					script.Append("function TextBoxExtenderKeyDownHandler(evt,controlName,handleNumerics)\n\r");
					script.Append("{\n\r");
					script.Append("     gottenEvent = evt || window.event;\n\r");
					script.Append("     if (gottenEvent.keyCode == 13)\n\r");
					script.Append("     {\n\r");
					script.Append("         var button = document.getElementById(controlName);\n\r");
					script.Append("         if (button != null)\n\r");
					script.Append("             button.click();\n\r");
					script.Append("         else\n\r");
					script.Append("             alert('DEFAULT BUTTON NOT FOUND');;\n\r");
					//                    script.Append("         window.event.returnValue = false;\n\r");
					script.Append("         return false;\n\r");
					script.Append("     }\n\r");
					script.Append("}\n\r");
					Page.ClientScript.RegisterClientScriptBlock(GetType(), ScriptKeyTextBoxExtendedKeyDownHandler, script.ToString(), true);
				}

				if (!string.IsNullOrEmpty(this.defaultButton))
					try
					{
						this.Attributes["OnKeyDown"] += "return TextBoxExtenderKeyDownHandler(event,'" + this.FindControl(this.defaultButton).ClientID + "','" + this.javascriptNumericsEnabled.ToString() + "');";
					}
					catch
					{
						if (this.Page is Dionysos.Web.UI.PageDefault)
						{
							Dionysos.Web.UI.PageDefault p = this.Page as Dionysos.Web.UI.PageDefault;
							this.Attributes["OnKeyDown"] += "return TextBoxExtenderKeyDownHandler(event,'" + p.ControlList.FindControl(this.defaultButton).ClientID + "','" + this.javascriptNumericsEnabled.ToString() + "');";
						}
					}
			}

			// Numeric plus and minus script
			if (this.JavascriptNumericsEnabled)
			{
				// Default button on Enter script;
				if (!this.Page.ClientScript.IsClientScriptBlockRegistered(ScriptTextBoxExtenderKeyDownNumericHandler))
				{
					StringBuilder script = new StringBuilder();
					script.Append("function TextBoxExtenderKeyDownNumericHandler(e)");
					script.Append("{");
					script.Append("     if(e.keyCode == 38)");
					script.Append("     {");
					script.Append("         this.value = parseInt(this.value)+1;");
					if (this.IndicateChangedValue)
					{
						script.Append("     this.style.backgroundColor = '" + this.IndicateChangedColor + "'");
					}
					script.Append("     }");
					script.Append("	 else if(e.keyCode == 40)");
					script.Append("	 {");
					script.Append("	 	this.value = parseInt(this.value)-1;");
					if (this.IndicateChangedValue)
					{
						script.Append("     this.style.backgroundColor = '" + this.IndicateChangedColor + "'");
					}
					script.Append("	 }");
					script.Append("}");
					script.Append("function TextBoxExtendedPlusMinusHandler(tbId,doAction)");
					script.Append("{");
					script.Append("    tb = document.getElementById(tbId);");
					script.Append("    if(doAction == \"+\")");
					script.Append("    {");
					script.Append("         tb.value = parseInt(tb.value)+1;");
					if (this.IndicateChangedValue)
					{
						script.Append("     tb.style.backgroundColor = '" + this.IndicateChangedColor + "'");
					}
					script.Append("    }");
					script.Append("    else");
					script.Append("    {");
					script.Append("	 	    tb.value = parseInt(tb.value)-1;");
					if (this.IndicateChangedValue)
					{
						script.Append("     tb.style.backgroundColor = '" + this.IndicateChangedColor + "'");
					}
					script.Append("    }");
					script.Append("}");
					Page.ClientScript.RegisterClientScriptBlock(GetType(), ScriptTextBoxExtenderKeyDownNumericHandler, script.ToString(), true);
				}
			}

			if (this.onChangeClientFunction.Length > 0 && this.onChangeClientControlId.Length > 0)
			{
				if (!this.Page.ClientScript.IsClientScriptBlockRegistered(ScriptTextBoxExtenderOnChangeHandler))
				{
					StringBuilder script = new StringBuilder();
					script.Append("function TextBoxExtenderOnChangeHandler(source)");
					script.Append("{");
					script.AppendFormat("    tb = document.getElementById({0});", this.onChangeClientControlId);
					script.AppendFormat("    tb.value = {0}(tb.value);", this.onChangeClientFunction);
					script.Append("}");
					Page.ClientScript.RegisterClientScriptBlock(GetType(), ScriptTextBoxExtenderOnChangeHandler, script.ToString(), true);
				}
			}

			// Add default value logic
			if (this.DefaultValue != string.Empty)
			{
				if (this.Text.Length <= 0 || this.Text == this.DefaultValue)
				{
					this.Attributes["title"] = this.DefaultValue;
					if (Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.TextBoxCustomDefaultValueImplementation, true))
					{
						// Custom implemnentation						
					}
					else
					{
						this.Text = this.DefaultValue;

						if (this.RemoveDefaultValueOnFocus)
						{
							string script = "function resettb(tb){	if(tb.value == tb.title)	{		tb.value = \"\";	}}";
							script += "function settb(tb){	if(tb.value == \"\")	{		tb.value = tb.title;	}  }";
							Page.ClientScript.RegisterClientScriptBlock(GetType(), ScriptTextBoxExtenderDefaultValue, script, true);

							this.Attributes.Add("onblur", string.Format("settb(this);", this.DefaultValue));
							this.Attributes.Add("onclick", string.Format("resettb(this);", this.DefaultValue));
						}
					}
				}
			}

			// Indicate Changed with Background Change
			if (this.IndicateChangedValue)
			{
				this.Attributes["onchange"] += string.Format("javascript:this.style.backgroundColor='{0}';", this.IndicateChangedColor);
			}

			// Indicate Changed with Background Change
			if (this.SaveGroup.Length > 0)
			{
				this.Attributes["onchange"] += string.Format("javascript:{0}=true;", this.SaveGroup);
			}

			if (this.onChangeClientFunction.Length > 0 && this.onChangeClientControlId.Length > 0)
			{
				this.Attributes["onkeyup"] += "TextBoxExtenderOnChangeHandler();";
			}

            // Add filter illegal characters logic (these characters causes the serialize method to throw an exception)        
            if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), ScriptTextBoxExtenderOnKeyUpHandler))
            {
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), ScriptTextBoxExtenderOnKeyUpHandler, EmbeddedResourceUtil.UnpackEmbeddedResourceToString("Dionysos.Web.UI.WebControls.TextBox.TextBox.js"), true);
            }                       
            
            this.Attributes["onkeyup"] += "inputValidator(this);";

		    if (!this.Placeholder.IsNullOrWhiteSpace())
		    {
		        this.Attributes.Add("placeholder", this.Placeholder);
		    }

			base.OnPreRender(e);
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if (this.isRequired)
			{
				this.CssClass += " required ";
			}

			bool renderReadOnly = false;
			if (this.Page.GetType().GetProperty("RenderReadOnly") != null)
			{
				object val = this.Page.GetType().GetProperty("RenderReadOnly").GetValue(this.Page, null);
				if (Convert.ToBoolean(val))
				{
					renderReadOnly = true;
				}
			}
			if ((renderReadOnly && !this.RenderReadOnlyOverwrite && this.renderLabel) || (this.pageMode == PageMode.View && this.renderLabel))
			{
				if (this.Text.Length > 0 || this.emptyText.Length > 0)
				{
					Dionysos.Web.UI.WebControls.Label lbl = new Dionysos.Web.UI.WebControls.Label();
					lbl.LocalizeText = false;
					lbl.Text = (this.Text.Length > 0 ? this.Text : this.emptyText);
					lbl.RenderControl(writer);
				}
				else
					writer.Write("&nbsp;");
			}
			else
			{
				//Perform the normal rendering
				if (this.isValid)
				{
					this.CssClass.Replace(" ivld ", "");
				}
				else
				{
					this.CssClass += " ivld ";
				}

				base.Render(writer);

				if (this.RenderIcon && !Instance.Empty(this.iconUrl))
				{
					writer.Write("&nbsp;");

					ImageButton imageBtn = new ImageButton();
					imageBtn.ImageUrl = ResolveUrl(this.iconUrl);
					imageBtn.CssClass = "buttonIcon";
					imageBtn.RenderControl(writer);
				}

				// Render Event Attacher for Javascript Numerics
				if (this.JavascriptNumericsEnabled)
				{
					writer.Write(string.Format("<script type=\"text/javascript\">document.getElementById('{0}').onkeydown = TextBoxExtenderKeyDownNumericHandler;</script>", this.ClientID));

					// Handle PlusMin buttons
					if (string.IsNullOrEmpty(this.AmountPlusImageUrl))
					{
						writer.Write(string.Format("&nbsp;<a href=\"javascript:TextBoxExtendedPlusMinusHandler('{0}','+')\"><big><strong>+</strong</big></a>&nbsp;", this.ClientID));
					}
					else
					{
						writer.Write(string.Format("&nbsp;<a href=\"javascript:TextBoxExtendedPlusMinusHandler('{0}','+')\"><img src=\"{1}\"></a>&nbsp;", this.ClientID, this.Page.ResolveUrl(this.AmountPlusImageUrl)));
					}

					if (string.IsNullOrEmpty(this.AmountMinusImageUrl))
					{
						writer.Write(string.Format("<a href=\"javascript:TextBoxExtendedPlusMinusHandler('{0}','-')\"><big><strong>-</strong></big></a>", this.ClientID));
					}
					else
					{
						writer.Write(string.Format("<a href=\"javascript:TextBoxExtendedPlusMinusHandler('{0}','-')\"><img src=\"{1}\"></a>", this.ClientID, this.Page.ResolveUrl(this.AmountMinusImageUrl)));
					}
				}
			}
		}

		/// <summary>
		/// Raises the System.Web.UI.Control.Init event
		/// </summary>
		/// <param name="e">An System.EventArgs object that contains the event data</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			Page.Validators.Add(this);
			this.EnableViewState = true;

			// Check whether a css class is set
			// if none is set, set the css class to input
			if (Instance.Empty(this.CssClass) && Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.TextBoxDefaultCssClass, true).Length > 0)
			{
				this.CssClass = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.TextBoxDefaultCssClass, true);
			}
		}

		/// <summary>
		/// Raises the System.Web.UI.Control.Load event
		/// </summary>
		/// <param name="e">An System.EventArgs object that contains the event data</param>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			// Set the page mode for this control
			if (Member.HasMember(Page, "PageMode"))
			{
				PageMode pageMode = (PageMode)Member.InvokeProperty(Page, "PageMode");
				if (Instance.Empty(pageMode))
				{
					throw new EmptyException("Variable 'pageMode' is empty.");
				}
				else
				{
					this.pageMode = pageMode;
				}
			}
		}

		/// <summary>
		/// Raises the System.Web.UI.Control.Unload event
		/// </summary>
		/// <param name="e">An System.EventArgs object that contains the event data</param>
		protected override void OnUnload(EventArgs e)
		{
			if (!Instance.Empty(Page))
			{
				Page.Validators.Remove(this);
			}

			base.OnUnload(e);
		}

		/// <summary>
		/// Validates the input of the control (if required)
		/// </summary>
		public virtual void Validate()
		{
            // Remove illegal characters from value
		    string newValue = RemoveInvalidXmlChars(this.Text);
		    if (!this.Text.Equals(newValue))
		    {
		        this.Text = newValue;
		    }

			this.isValid = true;
			if (this.useValidation)
			{
				if (this.pageMode == PageMode.View)
				{
					// No validation in view page mode
				}
				else
				{
					// Check whether input is required
					// and if so, check whether input is entered
					if (this.Page is Dionysos.Web.UI.PageLLBLGenEntity)
					{
						IDataErrorInfo errorInfo = ((Dionysos.Web.UI.PageLLBLGenEntity)this.Page).DataErrorInfo;
						string fieldName = StringUtil.RemoveLeadingLowerCaseCharacters(this.ID);
						if (errorInfo[fieldName] != null && errorInfo[fieldName].Length > 0)
						{
							this.isValid = false;
							this.errorMessage = String.Format("<strong>{0}</strong>: {1}", this.FriendlyName, errorInfo[fieldName].Replace(fieldName, this.FriendlyName));
						}
					}

					if (this.IsValid)
					{
						if (this.isRequired &&
							StringUtil.IsNullOrWhiteSpace(this.Text))
						{
							this.isValid = false;
							this.errorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Required, this.FriendlyName);
						}
					}
				}
			}
		}

		/// <summary>
		/// Helper method to fire up the value changed event
		/// </summary>
		/// <param name="sender">The sender</param>
		/// <param name="e">The EventArgs</param>
		public void OnValueChanged(object sender, EventArgs e)
		{
			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

	    private static string RemoveInvalidXmlChars(string text)
	    {
	        if (text == null) return null;
	        if (text.Length == 0) return text;

            // a bit complicated, but avoids memory usage if not necessary
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                var ch = text[i];
                if (XmlConvert.IsXmlChar(ch))
                {
                    result.Append(ch);
                }
            }
            return result.ToString();
        }

        private static bool HasInvalidXmlChars(string text)
        {
            if (text == null) return false;
            if (text.IsNullOrWhiteSpace()) return false;

            return text.Any(e => !XmlConvert.IsXmlChar(e));
        }

	    #endregion

		#region Event handlers

		private void TextBox_TextChanged(object sender, EventArgs e)
        {
            if (sender is TextBox textBox)
            {
				HasTextChanged = true;

                if (!textBox.Text.IsNullOrWhiteSpace())
                {
					textBox.Text = textBox.Text.Trim();
				}
			}

			this.OnValueChanged(sender, e);
		}

		#endregion

		#region Events

		/// <summary>
		/// Occurs when the value changed between posts to the server
		/// </summary>
		public event EventHandler ValueChanged;

		#endregion
	}
}