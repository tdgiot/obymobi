using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing string values in
    /// </summary>
    public class TextBoxMobile: TextBox
    {
        #region Fields
		private string wapInputFormat = string.Empty;		

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxString type
        /// </summary>
        public TextBoxMobile()
        {
            this.UseValidation = true;
			this.PreRender += new EventHandler(TextBoxMobile_PreRender);
        }

		void TextBoxMobile_PreRender(object sender, EventArgs e)
		{
			// Below not validated HTML MP
			//if (this.wapInputFormat.Length > 0)
			//{
			//    this.Attributes.Add("format", this.WapInputFormat);				
			//}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{			
			base.Render(writer);			
		}


        #endregion

        #region Methods

        #endregion

        #region Properties	

		/// <summary>
		/// Gets or sets the wapInputFormat
		/// </summary>
		public string WapInputFormat
		{
			get
			{
				return this.wapInputFormat;
			}
			set 
			{
				this.wapInputFormat = value;
			}
		}

        /// <summary>
        /// Gets or sets the System.String value of the input
        /// </summary>
        [Browsable(false)]
        public new string Value
        {
            get
            {
                return base.Value.ToString();
            }
            set
            {
                base.Value = value;
            }
        }

        #endregion
    }
}
