using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing string values in
    /// </summary>
    public class TextBoxMultiLine : TextBox
    {
        #region Fields

        private bool autoGrow = false;
        private bool prettyPrintJson = false;
        private const string ScriptKeyTextBoxAutoGrow = "TextAreaAutoGrow";

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxMultiLine type
        /// </summary>
        public TextBoxMultiLine()
        {
            this.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
        }

        #endregion

        #region Methods

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (this.AutoGrow || this.MaxLength > 0 || this.PrettyPrintJson)
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), ScriptKeyTextBoxAutoGrow, EmbeddedResourceUtil.UnpackEmbeddedResourceToString("Dionysos.Web.UI.WebControls.TextBox.TextBoxMultiLine.js"), true);

            // Add javascript for autogrow
            if (this.AutoGrow)
            {
                this.Attributes["onfocus"] += string.Format("grow(this, {0});", this.Rows);
            }

            if (this.MaxLength > 0)
            {
                this.Attributes["onkeydown"] += string.Format("checkTextAreaMaxLength(this, event,'{0}');", this.MaxLength);
            }

            if (this.PrettyPrintJson)
            {
                this.Attributes.Add("autofocus", "autofocus");
                this.Attributes["onfocus"] += "prettyPrintJson(this);";
            }
        }

        /// <summary>
        /// Renders the Lynx-media.Web.UI.WebControls.TextBox.TextBox control
        /// to the specified System.Web.UI.HtmlTextWriter instance
        /// </summary>
        /// <param name="writer">The HtmlTextWriter instance which writes the html text</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (this.PageMode == PageMode.View)
            {
                LiteralControl literal = new LiteralControl(string.Format("<span>{0}</span>", this.Value.Replace("\r\n", "<br />")));
                literal.RenderControl(writer);
            }
            else
            {
                base.Render(writer);

                //if (this.IsRequired)
                //{
                //    writer.Write("&nbsp;");

                //    Label label = new Label();
                //    label.Text = "*";
                //    label.CssClass = "required";
                //    label.RenderControl(writer);
                //}
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the System.String value of the input
        /// </summary>
        [Browsable(false)]
        public new string Value
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the AutoGrow which make the TextArea grow when the content is bigger than the TextArea
        /// </summary>
        public bool AutoGrow
        {
            get
            {
                return this.autoGrow;
            }
            set
            {
                this.autoGrow = value;
            }
        }

        public bool PrettyPrintJson
        {
            get
            {
                return this.prettyPrintJson;
            }
            set
            {
                this.prettyPrintJson = value;
            }
        }

        #endregion
    }
}
