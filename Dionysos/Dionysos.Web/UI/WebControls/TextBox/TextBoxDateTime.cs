using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing DateTime values in
    /// </summary>
    public class TextBoxDateTime : TextBox
    {
        #region Fields

        private System.DateTime value = System.DateTime.Now;
        private string dataFormatString = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxDateTime type
        /// </summary>
        public TextBoxDateTime()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the input of the control (if required)
        /// and checks whether the input is a DateTime value
        /// </summary>
        public override void Validate()
        {
            this.IsValid = true;
            if (this.UseValidation)
            {
                // Check whether input is required
                // and if so, check whether input is entered
                base.Validate();
                if (this.IsValid)
                {
                    // Check whether the input is a DateTime value
                    if (this.IsValid && !System.DateTime.TryParse(this.Text, out this.value))
                    {
                        this.IsValid = false;
                        this.ErrorMessage = string.Format(Resources.Dionysos_Web_UI_WebControls.TextBoxDateTime_ErrorMessage, this.FriendlyName);
                    }
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the System.DateTime value of the input
        /// </summary>
        [Browsable(false)]
        public new System.DateTime Value
        {
            get
            {
                System.DateTime.TryParse(this.Text, out this.value);
                return this.value;
            }
            set
            {
                this.value = value;
                if (!Instance.Empty(this.value))
                {
                    if (!Instance.Empty(this.dataFormatString))
                    {
                        base.Value = string.Format(this.dataFormatString, this.value);
                    }
                    else
                    {
                        base.Value = this.value.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the data format string
        /// </summary>
        [Browsable(true)]
        public string DataFormatString
        {
            get
            {
                return this.dataFormatString;
            }
            set
            {
                this.dataFormatString = value;
            }
        }

        #endregion
    }
}
