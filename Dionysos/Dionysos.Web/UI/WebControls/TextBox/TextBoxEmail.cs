using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing email addresses in
    /// </summary>
    public class TextBoxEmail : TextBox
    {
        #region Fields

        private string value = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxEmail type
        /// </summary>
        public TextBoxEmail()
        {
            // Set the defaults
            this.IconUrl = "~/Images/Icons/email.gif";
            this.UseValidation = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the input of the control (if required)
        /// and checks whether the input is a email address
        /// </summary>
        public override void Validate()
        {
            this.IsValid = true;
            if (this.UseValidation)
            {
                // Check whether input is required
                // and if so, check whether input is entered
                if (this.IsRequired && Instance.Empty(this.Text.Trim()))
                {
                    this.IsValid = false;
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Required, this.FriendlyName);
                }

                // Check whether the input is a email address
                EmailAddress temp;
                if (this.IsValid && !Instance.Empty(this.Text.Trim()) && !EmailAddress.TryParse(this.Text, out temp))
                {
                    this.IsValid = false;
                    this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBoxEmail_ErrorMessage, this.FriendlyName);
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Lynx-media.EmailAddress value of the input
        /// </summary>
        [Browsable(false)]
        public new string Value
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Text))
                {
                    EmailAddress temp;
                    if (EmailAddress.TryParse(this.Text, out temp))
                    {
                        this.value = this.Text;
                    }
                    else
                    {
                        this.value = string.Empty;
                    }
                }
                return this.value;
            }
            set
            {
                this.value = value;
                if (!Instance.Empty(this.value))
                {
                    base.Value = this.value;
                }
            }
        }

        #endregion
    }
}
