using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing passwords in
    /// </summary>
    public class TextBoxPassword : TextBoxString
    {
        #region Fields

        string verifyInputTextBoxId;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxPassword type
        /// </summary>
        public TextBoxPassword()
        {
            // Set the defaults
            this.TextMode = TextBoxMode.Password;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Renders the Lynx-media.Web.UI.WebControls.TextBox.TextBox control
        /// to the specified System.Web.UI.HtmlTextWriter instance
        /// </summary>
        /// <param name="writer">The HtmlTextWriter instance which writes the html text</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (this.PageMode == PageMode.View && this.RenderLabel)
            {
                if (this.IsRequired)
                {
                    this.CssClass += " required ";
                }

                Label valueLabel = new Label();
                string value = this.Value;
                if (value.Length > 0)
                {
                    valueLabel.Text = this.Value.ToString().Replace(value, "*".PadLeft(value.Length, '*'));
                }
                else
                {
                    valueLabel.Text = value;
                }
                valueLabel.CssClass = "label-value";
                valueLabel.RenderControl(writer);

                if (this.IsValid)
                {
                    this.CssClass.Replace(" ivld ", "");
                }
                else
                {
                    this.CssClass += " ivld ";
                }
            }
            else
            {
                base.Render(writer);
            }
        }

        #endregion

        #region Properties
        
        /// <summary>
        /// Set the Id of the second input control to validate the user entry (NOT YET IMPLEM ENTED)
        /// </summary>
        public string VerifyInputTextBoxId
        {
            get
            {
                return this.verifyInputTextBoxId;
            }
            set
            {
                this.verifyInputTextBoxId = value;
            }
        }

        #endregion
    }
}
