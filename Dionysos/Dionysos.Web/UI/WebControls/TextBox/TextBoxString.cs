using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing string values in
    /// </summary>
    public class TextBoxString : TextBox
    {
        #region Fields

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxString type
        /// </summary>
        public TextBoxString()
        {
            this.UseValidation = true;            
        }

        #endregion

        #region Methods

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the System.String value of the input
        /// </summary>
        [Browsable(false)]
        public new string Value
        {
            get
            {
                return base.Value.ToString();
            }
            set
            {
                base.Value = value;
            }
        }

        #endregion
    }
}
