﻿	// Inserts the given begintag sStartTagToInsert in front of the selected text and sEndTagToInsert behind the
	// selected text. If no text is selected, or the user uses a browser which doesn't support decent javascripting
	// the tags are inserted at the end of the text in tbxMessage.
	function InsertTagAroundSelection(sStartTagToInsert, sEndTagToInsert)
	{
		var sSelectedText;
		if (tbxMessage)
		{
			if (typeof tbxMessage.cursorPos != 'undefined')
			{
				var cursorPos = tbxMessage.cursorPos;
				sSelectedText = cursorPos.text;
				cursorPos.text = sStartTagToInsert + sSelectedText + sEndTagToInsert;
			}
			else 
			{
				if (typeof tbxMessage.selectionStart != 'undefined')
				{
					// remember scrollposition
					var scrollTop = tbxMessage.scrollTop;

					var sStart = tbxMessage.selectionStart;
					var sEnd = tbxMessage.selectionEnd;
					sSelectedText = tbxMessage.value.substring(sStart, sEnd);
					var newText = sStartTagToInsert + sSelectedText + sEndTagToInsert;
					tbxMessage.value = tbxMessage.value.substr(0, sStart) + newText + tbxMessage.value.substr(sEnd);
					var nStart = sStart == sEnd ? sStart + newText.length : sStart;
					var nEnd = sStart + newText.length;
					tbxMessage.setSelectionRange(nStart, nEnd);

					// reset scrollposition
					tbxMessage.scrollTop = scrollTop;
				}
				else
				{
					tbxMessage.value += sStartTagToInsert + sEndTagToInsert;
				}
			}

			tbxMessage.focus();
			if (typeof tbxMessage.cursorPos != 'undefined') 
			{
				tbxMessage.onselect();
			}
		}
		return false;
	}