using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing double values in
    /// </summary>
    public class TextBoxDouble : TextBox
    {
        #region Fields

        private double? value = null;
        private bool allowZero = false;
        private bool allowNegative = true;
        private int minimalValue = -1;
        private int maximalValue = -1;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxDouble type
        /// </summary>
        public TextBoxDouble()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the input of the control (if required)
        /// and checks whether the input is a double value
        /// </summary>
        public override void Validate()
        {
            this.IsValid = true;
            if (this.UseValidation)
            {
                // Check whether input is required
                // and if so, check whether input is entered
                base.Validate();
                if (this.IsValid)
                {
                    // Check whether the input is a double value
					double parsedValue;
                    if (this.IsValid && !Double.TryParse(this.Text, out parsedValue))
                    {
                        this.IsValid = false;
                        this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Required, this.FriendlyName);
                    }

                    // Check whether the input is an integer value
                    if (this.IsValid && !this.allowNegative && (this.value < 0))
                    {
                        this.IsValid = false;
                        this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Negative, this.FriendlyName);
                    }

                    // Check whether the input is an integer value
                    if (this.IsValid && !this.allowZero && (this.value == 0))
                    {
                        this.IsValid = false;
                        this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Zero, this.FriendlyName);
                    }

                    // Check whether the input is greater than the minimal value
                    if (this.IsValid && this.minimalValue != -1 && this.value < this.minimalValue)
                    {
                        this.IsValid = false;
                        this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_LowerThanMinimalValue, this.FriendlyName, this.MinimalValue);
                    }

                    // Check whether the input is smaller than the maximal value
                    if (this.IsValid && this.maximalValue != -1 && this.value > this.maximalValue)
                    {
                        this.IsValid = false;
                        this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_GreaterThanMaximalValue, this.FriendlyName, this.MaximalValue);
                    }

                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the System.Double value of the input
        /// </summary>
        [Browsable(false)]
        public new Double? Value
        {
            get
            {
				if (this.Text == string.Empty)
				{
					this.value = null;
				}
				else
				{
					double parsedValue;
					if(Double.TryParse(this.Text, out parsedValue))
						this.value = parsedValue;
					else
						this.value = null;
				}
                return this.value;
            }
            set
            {
                this.value = value;
                if (!Instance.Empty(this.value))
                {
                    base.Value = this.value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicated whether zero is allowed
        /// </summary>
        [Browsable(true)]
        public bool AllowZero
        {
            get
            {
                return this.allowZero;
            }
            set
            {
                this.allowZero = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicated whether negative values are allowed
        /// </summary>
        [Browsable(true)]
        public bool AllowNegative
        {
            get
            {
                return this.allowNegative;
            }
            set
            {
                this.allowNegative = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimal value 
        /// </summary>
        [Browsable(true)]
        public int MinimalValue
        {
            get
            {
                return this.minimalValue;
            }
            set
            {
                this.minimalValue = value;
            }
        }

        /// <summary>
        /// Gets or sets the PropertyName
        /// </summary>
        [Browsable(true)]
        public int MaximalValue
        {
            get
            {
                return this.maximalValue;
            }
            set
            {
                this.maximalValue = value;
            }
        }

        #endregion
    }
}
