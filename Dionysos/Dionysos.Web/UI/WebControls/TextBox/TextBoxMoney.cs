using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a TextBox for storing money values in
    /// </summary>
    public class TextBoxMoney : TextBox
    {
        #region Fields

        private decimal value = 0.0m;
        private bool allowNegative = true;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Web.UI.WebControls.TextBoxMoney type
        /// </summary>
        public TextBoxMoney()
        {
            // Set the defaults
            this.IconUrl = "~/Images/Icons/money_euro.gif";
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the input of the control (if required)
        /// and checks whether the input is a decimal value
        /// </summary>
        public override void Validate()
        {
            this.IsValid = true;
            if (this.UseValidation)
            {
                // Check whether input is required
                // and if so, check whether input is entered
                base.Validate();
                if (this.IsValid)
                {
                    // Check whether the input is a decimal value
                    string text = this.Text;
                    string result = string.Empty;

                    for (int i = 0; i < text.Length; i++)
                    {
                        char character = Char.Parse(text.Substring(i, 1));
                        if (Char.IsDigit(character) || Char.IsNumber(character) || Char.IsPunctuation(character) || Char.IsSeparator(character))
                        {
                            result += character.ToString();
                        }
                    }

                    if (this.IsValid && !Decimal.TryParse(result, out this.value))
                    {
                        this.IsValid = false;
                        this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBoxMoney_ErrorMessage, this.FriendlyName);
                    }

                    // Check whether the input is an integer value
                    if (this.IsValid && !this.allowNegative && (this.value < 0))
                    {
                        this.IsValid = false;
                        this.ErrorMessage = String.Format(Resources.Dionysos_Web_UI_WebControls.TextBox_ErrorMessage_Negative, this.FriendlyName); ;
                    }
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the System.Decimal value of the input
        /// </summary>
        [Browsable(false)]
        public new Decimal Value
        {
            get
            {
                Decimal.TryParse(this.Text, out this.value);
                return this.value;
            }
            set
            {
                this.value = value;
                if (!Instance.Empty(this.value))
                {
                    base.Value = this.value.ToString("N2");
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicates if negative values are allowed
        /// </summary>
        public bool AllowNegative
        {
            get
            {
                return this.allowNegative;
            }
            set
            {
                this.allowNegative = value;
            }
        }

        #endregion
    }
}
