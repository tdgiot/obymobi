using System;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.ComponentModel.Design;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Summary description for TimePicker.
	/// </summary>
	/// 
	public class TimePickerDesigner : System.Web.UI.Design.ControlDesigner
	{
		// Whether to display the html of the associated 
		// control using a large heading text size.		

		public TimePickerDesigner()
			: base()
		{
		}

		// Returns the html to use to represent the control at design time.
		public override string GetDesignTimeHtml()
		{
			return "[Time Picker]";
		}
	}

	/// <summary>
	/// Summary description for MouseOverDataGrid.
	/// </summary>
	[DesignerAttribute(typeof(TimePickerDesigner), typeof(IDesigner))]
	public class TimePicker : Control, INamingContainer, IValidator, IBindable
	{

		#region Required methods for Validator Interface
		private bool _valid = true;
		private string _errorMessage = "";
		private string _friendlyName = "Time Selection";
		public bool _blankAllowed = false;

		#region Boring standard for every control
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			Page.Validators.Add(this);
			this.EnableViewState = true;
		}

		protected override void OnUnload(EventArgs e)
		{
			if (Page != null)
			{
				Page.Validators.Remove(this);
			}
			base.OnUnload(e);
		}

		public bool IsValid
		{
			get { return _valid; }
			set
			{
				_valid = value;
			}
		}

		public string ErrorMessage
		{
			get { return _errorMessage; }
			set { _errorMessage = value; }
		}

		public string FriendlyName
		{
			get { return _friendlyName; }
			set { _friendlyName = value; }
		}
		
		public bool PreventValidation { get; set; }

		#endregion

		public virtual void Validate()
		{
            // GK Or am I cutting too much of a corner now?
            if (!this.Page.IsPostBack)
                return;

			var hourBox = this.GetHourBox();
			var minuteBox = this.GetMinuteBox();
			int hours, minutes;

			// Check
			if (this.PreventValidation)
			{
				// Nothing to do here :)
			}
            else if (this.AllowNull && !this.Hours.HasValue && !this.Minutes.HasValue)
            {
                // Fine, nothsing.
            }
            else if (!Int32.TryParse(hourBox.Text, out hours) || hours < 0 || hours > 23)
            {
                // Invalid hour
                this._valid = false;
                this.ErrorMessage = this.FriendlyName + ": The hours should be between 0 and 23.";
            }
            else if (!Int32.TryParse(minuteBox.Text, out minutes) || minutes < 0 || minutes > 59)
            {
                // Invalid minute
                this._valid = false;
                this.ErrorMessage = this.FriendlyName + ": The minutes should be between 0 and 59.";
            }
		}

		#endregion

		public TimePicker()
		{			
            this.AllowNull = true;
            EnsureChildControls();
			this.GetHourBox().TextChanged += new EventHandler(TimePicker_TextChanged);
			this.GetMinuteBox().TextChanged += new EventHandler(TimePicker_TextChanged);
		}

        private bool allowNull;
        public bool AllowNull {
            get
            {
                return this.allowNull;
            }
            set
            {
                this.allowNull = value;
            }
        }

		public System.DateTime? SelectedTime
		{
			get
			{
                if (this.Hours.HasValue && this.Minutes.HasValue)                
                    return new System.DateTime(1900, 1, 1, this.Hours.Value, this.Minutes.Value, 0);                
                else
                    return null;				
			}
			set
			{
				this.EnsureChildControls();
                if (value.HasValue)
                {
                    Hours = value.Value.Hour;
                    Minutes = value.Value.Minute;
                }
                else
                {
                    this.Hours = null;
                    this.Minutes = null;
                }
			}
		}

		public int? Hours
		{
			get
			{
				EnsureChildControls();
				int? ret;
				if (((System.Web.UI.WebControls.TextBox)GetHourBox()).Text.Length == 0 || ((System.Web.UI.WebControls.TextBox)GetMinuteBox()).Text.Length > 2)
				{
					ret = null;
				}
				else
				{
					ret = Convert.ToInt32("0" + ((System.Web.UI.WebControls.TextBox)GetHourBox()).Text);
				}
				return ret;
			}
			set
			{
				EnsureChildControls();
				if (value.ToString().Length == 1)
				{
					((System.Web.UI.WebControls.TextBox)GetHourBox()).Text = "0" + value.ToString();
				}
				else
				{
					((System.Web.UI.WebControls.TextBox)GetHourBox()).Text = value.ToString();
				}
			}
		}

		public int? Minutes
		{
			get
			{
				EnsureChildControls();
				int? ret;
				if (((System.Web.UI.WebControls.TextBox)GetMinuteBox()).Text.Length == 0 || ((System.Web.UI.WebControls.TextBox)GetMinuteBox()).Text.Length > 2)
				{
					ret = null;
				}
				else
				{
					ret = Convert.ToInt32("0" + ((System.Web.UI.WebControls.TextBox)GetMinuteBox()).Text);
				}
				return ret;
			}
			set
			{
				EnsureChildControls();
				if (value.ToString().Length == 1)
				{
					((System.Web.UI.WebControls.TextBox)GetMinuteBox()).Text = "0" + value.ToString();
				}
				else
				{
					((System.Web.UI.WebControls.TextBox)GetMinuteBox()).Text = value.ToString();
				}

			}
		}

		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			this.tbHours = new System.Web.UI.WebControls.TextBox();
            this.tbHours.ID = $"tbHours_{this.ID}";
            this.tbHours.Text = string.Empty;
            this.tbHours.CssClass = "inputNoResize";
            this.tbHours.MaxLength = 2;
            this.tbHours.Attributes.Add("style", "width:18px;");
            this.tbHours.Attributes.Add("onChange", "validateMaxValue(this,23);");
			//tbHours.ID = "TestUur";
			Controls.Add(tbHours);

			Controls.Add(new LiteralControl(":"));

			this.tbMinutes = new System.Web.UI.WebControls.TextBox();
            this.tbMinutes.ID = $"tbMinutes_{this.ID}";
            int minute = System.DateTime.Now.Minute;
			string minuteText = "";
			if (minute < 10)
			{
				minuteText = string.Format("0{0}", minute);
			}
			else
			{
				minuteText = string.Format("{0}", minute);
			}
            this.tbMinutes.Text = string.Empty;			
            this.tbMinutes.MaxLength = 2;
            this.tbMinutes.CssClass = "inputNoResize";            
            this.tbMinutes.Attributes.Add("style", "width:18px;");
            this.tbMinutes.Attributes.Add("onChange", "validateMaxValue(this,59);");
			Controls.Add(tbMinutes);
		}

        private System.Web.UI.WebControls.TextBox tbMinutes;
        private System.Web.UI.WebControls.TextBox tbHours;

		public void SetTimeFromShortTimeString(string shortTimeString)
		{
			this.Hours = Convert.ToInt32(Dionysos.StringUtil.GetAllBeforeFirstOccurenceOf(shortTimeString, char.Parse(":"), false));
			this.Minutes = Convert.ToInt32(Dionysos.StringUtil.GetAllAfterFirstOccurenceOf(shortTimeString, char.Parse(":")));
		}

		protected override void OnPreRender(EventArgs e)
		{
			//((System.Web.UI.WebControls.TextBox)GetHourBox()).Text = DateTime.UtcNow.Hour.ToString();
			//((System.Web.UI.WebControls.TextBox)GetMinuteBox()).Text = DateTime.UtcNow.Minute.ToString();
			string javaScript = "";
			javaScript += "<script type=\"text/javascript\">";
			javaScript += "function validateMaxValue(control, maxvalue)";
			javaScript += "{ if(control.value > maxvalue) control.value=maxvalue; if(control.value < 0) control.value=0; } </script>";
			if (!this.Page.ClientScript.IsClientScriptBlockRegistered("timePickerFunctions"))
				this.Page.ClientScript.RegisterClientScriptBlock(GetType(), "timePickerFunctions", javaScript);

            if (!this.AllowNull)
            {
                tbHours.BorderColor = Color.Red;
                tbHours.BorderWidth = Unit.Pixel(1);
            }
            if (!this.AllowNull)
            {
                tbMinutes.BorderColor = Color.Red;
                tbMinutes.BorderWidth = Unit.Pixel(1);
            }
		}

		private System.Web.UI.WebControls.TextBox GetHourBox()
		{
			System.Web.UI.WebControls.TextBox result = null;
			Boolean firstFound = true; // True, we need the first
			foreach (Control ctrl in this.Controls)
			{

				// TODO: Optimize, no loop but direct call                    
				if (ctrl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
				{
					if (firstFound)
					{
						result = (System.Web.UI.WebControls.TextBox)ctrl;
						break;
					}
					else
					{
						firstFound = true;
					}
				}
			}
			return result;
		}

		public System.Web.UI.WebControls.TextBox HourBox
		{
			get
			{
				return this.GetHourBox();
			}
		}

		private System.Web.UI.WebControls.TextBox GetMinuteBox()
		{
			System.Web.UI.WebControls.TextBox result = null;
			Boolean firstFound = false; // Flase we need second
			foreach (Control ctrl in this.Controls)
			{

				// TODO: Optimize, no loop but direct call                    
				if (ctrl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
				{
					if (firstFound)
					{
						result = (System.Web.UI.WebControls.TextBox)ctrl;
						break;
					}
					else
					{
						firstFound = true;
					}
				}
			}
			return result;
		}

		public System.Web.UI.WebControls.TextBox MinuteBox
		{
			get
			{
				return this.GetMinuteBox();
			}
		}

		#region IBindable Required

		private bool useDataBinding = true;

		/// <summary>
		/// Gets or sets the flag indicating whether databinding should be used
		/// </summary>
		[Browsable(true)]
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		/// <summary>
		/// Value of the CheckBox.Checked as boolean
		/// </summary>
		public DateTime? Value
		{
			get
			{
				return this.SelectedTime;
			}
			set
			{
				this.SelectedTime = value;
			}
		}

		/// <summary>
		/// Helper method to fire up the value changed event
		/// </summary>
		/// <param name="sender">The sender</param>
		/// <param name="e">The EventArgs</param>
		public void OnValueChanged(object sender, EventArgs e)
		{
			if (this.ValueChanged != null)
			{
				this.ValueChanged(sender, e);
			}
		}

		void TimePicker_TextChanged(object sender, EventArgs e)
		{
			this.OnValueChanged(sender, e);
		}

		/// <summary>
		/// Occurs when the value changed between posts to the server
		/// </summary>
		public event EventHandler ValueChanged;

		#endregion



	}
}
