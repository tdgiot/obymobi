using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is used for displaying button fields
    /// </summary>
    public class ButtonField : System.Web.UI.WebControls.ButtonField, ITranslatable
    {
        #region Fields

        private string confirmationMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ButtonField class
        /// </summary>	
        public ButtonField()
        {
            this.HookUpEvents();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Attach internal events to the ButtonField
        /// </summary>
        private void HookUpEvents()
        {            
        }

        #endregion

        #region Properties

        /// <summary>
        /// Message to popup in Java to confirm this button's action. (The D.W.GridView contains the logic for binding the JavaScript)
        /// </summary>
        public string ConfirmationMessage 
        {
            get
            {
                return this.confirmationMessage;
            }
            set
            {
                this.confirmationMessage = value;
            }
        }

        #endregion

        #region Event handlers



        #endregion
    }
}
