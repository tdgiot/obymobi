using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is used for displaying data bounded columns
    /// </summary>
    public class BoundColumn : System.Web.UI.WebControls.BoundColumn, ITranslatable
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the BoundColumn class
        /// </summary>	
        public BoundColumn()
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text of the bound field
        /// </summary>
        public string Text
        {
            get
            {
                return this.HeaderText;
            }
            set
            {
                this.HeaderText = value;                
            }
        }

        #endregion

        #region Event handlers



        #endregion
    }
}
