﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Reflection;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a web control for specifying the page size
    /// </summary>
    public class PageSizer : ListControl
    {
        #region Fields

        private string startText = string.Empty;
        private string endText = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls.PageSizer type
        /// </summary>
        public PageSizer()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Renders the control to the specified HTML Writer
        /// </summary>
        /// <param name="writer">The HtmlTextWriter object that receives the control content</param>
        protected override void Render(HtmlTextWriter writer)
        {
            //base.Render(writer);

            if (this.Items.Count == 0)
            {
                // No items to display
            }
            else
            {
                int currentPageSize = -1;
                if (this.Page is PageEntityCollection)
                {
                    currentPageSize = (this.Page as PageEntityCollection).PagingPageSize;
                }

                Table pageSizer = new Table();
                pageSizer.CellPadding = 0;
                pageSizer.CellSpacing = 0;
                pageSizer.CssClass = this.CssClass;

                TableRow pageSizerRow = new TableRow();

                if (this.startText.Length > 0)
                {
                    TableCell tableCellStartText = new TableCell();
                    tableCellStartText.Text = this.startText;

                    pageSizerRow.Cells.Add(tableCellStartText);
                }

                for (int i = 0; i < this.Items.Count; i++)
                {
                    TableCell tableCellPageSize = new TableCell();

                    if (this.Items[i].Value == currentPageSize.ToString())
                    {
                        Label lblPageSize = new Label();
                        lblPageSize.CssClass = "activePage";
                        lblPageSize.Text = this.Items[i].Value;
                        tableCellPageSize.Controls.Add(lblPageSize);
                    }
                    else
                    {
                        HyperLink hlPageSize = new HyperLink();
                        hlPageSize.CssClass = "pageSize";
                        hlPageSize.Text = this.Items[i].Value;

                        QueryStringHelper qs = new QueryStringHelper();
                        qs.AddItem("PageSize", this.Items[i].Value);

                        hlPageSize.NavigateUrl = qs.MergeQuerystringWithRawUrl(); // This make sure other selections in the query string are also preserved
                        tableCellPageSize.Controls.Add(hlPageSize);
                    }
                    pageSizerRow.Cells.Add(tableCellPageSize);
                }

                if (this.endText.Length > 0)
                {
                    TableCell tableCellEndText = new TableCell();
                    tableCellEndText.Text = this.endText;

                    pageSizerRow.Cells.Add(tableCellEndText);
                }

                pageSizer.Rows.Add(pageSizerRow);
                pageSizer.RenderControl(writer);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text which is being displayed before the page size items
        /// </summary>
        public string StartText
        {
            get
            {
                return this.startText;
            }
            set
            {
                this.startText = value;
            }
        }

        /// <summary>
        /// Gets or sets the text which is being displayed after the page size items
        /// </summary>
        public string EndText
        {
            get
            {
                return this.endText;
            }
            set
            {
                this.endText = value;
            }
        }

        #endregion
    }
}
