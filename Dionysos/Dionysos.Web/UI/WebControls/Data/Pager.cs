﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// The way the pager renders it's items
	/// </summary>
	public enum PagerRenderStyle
	{
		/// <summary>
		/// Render using Tables (original)
		/// </summary>
		Tables,
		/// <summary>
		/// Render using hrefs in a P tag
		/// </summary>
		HyperlinksInParagraph
	}

	public enum PagerNextPreviousStyle
	{
		/// <summary>
		/// Display dots at start/end of pager to go outside it's currently displayed boundaries
		/// </summary>
		Dots,
		/// <summary>
		/// Always display the first and last page number at the begin and end of page.
		/// </summary>
		AlwaysDisplayFirstAndLast
	}

	public enum PagerOutsideBoundariesStyle
	{
		/// <summary>
		/// Show the Current Page as the middle one (if page 12 is selected with buttoncount 10 you get: 8 9 10 11 *12* 13 14 15 16 17))
		/// </summary>
		CurrentPageCentered,
		/// <summary>
		/// Original behaviour (if page 12 is selected with buttoncount 10 you get: 10 11 *12* 13 14 15 16 17 18)
		/// </summary>
		PerRange
	}

	public class Pager : WebControl
	{
		#region Fields

		private PagerRenderStyle renderStyle = PagerRenderStyle.Tables;
		private int pageButtonCount = 10;
		private int pageSize = -1;
		private int currentPageIndex = -1;
		private int itemCount = -1;
		private bool displayForSinglePage = false;
		private string cssClass = "PagerTable";
		private string previousPageText = string.Empty;
		private string nextPageText = string.Empty;
		private PagerNextPreviousStyle pagerNextPrevStyle = PagerNextPreviousStyle.Dots;
		private PagerOutsideBoundariesStyle pagerOutsideBoundaryStyle = PagerOutsideBoundariesStyle.PerRange;
		protected StringBuilder htmlOutput;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Dionysos.Web.UI.WebControls.Pager class
		/// </summary>
		public Pager()
		{
			htmlOutput = new StringBuilder();
		}

		/// <summary>
		/// Constructs an instance of the Dionysos.Web.UI.WebControls.Pager class
		/// </summary>
		/// <param name="pagedDataSource">The PagedDataSource instance to create the pager for</param>
		public Pager(PagedDataSource pagedDataSource)
		{
			this.pageSize = pagedDataSource.PageSize;
			this.currentPageIndex = pagedDataSource.CurrentPageIndex;
			this.itemCount = pagedDataSource.Count;
			this.HookUpEvents();
		}

		#endregion

		#region Methods

		protected void HookUpEvents()
		{

		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			base.Render(writer);

			if (itemCount <= pageSize && !this.DisplayForSinglePage)
			{
				// Render nothing, it's 1 page and properties says it shouldn't render on 
			}
			else if (pageSize > 0 && CurrentPageIndex >= 0 && itemCount >= 0)
			{
				this.RenderBeginOfPager();

				int pages = (int)System.Math.Ceiling((double)((double)this.itemCount / (double)this.pageSize));

				// generate previous page control, if not first & it's filled in
				if (CurrentPageIndex != 0 && this.PreviousPageText.Length > 0)
				{
					this.RenderPagerItem(CurrentPageIndex - 1, this.PreviousPageText, false);
				}

				if (pages <= this.pageButtonCount)
				{
					// We can display all numbers (1 till last)                    
					for (int i = 0; i < pages; i++)
					{
						if (i != this.CurrentPageIndex)
						{
							this.RenderPagerItem(i, string.Empty, false);
						}
						else
						{
							this.RenderPagerItem(i, string.Empty, true);
						}
					}
				}
				else
				{
					// We have a PageButtonCount, we know the current page and put that in the middle if possible
					int buttonsInFrontOfCurrentIndex = (int)System.Math.Floor((double)((this.pageButtonCount - 1) / 2d));
					int buttonsBehindCurrentIndex = (int)System.Math.Ceiling((double)((this.pageButtonCount - 1) / 2d));

					// Now we check if we can have the preffered button count in front of it.
					// If not add the deducation to the buttons after.
					if (buttonsInFrontOfCurrentIndex > CurrentPageIndex)
					{
						buttonsBehindCurrentIndex += (buttonsInFrontOfCurrentIndex - CurrentPageIndex);
						buttonsInFrontOfCurrentIndex = CurrentPageIndex;
					}

					if ((pages - CurrentPageIndex) < buttonsBehindCurrentIndex)
					{
						// Need need extra buttons in front
						buttonsInFrontOfCurrentIndex += (buttonsBehindCurrentIndex - (pages - CurrentPageIndex));
						buttonsBehindCurrentIndex = (pages - CurrentPageIndex);
					}

					// If we can't display all at once numbers "pages > this.pageButtonCount"
					int start = currentPageIndex - buttonsInFrontOfCurrentIndex;

					if (start != 0)
					{
						// We render page 1 if we need to show first page always
						if (this.pagerNextPrevStyle == PagerNextPreviousStyle.AlwaysDisplayFirstAndLast)
						{
							this.RenderPagerItem(0, string.Empty, false);
						}

						// Render the dots which go to a page we don't show now
						int previousPageNo = start - 1;
						this.RenderPagerItem(previousPageNo, "...", false);
					}

					int lastPageIndexLink = -1;
					for (int i = start; i < (start + this.pageButtonCount) && i < pages; i++)
					{
						if (i != (this.CurrentPageIndex))
						{
							this.RenderPagerItem(i, string.Empty, false);
						}
						else
						{
							this.RenderPagerItem(i, string.Empty, true);
						}
					}

					if (lastPageIndexLink + 1 != pages)
					{
						// Two types of paging: show dots or show last page number
						int nextPageNo = start + this.pageButtonCount;

						this.RenderPagerItem(nextPageNo, "...", false);

						if (this.PagerNextPrevStyle == PagerNextPreviousStyle.AlwaysDisplayFirstAndLast)
						{
							this.RenderPagerItem(pages - 1, pages.ToString(), false);
						}
					}
				}

				// generate "next page" link, if not last
				if (CurrentPageIndex + 1 < pages && this.NextPageText.Length > 0)
				{
					this.RenderPagerItem(CurrentPageIndex + 1, this.NextPageText, false);
				}

				this.RenderEndOfPager();
			}
			else
			{
				LiteralControl literal = new LiteralControl();
				literal.Text = "[PAGER FAILED]";
				literal.RenderControl(writer);
			}

			writer.Write(this.htmlOutput.ToString());
		}

		void RenderBeginOfPager()
		{
			if (this.RenderStyle == PagerRenderStyle.Tables)
			{
				this.htmlOutput.Append(string.Format("<table class=\"{0}\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tr>", this.CssClass));
			}
			else
			{
				this.htmlOutput.Append(string.Format("<p class=\"{0}\">", this.CssClass));
			}
		}

		protected virtual void RenderPagerItem(int page, string customText, bool activeItem)
		{
			string text;
			if (customText.Length > 0)
				text = customText;
			else
			{
				int pageText = page + 1;
				text = pageText.ToString();
			}

			string href;
			QueryStringHelper qs = new QueryStringHelper();
			qs.AddItem("Page", page);
			href = qs.MergeQuerystringWithRawUrl();

			if (this.RenderStyle == PagerRenderStyle.Tables)
			{
				if (activeItem)
					this.htmlOutput.Append(string.Format("<td><span>{1}</span></td>", href, text));
				else
					this.htmlOutput.Append(string.Format("<td><a href=\"{0}\">{1}</a></td>", href, text));
			}
			else
			{
				if (activeItem)
					this.htmlOutput.Append(string.Format("<td><a href=\"{0}\" class=\"active\">{1}</a></td>", href, text));
				else
					this.htmlOutput.Append(string.Format("<td><a href=\"{0}\">{1}</a></td>", href, text));
			}
		}

		void RenderEndOfPager()
		{
			if (this.RenderStyle == PagerRenderStyle.Tables)
			{
				this.htmlOutput.Append("</tr></table>");
			}
			else
			{
				this.htmlOutput.Append("</p>");
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the page button count
		/// </summary>
		public int PageButtonCount
		{
			get
			{
				return this.pageButtonCount;
			}
			set
			{
				this.pageButtonCount = value;
			}
		}


		/// <summary>
		/// Gets or sets the PropertyName
		/// </summary>
		public string PropertyName
		{
			get
			{
				return this.PropertyName;
			}
			set
			{
				this.PropertyName = value;
			}
		}


		/// <summary>
		/// Gets or sets the css class of the pager
		/// </summary>
		public new string CssClass
		{
			get
			{
				return this.cssClass;
			}
			set
			{
				this.cssClass = value;
			}
		}

		/// <summary>
		/// Gets or sets ItemCount of the datasource 
		/// </summary>
		public int ItemCount
		{
			get
			{
				return this.itemCount;
			}
			set
			{
				this.itemCount = value;
			}
		}

		/// <summary>
		/// Gets or sets CurrentPageIndex 
		/// </summary>
		public int CurrentPageIndex
		{
			get
			{
				// Try to get from query string
				int page;
				if (QueryStringHelper.TryGetValue("Page", out page))
				{
					return page;
				}
				else
				{
					return this.currentPageIndex;
				}
			}
			set
			{
				this.currentPageIndex = value;
			}
		}

		/// <summary>
		/// Gets or sets PageSize 
		/// </summary>
		public int PageSize
		{
			get
			{
				return this.pageSize;
			}
			set
			{
				this.pageSize = value;
			}
		}

		/// <summary>
		/// Indicates if the Pager should be rendered when only 1 page is available
		/// </summary>
		public bool DisplayForSinglePage
		{
			get
			{
				return this.displayForSinglePage;
			}
			set
			{
				this.displayForSinglePage = value;
			}
		}

		/// <summary>
		/// Text to display for a link to the Previous Page
		/// </summary>
		public string PreviousPageText
		{
			get
			{
				return this.previousPageText;
			}
			set
			{
				this.previousPageText = value;
			}
		}

		/// <summary>
		/// Text to display for a link to the Next Page
		/// </summary>
		public string NextPageText
		{
			get
			{
				return this.nextPageText;
			}
			set
			{
				this.nextPageText = value;
			}
		}

		/// <summary>
		/// Gets/Sets the logic type of Next and Previous buttons
		/// </summary>
		public PagerNextPreviousStyle PagerNextPrevStyle
		{
			get
			{
				return this.pagerNextPrevStyle;
			}
			set
			{
				this.pagerNextPrevStyle = value;
			}
		}

		/// <summary>
		/// Gets/Sets the logic for the outside boundary style
		/// </summary>
		public PagerOutsideBoundariesStyle PagerOutsideBoundaryStyle
		{
			get
			{
				return this.pagerOutsideBoundaryStyle;
			}
			set
			{
				this.pagerOutsideBoundaryStyle = value;
			}
		}

		/// <summary>
		/// Gets or sets the RenderStyle
		/// </summary>
		public PagerRenderStyle RenderStyle
		{
			get
			{
				return this.renderStyle;
			}
			set
			{
				this.renderStyle = value;
			}
		}


		//void  Page_PreRender(object sender, EventArgs e)
		//{
		//    if(this.Page is PageEntityCollection)
		//    {
		//        PageEntityCollection pageC = this.Page as PageEntityCollection;
		//        this.currentPageIndex = pageC.PagingCurrentPage;
		//        this.pageSize = pageC.PagingPageSize;
		//        if(pageC.PagingItemCount.HasValue)
		//            this.itemCount = pageC.PagingItemCount.Value;
		//    }
		//}

		#endregion
	}
}
