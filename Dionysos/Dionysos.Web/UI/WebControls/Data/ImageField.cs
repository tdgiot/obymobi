using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is used for displaying button fields
    /// </summary>
    public class ImageField : System.Web.UI.WebControls.ImageField, ITranslatable
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ImageField class
        /// </summary>	
        public ImageField()
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text
        /// </summary>
        public string Text
        {
            get
            {
                return this.HeaderText;
            }
            set
            {
                this.HeaderText = value;
            }
        }

        #endregion

        #region Event handlers



        #endregion
    }
}
