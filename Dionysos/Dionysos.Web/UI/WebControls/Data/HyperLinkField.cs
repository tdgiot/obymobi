using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is used for displaying button fields
    /// </summary>
    public class HyperLinkField : System.Web.UI.WebControls.HyperLinkField, ITranslatable
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the HyperLinkField class
        /// </summary>	
        public HyperLinkField()
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties

        #endregion

        #region Event handlers



        #endregion
    }
}
