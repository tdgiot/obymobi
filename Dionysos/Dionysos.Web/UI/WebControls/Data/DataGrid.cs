using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using System.ComponentModel.Design;
using System.ComponentModel;

namespace Dionysos.Web.UI.WebControls
{
    public class DataGrid : System.Web.UI.WebControls.DataGrid
    {
        #region Fields & Properties

        private bool EvenRow;

        /// <summary>
        /// Enable/Disable Localization for "Text" when the Parent page has localization
        /// </summary>
        [Category("Localization"), DefaultValue(true)]
        public bool LocalizeHeadings
        {
            get
            {
                if (ViewState["LocalizeHeadings"] != null)
                    return (bool)ViewState["LocalizeHeadings"];
                else
                    return true;
            }
            set { ViewState["LocalizeHeadings"] = value; }
        }

        public int DeleteColumn
        {
            get
            {
                if (ViewState["DeleteColumn"] != null)
                    return (int)ViewState["DeleteColumn"];
                else
                    return -1;
            }
            set { ViewState["DeleteColumn"] = value; }
        }

        /// <summary>
        /// This gets or sets a boolean value indicating if the defaults of Today-IT should be used. (default = true)
        /// In this case: Styling for rows will be overriden with CssClass: dgoddrow, dgevenrow, dgheader
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(true)]
        public bool UseTodayITDefaults
        {
            get
            {
                if (ViewState["UseTodayITDefaults"] != null)
                    return (bool)ViewState["UseTodayITDefaults"];
                else
                    return true;
            }
            set
            {
                ViewState["UseTodayITDefaults"] = value;
            }
        }

        public string CssClassOddRow
        {
            get
            {
                if (ViewState["CssClassOddRow"] != null)
                    return (string)ViewState["CssClassOddRow"];
                else
                    return "dgoddrow";
            }
            set { ViewState["CssClassOddRow"] = value; }
        }

        public string CssClassEvenRow
        {
            get
            {
                if (ViewState["CssClassEvenRow"] != null)
                    return (string)ViewState["CssClassEvenRow"];
                else
                    return "dgevenrow";
            }
            set { ViewState["CssClassEvenRow"] = value; }
        }
        public string CssClassHeadRow
        {
            get
            {
                if (ViewState["CssClassHeadRow"] != null)
                    return (string)ViewState["CssClassHeadRow"];
                else
                    return "dgheadrow";
            }
            set { ViewState["CssClassHeadRow"] = value; }
        }

        #endregion

        #region Methods

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);            

            this.ItemCreated += new DataGridItemEventHandler(LocalizedDataGrid_ItemCreated);
            this.ItemDataBound += new DataGridItemEventHandler(LocalizedDataGrid_ItemDataBound);
            this.PreRender += new EventHandler(LocalizedDataGrid_PreRender);
        }

        private void LocalizedDataGrid_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (this.UseTodayITDefaults)
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (EvenRow)
                        e.Item.CssClass = this.CssClassEvenRow;
                    else
                        e.Item.CssClass = this.CssClassOddRow;

                    EvenRow = !EvenRow;
                }
                else if (e.Item.ItemType == ListItemType.Header)
                {
                    e.Item.CssClass = this.CssClassHeadRow;
                }
            }
        }

        private void LocalizedDataGrid_PreRender(object sender, EventArgs e)
        {
            this.CellPadding = 3;
            this.BorderWidth = 0;
            this.BorderStyle = 0;
            this.Width = Unit.Parse("100%");
            this.GridLines = GridLines.None;

            string javaScript = "";
            if (this.DeleteColumn > -1)
            {
                javaScript += "<script language=\"JavaScript\"> ";
                javaScript += "	function dgdc() ";
                javaScript += "	{  ";
                javaScript += "	  return confirm_delete('Are you sure want to delete this item?')";
                javaScript += "	}";
                javaScript += "	</script>";
                
                
                if (!this.Page.ClientScript.IsClientScriptBlockRegistered("dgDeleteConfirm"))
                    this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(),"dgDeleteConfirm", javaScript);

                string confirmJs = "<script language=\"JavaScript\">function confirm_delete(sMessage)";
			    confirmJs += @"{
			                if (confirm(sMessage)==true)
				                return true;
			                else
				                return false;
			                }</script>";
                if (!this.Page.ClientScript.IsClientScriptBlockRegistered("confirm_delete"))
                    this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(),"confirm_delete", confirmJs);
            }




            //this.HeaderStyle.Font.Bold = true;
            //this.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(224,224,224);
        }

        private void LocalizedDataGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            // GK TODO: Optimize, only 1 JAVAmessage and the cal lfor it
            if (this.DeleteColumn > -1)
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    ImageButton lb = (ImageButton)e.Item.Cells[this.DeleteColumn].FindControl("delBut");
                    if(lb != null)
                        lb.Attributes.Add("onclick", "return dgdc();");
                }
            }                               
        }

        #endregion

	}    
}
