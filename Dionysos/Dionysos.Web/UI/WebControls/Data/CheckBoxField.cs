using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is used for displaying button fields
    /// </summary>
    public class CheckBoxField : System.Web.UI.WebControls.CheckBoxField, ITranslatable
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the CheckBoxField class
        /// </summary>	
        public CheckBoxField()
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties

        #endregion

        #region Event handlers



        #endregion
    }
}
