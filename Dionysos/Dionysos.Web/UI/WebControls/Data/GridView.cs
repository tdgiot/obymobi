using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Reflection;
using Dionysos.Interfaces;
using Dionysos.Presentation;

namespace Dionysos.Web.UI.WebControls
{

	#region Cool to be implemented feature, grouping left column
	/*
	 *         private void SpanCellsRecursive(int columnIndex, int startRowIndex, int endRowIndex)
		{
			if (columnIndex >= this.GroupingDepth || columnIndex >= this.Columns.Count )
				return;

			TableCell groupStartCell = null;
			int groupStartRowIndex = startRowIndex;

			for (int i = startRowIndex; i < endRowIndex; i++)
			{
				TableCell currentCell = this.Rows[i].Cells[columnIndex];

				bool isNewGroup = (null == groupStartCell) || (0 != String.CompareOrdinal(currentCell.Text, groupStartCell.Text));

				if (isNewGroup)
				{
					if (null != groupStartCell)
					{
						SpanCellsRecursive(columnIndex + 1, groupStartRowIndex, i);
					}

					groupStartCell = currentCell;
					groupStartCell.RowSpan = 1;
					groupStartRowIndex = i;
				}
				else
				{
					currentCell.Visible = false;
					groupStartCell.RowSpan += 1;
				}
			}

			SpanCellsRecursive(columnIndex + 1, groupStartRowIndex, endRowIndex);
		}
	 * */
	#endregion

	/// <summary>
	/// Gui class used for displaying data collections
	/// </summary>
	public class GridView : System.Web.UI.WebControls.GridView, Dionysos.Interfaces.IDataGrid
	{
		#region Fields

		private bool manualColumns = false;
		private bool generateViewColumn = Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.ShowGridViewViewCommand);
		private bool generateEditColumn = true;
		private bool generateDeleteColumn = true;
		private bool showTitle = false;
		private string titleText = string.Empty;
		private string titleCssClass = "gridViewTitleHeader";
		private bool usePostBackSorting = false;
		int[] buttonFieldColumnIndexes = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the GridView class
		/// </summary>
		public GridView()
		{
			// Set the defaults
			this.AutoGenerateColumns = false;

			this.ShowFooter = false;
			this.GridLines = GridLines.None;
			this.CssClass = "datagrid";
			this.AlternatingRowStyle.CssClass = "datagrid-altrow";
			this.HeaderStyle.CssClass = "datagrid-header";
			this.SelectedRowStyle.CssClass = "datagrid-selected";
			this.AllowSorting = true;

			this.HookUpEvents();
		}

		#endregion

		#region Methods

		private void HookUpEvents()
		{
			this.RowDataBound += new GridViewRowEventHandler(GridView_RowDataBound);
			this.PreRender += new EventHandler(GridView_PreRender);
		}

		private void AssignDefaultSortExpressions()
		{
			for (int i = 0; i < this.Columns.Count; i++)
			{
				// Should some something with the shared parent class: DataControlField
				if (this.Columns[i] is System.Web.UI.WebControls.BoundField)
				{
					if (this.Columns[i].SortExpression.Length <= 0)
						this.Columns[i].SortExpression = ((System.Web.UI.WebControls.BoundField)this.Columns[i]).DataField;
				}
				else if (this.Columns[i] is System.Web.UI.WebControls.HyperLinkField)
				{
					if (this.Columns[i].SortExpression.Length <= 0)
						this.Columns[i].SortExpression = ((System.Web.UI.WebControls.HyperLinkField)this.Columns[i]).DataTextField;
				}
			}
		}

		/// <summary>
		/// Initializes the columns of the grid view instance based on the data column collection
		/// for the entity bound to the grid view
		/// </summary>
		/// <param name="entityName">The name of the entity to get the columns for</param>
		public virtual void InitializeColumns(string entityName)
		{
			if (Instance.ArgumentIsEmpty(entityName, "entityName"))
			{
				// Parameter 'entityName' is empty
			}
			else
			{
				if (this.generateDeleteColumn)
				{
					ButtonField deleteField = new ButtonField();
					deleteField.ButtonType = ButtonType.Image;
					deleteField.ConfirmationMessage = "Weet u zeker dat u het gekozen item wilt verwijderen?";
					deleteField.CommandName = "Delete";
					deleteField.Text = "Verwijderen";
					deleteField.ImageUrl = "~/Images/Icons/delete.png";
					deleteField.ItemStyle.Width = new Unit("20px");

					this.Columns.Insert(0, deleteField);
				}


				if (this.generateEditColumn)
				{
					ButtonField editField = new ButtonField();
					editField.ButtonType = ButtonType.Image;
					editField.CommandName = "Edit";
					editField.Text = "Wijzigen";
					editField.ImageUrl = "~/Images/Icons/pencil.png";
					editField.ItemStyle.Width = new Unit("20px");

					this.Columns.Insert(0, editField);
				}

				// DISABLED
				// Check whether the command columns should be generated for this gridview instance
				if (false && this.generateViewColumn)
				{
					ButtonField viewField = new ButtonField();
					viewField.ButtonType = ButtonType.Image;
					viewField.CommandName = "View";
					viewField.Text = "Bekijken";
					viewField.ImageUrl = "~/Images/Icons/ico_view.gif";
					viewField.ItemStyle.Width = new Unit("20px");

					this.Columns.Insert(0, viewField);
				}

				// Check whether the columns are automatically
				// being generated for this gridview instance
				// If not, create them manually
				if (this.manualColumns)
				{
					// Don't do any columns settings.
				}
				else if (!this.AutoGenerateColumns)
				{
					DataColumnCollection dataColumns = DataColumnUtil.GetDataColumnCollection(entityName);
					if (Instance.Empty(dataColumns))
					{
						throw new Dionysos.EmptyException("Variable 'dataColumns' is empty.");
					}
					else
					{
						for (int i = 0; i < dataColumns.Count; i++)
						{
							// Get a field from the field collection
							DataColumn dataColumn = dataColumns[i];

							// Create a bound field
							Dionysos.Web.UI.WebControls.BoundField column = new BoundField();
							column.DataField = dataColumn.Name;
							column.HeaderText = dataColumn.Name;

							this.Columns.Add(column);
						}
					}
				}

				this.AssignDefaultSortExpressions();
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Overrides the Generation of Columns, use when you manually want to manage the displayed columns
		/// </summary>
		public bool ManualColumns
		{
			get
			{
				return this.manualColumns;
			}
			set
			{
				this.manualColumns = value;
			}
		}

		/// <summary>
		/// Gets or sets a flag which indicates whether a view column should be generated for this gridview
		/// </summary>
		public bool GenerateViewColumn
		{
			get
			{
				return this.generateViewColumn;
			}
			set
			{
				this.generateViewColumn = value;
			}
		}

		/// <summary>
		/// Gets or sets a flag which indicates whether a edit column should be generated for this gridview
		/// </summary>
		public bool GenerateEditColumn
		{
			get
			{
				return this.generateEditColumn;
			}
			set
			{
				this.generateEditColumn = value;
			}
		}

		/// <summary>
		/// Gets or sets a flag which indicates whether a delete column should be generated for this gridview
		/// </summary>
		public bool GenerateDeleteColumn
		{
			get
			{
				return this.generateDeleteColumn;
			}
			set
			{
				this.generateDeleteColumn = value;
			}
		}

		/// <summary>
		/// Gets or sets a whether a GridView title should be rendered, is set to true when a value is assigned to TitleText
		/// </summary>
		public bool ShowTitle
		{
			get
			{
				return this.showTitle;
			}
			set
			{
				this.showTitle = value;
			}
		}

		/// <summary>
		/// Gets or sets the text to be displayed in the title, also sets ShowTitle to TRUE
		/// </summary>
		public string TitleText
		{
			get
			{
				return this.titleText;
			}
			set
			{
				this.titleText = value;
				if (this.titleText.Length > 0)
					this.showTitle = true;
			}
		}

		/// <summary>
		/// Gets or sets the cssclass for the title row
		/// </summary>
		public string TitleCssClass
		{
			get
			{
				return this.titleCssClass;
			}
			set
			{
				this.titleCssClass = value;
			}
		}

		/// <summary>
		/// Returns and int[] containing the indexes of the columns that are ButtonFields
		/// </summary>
		/// <returns></returns>
		private int[] ButtonFieldColumnIndexes
		{
			get
			{
				ArrayList indexes = new ArrayList();
				if (buttonFieldColumnIndexes == null)
				{
					for (int i = 0; i < this.Columns.Count; i++)
					{
						if (this.Columns[i] is ButtonField)
						{
							indexes.Add(i);
						}
					}

					this.buttonFieldColumnIndexes = new int[indexes.Count];

					for (int i = 0; i < indexes.Count; i++)
					{
						this.buttonFieldColumnIndexes[i] = (int)indexes[i];
					}
				}

				return buttonFieldColumnIndexes;
			}
		}

		/// <summary>
		/// Access the underlying table of the GridView
		/// </summary>
		protected Table InnerTable
		{
			get
			{
				if (this.HasControls())
				{
					return (Table)this.Controls[0];
				}

				return null;
			}
		}

		/// <summary>
		/// Use PostBack events for sorting (default is false)
		/// When false sorting commands are posted via the querystring
		/// </summary>
		public bool UsePageBackSorting
		{
			get
			{
				return this.usePostBackSorting;
			}
			set
			{
				this.usePostBackSorting = false;
			}
		}

		#endregion

		#region Event handlers

		private void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			// Attach Confirmation Messages to ButtonFields if needed
			#region Attach Confirmation Messages to ButtonFields if needed
			for (int i = 0; i < this.ButtonFieldColumnIndexes.Length; i++)
			{
				int index = this.buttonFieldColumnIndexes[i];

				// They should always have 1 control / but to make sure we loop
				ButtonField bfColumn = this.Columns[index] as ButtonField;
				if (!Instance.Empty(bfColumn.ConfirmationMessage))
				{
					for (int j = 0; j < e.Row.Cells[index].Controls.Count; j++)
					{
						ImageButton ibt = e.Row.Cells[index].Controls[j] as ImageButton;
						if (ibt != null)
						{
							// GK Is standard behviour of the ImageButton?
							// string confirmScript = string.Format("if(!confirm('{0}')){{return false;}}", bfColumn.ConfirmationMessage);
							// ibt.Attributes.Add("OnClick", confirmScript);
							ibt.PreSubmitWarning = bfColumn.ConfirmationMessage;
						}
						else
						{
							System.Web.UI.WebControls.ImageButton bt = e.Row.Cells[index].Controls[j] as System.Web.UI.WebControls.ImageButton;
							if (bt != null)
							{
								string confirmScript = string.Format("if(!confirm('{0}')){{return false;}}", bfColumn.ConfirmationMessage);
								bt.Attributes.Add("OnClick", confirmScript);
							}
						}
					}
				}
			}
			#endregion

			// Generate set Css for sorted and non-sorted columns + add links if !UsePageBackSorting
			#region Generate links for the column names to do sorting via the querystring

			string customSortExpression;
			if (!QueryStringHelper.TryGetValue("SortExp", out customSortExpression))
			{
				customSortExpression = this.SortExpression;
			}

			SortDirection customSortDirection;
			string sortDir;
			if (QueryStringHelper.TryGetValue("SortDir", out sortDir))
			{
				customSortDirection = sortDir.Equals("ASC", StringComparison.OrdinalIgnoreCase) ? SortDirection.Ascending : SortDirection.Descending;
			}
			else
			{
				customSortDirection = this.SortDirection;
			}

			GridView gridView = this;
			QueryStringHelper qs = null;
			if (e.Row.RowType == DataControlRowType.Header)
			{
				int sortedColumnIndex = -1;
				foreach (DataControlField field in gridView.Columns)
				{
					int columnId = gridView.Columns.IndexOf(field);

					// Check if it's the current sorted column
					if (field.SortExpression == customSortExpression)
					{
						sortedColumnIndex = gridView.Columns.IndexOf(field);
					}

					// Add hyperlink for sorting
					if (!this.UsePageBackSorting)
					{
						string text = field.HeaderText;
						string sort = field.SortExpression;

						// Check if we have a sort expression
						if (sort.Length > 0)
						{
							e.Row.Cells[columnId].Controls.Clear();

							qs = new QueryStringHelper();
							// Check if the current column is the sorted column
							if (sortedColumnIndex == columnId)
							{
								// Logic for the column that's currently sorted
								if (!String.IsNullOrEmpty(sortDir))
								{
									// Reverse sort direction
									qs.AddItem("SortDir", customSortDirection == SortDirection.Ascending ? "DESC" : "ASC");
								}
								else
								{
									qs.AddItem("SortDir", "ASC");
								}
							}
							else
							{
								// Normal column
								qs.AddItem("SortDir", "ASC");
								qs.AddItem("SortExp", sort);
							}

							Dionysos.Web.UI.WebControls.HyperLink hl = new HyperLink();
							hl.Text = text;
							hl.ToolTip = string.Format("Sorteren op {0}", text.ToLower());
							hl.NavigateUrl = qs.MergeQuerystringWithRawUrl();

							e.Row.Cells[columnId].Controls.Add(hl);
						}

					}
				}

				if (sortedColumnIndex > -1)
				{
					//  this is a header row,
					//  set the sort style
					if (customSortDirection == SortDirection.Ascending)
					{
						e.Row.Cells[sortedColumnIndex].CssClass += " sortedAsc";
					}
					else
					{
						e.Row.Cells[sortedColumnIndex].CssClass += " sortedDesc";
					}
				}
			}

			#endregion
		}

		private void GridView_PreRender(object sender, EventArgs e)
		{
			// Add a GridView title row above the Header
			if (this.ShowTitle)
			{
				GridViewRow row = new GridViewRow(0, -1, DataControlRowType.EmptyDataRow, DataControlRowState.Normal);
				TableCell cell = new TableCell();
				cell.CssClass = this.TitleCssClass;
				cell.ColumnSpan = this.Columns.Count;
				cell.Text = this.TitleText;
				row.Cells.Add(cell);
				this.InnerTable.Rows.AddAt(0, row);
			}
		}

		#endregion
	}
}
