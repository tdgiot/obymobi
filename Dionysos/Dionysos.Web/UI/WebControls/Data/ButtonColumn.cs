using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which is used for displaying button columns
    /// </summary>
    public class ButtonColumn : System.Web.UI.WebControls.ButtonColumn, ITranslatable
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ButtonColumn class
        /// </summary>	
        public ButtonColumn()
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties

        #endregion

        #region Event handlers



        #endregion
    }
}
