using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which is used for displaying data bounded fields
	/// </summary>
	public class BoundField : System.Web.UI.WebControls.BoundField, ITranslatable
	{
		#region Fields



		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the BoundField class
		/// </summary>	
		public BoundField()
		{
		}

		#endregion

		#region Methods



		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the text of the bound field
		/// </summary>
		public string Text
		{
			get
			{
				return this.HeaderText;
			}
			set
			{
				this.HeaderText = value;
			}
		}

		#endregion

		#region Event handlers



		#endregion
	}
}
