﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class using for displaying data collection based on templates
    /// </summary>
    public class DataList : System.Web.UI.WebControls.DataList
    {
        #region Fields

        private bool allowPaging = false;
        private int pageSize = 15;
        private int pageButtonCount = 10;
        private PagedDataSource pagedDataSource = null;
        private string pagerCssClass = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.DataList class
        /// </summary>
        public DataList()
        {
        }

        #endregion

        #region Methods

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!Instance.Empty(this.pagedDataSource))
            {
                Pager pager = new Pager(this.PagedDataSource);
                pager.PageButtonCount = this.pageButtonCount;
                pager.CssClass = this.pagerCssClass;
                pager.RenderControl(writer);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the flag whether paging is allowed
        /// </summary>
        public bool AllowPaging
        {
            get
            {
                return this.allowPaging;
            }
            set
            {
                this.allowPaging = value;
            }
        }

        /// <summary>
        /// Gets or sets the PagedDataSource instance of this DataList instance
        /// </summary>
        public PagedDataSource PagedDataSource
        {
            get
            {
                return this.pagedDataSource;
            }
            set
            {
                this.pagedDataSource = value;
            }
        }

        /// <summary>
        /// Gets or sets the pagesize
        /// </summary>
        public int PageSize
        {
            get
            {
                return this.pageSize;
            }
            set
            {
                this.pageSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the page button count
        /// </summary>
        public int PageButtonCount
        {
            get
            {
                return this.pageButtonCount;
            }
            set
            {
                this.pageButtonCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the index of the currently selected page
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (!Instance.Empty(this.pagedDataSource))
                {
                    return this.pagedDataSource.CurrentPageIndex;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                if (!Instance.Empty(this.pagedDataSource))
                {
                    this.pagedDataSource.CurrentPageIndex = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the css class of the pager
        /// </summary>
        public string PagerCssClass
        {
            get
            {
                return this.pagerCssClass;
            }
            set
            {
                this.pagerCssClass = value;
            }
        }

        #endregion

    }
}
