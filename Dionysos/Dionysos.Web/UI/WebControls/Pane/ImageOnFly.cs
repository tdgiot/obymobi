﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;

namespace Dionysos.Web.UI.WebControls
{
    public class ImageOnFly
    {
        #region Fields

        private string sourceFileName;
        private string errorDescription;
        private string destFileName;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls class
        /// </summary>
        /// <param name="filename">The path to the source file</param>
        public ImageOnFly(string filename)
        {
            this.sourceFileName = filename;
        }

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.WebControls class
        /// </summary>
        public ImageOnFly()
        {
        }

        #endregion

        #region Methods

        public string StartConvertTo(int ancho, int alto, string sufijo)
        {
            if ((SourceFileName != null) && (SourceFileName.Length > 0))
            {
                try
                {
                    Directory.SetCurrentDirectory(Directory.GetParent(SourceFileName).ToString());
                    //string[] dirs = Path.GetDirectoryName(SourceFileName).Split(new char[] { char.Parse(@"\") });                    
                    string resFileName = System.IO.Path.GetDirectoryName(SourceFileName) + @"\" + System.IO.Path.GetFileNameWithoutExtension(SourceFileName) + sufijo + System.IO.Path.GetExtension(SourceFileName);

                    //Rectangle rect = new Rectangle(0, 0, ancho, alto);
                    System.Drawing.Image original = System.Drawing.Image.FromFile(SourceFileName);
                    System.Drawing.Image imgResized = original.GetThumbnailImage(ancho, alto, null, IntPtr.Zero);
                    System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(imgResized);

                    if (!File.Exists(resFileName))
                    {
                        FileStream fs = File.Create(resFileName);
                        fs.Close();
                    }
                    bmp.Save(resFileName);

                    //resFileName = String.Format(@"~/{0}/{1}", dirs[dirs.Length - 1], Path.GetFileName(resFileName));

                    destFileName = resFileName;
                    bmp.Dispose();
                    return resFileName;
                }
                catch (Exception e)
                {
                    errorDescription = e.Message;
                    return null;
                }
            }
            else
            {
                errorDescription = "Path de la imagen fuente no establecida";
                return null;
            }
        }

        /// <summary>
        /// Borra la imagen creada on the fly para liberar los recursos.
        /// </summary>
        public void DeleteFileDest()
        {
            if ((DestFileName != null) && (DestFileName.Length > 0))
            {
                Directory.SetCurrentDirectory(Directory.GetParent(SourceFileName).ToString());
                string resFileName = System.IO.Path.GetDirectoryName(SourceFileName) + @"\" + System.IO.Path.GetFileNameWithoutExtension(SourceFileName) + "_tempIOF" + System.IO.Path.GetExtension(SourceFileName);
                if (File.Exists(resFileName))
                    File.Delete(resFileName);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///  Gets or sets the filename of the source file
        /// </summary>
        public string SourceFileName
        {
            get
            {
                return sourceFileName;
            }
            set
            {
                sourceFileName = value;
            }
        }

        /// <summary>
        /// Gets the description of the error
        /// </summary>
        public string ErrorDescription
        {
            get
            {
                return errorDescription;
            }
        }

        /// <summary>
        /// Gets or sets the filename of the destination file
        /// </summary>
        public string DestFileName
        {
            get
            {
                return destFileName;
            }
        }

        #endregion
    }
}
