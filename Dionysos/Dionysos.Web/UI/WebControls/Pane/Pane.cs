﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Collections;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.Drawing.Design;
using System.ComponentModel.Design;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a panel containing a heading and a content area
    /// </summary>
    [ParseChildren(true)]
    [Designer(typeof(PaneControlDesigner))]
    [ToolboxData("<{0}:Pane runat=server width=300px height=100px ></{0}:Pane>")]
    public class Pane : CompositeControl
    {
        #region Fields

        internal List<IAttributeAccessor> regions;
        private ImageOnFly imageOnFlyTitle = new ImageOnFly();
        private ImageOnFly imageOnFlyContent = new ImageOnFly();
        private ITemplate contentTemplate;

        private Style styleTitle;
        private Style styleContent;

        private Panel panelMain;
        private Panel panelTitle;
        private Panel panelContent;
        private Label labelTitle;
        private Label labelContent;

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            labelTitle = new Label();
            panelTitle = new Panel();
            labelContent = new Label();
            panelContent = new Panel();
            panelMain = new Panel();

            if (contentTemplate != null)
            {
                contentTemplate.InstantiateIn(panelContent);
            }

            panelMain.Controls.Add(panelTitle);
            panelMain.Controls.Add(panelContent);

            regions = new List<IAttributeAccessor>();
            regions.Add(panelContent);
            Controls.Add(panelMain);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            try
            {
                if (panelMain == null)
                    CreateChildControls();

                // Title label
                labelTitle.Width = Width;
                labelTitle.Text = TitleText;
                
                // Title panel
                panelTitle.Width = Width;
                panelTitle.Height = TitleHeight;
                panelTitle.Wrap = true;
                if ((ImageTitle != null) && (ImageTitle.Length > 0))
                {
                    string resStretch;
                    if (StretchImageTitle)
                    {
                        if (DesignMode)
                        {
                            panelTitle.BackImageUrl = ResolveClientUrl(ImageTitle);
                        }
                        else
                        {
                            imageOnFlyTitle.SourceFileName = HttpContext.Current.Server.MapPath(ImageTitle);
                            resStretch = imageOnFlyTitle.StartConvertTo(Convert.ToInt32(panelTitle.Width.Value), Convert.ToInt32(panelTitle.Height.Value), "_tempTitle");
                            if (resStretch != null)
                            {
                                panelTitle.BackImageUrl = "~/" + resStretch.Substring(Page.Server.MapPath(HttpContext.Current.Request.ApplicationPath).Length + 1);
                            }
                            else
                            {
                                panelTitle.BackImageUrl = ResolveClientUrl(ImageTitle);
                                writer.Write("imageOnFlyTitle Error: " + imageOnFlyTitle.ErrorDescription + "<br >");
                            }
                        }
                    }
                    else
                    {
                        panelTitle.BackImageUrl = ResolveClientUrl(ImageTitle);
                    }
                }
                else
                {
                    panelTitle.BackImageUrl = String.Empty;
                }
                panelTitle.Controls.Add(labelTitle);
                if (styleTitle != null)
                    panelTitle.ApplyStyle(StyleTitle);
                panelTitle.HorizontalAlign = HorizontalAlignTitle;

                // Content panel
                panelContent.Width = Width;
                panelContent.Height = Height;
                panelContent.Wrap = true;
                panelMain.Width = Width;
                panelMain.Height = Height;
                if ((ImageContent != null) && (ImageContent.Length > 0))
                {
                    string resStretch2;
                    if (StretchImageContent)
                    {
                        if (DesignMode)
                        {
                            panelContent.BackImageUrl = ResolveClientUrl(ImageContent);
                        }
                        else
                        {
                            imageOnFlyContent.SourceFileName = HttpContext.Current.Server.MapPath(ImageContent);
                            resStretch2 = imageOnFlyContent.StartConvertTo(Convert.ToInt32(panelMain.Width.Value), Convert.ToInt32(panelMain.Height.Value), "_tempContent");
                            if (resStretch2 != null)
                                panelContent.BackImageUrl = "~/" + resStretch2.Substring(Page.Server.MapPath(HttpContext.Current.Request.ApplicationPath).Length + 1);
                            else
                            {
                                panelContent.BackImageUrl = ResolveClientUrl(ImageContent);
                                writer.Write("imageOnFlyContent Error: " + imageOnFlyContent.ErrorDescription + "<br >");
                            }
                        }
                    }
                    else
                        panelContent.BackImageUrl = ResolveClientUrl(ImageContent);
                }
                else
                {
                    panelContent.BackImageUrl = String.Empty;
                }

                if (styleContent != null)
                    panelContent.ApplyStyle(StyleContent);

                panelContent.ScrollBars = ScrollBarsContent;
                panelContent.HorizontalAlign = HorizontalAlignContent;
                panelMain.RenderControl(writer);
            }
            catch (Exception e)
            {
                writer.Write("Render Error: <br >" + e.Message);
            }
        }

        protected override void LoadViewState(object savedState)
        {
            if (savedState == null)
            {
                base.LoadViewState(null);
                return;
            }
            else
            {
                object[] myState = (object[])savedState;
                if (myState.Length != 3)
                {
                    throw new ArgumentException("Invalid view state");
                }
                base.LoadViewState(myState[0]);
                ((IStateManager)StyleTitle).LoadViewState(myState[1]);
                ((IStateManager)StyleContent).LoadViewState(myState[2]);
            }
        }

        protected override object SaveViewState()
        {
            // Customized state management to save the state of styles.
            object[] myState = new object[3];

            myState[0] = base.SaveViewState();
            if (styleTitle != null)
                myState[1] = ((IStateManager)styleTitle).SaveViewState();
            if (styleContent != null)
                myState[2] = ((IStateManager)styleContent).SaveViewState();

            return myState;
        }

        protected override void TrackViewState()
        {
            // Customized state management to track the state 
            // of styles.
            base.TrackViewState();

            if (styleTitle != null)
                ((IStateManager)styleTitle).TrackViewState();
            if (styleContent != null)
                ((IStateManager)styleContent).TrackViewState();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the height of the title
        /// </summary>        
        [Category("Appearance"), Bindable(true), DefaultValue("25px"), Description("Height of the heading")]
        public Unit TitleHeight
        {
            get
            {
                object o = ViewState["TitleHeight"];
                return (o == null) ? Unit.Parse("20px") : (Unit)o;
            }
            set { ViewState["TitleHeight"] = value; }
        }

        /// <summary>
        /// Gets or sets the title text of the pane
        /// </summary>        
        [Category("Appearance"), Bindable(true), DefaultValue("Title"), Description("Title of the pane")]
        public string TitleText
        {
            get
            {
                object o = ViewState["TitleText"];
                return (o == null) ? "Title of the pane" : (string)o;
            }
            set
            {
                ViewState["TitleText"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the scrollbar mode for the content region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(ScrollBars.None), Description("The scrollbar mode for the content region of the pane")]
        public ScrollBars ScrollBarsContent
        {
            get
            {
                object o = ViewState["ScrollBarsContent"];
                return (o == null) ? ScrollBars.None : (ScrollBars)ViewState["ScrollBarsContent"];
            }
            set { ViewState["ScrollBarsContent"] = value; }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment for the content region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(HorizontalAlign.NotSet), Description("The horizontal alignment for the content region of the pane")]
        public HorizontalAlign HorizontalAlignContent
        {
            get
            {
                object o = ViewState["HorizontalAlignContent"];
                return (o == null) ? HorizontalAlign.NotSet : (HorizontalAlign)ViewState["HorizontalAlignContent"];
            }
            set { ViewState["HorizontalAlignContent"] = value; }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment for the title region of the pane
        /// </summary>        
        [Category("Layout"), DefaultValue(HorizontalAlign.NotSet), Description("The horizontal alignment for the title region of the pane")]
        public HorizontalAlign HorizontalAlignTitle
        {
            get
            {
                object o = ViewState["HorizontalAlignTitle"];
                return (o == null) ? HorizontalAlign.NotSet : (HorizontalAlign)ViewState["HorizontalAlignTitle"];
            }
            set { ViewState["HorizontalAlignTitle"] = value; }
        }

        /// <summary>
        /// Gets or sets the url of the image for the title region of the pane
        /// </summary>        
        [Category("Images"), Bindable(true), DefaultValue(""), Editor(typeof(PaneImageUrlEditor), typeof(UITypeEditor)), Description("The url of the image for the title region of the pane")]
        public String ImageTitle
        {
            get
            {
                object o = ViewState["ImageTitle"];
                return (o == null) ? String.Empty : (string)o;
            }
            set
            {
                ViewState["ImageTitle"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the url of the image for the content region of the pane
        /// </summary>        
        [Category("Images"), Bindable(true), DefaultValue(""), Editor(typeof(PaneImageUrlEditor), typeof(UITypeEditor)), Description("The url of the image for the content region of the pane"),]
        public String ImageContent
        {
            get
            {
                object o = ViewState["ImageContent"];
                return (o == null) ? String.Empty : (string)o;
            }
            set { ViewState["ImageContent"] = value; }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether the title image may be stretched
        /// </summary>        
        [Category("Images"), Bindable(true), DefaultValue(false), Description("The flag which indicates whether the title image may be stretched")]
        public bool StretchImageTitle
        {
            get
            {
                object o = ViewState["StretchImageTitle"];
                return (o == null) ? false : (bool)o;
            }
            set { ViewState["StretchImageTitle"] = value; }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether the content image may be stretched
        /// </summary>        
        [Category("Images"), Bindable(true), DefaultValue(false), Description("The flag which indicates whether the content image may be stretched")]
        public bool StretchImageContent
        {
            get
            {
                object o = ViewState["StretchImageContent"];
                return (o == null) ? false : (bool)o;
            }
            set { ViewState["StretchImageContent"] = value; }
        }

        [Browsable(false)]
        [MergableProperty(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(Pane))]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate ContentTemplate
        {
            get
            {
                return contentTemplate;
            }
            set
            {
                contentTemplate = value;
            }
        }

        [Category("Appearance"), DefaultValue(null), PersistenceMode(PersistenceMode.InnerProperty), Description("Style of the title area"),]
        public virtual Style StyleTitle
        {
            get
            {
                if (styleTitle == null)
                {
                    styleTitle = new Style();
                    if (IsTrackingViewState)
                        ((IStateManager)styleTitle).TrackViewState();
                }
                return styleTitle;
            }
        }

        [Category("Appearance"), DefaultValue(null), PersistenceMode(PersistenceMode.InnerProperty), Description("Style of the content area"),]
        public virtual Style StyleContent
        {
            get
            {
                if (styleContent == null)
                {
                    styleContent = new Style();
                    if (IsTrackingViewState)
                        ((IStateManager)styleContent).TrackViewState();
                }
                return styleContent;
            }
        }

        #endregion
    }

    /// <summary>
    /// Design class which is being used at design time for Pane instances 
    /// </summary>    
    public class PaneControlDesigner : CompositeControlDesigner
    {
        #region Fields

        private Pane pane;
        private int currentRegion = -1;
        private int nbRegions = 0;

        #endregion

        #region Methods

        public override void Initialize(IComponent component)
        {
            pane = (Pane)component;
            base.Initialize(component);
            SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
            SetViewFlags(ViewFlags.TemplateEditing, true);
        }

        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                TemplateGroupCollection collection = new TemplateGroupCollection();
                TemplateGroup group = new TemplateGroup("ContentTemplate");
                TemplateDefinition definition = new TemplateDefinition(this, "ContentTemplate", pane, "ContentTemplate", false);
                group.AddTemplateDefinition(definition);
                collection.Add(group);
                return collection;
            }
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            if (pane.regions != null)
            {
                nbRegions = pane.regions.Count;
                for (int i = 0; i < nbRegions; i++)
                {
                    pane.regions[i].SetAttribute(DesignerRegion.DesignerRegionAttributeName, i.ToString());
                }
            }
        }

        public override string GetDesignTimeHtml(DesignerRegionCollection regions)
        {
            this.CreateChildControls();

            for (int i = 0; i < nbRegions; i++)
            {
                DesignerRegion r;
                if (currentRegion == i)
                    r = new EditableDesignerRegion(this, i.ToString());
                else
                    r = new DesignerRegion(this, i.ToString());
                regions.Add(r);
            }

            if ((currentRegion >= 0) && (currentRegion < nbRegions))
                regions[currentRegion].Highlight = true;
            return base.GetDesignTimeHtml(regions);
        }

        protected override void OnClick(DesignerRegionMouseEventArgs e)
        {
            base.OnClick(e);
            currentRegion = -1;
            if (e.Region != null)
            {
                for (int i = 0; i < nbRegions; i++)
                {
                    if (e.Region.Name == i.ToString())
                    {
                        currentRegion = i;
                        break;
                    }
                }
                UpdateDesignTimeHtml();
            }
        }

        public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
        {
            IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
            if (host != null)
            {
                ITemplate contentTemplate;
                if (currentRegion == 0)
                {
                    contentTemplate = pane.ContentTemplate;
                    return ControlPersister.PersistTemplate(contentTemplate, host);
                }
            }
            return String.Empty;
        }

        public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
        {
            if (content == null)
                return;
            IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
            if (host != null)
            {
                ITemplate template = ControlParser.ParseTemplate(host, content);
                if (template != null)
                {
                    if (currentRegion == 0)
                    {
                        pane.ContentTemplate = template;
                    }
                }
            }
        }

        #endregion
    }

    public class PaneImageUrlEditor : ImageUrlEditor
    {
        protected override string Caption
        {
            get
            {
                return "Select the image of the Pane";
            }
        }
    }
}
