using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
    /// <summary>
    /// Gui class which represents a subpanel which contains a entity collection
    /// </summary>
    public abstract class SubPanelEntityCollection : SubPanelOnGuiEntitySimple, ISubPanelEntityCollection
    {
        #region Fields

        private string entityName = string.Empty;
        private string relatedEntityIdField = string.Empty;
        private object dataSource = null;
        private Dionysos.Web.UI.WebControls.GridView mainGridView;
        private Dionysos.Web.UI.WebControls.Repeater mainRepeater;
        private Dionysos.Web.UI.WebControls.DataList mainDataList;
        private string entityPageUrl = string.Empty;
        private bool hideEditDeleteViewButtonsFromGrid = false;
        private bool allowGridViewSorting = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the SubPanelEntityCollection class
        /// </summary>
        public SubPanelEntityCollection()
        {
            // Hookup the event
            this.HookupEvents();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Hookup the events to the corresponding event handlers
        /// </summary>
        private void HookupEvents()
        {
            this.Load += new EventHandler(SubPanelEntityCollection_Load);
        }

        /// <summary>
        /// Init the panel which also initializes the entity if the EntityId is available, otherwise it retries it in OnInitComplete()
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Get the first GridView of the subpanel and use it as MainGridView
        /// </summary>
        /// <returns></returns>
        private Dionysos.Web.UI.WebControls.GridView GetMainGridView()
        {
            // Walk through the control collection of this page
            for (int i = 0; i < this.ControlList.Count; i++)
            {
                // Get a control from the control collection
                // and check whether the control is a data grid
                Control control = this.ControlList[i];
                if (control is Dionysos.Web.UI.WebControls.GridView)
                {
                    return control as Dionysos.Web.UI.WebControls.GridView;
                }
            }
            return null;
        }

        /// <summary>
        /// Get the first Repeater of the subpanel and use it as MainRepeater
        /// </summary>
        /// <returns></returns>
        private Dionysos.Web.UI.WebControls.Repeater GetMainRepeater()
        {
            // Walk through the control collection of this page
            for (int i = 0; i < this.ControlList.Count; i++)
            {
                // Get a control from the control collection
                // and check whether the control is a data grid
                Control control = this.ControlList[i];
                if (control is Dionysos.Web.UI.WebControls.Repeater)
                {
                    return control as Dionysos.Web.UI.WebControls.Repeater;
                }
            }
            return null;
        }

        /// <summary>
        /// Get the first DataList of the subpanel and use it as MainDataList
        /// </summary>
        /// <returns></returns>
        private Dionysos.Web.UI.WebControls.DataList GetMainDataList()
        {
            // Walk through the control collection of this page
            for (int i = 0; i < this.ControlList.Count; i++)
            {
                // Get a control from the control collection
                // and check whether the control is a data grid
                Control control = this.ControlList[i];
                if (control is Dionysos.Web.UI.WebControls.DataList)
                {
                    return control as Dionysos.Web.UI.WebControls.DataList;
                }
            }
            return null;
        }

        /// <summary>
        /// Initializes the page. This is a virtual method and can be overridden in subclasses.
        /// </summary>
        /// <returns>True if initialization was succesful, False if not</returns>
        public virtual bool InitializeGui()
        {
            // Retrieve information from the querystring
            this.ProcessQueryString();

            // Determine entity name
            this.GetEntityName();

            // Set Edit Columns
            if (!Instance.Empty(this.MainGridView))
            {
                if (this.HideEditDeleteViewButtonsFromGridView)
                {
                    this.MainGridView.GenerateDeleteColumn = false;
                    this.MainGridView.GenerateEditColumn = false;
                    this.MainGridView.GenerateViewColumn = false;
                }
                else
                {
                    /*
                    this.MainGridView.GenerateDeleteColumn = true;
                    this.MainGridView.GenerateEditColumn = true;
                    this.MainGridView.GenerateViewColumn = true;
                    */
                }
            }

            // Set sorting allowed?
            if (this.MainGridView != null)
                this.MainGridView.AllowSorting = this.AllowGridViewSorting;

            // 080506 GK Removed PostBack condition
            //if (!this.IsPostBack)
            //{
            // Initialize the entity collection
            this.InitializeEntityCollection();

            // Initialize the databindings
            this.InitializeDataBindings();
            //}

            // Hide the Add button            
            if (this.Parent is PageEntity && ((PageEntity)this.Parent).PageMode == PageMode.View)
            {
                var button = this.FindControl("btAdd");
                if (button is Control)
                {
                    ((Control)button).Visible = false;
                }
            }

            return true;
        }

        /// <summary>
        /// Processes the Request.QueryStringHelper instance. This is a virtual method and can be overridden in subclasses.
        /// </summary>
        public virtual void ProcessQueryString()
        {
            // GK No more code, just like the PageEntity code from entity name to GetEntityName
        }

        /// <summary>
        /// Gets the EntityName, if not set in OnInit, from QueryStringHelper and finally via the FileName of the Page
        /// </summary>
        public virtual void GetEntityName()
        {
            // If the EntityName is empty (could be set by 
            if (Instance.Empty(this.EntityName))
            {
                string entityName;
                if (!QueryStringHelper.TryGetValue("entity", out entityName))
                {
                    entityName = System.IO.Path.GetFileNameWithoutExtension(this.Request.Path);
                }

                this.EntityName = entityName;
            }
        }

        /// <summary>
        /// On Parent DataSource Loaded  to this panel it's entity collection
        /// </summary>
        protected override void OnParentDataSourceLoaded()
        {
            base.OnParentDataSourceLoaded();
            this.InitializeEntityCollection();
        }

        /// <summary>
        /// Initializes the entity collection of the page. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <returns>True if initialization was succesful, False if not</returns>
        public abstract bool InitializeEntityCollection();

        /// <summary>
        /// Initializes the data data bindings of the controls on the page. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <returns>True if initialization was succesful, False if not</returns>
        public abstract bool InitializeDataBindings();

        /// <summary>
        /// Refreshes the data collection of the page. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <returns>True if refreshing was succesful, False if not</returns>
        public abstract bool Refresh();

        /// <summary>
        /// Views an data item from the data collection of the page. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <param name="id">The primary key value of the entity to view</param>
        /// <returns>True if viewing was succesfull, False if not</returns>
        public abstract bool View(object id);

        /// <summary>
        /// Adds an data item to the data collection of the page. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <returns>True if adding was succesfull, False if not</returns>
        public abstract bool Add();

        /// <summary>
        /// Edits the currently selected data item of the data collection of the page. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <param name="id">The primary key value of the entity to edit</param>
        /// <returns>True if editing was succesfull, False if not</returns>
        public abstract bool Edit(object id);

        /// <summary>
        /// Deletes the currently selected data item from the data collection of the page. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <param name="id">The primary key value of the entity to delete</param>
        /// <returns>True if deletion was successfull, False if not</returns>
        public abstract bool Delete(object id);

        /// <summary>
        /// Closes the page and returns to the referring page. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Invokes the specified command which has been fired by the main grid view
        /// </summary>
        public abstract void InvokeCommand(string commandName, int index);

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the EntityName        
        /// </summary>
        [Browsable(true)]
        public virtual string EntityName
        {
            get
            {
                return this.entityName;
            }
            set
            {
                this.entityName = value;
            }
        }

        /// <summary>
        /// Indicate if the Edit columsn should be generated for the gridview
        /// </summary>
        [Browsable(true)]
        public virtual bool HideEditDeleteViewButtonsFromGridView
        {
            get
            {
                return this.hideEditDeleteViewButtonsFromGrid;
            }
            set
            {
                this.hideEditDeleteViewButtonsFromGrid = value;
            }
        }

        /// <summary>
        /// Gets or sets the RelatedEntityIdField (If it's a ProductExtension Entity, this could be ProductId)        
        /// </summary>
        public virtual string RelatedEntityIdField
        {
            get
            {
                return this.relatedEntityIdField;
            }
            set
            {
                this.relatedEntityIdField = value;
            }
        }

        /// <summary>
        /// Gets or sets the DataSource 
        /// </summary>
        public object DataSource
        {
            get
            {
                return this.dataSource;
            }
            set
            {
                this.dataSource = value;
            }
        }

        /// <summary>
        /// The GridView that should be populated and handled by the logic of the PageEntityCollection
        /// </summary>
        public Dionysos.Web.UI.WebControls.GridView MainGridView
        {
            get
            {
                if (this.mainGridView == null)
                {
                    this.mainGridView = this.GetMainGridView();
                }
                return this.mainGridView;
            }
            set
            {
                this.mainGridView = value;
            }
        }

        /// <summary>
        /// The Repeater that should be populated and handled by the logic of the PageEntityCollection
        /// </summary>
        public Dionysos.Web.UI.WebControls.Repeater MainRepeater
        {
            get
            {
                if (this.mainRepeater == null)
                {
                    this.mainRepeater = this.GetMainRepeater();
                }
                return this.mainRepeater;
            }
            set
            {
                this.mainRepeater = value;
            }
        }

        /// <summary>
        /// The DataList that should be populated and handled by the logic of the PageEntityCollection
        /// </summary>
        public Dionysos.Web.UI.WebControls.DataList MainDataList
        {
            get
            {
                if (this.mainDataList == null)
                {
                    this.mainDataList = this.GetMainDataList();
                }
                return this.mainDataList;
            }
            set
            {
                this.mainDataList = value;
            }
        }

        /// <summary>
        /// Field to sort the DataSource on
        /// </summary>
        public string DataSortField
        {
            get
            {
                if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSortField"))
                {
                    string field = string.Empty;
                    if (Instance.Empty(this.MainGridView))
                    {
                        return string.Empty;
                    }
                    else
                    {
                        if (this.MainGridView.Columns.Count > 0)
                        {
                            // If empty take first column of grid                    
                            if (this.MainGridView.Columns[0] is Dionysos.Web.UI.WebControls.BoundField)
                            {
                                field = ((Dionysos.Web.UI.WebControls.BoundField)this.MainGridView.Columns[0]).DataField;
                            }
                            else if (this.MainGridView.Columns[0] is Dionysos.Web.UI.WebControls.HyperLinkField)
                            {
                                field = ((Dionysos.Web.UI.WebControls.HyperLinkField)this.MainGridView.Columns[0]).DataTextField;
                            }
                        }
                        this.ViewState.Add("DataSortField", field);
                    }
                }
                return Dionysos.Web.ViewState.GetParameterAsString(this.ViewState, "DataSortField");
            }
            set
            {
                if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSortField"))
                {
                    this.ViewState.Add("DataSortField", string.Empty);
                }
                this.ViewState["DataSortField"] = value;
            }
        }

        /// <summary>
        /// Direction in which to sort the DataSource
        /// </summary>
        public ListSortDirection DataSortDirection
        {
            get
            {
                if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSortDirection"))
                {
                    this.ViewState.Add("DataSortDirection", ListSortDirection.Ascending);
                }

                return Dionysos.Web.ViewState.GetParameterAsListSortDirection(this.ViewState, "DataSortDirection");
            }
            set
            {
                if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSortDirection"))
                {
                    this.ViewState.Add("DataSortDirection", string.Empty);
                }
                this.ViewState["DataSortDirection"] = value;
            }
        }

        /// <summary>
        /// Object to filter the data of the collection on
        /// </summary>
        public object DataSourceFilter
        {
            get
            {
                if (Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSourceFilter"))
                {
                    return Dionysos.Web.ViewState.GetParameter(this.ViewState, "DataSourceFilter");
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSourceFilter"))
                {
                    this.ViewState.Add("DataSourceFilter", null);
                }
                this.ViewState["DataSourceFilter"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the url of the entity page
        /// </summary>
        [Browsable(true)]
        public virtual string EntityPageUrl
        {
            get
            {
                return this.entityPageUrl;
            }
            set
            {
                this.entityPageUrl = value;
            }
        }

        /// <summary>
        /// Field to sort the DataSource on
        /// </summary>
        public int CurrentPage
        {
            get
            {
                if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "CurrentPage"))
                {
                    return 0;
                }
                else
                {
                    return Dionysos.Web.ViewState.GetParameterAsInt(this.ViewState, "CurrentPage");
                }
            }
            set
            {
                if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "CurrentPage"))
                {
                    this.ViewState.Add("CurrentPage", 1);
                }
                this.ViewState["CurrentPage"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the allowGridViewSorting
        /// </summary>
        public bool AllowGridViewSorting
        {
            get
            {
                return this.allowGridViewSorting;
            }
            set
            {
                this.allowGridViewSorting = value;
            }
        }

        #endregion

        #region Event handlers

        private void SubPanelEntityCollection_Load(object sender, EventArgs e)
        {
            this.InitializeGui();

            if (!Instance.Empty(this.MainGridView))
            {
                // Add events, here because GridView has to be availabe
                this.MainGridView.Sorting += new System.Web.UI.WebControls.GridViewSortEventHandler(mainGridView_Sorting);
                this.MainGridView.PageIndexChanging += new System.Web.UI.WebControls.GridViewPageEventHandler(mainGridView_PageIndexChanging);
                this.MainGridView.RowCommand += new System.Web.UI.WebControls.GridViewCommandEventHandler(mainGridView_RowCommand);
            }
        }

        /// <summary>
        /// Handle Sorting from MainGridView
        /// </summary>
        /// <param name="sender">GridView Sender</param>
        /// <param name="e">Arguments</param>
        private void mainGridView_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            // Set the DataSortField and DataSortDirection
            // If the SortExpression stays the same change the SortDirection
            // If the SortExpression if new / different Sort Ascending
            // GK Separate Method            
            // Direction
            if (this.DataSortField == string.Empty || this.DataSortField != e.SortExpression)
            {
                this.DataSortDirection = ListSortDirection.Ascending;
            }
            else
            {
                if (this.DataSortDirection == ListSortDirection.Ascending)
                {
                    this.DataSortDirection = ListSortDirection.Descending;
                }
                else
                {
                    this.DataSortDirection = ListSortDirection.Ascending;
                }
            }

            // Field
            this.DataSortField = e.SortExpression;

            // Update DataSoruce (GK Make method)
            this.CurrentPage = 0;
            this.InitializeEntityCollection();
            this.InitializeDataBindings();

        }

        /// <summary>
        /// Handle Page Changed from MainGridView
        /// </summary>
        /// <param name="sender">GridView Sender</param>
        /// <param name="e">Arguments</param>
        private void mainGridView_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            this.CurrentPage = e.NewPageIndex;

            // Update DataSoruce (GK Make method)
            this.InitializeEntityCollection();
            this.InitializeDataBindings();
        }

        private void mainGridView_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            // Get the command name from the event args
            // in order to see which button has been clicked
            string commandName = e.CommandName.ToUpper();

            switch (commandName)
            {
                case "VIEW":
                case "EDIT":
                case "DELETE":
                    // Convert the row index stored in the CommandArgument
                    // property to an Integer.
                    int index = Convert.ToInt32(e.CommandArgument);


                    // Invoke the command method
                    this.InvokeCommand(commandName, index);
                    break;
            }
        }

        #endregion
    }
}
