using System;
using Dionysos.Interfaces;
using Dionysos.Delegates.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which represents a subpanel which contains a entity, must be placed on a EntityPage
	/// </summary>
	public abstract class SubPanelEntity : SubPanelOnGuiEntitySimple, ISubPanelEntity, IGuiEntitySimple
	{
        /// <summary>
		/// Constructs an instance of the SubPanelEntity class
		/// </summary>
		public SubPanelEntity()
		{
			// Hookup the events
			this.HookupEvents();
		}

        /// <summary>
		/// Init the panel which also initializes the entity if the EntityId is available, otherwise it retries it in OnInitComplete()
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.Page.InitComplete += new EventHandler(Page_InitComplete);
		}

		/// <summary>
		/// Assign eventhandlers for events of this control
		/// </summary>
		private void HookupEvents()
		{
		}

		protected override void OnParentDataSourceLoaded()
		{
			base.OnParentDataSourceLoaded();
			bool preventLoad = false;
			this.OnDataSourceLoad(ref preventLoad);
			if (!preventLoad)
				this.InitializeEntity(false);
			this.OnDataSourceLoaded();
		}

		/// <summary>
		/// Saves the current DataSource, can be overridden to perform custom saving functionality.
		/// </summary>
		/// <returns></returns>
		public abstract bool Save();

	    public abstract bool Validate();

	    /// <summary>
		/// Initializes the entity. This is an abstract method and must be overridden in subclasses.
		/// This method is run twice each page request, so it should include logic to prevent double loading.
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public abstract bool InitializeEntity(bool pageInitComplete);

        /// <summary>
		/// Gets or sets the EntityName        
		/// </summary>
		public virtual string EntityName { get; set; } = string.Empty;

        /// <summary>
		/// Gets or sets the RelatedEntityIdField (If it's a ProductExtension Entity, this could be ProductId)        
		/// </summary>
		public virtual string RelatedEntityIdField { get; set; } = string.Empty;

        /// <summary>
		/// Gets or sets the EntityId
		/// </summary>
		public virtual object EntityId { get; set; } = null;

        /// <summary>
		/// Gets or sets the flag indicating whether databinding should be used
		/// </summary>
		public new bool UseDataBinding { get; set; } = true;

        /// <summary>
		/// Gets or sets the DataSource 
		/// </summary>
		public virtual IEntity DataSource { get; set; } = null;

        public PageEntity PageAsPageEntity
		{
			get
			{
				return this.Page as PageEntity;
			}
		}
        
		void SubPanelEntity_Init(object sender, EventArgs e)
		{
			// In case the panel has !UseDataBinding, disable all DataBinding for Controls on the Panel
			if (!this.UseDataBinding)
			{
				for (int i = 0; i < this.Controls.Count; i++)
				{
					if (this.Controls[i] is IBindable)
					{
						((IBindable)this.Controls).UseDataBinding = false;
					}
				}
			}
		}

		void Page_InitComplete(object sender, EventArgs e)
		{
			bool preventLoad = false;
			this.OnDataSourceLoad(ref preventLoad);
			if (!preventLoad)
				this.InitializeEntity(true);
			this.OnDataSourceLoaded();
		}

		public event DataSourceLoadHandler DataSourceLoad;
		public event DataSourceLoadedHandler DataSourceLoaded;

		protected void OnDataSourceLoaded()
		{
			if (this.DataSourceLoaded != null)
				this.DataSourceLoaded(this);
		}


		protected void OnDataSourceLoad(ref bool preventLoad)
		{
			if (this.DataSourceLoad != null)
				this.DataSourceLoad(this, ref preventLoad);
		}

	}
}
