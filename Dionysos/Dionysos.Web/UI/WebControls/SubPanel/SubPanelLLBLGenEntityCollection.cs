using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Gui class which represents a subpanel which contains a LLBLGen entity collection
	/// </summary>
	public class SubPanelLLBLGenEntityCollection : SubPanelEntityCollection
	{
		#region Fields

		private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the SubPanelLLBLGenEntityCollection class
		/// </summary>
		public SubPanelLLBLGenEntityCollection()
		{
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the entity collection of the subpanel
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public override bool InitializeEntityCollection()
		{
			if (this.CollectionOnParentDataSource != string.Empty)
			{
				try
				{
					this.entityCollection = Dionysos.Reflection.Member.InvokeProperty(this.PageAsPageLLBLGenEntity.DataSource, this.CollectionOnParentDataSource) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
				}
				catch (Exception e)
				{
					throw new Dionysos.TechnicalException("The property 'CollectionOnParentDataSource' has a value ('{0}') which is not a valid property of the DataSource of the Page to which this SubPanel is bound.\r\n ERROR:" + e.Message, this.CollectionOnParentDataSource);
				}
			}
			else
			{
				this.entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
				this.entityCollection.GetMulti(this.DataSourceFilter, -1, null, this.DataSourceRelations);
			}

			return !Instance.Empty(this.entityCollection);
		}

		/// <summary>
		/// Initializes the data data bindings of the controls on the subpanel.
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public override bool InitializeDataBindings()
		{
			if (this.DataSortField != string.Empty)
			{
				this.entityCollection.Sort(this.DataSortField, this.DataSortDirection, null);
			}

			// Since InitializeDataBindings can be ran on postback (Sort/Page) we need a check here
			if (!this.IsPostBack)
			{
				if (!Instance.Empty(this.MainGridView))
				{
					this.MainGridView.InitializeColumns(this.EntityName);
				}
			}

			if (!Instance.Empty(this.MainGridView))
			{
				this.MainGridView.PageIndex = this.CurrentPage;
				this.MainGridView.DataSource = this.entityCollection;
				this.MainGridView.DataBind();
			}

			return true;
		}

		/// <summary>
		/// Refreshes the data collection of the subpanel
		/// </summary>
		/// <returns></returns>
		public override bool Refresh()
		{
			this.InitializeEntityCollection();
			this.InitializeDataBindings();
			return true;
		}

		/// <summary>
		/// Views an data item from the data collection of the subpanel.
		/// </summary>
		/// <returns>True if viewing was succesfull, False if not</returns>
		public override bool View(object id)
		{
			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("id", id);
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "view");

			if (Instance.Empty(this.EntityPageUrl))
			{
				throw new EmptyException("The 'EntityPageUrl' property is not set for the SubPanelLLBLGenEntityCollection instance.");
			}
			else
			{
				Response.Redirect(ResolveUrl(this.EntityPageUrl + queryString.Value));
			}

			return true;
		}

		/// <summary>
		/// Adds an data item to the data collection of the subpanel
		/// </summary>		
		/// <returns>True if adding was succesfull, False if not</returns>
		public override bool Add()
		{
			return this.Add(null);
		}

		/// <summary>
		/// Adds an data item to the data collection of the subpanel
		/// </summary>
		/// <param name="queryStringParameters">The query string parameters.</param>
		/// <returns>
		/// True if adding was succesfull, False if not
		/// </returns>
		public bool Add(QueryStringHelper queryStringParameters)
		{
			// First save
			this.Page.Validate();
			if (this.PageAsPageLLBLGenEntity.Save())
			{
				QueryStringHelper queryString;
				if (queryStringParameters != null)
					queryString = queryStringParameters;
				else
					queryString = new QueryStringHelper();

				queryString.AddItem("entity", this.EntityName);
				queryString.AddItem("mode", "add");

				// Add RelationalData if possible
				if (this.PageAsPageLLBLGenEntity.DataSource != null &&
					this.PageAsPageLLBLGenEntity.DataSource.PrimaryKeyFields.Count == 1)
				{
					queryString.AddItem(this.PageAsPageLLBLGenEntity.DataSource.PrimaryKeyFields[0].Name, this.PageAsPageLLBLGenEntity.DataSource.PrimaryKeyFields[0].CurrentValue);
				}

				if (Instance.Empty(this.EntityPageUrl))
				{
					throw new EmptyException("The 'EntityPageUrl' property is not set for the SubPanelLLBLGenEntityCollection instance.");
				}
				else
				{
					Response.Redirect(ResolveUrl(this.EntityPageUrl + queryString.Value), true);
				}
			}
			return true;
		}

		/// <summary>
		/// Edits the currently selected data item of the data collection of the subpanel
		/// </summary>
		/// <returns>True if editing was succesfull, False if not</returns>
		public override bool Edit(object id)
		{
			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("id", id);
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "edit");

			if (Instance.Empty(this.EntityPageUrl))
			{
				throw new EmptyException("The 'EntityPageUrl' property is not set for the SubPanelLLBLGenEntityCollection instance.");
			}
			else
			{
				Response.Redirect(ResolveUrl(this.EntityPageUrl + queryString.Value), true);
			}

			return true;
		}

		/// <summary>
		/// Deletes the currently selected data item from the data collection of the subpanel
		/// </summary>
		/// <returns>True if deletion was successfull, False if not</returns>
		public override bool Delete(object id)
		{
			/*
			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("id", id);
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "delete");			

			if (Instance.Empty(this.EntityPageUrl))
			{
				throw new EmptyException("The 'EntityPageUrl' property is not set for the SubPanelLLBLGenEntityCollection instance.");
			}
			else
			{
				Response.Redirect(ResolveUrl(this.EntityPageUrl + queryString.Value), true);
			}
			*/
			LLBLGenEntityUtil llblgenEntityUtil = new LLBLGenEntityUtil();
			if (llblgenEntityUtil.DeleteEntity(this.EntityName, Convert.ToInt32(id)))
			{
				WebShortcuts.RedirectUsingRawUrl();
				return true;
			}
			else
				return false;
		}

		/// <summary>
		/// Closes the page and returns to the referring page
		/// </summary>
		public override void Close()
		{
		}

		public override void InvokeCommand(string commandName, int index)
		{
			object id = null;

			// Update DataSoruce (GK Make method)
			this.InitializeEntityCollection();
			this.InitializeDataBindings();

			// Check whether the datasource of this gridview instance is empty
			if (Instance.Empty(this.MainGridView))
			{
				throw new EmptyException("Variable 'mainGridView' is empty.");
			}
			else if (Instance.Empty(this.MainGridView.DataSource))
			{
				throw new EmptyException("Variable 'mainGridView.DataSource' is empty.");
			}
			else
			{
				SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection = this.MainGridView.DataSource as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;

				if (entityCollection.Count > index)
				{
					SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = entityCollection[index];
					if (Instance.Empty(entity))
					{
						throw new EmptyException("Variable 'entity' is empty.");
					}
					else
					{
						// GK Better to retrieve PK field value if possible
						string idFieldName;
						if (entity.PrimaryKeyFields.Count == 1)
							idFieldName = entity.PrimaryKeyFields[0].Name;
						else
							idFieldName = entity.LLBLGenProEntityName.Replace("Entity", "") + "Id";
						SD.LLBLGen.Pro.ORMSupportClasses.IEntityField idField = entity.Fields[idFieldName];
						if (Instance.Empty(idField))
						{
							throw new EmptyException("Variable 'idField' is empty. Possible cause: idField (PK field of Entity) != EntityName + Id, check PK Column Name in Database for typos");
						}
						else
						{
							id = idField.CurrentValue;
						}
					}
				}
				else
				{
					throw new ApplicationException("Index is bigger than the size of the collection.");
				}
			}

			// Check which command was invoked and which
			// method whould be invoked on the page
			switch (commandName)
			{
				case "VIEW":
					this.View(id);
					break;

				case "EDIT":
					this.Edit(id);
					break;

				case "DELETE":
					this.Delete(id);
					break;
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Custom override for LLBLGen, if not set it simply goes to EnityName.aspx in the same folder as the page which contains the panel
		/// </summary>
		public override string EntityPageUrl
		{
			get
			{
				if (Instance.Empty(base.EntityPageUrl))
				{
					// Get path of the Request
					int lastSlash = this.Request.Path.LastIndexOf("/");
					string url = this.Request.Path.Substring(0, lastSlash + 1);
					return url + this.EntityName + ".aspx";
				}
				else
				{
					return base.EntityPageUrl;
				}
			}
			set
			{
				base.EntityPageUrl = value;
			}
		}

		/// <summary>
		/// Gets or sets the DataSource 
		/// </summary>
		public new SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection DataSource
		{
			get
			{
				return this.entityCollection;
			}
			set
			{
				this.entityCollection = value;
			}
		}

		/// <summary>
		/// Gets the Parent page as a PageLLBGenEntity
		/// </summary>
		public PageLLBLGenEntity PageAsPageLLBLGenEntity
		{
			get
			{
				return this.Page as PageLLBLGenEntity;
			}
		}

		/// <summary>
		/// Gets or sets the Filter for the current EntityCollectionPage
		/// Only used when CollectionOnParentDataSource is empty
		/// </summary>
		public new SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression DataSourceFilter
		{
			get
			{
				return base.DataSourceFilter as SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression;
			}
			set
			{
				base.DataSourceFilter = value;
			}
		}

		/// <summary>
		/// Gets or sets the Relations for the datasource (LLBLGen RelationCollection) for filtering/sorting on related entities
		/// Only used when CollectionOnParentDataSource is empty 
		/// </summary>
		public RelationCollection DataSourceRelations
		{
			get
			{
				if (Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSourceRelations"))
				{
					return Dionysos.Web.ViewState.GetParameter(this.ViewState, "DataSourceRelations") as RelationCollection;
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSourceRelations"))
				{
					this.ViewState.Add("DataSourceRelations", null);
				}
				this.ViewState["DataSourceRelations"] = value;
			}
		}

		/// <summary>
		/// Gets / Sets the name of the EnittyCollection on the Parent DataSource that has to be used as the DataSource for this Subpanel (i.e. OrderItemCollection for OrderEntity)
		/// </summary>
		public string CollectionOnParentDataSource
		{
			get
			{
				if (Dionysos.Web.ViewState.HasParameter(this.ViewState, "CollectionOnParentDataSource"))
				{
					return Dionysos.Web.ViewState.GetParameterAsString(this.ViewState, "CollectionOnParentDataSource");
				}
				else
				{
					return string.Empty;
				}
			}
			set
			{
				if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "CollectionOnParentDataSource"))
				{
					this.ViewState.Add("CollectionOnParentDataSource", null);
				}
				this.ViewState["CollectionOnParentDataSource"] = value;
			}
		}


		#endregion
	}
}
