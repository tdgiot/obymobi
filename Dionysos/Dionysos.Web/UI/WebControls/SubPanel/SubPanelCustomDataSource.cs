using System;
using System.Web.UI;
using Dionysos.Delegates.Web;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Represent a sub panel control.
	/// </summary>
	public class SubPanelCustomDataSource : UserControl
	{
		#region Fields
		
		private object dataSource;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the DataSource 
		/// </summary>
		public object DataSource
		{
			get
			{
				return this.dataSource;
			}
			set
			{
				this.dataSource = value;
			}
		}

        public PageDefault PageAsPageDefault
        {
            get
            {
                return this.Page as PageDefault;
            }
        }

		#endregion

		#region Contructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SubPanel"/> class.
		/// </summary>
        public SubPanelCustomDataSource()
		{
			// Hookup the events
			this.Load += SubPanel_Load;
		}

		#endregion

		#region Event handlers

        /// <summary>
        /// Init the panel which also initializes the entity if the EntityId is available, otherwise it retries it in OnInitComplete()
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Page.InitComplete += new EventHandler(Page_InitComplete);
        }

        void Page_InitComplete(object sender, EventArgs e)
        {
            bool preventLoad = false;
            this.OnDataSourceLoad(ref preventLoad);            
            this.OnDataSourceLoaded();
        }

		/// <summary>
		/// Handles the Load event of the SubPanel control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void SubPanel_Load(object sender, EventArgs e)
		{
		
		}

		#endregion

		#region Methods		

		#endregion

        #region Delagets, Delagtes Callers & Events

        public event DataSourceLoadHandler DataSourceLoad;
        public event DataSourceLoadedHandler DataSourceLoaded;

        protected void OnDataSourceLoaded()
        {
            if (this.DataSourceLoaded != null)
                this.DataSourceLoaded(this);
        }


        protected void OnDataSourceLoad(ref bool preventLoad)
        {
            if (this.DataSourceLoad != null)
                this.DataSourceLoad(this, ref preventLoad);
        }

        #endregion
	}
}
