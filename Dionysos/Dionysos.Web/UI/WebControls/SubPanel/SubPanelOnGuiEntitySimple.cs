﻿using System;
using System.Web.UI;
using Dionysos.Delegates.Web;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Represents a sub panel control with a parent data source.
	/// </summary>
	public class SubPanelOnGuiEntitySimple : SubPanel
	{
		#region Fields

		/// <summary>
		/// The parent.
		/// </summary>
		private IGuiEntitySimple parent = null;

		/// <summary>
		/// Indicates whether the parent data source load event has fired.
		/// </summary>
		private bool hasParentDataSourceLoadFired;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the parent IGuiEntitySimple.
		/// </summary>
		/// <returns>A reference to the server control's parent control.</returns>
		public new IGuiEntitySimple Parent
		{
			get
			{
				IGuiEntitySimple parent = this.parent ?? this.GetIGuiElementSimpleParent(this);
				if (parent == null) throw new TechnicalException("Control '{0}' behoort op een IGuiEntitySimple te worden geplaatst.", this.GetType().ToString());

				return parent;
			}
			set
			{
				this.parent = value;

				if (value != null) value.DataSourceLoaded += new DataSourceLoadedHandler(Parent_DataSourceLoaded);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SubPanelOnGuiEntitySimple"/> class.
		/// </summary>
		public SubPanelOnGuiEntitySimple()
		{
			this.PreRender += new EventHandler(SubPanelOnGuiEntitySimple_PreRender);
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the PreRender event of the SubPanelOnGuiEntitySimple control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void SubPanelOnGuiEntitySimple_PreRender(object sender, EventArgs e)
		{
			if (!this.hasParentDataSourceLoadFired)
				throw new TechnicalException("{0}: Parent DataSource Load was not fired! Has the parent been set? Has the panel been added as tab using the PageControl?", this.GetType().BaseType.FullName);
		}

		/// <summary>
		/// Handles the DataSourceLoaded event of the Parent control.
		/// </summary>
		/// <param name="sender">The sender.</param>
		private void Parent_DataSourceLoaded(object sender)
		{
			this.OnParentDataSourceLoaded();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Called when the parent data source is loaded.
		/// </summary>
		protected virtual void OnParentDataSourceLoaded()
		{
			this.hasParentDataSourceLoadFired = true;
		}

		/// <summary>
		/// Recursively gets the IGuiEntitySimple control.
		/// </summary>
		/// <param name="control">The control.</param>
		/// <returns>
		/// The IGuiEntitySimple control if found; otherwise, <c>null</c>.
		/// </returns>
		protected IGuiEntitySimple GetIGuiElementSimpleParent(Control control)
		{
			IGuiEntitySimple iGuiEntitySimple = control as IGuiEntitySimple;
			if (control == this && control.Parent != null)
			{
				// Don't return itself
				return this.GetIGuiElementSimpleParent(control.Parent);
			}
			else if (iGuiEntitySimple != null)
			{
				// Found control
				return iGuiEntitySimple;
			}
			else if (control.Parent != null)
			{
				// Try to get from parent control
				return this.GetIGuiElementSimpleParent(control.Parent);
			}
			else
			{
				// None found
				return null;
			}
		}

		#endregion
	}
}
