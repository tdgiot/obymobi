using System;
using System.Web.UI;
using Dionysos.Interfaces;
using Dionysos.Interfaces.Presentation;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// Represent a sub panel control.
	/// </summary>
	public class SubPanel : UserControl, ISubPanel, IBindable, IUielement
	{
		#region Fields

		/// <summary>
		/// The parent.
		/// </summary>
		private IGuiEntitySimple parent;

		/// <summary>
		/// Indicates whether to use data binding.
		/// </summary>
		private bool useDataBinding = true;

		/// <summary>
		/// The control list.
		/// </summary>
		private ControlCollection controlList;

		#endregion

		#region Properties

        /// <summary>
        /// Gets / sets (GK: sets ?!?) the Parent page as a PageLALBGenEntity
        /// </summary>
        public PageLLBLGenEntity PageAsPageLLBLGenEntity
        {
            get
            {
                return this.Page as PageLLBLGenEntity;
            }
        }

		/// <summary>
		/// Gets or sets the parent IGuiEntitySimple.
		/// </summary>
		/// <returns>A reference to the server control's parent control.</returns>
		public new IGuiEntitySimple Parent
		{
			get
			{
				return this.parent;
			}
			set
			{
				this.parent = value;
			}
		}

		/// <summary>
		/// Gets or sets the flag which indicates whether databinding should be used.
		/// </summary>
		public bool UseDataBinding
		{
			get
			{
				return this.useDataBinding;
			}
			set
			{
				this.useDataBinding = value;
			}
		}

		/// <summary>
		/// Gets the list of controls on this page.
		/// </summary>
		/// <value>
		/// The control list.
		/// </value>
		public ControlCollection ControlList
		{
			get
			{
				if (this.controlList == null)
				{
					this.CollectControls();
				}

				return this.controlList ?? (this.controlList = new ControlCollection());
			}
		}

		#endregion

		#region Contructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SubPanel"/> class.
		/// </summary>
		public SubPanel()
		{
			// Hookup the events
			this.Load += new EventHandler(SubPanel_Load);
		}

		#endregion

		#region Event handlers

		/// <summary>
		/// Handles the Load event of the SubPanel control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void SubPanel_Load(object sender, EventArgs e)
		{
			// Initialize parent
			this.InitializeParent();

			// Collect the controls on the page
			this.CollectControls();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initialize the parent.
		/// </summary>
		public virtual void InitializeParent()
		{
			this.Parent = (IGuiEntity)this.Page;
		}

		/// <summary>
		/// Collects the controls from the page.
		/// </summary>
		public void CollectControls()
		{
			ControlCollection controlList = new ControlCollection();
			foreach (Control control in this.Controls)
			{
				this.CollectControls(control, ref controlList);
			}

			this.controlList = controlList;
		}

		/// <summary>
		/// Recursively gets the controls from the specified control.
		/// </summary>
		/// <param name="control">The control.</param>
		private void CollectControls(Control control, ref ControlCollection controlList)
		{
			if (control != null &&
				controlList != null)
			{
				// Add control
				controlList.Add(control);

				// Add sub controls
				foreach (Control subControl in control.Controls)
				{
					this.CollectControls(subControl, ref controlList);
				}
			}
		}

		/// <summary>
		/// Adds the informator.
		/// </summary>
		/// <param name="informatorType">The type of the informator.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An array containing zero or more objects to format.</param>
		protected void AddInformator(InformatorType informatorType, string format, params object[] args)
		{
			if (Page is Page dionysosPage)
			{
				dionysosPage.AddInformator(informatorType, format, args);
			}			
		}

		/// <summary>
		/// Translates the specified translation key.
		/// </summary>
		/// <param name="translationKey">The translation key, a prefix is not required, the PageClass will be used for that.</param>
		/// <param name="translationValue">The translation value.</param>
		/// <returns></returns>
		/// <exception cref="Dionysos.TechnicalException">Dionysos.Global.TranslationProvider == null, method 'PageDefault.Translate' is a shortcust to Dionysos.Global.TranslationProvider and requires Dionysos.Global.TranslationProvider != null.</exception>
		protected string Translate(string translationKey, string translationValue, bool manualTranslationKey = false)
		{
            return Page is PageDefault pageDefault
                ? pageDefault.Translate(translationKey, translationValue, manualTranslationKey)
                : translationKey;
        }

        #endregion
    }
}
