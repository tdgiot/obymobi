using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Web.UI;
using Dionysos.Reflection;

namespace Dionysos.Web.UI.WebControls
{
	/// <summary>
	/// GUI class which represents a sub panel which contains a LLBLGen entity.
	/// </summary>
	public abstract class SubPanelLLBLGenEntity : SubPanelEntity
	{
		#region Fields

		private string entityOnParentDataSource = String.Empty;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the DataSource 
		/// </summary>
		public new IEntity DataSource
		{
			get
			{
				return base.DataSource as IEntity;
			}
			set
			{
				base.DataSource = value;
			}
		}

		/// <summary>
		/// Gets / sets (GK: sets ?!?) the Parent page as a PageLALBGenEntity
		/// </summary>
		public PageLLBLGenEntity PageAsPageLLBLGenEntity
		{
			get
			{
				return this.Page as PageLLBLGenEntity;
			}
		}

		/// <summary>
		/// Overridden new EntityId in int format.
		/// </summary>
		public new int EntityId
		{
			get
			{
				int retval = -1;
				try
				{
					if (base.EntityId != null)
					{
						retval = Convert.ToInt32(base.EntityId);
					}

					if (retval < 0 && this.DataSource != null)
					{
						if (this.DataSource.PrimaryKeyFields.Count == 1)
						{
							if (this.DataSource.PrimaryKeyFields[0].CurrentValue != null)
							{
								retval = (int)this.DataSource.PrimaryKeyFields[0].CurrentValue;
							}
						}
					}
				}
				catch
				{
					Dionysos.Exceptions.ExceptionManager.HandleException(new InvalidCastException("Non-integer EntityId on PageLLBLGenEntity"));
				}
				return retval;
			}
			set
			{
				base.EntityId = value;
			}
		}

		/// <summary>
		/// Overriden for LLBLGenEntities, adds "Entity" to base.EntityName is it's not yet in place.
		/// </summary>
		public override string EntityName
		{
			get
			{
				if (base.EntityName.EndsWith("Entity") || Instance.Empty(base.EntityName))
				{
					return base.EntityName;
				}
				else
				{
					return base.EntityName + "Entity";
				}
			}
			set
			{
				base.EntityName = value;
			}
		}

		/// <summary>
		/// Gets or sets the RelatedEntityIdField (If it's a ProductExtension Entity, this could be ProductId)
		/// If it's empty the system will automaticly try to find the name of the ParentEntityPK field on the Entity of the Panel
		/// </summary>
		public override string RelatedEntityIdField
		{
			get
			{
				if (base.RelatedEntityIdField == string.Empty)
				{
					// find pk fieldname of parent entity
					return this.PageAsPageLLBLGenEntity.DataSource.PrimaryKeyFields[0].Name;
				}
				else
				{
					return base.RelatedEntityIdField;
				}
			}
			set
			{
				base.RelatedEntityIdField = value;
			}
		}

		/// <summary>
		/// Gets or sets the EntityOnParentDataSource
		/// </summary>
		public string EntityOnParentDataSource
		{
			get
			{
				return this.entityOnParentDataSource;
			}
			set
			{
				this.entityOnParentDataSource = value;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Saves the current DataSource, can be overridden to perform custom saving functionality.
		/// </summary>
		/// <returns></returns>
		public override bool Save()
		{
			if (this.SetNonBindableData())
			{
				return this.DataSource.Save() && this.PageAsPageEntity.Refresh();
			}
			else
			{
				throw new Exception("SetNonBindableData failed");
			}
		}

	    public override bool Validate()
	    {
	        return true;
	    }

	    /// <summary>
		/// This function is called just before the datasource is Saved, for new entities this function should include setting the ID of the parent entity
		/// </summary>
		public virtual bool SetNonBindableData()
		{
			if (this.DataSource.Fields[this.RelatedEntityIdField] != null)
				this.DataSource.SetNewFieldValue(this.RelatedEntityIdField, this.PageAsPageLLBLGenEntity.EntityId);
			else
				throw new Dionysos.TechnicalException("Related Field '{0}' cannot be set for Entity of Type '{1}' on Subpanel '{2}'. Field not available on Entity",
					this.RelatedEntityIdField, this.DataSource.LLBLGenProEntityName, this.ID);

			return true;
		}

		/// <summary>
		/// Initializes the entity.
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public override bool InitializeEntity(bool pageInitComplete)
		{
			// GK Kind of tricky gokken op 1 PK field
			bool loadEntity = true;

			// Dit stukje code moet misschien herzien, maar een UserControl laat DIRECT zijn init als hij op een pagina geplaatst is. 
			// Dus of we moeten besluiten dat alle panels vanuit de db of op andere wijze worden geladen of we moet dit stukje code
			// behouden. Leg wel ff uit als je mailt of belt.
			// Het heeft er mee te maken dat deze functie 2x word aangeroepen, oninit en oninitcomplete vanuit subpanelentity
			if (this.DataSource != null && ((this.EntityId > 0 && !this.DataSource.IsNew) || this.DataSource.IsNew))
			{
				// Er is een EntityId > 0 en de Entity komt uit Persistent (!IsNew)
				loadEntity = false;
			}

			if (loadEntity)
			{
				if (!String.IsNullOrEmpty(this.EntityOnParentDataSource) &&
					this.PageAsPageLLBLGenEntity.DataSource != null &&
					!this.PageAsPageLLBLGenEntity.DataSource.IsNew)
				{
					// Retrieve from parent
					try
					{
						this.DataSource = Dionysos.Reflection.Member.InvokeProperty(this.PageAsPageLLBLGenEntity.DataSource, this.EntityOnParentDataSource) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
					}
					catch (Exception ex)
					{
						throw new Dionysos.TechnicalException("Het is niet gelukt om data datasource te vullen met de property '{0}' van pagina waarop het subpanel '{1}' voor entity '{2}' is geplaatst. Error: {3}",
							this.EntityOnParentDataSource, this.ID, this.EntityName, ex.Message);
					}
				}
				else if (Instance.Empty(this.EntityId))
				{
					// Retrieve empty datasource
					this.DataSource = DataFactory.EntityFactory.GetEntity(this.EntityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
				}
				else
				{
					// Retrieve by PK value
					this.DataSource = DataFactory.EntityFactory.GetEntity(this.EntityName, this.EntityId) as SD.LLBLGen.Pro.ORMSupportClasses.IEntity;
				}
			}
			return !Instance.Empty(this.DataSource);
		}

		#endregion
	}
}
