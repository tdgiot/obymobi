﻿using System;
using System.Reflection;
using System.Web.UI;
using Dionysos.Interfaces;
using Dionysos.Reflection;

namespace Dionysos.Web.UI.WebControls
{
	public abstract class SubPanelQueryStringDataBinding : System.Web.UI.UserControl
	{
		#region Fields

		/// <summary>
		/// The checkbox list values seperator.
		/// </summary>
		private const char CheckBoxListValuesSeperator = '|';

		#endregion

		#region Properties

		/// <summary>
		/// Gets the page as PageDefault.
		/// </summary>
		private PageDefault PageAsPageDefault
		{
			get
			{
				return this.Page as PageDefault;
			}
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Init Method, First loads DefaultValuesToControls, then binds the QueryString values to the Controls (unless it's postback)
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.SetDefaultValuesToControls();
			this.BindQueryStringToControls();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the default values to the controls.
		/// </summary>
		protected abstract void SetDefaultValuesToControls();

		/// <summary>
		/// Binds the query string values to the controls.
		/// </summary>
		protected virtual void BindQueryStringToControls()
		{
			this.PageAsPageDefault.CollectControls();
			foreach (Control control in this.PageAsPageDefault.ControlList)
			{
				string fieldName = control.ID.RemoveLeadingLowerCaseCharacters();

				string stringValue;
				if (!String.IsNullOrEmpty(fieldName) &&
					QueryStringHelper.TryGetValue(fieldName, out stringValue) &&
					!String.IsNullOrEmpty(stringValue))
				{
					CheckBoxList checkBoxListControl = control as CheckBoxList;
					IBindable bindableControl = control as IBindable;
					if (checkBoxListControl != null)
					{
						checkBoxListControl.SelectMultipleItems(stringValue, PageQueryStringDataBinding.CheckBoxListValuesSeperator);
					}
					else if (bindableControl != null &&
						bindableControl.UseDataBinding)
					{
						PropertyInfo propertyInfo = Member.GetLowestProperty(control, "Value");
						if (propertyInfo != null)
						{
							object valueToSet = null;

							// Use Value property with the same type as the datasource or an unspecified type (object)
							if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(int?))
							{
								valueToSet = QueryStringHelper.GetValue<int>(fieldName);
							}
							else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(int))
							{
								valueToSet = QueryStringHelper.GetValue<int>(fieldName) ?? default(int);
							}
							else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(decimal?))
							{
								valueToSet = QueryStringHelper.GetValue<decimal>(fieldName);
							}
							else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(decimal))
							{
								valueToSet = QueryStringHelper.GetValue<decimal>(fieldName) ?? default(decimal);
							}
							else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(DateTime?))
							{
								valueToSet = QueryStringHelper.GetValue<DateTime>(fieldName);
							}
							else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(DateTime))
							{
								valueToSet = QueryStringHelper.GetValue<DateTime>(fieldName) ?? default(DateTime);
							}
							else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(bool))
							{
								valueToSet = QueryStringHelper.GetValue<bool>(fieldName) ?? default(bool);
							}
							else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(string))
							{
								valueToSet = stringValue;
							}
							else throw new NotImplementedException(String.Format("PageQueryStringDataBinding hasn't got an implementation for Type: '{0}' used by field '{1}'", propertyInfo.PropertyType.UnderlyingSystemType.ToString(), fieldName));

							propertyInfo.SetValue(control, valueToSet, null);
						}
					}
				}
			}
		}

		/// <summary>
		/// Gets the query string instance with values from the controls.
		/// </summary>
		/// <returns></returns>
		protected virtual QueryStringHelper GetQueryStringInstanceWithValues()
		{
			QueryStringHelper qsh = new QueryStringHelper();

			foreach (Control control in this.PageAsPageDefault.ControlList)
			{
				string fieldName = control.ID.RemoveLeadingLowerCaseCharacters();

				CheckBoxList checkBoxListControl = control as CheckBoxList;
				IBindable bindableControl = control as IBindable;

				if (checkBoxListControl != null)
				{
					qsh.AddItem(fieldName, checkBoxListControl.SelectedValues(PageQueryStringDataBinding.CheckBoxListValuesSeperator));
				}
				else if (bindableControl != null &&
					bindableControl.UseDataBinding)
				{
					if (Member.HasProperty(control, "Value"))
					{
						qsh.AddItem(fieldName, Member.InvokeProperty(control, "Value"));
					}
				}
			}

			return qsh;
		}

		#endregion
	}
}
