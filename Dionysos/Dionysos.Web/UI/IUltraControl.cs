﻿using Dionysos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI
{
    public interface IUltraControl : INamingContainer, IValidator, IBindable
    {
        int SelectedIndex { get; set; }
        bool Enabled { get; set; }
        bool IsRequired { get; set; }
        Unit Width { get; set; }
        int? Value { get; set; }
    }
}
