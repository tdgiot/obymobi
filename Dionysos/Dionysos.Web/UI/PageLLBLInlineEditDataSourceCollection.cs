﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Data;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Class for LLBL Inline Edit DataSource
	/// </summary>
	public class PageLLBLInlineEditDataSourceCollection : PageInlineEditDataSourceCollection
	{
		#region Fields
		#endregion

		#region Methods

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			// Initialize the DataSource object			
			LLBLGenProDataSource ds = new LLBLGenProDataSource();
			ds.ID = this.DataSourceId;
			ds.LivePersistence = false; // We want to be able to control it
			this.Controls.Add(ds);
			this.DataSource = ds;

			this.HookUpEvents();
		}

		/// <summary>
		/// Hook up all relevant events
		/// </summary>
		void HookUpEvents()
		{
			// Set events for the DataSource object
			this.DataSource.PerformSelect += new EventHandler<PerformSelectEventArgs>(DataSource_PerformSelect);
			this.DataSource.PerformGetDbCount += new EventHandler<PerformGetDbCountEventArgs>(DataSource_PerformGetDbCount);
			this.DataSource.PerformWork += new EventHandler<PerformWorkEventArgs>(DataSource_PerformWork);
		}

		/// <summary>
		/// Initialise the LLBLGenDataSource
		/// </summary>
		/// <returns></returns>
		public override bool InitializeDataSource()
		{
			// Initalize DataSource
			LLBLGenProDataSource ds = this.DataSource;

			// Initialize some standard values
			ds.ID = this.DataSourceId;
			ds.LivePersistence = true;
			ds.CacheLocation = DataSourceCacheLocation.Session;
			ds.DataContainerType = DataSourceDataContainerType.EntityCollection;
			ds.EnablePaging = true;

			// Retrieve the full type, assembly name of the collection to load
			SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection collection = Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
			Type typeOfCollection = collection.GetType();
			ds.EntityCollectionTypeName = string.Format("{0}, {1}", typeOfCollection.FullName, typeOfCollection.Assembly);

			// Set PK fieldname
			SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = Dionysos.DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
			this.MainGridView.KeyFieldName = entity.PrimaryKeyFields[0].Name;			

			return true;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Perform the Work
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Arguments</param>
		void DataSource_PerformWork(object sender, PerformWorkEventArgs e)
		{
			// Perform the work passed in via the PerformWorkEventArgs object. 
			// Start a new transaction with the passed in unit of work.
			Exception ex = null;
			ITransaction trans = Dionysos.Data.LLBLGen.LLBLGenUtil.GetTransaction(IsolationLevel.ReadCommitted, this.UniqueID);
			try
			{
				// pass the transaction to the Commit routine and tell it to autocommit
				// when the work is done. If an exception is thrown, the transaction will
				// be rolled back by the Dispose call of the using statement. 
				e.Uow.Commit(trans, true);
			}
			catch (Exception catchedException)
			{
				trans.Rollback();
				ex = catchedException;
			}
			finally 
			{				
				trans.Dispose();
				if (ex != null)
				{
					throw ex;
				}
			}						
		}

		/// <summary>
		/// Perform the GetDbCount
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Arguments</param>
		void DataSource_PerformGetDbCount(object sender, PerformGetDbCountEventArgs e)
		{
			// get the total number of orders which match the filter passed in via the
			// PerformGetDbCountEventArgs.
			e.DbCount = e.ContainedCollection.GetDbCount(e.Filter, e.Relations);

		}

		/// <summary>
		/// Perform the Select
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Arguments</param>
		void DataSource_PerformSelect(object sender, PerformSelectEventArgs e)
		{
			// fetch all orders which are in the selected page using the filter passed in
			// via the PerformSelectEventArgs object. 
			e.ContainedCollection.GetMulti(e.Filter, e.MaxNumberOfItemsToReturn, e.Sorter,
					e.Relations, e.PrefetchPath, e.PageNumber, e.PageSize);

		}

		#endregion

		#region Properties

		/// <summary>
		/// DataSource as a LLBLGenProDataSource datatype
		/// </summary>
		public new LLBLGenProDataSource DataSource
		{
			get
			{
				return base.DataSource as LLBLGenProDataSource;
			}
			set
			{
				base.DataSource = value;
			}
		}

		#endregion

	}
}
