﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Interfaces;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Interfaces.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data;
using Dionysos.Web.Security;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Class for displaying collections from a datasource
	/// </summary>
	public abstract class PageOverviewDataSourceCollection : PageDataSourceCollection, IGridViewEntityCollectionReady
	{
		#region Fields

		private GridViewEntityCollection mainGridView;
        private List<IView> defaultViews = null;
        //private List<IView> customViews = null;
        private IView selectedView = null;

		#endregion

		#region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.PageOverviewDataSourceCollection type
        /// </summary>
		public PageOverviewDataSourceCollection()
		{
            // Hookup the events
			this.HookUpEvents();
		}

        #endregion

        #region Methods

        /// <summary>
        /// Hookup the events to the corresponding event handlers
        /// </summary>
        private void HookUpEvents()
		{
            this.Init += new EventHandler(PageOverviewDataSourceCollection_Init);
			this.Load += new EventHandler(PageOverviewDataSourceCollection_Load);			
		}

		void MainGridView_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
		{
			if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
			{				
				e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#dfe6e7'");
				e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#ffffff'");
			}
		}

		private void ConfigureGridView()
		{
			this.MainGridView.ShowDeleteColumn = false;
			this.MainGridView.ShowEditColumn = false;
			this.MainGridView.ShowDeleteHyperlinkColumn = true;
			this.mainGridView.ShowEditHyperlinkColumn = true;

			// Create redirect on row click
			string pkFieldName = Dionysos.Data.LLBLGen.LLBLGenEntityUtil.GetPkFieldInfo((IEntity)Dionysos.DataFactory.EntityFactory.GetEntity(this.EntityName)).Name;
			string methodName = string.Format("{0}redirectToEditPage", pkFieldName);
			string editPage = this.ResolveUrl(this.EntityPageUrl) + string.Format("?entity={0}&mode=edit&id=", this.EntityName);
			this.MainGridView.ClientSideEvents.RowDblClick = string.Format(@"function(s, e) {{ s.GetRowValues(e.visibleIndex, '{0}', {1});  }}", pkFieldName, methodName);			
			this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), methodName, string.Format("function {0}(values) {{ window.location = '{1}' + values; }}", methodName, editPage), true);

			// Create a javascript function for deleting
			string deleteMethodName = string.Format("{0}redirectToDeletePage", pkFieldName);
			string deletePage = editPage.Replace("edit", "delete");
			string confirmationMessage = string.Format("Weet u zeker dat u deze {0} wilt verwijderen?", EntityInformationUtil.GetEntityInformation(this.EntityName).FriendlyName.ToLower());
			this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), deleteMethodName, string.Format("\r\nfunction {0}(entityId) {{ if(confirm('{2}')) {{ window.location = '{1}' + entityId; }} }}", deleteMethodName, deletePage, confirmationMessage), true);

			// The hover is created on HtmlRowCreated
			this.MainGridView.HtmlRowCreated += new DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventHandler(MainGridView_HtmlRowCreated);
		}

		void PageOverviewDataSourceCollection_Load(object sender, EventArgs e)
		{
			this.ConfigureGridView();
			if (this.EntityPageUrl.Length == 0)
			{
				this.MultiValidatorDefault.AddError("LET OP: De EntityPageUrl is nog niet gevuld!");
				this.Validate();
			}
		}

		/// <summary>
		/// Method to load the DataSource
		/// </summary>
		/// <returns></returns>
		public abstract override bool InitializeDataSource();

		/// <summary>
		/// Method to bind the DataSource to the relevant Control(s)
		/// </summary>
		/// <returns></returns>
		public override bool InitializeDataBindings()
		{
			this.MainGridView.DataSourceID = this.DataSourceId;

			return true;
		}

		/// <summary>
		/// Get the first GridView of the subpanel and use it as MainGridView
		/// </summary>
		/// <returns></returns>
		private GridViewEntityCollection GetMainGridView()
		{
			// Walk through the control collection of this page
			for (int i = 0; i < this.ControlList.Count; i++)
			{
				// Get a control from the control collection
				// and check whether the control is a data grid
				Control control = this.ControlList[i];
                if (control is GridViewEntityCollection)
				{
                    return control as GridViewEntityCollection;
				}
			}
			return null;
		}

		#endregion

		#region Properties

		/// <summary>
		/// The GridView that should be populated and handled by the logic of the PageEntityCollection
		/// </summary>
		public GridViewEntityCollection MainGridView
		{
			get
			{
				if (this.mainGridView == null)
				{
					// On first call set the ClientInstanceName					
					this.mainGridView = this.GetMainGridView();
					this.mainGridView.ClientInstanceName = "gridView" + this.UniqueID;
				}
				return this.mainGridView;
			}
			set
			{
				this.mainGridView = value;
			}
		}

        /// <summary>
        /// Gets or sets the currently selected view
        /// </summary>
        public IView SelectedView
        {
            get
            {
				// Get the default views for this type
				if (this.selectedView == null)
				{
					this.defaultViews = ViewUtil.GetDefaultViews(this.Page.GetType().BaseType.FullName, this.EntityName);
					if (this.defaultViews.Count == 0)
						throw new TechnicalException(string.Format("No default views found for UIElement '{0}' and entity name '{1}'. Check whether the view exists in the master database and in the local database.", this.Page.GetType().BaseType.FullName, this.EntityName));
					else if (this.defaultViews.Count > 0)
						this.selectedView = this.defaultViews[0];
				}

                return this.selectedView;
            }
            set
            {
                this.selectedView = value;
            }
        }

        /// <summary>
        /// Gets or sets the default views
        /// </summary>
        public List<IView> ViewsDefault
        {
            get
            {
                return this.defaultViews;
            }
            set
            {
                this.defaultViews = value;
            }
        }

        ///// <summary>
        ///// Gets or sets the custom views
        ///// </summary>
        //public List<IView> ViewsCustom
        //{
        //    get
        //    {
        //        return this.customViews;
        //    }
        //    set
        //    {
        //        this.customViews = value;
        //    }
        //}

		#endregion

        #region Event handlers

        private void PageOverviewDataSourceCollection_Init(object sender, EventArgs e)
        {            
        }

        #endregion
    }
}
