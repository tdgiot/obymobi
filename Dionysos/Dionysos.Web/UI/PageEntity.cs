using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Delegates.Web;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Abstract gui class for displaying webpages containing entities
	/// </summary>
	public abstract class PageEntity : Dionysos.Web.UI.PageDefault, Dionysos.Interfaces.IGuiEntity
	{
        public PageEntity()
        {
            this.PreInit += PageEntity_PreInit;
        }
        
		/// <summary>
		/// Adds an data item to the data entity of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if adding was succesfull, False if not
		/// </returns>
		public abstract bool Add();

		/// <summary>
		/// Saves the current data entity and reloads the page using it's current EntityId. This has to be overridden in subclasses to have access to the EntityId
		/// </summary>
		/// <returns>
		/// True if saving was successfull, False if not
		/// </returns>
		public abstract bool Apply();

		/// <summary>
		/// Redirects to the previous page using the ReturnUrl query string, Crumbles, ReturnUrl property or the default convention.
		/// </summary>
		public override void Cancel()
		{
			if (QueryStringHelper.TryGetValue("ReturnUrl", out string returnUrl))
			{
				this.Response.Redirect(returnUrl, true);
			}
			else if (!String.IsNullOrEmpty(this.ReturnUrl))
			{
				this.Response.Redirect(this.ReturnUrl, true);
			}
            else if (this.Crumbles.Count > 0)
            {
                base.Cancel();
            }
			else
			{
				this.Response.Redirect(this.Request.Path.Replace(".aspx", "s.aspx"), true);
			}
		}

		/// <summary>
		/// Closes the page and returns to the referring page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		public abstract void Close();

		/// <summary>
		/// Deletes the current data entity of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if deletion was successfull, False if not
		/// </returns>
		public abstract bool Delete();

		/// <summary>
		/// Disables the field.
		/// </summary>
		/// <param name="control">The control.</param>
		/// <param name="fieldName">Name of the field.</param>
		public void DisableField(Control control, string fieldName)
		{
			foreach (Control childControl in control.Controls)
			{
				if (!String.IsNullOrEmpty(childControl.ID) &&
					(String.IsNullOrEmpty(fieldName) || childControl.ID.RemoveLeadingLowerCaseCharacters().Equals(fieldName)) &&
					childControl is WebControl &&
					childControl is IBindable &&
					((IBindable)childControl).UseDataBinding)
				{
					((WebControl)childControl).Enabled = false;
				}
				else
				{
					this.DisableField(childControl, fieldName);
				}
			}
		}

		/// <summary>
		/// Disables the fields.
		/// </summary>
		public void DisableFields()
		{
			this.DisableFields(this);
		}

		/// <summary>
		/// Disables the fields.
		/// </summary>
		/// <param name="control">The control.</param>
		public void DisableFields(Control control)
		{
			this.DisableField(control, null);
		}

		/// <summary>
		/// Edits the current data entity of the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if editing was succesfull, False if not
		/// </returns>
		public abstract bool Edit();

		/// <summary>
		/// Gets the EntityName, if not set in OnInit, from QueryStringHelper and finally via the FileName of the Page
		/// </summary>
		public virtual void GetEntityName()
		{
			// If the EntityName is empty (could be set by
			if (String.IsNullOrEmpty(this.EntityName))
			{
				string entityName;
				if (!QueryStringHelper.TryGetValue(QueryString.EntityType, out entityName))
				{
					entityName = System.IO.Path.GetFileNameWithoutExtension(this.Request.Path);
				}

				this.EntityName = entityName;
			}
		}

		/// <summary>
		/// Initializes the data data bindings of the controls on the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if initialization was succesful, False if not
		/// </returns>
		public abstract bool InitializeDataBindings();

		/// <summary>
		/// Initializes the entity. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if initialization was succesful, False if not
		/// </returns>
		public abstract bool InitializeEntity();

		/// <summary>
		/// Initializes the page. This is a virtual method and can be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if initialization was succesful, False if not
		/// </returns>
		public virtual bool InitializeGui()
		{
			return this.InitializeDataBindings();
        }

        /// <summary>
        /// Processes the Request.QueryStringHelper instance. This is a virtual method and can be overridden in subclasses.
        /// </summary>
        /// <exception cref="InvalidLinkException">Deze pagina is aangeroepen via een ongeldige link.</exception>
        public virtual void ProcessQueryString()
		{
			// Check whether an entity name and a page mode have been passed in the querystring

			// Set the entity name and page based on the querystring
			if (this.PageMode == PageMode.None)
			{
				string mode;
				PageMode pageMode;
				if (QueryStringHelper.TryGetValue(QueryString.PageMode, out mode) &&
					EnumUtil.TryParse(mode, true, out pageMode))
				{
					this.PageMode = pageMode;
				}
				else
				{
					this.PageMode = PageMode.Edit;
				}
			}

			// We need a primary key when we are viewing or editing an entity
			if (this.PageMode != PageMode.Add)
			{
				// Check whether a primary key is specified in the querystring
				int entityId;
				if (this.EntityId != null)
				{
					// GK Nothing to worry about, the Entity Id has been set in OnInit or in another way from the code
				}
				else if (!QueryStringHelper.TryGetValue(QueryString.EntityId, out entityId))
				{
					throw new InvalidLinkException("Deze pagina is aangeroepen via een ongeldige link.");
				}
				else
				{
					// Set the current primary key based on the querystring
					this.EntityId = entityId;
				}
			}
		}

		/// <summary>
		/// Refreshes the page. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if refreshing was succesful, False if not
		/// </returns>
		public abstract bool Refresh();

		/// <summary>
		/// Saves the current data entity of the page and runs Save() on all SubPanels which are ICustomDataHandling. This is an abstract method and must be overridden in subclasses.
		/// </summary>
		/// <returns>
		/// True if saving was successful, False if not
		/// </returns>
		public virtual bool SaveCommand()
        {
            if (this.SaveTransactionToUse != null)
            {
                return this.SaveUsingTransaction();
            }

            return this.Save();
        }

        /// <summary>
        /// Save the page in a transaction making sure all changes are roll backed in case an exception occurs.
        /// </summary>
        /// <returns></returns>
        public virtual bool SaveUsingTransaction()
        {
            bool success = false;

            try
            {
                success = this.Save();

                this.SaveTransactionToUse.Commit();
            }
            catch (Exception ex)
            {
                this.SaveTransactionToUse.Rollback();
                throw ex;
            }
            finally
            {
                this.SaveTransactionToUse.Dispose();
            }

            return success;
        }

        /// <summary>
        /// Saves the current data entity of the page and runs Save() on all SubPanels which are ICustomDataHandling. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <returns>
        /// True if saving was successfull, False if not
        /// </returns>
        public virtual bool Save()
        {
            bool success = true;

            if (!this.ValidatePanels())
            {
                return false;
            }

            // Loop through all controls and find Panels
            for (int i = 0; i < this.ControlList.Count; i++)
            {
                try
                {
                    if (this.ControlList[i].Visible && this.ControlList[i] is ISubPanelEntity)
                    {
                        ISubPanelEntity panel = this.ControlList[i] as ISubPanelEntity;
                        if (!panel.Save())
                        {
                            success = false;
                        }
                    }
                    else if (this.ControlList[i].Visible && this.ControlList[i] is ISaveableControl)
                    {
                        ISaveableControl saveableControl = this.ControlList[i] as ISaveableControl;
                        if (!saveableControl.Save())
                        {
                            success = false;
                        }
                    }
                }
                catch (EntitySaveException ex)
                {
                    success = false;
                    this.MultiValidatorDefault.AddError(ex.Message);
                }
            }

            return success;
        }

        /// <summary>
        /// Saved the entity and redirects to the previous page (using the ReturnUrl query string, Crumbles, ReturnUrl property or the default convention).
        /// </summary>
        /// <returns>
        ///   <c>true</c> if the entity is successfully saved; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool SaveAndGo()
		{
			// TODO: GK Implement ReturnUrl
			if (this.IsValid && this.SaveCommand())
			{
				this.Cancel();
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Lazy SaveAndNew, simple save's and directs to: this.Request.Path + "?mode=add"
		/// </summary>
		/// <returns>
		/// If save succeeded
		/// </returns>
		public virtual bool SaveAndNew()
		{
			if (this.IsValid && this.SaveCommand())
			{
				// Redirect to Page in Add mode
				string url = this.Request.Path + "?mode=add";

				// Rebuild URL with mode Add and no Id
				for (int i = 0; i < this.Request.QueryString.AllKeys.Length; i++)
				{
					if (this.Request.QueryString.AllKeys[i] != null &&
						this.Request.QueryString.AllKeys[i].ToLowerInvariant() != "mode" &&
						this.Request.QueryString.AllKeys[i].ToLowerInvariant() != "id")
					{
						string key = this.Request.QueryString.AllKeys[i];
						url += String.Format("&{0}={1}", key, this.Request.QueryString[key]);
					}
				}

				this.Response.Redirect(url, false);
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// Saves the current data entity of the page and runs Save() on all SubPanels which are ICustomDataHandling. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        /// <returns>
        /// True if saving was successfull, False if not
        /// </returns>
        public bool ValidatePanels()
        {
            bool success = true;

            // Loop through all controls and find Panels
            for (int i = 0; i < this.ControlList.Count; i++)
            {
                try
                {
                    if (this.ControlList[i].Visible && this.ControlList[i] is ISubPanelEntity)
                    {
                        ISubPanelEntity panel = this.ControlList[i] as ISubPanelEntity;
                        if (!panel.Validate())
                        {
                            success = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    this.MultiValidatorDefault.AddError(ex.Message);
                }
            }

            this.Validate();

            return success;
        }
		
        public bool UseEntityInformation { get; set; } = true;

        /// <summary>
        /// Transaction to use when saving the page.
        /// </summary>
        public ITransaction SaveTransactionToUse { get; set; }

        /// <summary>
		/// Provide the DataErrorInfo object for the Entity
		/// (Is not implemented a hard exception is thrown)
		/// </summary>
		/// <value>
		/// The data error information.
		/// </value>
		public IDataErrorInfo DataErrorInfo
		{
			get
			{
				return (IDataErrorInfo)this.DataSource;
			}
		}

		/// <summary>
		/// Gets or sets the data entity of the page
		/// </summary>
		public virtual IEntity DataSource { get; set; }

		/// <summary>
		/// Gets or sets the value of the primary key of the entity of this page
		/// </summary>
		/// <value>
		/// The entity unique identifier.
		/// </value>
		public object EntityId
		{
			get
			{
				return (this.ViewState["EntityId"] as int?) ?? (this.ViewState["EntityId"] = QueryStringHelper.GetValue<int>(PageEntity.QueryString.EntityId));
			}
			set
			{
				this.ViewState["EntityId"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the entity for the data entity of this page
		/// </summary>
		public virtual string EntityName
		{
			get
			{
				return (this.ViewState["EntityName"] as string) ?? String.Empty;
			}
			set
			{
				this.ViewState["EntityName"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the Lynx_media.Web.PageMode for this page
		/// </summary>
		/// <value>
		/// The page mode.
		/// </value>
		public PageMode PageMode
		{
			get
			{
				return (this.ViewState["PageMode"] as PageMode?) ?? PageMode.None;
			}
			set
			{
				this.ViewState["PageMode"] = value;
			}
		}

		/// <summary>
		/// Style of controls which will be rendered as Read Only (Default: DisabledControls)
		/// </summary>
		/// <value>
		/// The render style.
		/// </value>
		public RenderStyle RenderStyle
		{
			get
			{
				return (this.ViewState["RenderStyle"] as RenderStyle?) ?? RenderStyle.DisabledControls;
			}
			set
			{
				this.ViewState["RenderStyle"] = value;
			}
		}
 
 
        void PageEntity_PreInit(object sender, EventArgs e)
        {
            // GK Moved here from OnInit, as these need nothing to wait for, and the earlier the better - I need it.

            // Retrieve information from the querystring
            this.ProcessQueryString();

            // Determine entity name
            this.GetEntityName();
        }

		/// <summary>
		/// Raises the <see cref="E:Init" /> event.
		/// </summary>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

            // Move to PreInit: this.ProcessQueryString();, this.GetEntityName();

			// Initialize the entity
			bool preventLoad = false;
			this.OnDataSourceLoad(ref preventLoad);
			if (!preventLoad)
			{
				this.InitializeEntity();
			}

			// Delete Entity
			if (this.PageMode == PageMode.Delete)
			{
				if (this.Delete())
				{
					this.Cancel();
				}
			}

			// Fire loaded after DELETE, because most of the time it's of no use.
			this.OnDataSourceLoaded();
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Load" /> event.
		/// </summary>
		/// <param name="e">The <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		protected override void OnLoad(EventArgs e)
		{            	            
            this.InitializeGui();

            base.OnLoad(e);
		}

		public static class QueryString
		{
			/// <summary>
			/// The query string parameter for the entity type that is passed to this page.
			/// </summary>
			public static string EntityType = "entity";

			/// <summary>
			/// The query string parameter for the id of the entity that must be loaded.
			/// </summary>
			public static string EntityId = "id";

			/// <summary>
			/// The query string parameter for setting the page mode for working with the data.
			/// </summary>
			public static string PageMode = "mode";
		}

		/// <summary>
		/// Event fired before loading the entity.
		/// </summary>
		public event DataSourceLoadHandler DataSourceLoad;

		/// <summary>
		/// Event fired after loading the entity.
		/// </summary>
		public event DataSourceLoadedHandler DataSourceLoaded;

		/// <summary>
		/// Called before loading the entity.
		/// </summary>
		/// <param name="preventLoad">If set to <c>true</c> prevents loading.</param>
		protected void OnDataSourceLoad(ref bool preventLoad)
		{
			if (this.DataSourceLoad != null)
			{
				this.DataSourceLoad(this, ref preventLoad);
			}
		}

		/// <summary>
		/// Called after loadeding the entity.
		/// </summary>
		protected void OnDataSourceLoaded()
		{
			if (this.DataSourceLoaded != null)
			{
				this.DataSourceLoaded(this);
			}
		}        

    }
}