﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Reflection;
using Dionysos.Interfaces;
using System.Reflection;
using Dionysos.Web.UI.WebControls;


namespace Dionysos.Web.UI
{
	public abstract class PageQueryStringDataBinding : PageDefault
	{
		#region Fields

		/// <summary>
		/// Checkbox list value seperator.
		/// </summary>
		public const char CheckBoxListValuesSeperator = '|';

		#endregion

		#region Methods

		/// <summary>
		/// Init Method, First loads DefaultValuesToControls, then binds the QueryString values to the Controls (unless it's postback)
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.SetDefaultValuesToControls();
			this.BindQueryStringToControls();
		}

		/// <summary>
		/// Binds the query string to controls.
		/// </summary>
		protected virtual void BindQueryStringToControls()
		{
			foreach (System.Web.UI.Control control in this.ControlList)
			{
				// Get and validate key
				string key = control.ID.RemoveLeadingLowerCaseCharacters();
				if (String.IsNullOrEmpty(key)) continue;

				// Get and set value
				string stringValue;
				if (QueryStringHelper.TryGetValue(key, out stringValue))
				{
					IBindable bindableControl = control as IBindable;
					if (bindableControl != null &&
						bindableControl.UseDataBinding &&
						Member.HasProperty(control, "Value"))
					{
						// Set value for databindable control
						object valueToSet = null;

						// Use Value property with the same type as the datasource or an unspecified type (object)
						PropertyInfo propertyInfo = Member.GetLowestProperty(control, "Value");
						if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(int) ||
							propertyInfo.PropertyType.UnderlyingSystemType == typeof(int?))
						{
							int value;
							if (Int32.TryParse(stringValue, out value)) valueToSet = value;
						}
						else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(decimal) ||
									propertyInfo.PropertyType.UnderlyingSystemType == typeof(decimal?))
						{
							decimal value;
							if (Decimal.TryParse(stringValue, out value)) valueToSet = value;
						}
						else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(DateTime) ||
									propertyInfo.PropertyType.UnderlyingSystemType == typeof(DateTime?))
						{
							DateTime value;
							if (DateTime.TryParse(stringValue, out value)) valueToSet = value;
						}
						else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(bool))
						{
							bool value;
							if (Boolean.TryParse(stringValue, out value)) valueToSet = value;
							else valueToSet = false;
						}
						else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(string))
						{
							valueToSet = stringValue;
						}
						else throw new NotImplementedException(String.Format("PageQueryStringDataBinding hasn't got an implementation for Type: '{0}' used by field '{1}'", propertyInfo.PropertyType.UnderlyingSystemType.ToString(), key));

						propertyInfo.SetValue(control, valueToSet, null);
					}
					else
					{
						// Set value for custom controls
						CheckBoxList checkBoxList = control as CheckBoxList;
						if (checkBoxList != null)
						{
							// Set value for check box list
							checkBoxList.SelectMultipleItems(stringValue, PageQueryStringDataBinding.CheckBoxListValuesSeperator);
						}
					}
				}
			}
		}

		/// <summary>
		/// Gets the query string instance with values.
		/// </summary>
		/// <returns>
		/// An instance of QueryStringHelper with all control values.
		/// </returns>
		protected virtual QueryStringHelper GetQueryStringInstanceWithValues()
		{
			QueryStringHelper qs = new QueryStringHelper();

			foreach (var control in this.ControlList)
			{
				// Get and validate key
				string key = control.ID.RemoveLeadingLowerCaseCharacters();
				if (String.IsNullOrEmpty(key)) continue;

				// Get and add value
				IBindable bindableControl = control as IBindable;
				if (bindableControl != null &&
					bindableControl.UseDataBinding &&
					Member.HasProperty(control, "Value"))
				{
					// Add value from databindable control
					qs.AddItem(key, Member.InvokeProperty(control, "Value"));
				}
				else
				{
					// Get value from custom controls
					CheckBoxList checkBoxList = control as CheckBoxList;
					if (checkBoxList != null)
					{
						// Add value from check box list
						qs.AddItem(key, checkBoxList.SelectedValues(PageQueryStringDataBinding.CheckBoxListValuesSeperator));
					}
				}
			}

			return qs;
		}

        /// <summary>
        /// Reload the page by redirecting to the RawUrl including the current values of the controls
        /// </summary>        
        protected void RedirectWithControlValues()
        {
            this.Response.Redirect(this.GetQueryStringInstanceWithValues().MergeQuerystringWithRawUrl(), true); 
        }

        /// <summary>
        /// Reload the page by redirecting to the RawUrl including the current values of the controls
        /// </summary>
        [Obsolete("Use RedirectWithControlValues")]
        public void RefreshWithControlValues()
        {
            this.RefreshWithControlValues();
        }

		/// <summary>
		/// Use this method to set the default value for each control
		/// </summary>
		protected abstract void SetDefaultValuesToControls();

		#endregion
	}
}
