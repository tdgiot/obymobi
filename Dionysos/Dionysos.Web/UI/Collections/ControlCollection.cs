using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Collection class used for storing System.Web.UI.Control instances in
    /// </summary>
    public class ControlCollection : ICollection<Control>
    {
        #region Fields

        private IList<Control> items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ControlCollection class
        /// </summary>	
        public ControlCollection()
        {
            this.items = new List<Control>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an System.Web.UI.Control instance to the collection
        /// </summary>
        /// <param name="control">The System.Web.UI.Control instance to add to the collection</param>
        public void Add(Control control)
        {
            this.items.Add(control);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified System.Web.UI.Control instance is already in the collection
        /// </summary>
        /// <param name="control">The System.Web.UI.Control instance to check</param>
        /// <returns>True if the System.Web.UI.Control instance is in the collection, False if not</returns>
        public bool Contains(Control control)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Control) == control)
                {
                    contains = true;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Control[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified System.Web.UI.Control instance from this collection
        /// </summary>
        /// <param name="control">The System.Web.UI.Control instance to remove</param>
        public bool Remove(Control control)
        {
            this.items.Remove(control);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.ControlCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Control> GetEnumerator()
        {
            return (IEnumerator<Control>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.ControlCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Custom FindControl on this custom Dionysos Control Collection
        /// </summary>
        /// <returns>Found Control or null</returns>
        public Control FindControl(string id)
        {
            Control retval = null;
            for(int i = 0; i < this.Count; i++)
            {
                if (this[i].ID == id)
                {
                    retval = this[i];
                    break;
                }
            }

            return retval;
        }

		/// <summary>
		/// Recursively collects the controls from the page and places them into the collection
		/// </summary>
		public void CollectControls(Page page)
		{			
			for (int i = 0; i < page.Controls.Count; i++)
			{
				Control control = page.Controls[i];				
				this.CollectControls(control);				
			}		
		}

		/// <summary>
		/// Recursively collects the controls form the controlcolleciton 
		/// </summary>
		/// <param name="controlCollection"></param>
		public void CollectControls(System.Web.UI.ControlCollection controlCollection)
		{
			for (int i = 0; i < controlCollection.Count; i++)
			{
				Control control = controlCollection[i];
				this.CollectControls(control);
			}		
		}

		/// <summary>
		/// Recursively collects the controls from the specified control recursively
		/// </summary>
		/// <param name="control">The System.Web.UI.Control to get the subcontrols from</param>
		public void CollectControls(Control control)
		{
			// check for null, if so an exception will be thrown
			Instance.ArgumentIsEmpty(control, "control");

			if (control.GetType().ToString().ToLower() == "DevExpress.Web.ASPxGridView.ASPxGridView" || (control.Controls.Count == 0) || (Dionysos.TypeUtil.GetTypeString(control) == "Dionysos.Web.UI.WebControls.TextBoxRelation"))
			{
				this.Add(control);
			}
			else
			{
				this.Add(control);

				for (int i = 0; i < control.Controls.Count; i++)
				{
					this.CollectControls(control.Controls[i]);
				}
			}

			return;
		}

		/// <summary>
		/// Convert this Dionysos ControlCollection to a System.Web.UI.ControlCollection
		/// </summary>
		/// <returns>All controls in a System.Web.UI.ControlCollection</returns>
		public System.Web.UI.ControlCollection GetAsSystemWebUiControlCollection()
		{
			System.Web.UI.ControlCollection toReturn = new System.Web.UI.ControlCollection(null);

			for (int i = 0; i < this.Count; i++)
			{
				toReturn.Add(this[i]);
			}

			return toReturn;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the amount of items in this collection
		/// </summary>
		public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an System.Web.UI.Control instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the System.Web.UI.Control instance to get</param>
        /// <returns>An System.Web.UI.Control instance</returns>
        public Control this[int index]
        {
            get
            {
                return this.items[index] as Control;
            }
        }

        #endregion
    }
}
