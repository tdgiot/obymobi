﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Collection of LinkedItem objects
	/// </summary>
	public class LinkedItemCollection : List<LinkedItem>
	{
		#region Properties

		/// <summary>
		/// Gets the maximum VersionId within this collection.
		/// </summary>
		public long VersionId
		{
			get
			{
				return this.Max(l => l.VersionId);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Add method for backwards compatability (used to be a List of strings)
		/// </summary>
		/// <param name="link">Relative link to CSS or JS file</param>
		[Obsolete("Use overloaded Add with sortOrder, MasterPages > 100, Pages > 1000.", true)]
		public void Add(string link)
		{
			this.Add(new LinkedItem(link));
		}

		/// <summary>
		/// Add method for quick insertion of links
		/// </summary>
		/// <param name="link">Relative link to CSS or JS file</param>
		/// <param name="sortOrder">SortOrder to sort within the set (defaults to 0)</param>
		public void Add(string link, int sortOrder)
		{
			LinkedItem item = new LinkedItem(link);
			item.SortOrder = sortOrder;
			this.Add(item);
		}

		/// <summary>
		/// Add method for quick insertion of links
		/// </summary>
		/// <param name="link">Relative link to CSS or JS file</param>
		/// <param name="sortOrder">SortOrder to sort within the set (defaults to 0)</param>
		/// <param name="setName">SetName to use, defaults to NULL</param>
		public void Add(string link, int sortOrder, string setName)
		{
			LinkedItem item = new LinkedItem(link);
			item.SortOrder = sortOrder;
			item.SetName = setName;
			this.Add(item);
		}

		/// <summary>
		/// Returns a List of collections, combined on SetName
		/// </summary>
		/// <returns>List of LinkedItemCollection</returns>
		public List<LinkedItemCollection> GetSets()
		{
			SortedDictionary<String, LinkedItemCollection> sortedSets = new SortedDictionary<String, LinkedItemCollection>();

			// Create new collection
			foreach (LinkedItem item in this)
			{
				// Add new collection, if no available or this is a new SetName
				if (!sortedSets.ContainsKey(item.SetName))
				{
					sortedSets.Add(item.SetName, new LinkedItemCollection());
				}
				sortedSets[item.SetName].Add(item);
			}

			// Convert dictionary to list
			List<LinkedItemCollection> sets = new List<LinkedItemCollection>();
			foreach (LinkedItemCollection item in sortedSets.Values)
			{
				sets.Add(item);
			}
			return sets;
		}

		#endregion
	}

	/// <summary>
	/// Creates a new LinkedItem class, used for attaching CSS and Javascript files to the page.
	/// </summary>
	public class LinkedItem : IComparable
	{
		#region Fields

		/// <summary>
		/// The relative link.
		/// </summary>
		private readonly string link;

		/// <summary>
		/// The set name.
		/// </summary>
		private string setName = "_default";

		/// <summary>
		/// The version id.
		/// </summary>
		private long? versionId;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the relative link.
		/// </summary>
		public string Link
		{
			get
			{
				return this.link;
			}
		}

		/// <summary>
		/// Gets or sets the sort order.
		/// </summary>
		public int SortOrder { get; set; }

		/// <summary>
		/// Gets or sets the set name, used to create multiple sets.
		/// </summary>
		/// <remarks>
		/// When a set name is set, the sort order is used per set and sets get ordered alphabetically.
		/// </remarks>
		public string SetName
		{
			get
			{
				return this.setName;
			}
			set
			{
				if (value == null) throw new InvalidOperationException("The set name must have a value.");
				this.setName = value;
			}
		}

		/// <summary>
		/// Gets or sets the version id of the linked file.
		/// </summary>
		public long VersionId
		{
			get
			{
				if (!this.versionId.HasValue)
				{
					try
					{
						this.versionId = File.GetLastWriteTimeUtc(HttpContext.Current.Server.MapPath(this.Link)).Ticks;
					}
					catch
					{
						this.versionId = 0;
					}
				}

				return this.versionId.Value;
			}
			set
			{
				this.versionId = value;
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="LinkedItem"/> class.
		/// </summary>
		/// <param name="link">The relative link.</param>
		public LinkedItem(string link)
		{
			this.link = link;
		}

		#endregion

		#region IComparable Members

		/// <summary>
		/// Custom CompareTo method for sorting on SortOrder
		/// </summary>
		/// <param name="obj">Object to compare to</param>
		/// <returns>Lower than zero (instance is less than obj), zero (instance of equal to obj) or greater than zero (instance of greater than obj or obj is a NULL reference)</returns>
		public int CompareTo(object obj)
		{
			LinkedItem other = obj as LinkedItem;
			if (other != null)
			{
				int value = this.SortOrder.CompareTo(other.SortOrder);
				if (value == 0) value = -1;

				return value;
			}
			else
			{
				return 1;
			}
		}

		#endregion
	}
}