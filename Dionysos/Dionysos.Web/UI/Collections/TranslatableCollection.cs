using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Dionysos.Interfaces;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Collection class used for storing Lynx-media.Interfaces.ITranslatable instances in
    /// </summary>
    public class TranslatableCollection : ICollection<ITranslatable>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the TranslatableCollection class
        /// </summary>	
        public TranslatableCollection()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Lynx-media.Interfaces.ITranslatable instance to the collection
        /// </summary>
        /// <param name="control">The Lynx-media.Interfaces.ITranslatable instance to add to the collection</param>
        public void Add(ITranslatable control)
        {
            this.items.Add(control);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Lynx-media.Interfaces.ITranslatable instance is already in the collection
        /// </summary>
        /// <param name="control">The Lynx-media.Interfaces.ITranslatable instance to check</param>
        /// <returns>True if the Lynx-media.Interfaces.ITranslatable instance is in the collection, False if not</returns>
        public bool Contains(ITranslatable control)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as ITranslatable) == control)
                {
                    contains = true;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(ITranslatable[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Lynx-media.Interfaces.ITranslatable instance from this collection
        /// </summary>
        /// <param name="control">The Lynx-media.Interfaces.ITranslatable instance to remove</param>
        public bool Remove(ITranslatable control)
        {
            this.items.Remove(control);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.TranslatableCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<ITranslatable> GetEnumerator()
        {
            return (IEnumerator<ITranslatable>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.TranslatableCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Lynx-media.Interfaces.ITranslatable instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx-media.Interfaces.ITranslatable instance to get</param>
        /// <returns>An Lynx-media.Interfaces.ITranslatable instance</returns>
        public ITranslatable this[int index]
        {
            get
            {
                return this.items[index] as ITranslatable;
            }
        }

        #endregion
    }
}
