using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Collection class used for storing menu items of type System.Object in
    /// </summary>
    public class MenuItemCollection : ICollection<MenuItem>
    {
        #region Fields

        private ArrayList items;
		private MenuItem parent;

        #endregion

        #region Constructors

		/// <summary>
		/// Constructs an instance of the MenuItemCollection class
		/// </summary>
		/// <param name="parent">Parent MenuItem if it's used a a Child Item collection, </param>
        public MenuItemCollection(MenuItem parent)
        {
			this.parent = parent;
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an System.Web.UI.WebControls.MenuItem instance to the collection and sets it parent based on the parent of this collection
        /// </summary>
        /// <param name="menuItem">The System.Web.UI.WebControls.MenuItem instance to add to the collection</param>
        public void Add(MenuItem menuItem)
        {
			menuItem.Parent = this.parent;
            this.items.Add(menuItem);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the System.Web.UI.WebControls.MenuItem instance is already in the collection
        /// </summary>
        /// <param name="menuItem">The System.Web.UI.WebControls.MenuItem instance to check</param>
        /// <returns>True if the System.Web.UI.WebControls.MenuItem instance is in the collection, False if not</returns>
        public bool Contains(MenuItem menuItem)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as object) == menuItem)
                {
                    contains = true;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(MenuItem[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified System.Web.UI.WebControls.MenuItem instance from this collection
        /// </summary>
        /// <param name="menuItem">The menu item instance to remove</param>
        public bool Remove(MenuItem menuItem)
        {
            this.items.Remove(menuItem);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.MenuItemCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<MenuItem> GetEnumerator()
        {
            return (IEnumerator<MenuItem>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.MenuItemCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Get the first selected item at the specified depth 
        /// </summary>
        /// <param name="depth">The depth from which to retrieve the selected item (currently only implemented for 0 and 1)</param>
        /// <returns>First occurance of a selected item at the specified depth</returns>
        public MenuItem GetSelectedItemAtDepth(int depth)
        {
            MenuItem returnItem = null;
            if (depth > 1)
            {
                throw new NotImplementedException("GetSelectedItemOfDepth is not implemented for a depth of 2 or higher");
            }
            else if (depth < 0)
            {
                throw new NotImplementedException("GetSelectedItemOfDepth is not implemented for a depth of lower than 0");
            }
            else 
            {
                for (int i = 0; i < this.items.Count; i++)
                {
                    MenuItem item = this[i] as MenuItem;
                    if(item != null)
                    {
                        if (depth == 0 && item.Selected)
                        {
                            returnItem = item;                            
                        }
                        else
                        {
                            for (int j = 0; j < item.ChildItems.Count; j++)
                            {
								// GK Was here... found this: (made by himself, including the comment)
								//MenuItem childItem = item.ChildItems[0]; //. ChildItems[j];
								// Replaced with:
								MenuItem childItem = item.ChildItems[j]; //. ChildItems[j];
                                if (childItem.Selected)
                                {
                                    returnItem = childItem;
                                    // Exit loop after it's found:
                                    break;
                                }
                            }
                        }
                    }

                    // Stop loop if the item is found
                    if (returnItem != null)
                    {
                        break;
                    }
                }
            }

            return returnItem;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets a System.Web.UI.WebControls.MenuItem instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the System.Web.UI.WebControls.MenuItem instance to get</param>
        /// <returns>A menu item of type System.Web.UI.WebControls.MenuItem</returns>
        public MenuItem this[int index]
        {
            get
            {
                return this.items[index] as MenuItem;
            }
        }

        #endregion
    }
}
