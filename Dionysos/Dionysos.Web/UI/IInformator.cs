﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI
{
	public interface IInformator
	{
		/// <summary>
		/// Text to display in the ValidationSummary
		/// </summary>
		string Text { get; set; }

		/// <summary>
		/// Type of Informator, which determines how it's displayed in the ValidationSummary
		/// </summary>
		InformatorType InformatorType { get; set; }
	}
}
