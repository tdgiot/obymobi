﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Factory class which is being used to retrieve menu structures from a database
    /// </summary>
    public class LLBLGenMenuFactory : IMenuFactory
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.LLBLGenMenuFactory class
        /// </summary>
        public LLBLGenMenuFactory()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the menu and return an object containing the menu
        /// </summary>
        /// <returns>An object containing the menu</returns>
        public object LoadMenu(string typeName)
        {
            // Create and initialize a MenuItemCollection instance
            MenuItemCollection menuItemCollection = new MenuItemCollection(null);

            if (Instance.ArgumentIsEmpty(typeName, "typeName"))
            {
                // Parameter 'typeName' is empty
            }
            else
            {
                // Get the UIElement entity based on the typename
                IEntity uiElementEntity = this.GetUIElement(typeName);

                // Get the modules
                SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection moduleCollection = this.GetModules();

                for (int i = 0; i < moduleCollection.Count; i++)
                {
                    // Create (parent) item from module 
                    IEntity module = moduleCollection[i];

                    MenuItem item = new MenuItem();

                    item.Enabled = true;
                    item.ImageUrl = module.Fields["IconPath"].CurrentValue.ToString();

					IEntity temp = Dionysos.Reflection.Member.InvokeProperty(module, "UiElementTypeNameFull") as IEntity;
                    item.NavigateUrl = temp.Fields["Url"].CurrentValue.ToString();

                    item.Text = module.Fields["NameShort"].CurrentValue.ToString();
                    item.ToolTip = module.Fields["NameFull"].CurrentValue.ToString();
                    item.Value = module.Fields["NameSystem"].CurrentValue.ToString();

                    menuItemCollection.Add(item);

                    // If this module is parent of uielement make it selected
                    if (uiElementEntity.Fields["ModuleId"].CurrentValue.ToString() == module.Fields["ModuleId"].CurrentValue.ToString())
                    {
                        item.Selected = true;
                    }

                    // We load the childeren if the item is selected, because then they'll be
                    // used as submenu items, or when the function parameter forces it to do so.
                    if (item.Selected) //|| loadAllChilderen)
                    {
                        // Create childs for uielements                  
                        SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection uiElements = DataFactory.EntityCollectionFactory.GetEntityCollection("UIElement") as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;

                        PredicateExpression filter2 = new PredicateExpression();
                        filter2.Add(new FieldCompareValuePredicate(uiElementEntity.Fields["DisplayInMenu"], ComparisonOperator.Equal, true));
                        filter2.Add(new FieldCompareValuePredicate(uiElementEntity.Fields["ModuleId"], ComparisonOperator.Equal, module.Fields["ModuleId"].CurrentValue));

                        SortExpression sort2 = new SortExpression();
                        sort2.Add(new SortClause(uiElementEntity.Fields["SortOrder"], SortOperator.Ascending));

                        uiElements.GetMulti(filter2, 0, sort2, null);

                        for (int j = 0; j < uiElements.Count; j++)
                        {
                            IEntity element = uiElements[j];

                            MenuItem elementitem = new MenuItem();

                            elementitem.Enabled = true;
                            elementitem.ImageUrl = element.Fields["IconPath"].CurrentValue.ToString();
                            elementitem.NavigateUrl = element.Fields["Url"].CurrentValue.ToString();
                            elementitem.Text = element.Fields["NameShort"].CurrentValue.ToString();
                            elementitem.ToolTip = element.Fields["NameFull"].CurrentValue.ToString();
                            elementitem.Value = element.Fields["TypeNameFull"].CurrentValue.ToString();

                            item.ChildItems.Add(elementitem);

                            // If this is the selected item mark the item and it's parent as selected
                            if (element.Fields["UIElementId"].CurrentValue.ToString() == uiElementEntity.Fields["UIElementId"].CurrentValue.ToString())
                            {
                                elementitem.Selected = true;
                                elementitem.Parent.Selected = true;
                            }
                        }
                    }
                }
            }

            return menuItemCollection;
        }

        private IEntity GetUIElement(string typeName)
        {
            IEntity returnEntity = null;
            
            SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection uiElementCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("UIElement") as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
            IEntity uiElementEntity = DataFactory.EntityFactory.GetEntity("UIElement") as IEntity;

            if (Instance.Empty(uiElementCollection))
            {
                throw new EmptyException("Variable 'uiElementCollection' is empty.");
            }
            else if (Instance.Empty(uiElementEntity))
            {
                throw new EmptyException("Variable 'uiElementEntity' is empty.");
            }
            else
            {
                // Create and initialize a filter instance
                PredicateExpression filter = new PredicateExpression();
                filter.Add(new FieldCompareValuePredicate(uiElementEntity.Fields["TypeNameFull"], ComparisonOperator.Equal, typeName));

                // Retrieve the UIElement items from the database
                // using the constructed filter
                uiElementCollection.GetMulti(filter);

                if (uiElementCollection.Count == 0)
                {
                    throw new EmptyException(string.Format("No UIElements found for the type '{0}'", typeName));
                }
                else if (uiElementCollection.Count == 1)
                {
                    // One UIElement item is found
                    // so get it from the collection
                    returnEntity = uiElementCollection[0];
                }
            }

            return returnEntity;
        }

        private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection GetModules()
        {
            // Get allowed modules
            SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection moduleCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("Module") as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
            IEntity moduleEntity = DataFactory.EntityFactory.GetEntity("Module") as IEntity;

            if (Instance.Empty(moduleCollection))
            {
                throw new EmptyException("Variable 'moduleCollection' is empty.");
            }
            else if (Instance.Empty(moduleEntity))
            {
                throw new EmptyException("Variable 'moduleEntity' is empty.");
            }
            else
            {
                // Create and initialize a filter instance
                SortExpression sort = new SortExpression();
                sort.Add(new SortClause(moduleEntity.Fields["SortOrder"], SortOperator.Ascending));

                // Retrieve the module items from the database
                // using the constructed sort expression
                moduleCollection.GetMulti(null, 0, sort);

                if (moduleCollection.Count == 0)
                {
                    throw new EmptyException("No modules found in datasource.");
                }
            }

            return moduleCollection;
        }

        #endregion
    }
}
