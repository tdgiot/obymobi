﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web.UI
{
	public enum InformatorType : int
	{
		/// <summary>
		/// Hidden
		/// </summary>
		Hidden = 0,
		/// <summary>
		/// Information
		/// </summary>
		Information = 1000,
		/// <summary>
		/// Warning
		/// </summary>
		Warning = 2000,
	}
}
