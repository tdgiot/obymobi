using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Caching;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// An abstract class for pages to display reports
	/// </summary>
	public abstract class PageReport : PageDefault
	{
		#region Fields

		private bool cacheSessionSpecific = false;
		private bool useCaching = false;
		private bool preventAutomaticReportLoad = false;
		private string reportName;
		private object report = null;
		private object reportViewer = null;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating whether this <see cref="PageReport"/> is cached.
		/// </summary>
		/// <value>
		///   <c>true</c> if cached; otherwise, <c>false</c>.
		/// </value>
		private bool Cached
		{
			get
			{
				if (this.UseCaching)
					return CacheHelper.HasValue(this.ReportName, this.CacheSessionSpecific);
				else
					return false;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to cache the report only for the current session.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the report is cached only for the current session; otherwise, <c>false</c>.
		/// </value>
		public bool CacheSessionSpecific
		{
			get
			{
				return this.cacheSessionSpecific;
			}
			set
			{
				this.cacheSessionSpecific = value;
			}
		}


		/// <summary>
		/// Gets or sets a value indicating whether to prevent automatically loading the report on load.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if automatic report loading is prevented on load; otherwise, <c>false</c>.
		/// </value>
		public bool PreventAutomaticReportLoad
		{
			get
			{
				return this.preventAutomaticReportLoad;
			}
			set
			{
				this.preventAutomaticReportLoad = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the report.
		/// </summary>
		/// <value>
		/// The name of the report.
		/// </value>
		public string ReportName
		{
			get
			{
				return this.reportName;
			}
			set
			{
				this.reportName = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to use caching.
		/// </summary>
		/// <value>
		///   <c>true</c> if caching is used; otherwise, <c>false</c>.
		/// </value>
		public bool UseCaching
		{
			get
			{
				return this.useCaching;
			}
			set
			{
				this.useCaching = value;
			}
		}

		/// <summary>
		/// Gets or sets the report viewer.
		/// </summary>
		/// <value>
		/// The report viewer.
		/// </value>
		public virtual object ReportViewer
		{
			get
			{
				return this.reportViewer;
			}
			set
			{
				this.reportViewer = value;
			}
		}

		/// <summary>
		/// Gets or sets the report.
		/// </summary>
		/// <value>
		/// The report.
		/// </value>
		public virtual object Report
		{
			get
			{
				return this.report;
			}
			set
			{
				this.report = value;
			}
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Raises the <see cref="E:Init"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			if (String.IsNullOrEmpty(this.ReportName)) throw new TechnicalException("The PageReport derived is missing a valid ReportName");

			this.Load += new EventHandler(PageReport_Load);
		}

		/// <summary>
		/// Handles the Load event of the PageReport control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PageReport_Load(object sender, EventArgs e)
		{
			if (!this.PreventAutomaticReportLoad)
			{
				if (this.UseCaching &&
					this.Cached)
				{
					this.RestoreReport();
				}
				else
				{
					this.CreateReport();

					if (this.UseCaching)
					{
						this.StoreReport(this.Report);
					}
				}

				this.BindReportToViewer();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Method that should initialize and load the Report property. Abstract.
		/// </summary>
		public abstract void CreateReport();

		/// <summary>
		/// Bind the report to the viewer
		/// </summary>
		protected abstract void BindReportToViewer();

		/// <summary>
		/// Store the report in the cache
		/// </summary>
		/// <param name="report">Object containing the report</param>
		protected virtual void StoreReport(object report)
		{
			CacheHelper.Add(this.CacheSessionSpecific, this.ReportName, report, null, DateTime.Now.AddMinutes(2), TimeSpan.Zero, CacheItemPriority.Low, null);
		}

		/// <summary>
		/// Restore the report from the Cache
		/// </summary>
		protected virtual void RestoreReport()
		{
		    CacheHelper.TryGetValue(this.ReportName, this.CacheSessionSpecific, out this.report);
		}

		/// <summary>
		/// Remove the report form the Cache
		/// </summary>
		protected virtual void RemoveCache()
		{
			CacheHelper.Remove(this.CacheSessionSpecific, this.ReportName);
		}

		#endregion
	}
}
