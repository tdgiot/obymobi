using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Reflection;

namespace Dionysos.Web.UI
{
	/// <summary>
	/// Gui class for displaying webpages with SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instances
	/// </summary>
	public class PageLLBLGenEntityCollection : PageEntityCollection
	{
		#region Fields

		private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection = null;
		private bool? llblGenPaging = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Lynx_media.Web.UI.PageLLBLGenEntityCollection class
		/// </summary>
		public PageLLBLGenEntityCollection()
		{
			this.DataSourceRelations = new RelationCollection();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the entity collection of the page
		/// Depending on the property LLBLGenPaging, only data of the current page is loaded or the full dataset
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public override bool InitializeEntityCollection()
		{
			this.entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;

			// Init some of the elements needed in the GetMulti later
			SortExpression sort;
			this.InitializeSortExpression(out sort, this.DataSourceRelations);
			this.InitializePredicateExpression();
			bool isSorted = false;
			if (sort.Count > 0)
				isSorted = true;

			// Do the actual entity retrievel
			if (this.LLBLGenPaging)
			{
				// LLBLGen uses 1 based paging, so we need to add 1!
				int currentPageForLlblPaging = this.PagingCurrentPage + 1;

				// Fetch the entities;
				this.entityCollection.GetMulti(this.DataSourceFilter, -1, sort, this.DataSourceRelations, null, currentPageForLlblPaging, this.PagingPageSize);

				if (this.entityCollection.Count < this.PagingPageSize)
				{
					// If count is less than page size, we don't need to fetch the total again
					this.PagingItemCount = this.entityCollection.Count;
				}
				else if (this.entityCollection.Count > 0)
				{
					// MDB Experimental :)
					this.PagingItemCount = (int)Member.InvokeMethod(this.entityCollection, "GetScalar", new Type[] { Dionysos.Data.LLBLGen.LLBLGenUtil.GetFieldIndexEnumerator(this.EntityName).GetType(), typeof(IExpression), typeof(AggregateFunction), typeof(IPredicate), typeof(IRelationCollection), typeof(IGroupByCollection) }, new object[] { 0, null, AggregateFunction.CountDistinct, this.DataSourceFilter, this.DataSourceRelations, null });
				}
				else
				{
					// Nothing fetched,count is automaticly 0
					this.PagingItemCount = 0;
				}
			}
			else
			{
				// Do retrieval of all entities, no LLBLGen paging
				this.entityCollection.GetMulti(this.DataSourceFilter, -1, sort, this.DataSourceRelations);
				this.PagingItemCount = this.entityCollection.Count;
			}

			if (this.DataSortField.Length > 0 && !isSorted)
			{
				this.entityCollection.Sort(this.DataSortField, this.DataSortDirection, null);
			}

			return !Instance.Empty(this.entityCollection);
		}

		void InitializePredicateExpression()
		{
			// The query string should be formed like: Field;filterValue
			string filter;
			if (!QueryStringHelper.TryGetValue("Filter", out filter))
			{
				filter = String.Empty;
			}

			if (filter.Length > 0 && filter.Contains(";"))
			{
				int semiColonLocation = filter.IndexOf(";");
				string fieldName = filter.Substring(0, semiColonLocation);
				string filterValue = filter.Substring(semiColonLocation + 1);
				string wildcardedFilterValue = string.Format("%{0}%", filterValue);

				if (fieldName.Length > 0 && filterValue.Length > 0)
				{
					// Get entity instance
					SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;

					if (!fieldName.Contains("."))
					{
						// "Simple" filter, not on a related field
						if (entity.Fields[fieldName] == null)
						{
							throw new FunctionalException(string.Format("Filtering mislukt: Het veld {0} (op {1}) bestaat niet op de database en kan daarom niet al filter worden gebruikt", fieldName, this.EntityName));
						}
						this.DataSourceFilter.Add(new FieldLikePredicate(entity.Fields[fieldName], wildcardedFilterValue));
					}
					else
					{
						// "Complex" filter, on a related field
						Dionysos.Data.LLBLGen.LLBLGenEntityUtil entityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();
						int dotLocation = fieldName.IndexOf(".");
						string foreignKeyField = fieldName.Substring(0, dotLocation);
						string fieldOnRelatedEntity = fieldName.Substring(dotLocation + 1);

						// Check if the foreignkey field exists
						if (entity.Fields[foreignKeyField] != null)
						{
							// Retrieve relation for the ForeignKeyField
							IEntityRelation relation = entityUtil.GetRelationByField(entity, entity.Fields[foreignKeyField]);

							// If relation not found, throw error
							if (relation == null)
							{
								throw new FunctionalException(string.Format("Filtering mislukt: Er kon geen relatie worden gevonden voor een filter van {0} voor entity {1}", this.DataSortField, this.EntityName));
							}
							else
							{
								// Add the required relation
								this.DataSourceRelations.Add(relation);

								// Create the SortExpression
								IEntity relatedEntity = Dionysos.DataFactory.EntityFactory.GetEntity(relation.MappedFieldName) as IEntity;
								if (relatedEntity == null)
									throw new FunctionalException(string.Format("Filtering mislukt: De gerelateerde entiteit {0} kon niet worden gevonden.", relation.MappedFieldName));
								else if (relatedEntity.Fields[fieldOnRelatedEntity] == null)
									throw new FunctionalException(string.Format("Filtering mislukt: Het veld {0} op de gerelateerde entiteit {1} kon niet worden gevonden.", fieldOnRelatedEntity, relation.MappedFieldName));
								this.DataSourceFilter.Add(new FieldLikePredicate(relatedEntity.Fields[fieldOnRelatedEntity], wildcardedFilterValue));
							}
						}
					}
				}
			}
		}

		void InitializeSortExpression(out SortExpression sort, RelationCollection relations)
		{
			// Get the sort direction LLBLGen style
			sort = new SortExpression();
			SortOperator sortDirection = SortOperator.Ascending;
			if (this.DataSortDirection == System.ComponentModel.ListSortDirection.Ascending)
				sortDirection = SortOperator.Ascending;
			else
				sortDirection = SortOperator.Descending;

			if (this.DataSortField.Length > 0)
			{
				SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
				if (this.DataSortField.Contains("."))
				{
					// Sort on related field

					Dionysos.Data.LLBLGen.LLBLGenEntityUtil entityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();
					int dotLocation = this.DataSortField.IndexOf(".");

					string foreignKeyField = this.DataSortField.Substring(0, dotLocation);
					string fieldOnRelatedEntity = this.DataSortField.Substring(dotLocation + 1);

					// Check if the foreignkey field exists
					if (entity.Fields[foreignKeyField] != null)
					{
						// Retrieve relation for the ForeignKeyField
						IEntityRelation relation = entityUtil.GetRelationByField(entity, entity.Fields[foreignKeyField]);

						// If relation not found, throw error
						if (relation == null)
						{
							throw new FunctionalException(string.Format("Sortering mislukt: Er kon geen relatie worden gevonden voor een sortering van {0} voor entity {1}", this.DataSortField, this.EntityName));
						}
						else
						{
							// Add the required relation
							this.DataSourceRelations.Add(relation);

							// Create the SortExpression
							IEntity relatedEntity = Dionysos.DataFactory.EntityFactory.GetEntity(relation.MappedFieldName) as IEntity;
							if (relatedEntity == null)
								throw new FunctionalException(string.Format("Sortering mislukt: De gerelateerde entiteit {0} kon niet worden gevonden.", relation.MappedFieldName));
							else if (relatedEntity.Fields[fieldOnRelatedEntity] == null)
								throw new FunctionalException(string.Format("Sortering mislukt: Het veld {0} op de gerelateerde entiteit {1} kon niet worden gevonden.", fieldOnRelatedEntity, relation.MappedFieldName));
							sort.Add(new SortClause(relatedEntity.Fields[fieldOnRelatedEntity], sortDirection));
						}
					}

				}
				else if (entity.Fields[this.DataSortField] == null)
				{
					// For backwards compat. below error is not thrown, but in this scenario with paging only the shwon page is sorted, not all entities)
					//throw new Dionysos.FunctionalException(string.Format("Er kan op het veld {0} niet worden gesorteerd omdat het niet beschikbaar is voor entity {1}", this.DataSortField,this.EntityName));
				}
				else
				{
					sort.Add(new SortClause(entity.Fields[this.DataSortField], sortDirection));
				}
			}
		}

		/// <summary>
		/// Initializes the data data bindings of the controls on the page.
		/// </summary>
		/// <returns>True if initialization was succesful, False if not</returns>
		public override bool InitializeDataBindings()
		{
			if (this.ViewMode == CollectionViewMode.List)
			{
				// Since InitializeDataBindings can be ran on postback (Sort/Page) we need a check here
				if (!this.IsPostBack)
				{
					this.MainGridView.InitializeColumns(this.EntityName);
				}

				if (!this.LLBLGenPaging)
					this.MainGridView.PageIndex = this.PagingCurrentPage;
				else
					this.MainGridView.AllowPaging = false;

				this.MainGridView.DataSource = this.entityCollection;
				this.MainGridView.DataBind();
			}
			else if (this.ViewMode == CollectionViewMode.Tiles)
			{
				if (!Instance.Empty(this.MainDataList))
				{
					if (this.MainDataList.AllowPaging && !this.LLBLGenPaging)
					{
						this.MainDataList.PagedDataSource = new PagedDataSource();
						this.MainDataList.PagedDataSource.PageSize = this.MainDataList.PageSize;
						this.MainDataList.PagedDataSource.CurrentPageIndex = this.PagingCurrentPage;
						this.MainDataList.PagedDataSource.AllowPaging = true;
						this.MainDataList.PagedDataSource.DataSource = this.entityCollection;
						this.MainDataList.DataSource = this.MainDataList.PagedDataSource;
						this.MainDataList.DataBind();
					}
					else
					{
						this.MainDataList.AllowPaging = false;
						this.MainDataList.DataSource = this.entityCollection;
						this.MainDataList.DataBind();
					}
				}
			}
			else if (this.ViewMode == CollectionViewMode.Details)
			{
				if (!Instance.Empty(this.MainRepeater))
				{
					if (this.MainRepeater.AllowPaging && !this.LLBLGenPaging)
					{
						this.MainRepeater.PagedDataSource = new PagedDataSource();
						this.MainRepeater.PagedDataSource.PageSize = this.MainRepeater.PageSize;
						this.MainRepeater.PagedDataSource.CurrentPageIndex = this.PagingCurrentPage;
						this.MainRepeater.PagedDataSource.AllowPaging = true;
						this.MainRepeater.PagedDataSource.DataSource = this.entityCollection;
						this.MainRepeater.DataSource = this.MainRepeater.PagedDataSource;
						this.MainRepeater.DataBind();
					}
					else
					{
						this.MainRepeater.AllowPaging = false;
						this.MainRepeater.DataSource = this.entityCollection;
						this.MainRepeater.DataBind();
					}
				}
			}

			return true;
		}

		/// <summary>
		/// Refreshes the data collection of the page
		/// </summary>
		/// <returns></returns>
		public override bool Refresh()
		{
			DataFactory.EntityCollectionUtil.Refresh(ref this.entityCollection);
			return true;
		}

		/// <summary>
		/// Views an data item from the data collection of the page.
		/// </summary>
		/// <returns>True if viewing was succesfull, False if not</returns>
		public override bool View(object id)
		{
			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("id", id);
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "view");

			if (Instance.Empty(this.EntityPageUrl))
			{
				throw new EmptyException("The 'EntityPageUrl' property is not set for the PageEntityCollection instance.");
			}
			else
			{
				Response.Redirect(ResolveUrl(this.EntityPageUrl + queryString.Value));
			}

			return true;
		}

		/// <summary>
		/// Adds an data item to the data collection of the form
		/// </summary>
		/// <returns>True if adding was succesfull, False if not</returns>
		public override bool Add()
		{
			MessageBox.Show("Add");

			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "add");

			if (Instance.Empty(this.EntityPageUrl))
			{
				throw new EmptyException("The 'EntityPageUrl' property is not set for the PageEntityCollection instance.");
			}
			else
			{
				Response.Redirect(ResolveUrl(this.EntityPageUrl + queryString.Value), true);
			}

			return true;
		}

		/// <summary>
		/// Edits the currently selected data item of the data collection of the form
		/// </summary>
		/// <returns>True if editing was succesfull, False if not</returns>
		public override bool Edit(object id)
		{
			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("id", id);
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "edit");

			if (Instance.Empty(this.EntityPageUrl))
			{
				throw new EmptyException("The 'EntityPageUrl' property is not set for the PageEntityCollection instance.");
			}
			else
			{
				Response.Redirect(ResolveUrl(this.EntityPageUrl + queryString.Value), true);
			}

			return true;
		}

		/// <summary>
		/// Deletes the currently selected data item from the data collection of the form
		/// </summary>
		/// <returns>True if deletion was successfull, False if not</returns>
		public override bool Delete(object id)
		{
			bool success = false;
			IEntity entity = Dionysos.DataFactory.EntityFactory.GetEntity(this.EntityName, (int)id) as IEntity;

			if (Instance.Empty(entity))
			{
				throw new EmptyException("Entity of type {0} with {1} could not be loaded.", this.EntityName, id);
			}
			else
			{
				try
				{
					success = entity.Delete();
				}
				catch (EntityDeleteException eDel)
				{
					this.MultiValidatorDefault.AddError(eDel.Message);
					this.Validate();
				}
			}


			// If we have Succces and this is a PostBack (which means the user opened an Entity and wants to delete it)
			// we need to Redirect back like a SaveAndGo via Cancel after the delete
			// Otherwise the user get presented the data of a deleted entity
			if (success)
				WebShortcuts.RedirectUsingRawUrl();

			return success;
			/*
			QueryStringHelper queryString = new QueryStringHelper();
			queryString.AddItem("id", id);
			queryString.AddItem("entity", this.EntityName);
			queryString.AddItem("mode", "delete");

			if (Instance.Empty(this.EntityPageUrl))
			{
				throw new EmptyException("The 'EntityPageUrl' property is not set for the PageEntityCollection instance.");
			}
			else
			{
				Response.Redirect(ResolveUrl(this.EntityPageUrl + queryString.Value),true);
			}

			return true;
			*/
		}

		/// <summary>
		/// Closes the page and returns to the referring page
		/// </summary>
		public override void Close()
		{
		}


		public override void InvokeCommand(string commandName, int index)
		{
			object id = null;

			// Update DataSoruce (GK Make method)
			this.InitializeEntityCollection();
			this.InitializeDataBindings();

			// Check whether the datasource of this gridview instance is empty
			if (Instance.Empty(this.MainGridView))
			{
				throw new EmptyException("Variable 'mainGridView' is empty.");
			}
			else if (Instance.Empty(this.MainGridView.DataSource))
			{
				throw new EmptyException("Variable 'mainGridView.DataSource' is empty.");
			}
			else
			{
				SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection = this.MainGridView.DataSource as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;

				if (entityCollection.Count > index)
				{
					SD.LLBLGen.Pro.ORMSupportClasses.IEntity entity = entityCollection[index];
					if (Instance.Empty(entity))
					{
						throw new EmptyException("Variable 'entity' is empty.");
					}
					else
					{
						id = (int)Data.LLBLGen.LLBLGenEntityUtil.GetPkFieldInfo(entity).CurrentValue;
					}
				}
			}

			// Check which command was invoked and which
			// method whould be invoked on the page
			switch (commandName)
			{
				case "VIEW":
					this.View(id);
					break;

				case "EDIT":
					this.Edit(id);
					break;

				case "DELETE":
					this.Delete(id);
					break;
			}
		}

		/// <summary>
		/// Set the Paging properties of the page to each Pager control on the page.
		/// </summary>
		public override void SetPagers()
		{
			foreach (Dionysos.Web.UI.WebControls.Pager pgr in this.Pagers)
			{
				if (this.LLBLGenPaging)
				{
					pgr.CurrentPageIndex = this.PagingCurrentPage;
					pgr.PageSize = this.PagingPageSize;
					if (this.PagingItemCount.HasValue)
						pgr.ItemCount = this.PagingItemCount.Value;
				}
				else
				{
					pgr.Visible = false;
				}
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Custom override for LLBLGen, removes the plural "s" from the filename and returns it if base.EntityPageUrl is empty
		/// </summary>
		public override string EntityPageUrl
		{
			get
			{
				if (Instance.Empty(base.EntityPageUrl))
				{
					return this.Request.Path.Replace("s.aspx", ".aspx");
				}
				else
				{
					return base.EntityPageUrl;
				}
			}
			set
			{
				base.EntityPageUrl = value;
			}
		}


		/// <summary>
		/// Gets or sets the data collection of the form
		/// </summary>
		public new SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection DataSource
		{
			get
			{
				return this.entityCollection;
			}
			set
			{
				this.entityCollection = value;
			}
		}

		/// <summary>
		/// Gets or sets the Filter for the current EntityCollectionPage
		/// </summary>
		public new SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression DataSourceFilter
		{
			get
			{
				if (Instance.Empty(base.DataSourceFilter))
					base.DataSourceFilter = new PredicateExpression();
				else if (!(base.DataSourceFilter is PredicateExpression))
					base.DataSourceFilter = new PredicateExpression();
				return base.DataSourceFilter as SD.LLBLGen.Pro.ORMSupportClasses.PredicateExpression;
			}
			set
			{
				base.DataSourceFilter = value;
			}
		}

		/// <summary>
		/// Gets or sets the Relations for the datasource (LLBLGen RelationCollection) for filtering/sorting on related entities
		/// </summary>
		public RelationCollection DataSourceRelations
		{
			get
			{
				if (Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSourceRelations"))
				{
					return Dionysos.Web.ViewState.GetParameter(this.ViewState, "DataSourceRelations") as RelationCollection;
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (!Dionysos.Web.ViewState.HasParameter(this.ViewState, "DataSourceRelations"))
				{
					this.ViewState.Add("DataSourceRelations", null);
				}
				this.ViewState["DataSourceRelations"] = value;
			}
		}

		/// <summary>
		/// Let LLBLGen logic to the paging on the database, this means only the required records are fetched.
		/// Watch out: LLBGenPaging uses a 1-based index (Page 1 = 1), while .NET uses Page 1 = 0 as pageIndex.
		/// In the FetchData logic the this.CurrentPage is automaticly increased with 1 to make it work with standard .NET behaviour
		/// </summary>
		public bool LLBLGenPaging
		{
			get
			{
				if (!this.llblGenPaging.HasValue)
					this.llblGenPaging = Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.UseLlblGenPaging);

				return this.llblGenPaging.Value;
			}
			set
			{
				this.llblGenPaging = value;
			}
		}

		#endregion
	}
}
