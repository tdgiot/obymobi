using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using Dionysos.Web;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Gui class which represents a webform which displays exceptions
    /// </summary>
    public abstract class PageException : PageDefault
    {
        #region Fields

        private Exception exception = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Web.UI.PageException class
        /// </summary>
        public PageException()
        {
            // Hookup the events
            this.HookupEvents();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Hookup the events to the corresponding event handlers
        /// </summary>
        private void HookupEvents()
        {
            this.Load += new EventHandler(PageException_Load);
        }

        /// <summary>
        /// Gets the last exception from the session state
        /// </summary>
        private void GetException()
        {
            SessionHelper.TryGetValue("Exception", out this.exception);
        }

        /// <summary>
        /// Displays the exception information. This is an abstract method and must be overridden in subclasses.
        /// </summary>
        public abstract void ShowException();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the exception
        /// </summary>
        [Browsable(false)]
        public Exception Exception
        {
            get
            {
                return this.exception.GetBaseException();
            }
            set
            {
                this.exception = value;
            }
        }

        #endregion

        #region Event handlers

        private void PageException_Load(object sender, EventArgs e)
        {
            this.GetException();
            this.ShowException();
        }

        #endregion
    }
}
