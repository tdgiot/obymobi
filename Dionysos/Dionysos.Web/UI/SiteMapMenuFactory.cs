﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.UI
{
    /// <summary>
    /// Factory class which is being used to retrieve menu structures from a database
    /// </summary>
    public class SiteMapMenuFactory : IMenuFactory
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Web.UI.LLBLGenMenuFactory class
        /// </summary>
        public SiteMapMenuFactory()
        {
            
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the menu and return an object containing the menu
        /// </summary>
        /// <returns>An object containing the menu</returns>
        public object LoadMenu(string typeName)
        {
            // Create and initialize a MenuItemCollection instance
            MenuItemCollection menuItemCollection = new MenuItemCollection(null);

            if (Instance.ArgumentIsEmpty(typeName, "typeName"))
            {
                // Parameter 'typeName' is empty                
            }
            else
            {
                // Get the UIElement entity based on the typename
                SiteMapNode uiElementNode = this.GetUIElement(typeName);

                // Get the modules
                SiteMapNodeCollection moduleNodeCollection = this.GetModules();

                for (int i = 0; i < moduleNodeCollection.Count; i++)
                {
                    // Create (parent) item from module 
                    SiteMapNode module = moduleNodeCollection[i];

                    MenuItem mainNavigationMenuItem = new MenuItem();

                    mainNavigationMenuItem.Enabled = true;
                    mainNavigationMenuItem.ImageUrl = module["iconpath"];
                    
                    // Url should be taken from first childeNode
                    mainNavigationMenuItem.NavigateUrl = module.ChildNodes[0].Url;
                    mainNavigationMenuItem.Text = module["nameshort"];
                    mainNavigationMenuItem.ToolTip = module["namefull"];
                    mainNavigationMenuItem.Value = module["namesystem"];

                    menuItemCollection.Add(mainNavigationMenuItem);

                    // If this module is parent of uielement make it selected
                    if (uiElementNode == null)
                    { 
                        throw new Dionysos.FunctionalException(string.Format("There's no SiteMap node in the Web.sitemap file for the type/classname (\"{0}\") of the requested page",typeName));
                    }
                    else if (uiElementNode.ParentNode == module)
                    {
                        mainNavigationMenuItem.Selected = true;
                    }

                    // We load the childeren if the item is selected, because then they'll be
                    // used as submenu items, or when the function parameter forces it to do so.
                    if (mainNavigationMenuItem.Selected) //|| loadAllChilderen)
                    {
                        for (int j = 0; j < module.ChildNodes.Count; j++)
                        {
                            SiteMapNode subItem = module.ChildNodes[j];

                            MenuItem subNavigationMenuItem = new MenuItem();

                            subNavigationMenuItem.Enabled = true;
                            subNavigationMenuItem.ImageUrl = subItem["iconpath"];
                            subNavigationMenuItem.NavigateUrl = subItem.Url;
                            subNavigationMenuItem.Text = subItem["nameshort"];
                            subNavigationMenuItem.ToolTip = subItem["namefull"];
                            subNavigationMenuItem.Value = subItem["typenamefull"];

                            if (subItem["hide"] == null)
                            {
                                mainNavigationMenuItem.ChildItems.Add(subNavigationMenuItem);

                                // If this is the selected item mark the item and it's parent as selected
                                if (subItem["typenamefull"].ToString() == typeName)
                                {
                                    subNavigationMenuItem.Selected = true;
                                    subNavigationMenuItem.Parent.Selected = true;
                                }
                            }
                        }
                    }
                }
            }

            return menuItemCollection;
        }

        /// <summary>
        /// Get the SiteMapNode belonging to the page of the supplied typeName
        /// UiElements are items at level 2 in the site map (So: Root > Module > UiElement)
        /// </summary>
        /// <param name="typeName">typeName is the full NameSpace + Class name of the Page</param>
        /// <returns>SiteMapNode or null if not found</returns>
        private SiteMapNode GetUIElement(string typeName)
        {
            SiteMapNode returnNode = null;


            for (int i = 0; i < SiteMap.RootNode.ChildNodes.Count; i++)
            { 
                SiteMapNode moduleNode = SiteMap.RootNode.ChildNodes[i];
                for (int j = 0; j < moduleNode.ChildNodes.Count; j++)
                {
                    if (moduleNode.ChildNodes[j]["typenamefull"] != null &&
                        moduleNode.ChildNodes[j]["typenamefull"] == typeName)
                    {
                        returnNode = moduleNode.ChildNodes[j];
                        break;
                    }
                }
                // If found we don't need to search in other modules
                if (returnNode != null)
                {
                    break;
                }
            }

            return returnNode;
        }


        private SiteMapNodeCollection GetModules()
        {
            return SiteMap.RootNode.ChildNodes;
        }

        #endregion
    }
}
