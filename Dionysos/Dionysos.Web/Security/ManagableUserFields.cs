﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.Security
{
	/// <summary>
	/// Class containing all necessary ManagableUserFields on a ManagableUserEntity
	/// </summary>
	public class ManagableUserFields
	{
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the Username
		/// </summary>
		public string Username = "Username";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the Firstname
		/// </summary>
		public string Firstname = "Firstname";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the Lastname
		/// </summary>
		public string Lastname = "Lastname";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the MiddleName
		/// </summary>
		public string MiddleName = "MiddleName";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the Email
		/// </summary>
		public string Email = "Email";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the Password
		/// </summary>
		public string Password = "Password";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the IsApproved
		/// </summary>
		public string IsApproved = "IsApproved";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the LastLoginDate
		/// </summary>
		public string LastLoginDate = "LastLoginDate";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the LastPasswordChangedDate
		/// </summary>
		public string LastPasswordChangedDate = "LastPasswordChangedDate";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the IsOnline
		/// </summary>
		public string IsOnline = "IsOnline";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the IsLockedOut
		/// </summary>
		public string IsLockedOut = "IsLockedOut";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the LastLockedOutDate
		/// </summary>
		public string LastLockedOutDate = "LastLockedOutDate";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the FailedPasswordAttemptCount
		/// </summary>
		public string FailedPasswordAttemptCount = "FailedPasswordAttemptCount";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the FailedPasswordAttemptWindowStart
		/// </summary>
		public string FailedPasswordAttemptWindowStart = "FailedPasswordAttemptWindowStart";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the FailedPasswordAttemptLastDate
		/// </summary>
		public string FailedPasswordAttemptLastDate = "FailedPasswordAttemptLastDate";
		/// <summary>
		/// Field mapping to the databasefield on the entity containing the RandomValue
		/// </summary>
		public string RandomValue = "RandomValue";
	}
}
