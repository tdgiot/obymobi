﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.Security
{
	/// <summary>
	/// Result of a SignIn attempt
	/// </summary>
	public enum AuthenticateResult : int
	{
		/// <summary>
		/// Login was succesfull
		/// </summary>
		Unknown = 0,
		/// <summary>
		/// Login was succesfull
		/// </summary>
		Succes = 1,
		/// <summary>
		/// Username is unknown
		/// </summary>
		LoginUnkown = 2,
		/// <summary>
		/// Password is incorrect
		/// </summary>
		PasswordIncorrect = 3,
		/// <summary>
		/// User is locked out
		/// </summary>
		UserIsLockedOut = 4,
		/// <summary>
		/// User is not activated
		/// </summary>
		UserIsNotActivated = 5,
		/// <summary>
		/// Get returned if a techinical reason made the signin fail (db unavailable) 
		/// </summary>
		TechnicalFailure = 6,
		/// <summary>
		/// The username was not supplied
		/// </summary>
		UsernameEmpty = 7,
		/// <summary>
		/// The password was not supplied
		/// </summary>
		PasswordEmpty = 8,
		/// <summary>
		/// The emailaddress was invalid
		/// </summary>
		InvalidEmailAddress = 9,
		/// <summary>
		/// Waiting for next 3 signin attempts
		/// </summary>
		AwaitingAttempts = 10,
        
        /// <summary>
        /// No role assigned to user
        /// </summary>
        NoRoleAssignedToUser = 11,
        /// <summary>
        /// No company assigned to user
        /// </summary>
        NoCompanyAssignedToUser = 12,
        /// <summary>
        /// No account or user related to company
        /// </summary>
        NoCompanyAssignedToRelatedAccount = 13,
	}
}
