﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.Security
{
    /// <summary>
    /// Result of a user creation attempt
    /// </summary>
    public enum UserCreationResult
    {
        DuplicateEmail,
        DuplicateProviderUserKey,
        DuplicateUserName,
        InvalidAnswer,
        InvalidEmail,
        InvalidPassword,
        InvalidProviderUserKey,
        InvalidQuestion,
        InvalidUserName,
        ProviderError,
        UserRejected,
        Success
    }
}
