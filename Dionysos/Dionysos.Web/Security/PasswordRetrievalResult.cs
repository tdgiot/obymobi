﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.Security
{
    /// <summary>
    /// Result of a password retrieval attempt
    /// </summary>
    public enum PasswordRetrievalResult
    {
        /// <summary>
        /// Password retrieval was succesfull
        /// </summary>
        Succes,
        /// <summary>
        /// Username is unknown
        /// </summary>
        LoginUnkown,
        /// <summary>
        /// User is locked out
        /// </summary>
        UserIsLockedOut,
        /// <summary>
        /// User is not activated
        /// </summary>
        UserIsNotActivated,
        /// <summary>
        /// Get returned if a technical reason made the passwor retrieval fail (db unavailable) 
        /// </summary>
        TechnicalFailure,
        /// <summary>
        /// The username was not supplied
        /// </summary>
        UsernameEmpty
    }
}
