﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.Security
{
    /// <summary>
    /// Managable User for UserManager
    /// </summary>
    public interface IManagableUser : IEntity
    {
        /// <summary>
        /// Short displayname used for logged-on status indicators
        /// </summary>
        string ShortDisplayName
        {
            get;
        }

        /// <summary>
        /// Username
        /// </summary>
        string Username
        {
            get;
        }

        /// <summary>
        /// UserId
        /// </summary>
        int UserId
        {
            get;
        }

        string Firstname
        {
            get;
        }

        string Lastname
        {
            get;
        }

        string MiddleName
        {
            get;
        }

        string Email
        {
            get;
        }

        string Password
        {
            get;
            set;
        }

        bool IsApproved
        {
            get;
            set;
        }

        DateTime? LastLoginDateUTC
        {
            get;
            set;
        }

        DateTime? LastPasswordChangedDateUTC
        {
            get;
            set;
        }

        bool IsLockedOut
        {
            get;
            set;
        }

        DateTime? LastLockedOutDateUTC
        {
            get;
            set;
        }

        int? FailedPasswordAttemptCount
        {
            get;
            set;
        }

        DateTime? FailedPasswordAttemptWindowStartUTC
        {
            get;
            set;
        }

        DateTime? FailedPasswordAttemptLastDateUTC
        {
            get;
            set;
        }

        string RandomValue
        {
            get;
            set;
        }

        string[] Roles
        {
            get;
        }

        int PageSize
        {
            get;
        }
    }
}
