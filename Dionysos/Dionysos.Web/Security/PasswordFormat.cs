﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web.Security
{
    /// <summary>
    /// Enumerator class which describes the encryption format for storing passwords for users
    /// </summary>
    public enum PasswordFormat
    {
        /// <summary>
        /// Passwords are not encrypted
        /// </summary>
        Clear = 0,
        /// <summary>
        /// Passwords are encrypted one-way using the SHA1 hashing algorithm.
        /// </summary>
        Hashed = 1,
        /// <summary>
        /// Passwords are encrypted
        /// </summary>
        Encrypted = 2,
    }
}
