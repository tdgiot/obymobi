﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using Dionysos.Security.Cryptography;
using Dionysos.Net.Mail;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Web.Security
{
    /// <summary>
    /// Class to manage everything for user authentication, creation, etc.
    /// </summary>
    public class UserManager
    {
        #region Fields

        private const string CurrentUserCacheKey = "UserManager.CurrentUser";

        /// <summary>
        /// The instance.
        /// </summary>
        private static readonly UserManager instance = new UserManager();

        /// <summary>
        /// The user entity name.
        /// </summary>
        private string userEntityName = String.Empty;

        /// <summary>
        /// Indicates whether passwords are case sensitive.
        /// </summary>
        private bool caseSensitivePasswords = true;

        /// <summary>
        /// The password format.
        /// </summary>
        private PasswordFormat passwordFormat = PasswordFormat.Hashed;

        /// <summary>
        /// The field mappings.
        /// </summary>
        private ManagableUserFields fieldMappings = new ManagableUserFields();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public static UserManager Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Get the current user from the session, if not available via Identity.
        /// </summary>
        /// <value>
        /// The current user if available; otherwise, <c>null</c>.
        /// </value>
        public static IManagableUser CurrentUser
        {
            get
            {
                IManagableUser toReturn = null;

                if (HttpContext.Current == null)
                {
                    // Current is empty
                }
                else if (HttpContext.Current.User == null)
                {
                    // User is empty
                }
                else if (HttpContext.Current.User.Identity == null)
                {
                    // Identify is empty
                }
                else if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    // User is not authenticated
                }
                else
                {
                    int userId = int.Parse(HttpContext.Current.User.Identity.Name);
                    if (SessionHelper.TryGetValue(CurrentUserCacheKey, out toReturn))
                    {
                        // If a user signs out it's possible the user is still in the Session, therefore check if this is the correct user
                        if (toReturn.UserId != userId)
                        {
                            toReturn = null;
                        }
                    }

                    if (toReturn == null)
                    {
                        toReturn = DataFactory.EntityFactory.GetEntity(Instance.UserEntityName, userId) as IManagableUser;

                        // Set in session if available
                        SessionHelper.SetValue(CurrentUserCacheKey, toReturn);
                    }
                }

                return toReturn;
            }
        }

        /// <summary>
        /// Gets or sets the name of the UserEntity.
        /// </summary>
        /// <value>
        /// The name of the UserEntity.
        /// </value>
        public string UserEntityName
        {
            get
            {
                return instance.userEntityName;
            }
            set
            {
                // First validate for required fields, then set.
                this.ValidateUserEntityObject(value);

                instance.userEntityName = value;
            }
        }

        /// <summary>
        /// Name of the User Entity in the LLBLGen project
        /// </summary>
        public bool UsePasswordSalting
        {
            get
            {
                return Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.PasswordSalting, true);
            }
            set
            {
                // First validate for required fields, then set.
                Dionysos.ConfigurationManager.SetValue(DionysosWebConfigurationConstants.PasswordSalting, value, true);
            }
        }

        /// <summary>
        /// Enable / disable CaseSensitive passwords
        /// </summary>
        public bool CaseSensitivePasswords
        {
            get
            {
                return instance.caseSensitivePasswords;
            }
            set
            {
                // First validate for required fields, then set.
                instance.caseSensitivePasswords = value;
            }
        }

        /// <summary>
        /// Gets or sets the password format
        /// </summary>
        public PasswordFormat PasswordFormat
        {
            get
            {
                return instance.passwordFormat;
            }
            set
            {
                instance.passwordFormat = value;
            }
        }

        /// <summary>
        /// If enabled the check for IsApproved is skipped
        /// </summary>
        public bool OverrideIsApproved
        {
            get
            {
                return Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.IsApprovedOverride, true);
            }
            set
            {
                // First validate for required fields, then set.
                Dionysos.ConfigurationManager.SetValue(DionysosWebConfigurationConstants.IsApprovedOverride, value, true);
            }
        }

        /// <summary>
        /// For some fields DIRECT mapping on the Field[] propery entity is required, this mappings are set in this class
        /// </summary>
        /// <returns></returns>
        public ManagableUserFields FieldMappings
        {
            get
            {
                return instance.fieldMappings;
            }
        }

        /// <summary>
        /// Retrieve from the ConfigurationProvider if AutoLogon is enabled
        /// </summary>
        public bool AutoLogon
        {
            get
            {
                return Dionysos.Global.ConfigurationProvider.GetBool(DionysosWebConfigurationConstants.AutoLogon);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes the <see cref="UserManager" /> class.
        /// </summary>
        /// <remarks>
        /// Explicit static constructor to tell C# compiler not to mark type as beforefieldinit
        /// </remarks>
        static UserManager()
        { }

        /// <summary>
        /// Prevents a default instance of the <see cref="UserManager" /> class from being created.
        /// </summary>
        private UserManager()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves the password from the IManagableUser instance with the specified user name
        /// </summary>
        /// <param name="userName">The username of the IManagableUser instance to get the password for</param>
        /// <param name="password">The password.</param>
        /// <returns>
        /// A System.String instance containing the password
        /// </returns>
        public PasswordRetrievalResult GetPassword(string userName, out string password)
        {
            PasswordRetrievalResult result = PasswordRetrievalResult.TechnicalFailure;
            password = string.Empty;

            if (instance.passwordFormat == PasswordFormat.Hashed)
            {
                throw new TechnicalException("Cannot retrieve Hashed passwords.");
            }
            else
            {
                if (System.Instance.Empty(userName))
                {
                    // Supplied user name is empty
                    result = PasswordRetrievalResult.UsernameEmpty;
                }
                else
                {
                    // Supplied user name is not empty, get the
                    // IManagableUser instance for the supplied user name
                    IManagableUser user = GetUserByUsername(userName);
                    if (System.Instance.Empty(user))
                    {
                        // User could not be found
                        result = PasswordRetrievalResult.LoginUnkown;
                    }
                    else
                    {
                        if (user.IsLockedOut)
                        {
                            result = PasswordRetrievalResult.UserIsLockedOut;
                        }
                        else if (!user.IsApproved && !this.OverrideIsApproved)
                        {
                            result = PasswordRetrievalResult.UserIsNotActivated;
                        }
                        else
                        {
                            password = user.Password;

                            if (instance.passwordFormat == PasswordFormat.Encrypted)
                                password = DecodePassword(user.Password);

                            result = PasswordRetrievalResult.Succes;
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Sends the password of the user for the specified username
        /// </summary>
        /// <param name="userName">The username of the user to send the password to</param>
        /// <returns>
        /// True if sending the password was successful, False if not
        /// </returns>
        /// <exception cref="EmptyException">Variable 'user' is empty.</exception>
        public bool SendPassword(string userName)
        {
            bool success = false;

            string password = string.Empty;
            PasswordRetrievalResult result = UserManager.Instance.GetPassword(userName, out password);
            if (result == PasswordRetrievalResult.Succes)
            {
                IManagableUser user = GetUserByUsername(userName);
                if (System.Instance.Empty(user))
                {
                    throw new EmptyException("Variable 'user' is empty.");
                }
                else if (System.Instance.Empty(user.Email))
                {
                    throw new EmptyException(string.Format("User '{0}' has no e-mailadress specified", userName));
                }
                else
                {
                    string subject = Dionysos.ConfigurationManager.GetString(Dionysos.DionysosConfigurationConstants.PasswordRetrievalEmailSubject);
                    string body = Dionysos.ConfigurationManager.GetString(Dionysos.DionysosConfigurationConstants.PasswordRetrievalEmailBody);
                    body = body.Replace("<% UserName %>", userName);
                    body = body.Replace("<% Password %>", password);

                    success = Dionysos.Net.Mail.MailUtil.SendMail(subject, body, user.Email, string.Empty, true);
                }
            }

            return success;
        }

        /// <summary>
        /// Checks whether the specified passwords match according to the password format
        /// </summary>
        /// <param name="password">The password specified by the user</param>
        /// <param name="dbpassword">The password stored in the database</param>
        /// <param name="user">The user.</param>
        /// <returns>
        /// True if the password match, False if not
        /// </returns>
        private bool CheckPassword(string password, string dbpassword, IManagableUser user)
        {
            string pass1 = password;
            string pass2 = dbpassword;

            if (System.Instance.Empty(user))
            {
                throw new EmptyException("Variable 'user' is empty.");
            }
            else
            {
                switch (instance.passwordFormat)
                {
                    case PasswordFormat.Encrypted:
                        // Passwords are stored encrypted,
                        // decrypt database password in order to compare
                        pass2 = DecodePassword(dbpassword);
                        break;

                    case PasswordFormat.Hashed:
                        // Passwords are stored hashed,
                        // encrypt specified password in order to compare
                        if (instance.UsePasswordSalting)
                        {
                            pass1 = EncodePassword(password, user);
                        }
                        else
                        {
                            // Passwords are stored clear,
                            // check whether they are stored case sensitive
                            if (!caseSensitivePasswords)
                            {
                                pass1 = pass1.ToUpper();
                                pass2 = pass2.ToUpper();
                            }
                        }
                        break;
                }

                if (pass1 == pass2)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Encodes a password according to the password format
        /// </summary>
        /// <param name="password">The non-encoded password to encode</param>
        /// <param name="user">The user to encode the password for</param>
        /// <returns>A System.String instance containing the encoded password</returns>
        public string EncodePassword(string password, IManagableUser user)
        {
            string encoded = string.Empty;
            if (instance.UsePasswordSalting)
            {
                // Apply salting
                if (user.RandomValue == string.Empty || user.RandomValue.Length < 10)
                    user.RandomValue = Dionysos.Web.PasswordHashUtil.GetSalt(128);

                string salt = user.RandomValue;

                encoded = Dionysos.Web.PasswordHashUtil.GetPasswordHash(password, ref salt);
            }
            else if (instance.PasswordFormat == PasswordFormat.Encrypted)
            {
                encoded = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(password);
            }
            else
            {
                // No salting
                encoded = password;
            }

            return encoded;
        }

        /// <summary>
        /// Decrypts a encoded password
        /// </summary>
        /// <param name="encodedPassword">The encoded password to decrypt</param>
        /// <returns>An unencoded password</returns>
        public string DecodePassword(string encodedPassword)
        {
            string password = string.Empty;

            if (System.Instance.Empty(encodedPassword))
            {
                // No password specified
            }
            else
            {
                password = encodedPassword;

                switch (instance.passwordFormat)
                {
                    case PasswordFormat.Clear:
                        break;
                    case PasswordFormat.Encrypted:
                        password = Cryptographer.DecryptStringUsingRijndael(password);
                        break;
                    case PasswordFormat.Hashed:
                        throw new TechnicalException("Cannot un-encode a hashed password.");
                }
            }

            return password;
        }

        /// <summary>
        /// Activates a user account by setting the isApproved flag to True
        /// </summary>
        /// <param name="userName">The username of the account to activate</param>
        /// <param name="approvalGuid">The approval guid which has been sent via e-mail</param>
        /// <returns>
        /// True if activating the user account was succesful, False if not
        /// </returns>
        public bool ApproveUser(string userName, string approvalGuid)
        {
            return ApproveUser(userName, approvalGuid, false);
        }

        /// <summary>
        /// Activates a user account by setting the isApproved flag to True
        /// </summary>
        /// <param name="userName">The username of the account to activate</param>
        /// <param name="approvalGuid">The approval guid which has been sent via e-mail</param>
        /// <param name="signOnAfterApproval">Flag which indicates if the user is automatically signed on after approval</param>
        /// <returns>True if activating the user account was succesful, False if not</returns>
        public bool ApproveUser(string userName, string approvalGuid, bool signOnAfterApproval)
        {
            bool success = false;

            IManagableUser user = GetUserByUsername(userName);
            if (System.Instance.Empty(user))
            {
                throw new EmptyException("Variable 'user' is empty.");
            }
            else
            {
                user.IsApproved = true;
                if (user.Save())
                {
                    success = true;

                    if (signOnAfterApproval)
                        AuthenticateUserAndSignOn(userName, DecodePassword(user.Password));
                }
            }

            return success;
        }

        /// <summary>
        /// Retrieve a user Entity by it's username
        /// </summary>
        /// <param name="username">Username to find user for</param>
        /// <returns>Requested UserEntity or null</returns>
        public IManagableUser GetUserByUsername(string username)
        {
            // Build predicate & retrieve users for username
            if (instance.UserEntityName.IsNullOrWhiteSpace())
                throw new Dionysos.FunctionalException("Usage of the UserManager requires a UserEntityName to be set");

            IEntity userEntityForFieldCreation = Dionysos.DataFactory.EntityFactory.GetEntity(this.UserEntityName) as IEntity;
            EntityField fieldUsername = userEntityForFieldCreation.Fields[fieldMappings.Username] as EntityField;

            PredicateExpression filterUser = new PredicateExpression();
            filterUser.Add(fieldUsername == username);

            IEntityCollection users = Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(this.userEntityName) as IEntityCollection;
            users.GetMulti(filterUser);

            if (users.Count > 1)
            {
                throw new Dionysos.TechnicalException(string.Format("Multiple Users found with same for value '{0}' of field '{1}'", username, fieldMappings.Username));
            }
            else if (users.Count == 1)
            {
                return users[0] as IManagableUser;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Authenticate a User (THIS DOESN'T SIGN THE USER ON!!)
        /// You still need to set the Auth. Cookie
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="nonHashedPassword">password</param>
        /// <param name="user">contains the authenticated user if succefull</param>
        /// <returns>Result of authentication</returns>
        public AuthenticateResult AuthenticateUserAndRetrieve(string username, string nonHashedPassword, out IManagableUser user)
        {
            AuthenticateResult toReturn = AuthenticateResult.TechnicalFailure;

            user = null;
            if (!username.IsNullOrWhiteSpace())
            {
                try
                {
                    user = this.GetUserByUsername(username);
                }
                catch (Exception ex)
                {
                    user = null;
                    ErrorLoggerWeb.LogError(ex, "Problem while retrieving user {0}", username);
                    toReturn = AuthenticateResult.TechnicalFailure;
                }
            }

            EmailAddress email = null;

            if (username.Length <= 0)
            {
                toReturn = AuthenticateResult.UsernameEmpty;
                user = null;
            }
            else if (fieldMappings.Username.Equals("Email", StringComparison.OrdinalIgnoreCase) && !EmailAddress.TryParse(username, out email))
            {
                toReturn = AuthenticateResult.InvalidEmailAddress;
                user = null;
            }
            else if (nonHashedPassword.Length <= 0)
            {
                toReturn = AuthenticateResult.PasswordEmpty;
                user = null;
            }
            else if (user == null)
            {
                toReturn = AuthenticateResult.LoginUnkown;
                user = null;
            }
            else if (!this.AuthenticateUser(user, nonHashedPassword))
            {
                toReturn = AuthenticateResult.PasswordIncorrect;

                if (user.FailedPasswordAttemptCount == 0)
                {
                    user.FailedPasswordAttemptWindowStartUTC = DateTime.UtcNow;
                }

                if (user.FailedPasswordAttemptCount >= 4 && !Dionysos.ConfigurationManager.GetBool(DionysosWebConfigurationConstants.AllowUnlimitedSignOnAttempts))
                {
                    toReturn = AuthenticateResult.UserIsLockedOut;
                    user.IsLockedOut = true;
                    user.FailedPasswordAttemptCount++;
                }
                else
                {
                    if (!user.FailedPasswordAttemptCount.HasValue)
                        user.FailedPasswordAttemptCount = 0;
                    user.FailedPasswordAttemptCount++;
                }

                user.FailedPasswordAttemptLastDateUTC = DateTime.UtcNow;
                user.Save();
                user = null;
            }
            else if (user.IsLockedOut)
            {
                toReturn = AuthenticateResult.UserIsLockedOut;
                user = null;
            }
            else if (!user.IsApproved && !this.OverrideIsApproved)
            {
                toReturn = AuthenticateResult.UserIsNotActivated;
                user = null;
            }
            else
            {
                if (user.FailedPasswordAttemptCount > 0)
                {
                    user.FailedPasswordAttemptCount = 0;
                    user.SetNewFieldValue(fieldMappings.FailedPasswordAttemptLastDate, null);
                    user.SetNewFieldValue(fieldMappings.FailedPasswordAttemptWindowStart, null);
                    user.Save();
                }
                toReturn = AuthenticateResult.Succes;
            }

            return toReturn;
        }

        /// <summary>
        /// Authenticate a User (THIS DOESN'T SIGN THE USER ON!!)
        /// You still need to set the Auth. Cookie
        /// User won't get locked out after failed password attempts, but time to wait between 3 attempts increments
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="nonHashedPassword">password</param>
        /// <param name="user">contains the authenticated user if succefull</param>
        /// <returns>Result of authentication</returns>
        public AuthenticateResult AuthenticateUserLimitedAndRetrieve(string username, string nonHashedPassword, out IManagableUser user)
        {
            AuthenticateResult toReturn = AuthenticateResult.TechnicalFailure;

            user = null;
            if (!username.IsNullOrWhiteSpace())
            {
                try
                {
                    user = this.GetUserByUsername(username);
                }
                catch (Exception ex)
                {
                    user = null;
                    ErrorLoggerWeb.LogError(ex, "Problem while retrieving user {0}", username);
                    toReturn = AuthenticateResult.TechnicalFailure;
                }
            }

            if (user == null)
            {
                toReturn = AuthenticateResult.LoginUnkown;
            }
            else
            {
                EmailAddress email = null;
                bool awaitingAttempts = false;
                if (user.FailedPasswordAttemptCount.HasValue && user.FailedPasswordAttemptCount.Value >= 3 && user.FailedPasswordAttemptCount % 3 == 0)
                {
                    int waitMinutes = (int)System.Math.Pow(user.FailedPasswordAttemptCount.Value / 3, 2);

                    DateTime waitCompleted = user.FailedPasswordAttemptLastDateUTC.Value;
                    waitCompleted = waitCompleted.AddMinutes(waitMinutes);

                    if (waitCompleted > DateTime.UtcNow)
                    {
                        awaitingAttempts = true;
                    }
                }

                if (username.Length <= 0)
                {
                    toReturn = AuthenticateResult.UsernameEmpty;
                    user = null;
                }
                else if (fieldMappings.Username.Equals("Email", StringComparison.OrdinalIgnoreCase) && !EmailAddress.TryParse(username, out email))
                {
                    toReturn = AuthenticateResult.InvalidEmailAddress;
                    user = null;
                }
                else if (nonHashedPassword.Length <= 0)
                {
                    toReturn = AuthenticateResult.PasswordEmpty;
                    user = null;
                }
                else if (awaitingAttempts)
                {
                    toReturn = AuthenticateResult.AwaitingAttempts;
                }
                else if (!this.AuthenticateUser(user, nonHashedPassword))
                {
                    toReturn = AuthenticateResult.PasswordIncorrect;

                    if (user.FailedPasswordAttemptCount == 0)
                    {
                        user.FailedPasswordAttemptWindowStartUTC = DateTime.UtcNow;
                    }

                    if (!user.FailedPasswordAttemptCount.HasValue)
                        user.FailedPasswordAttemptCount = 0;

                    user.FailedPasswordAttemptCount++;
                    user.FailedPasswordAttemptLastDateUTC = DateTime.UtcNow;
                    user.Save();
                }
                else if (user.IsLockedOut)
                {
                    toReturn = AuthenticateResult.UserIsLockedOut;
                    user = null;
                }
                else if (!user.IsApproved && !this.OverrideIsApproved)
                {
                    toReturn = AuthenticateResult.UserIsNotActivated;
                    user = null;
                }
                else
                {
                    if (user.FailedPasswordAttemptCount > 0)
                    {
                        user.FailedPasswordAttemptCount = 0;
                        user.SetNewFieldValue(fieldMappings.FailedPasswordAttemptLastDate, null);
                        user.SetNewFieldValue(fieldMappings.FailedPasswordAttemptWindowStart, null);
                        user.Save();
                    }
                    toReturn = AuthenticateResult.Succes;
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Authenticates a user and sets the authentication cookie
        /// </summary>
        /// <param name="username">The username of the user to authenticate and sign on</param>
        /// <param name="password">The password of the user to authenticate and sign on</param>
        public bool AuthenticateUserAndSignOn(string username, string password)
        {
            bool success = false;

            IManagableUser user = null;

            AuthenticateResult result = AuthenticateUserAndRetrieve(username, password, out user);
            if (result == AuthenticateResult.Succes)
            {
                FormsAuthentication.SetAuthCookie(user.UserId.ToString(), true);
                success = true;
            }

            return success;
        }

        /// <summary>
        /// Authenticate a User
        /// </summary>
        /// <param name="user">User Entity containing the user to be authenticated</param>
        /// <param name="nonEncodedPassword">Password</param>
        /// <returns>Authentication status</returns>
        public bool AuthenticateUser(IManagableUser user, string nonEncodedPassword)
        {
            return CheckPassword(nonEncodedPassword, user.Password, user);
        }

        /// <summary>
        /// Sets the password for a user and saves the entity
        /// </summary>
        /// <param name="user">The IManagableUser instance to set the password for</param>
        /// <param name="newPassword">The new password for the specified user</param>
        public void SetPassword(IManagableUser user, string newPassword)
        {
            user.Password = EncodePassword(newPassword, user);
            /*
			if (instance.UsePasswordSalting)
			{
				// Apply salting
				if (user.RandomValue == string.Empty || user.RandomValue.Length < 10)
					user.RandomValue = Dionysos.Web.PasswordHashUtil.GetSalt(128);

				string salt = user.RandomValue;

				user.Password = Dionysos.Web.PasswordHashUtil.GetPasswordHash(newPassword, ref salt);
			}
			else if (instance.PasswordFormat == PasswordFormat.Encrypted)
			{
				user.Password = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(newPassword);
			}
			else
			{
				// No salting
				user.Password = newPassword;
			}
			*/
            user.FailedPasswordAttemptCount = null;
            user.FailedPasswordAttemptLastDateUTC = null;
            user.FailedPasswordAttemptWindowStartUTC = null;
            user.Save();
        }

        /// <summary>
        /// Validates a user entity object by checking whether certain required fields are available
        /// </summary>
        /// <param name="entityName">The name of the entity to check</param>
        private void ValidateUserEntityObject(string entityName)
        {
            IEntity userEntity = Dionysos.DataFactory.EntityFactory.GetEntity(entityName) as IEntity;

            if (userEntity.Fields[fieldMappings.Username] == null)
            {
                throw new Dionysos.TechnicalException(string.Format("UserManager: Provided UserEntity doesn't contain a the field \"{0}\" (check the FieldMappings on ther UserManager and make sure the FieldMappings are set before the EntityName is provided", fieldMappings.Username));
            }
            if (userEntity.Fields[fieldMappings.Password] == null)
            {
                throw new Dionysos.TechnicalException(string.Format("UserManager: Provided UserEntity doesn't contain a the field \"{0}\" (check the FieldMappings on ther UserManager and make sure the FieldMappings are set before the EntityName is provided", fieldMappings.Password));
            }
            if (this.UsePasswordSalting)
            {
                if (userEntity.Fields[fieldMappings.RandomValue] == null)
                {
                    throw new Dionysos.TechnicalException(string.Format("UserManager: Provided UserEntity doesn't contain a the field \"{0}\", only required for PasswordSalting, set to FALSE if not required before the EnityType is set. (check the FieldMappings on ther UserManager and make sure the FieldMappings are set before the EntityName is provided", fieldMappings.RandomValue));
                }
            }

            if (!(userEntity is IManagableUser))
            {
                throw new Dionysos.TechnicalException("UserManager: Provided User Entity doesn't implement the IManagableUser interface");
            }
        }

        /// <summary>
        /// Refresh Current User
        /// </summary>
        public static void RefreshCurrentUser()
        {
            SessionHelper.RemoveValue(CurrentUserCacheKey);
        }

        #endregion
    }
}
