﻿using Dionysos.IO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Dionysos.Web
{
    /// <summary>
    /// Singleton utility class for paths in web applications 
    /// </summary>
    public class PathHelper
    {
        #region Fields

        static PathHelper instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the PathHelper class if the instance has not been initialized yet
        /// </summary>
        public PathHelper()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new PathHelper();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static PathHelper instance
        /// </summary>
        private void Init()
        {
        }

        public static string GetFullUrl(string relativeUrl)
        {
            string Port = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (Port == null || Port == "80" || Port == "443")
                Port = "";
            else
                Port = ":" + Port;

            string Protocol = HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (Protocol == null || Protocol == "0")
                Protocol = "http://";
            else
                Protocol = "https://";

            // *** Figure out the base Url which points at the application's root
            return Protocol + HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + Port + relativeUrl;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Web server relative path to the application root
        /// Example: /WestwindWebStore/
        /// </summary>
        public static string ApplicationPath
        {
            get
            {
                return HttpContext.Current.Request.ApplicationPath;
            }
        }

        /// <summary>
        /// Gets the file system path to the application root
        /// Example: D:\inetpub\wwwroot\WestWindWebStore\
        /// </summary>
        public static string PhysicalApplicationPath
        {
            get
            {
                return HttpContext.Current.Request.PhysicalApplicationPath;
            }
        }

        /// <summary>
        /// Gets the full file system path to the currently executing page
        /// Example: D:\inetpub\wwwroot\WestWindWebStore\Item.aspx
        /// </summary>
        public static string PhysicalPath
        {
            get
            {
                return HttpContext.Current.Request.PhysicalPath;
            }
        }

        // Moved from Dionysos.FileUtil
        /// <summary>
        /// Tries to get a physical path for the supplied path
        /// This means first checks if it's a full valid path and
        /// then if it's a virtual path and try to work with Server.MapPath
        /// </summary>
        /// <param name="path">Path to find the physical path for</param>
        /// <returns>Boolean indicating the file could be found</returns>
        public static bool GetPhysicalPath(ref string path)
        {
            bool success = false;

            if (FileUtil.FileExists(path))
            {
                success = true;
            }
            else if (System.Web.HttpContext.Current != null)
            {
                string mappedPath = System.Web.HttpContext.Current.Server.MapPath(path);
                if (FileUtil.FileExists(mappedPath))
                {
                    success = true;
                    path = mappedPath;
                }
            }

            return success;
        }

        /// <summary>
        /// Gets the Web server relative path to the currently executing page
        /// Example: /WestwindWebStore/item.aspx
        /// </summary>
        public static string FilePath
        {
            get
            {
                return HttpContext.Current.Request.FilePath;
            }
        }

        /// <summary>
        /// Gets the file name of the currently executing page
        /// Example: item.aspx
        /// </summary>
        public static string FileName
        {
            get
            {
                return FilePath.ToLower().Replace(ApplicationPath.ToLower(), string.Empty).Replace("/","");
            }
        }

        /// <summary>
        /// Gets the Web server relative url inclusief query string
        /// Example: /WestwindWebStore/item.aspx?sku=WWHELP30
        /// </summary>
        public static string RawUrl
        {
            get
            {
                return HttpContext.Current.Request.RawUrl;
            }
        }

        /// <summary>
        /// Gets the fully qualified url
        /// Example: http://www.west-wind.com/Webstore/item.aspx?sku=WWHELP30
        /// </summary>
        public static string Url
        {
            get
            {
                return HttpContext.Current.Request.Url.ToString();
            }
        }

        /// <summary>
        /// Gets the fully qualified url without the query string
        /// Example: http://www.west-wind.com/Webstore/item.aspx
        /// </summary>
        public static string UrlWithoutQueryString
        {
            get
            {
                string url = Url;
                if (url.Contains("?"))
                    url = url.Substring(0, url.IndexOf("?"));
                return url;
            }
        }

        #endregion
    }
}