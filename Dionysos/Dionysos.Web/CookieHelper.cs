﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Web;
using System.Linq;

namespace Dionysos.Web
{
	/// <summary>
	/// Class which represents a query string
	/// </summary>
	public class CookieHelper
	{
		#region Fields

		static CookieHelper instance = null;
		const string passPhrase = "BigBrotherpr@se";        // can be any string
		const string saltValue = "s@1tBrother";        // can be any string
		const string hashAlgorithm = "SHA1";             // can be "MD5"
		const int passwordIterations = 2;                  // can be any number
		const string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
		const int keySize = 256;                // can be 192 or 128        

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Lynx_media.Web.CookieHelper type if the instance has not been initialized yet
		/// </summary>
		static CookieHelper()
		{
			if (instance == null)
			{
				// first create the instance
				instance = new CookieHelper();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}

		#endregion

		#region Methods

		private void Init()
		{
		}

		/// <summary>
		/// Checks whether a Cookie parameter exists in the Cookie instance
		/// </summary>
		/// <param name="name">The name of the Cookie parameter</param>        
		/// <returns>
		/// True if the Cookie parameter exists, False if not
		/// </returns>
		public static bool HasParameter(string name)
		{
			return HasParameter(name, false);
		}

		/// <summary>
		/// Checks whether a Cookie parameter exists in the Cookie instance
		/// </summary>
		/// <param name="name">The name of the Cookie parameter</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// True if the Cookie parameter exists, False if not
		/// </returns>
		public static bool HasParameter(string name, bool encrypted = false)
		{
			bool hasParameter = true;

			if (Instance.ArgumentIsEmpty(name, "name"))
			{
				// Argument 'name' is empty
			}
			else
			{
				if (encrypted)
				{
					name = CookieHelper.EncryptValue(name);
				}

				HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
				if (Instance.Empty(cookie))
				{
					hasParameter = false;
				}
			}

			return hasParameter;
		}

        public static bool TryGetValue<T>(string key, out T value, bool encrypted = false)
        {
            bool toReturn = false;
            value = default(T);
            HttpContext context = HttpContext.Current;
            if (context == null)            
                throw new ArgumentNullException("context");
            else if(context.Request == null)
                throw new ArgumentNullException("context.Request");
            else if (context.Request.Cookies == null)
                throw new ArgumentNullException("context.Request.Cookies");            

            if (key != null && context.Request.Cookies.AllKeys.Contains(key))
            {
                string valueAsString = context.Request.Cookies[key].Value;

                // Conversion
                value = (T)Convert.ChangeType(valueAsString, typeof(T));
                toReturn = true;
            }
            
            return toReturn;
        }

		/// <summary>
		/// Retrieves a Cookie parameter from the Cookie instance
		/// </summary>
		/// <param name="name">The name of the Cookie parameter</param>        
		/// <returns>
		/// An object instance containing the value of the Cookie parameter
		/// </returns>
		public static string GetParameter(string name)
		{
			return GetParameter(name, false);
		}

		/// <summary>
		/// Retrieves a Cookie parameter from the Cookie instance
		/// </summary>
		/// <param name="name">The name of the Cookie parameter</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An object instance containing the value of the Cookie parameter
		/// </returns>
		public static string GetParameter(string name, bool encrypted = false)
		{
			string value = string.Empty;

			if (Instance.ArgumentIsEmpty(name, "name"))
			{
				// Argument 'name' is empty
			}
			else
			{
				if (encrypted)
				{
					name = CookieHelper.EncryptValue(name);
				}
				HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
				if (Instance.Empty(cookie) ||
					(cookie.Expires != DateTime.MinValue && cookie.Expires < DateTime.Now))
				{
					// No cookie found or expired
				}
				else
				{
					if (Instance.Empty(cookie.Value))
					{
						// String Emtpy
					}
					else
					{
						if (encrypted)
							value = CookieHelper.DecryptValue(cookie.Value);
						else
							value = cookie.Value;
					}
				}
			}

			return value;
		}

		/// <summary>
		/// Gets a System.Boolean value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// An System.Boolean instance containing the current value
		/// </returns>
		public static bool GetParameterAsBoolean(string name)
		{
			return GetParameterAsBoolean(name, false);
		}

		// GK Yes, this is not consistent with the other methods, but because of all the BOOL parameters
		// changing it in the same way as the others could give ambigious / unexpected results.
		/// <summary>
		/// Gets a System.Boolean value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Boolean instance containing the current value
		/// </returns>
		public static bool GetParameterAsBooleanEncrypted(string name)
		{
			return bool.Parse(GetParameter(name, true));
		}

		/// <summary>
		/// Gets a System.Boolean value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name, if it's not in the query string the defaultValue is returned
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="defaultValue">if set to <c>true</c> [default value].</param>        
		/// <returns>
		/// An System.Boolean instance containing the current value or defaultValue
		/// </returns>
		public static bool GetParameterAsBoolean(string name, bool defaultValue)
		{
			if (CookieHelper.HasParameter(name, false))
				return bool.Parse(GetParameter(name, false));
			else
				return defaultValue;
		}

		// GK Yes, this is not consistent with the other methods, but because of all the BOOL parameters
		// changing it in the same way as the others could give ambigious / unexpected results.
		/// <summary>
		/// Gets a System.Boolean value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name, if it's not in the query string the defaultValue is returned
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="defaultValue">if set to <c>true</c> [default value].</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Boolean instance containing the current value or defaultValue
		/// </returns>
		public static bool GetParameterAsBooleanEncrypted(string name, bool defaultValue)
		{
			if (CookieHelper.HasParameter(name, true))
				return bool.Parse(GetParameter(name, true));
			else
				return defaultValue;
		}

		/// <summary>
		/// Gets a System.Byte value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// An System.Byte instance containing the current value
		/// </returns>
		public static byte GetParameterAsByte(string name)
		{
			return GetParameterAsByte(name, false);
		}

		/// <summary>
		/// Gets a System.Byte value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Byte instance containing the current value
		/// </returns>
		public static byte GetParameterAsByte(string name, bool encrypted = false)
		{
			return byte.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a System.Char value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// An System.Char instance containing the current value
		/// </returns>
		public static char GetParameterAsChar(string name)
		{
			return GetParameterAsChar(name, false);
		}

		/// <summary>
		/// Gets a System.Char value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Char instance containing the current value
		/// </returns>
		public static char GetParameterAsChar(string name, bool encrypted = false)
		{
			return char.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a System.Decimal value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// An System.Decimal instance containing the current value
		/// </returns>
		public static decimal GetParameterAsDecimal(string name)
		{
			return GetParameterAsDecimal(name, false);
		}

		/// <summary>
		/// Gets a System.Decimal value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Decimal instance containing the current value
		/// </returns>
		public static decimal GetParameterAsDecimal(string name, bool encrypted = false)
		{
			return decimal.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a System.Double value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// An System.Double instance containing the current value
		/// </returns>
		public static double GetParameterAsDouble(string name)
		{
			return GetParameterAsDouble(name, false);
		}

		/// <summary>
		/// Gets a System.Double value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Double instance containing the current value
		/// </returns>
		public static double GetParameterAsDouble(string name, bool encrypted = false)
		{
			return double.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a System.Single value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Single instance containing the current value
		/// </returns>
		public static float GetParameterAsFloat(string name)
		{
			return GetParameterAsFloat(name, false);
		}

		/// <summary>
		/// Gets a System.Single value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Single instance containing the current value
		/// </returns>
		public static float GetParameterAsFloat(string name, bool encrypted = false)
		{
			return float.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a System.Int32 value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// An System.Int32 instance containing the current value
		/// </returns>
		public static int GetParameterAsInt(string name)
		{
			return GetParameterAsInt(name, false);
		}

		/// <summary>
		/// Gets a System.Int32 value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Int32 instance containing the current value
		/// </returns>
		public static int GetParameterAsInt(string name, bool encrypted = false)
		{
			return int.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a System.Int32 value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name, if it fails it returns the supplied defaultValue
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="defaultValue">The default value.</param>        
		/// <returns>
		/// An System.Int32 instance containing the current value
		/// </returns>
		public static int GetParameterAsInt(string name, int defaultValue)
		{
			return GetParameterAsInt(name, false);
		}

		/// <summary>
		/// Gets a System.Int32 value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name, if it fails it returns the supplied defaultValue
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Int32 instance containing the current value
		/// </returns>
		public static int GetParameterAsInt(string name, int defaultValue, bool encrypted = false)
		{
			if (CookieHelper.HasParameter(name, encrypted))
			{
				int.TryParse(GetParameter(name, encrypted), out defaultValue);
			}
			return defaultValue;
		}

		/// <summary>
		/// Gets a System.Int64 value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Int64 instance containing the current value
		/// </returns>
		public static long GetParameterAsLong(string name)
		{
			return GetParameterAsLong(name, false);
		}

		/// <summary>
		/// Gets a System.Int64 value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Int64 instance containing the current value
		/// </returns>
		public static long GetParameterAsLong(string name, bool encrypted = false)
		{
			return long.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a System.Int16 value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// An System.Int16 instance containing the current value
		/// </returns>
		public static short GetParameterAsShort(string name)
		{
			return GetParameterAsShort(name, false);
		}

		/// <summary>
		/// Gets a System.Int16 value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Int16 instance containing the current value
		/// </returns>
		public static short GetParameterAsShort(string name, bool encrypted = false)
		{
			return short.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a DateTime value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// An DateTime instance containing the current value
		/// </returns>
		public static System.DateTime GetParameterAsDateTime(string name)
		{
			return GetParameterAsDateTime(name, false);
		}

		/// <summary>
		/// Gets a DateTime value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An DateTime instance containing the current value
		/// </returns>
		public static System.DateTime GetParameterAsDateTime(string name, bool encrypted = false)
		{
			return System.DateTime.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a TimeStamp value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>        
		/// <returns>
		/// A TimeStamp instance containing the current value
		/// </returns>
		public static TimeStamp GetParameterAsTimeStamp(string name)
		{
			return GetParameterAsTimeStamp(name, false);
		}

		/// <summary>
		/// Gets a TimeStamp value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// A TimeStamp instance containing the current value
		/// </returns>
		public static TimeStamp GetParameterAsTimeStamp(string name, bool encrypted = false)
		{
			return TimeStamp.Parse(GetParameter(name, encrypted));
		}

		/// <summary>
		/// Gets a System.Int32 instance of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="enumType">Type of the enum.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Int32 instance containing the current value
		/// </returns>
		public static int GetParameterAsEnumValue(string name, System.Type enumType)
		{
			return GetParameterAsEnumValue(name, enumType, false);
		}

		/// <summary>
		/// Gets a System.Int32 instance of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="enumType">Type of the enum.</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		/// <returns>
		/// An System.Int32 instance containing the current value
		/// </returns>
		public static int GetParameterAsEnumValue(string name, System.Type enumType, bool encrypted = false)
		{
			Enum temp = (Enum)Enum.Parse(enumType, GetParameter(name, encrypted), true);
			return Convert.ToInt32(temp);
		}

		/// <summary>
		/// Gets a System.String value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <returns>An System.String instance containing the current value</returns>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		public static string GetParameterAsString(string name)
		{
			return GetParameterAsString(name, false);
		}

		/// <summary>
		/// Gets a System.String value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name
		/// </summary>
		/// <returns>An System.String instance containing the current value</returns>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		public static string GetParameterAsString(string name, bool encrypted = false)
		{
			return GetParameter(name, encrypted);
		}

		/// <summary>
		/// Gets a System.String value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name, if it fails (not available) it returns the supplied defaultValue
		/// </summary>
		/// <returns>An System.String instance containing the current value or default value if failed (not available)</returns>
		public static string GetParameterAsString(string name, string defaultValue)
		{
			return GetParameterAsString(name, false);
		}

		/// <summary>
		/// Gets a System.String value of a Cookie parameter using the specified
		/// HttpRequest instance and the specified parameter name, if it fails (not available) it returns the supplied defaultValue
		/// </summary>
		/// <returns>An System.String instance containing the current value or default value if failed (not available)</returns>
		public static string GetParameterAsString(string name, string defaultValue, bool encrypted = false)
		{
			if (CookieHelper.HasParameter(name, encrypted))
			{
				return CookieHelper.GetParameterAsString(name, encrypted);
			}
			else
			{
				return defaultValue;
			}
		}

		/// <summary>
		/// Adds a cookie to the current http response
		/// </summary>
		/// <param name="name">The name of the cookie to add</param>
		/// <param name="value">The value of the cookie to add</param>
		/// <param name="expireDateTime">The expire date of the cookie</param>        
		public static void AddCookie(string name, object value, DateTime expireDateTime)
		{
			CookieHelper.AddCookie(name, value, expireDateTime, false);
		}

		/// <summary>
		/// Adds a cookie to the current http response
		/// </summary>
		/// <param name="name">The name of the cookie to add</param>
		/// <param name="value">The value of the cookie to add</param>
		/// <param name="expireDateTime">The expire date of the cookie</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		public static void AddCookie(string name, object value, DateTime expireDateTime, bool encrypted = false, bool httpOnly = false)
		{
			if (encrypted)
			{
				name = CookieHelper.EncryptValue(name);
				value = CookieHelper.EncryptValue(value.ToString());
			}

			HttpCookie cookie = new HttpCookie(name, value.ToString());
			cookie.Expires = expireDateTime;
            cookie.HttpOnly = httpOnly;
			HttpContext.Current.Response.Cookies.Add(cookie);
		}

		/// <summary>
		/// Removes a cookie from the current http response
		/// </summary>
		/// <param name="name">The name of the cookie to remove</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		public static void RemoveCookie(string name)
		{
			CookieHelper.RemoveCookie(name, false);
		}

		/// <summary>
		/// Removes a cookie from the current http response
		/// </summary>
		/// <param name="name">The name of the cookie to remove</param>
		/// <param name="encrypted">If set to <c>true</c> both name and value are encrypted in the cookie.</param>
		public static void RemoveCookie(string name, bool encrypted = false)
		{
			if (encrypted)
			{
				name = CookieHelper.EncryptValue(name);
			}

			if (HasParameter(name, false) && HttpContext.Current.Request.Cookies.AllKeys.Contains(name))
			{
				HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
				if (cookie != null)
					cookie.Expires = DateTime.Now.AddDays(-1);
				HttpContext.Current.Response.Cookies.Set(cookie);
			}
		}

		/// <summary>
		/// Extends the Expirey date of a cookie, won't change anything if the cookie is not availebl.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="daysFromToday"></param>
		/// <param name="encrypted"></param>
		public static void ExtendCookie(string name, int daysFromToday, bool encrypted = false)
		{
			if (encrypted)
			{
				name = CookieHelper.EncryptValue(name);
			}

			if (HasParameter(name, encrypted) && HttpContext.Current.Request.Cookies.AllKeys.Contains(name))
			{
				HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
				if (cookie != null)
					cookie.Expires = DateTime.Now.AddDays(daysFromToday);
				HttpContext.Current.Response.Cookies.Set(cookie);
			}
		}

		private static string EncryptValue(string value)
		{
			return HttpUtility.UrlEncode(CrypographyUtil.Encrypt(value, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize));
		}

		private static string DecryptValue(string value)
		{
			return CrypographyUtil.Decrypt(HttpUtility.UrlDecode(value), passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
		}

		#endregion
	}
}