﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;
using Dionysos.Web.HttpModules.RewriteUnits;

namespace Dionysos.Web
{
	/// <summary>
	/// Configuration class for Dionysos.Web
	/// </summary>
	public class DionysosWebConfigurationInfo : ConfigurationItemCollection
	{
		/// <summary>
		/// Constructs an instance of the B2BConfiguration class
		/// </summary>
		public DionysosWebConfigurationInfo()
		{
			string sectionName = "Dionysos.Web";

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.PageSize, "Standaard aantal te tonen regels in een overzicht", 20, typeof(int), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.CollectionViewMode, "Standaard methode van tonen", (int)CollectionViewMode.List, typeof(CollectionViewMode), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.CollectionViewModeFrontend, "Standaard methode van tonen in frontend", (int)CollectionViewMode.Tiles, typeof(CollectionViewMode), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UseLlblGenPaging, "Maak gebruik van 'paging' (LLBL style)", true, typeof(bool), true));

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UseUserManager, "Maak gebruik van UserManagement via de UserManager", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.AutoLogon, "Schakel het automatisch inloggen als standaard gebruiker in of uit", false, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.AutoLogonUsername, "De te gebruiken gebruikersnaame voor het automatisch inloggen ", "", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.PasswordSalting, "Wachtwoorden versleutelen zodat ze niet meer uitgelezen kunnen worden (Salting + Hashing)", true, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.IsApprovedOverride, "Sla het controleren op 'Bevestigd' (IsApproved) over voor het inloggen", true, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.AllowUnlimitedSignOnAttempts, "Toestaan dat er oneindig vaak wordt geprobeerd in te loggen, zonder blokkering.", false, typeof(bool)));

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.ShowGridViewViewCommand, "De 'Bekijken' knop tonen in GridViews ", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.SavePageControlActiveTabs, "Sla actieve tab van PageControl op tussen pagina's.", false, typeof(bool)));

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UrlRewriteCultureInformation, "Parse Culture Information uit de Url's", false, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UrlRewritePageUrl, "Parse volledige PageUrl's voor Cms gebruik", false, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UrlRewriteHandlingFile, "De file waarnaar geparsed url rewritten requests moeten. Default: Default.aspx", "Default.aspx", typeof(string), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UrlRewriteDefaultPage, "Kan gebruikt worden om te zorgen dat een www.site.nl/ request naar een andere pagina gaat dan de default handling page", "", typeof(string), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UrlRewriteByMultipleHandlingFiles, "Stel in dat het begin van de url wordt gerbuikt als handling file, /producten/group-a verwijst naar producten.aspx?PageUrl=group-a", false, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.ExcludedPathsInPathRewriting, "Paden die overgeslagen moeten worden in url rewriting (komma gescheiden).", string.Empty, typeof(string), true));

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.CssJsCombining, "Combineer Css/Js Files (werkt alleen indien ze op de MasterPage met AddStyleSheetLinks/AddJavascriptIncludes zijn toegevoegd).", false, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.CssJsCompressing, "Compress Css/Js Files (werkt alleen met CssJsCombining en indien ze op de MasterPage met AddStyleSheetLinks/AddJavascriptIncludes zijn toegevoegd).", true, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.CssJsMinification, "Minify Css/Js Files (werkt alleen met CssJsCombining en indien ze op de MasterPage met AddStyleSheetLinks/AddJavascriptIncludes zijn toegevoegd).", true, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.CssExplicitlyMediaScreenOnly, "Excpliciet zetten van 'Media=screen' voor linked Css files op de MasterPage.", false, typeof(bool), true));

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.GoogleGenericApiKey, "Google Server API Key", "", typeof(string), true));

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.GoogleMapsApiKey, "Google Browser API key (o.a. Maps - je API key is hetzelfde voor elke service) API Key (Standaard voor studio.amteam.nl)", "ABQIAAAAUqmPZC5xOrgKJoqE4HCKIRT0VdAE2y5c3Bk0LxWTyQ1qiRu6qBRlp0zzPXNhiKPaWRL3op-JMnjDcA", typeof(string), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.GoogleAnalyticsTrackerId, "Google Analytics key", "", typeof(string), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.GoogleAnalyticsSiteSpeed, "Google Analytics Site Speed activeren", false, typeof(bool), true));

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.FileNotFoundPageFileName, "Naam van het bestand voor 404 errors (standaard: 404.aspx)", "404.aspx", typeof(string), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.DisableCache, "Disabled alle cache functionaliteit via de CacheHelper", false, typeof(bool)));

            this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.TrimText, "Trim string on databind", false, typeof(bool)));

			// PreferredDomain
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.PreferredDomain, "Het voorkeursdomein dat u wilt gebruiken.", "", typeof(string), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.PreferredDomainRewriteType, "Het type rewriting voor het voorkeursdomein (0 = PreferredDomain, 1 = AddWww, 2 = RemoveWww).", (int)DomainRewriteType.PreferredDomain, typeof(DomainRewriteType), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.RedirectToHttps, "Normaal HTTP verkeer omleiden naar beveiligd HTTPS verkeer.", false, typeof(bool), true));

			// TextBox
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.TextBoxDefaultCssClass, "Standaard Css class voor TextBox elementen.", "input", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.TextBoxCustomDefaultValueImplementation, "Handmatige implementatie voor TextBox.DefaultValue handling", false, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.TextBoxDecimalAllowPeriodAndCommaAsDecimalSeperator, "Gebruik punt en komma als verdeler van een decimaal getal.", false, typeof(bool)));

			// Globalization
			//this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.Globalization_Enabled, "Switch to enable/disable Globalization.", false, typeof(bool)));			
			//this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.Globalization_DefaultCulture, "Default Culture String.", "nl-NL", typeof(string)));
			//this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.Globalization_AutoLearnControls, "Determine if the system should auto learn non translated controls.", false, typeof(bool)));

			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UsePostRedirectGet, "Gebruik PRG design pattern voor het opslaan van entities op PageLLBLGenEntity pagina's.", true, typeof(bool), true));

			// Twitter
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.TwitterConsumerKey, "De Twitter consumer key voor autorisatie.", "xNPQUxxdrQxMJNYHD69TQ", typeof(String), false));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.TwitterConsumerSecret, "De Twitter consumer secret voor autorisatie.", "4Pc3lkHM6UyovMcnoqom1X3dSlOxBdeaOA8c2jQlzo", typeof(String), false));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.TwitterToken, "De Twitter access token voor het opvragen van afgeschremde gegevens.", String.Empty, typeof(String), false));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.TwitterTokenSecret, "De Twitter access token secret voor het opvragen van afgeschermde gegevens.", String.Empty, typeof(String), false));

			// Facebook
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.FacebookAppId, "De Facebook App ID voor authorisatie.", "163445853768386", typeof(String), false));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.FacebookAppSecret, "De Facebook App Secret voor autorisatie.", "dfd4ba86c6af423d342deb7c58926c80", typeof(String), false));

			// LinkedIn
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.LinkedInConsumerKey, "De LinkedIn API key voor autorisatie.", "oaaxcelj3z51", typeof(String), false));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.LinkedInConsumerSecret, "De LinkedIn Secret key voor autorisatie.", "SQLQfRJh0lO8aMnK", typeof(String), false));

            // Google
            this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.GoogleClientId, "De Google Client ID voor autorisatie.", "1056171545705-mjo1d3a239v257fi8mdrdvebcirpdf3v.apps.googleusercontent.com", typeof(String), false));
            this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.GoogleClientSecret, "De Google Secret key voor autorisatie.", "6YfaWf9e2BLx6usdSCt2cScL", typeof(String), false));

			// Error logging
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.LogApplicationStartAndRestart, "Log Application_Start en Application_SessionReset.", false, typeof(bool), true));
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.ErrorLoggerWebEmailRecipientAddresses, "De e-mailadressen waarnaar de error logs verstuurd worden (puntkomma gescheiden).", String.Empty, typeof(String), true));

			// Cookie consent
			this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.CookieConsentEnabled, "Cookie consent balk inschakelen.", false, typeof(bool), true));

            this.Add(new ConfigurationItem(sectionName, DionysosWebConfigurationConstants.UltraControlRenderer, "How are Ultra Controls rendered? 1 = ASPNET, 2 = DevEx", 1, typeof(int), true));
		}
	}
}
