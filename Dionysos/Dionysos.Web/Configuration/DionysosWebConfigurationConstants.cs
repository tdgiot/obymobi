﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Web
{
	/// <summary>
	/// Configuration class which contains constants for the names of configuration items
	/// </summary>
	public class DionysosWebConfigurationConstants
	{
		public const string PageSize = "DefaultPageSize";
		public const string CollectionViewMode = "DefaultViewMode";
		public const string CollectionViewModeFrontend = "DefaultViewModeFrontend";
		public const string UseLlblGenPaging = "UseLlblGenPaging";
		public const string UseUserManager = "UseUserManager";
		public const string DisableCache = "DisableCache";
		public const string AutoLogon = "AutoLogon";
		public const string AutoLogonUsername = "AutoLogonUsername";
		public const string PasswordSalting = "PasswordSalting";
		public const string IsApprovedOverride = "IsApprovedOverride";
		public const string ShowGridViewViewCommand = "ShowGridViewViewCommand";
		public const string SavePageControlActiveTabs = "SavePageControlActiveTabs";
		public const string UrlRewriteCultureInformation = "UrlRewriteCultureInformation";
		public const string UrlRewriteHandlingFile = "UrlRewriteHandlingFile";
		public const string UrlRewriteDefaultPage = "UrlRewriteDefaultPage";
		public const string UrlRewriteByMultipleHandlingFiles = "UrlRewriteByMultipleHandlingFiles";
		public const string UrlRewritePageUrl = "UrlRewritePageUrl";
		public const string FileNotFoundPageFileName = "404PageFileName";
		public const string ExcludedPathsInPathRewriting = "ExcludedPathsInPathRewriting";
		public const string CssJsMinification = "CssJsMinification";
		public const string CssJsCompressing = "CssJsCompressing";
		public const string CssJsCombining = "CssJsCombining";
		public const string CssExplicitlyMediaScreenOnly = "CssExplicitlyMediaScreenOnly";
		public const string GoogleMapsApiKey = "GoogleMapsApiKey";
		public const string GoogleAnalyticsTrackerId = "GoogleAnalyticsTrackerId";
		public const string GoogleAnalyticsSiteSpeed = "GoogleAnalyticsSiteSpeed";
		public const string GoogleGenericApiKey = "GoogleGenericApiKey";
		public const string PreferredDomain = "PreferredDomain";
		public const string PreferredDomainRewriteType = "PreferredDomainRewriteType";
		public const string RedirectToHttps = "RedirectToHttps";
		public const string TextBoxDefaultCssClass = "TextBox_DefaultCssClass";
		public const string TextBoxCustomDefaultValueImplementation = "TextBox_CustomDefaultValueImplementation";
		public const string TextBoxDecimalAllowPeriodAndCommaAsDecimalSeperator = "TextBoxDecimalAllowPeriodAndCommaAsDecimalSeperator ";
		public const string AllowUnlimitedSignOnAttempts = "AllowUnlimitedSignOnAttempts";
		public const string UsePostRedirectGet = "PostRedirectGet";
		public const string TwitterConsumerKey = "TwitterConsumerKey";
		public const string TwitterConsumerSecret = "TwitterConsumerSecret";
		public const string TwitterToken = "TwitterToken";
		public const string TwitterTokenSecret = "TwitterTokenSecret";
		public const string FacebookAppId = "FacebookAppId";
		public const string FacebookAppSecret = "FacebookAppSecret";
		public const string LinkedInConsumerKey = "LinkedInConsumerKey";
		public const string LinkedInConsumerSecret = "LinkedInConsumerSecret";
        public const string GoogleClientId = "GoogleClientId";
        public const string GoogleClientSecret = "GoogleClientSecret";
		public const string LogApplicationStartAndRestart = "LogApplicationStartAndRestart";
		public const string ErrorLoggerWebEmailRecipientAddresses = "ErrorLoggerWebEmailRecipientAddresses";
		public const string CookieConsentEnabled = "CookieConsentEnabled";
	    public const string TrimText = "TrimText";
        public const string UltraControlRenderer = "UltraControlRenderer";
	}
}
