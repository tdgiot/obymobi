﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web
{
	/// <summary>
	/// Enumerator class for the mode of pages which contain data entities
	/// </summary>
	public enum CollectionPageMode
	{
		/// <summary>
		/// The details of an entity are being viewed
		/// </summary>
		View,

		/// <summary>
		/// The details of an entity are being edited
		/// </summary>
		print
	}
}
