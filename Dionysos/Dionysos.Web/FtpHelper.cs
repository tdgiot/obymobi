﻿using System.IO;
using System.Net;
using System.Text;

namespace Dionysos.Web
{
    public static class FtpHelper
    {
        public static string UploadFile(string ftpUrl, string filename)
        {
            return UploadFile(ftpUrl, "anonymous", "johndoe@contoso.com", filename);
        }

        public static string UploadFile(string ftpUrl, string username, string password, string filename)
        {
            var  fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            return UploadFile(ftpUrl, username, password, fs);
        }

        public static string UploadFile(string ftpUrl, string username, string password, FileStream fs)
        {            
            var imageData = new byte[fs.Length];
            fs.Read(imageData, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();

            return UploadFile(ftpUrl, username, password, imageData);
        }

        public static string UploadFile(string ftpUrl, string username, string password, byte[] fileBytes)
        {
            string returnValue;
            try
            {
                var request = CreateFtpWebRequest(ftpUrl);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.ContentLength = fileBytes.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileBytes, 0, fileBytes.Length);
                requestStream.Close();

                var response = (FtpWebResponse)request.GetResponse();
                response.Close();

                returnValue = response.StatusDescription;
            }
            catch (WebException ex)
            {
                returnValue = ex.Message;
            }

            return returnValue;
        }

        public static string TestConnect(string ftpUrl, string username, string password)
        {
            string returnValue;
            try
            {
                var request = CreateFtpWebRequest(ftpUrl);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(username, password);

                var response = (FtpWebResponse)request.GetResponse();

                response.Close();
                
                returnValue = response.StatusDescription;
            }
            catch (WebException ex)
            {
                returnValue = ex.Message;
            }

            return returnValue;
        }

        private static FtpWebRequest CreateFtpWebRequest(string ftpUrl)
        {
            if (!ftpUrl.StartsWith("ftp://"))
                ftpUrl = "ftp://" + ftpUrl;

            return (FtpWebRequest)WebRequest.Create(ftpUrl);
        }
    }
}
