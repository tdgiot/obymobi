﻿using System.Collections.Generic;

namespace Dionysos.Web.Caching
{
    public class DummyCachingStore : ICachingStore
    {
        public CachingStoreType Type { get { return CachingStoreType.Dummy; } }

        public bool HasCache { get { return true; } }

        public bool TryGetValue<T>(string key, out T value)
        {
            value = default(T);
            return false;
        }

        public bool HasValue(string key)
        {
            return false;
        }

        public T GetValue<T>(string key)
        {
            return default(T);
        }

        public bool Add<T>(string key, T value)
        {
            return false;
        }

        public bool AddAbsoluteExpire<T>(string key, T value, int expireInXMinutes)
        {
            return false;
        }

        public void AddSlidingExpire<T>(string key, T value, int expireInXMinutes)
        {
        }

        public bool RemoveValue(string key)
        {
            return false;
        }

        public void RemoveWildcard(string wildcard)
        {
        
        }

        public List<string> GetAllCacheKeys()
        {
            return new List<string>();
        }

        public void Clear()
        {
            
        }
    }
}