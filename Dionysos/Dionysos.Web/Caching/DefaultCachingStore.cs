﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;

namespace Dionysos.Web.Caching
{
    public class DefaultCachingStore : ICachingStore
    {
        public CachingStoreType Type { get { return CachingStoreType.InMemory; } }

        public bool HasCache
        {
            get
            {
                return HttpContext.Current != null && HttpContext.Current.Cache != null;
            }
        }

        public bool TryGetValue<T>(string key, out T value)
        {
            value = default(T);
            if (!HasCache)
                return false;

            object cacheValue = HttpContext.Current.Cache.Get(key);
            if (cacheValue != null && cacheValue is T)
            {
                // Value is valid
                value = (T)cacheValue;
                return true;
            }

            return false;
        }

        public T GetValue<T>(string key)
        {
            T value;
            TryGetValue(key, out value);

            return value;
        }

        public bool HasValue(string key)
        {
            object dummy;
            return TryGetValue(key, out dummy);
        }

        public bool Add<T>(string key, T value)
        {
            if (HasCache)
            {
                HttpContext.Current.Cache.Insert(key, value, null, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                return true;
            }

            return false;
        }

        public bool AddAbsoluteExpire<T>(string key, T value, int expireInXMinutes)
        {
            if (HasCache)
            {
                HttpContext.Current.Cache.Insert(key, value, null, DateTime.Now.AddMinutes(expireInXMinutes), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                return true;
            }

            return false;
        }

        public void AddSlidingExpire<T>(string key, T value, int expireInXMinutes)
        {
            if (HasCache)
            {
                HttpContext.Current.Cache.Insert(key, value, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, expireInXMinutes, 0), CacheItemPriority.Normal, null);
            }
        }

        public bool RemoveValue(string key)
        {
            if (!HasCache)
                return false;

            HttpContext.Current.Cache.Remove(key);

            return true;
        }

        public void RemoveWildcard(string pattern)
        {
            pattern = "^" + Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", ".") + "$";
            Regex regex = new Regex(pattern, RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);

            List<string> keys = new List<string>();
            IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (regex.IsMatch(enumerator.Key.ToString()))
                {
                    keys.Add(enumerator.Key.ToString());
                }
            }

            foreach (string key in keys)
            {
                HttpContext.Current.Cache.Remove(key);
            }
        }

        public List<string> GetAllCacheKeys()
        {
            List<string> keyList = new List<string>();

            if (!HasCache)
                return keyList;
            
            IDictionaryEnumerator cacheEnumerator = HttpContext.Current.Cache.GetEnumerator();
            while (cacheEnumerator.MoveNext())
            {
                keyList.Add((string)cacheEnumerator.Key);
            }
            return keyList;
        }

        /// <remarks>
        /// The enumerator only is valid as long as the original collection remains unchanged. So while the 
        /// code might work most of the time, there might be small errors in certain situations. Best is to 
        /// use the following code, which does essentially the same, but does not use the enumerator directly.
        /// </remarks>
        public void Clear()
        {
            List<string> keys = new List<string>();
            IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();

            while (enumerator.MoveNext())
            {
                keys.Add(enumerator.Key.ToString());
            }

            foreach (string t in keys)
            {
                HttpContext.Current.Cache.Remove(t);
            }
        }
    }
}