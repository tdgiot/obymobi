﻿namespace Dionysos.Web.Caching
{
    public enum CachingStoreType
    {
        InMemory,
        Redis,
        Dummy
    }
}