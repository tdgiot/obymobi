﻿using System.Collections.Generic;

namespace Dionysos.Web.Caching
{
    public interface ICachingStore
    {
        CachingStoreType Type { get; }

        /// <summary>
        /// Gets whether HttpContext.Current.Cache is not null.
        /// </summary>
        bool HasCache { get; }

        /// <summary>
        /// Tries to get the value from the Cache using the specified key.
        /// </summary>
        /// <param name="key">The key of the Cache value.</param>		
        /// <param name="value">When this method returns true, contains the value retrieved from the Cache; otherwise returns the default value for the specified type.</param>
        /// <returns>true if the key and corresponding value exists in the Cache; otherwise, false.</returns>
        bool TryGetValue<T>(string key, out T value);

        /// <summary>
        /// Gets the value from the Cache using the specified key.
        /// </summary>
        /// <param name="key">The key of the Cache value.</param>
        /// <returns>Returns the value, if found and type matches; otherwise returns the default value.</returns>
        T GetValue<T>(string key);

        /// <summary>
        /// Indicates whether the key exists in the Cache and is of the specified type.
        /// </summary>
        /// <param name="key">The key of the Cache value.</param>		
        /// <returns>true if the value exists in the Cache and is of the specified type; otherwise false.</returns>
        bool HasValue(string key);

        /// <summary>
        /// Add item to cache
        /// </summary>
        /// <param name="key">The cache key used to reference the item.</param>
        /// <param name="value">The item to be added to the cache.</param>
        bool Add<T>(string key, T value);

        /// <summary>
        /// Add item to the cache with absolute expiration
        /// </summary>
        /// <param name="key">The cache key used to reference the item.</param>
        /// <param name="value">The item to be added to the cache.</param>
        /// <param name="expireInXMinutes">The amount of minutes to keep it in cache (1440 minutes = 1 day)</param>
        bool AddAbsoluteExpire<T>(string key, T value, int expireInXMinutes);

        /// <summary>
        /// Add item to the cache with sliding expiration.
        /// </summary>
        /// <param name="key">The cache key used to reference the item.</param>
        /// <param name="value">The item to be added to the cache.</param>
        /// <param name="expireInXMinutes">The amount of minutes to slide</param>
        void AddSlidingExpire<T>(string key, T value, int expireInXMinutes);

        /// <summary>
        /// Removes the value with the specified key from the Cache.
        /// </summary>
        /// <param name="key">The key for the Cache value</param>
        /// <returns>true if Cache value is removed; otherwise false.</returns>
        bool RemoveValue(string key);

        void RemoveWildcard(string wildcard);

        /// <summary>
        /// Get All Cache Keys
        /// </summary>
        /// <returns></returns>
        List<string> GetAllCacheKeys();

        /// <summary>
        /// Removes all items from the Cache
        /// </summary>
        void Clear();
    }
}