using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Caching;
using Dionysos.Web.Caching;

namespace Dionysos.Web
{
    /// <summary>
    ///     Class which can support Caching on a per session basis.
    /// </summary>
    public static class CacheHelper
    {
        #region  Fields

        public const string LAST_CACHE_CLEARED = "LAST_CACHE_CLEARED";

        #endregion

        #region Constructors

        static CacheHelper()
        {
            CacheHelper.CachingStoreL1 = new DefaultCachingStore();
            CacheHelper.CachingStoreL2 = new DummyCachingStore();
        }

        #endregion

        #region Properties

        private static ICachingStore CachingStoreL1 { get; set; }
        private static ICachingStore CachingStoreL2 { get; set; }

        #endregion

        public static void SetCachingStore(ICachingStore store)
        {
            if (store == null)
            {
                store = new DummyCachingStore();
            }

            CacheHelper.CachingStoreL2 = store;
        }



        /// <summary>
        ///     Gets the Session specific key (SessionID + "__" + key).
        /// </summary>
        /// <param name="key">The key of the Cache value.</param>
        /// <returns>The Session specific key for a Cache value. If no session is available, returns the original key.</returns>
        public static string GetSessionSpecificCacheKey(string key)
        {
            return (SessionHelper.HasSession) ? SessionHelper.SessionID + "__" + key : key;
        }

        /// <summary>
        ///     Checks whether the Cache contains a value for the specified key.
        /// </summary>
        /// <param name="key">The key of the Cache value.</param>
        /// <param name="sessionSpecific">If true, checks whether a session specific value exists.</param>
        /// <returns>true if a value exists in the Cache for the specified key; otherwise false.</returns>
        //[Obsolete("Use TryGetValue to get a strong typed value from the Cache without exceptions (if the value is empty) or race conditions (if the cache times out before the value is retrieved).")]
        public static bool HasValue(string key, bool sessionSpecific)
        {
            bool hasValue = false;

            if (sessionSpecific)
            {
                // Get session specific key
                key = CacheHelper.GetSessionSpecificCacheKey(key);
            }

            if (key == null)
            {
                // Key can't be null
            }
            else if (ConfigurationManager.GetBool(DionysosWebConfigurationConstants.DisableCache))
            {
                // Cache is disabled
            }
            else
            {
                // Check value
                hasValue = CacheHelper.CachingStoreL1.HasValue(key);
                if (!hasValue)
                {
                    hasValue = CacheHelper.CachingStoreL2.HasValue(key);
                }
            }

            return hasValue;
        }

        /// <summary>
        ///     Tries to get the value from the Cache using the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="key">The key of the Cache value.</param>
        /// <param name="sessionSpecific">If true, gets the session specific value.</param>
        /// <param name="value">
        ///     When this method returns true, contains the value retrieved from the Cache; otherwise null
        /// </param>
        /// <returns>true if the key and corresponding value exists in the Cache; otherwise, false.</returns>
        public static bool TryGetValue<T>(string key, bool sessionSpecific, out T value)
        {
            if (sessionSpecific)
            {
                // Get session specific key
                key = CacheHelper.GetSessionSpecificCacheKey(key);
            }

            if (Instance.ArgumentIsEmpty(key, "key"))
            {
                // Key can't be null
            }
            else if (ConfigurationManager.GetBool(DionysosWebConfigurationConstants.DisableCache))
            {
                // Cache is disabled
            }
            else
            {
                // Get value
                if (CacheHelper.CachingStoreL1.TryGetValue(key, out value))
                {
                    return true;
                }
                if (CacheHelper.CachingStoreL2.TryGetValue(key, out value))
                {
                    // Add value to L1 cache
                    CacheHelper.CachingStoreL1.Add(key, value);
                    return true;
                }
            }

            // Value not found
            value = default(T);
            return false;
        }

        public static T Add<T>(bool sessionSpecific, string key, T value, CacheDependency dependencies = null)
        {
            if (sessionSpecific)
            {
                // Get session specific key
                key = CacheHelper.GetSessionSpecificCacheKey(key);
            }

            CacheHelper.CachingStoreL1.Add(key, value);
            CacheHelper.CachingStoreL2.Add(key, value);

            return value;
        }

        /// <summary>
        ///     Adds a value to the Cache.
        /// </summary>
        /// <param name="sessionSpecific">Bool indicating if the value has to be written to the session(/user) specific cache</param>
        /// <param name="key">The key for the Cache value.</param>
        /// <param name="value">The item to be added to the cache.</param>
        /// <param name="dependencies">
        ///     The file or cache key dependencies for the item. When any dependency changes, the object
        ///     becomes invalid and is removed from the cache.
        /// </param>
        /// <param name="absoluteExpiration">
        ///     The time at which the added object expires and is removed from the cache. If you are
        ///     using sliding expiration, the absoluteExpiration parameter must be NoAbsoluteExpiration.
        /// </param>
        /// <param name="slidingExpiration">
        ///     The interval between the time the added object was last accessed and the time at which
        ///     that object expires. If this value is the equivalent of 20 minutes, the object expires and is removed from the
        ///     cache 20 minutes after it is last accessed. If you are using absolute expiration, the slidingExpiration parameter
        ///     must be NoSlidingExpiration.
        /// </param>
        /// <param name="priority">
        ///     The relative cost of the object, as expressed by the CacheItemPriority enumeration. The cache
        ///     uses this value when it evicts objects; objects with a lower cost are removed from the cache before objects with a
        ///     higher cost.
        /// </param>
        /// <param name="onRemoveCallBack">
        ///     A delegate that, if provided, is called when an object is removed from the cache. You
        ///     can use this to notify applications when their objects are deleted from the cache.
        /// </param>
        /// <returns>An Object if the item was previously stored in the Cache; otherwise, a null reference.</returns>
        public static T Add<T>(bool sessionSpecific, string key, T value, CacheDependency dependencies, DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemPriority priority, CacheItemRemovedCallback onRemoveCallBack)
        {
            if (sessionSpecific)
            {
                // Get session specific key
                key = CacheHelper.GetSessionSpecificCacheKey(key);
            }

            if (absoluteExpiration != Cache.NoAbsoluteExpiration)
            {
                int cacheMinutes = (int)absoluteExpiration.Subtract(DateTime.Now).TotalMinutes;
                if (cacheMinutes > 0)
                {
                    CacheHelper.CachingStoreL1.AddAbsoluteExpire(key, value, cacheMinutes);
                    CacheHelper.CachingStoreL2.AddAbsoluteExpire(key, value, cacheMinutes);
                }
            }
            else if (slidingExpiration != Cache.NoSlidingExpiration)
            {
                CacheHelper.CachingStoreL1.AddSlidingExpire(key, value, (int)slidingExpiration.TotalMinutes);
                CacheHelper.CachingStoreL2.AddSlidingExpire(key, value, (int)slidingExpiration.TotalMinutes);
            }
            else
            {
                CacheHelper.CachingStoreL1.Add(key, value);
                CacheHelper.CachingStoreL2.Add(key, value);
            }

            return value;
        }

        /// <summary>
        ///     Add item to the cache with absolute expiration
        /// </summary>
        /// <param name="sessionSpecific">Bool indicating if the value has to be written to the session(/user) specific cache</param>
        /// <param name="key">The cache key used to reference the item.</param>
        /// <param name="value">The item to be added to the cache.</param>
        /// <param name="expireInXMinutes">The amount of minutes to keep it in cache (1440 minutes = 1 day)</param>
        /// <returns>
        ///     An Object if the item was previously stored in the Cache; otherwise, a null reference.
        /// </returns>
        public static T AddAbsoluteExpire<T>(bool sessionSpecific, string key, T value, int expireInXMinutes)
        {
            return CacheHelper.AddAbsoluteExpire(sessionSpecific, key, value, null, expireInXMinutes);
        }

        /// <summary>
        ///     Add item to the cache with absolute expiration
        /// </summary>
        /// <param name="sessionSpecific">Bool indicating if the value has to be written to the session(/user) specific cache</param>
        /// <param name="key">The cache key used to reference the item.</param>
        /// <param name="value">The item to be added to the cache.</param>
        /// <param name="dependencies">The dependencies.</param>
        /// <param name="expireInXMinutes">The amount of minutes to keep it in cache (1440 minutes = 1 day)</param>
        /// <returns>
        ///     An Object if the item was previously stored in the Cache; otherwise, a null reference.
        /// </returns>
        public static T AddAbsoluteExpire<T>(bool sessionSpecific, string key, T value, CacheDependency dependencies, int expireInXMinutes)
        {
            return CacheHelper.Add(sessionSpecific, key, value, dependencies, DateTime.Now.AddMinutes(expireInXMinutes), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }

        /// <summary>
        ///     Add item to the cache with sliding expiration.
        /// </summary>
        /// <param name="sessionSpecific">Bool indicating if the value has to be written to the session(/user) specific cache</param>
        /// <param name="key">The cache key used to reference the item.</param>
        /// <param name="value">The item to be added to the cache.</param>
        /// <param name="expireInXMinutes">The amount of minutes to slide</param>
        /// <returns>
        ///     An Object if the item was previously stored in the Cache; otherwise, a null reference.
        /// </returns>
        public static T AddSlidingExpire<T>(bool sessionSpecific, string key, T value, int expireInXMinutes)
        {
            return CacheHelper.AddSlidingExpire(sessionSpecific, key, value, null, expireInXMinutes);
        }

        /// <summary>
        ///     Add item to the cache with sliding expiration.
        /// </summary>
        /// <param name="sessionSpecific">Bool indicating if the value has to be written to the session(/user) specific cache</param>
        /// <param name="key">The cache key used to reference the item.</param>
        /// <param name="value">The item to be added to the cache.</param>
        /// <param name="dependencies">The dependencies.</param>
        /// <param name="expireInXMinutes">The amount of minutes to slide</param>
        /// <param name="cacheItemPriority">Default is normal</param>
        /// <returns>
        ///     An Object if the item was previously stored in the Cache; otherwise, a null reference.
        /// </returns>
        public static T AddSlidingExpire<T>(bool sessionSpecific, string key, T value, CacheDependency dependencies, int expireInXMinutes, CacheItemPriority cacheItemPriority = CacheItemPriority.Normal)
        {
            return CacheHelper.Add(sessionSpecific, key, value, dependencies, Cache.NoAbsoluteExpiration, new TimeSpan(0, expireInXMinutes, 0), cacheItemPriority, null);
        }

        /// <summary>
        ///     Removes the specified item form the application's System.Web.Caching.Cache object
        /// </summary>
        /// <param name="sessionSpecific">Removes the item from the session specific cache</param>
        /// <param name="key">Key to remove</param>
        public static void Remove(bool sessionSpecific, string key)
        {
            if (sessionSpecific)
            {
                // Get session specific key
                key = CacheHelper.GetSessionSpecificCacheKey(key);
            }
            
            CacheHelper.CachingStoreL1.RemoveValue(key);
            CacheHelper.CachingStoreL2.RemoveValue(key);
        }

        /// <summary>
        ///     Get All Cache Keys
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllCacheKeys()
        {
            return CacheHelper.CachingStoreL1.GetAllCacheKeys();
        }

        /// <summary>
        ///     Removes all Cached items of which the key contains the keyContains value
        /// </summary>
        /// <param name="sessionSpecific">True = Session specific only, False = Non-Session specific only, Null = All </param>
        /// <param name="keyContains"></param>
        public static void RemoveMultiple(bool? sessionSpecific, string keyContains)
        {
            // Get the session specific prefix
            string sessionPrefix = sessionSpecific.GetValueOrDefault(false) ? HttpContext.Current.Session.SessionID + "__" : string.Empty;

            // Get all keys
            List<string> allKeys = CacheHelper.GetAllCacheKeys();

            foreach (string key in allKeys)
            {
                bool remove = false;

                // Check if key contains the requested value
                if (key.Contains(keyContains))
                {
                    // Check if key meets conditions
                    if (!sessionSpecific.HasValue)
                    {
                        // Null = All
                        remove = true;
                    }
                    else if (sessionSpecific.Value && key.StartsWith(sessionPrefix))
                    {
                        // True = Session specific only
                        remove = true;
                    }
                    else if (!sessionSpecific.Value && !key.StartsWith(sessionPrefix))
                    {
                        // False = Non-Session specific only
                        remove = true;
                    }

                    // Remove if meets all criteria to be removed
                    if (remove)
                    {
                        CacheHelper.CachingStoreL1.RemoveValue(key);
                        CacheHelper.CachingStoreL2.RemoveValue(key);
                    }
                }
            }
        }

        public static void RemoveWildcard(string wildcard)
        {
            CachingStoreL1.RemoveWildcard(wildcard);
            CachingStoreL2.RemoveWildcard(wildcard);
        }

        /// <summary>
        ///     Removes all items from the Cache
        ///     Note - should not remove from cache in while loop since enumerator is only valid while collection remaind intact:
        ///     http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpref/html/frlrfsystemcollectionsidictionaryenumeratorclasstopic.asp
        /// </summary>
        public static void Clear()
        {
            if (HttpContext.Current != null)
            {
                CacheHelper.CachingStoreL1.Clear();

                CacheHelper.CachingStoreL1.Add(CacheHelper.LAST_CACHE_CLEARED, DateTime.Now);
            }
        }

        public static string GetLastCacheCleared()
        {
            DateTime lastCacheCleared;
            if (!CacheHelper.CachingStoreL1.TryGetValue(CacheHelper.LAST_CACHE_CLEARED, out lastCacheCleared))
            {
                return "Never";
            }

            return lastCacheCleared.ToString(CultureInfo.InvariantCulture);
        }
    }
}