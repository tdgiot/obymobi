﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Dionysos.IO;
using Dionysos.Web.Security;

namespace Dionysos.Web
{
	public static class InformationLoggerWeb
	{
		#region Fields

		/// <summary>
		/// The locker object.
		/// </summary>
		private static object locker = new object();

		#endregion

		#region Properties

		/// <summary>
		/// The relative logging directory.
		/// </summary>
		public static string RelativeLoggingDirectory
		{
			get
			{
                string path = string.Empty;
                try
                {
                    path = ConfigurationManager.GetString(DionysosConfigurationConstants.LogFileFolder);
                }
                catch 
                {
                    path = "~/App_Data/Log/";
                }
				return path;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Logs the information message using HttpContext.Current.
		/// </summary>
		/// <param name="fileName">The filename to store the message in (prefixed with timestamp).</param>
		public static void LogInformation(string fileName)
		{
			LogInformation(HttpContext.Current, fileName, null);
		}

		/// <summary>
		/// Logs the information message using HttpContext.Current.
		/// </summary>
		/// <param name="fileName">The filename to store the message in (prefixed with timestamp).</param>
		/// <param name="message">The additional message format.</param>
		/// <param name="args">Any additional items to format the message with.</param>
		public static void LogInformation(string fileName, string message, params object[] args)
		{
			LogInformation(HttpContext.Current, fileName, message, args);
		}

		/// <summary>
		/// Logs the information message.
		/// </summary>
		/// <param name="context">The HttpContext to use.</param>
		/// <param name="fileName">The filename to store the message in (prefixed with timestamp).</param>
		/// <param name="message">The additional message format.</param>
		/// <param name="args">Any additional items to format the message with.</param>
		public static void LogInformation(HttpContext context, string fileName, string message, params object[] args)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			// Build info text
			StringBuilder infoText = new StringBuilder();
			if (args == null || args.Length == 0)
			{
				infoText.AppendFormatLine("Information: {0}", message);
			}
			else
			{
				infoText.AppendFormatLine("Information: {0}", String.Format(message ?? String.Empty, args));
			}

			infoText.AppendLine();

			IManagableUser user = UserManager.CurrentUser;
			if (user != null)
			{
				infoText.AppendFormatLine("Current user: {0} ({1})", user.Username, StringUtil.CombineWithSpace(user.Firstname, user.MiddleName, user.Lastname));
				infoText.AppendLine();
			}

			try
			{
				infoText.AppendFormatLine("Information from: {0} ({1})", context.Request.Url, context.Request.RawUrl);
				infoText.AppendFormatLine("URL: {0}", new Uri(context.Request.Url, context.Request.RawUrl));
				infoText.AppendFormatLine("HTTP method: {0}", context.Request.HttpMethod);
				infoText.AppendFormatLine("Referer: {0}", context.Request.UrlReferrer);
				infoText.AppendFormatLine("IP address: {0}", context.Request.UserHostAddress);
				infoText.AppendFormatLine("Hostname: {0}", context.Request.UserHostName);
				infoText.AppendFormatLine("Languages: {0}", StringUtil.CombineWithSeperator(", ", context.Request.UserLanguages));
				infoText.AppendFormatLine("User agent: {0}", context.Request.UserAgent);
				infoText.AppendLine();
			}
			catch
			{
				// Request is not available in this context
			}

			// Check for Logs folder
			string path = context.Server.MapPath(RelativeLoggingDirectory);
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}

			// Write file
			fileName = FileUtil.SanitizeFilename(fileName);
			string filePath;
			lock (locker)
			{
				// Get unique filePath
				do
				{
					filePath = Path.Combine(path, DateTimeUtil.DateTimeToSimpleDateTimeTicksStamp(DateTime.Now) + "-" + fileName + ".log");
				}
				while (File.Exists(filePath));

				using (StreamWriter writer = File.CreateText(filePath))
				{
					writer.Write(infoText.ToString());
				}
			}
		}

		#endregion
	}
}
