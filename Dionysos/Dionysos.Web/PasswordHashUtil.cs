using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Dionysos.Web
{
	/// <summary>
	/// Static class used for password operations
	/// </summary>
	public static class PasswordHashUtil
	{

		#region Methods

		/// <summary>
		/// Generate a SHA1 password hash based on a combined password and salt (or password only, not recommended!)
		/// </summary>
		/// <param name="password">the password</param>        
		/// <param name="salt">the salt, if empty it's filled with a random value</param>        
		/// <returns>Hashed Password</returns>
		public static string GetPasswordHash(string password, ref string salt)
		{
			if (salt.Length <= 0)
			{
				salt = GetSalt(64);
			}
			password = password + salt;
			return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
		}

		/// <summary>
		/// Generate a SHA1 password hash based on a combined password and salt (or password only, not recommended!)
		/// </summary>
		/// <param name="password">the password</param>        
		/// <param name="salt">the salt, if empty it's filled with a random value</param>        
		/// <returns>Hashed Password</returns>
		public static string GetPasswordSHA1StandardHash(string password, ref string salt)
		{
			if (salt.Length <= 0)
				salt = GetSalt(64);

			password = password + salt;

			SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider();
			return Convert.ToBase64String(sha.ComputeHash(Encoding.UTF8.GetBytes(password))).Replace("-", "");
		}

		/// <summary>
		/// Get Random string to be used as salt for password
		/// </summary>
		/// <param name="size">Size</param>
		/// <returns>Random string of size</returns>
		public static string GetSalt(int size)
		{
			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			byte[] buff = new byte[size];
			rng.GetBytes(buff);

			return Convert.ToBase64String(buff);
		}

		/// <summary>
		/// Generate a SHA1 password hash based on a combined  password and salt (or password only, not recommended!)
		/// </summary>
		/// <param name="passwordAndSaltCombined">The password and salt.</param>
		/// <returns>
		/// Hashed Password
		/// </returns>
		public static string GetPasswordHash(string passwordAndSaltCombined)
		{
			return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(passwordAndSaltCombined, "SHA1");
		}

		/// <summary>
		/// Generate a password of a specified length
		/// </summary>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string GetRandomPasswordUsingGUID(int length)
		{
			// Get the GUID
			string guidResult = System.Guid.NewGuid().ToString();

			// Remove the hyphens
			guidResult = guidResult.Replace("-", string.Empty);

			// Make sure length is valid
			if (length <= 0 || length > guidResult.Length)
				throw new ArgumentException("Length must be between 1 and " + guidResult.Length);

			// Return the first length bytes
			return guidResult.Substring(0, length);
		}

		/// <summary>
		/// Gets the password.
		/// </summary>
		/// <param name="length">The length.</param>
		/// <param name="allowedChars">The allowed chars.</param>
		/// <returns>
		/// The password.
		/// </returns>
		/// <exception cref="System.ArgumentOutOfRangeException">length;The length cannot be less than zero.</exception>
		/// <exception cref="System.ArgumentNullException">allowedChars</exception>
		/// <exception cref="System.ArgumentException">allowedChars cannot be empty.;allowedChars</exception>
		public static string GetPassword(int length, string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
		{
			if (length < 0) throw new ArgumentOutOfRangeException("length", length, "The length cannot be less than zero.");
			if (allowedChars == null) throw new ArgumentNullException("allowedChars");
			if (String.IsNullOrEmpty(allowedChars)) throw new ArgumentException("allowedChars cannot be empty.", "allowedChars");

			const int byteSize = 0x100;
			char[] allowedCharSet = allowedChars.ToCharArray();
			if (byteSize < allowedCharSet.Length) throw new ArgumentException(String.Format("allowedChars may contain no more than {0} characters.", byteSize));

			RandomNumberGenerator rng = new RNGCryptoServiceProvider();
			StringBuilder result = new StringBuilder();
			byte[] buf = new byte[128];
			while (result.Length < length)
			{
				rng.GetBytes(buf);
				for (var i = 0; i < buf.Length && result.Length < length; ++i)
				{
					// Divide the byte into allowedCharSet-sized groups. If the
					// random value falls into the last group and the last group is
					// too small to choose from the entire allowedCharSet, ignore
					// the value in order to avoid biasing the result.
					int outOfRangeStart = byteSize - (byteSize % allowedCharSet.Length);
					if (outOfRangeStart <= buf[i]) continue;
					result.Append(allowedCharSet[buf[i] % allowedCharSet.Length]);
				}
			}

			return result.ToString();
		}

		#endregion
	}
}