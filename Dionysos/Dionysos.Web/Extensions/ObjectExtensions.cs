﻿using System;
using System.Reflection;

namespace Dionysos.Web.Extensions
{
    public static class ObjectExtensions
    {
        public static bool TryGetPropertyValue(this Object obj, string propertyName, out object value)
        {
            try
            {
                Type type = obj?.GetType();
                PropertyInfo property = type?.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);

                value = property?.GetValue(obj, null);

                return value != null;
            }
            catch
            {
                value = null;

                return false;
            }
        }
    }
}
