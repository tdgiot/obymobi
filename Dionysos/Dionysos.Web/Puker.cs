﻿using System.Web;
using System.IO;

namespace Dionysos.Web
{
	/// <summary>
	/// Class used to get the Raw Http Request
	/// </summary>
	public static class Puker
	{
		/// <summary>
		/// Get the full HttpRequest as a String
		/// </summary>
		/// <returns>Request as a string</returns>
		public static string BarfRawHttpRequest()
		{
		    if (HttpContext.Current == null)
		        return "No context found";

			HttpRequest request = HttpContext.Current.Request;
			var requestStream = new MemoryStream();
			TextWriter writer = new StreamWriter(requestStream);
			writer.Write(request.HttpMethod);
			writer.Write(" ");
			writer.Write(request.Path);
			writer.Write(request.Url.Query);
			writer.Write(" ");
			writer.WriteLine(request.ServerVariables["SERVER_PROTOCOL"]);
			writer.WriteLine(request.ServerVariables["ALL_RAW"]);

			// Variables
			foreach (var name in request.ServerVariables.AllKeys)
			{
				writer.WriteLine(name + ": " + request.ServerVariables[name]);
			}

			writer.Flush();

		    if (request.ContentLength > 0 && request.ContentLength < 1024)
		    {
		        long inputstreamStartPosition = request.InputStream.Position;

		        var buffer = new byte[request.ContentLength];
		        request.InputStream.Read(buffer, 0, request.ContentLength);
		        requestStream.Write(buffer, 0, request.ContentLength);

		        request.InputStream.Position = inputstreamStartPosition;
		    }

		    requestStream.Position = 0;
			var sr = new StreamReader(requestStream);			
			return sr.ReadToEnd();			
		}
	}
}
