﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Web
{
	/// <summary>
	/// Type of a page
	/// </summary>
	public enum PageType
	{
		/// <summary>
		/// Standard, not specific type page
		/// </summary>
		Standard,
		/// <summary>
		/// Page displaying a single entity
		/// </summary>
		Entity,
		/// <summary>
		/// Page displaying a collection of entities
		/// </summary>
		EntityCollection
	}
}
