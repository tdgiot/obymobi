﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Reflection;

namespace Dionysos.Web
{
	/// <summary>
	/// Utility class for extracting embedded scripts.
	/// </summary>
	/// <remarks>
	/// Uses a naming convention. All scripts should be placed 
	/// in the Resources\Scripts folder. The scriptName is just 
	/// the filename of the script.
	/// </remarks>
	public static class EmbeddedResourceUtil
	{
		public static string UnpackScript(string resourceName, string scriptLanguage)
		{
			return "<script type=\"text/javascript\">"
			  + Environment.NewLine
			  + UnpackEmbeddedResourceToString(resourceName)
			  + Environment.NewLine
			  + "</script>";
		}

		// Unpacks the embedded resource to string.
		public static string UnpackEmbeddedResourceToString(string resourceName)
		{
			Assembly executingAssembly = Assembly.GetExecutingAssembly();
			Stream resourceStream = executingAssembly.GetManifestResourceStream(resourceName);
			using (StreamReader reader = new StreamReader(resourceStream, Encoding.ASCII))
			{
				return reader.ReadToEnd();
			}
		}
	}


}
