using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Web;
using Dionysos.Web.HttpModules;

namespace Dionysos.Web
{
    /// <summary>
    /// Class which represents a query string
    /// </summary>
    public class RequestVariableHelper
    {
        #region Fields

        #endregion

        #region Constructors
        #endregion

        #region Methods

        /// <summary>
        /// Checks whether a querystring parameter exists in the querystring instance
        /// from the specified HttpRequest instance
        /// </summary>
        /// <param name="name">The name of the querystring parameter</param>
        /// <returns>True if the querystring parameter exists, False if not</returns>
        public static bool HasParameter(string name)
        {
            bool hasParameter = true;

            if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Argument 'name' is empty
            }
            else
            {                
				if (HttpContext.Current.Request[name] == null)
                {
                    hasParameter = false;
                }
            }

            return hasParameter;
        }

        /// <summary>
        /// Retrieves a querystring parameter from the querystring instance 
        /// from the specified HttpRequest instance
        /// </summary>
        /// <param name="name">The name of the querystring parameter</param>
        /// <returns>An object instance containing the value of the querystring parameter</returns>
        public static string GetParameter(string name)
        {
            string value = string.Empty;

            if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Argument 'name' is empty
            }
            else
            {
				if (HttpContext.Current.Request[name] == null)
				{
					throw new Dionysos.EmptyException(string.Format("The value of Request Variable '{0}' is empty!", name));
				}
				else
				{
					value = HttpContext.Current.Request[name];
				}
            }

            return value;
        }

        /// <summary>
        /// Gets a System.Double value of a querystring parameter using the specified
        /// HttpRequest instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Double instance containing the current value</returns>
        public static double GetDouble(string name)
        {
            return double.Parse(GetParameter(name));
        }

        /// <summary>
        /// Gets a System.Int32 value of a querystring parameter using the specified
        /// HttpRequest instance and the specified parameter name
        /// </summary>
        /// <returns>An System.Int32 instance containing the current value</returns>
        public static int GetInt(string name)
        {
			return int.Parse(GetParameter(name));
        }

        /// <summary>
        /// Gets a System.String value of a querystring parameter using the specified
        /// HttpRequest instance and the specified parameter name
        /// </summary>
        /// <returns>An System.String instance containing the current value</returns>
        public static string GetString(string name)
        {
			return GetParameter(name);
        }

        #endregion


        #region Properties

      

        #endregion
    }
}