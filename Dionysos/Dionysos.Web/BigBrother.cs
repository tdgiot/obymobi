﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Dionysos.Web
{
    /// <summary>
    /// Helper class for storing and retrieving cookie data for anonymous website visitors
    /// The benefit of using this class is that it will only write to 1 cookie and do this encrypted.
    /// </summary>
    public class BigBrother
    {

        #region Fields

        const string passPhrase = "BigBrotherpr@se";        // can be any string
        const string saltValue = "s@1tBrother";        // can be any string
        const string hashAlgorithm = "SHA1";             // can be "MD5"
        const int passwordIterations = 2;                  // can be any number
        const string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
        const int keySize = 256;                // can be 192 or 128

        #endregion

        #region Methods

        /// <summary>
        /// Determine if a certain value is available in Big Brother
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns></returns>
        public static bool HasValue(string key)
        {
            bool toReturn = false;
            //key = CrypographyUtil.Encrypt(key, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);

            if (Cookie.Values[key] != null)
                toReturn = true;

            return toReturn;
        }

        /// <summary>
        /// Add a value for a certain key. Any existing data for that key will be deleted.
        /// </summary>
        /// <param name="key">Key name</param>
        /// <param name="value">Value</param>
        /// <returns></returns>
        public static void SetValue(string key, string value)
        {
            // Encrypt value
            if (value.Length > 0)
            {
                //key = CrypographyUtil.Encrypt(key, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);				
                //value = CrypographyUtil.Encrypt(value, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);				
            }

            if (Cookie.Values[key] != null)
                Cookie.Values[key] = value;
            else
                Cookie.Values.Add(key, value);

            SetCookieToResponse();
        }

        /// <summary>
        /// Get the value from the Big Brother cookie for a certain key
        /// </summary>
        /// <param name="key">Key to fetch value for</param>
        /// <returns></returns>
        public static object GetValue(string key)
        {
            //string encryptedKey = CrypographyUtil.Encrypt(key, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
            if (Cookie[key] != null)
            {
                //string toReturn = CrypographyUtil.
                //return CrypographyUtil.Decrypt(Cookie[encryptedKey], passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
                return Cookie[key];
            }
            else
                return null;
        }

        /// <summary>
        /// Get value as a String
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetString(string key)
        {
            string toReturn = string.Empty;

            if (GetValue(key) != null)
                toReturn = GetValue(key).ToString();

            return toReturn;
        }

        /// <summary>
        /// Get values as a Int
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int? GetInt(string key)
        {
            int? toReturn = null;
            if (GetValue(key) != null)
            {
                int temp;
                if (int.TryParse(GetString(key), out temp))
                    toReturn = temp;
            }
            return toReturn;
        }

        /// <summary>
        /// Get values as a bool
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool? GetBool(string key)
        {
            bool? toReturn = null;
            if (GetValue(key) != null)
            {
                bool temp;
                if (bool.TryParse(GetString(key), out temp))
                    toReturn = temp;
            }
            return toReturn;
        }

        /// <summary>
        /// Remove a value from the Big Brother Cookie
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveValue(string key)
        {
            if (Cookie[key] != null)
                Cookie.Values.Remove(key);

            SetCookieToResponse();
        }

        /// <summary>
        /// Append the cookie to the Request.Response
        /// </summary>
        public static void SetCookieToResponse()
        {
            HttpContext.Current.Response.Cookies.Add(Cookie);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Get the Cookie holding all BigBrother's data
        /// </summary>
        public static HttpCookie Cookie
        {
            get
            {
                if (HttpContext.Current.Items["BigBrother.Cookie"] == null)
                {
                    if (HttpContext.Current.Request.Cookies["bbcbbc"] != null)
                    {
                        HttpContext.Current.Items["BigBrother.Cookie"] = HttpContext.Current.Request.Cookies["bbcbbc"];
                    }
                    else
                    {
                        HttpCookie cookie = new HttpCookie("bbcbbc");
                        cookie.Name = "bbcbbc";
                        cookie.HttpOnly = true;
                        HttpContext.Current.Items["BigBrother.Cookie"] = cookie;
                    }
                }

                ((HttpCookie)HttpContext.Current.Items["BigBrother.Cookie"]).Expires = DateTime.Now.AddMonths(12);
                return (HttpCookie)HttpContext.Current.Items["BigBrother.Cookie"];
            }
        }

        #endregion

    }
}
