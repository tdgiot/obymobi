using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Dionysos.Web.HttpModules;

namespace Dionysos.Web
{
	/// <summary>
	/// Represents a query string.
	/// </summary>
	public class QueryStringHelper
	{
		#region Fields

		/// <summary>
		/// The query string elements.
		/// </summary>
		private readonly QueryStringElementCollection queryStringElements = new QueryStringElementCollection();

		#endregion

		#region Properties

		/// <summary>
		/// Gets the query string as a string.
		/// </summary>
		public string Value
		{
			get
			{
				return this.queryStringElements.ToString();
			}
		}

		/// <summary>
		/// Gets the query string as a NameValueCollection.
		/// </summary>
		public NameValueCollection QueryString
		{
			get
			{
				return HttpUtility.ParseQueryString(this.Value);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="QueryStringHelper"/> class.
		/// </summary>
		public QueryStringHelper()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="QueryStringHelper"/> class.
		/// </summary>
		/// <param name="queryString">The query string.</param>
		public QueryStringHelper(string queryString)
		{
			if (queryString == null) throw new ArgumentNullException("queryString");

			this.queryStringElements = new QueryStringElementCollection(queryString);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="QueryStringHelper"/> class.
		/// </summary>
		/// <param name="queryString">The query string.</param>
		public QueryStringHelper(NameValueCollection queryString)
		{
			if (queryString == null) throw new ArgumentNullException("queryString");

			foreach (string key in queryString.AllKeys)
			{
				if (String.IsNullOrEmpty(key)) continue;

				this.queryStringElements.Add(key, queryString[key]);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds the query string item.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		public void AddItem(string name, object value)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (String.IsNullOrEmpty(name)) throw new ArgumentException("Value cannot be empty.", "name");

			this.queryStringElements.Add(name, value);
		}

		/// <summary>
		/// Adds the query string item.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="values">The values.</param>
		public void AddItem(string name, IEnumerable<int> values)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (String.IsNullOrEmpty(name)) throw new ArgumentException("Value cannot be empty.", "name");
			if (values == null) throw new ArgumentNullException("values");

			string value = null;
			foreach (int element in values)
			{
				value = StringUtil.CombineWithComma(value, element);
			}

			this.queryStringElements.Add(name, value);
		}

		/// <summary>
		/// Removes the query string item.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>
		///   <c>true</c> if item is successfully removed; otherwise, false.
		/// </returns>
		public bool RemoveItem(string name)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (String.IsNullOrEmpty(name)) throw new ArgumentException("Value cannot be empty.", "name");

			return this.queryStringElements.Remove(name);
		}

		/// <summary>
		/// This function returns a URL with the QueryString merged with the existing querystring of the request. Items in the QuerySTring of the request will get replaced by items in the QueryStringHelper.Items
		/// </summary>
		/// <returns>
		/// Merged Url to be used for Redirect
		/// </returns>
		/// <remarks>
		/// This function does not merge with the RawUrl, but uses the Path (eg. the executing page filename).
		/// </remarks>
		public string MergeQuerystringWithRawUrl()
		{
			return GlobalizationHelper.UseGlobalization ? this.MergeQuerystringWithUrl(GenericRewriteModule.OriginalRequestPath) : this.MergeQuerystringWithUrl(HttpContext.Current.Request.Path);
		}

		/// <summary>
		/// This function returns a URL with the QueryString merged with the existing querystring of the url. Items in the QuerySTring of the request will get replaced by items in the QueryStringHelper.Items
		/// </summary>
		/// <param name="url">Url to merge with</param>
		/// <returns>
		/// Merged Url to be used for Redirect
		/// </returns>
		public string MergeQuerystringWithUrl(string url)
		{
			return this.MergeQuerystringWithUrl(url, true);
		}

		/// <summary>
		/// This function returns a URL with the QueryString merged with the existing querystring of the url. Items in the QuerySTring of the request will get replaced by items in the QueryStringHelper.Items
		/// </summary>
		/// <param name="url">Url to merge with</param>
		/// <param name="includeCurrentQueryString">Indicate if the existing querystring (HttpContext.Current.Request.QueryString) should be included in the new url</param>
		/// <returns>
		/// Merged Url to be used for Redirect
		/// </returns>
		public string MergeQuerystringWithUrl(string url, bool includeCurrentQueryString)
		{
			return this.MergeQuerystringWithUrl(url, includeCurrentQueryString, false);
		}

		/// <summary>
		/// This function returns a URL with the QueryString merged with the existing querystring of the url. Items in the QuerySTring of the request will get replaced by items in the QueryStringHelper.Items
		/// </summary>
		/// <param name="url">Url to merge with</param>
		/// <param name="includeCurrentQueryString">Indicate if the existing querystring (HttpContext.Current.Request.QueryString) should be included in the new url</param>
		/// <param name="preventGlobalizationLogic">If true there will be no culture (ie nl-NL) be added to the url if missing.</param>
		/// <returns>
		/// Merged Url to be used for Redirect
		/// </returns>
		public string MergeQuerystringWithUrl(string url, bool includeCurrentQueryString, bool preventGlobalizationLogic)
		{
			if (includeCurrentQueryString)
			{
				NameValueCollection queryString = HttpContext.Current.Request.QueryString;
				foreach (string key in queryString.AllKeys)
				{
					if (!String.IsNullOrEmpty(key) &&
						!this.queryStringElements.Contains(key))
					{
						this.queryStringElements.Add(key, queryString[key]);
					}
				}
			}

			return url + this.Value;
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// Indicates whether the key exists in the QueryString.
		/// </summary>
		/// <param name="key">The key of the QueryString value.</param>
		/// <returns>
		/// true if the value exists in the QueryString; otherwise false.
		/// </returns>
		public static bool HasValue(string key)
		{
			return QueryStringHelper.HasValue<string>(key, null);
		}

		/// <summary>
		/// Indicates whether the key exists in the QueryString and can be converted to the specified type.
		/// </summary>
		/// <typeparam name="T">The type of the value to check.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <returns>
		/// true if the value exists in the QueryString and can be converted to the specified type; otherwise false.
		/// </returns>
		public static bool HasValue<T>(string key)
		{
			return QueryStringHelper.HasValue<T>(key, null);
		}

		/// <summary>
		/// Indicates whether the key exists in the QueryString and can be converted to the specified type.
		/// </summary>
		/// <typeparam name="T">The type of the value to check.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>
		/// true if the value exists in the QueryString and can be converted to the specified type; otherwise false.
		/// </returns>
		public static bool HasValue<T>(string key, IFormatProvider provider)
		{
			T value;
			return QueryStringHelper.TryGetValue<T>(key, out value, provider);
		}

		/// <summary>
		/// Indicates whether the key exists in the QueryString and is equal to the specified value.
		/// </summary>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">The value to compare with.</param>
		/// <param name="comparisonType">Type of the comparison.</param>
		/// <returns>
		/// true if the value exists in the QueryString, can be converted to the specified type and is equals to the specified value; otherwise false.
		/// </returns>
		public static bool HasValueAndEquals(string key, string value, StringComparison comparisonType)
		{
			return QueryStringHelper.HasValueAndEquals(key, value, null, comparisonType);
		}

		/// <summary>
		/// Indicates whether the key exists in the QueryString and is equal to the specified value.
		/// </summary>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">The value to compare with.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <param name="comparisonType">Type of the comparison.</param>
		/// <returns>
		/// true if the value exists in the QueryString, can be converted to the specified type and is equals to the specified value; otherwise false.
		/// </returns>
		public static bool HasValueAndEquals(string key, string value, IFormatProvider provider, StringComparison comparisonType)
		{
			string actual;
			return value != null && QueryStringHelper.TryGetValue(key, out actual, provider) && value.Equals(actual, comparisonType);
		}

		/// <summary>
		/// Indicates whether the key exists in the QueryString, can be converted to the specified type and is equal to the specified value.
		/// </summary>
		/// <typeparam name="T">The type of the value to check.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">The value to compare with.</param>
		/// <returns>
		/// true if the value exists in the QueryString, can be converted to the specified type and is equals to the specified value; otherwise false.
		/// </returns>
		public static bool HasValueAndEquals<T>(string key, T value)
		{
			return QueryStringHelper.HasValueAndEquals<T>(key, value, null);
		}

		/// <summary>
		/// Indicates whether the key exists in the QueryString, can be converted to the specified type and is equal to the specified value.
		/// </summary>
		/// <typeparam name="T">The type of the value to check.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">The value to compare with.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>
		/// true if the value exists in the QueryString, can be converted to the specified type and is equals to the specified value; otherwise false.
		/// </returns>
		public static bool HasValueAndEquals<T>(string key, T value, IFormatProvider provider)
		{
			T actual;
			return value != null && QueryStringHelper.TryGetValue<T>(key, out actual, provider) && value.Equals(actual);
		}

		/// <summary>
		/// Gets the value from the QueryString using the specified key.
		/// </summary>
		/// <param name="key">The key of the QueryString value.</param>
		/// <returns>
		/// Returns the value, if found; otherwise returns null.
		/// </returns>
		public static string GetValue(string key)
		{
			return QueryStringHelper.GetValue(key, null);
		}

		/// <summary>
		/// Gets the value from the QueryString using the specified key.
		/// </summary>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>
		/// Returns the value, if found; otherwise returns null.
		/// </returns>
		public static string GetValue(string key, IFormatProvider provider)
		{
			string value;
			if (QueryStringHelper.TryGetValue(key, out value, provider))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Gets the value from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <returns>
		/// Returns the value, if found and type matches; otherwise returns null.
		/// </returns>
		public static T? GetValue<T>(string key)
			where T : struct
		{
			return QueryStringHelper.GetValue<T>(key, null);
		}

		/// <summary>
		/// Gets the value from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>
		/// Returns the value, if found and type matches; otherwise returns null.
		/// </returns>
		public static T? GetValue<T>(string key, IFormatProvider provider)
			where T : struct
		{
			T value;
			if (QueryStringHelper.TryGetValue(key, out value, provider))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Tries to get the value from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
		/// <returns>
		/// true if the key and corresponding value exists in the QueryString; otherwise false.
		/// </returns>
		public static bool TryGetValue<T>(string key, out T value)
		{
			return QueryStringHelper.TryGetValue<T>(key, out value, null);
		}

		/// <summary>
		/// Tries to get the value from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>
		/// true if the key and corresponding value exists in the QueryString; otherwise false.
		/// </returns>
		public static bool TryGetValue<T>(string key, out T value, IFormatProvider provider)
		{
			string queryStringValue = HttpContext.Current.NullSafe(c => c.Request).NullSafe(r => r.QueryString).NullSafe(q => q[key]);

			if (queryStringValue != null)
			{
				// Value is found, try to change the type
				try
				{
					value = (T)Convert.ChangeType(queryStringValue, typeof(T), provider);
					return true;
				}
				catch { } // Type could not be changed
			}

			// Value is not found, return default
			value = default(T);
			return false;
		}

		/// <summary>
		/// Gets the comma seperated values from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <returns>
		/// Returns the value, if found and type matches; otherwise returns an empty array.
		/// </returns>
		public static T[] GetValues<T>(string key)
		{
			return QueryStringHelper.GetValues<T>(key, null, ',');
		}

		/// <summary>
		/// Gets the seperated values (using the specified seperator) from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="seperator">The seperator.</param>
		/// <returns>
		/// Returns the value, if found and type matches; otherwise returns an empty array.
		/// </returns>
		public static T[] GetValues<T>(string key, char seperator)
		{
			return QueryStringHelper.GetValues<T>(key, null, seperator);
		}

		/// <summary>
		/// Gets the comma seperated values from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>
		/// Returns the value, if found and type matches; otherwise returns an empty array.
		/// </returns>
		public static T[] GetValues<T>(string key, IFormatProvider provider)
		{
			return QueryStringHelper.GetValues<T>(key, provider, ',');
		}

		/// <summary>
		/// Gets the seperated values (using the specified seperator) from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <param name="seperator">The seperator.</param>
		/// <returns>
		/// Returns the value, if found and type matches; otherwise returns an empty array.
		/// </returns>
		public static T[] GetValues<T>(string key, IFormatProvider provider, char seperator)
		{
			T[] value;
			if (QueryStringHelper.TryGetValues(key, out value, provider, seperator))
			{
				return value;
			}
			else
			{
				return new T[0];
			}
		}

		/// <summary>
		/// Tries to get the comma seperated values from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
		/// <returns>
		/// true if the key and corresponding value exists in the QueryString; otherwise false.
		/// </returns>
		public static bool TryGetValues<T>(string key, out T[] value)
		{
			return QueryStringHelper.TryGetValues<T>(key, out value, null, ',');
		}

		/// <summary>
		/// Tries to get the seperated values (using the specified seperator) from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
		/// <param name="seperator">The seperator.</param>
		/// <returns>
		/// true if the key and corresponding value exists in the QueryString; otherwise false.
		/// </returns>
		public static bool TryGetValues<T>(string key, out T[] value, char seperator)
		{
			return QueryStringHelper.TryGetValues<T>(key, out value, null, seperator);
		}

		/// <summary>
		/// Tries to get the comma seperated values from the QueryString using the specified key.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>
		/// true if the key and corresponding value exists in the QueryString; otherwise false.
		/// </returns>
		public static bool TryGetValues<T>(string key, out T[] value, IFormatProvider provider)
		{
			return QueryStringHelper.TryGetValues<T>(key, out value, provider, ',');
		}

		/// <summary>
		/// Tries to get the seperated values (using the specified seperator) from the QueryString using the specified key and seperator.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="key">The key of the QueryString value.</param>
		/// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <param name="seperator">The seperator.</param>
		/// <returns>
		/// true if the key and corresponding value exists in the QueryString; otherwise false.
		/// </returns>
		public static bool TryGetValues<T>(string key, out T[] value, IFormatProvider provider, char seperator)
		{
			string queryStringValue = HttpContext.Current.NullSafe(c => c.Request).NullSafe(r => r.QueryString).NullSafe(q => q[key]);

			if (queryStringValue != null)
			{
				// Value is found, try to change the type
				try
				{
					value = queryStringValue.Split(seperator).Select(qsv => (T)Convert.ChangeType(qsv, typeof(T), provider)).ToArray();
					return true;
				}
				catch { } // Type could not be changed
			}

			// Value is not found, return default
			value = default(T[]);
			return false;
		}

		#endregion

		#region Legacy Static Methods

		public static bool GetBool(string name)
		{
			return QueryStringHelper.GetValue<bool>(name).GetValueOrDefault();
		}

		public static bool GetBool(string name, bool defaultValue)
		{
			return QueryStringHelper.GetValue<bool>(name) ?? defaultValue;
		}

		public static byte GetByte(string name)
		{
			return QueryStringHelper.GetValue<byte>(name).GetValueOrDefault();
		}

		public static char GetChar(string name)
		{
			return QueryStringHelper.GetValue<char>(name).GetValueOrDefault();
		}

		public static bool GetDateTime(string name, out DateTime? value)
		{
			if (!QueryStringHelper.TryGetValue(name, out value))
			{
				value = null;
				return false;
			}

			return true;
		}

		public static bool GetDateTime(string name, out DateTime value)
		{
			return QueryStringHelper.TryGetValue(name, out value);
		}

		public static decimal GetDecimal(string name)
		{
			return QueryStringHelper.GetValue<decimal>(name).GetValueOrDefault();
		}

		public static double GetDouble(string name)
		{
			return QueryStringHelper.GetValue<double>(name).GetValueOrDefault();
		}

		public static float GetFloat(string name)
		{
			return QueryStringHelper.GetValue<float>(name).GetValueOrDefault();
		}

		public static bool GetInt(string name, out int value)
		{
			if (!QueryStringHelper.TryGetValue(name, out value))
			{
				value = -1;
				return false;
			}

			return true;
		}

		public static bool GetInt(string name, out int? value)
		{
			if (!QueryStringHelper.TryGetValue(name, out value))
			{
				value = null;
				return false;
			}

			return true;
		}

		public static int GetInt(string name)
		{
			return QueryStringHelper.GetValue<int>(name).GetValueOrDefault();
		}

		public static long GetLong(string name)
		{
			return QueryStringHelper.GetValue<long>(name).GetValueOrDefault();
		}

		public static string GetString(string name)
		{
			return QueryStringHelper.GetValue(name);
		}

		public static string GetString(string name, bool returnEmptyIfNotSet)
		{
			return QueryStringHelper.GetValue(name) ?? (returnEmptyIfNotSet ? String.Empty : null);
		}

		public static bool GetString(string name, out string value)
		{
			if (!QueryStringHelper.TryGetValue(name, out value))
			{
				value = String.Empty;
				return false;
			}

			return true;
		}

		#endregion

		#region Obsolete Methods

		[Obsolete("This method is obsolete; use GetString or GetValue instead.")]
		public static string GetParameter(string name)
		{
			string value = String.Empty;

			if (name != null &&
				!QueryStringHelper.TryGetValue<string>(name, out value))
			{
				throw new EmptyException("The value of queryString parameter '{0}' is empty!", name);
			}

			return value;
		}

		[Obsolete("This method is obsolete; use GetBool or GetValue<bool> instead.")]
		public static bool GetParameterAsBoolean(string name)
		{
			return QueryStringHelper.GetValue<bool>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetBool or GetValue<bool> instead.")]
		public static bool GetParameterAsBoolean(string name, bool defaultValue)
		{
			return QueryStringHelper.GetValue<bool>(name) ?? defaultValue;
		}

		[Obsolete("This method is obsolete; use GetByte or GetValue<byte> instead.")]
		public static byte GetParameterAsByte(string name)
		{
			return QueryStringHelper.GetValue<byte>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetChar or GetValue<char> instead.")]
		public static char GetParameterAsChar(string name)
		{
			return QueryStringHelper.GetValue<char>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetDateTime or GetValue<DateTime> instead.")]
		public static DateTime GetParameterAsDateTime(string name)
		{
			return QueryStringHelper.GetValue<DateTime>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetDecimal or GetValue<decimal> instead.")]
		public static decimal GetParameterAsDecimal(string name)
		{
			return QueryStringHelper.GetValue<decimal>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetDouble or GetValue<double> instead.")]
		public static double GetParameterAsDouble(string name)
		{
			return QueryStringHelper.GetValue<double>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetValue and EnumUtil.ToEnum instead.")]
		public static int GetParameterAsEnumValue(string name, Type enumType)
		{
			string value;
			if (QueryStringHelper.TryGetValue(name, out value))
			{
				return (int)Enum.Parse(enumType, value, true);
			}

			return default(int);
		}

		[Obsolete("This method is obsolete; use GetFloat or GetValue<float> instead.")]
		public static float GetParameterAsFloat(string name)
		{
			return QueryStringHelper.GetValue<float>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetInt or GetValue<int> instead.")]
		public static int GetParameterAsInt(string name)
		{
			return QueryStringHelper.GetValue<int>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetInt or GetValue<int> instead.")]
		public static int GetParameterAsInt(string name, int defaultValue)
		{
			return QueryStringHelper.GetValue<int>(name) ?? defaultValue;
		}

		[Obsolete("This method is obsolete; use GetValues<int> instead.")]
		public static List<int> GetParameterAsIntList(string name)
		{
			return QueryStringHelper.GetValues<int>(name).ToList();
		}

		[Obsolete("This method is obsolete; use GetValue<long> instead.")]
		public static long GetParameterAsLong(string name)
		{
			return QueryStringHelper.GetValue<long>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetValue<short> instead.")]
		public static short GetParameterAsShort(string name)
		{
			return QueryStringHelper.GetValue<short>(name).GetValueOrDefault();
		}

		[Obsolete("This method is obsolete; use GetString or GetValue instead.")]
		public static string GetParameterAsString(string name)
		{
			return QueryStringHelper.GetValue(name);
		}

		[Obsolete("This method is obsolete; use GetString or GetValue instead.")]
		public static string GetParameterAsString(string name, string defaultValue)
		{
			string value;
			if (QueryStringHelper.TryGetValue(name, out value))
			{
				return value;
			}

			return defaultValue;
		}

		[Obsolete("This method is obsolete; use GetValue<TimeStamp> instead (TimeStamp should be converted to a struct first).")]
		public static TimeStamp GetParameterAsTimeStamp(string name)
		{
			string value;
			if (QueryStringHelper.TryGetValue(name, out value))
			{
				return TimeStamp.Parse(value);
			}

			return default(TimeStamp);
		}

		[Obsolete("This method is obsolete; use HasValue instead.")]
		public static bool HasParameter(string name)
		{
			return QueryStringHelper.HasValue(name);
		}

		[Obsolete("This method is obsolete; use HasValueAndEquals instead.")]
		public static bool HasParameterAndEquals(string name, string value)
		{
			return QueryStringHelper.HasValueAndEquals(name, value, StringComparison.OrdinalIgnoreCase);
		}

		#endregion
	}
}