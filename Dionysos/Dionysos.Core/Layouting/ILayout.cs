﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Layouting
{
	/// <summary>
	/// Represents a layout.
	/// </summary>
	public interface ILayout
	{
		/// <summary>
		/// Gets or sets the system name.
		/// </summary>
		/// <value>
		/// The system name.
		/// </value>
		string SystemName { get; set; }

		/// <summary>
		/// Gets or sets the HTML markup.
		/// </summary>
		/// <value>
		/// The HTML markup.
		/// </value>
		string HtmlMarkup { get; set; }

		/// <summary>
		/// Gets the layout elements.
		/// </summary>
		LayoutElementList LayoutElements { get; }
	}
}
