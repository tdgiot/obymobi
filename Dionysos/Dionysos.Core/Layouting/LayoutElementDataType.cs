﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Layouting
{
	/// <summary>
	/// Represents a layout element data type.
	/// </summary>
	public enum LayoutElementDataType
	{
		/// <summary>
		/// Layout element.
		/// </summary>
		Layout,
		/// <summary>
		/// Single line element.
		/// </summary>
		SingleLine,
		/// <summary>
		/// Single line formatted element.
		/// </summary>
		SingleLineFormatted,
		/// <summary>
		/// Hidden single line element.
		/// </summary>
		SingleLineHidden,
		/// <summary>
		/// Single line element with an URL.
		/// </summary>
		SingleLineUrl,
		/// <summary>
		/// Single line element with a select option.
		/// </summary>
		SingleLineSelect,
		/// <summary>
		/// Multi line element.
		/// </summary>
		MultiLine,
		/// <summary>
		/// Hidden multi line element.
		/// </summary>
		MultiLineHidden,
		/// <summary>
		/// Image element.
		/// </summary>
		Image,
		/// <summary>
		/// YouTube element.
		/// </summary>
		Youtube,
		/// <summary>
		/// YouTube iframe element.
		/// </summary>
		YoutubeIframe,
		/// <summary>
		/// Vimeo element.
		/// </summary>
		Vimeo,
		/// <summary>
		/// SlideShare element.
		/// </summary>
		SlideShare,
		/// <summary>
		/// Scribd element.
		/// </summary>
		Scribd,
		/// <summary>
		/// Flash element.
		/// </summary>
		Flash,
		/// <summary>
		/// Audio element
		/// </summary>
		Audio,
		/// <summary>
		/// Unescaped HTML element.
		/// </summary>
		Html,
		/// <summary>
		/// PlaceHolder element handled by IPlaceHolderHandler.
		/// </summary>
		PlaceHolder,
		/// <summary>
		/// Date element.
		/// </summary>
		Date,
		/// <summary>
		/// Mailto response links element.
		/// </summary>
		MailtoResponseLinks,
		/// <summary>
		/// Element that can be True or False (using a checkbox).
		/// </summary>
		Boolean
	}
}
