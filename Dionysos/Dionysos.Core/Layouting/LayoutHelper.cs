﻿using System;
using System.Text.RegularExpressions;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Layouting
{
	/// <summary>
	/// Class for helping in Layouting actions
	/// </summary>
	public class LayoutHelper
	{
		#region Fields

		private string layoutMarkup = String.Empty;
		private string systemName = String.Empty;
		private string entityType = String.Empty;
		private IEntityCollection entityCollection = null;
		private LayoutElementList layoutElements = null;
		private bool layoutIsParsed = false;
		private MatchCollection layoutRegexMatches = null;
		private Regex regexLayout = new Regex("<!-- BeginLayoutElement SystemName=\"(.*?)\" DataType=\"(.*?)\" FriendlyName=\"(.*?)\" SortOrder=\"(.*?)\" -->(.*?)<!-- EndLayoutElement -->", RegexOptions.Singleline);

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the layoutElements
		/// </summary>
		public LayoutElementList LayoutElements
		{
			get
			{
				return this.layoutElements;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initialize a Layout class
		/// </summary>
		/// <param name="systemName">Systemname of the Layout</param>
		/// <param name="layoutMarkup">String containing the layoutMarkup</param>
		/// <param name="entityType">EntityType to be used for storage</param>
		public LayoutHelper(string systemName, string layoutMarkup, string entityType)
			: this(systemName, layoutMarkup, entityType, null)
		{ }

		/// <summary>
		/// Initialize a Layout class
		/// </summary>
		/// <param name="systemName">Systemname of the Layout</param>
		/// <param name="layoutMarkup">String containing the layoutMarkup</param>
		/// <param name="entityType">EntityType to be used for storage</param>
		/// <param name="entityCollection">EntityCollection containing the LayoutElements</param>
		public LayoutHelper(string systemName, string layoutMarkup, string entityType, IEntityCollection entityCollection)
		{
			// Get all LayoutElements from the layoutMarkup
			if (Instance.Empty(layoutMarkup))
			{
				throw new EmptyException("LayoutHelper class kan niet worden geïntieerd met layoutMarkup == string.Empty");
			}
			else if (Instance.Empty(entityType))
			{
				throw new EmptyException("LayoutHelper class kan niet worden geïntieerd met entityType == string.Empty");
			}
			else if (Instance.Empty(systemName))
			{
				throw new EmptyException("LayoutHelper class kan niet worden geïntieerd met systemName == string.Empty");
			}

			this.systemName = systemName;
			this.layoutMarkup = layoutMarkup;
			this.entityType = entityType;
			this.entityCollection = entityCollection;

			this.InitializeLayout();
		}

		#endregion

		#region Methods

		private void InitializeLayout()
		{
			// Initialize collection
			this.layoutElements = new LayoutElementList();
			this.layoutElements.LayoutSystemName = this.systemName;

			// Read the elements from the layoutMarkup
			this.ReadRegexMatchesFromLayout();

			// Create LayoutElements form the RegexMatches
			this.ParseLayout();

			// Replace a loaded elements with filled elements from the entity collection
			if (this.entityCollection != null)
			{
				this.LayoutElements.AddFromEntityCollection(this.entityCollection);
			}
		}

		private void ReadRegexMatchesFromLayout()
		{
			this.layoutRegexMatches = this.regexLayout.Matches(this.layoutMarkup);
		}

		private void ParseLayout()
		{
			if (this.layoutIsParsed)
			{
				throw new TechnicalException("ParseLayout kan slechts 1x worden aangeroepen in de levensduur van een Layout class instantie");
			}
			else
			{
				for (int i = 0; i < this.layoutRegexMatches.Count; i++)
				{
					Match m = this.layoutRegexMatches[i];
					if (m.Groups.Count != 6) // We need 5, but it's always 0 = fullMatch, 1 = firstGroup
					{
						throw new InvalidTypeException("Er is een ongeldige parameter declaratie gevonden in de layoutMarkup: " + m.Value);
					}
					else
					{
						// Parse the system name if it includes width & height
						string dataType = m.Groups[2].Value;
						int? maxWidth = null;
						int? maxHeight = null;

						if (dataType.Contains("-"))
						{
							int dashLocation = dataType.IndexOf("-");
							int commaLocation = dataType.IndexOf(",");
							if (commaLocation > 0)
							{
								maxWidth = Convert.ToInt32(dataType.Substring(dashLocation + 1, commaLocation - (dashLocation + 1)));
								maxHeight = Convert.ToInt32(dataType.Substring(commaLocation + 1));
							}
							else
							{
								// Only width specified
								maxWidth = Convert.ToInt32(dataType.Substring(dashLocation + 1));
							}
						}

						// Again we start from 1 not 0, since 0 is the fullMatch
						this.LayoutElements.Add(this.entityType,
							m.Groups[1].Value.Trim(),
							GetDataTypeFromString(dataType),
							maxWidth,
							maxHeight,
							m.Groups[3].Value,
							m.Groups[5].Value,
							Convert.ToInt32(m.Groups[4].Value));
					}
				}
			}

			// Sort the collection on the SortOrder
			this.LayoutElements.Sort(LayoutElementList.SortOrderComparison);

			layoutIsParsed = true;
		}

		/// <summary>
		/// Get a Regex for replacing elements in a Layout
		/// </summary>
		/// <param name="systemName">Systemname of the to be replaced element</param>
		/// <returns>Regex</returns>
		public static Regex GetReplaceRegExForSystemName(string systemName)
		{
			Regex regexLayoutElement = new Regex(String.Format("<!-- BeginLayoutElement SystemName=\"{0}\" DataType=\"(.*?)\" FriendlyName=\"(.*?)\" SortOrder=\"(.*?)\" -->(.*?)<!-- EndLayoutElement -->", systemName), RegexOptions.Compiled | RegexOptions.Singleline);
			return regexLayoutElement;
		}

		/// <summary>
		/// Renders the ouput of the temlate based on the current LayoutElementList
		/// </summary>
		/// <param name="asLayout">If this is TRUE all elements which have Value == Default value are not being replaced, so still available as real LayoutElements</param>
		/// <returns></returns>
		public string Render(bool asLayout)
		{
			return this.layoutMarkup;
		}

		/// <summary>
		/// Convert a DataType string to the Enum value
		/// </summary>
		/// <param name="dataType">string name of the DataType</param>
		/// <returns>Enum value for the supplied string</returns>
		public static LayoutElementDataType GetDataTypeFromString(string dataType)
		{
			// Parse any attributes from the dataType
			if (dataType.Contains("-"))
			{
				dataType = dataType.Substring(0, dataType.IndexOf("-"));
			}
			return (LayoutElementDataType)Enum.Parse(typeof(LayoutElementDataType), dataType);
		}

		#endregion
	}
}