﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Layouting
{
	/// <summary>
	/// Layout rendering format
	/// </summary>
	public enum LayoutRenderFormat
	{
		/// <summary>
		/// Full HtmlMarkUp with full Url (for Web usage)
		/// </summary>
		HtmlMarkup,
		/// <summary>
		/// Plain text
		/// </summary>
		PlainText
	}
}
