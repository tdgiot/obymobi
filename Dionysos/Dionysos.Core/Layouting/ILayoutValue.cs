﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Layouting
{
	/// <summary>
	/// Represents a layout value.
	/// </summary>
	public interface ILayoutValue : IEntity
	{
		/// <summary>
		/// Gets or sets the system name.
		/// </summary>
		/// <value>
		/// The system name.
		/// </value>
		string SystemName { get; set; }

		/// <summary>
		/// Gets or sets the instance number of the layout element.
		/// </summary>
		/// <value>
		/// The instance number of the layout element.
		/// </value>
		int InstanceNo { get; set; }

		/// <summary>
		/// Gets or sets the sort order.
		/// </summary>
		/// <value>
		/// The sort order.
		/// </value>
		/// <remarks>
		/// This value determines in which order the layout elements are shown to the user, not the order of rendering.
		/// </remarks>
		int SortOrder { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		string Value { get; set; }

		/// <summary>
		/// Gets or sets the secondary value.
		/// </summary>
		/// <value>
		/// The secondary value.
		/// </value>
		string Value2 { get; set; }

		/// <summary>
		/// Gets or sets the tertiary value.
		/// </summary>
		/// <value>
		/// The tertiary value.
		/// </value>
		string Value3 { get; set; }

		/// <summary>
		/// Gets or sets the quaternary value.
		/// </summary>
		/// <value>
		/// The quaternary value.
		/// </value>
		string Value4 { get; set; }

		/// <summary>
		/// Gets or sets the width that can be used by the layout element (especially for images, but also paragraphs).
		/// </summary>
		/// <value>
		/// The width.
		/// </value>
		int? Width { get; set; }

		/// <summary>
		/// Gets the height that can be used by the layout element (especially for images, but also paragraphs).
		/// </summary>
		/// <value>
		/// The height.
		/// </value>
		int? Height { get; set; }
	}
}
