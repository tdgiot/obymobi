﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Layouting
{
	/// <summary>
	/// Represents a layout element.
	/// </summary>
	public interface ILayoutElement
	{
		/// <summary>
		/// Gets or sets the system name.
		/// </summary>
		/// <value>
		/// The system name.
		/// </value>
		string SystemName { get; set; }

		/// <summary>
		/// Gets or sets the friendly name.
		/// </summary>
		/// <value>
		/// The friendly name.
		/// </value>
		string FriendlyName { get; set; }

		/// <summary>
		/// Gets or sets the data type of the layout element.
		/// </summary>
		/// <value>
		/// The data type of the layout element.
		/// </value>
		LayoutElementDataType DataType { get; set; }

		/// <summary>
		/// Gets or sets the default value.
		/// </summary>
		/// <value>
		/// The default value.
		/// </value>
		string DefaultValue { get; set; }

		/// <summary>
		/// Gets or sets the sort order.
		/// </summary>
		/// <value>
		/// The sort order.
		/// </value>
		/// <remarks>
		/// This value determines in which order the layout elements are shown to the user, not the order of rendering.
		/// </remarks>
		int? SortOrder { get; set; }

		/// <summary>
		/// Gets or sets the maximum width that can be used by the layout element (especially for images, but also paragraphs).
		/// </summary>
		/// <value>
		/// The maximum width.
		/// </value>
		int? MaxWidth { get; set; }

		/// <summary>
		/// Gets the maximum height that can be used by the layout element (especially for images, but also paragraphs).
		/// </summary>
		/// <value>
		/// The maximum height.
		/// </value>
		int? MaxHeight { get; set; }

		/// <summary>
		/// Gets or sets the instance number of the layout element.
		/// </summary>
		/// <value>
		/// The instance number of the layout element.
		/// </value>
		/// <remarks>
		/// This number is used for template elements that contain more than 1 subitem.
		/// In a layout this can be because 1 area allows for multiple Layout's as content (i.e.: NewsPage.Messages can contain: Message, MessageWithPicture, etc.).
		/// In a LayoutEntityLink (actual content) this can be because multiple messages are placed.
		/// </remarks>
		int InstanceNo { get; set; }

		/// <summary>
		/// Gets the layout (if this is a subelement).
		/// </summary>
		ILayout Layout { get; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="ILayoutElement"/> is repeatable.
		/// </summary>
		/// <value>
		///   <c>true</c> if repeatable; otherwise, <c>false</c>.
		/// </value>
		bool Repeatable { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="ILayoutElement"/> is optional.
		/// </summary>
		/// <value>
		///   <c>true</c> if optional; otherwise, <c>false</c>.
		/// </value>
		bool Optional { get; set; }

		/// <summary>
		/// Gets or sets the sub elements.
		/// </summary>
		/// <value>
		/// The sub elements.
		/// </value>
		LayoutElementList SubElements { get; set; }
	}
}
