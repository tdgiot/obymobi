﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Layouting
{
    /// <summary>
    /// Collection of LayoutElements
    /// </summary>
    public class LayoutElementList : List<ILayoutElement>
    {
        private string layoutSystemName = string.Empty;

        /// <summary>
        /// Add element to the List
        /// </summary>
        /// <param name="entityType">entityType, used to create an Entity implemeting the interface to hold the values</param>
        /// <param name="systemName">systemName</param>
        /// <param name="dataType">dataType</param>
        /// <param name="maxWidth">maxWidth</param>
        /// <param name="maxHeight">maxHeight</param>
        /// <param name="friendlyName">friendlyName</param>
        /// <param name="defaultValue">defaultValue</param>        
        /// <param name="sortOrder">sortOrder</param>        
        public void Add(string entityType, string systemName, LayoutElementDataType dataType, int? maxWidth, int? maxHeight, string friendlyName, string defaultValue, int sortOrder)
        {
            ILayoutElement element = Dionysos.DataFactory.EntityFactory.GetEntity(entityType) as ILayoutElement;

            #region Retrieve entity + error handling
            if (element == null)
            {
                if (!entityType.EndsWith("Entity"))
                {
                    element = Dionysos.DataFactory.EntityFactory.GetEntity(entityType + "Entity") as ILayoutElement;
                }

                if(element == null)
                    throw new Dionysos.TechnicalException(string.Format("Bij het aanmaken van het LayoutElementList element is er een niet bestaande EntityType meegegeven of de EntityType implementeerd de interface ILayoutElement niet - ({0})",entityType));
            }
            #endregion

            element.DataType = dataType;
            element.SystemName = systemName;
            element.FriendlyName = friendlyName;
            element.DefaultValue = defaultValue;
            element.SortOrder = sortOrder;
            element.MaxWidth = maxWidth;
            element.MaxHeight = maxHeight;

            this.Add(element);
        }

        /// <summary>
        /// Fill the LayoutElementList based on contents of an EntityCollection
        /// The Entities in the EntityCollection should implement ILayoutElement
        /// </summary>
        /// <param name="entityCollection"></param>
        public void AddFromEntityCollection(IEntityCollection entityCollection)
        {
            for (int i = 0; i < entityCollection.Count; i++)
            {
                ILayoutElement current;
                try
                {
                    current = entityCollection[i] as ILayoutElement;
                }
                catch
                {
                    throw new InvalidTypeException("Een LayoutElementList kan niet worden gevuld met entities die de ILayoutElement niet implementeren.");
                }
                
                // If it exists, replace but keeping the default value
                int indexOfSystemName = this.Find(current.SystemName);
                string defaultValue = string.Empty;
                if (indexOfSystemName >= 0)
                {
                    defaultValue = this[indexOfSystemName].DefaultValue;
                    this.RemoveAt(indexOfSystemName);
                }

                current.DefaultValue = defaultValue;

                this.Add(current);                
            }
        }

        /// <summary>
        /// Comparison for sorting on SortOrder
        /// </summary>
        public static Comparison<ILayoutElement> SortOrderComparison = delegate(ILayoutElement t1, ILayoutElement t2)
        {
            return t1.SortOrder.Value.CompareTo(t2.SortOrder.Value);
        };

        /// <summary>
        /// Get the index of a ILayoutElement in the list based on it's SystemName
        /// </summary>
        /// <param name="systemName">SystemName to search for</param>
        /// <returns>Index of item, -1 if not found.</returns>
        public int Find(string systemName)
        {
            int toReturn = -1;
            for (int i = 0; i < this.Count; i++)
            {
                ILayoutElement e = this[i];
                if (e.SystemName == systemName)
                {
                    toReturn = i;
                    break;
                }
            }
            return toReturn;
        }        

        /// <summary>
        /// Gets or sets the layoutSystemName
        /// </summary>
        public string LayoutSystemName
        {
            get
            {
                return this.layoutSystemName;
            }
            set
            {
                this.layoutSystemName = value;
            }
        }        

    }
}
