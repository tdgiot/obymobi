using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;

namespace Dionysos
{
    /// <summary>
    /// Class which contains information about the application's environment
    /// </summary>
    public class EnvironmentInfo : IEnvironmentInfo
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Marti.EnvironmentInfo type
        /// </summary>
        public EnvironmentInfo()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the machine
        /// </summary>
        public string MachineName
        {
            get
            {
                return Environment.MachineName;
            }
        }

        /// <summary>
        /// Gets the operation system of the machine
        /// </summary>
        public string OperatingSystem
        {
            get
            {
                return Environment.OSVersion.VersionString;
            }
        }

        /// <summary>
        /// Gets the platform of the machine
        /// </summary>
        public string Platform
        {
            get
            {
                return Enum.GetName(typeof(PlatformID), Environment.OSVersion.Platform);
            }
        }

        /// <summary>
        /// Gets the installed service pack(s) of the machine
        /// </summary>
        public string ServicePack
        {
            get
            {
                return Environment.OSVersion.ServicePack;
            }
        }

        /// <summary>
        /// Gets the version of the operating system of the machine
        /// </summary>
        public string OSVersion
        {
            get
            {
                return Environment.OSVersion.Version.ToString();
            }
        }

        /// <summary>
        /// Gets the amount of processors in this machine
        /// </summary>
        public int ProcessorCount
        {
            get
            {
                return Environment.ProcessorCount;
            }
        }

        /// <summary>
        /// Gets the current stack trace
        /// </summary>
        public string StackTrace
        {
            get
            {
                return Environment.StackTrace;
            }
        }

        /// <summary>
        /// Gets the amount of milliseconds elapsed since the system started
        /// </summary>
        public int TickCount
        {
            get
            {
                return Environment.TickCount;
            }
        }

        /// <summary>
        /// Gets the amount of seconds elapsed since the system started
        /// </summary>
        public float SecondCount
        {
            get
            {
                return (Environment.TickCount / 1000);
            }
        }

        /// <summary>
        /// Gets the amount of minutes elapsed since the system started
        /// </summary>
        public float MinuteCount
        {
            get
            {
                return (Environment.TickCount / 60000);
            }
        }

        /// <summary>
        /// Gets the amount of hours elapsed since the system started
        /// </summary>
        public float HourCount
        {
            get
            {
                return (Environment.TickCount / 3600000);
            }
        }

        /// <summary>
        /// Gets the amount of days elapsed since the system started
        /// </summary>
        public float DayCount
        {
            get
            {
                return (Environment.TickCount / 86400000);
            }
        }

        /// <summary>
        /// Gets the network domain name associated with the current user
        /// </summary>
        public string UserDomainName
        {
            get
            {
                return Environment.UserDomainName;
            }
        }

        /// <summary>
        /// Gets the name of the user who started the current thread
        /// </summary>
        public string UserName
        {
            get
            {
                return Environment.UserName;
            }
        }

        /// <summary>
        /// Gets a System.Version instance that describes the current version of the Common Language Runtime
        /// </summary>
        public Version Version
        {
            get
            {
                return Environment.Version;
            }
        }

        /// <summary>
        /// Gets the amount of physical memory in bytes mapped to the current process context
        /// </summary>
        public long MemoryBytes
        {
            get
            {
                return Environment.WorkingSet;
            }
        }

        /// <summary>
        /// Gets the amount of physical memory in kilobytes mapped to current process context
        /// </summary>
        public long MemoryKiloBytes
        {
            get
            {
                return (Environment.WorkingSet / 1024);
            }
        }

        /// <summary>
        /// Gets the amount of physical memory in megabytes mapped to current process context
        /// </summary>
        public long MemoryMegaBytes
        {
            get
            {
                return (Environment.WorkingSet / 1048576);
            }
        }

        /// <summary>
        /// Gets the command line for this process
        /// </summary>
        public string CommandLine
        {
            get
            {
                return Environment.CommandLine;
            }
        }

        /// <summary>
        /// Gets or sets the fully qualified path of the current directory
        /// </summary>
        public string CurrentDirectory
        {
            get
            {
                return Environment.CurrentDirectory;
            }
            set
            {
                Environment.CurrentDirectory = value;
            }
        }

        /// <summary>
        /// Gets the commandline argument for the current process
        /// </summary>
        public string[] CommandLineArgs
        {
            get
            {
                return this.GetCommandLineArgs();
            }
        }

        /// <summary>
        /// Gets a string representation of the commandline arguments for the current process
        /// </summary>
        public string CommandLineArgsString
        {
            get
            {
                return this.GetCommandLineArgsString();
            }
        }

        /// <summary>
        /// Gets a string array containing the names of the logical drives of this computer
        /// </summary>
        public string[] LogicalDrives
        {
            get
            {
                return Environment.GetLogicalDrives();
            }
        }

        /// <summary>
        /// Gets a string representation of the logical drives of this computer
        /// </summary>
        public string LogicalDriveString
        {
            get
            {
                return this.GetLogicalDrivesString();
            }
        }

        /// <summary>
        /// Gets the fully qualified path to the system directory
        /// </summary>
        public string SystemDirectory
        {
            get
            {
                return Environment.SystemDirectory;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a string array containing the commandline arguments for the current process
        /// </summary>
        /// <returns></returns>
        public string[] GetCommandLineArgs()
        {
            return Environment.GetCommandLineArgs();
        }

        private string GetCommandLineArgsString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            string[] commandLineArgs = this.GetCommandLineArgs();
            for (int i = 0; i < commandLineArgs.Length; i++)
            {
                string arg = commandLineArgs[i];
                stringBuilder.AppendFormat("{0} ", arg);
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Returns a string array containing the logical drives for the current machines
        /// </summary>
        /// <returns></returns>
        public string[] GetLogicalDrives()
        {
            return Environment.GetLogicalDrives();
        }

        private string GetLogicalDrivesString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            string[] logicalDrives = this.GetLogicalDrives();
            for (int i = 0; i < logicalDrives.Length; i++)
            {
                string logicalDrive = logicalDrives[i];
                stringBuilder.AppendFormat("{0} ", logicalDrive);
            }

            return stringBuilder.ToString();
        }

        private string ProcessStackTrace()
        {
            string stackTrace = string.Empty;

            string[] stackTraceLines = this.StackTrace.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            for (int i = 0; i < stackTraceLines.Length; i++)
            {
                stackTrace += i.ToString("00") + ".";
                stackTrace += "\t\t\t\t";

                for (int j = 0; j < i; j++)
                {
                    stackTrace += "  ";
                }

                stackTrace += stackTraceLines[i].TrimStart();
                stackTrace += "\r\n";
            }

            return stackTrace;
        }

        /// <summary>
        /// Create a generic string instance containing all the environment information in it
        /// </summary>
        /// <returns>A string containing all the environment information</returns>
        public new string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("*** User information ***");
            stringBuilder.AppendFormat("User name:\t\t\t{0}\r\n", this.UserName);
            stringBuilder.AppendFormat("User domain name:\t\t\t{0}\r\n\r\n", this.UserDomainName);

            stringBuilder.AppendLine("*** Machine information ***");
            stringBuilder.AppendFormat("Machine name:\t\t\t{0}\r\n", this.MachineName);
            stringBuilder.AppendFormat("Operating system:\t\t\t{0}\r\n", this.OperatingSystem);
            stringBuilder.AppendFormat("Time in use:\t\t\t{0} days : {1} hours : {2} minutes : {3} seconds : {4} milliseconds\r\n", this.DayCount, this.HourCount - (this.DayCount * 24), this.MinuteCount - (this.HourCount * 60), this.SecondCount - (this.MinuteCount * 60), this.TickCount - (this.SecondCount * 1000));
            stringBuilder.AppendFormat("System directory:\t\t\t{0}\r\n", this.SystemDirectory);
            stringBuilder.AppendFormat("Logical drives:\t\t\t{0}\r\n", this.GetLogicalDrivesString());
            stringBuilder.AppendFormat("Processor count:\t\t\t{0}\r\n\r\n", this.ProcessorCount);

            stringBuilder.AppendLine("*** Process information ***");
            stringBuilder.AppendFormat("Memory in use:\t\t\t{0} MegaBytes : {1} KiloBytes : {2} Bytes\r\n", this.MemoryMegaBytes, this.MemoryKiloBytes - (this.MemoryMegaBytes * 1024), this.MemoryBytes - (this.MemoryKiloBytes * 1024));
            stringBuilder.AppendFormat("Current directory:\t\t\t{0}\r\n", this.CurrentDirectory);
            stringBuilder.AppendFormat("Commandline:\t\t\t{0}\r\n", this.CommandLine);
            stringBuilder.AppendFormat("Commandline arguments:\t\t{0}\r\n", this.CommandLineArgsString);
            //stringBuilder.AppendFormat("Current stacktrace: \r\n{0}\r\n\r\n", this.ProcessStackTrace());

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Writes the current EnvironmentInfo instance to XML
        /// </summary>
        public void ToXML()
        {
        }

        #endregion
    }
}
