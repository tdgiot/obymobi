﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Interfaces;
using System.Collections;

namespace Dionysos
{
	/// <summary>
	/// Class which represent a License
	/// </summary>
	public class License : ILicense
	{
		#region Fields

		private ICollection<ILicenseModule> modules = new List<ILicenseModule>();

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Dionysos.License type
		/// </summary>
		public License()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the modules for the license
		/// </summary>
		public ICollection<ILicenseModule> Modules
		{
			get
			{
				return this.modules;
			}
			set
			{
				this.modules = value;
			}
		}

		/// <summary>
		/// Gets or set the module license for the specified module name
		/// </summary>
		/// <param name="moduleName">The name of the module to get the license for</param>
		/// <returns>An ILicenseModule instance</returns>
		public ILicenseModule this[string moduleName]
		{
			get
			{
				// Lookup the module and return result
				ILicenseModule returnValue = null;

				foreach (ILicenseModule licencedModule in this.Modules)
				{
					if (licencedModule.NameSystem == moduleName)
					{
						returnValue = licencedModule;
						break;
					}
				}

				return returnValue;
			}
			set
			{
				// If module already exists, remove it first
				if (this.Modules.Contains(this[moduleName]))
				{
					this.Modules.Remove(this[moduleName]);
				}

				this.Modules.Add(value);
			}
		}

		#endregion
	}
}
