﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Reflection;
using System.Web;
using System.Web.Caching;

namespace Dionysos
{
	/// <summary>
	/// Utility class which contains helper methods for licensing purposes.
	/// </summary>
	public class LicenseUtil
	{
		#region Fields

		/// <summary>
		/// The cache key for License.
		/// </summary>
		public const string CacheKeyForLicense = "LicenseUtil.License";

		/// <summary>
		/// The cache key for LicensedModules.
		/// </summary>
		public const string CacheKeyForLicensedModules = "LicenseUtil.LicensedModules";

		/// <summary>
		/// The cache key for LicensedUIElements.
		/// </summary>
		public const string CacheKeyForLicensedUIElements = "LicenseUtil.LicensedUIElements";

		#endregion

		#region Methods

		/// <summary>
		/// Get the license information which contains information about which modules and ui elements are enabled
		/// </summary>
		/// <returns>An ILicense instante containing the license information</returns>
		public static ILicense GetLicense()
		{
			return GetLicense(true);
		}

		/// <summary>
		/// Get the license information which contains information about which modules and ui elements are enabled
		/// </summary>
		/// <param name="fromCache">Flag which indicates whether the information should be restored from cache</param>
		/// <returns>An ILicense instante containing the license information</returns>
		public static ILicense GetLicense(bool fromCache)
		{
			ILicense license = null;

			// Check whether the license exists in cache
			string cacheKey = LicenseUtil.CacheKeyForLicense;
			if (fromCache)
				if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Cache[cacheKey] != null)
					license = System.Web.HttpContext.Current.Cache[cacheKey] as ILicense;

			// Check whether the license is found in cache
			if (Instance.Empty(license))
			{
				// Initialize a new license instance
				license = new License();

				// Initialize an entity collection containing the modules
				IEntityCollection moduleCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("Module") as IEntityCollection;
				if (Instance.Empty(moduleCollection))
				{
					throw new EmptyException("Entity collection with entity name 'Module' could not be initialized.");
				}
				else
				{
					IEntity moduleEntity = DataFactory.EntityFactory.GetEntity("Module") as IEntity;
					SortExpression sort = new SortExpression(new SortClause(moduleEntity.Fields["SortOrder"], SortOperator.Ascending));

					moduleCollection.GetMulti(null, 0, sort);

					for (int i = 0; i < moduleCollection.Count; i++)
					{
						// Get an module entity from the collection
						// And retrieve the name of the module
						ILicenseModule licenseModule = moduleCollection[i] as ILicenseModule;
						if (licenseModule != null)
						{
							// Add the LicenseModule instance to the license
							license.Modules.Add(licenseModule);
						}
					}
				}

				// Add to cache if possible
				if (System.Web.HttpContext.Current != null)
					System.Web.HttpContext.Current.Cache.Add(cacheKey, license, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Normal, null);
			}

			return license;
		}

		/// <summary>
		/// Gets a collection containing the licensed modules
		/// </summary>
		/// <returns>An List instance containing the licensed modules</returns>
		public static List<ILicenseModule> GetLicensedModules()
		{
			return GetLicensedModules(true);
		}

		/// <summary>
		/// Gets a collection containing the licensed modules
		/// </summary>
		/// <param name="fromCache">Flag which indicates whether the information should be restored from cache</param>
		/// <returns>An List instance containing the licensed modules</returns>
		public static List<ILicenseModule> GetLicensedModules(bool fromCache)
		{
			List<ILicenseModule> toReturn = null;

			string cacheKey = LicenseUtil.CacheKeyForLicensedModules;
			if (fromCache)
			{
				if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Cache[cacheKey] != null)
					toReturn = System.Web.HttpContext.Current.Cache[cacheKey] as List<ILicenseModule>;
			}

			if (toReturn == null)
			{
				// Get the license
				ILicense license = GetLicense(fromCache);
				toReturn = new List<ILicenseModule>();

				foreach (ILicenseModule module in license.Modules)
				{
					if (module.IsLicensed)
						toReturn.Add(module);
				}

				if (System.Web.HttpContext.Current != null)
					System.Web.HttpContext.Current.Cache.Add(cacheKey, toReturn, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Normal, null);
			}

			return toReturn;
		}

		/// <summary>
		/// Gets the licensed UI elements form the cache.
		/// </summary>
		/// <returns>The list of licensed UI elements.</returns>
		public static List<ILicenseUIElement> GetLicensedUIElements()
		{
			return GetLicensedUIElements(true);
		}

		/// <summary>
		/// Gets the licensed UI elements.
		/// </summary>
		/// <param name="fromCache">If set to <c>true</c> gets the list from cache.</param>
		/// <returns>The list of licensed UI elements.</returns>
		public static List<ILicenseUIElement> GetLicensedUIElements(bool fromCache)
		{
			List<ILicenseUIElement> licensedUIElements = null;

			string cacheKey = LicenseUtil.CacheKeyForLicensedUIElements;
			if (fromCache && HttpContext.Current != null && HttpContext.Current.Cache[cacheKey] != null)
			{
				// Get list from cache
				licensedUIElements = HttpContext.Current.Cache[cacheKey] as List<ILicenseUIElement>;
			}

			if (licensedUIElements == null)
			{
				// Create new list
				licensedUIElements = new List<ILicenseUIElement>();
				foreach (ILicenseModule licenseModule in GetLicensedModules(fromCache))
				{
					foreach (ILicenseUIElement licenseUIElement in licenseModule.UIElements)
					{
						if (licenseUIElement.IsLicensed)
						{
							licensedUIElements.Add(licenseUIElement);
						}
					}
				}

				if (HttpContext.Current != null)
				{
					// Add list to cache
					HttpContext.Current.Cache.Add(cacheKey, licensedUIElements, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
				}
			}

			return licensedUIElements;
		}

		/// <summary>
		/// Remove the cached license from the cache.
		/// </summary>
		public static void RemoveFromCache()
		{
			HttpContext.Current.Cache.Remove(LicenseUtil.CacheKeyForLicense);
		}

		/// <summary>
		/// Check whether the module is licensed.
		/// </summary>
		/// <param name="moduleNameSystem">The NameSystem of the module.</param>
		/// <returns>
		///   <c>true</c> if the module exists and is licensed; otherwise, <c>false</c>.
		/// </returns>
		public static bool ModuleIsLicensed(string moduleNameSystem)
		{
			bool isLicensed = false;

			ILicenseModule moduleLicense = LicenseUtil.GetLicense()[moduleNameSystem];
			if (moduleLicense != null &&
				moduleLicense.IsLicensed)
			{
				isLicensed = true;
			}

			return isLicensed;
		}

		/// <summary>
		/// Check whether the ui element is licensed.
		/// </summary>
		/// <param name="moduleNameSystem">The NameSystem of the module.</param>
		/// <param name="uiElementNameSystem">The UI element name system.</param>
		/// <returns>
		///   <c>true</c> if the ui element exists and is licensed; otherwise, <c>false</c>.
		/// </returns>
		public static bool UIElementIsLicensed(string moduleNameSystem, string uiElementNameSystem)
		{
			string uiElementTypeNameFull;
			return LicenseUtil.UIElementIsLicensed(moduleNameSystem, uiElementNameSystem, out uiElementTypeNameFull);
		}

		/// <summary>
		/// Check whether the ui element is licensed.
		/// </summary>
		/// <param name="moduleNameSystem">The NameSystem of the module.</param>
		/// <param name="uiElementNameSystem">The UI element name system.</param>
		/// <param name="uiElementTypeNameFull">The UI element type name full.</param>
		/// <returns>
		///   <c>true</c> if the ui element exists and is licensed; otherwise, <c>false</c>.
		/// </returns>
		public static bool UIElementIsLicensed(string moduleNameSystem, string uiElementNameSystem, out string uiElementTypeNameFull)
		{
			bool isLicensed = false;
			uiElementTypeNameFull = null;

			ILicenseModule moduleLicense = LicenseUtil.GetLicense()[moduleNameSystem];
			if (moduleLicense != null &&
				moduleLicense.IsLicensed)
			{
				ILicenseUIElement uiElementLicense = moduleLicense[uiElementNameSystem];
				if (uiElementLicense != null)
				{
					isLicensed = uiElementLicense.IsLicensed;
					uiElementTypeNameFull = uiElementLicense.TypeNameFull;
				}
			}

			return isLicensed;
		}

		#endregion
	}
}