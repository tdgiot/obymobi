﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Utility class for doing null-safe operations.
	/// </summary>
	public static class NullSafeUtil
	{
		/// <summary>
		/// Returns the result of the selector if the source is not null; otherwise returns the default value of the resulting type.
		/// </summary>
		/// <typeparam name="TSource">The source type to select from.</typeparam>
		/// <typeparam name="TResult">The resulting type.</typeparam>
		/// <param name="source">The source to select from.</param>
		/// <param name="selector">The selector function to run on the source.</param>
		/// <returns>Returns the result of the selector if source is not null; otherwise returns the default value of the resulting type.</returns>
		public static TResult NullSafe<TSource, TResult>(this TSource source, Func<TSource, TResult> selector)
		{
			if (source != null)
			{
				return selector(source);
			}
			else
			{
				return default(TResult);
			}
		}

		/// <summary>
		/// Runs the action if the source is not null.
		/// </summary>
		/// <typeparam name="T">The source type to run the action on.</typeparam>
		/// <param name="source">The source to run the action on.</param>
		/// <param name="action">The action to run.</param>
		public static void NullSafe<T>(this T source, Action<T> action)
		{
			if (source != null)
			{
				action(source);
			}
		}
	}
}
