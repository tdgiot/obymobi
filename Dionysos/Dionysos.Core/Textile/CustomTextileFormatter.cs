﻿using Dionysos.Textile.Blocks;
using Textile;
using Textile.Blocks;
using Textile.States;

namespace Dionysos.Textile
{
	/// <summary>
	/// Custom Textile formatter.
	/// </summary>
	public class CustomTextileFormatter : GenericFormatter
	{
		/// <summary>
		/// Public constructor, where the formatter is hooked up to an outputter.
		/// </summary>
		/// <param name="output">The outputter to be used.</param>
		/// <param name="formatInline">If set to <c>true</c> only formats inline styles.</param>
		/// <param name="restricted">If set to <c>true</c> uses restricted styles.</param>
		public CustomTextileFormatter(IOutputter output, bool formatInline = false, bool restricted = false)
			: base(output, formatInline ? typeof(PassthroughFormatterState) : typeof(ParagraphFormatterState))
		{
			//this.UseRestrictedMode = restricted;

			if (!restricted) this.RegisterFormatterState(typeof(HeaderFormatterState));
			if (!restricted) this.RegisterFormatterState(typeof(BlockQuoteFormatterState));
			if (!formatInline) this.RegisterFormatterState(typeof(ParagraphFormatterState));
			if (!restricted) this.RegisterFormatterState(typeof(FootNoteFormatterState));
			if (!formatInline) this.RegisterFormatterState(typeof(OrderedListFormatterState));
			if (!formatInline) this.RegisterFormatterState(typeof(UnorderedListFormatterState));
			if (!formatInline) this.RegisterFormatterState(typeof(TableFormatterState));
			if (!formatInline) this.RegisterFormatterState(typeof(TableRowFormatterState));
			if (!restricted) this.RegisterFormatterState(typeof(CodeFormatterState));
			if (!restricted) this.RegisterFormatterState(typeof(PreFormatterState));
			if (!restricted) this.RegisterFormatterState(typeof(PreCodeFormatterState));
			if (!restricted) this.RegisterFormatterState(typeof(PreBlockFormatterState));
			if (!restricted) this.RegisterFormatterState(typeof(NoTextileFormatterState));

			if (!restricted) this.RegisterBlockModifier(new NoTextileBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new CodeBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new PreBlockModifier());
			this.RegisterBlockModifier(new HyperLinkBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new ImageBlockModifier());
			this.RegisterBlockModifier(new CustomGlyphBlockModifier());
			this.RegisterBlockModifier(new EmphasisPhraseBlockModifier());
			this.RegisterBlockModifier(new StrongPhraseBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new ItalicPhraseBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new BoldPhraseBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new CitePhraseBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new DeletedPhraseBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new InsertedPhraseBlockModifier());
			this.RegisterBlockModifier(new SuperScriptPhraseBlockModifier());
			this.RegisterBlockModifier(new SubScriptPhraseBlockModifier());
			this.RegisterBlockModifier(new SpanPhraseBlockModifier());
			if (!restricted) this.RegisterBlockModifier(new FootNoteReferenceBlockModifier());
		}
	}
}
