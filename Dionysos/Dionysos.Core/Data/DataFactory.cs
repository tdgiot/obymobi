using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;

namespace Dionysos
{
    /// <summary>
    /// Singleton factory class for creating data objects with
    /// </summary>
    public class DataFactory
    {
        #region Fields

        static DataFactory instance = null;

        private IEntityCollectionFactory entityCollectionFactory = null;
        private IEntityFactory entityFactory = null;
        private IEntityFieldFactory entityFieldFactory = null;

        private IEntityUtil entityUtil = null;
        private IEntityCollectionUtil entityCollectionUtil = null;

        private IEntityPrefetchPathFactory entityPrefetchPathFactory = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the DataFactory class if the instance has not been initialized yet
        /// </summary>
        static DataFactory()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new DataFactory();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static DataFactory instance
        /// </summary>
        private void Init()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Dionysos.Interfaces.IEntityFieldFactory instance
        /// </summary>
        public static IEntityFieldFactory EntityFieldFactory
        {
            get
            {
                if (Instance.Empty(instance.entityFieldFactory))
                {
                    throw new Dionysos.EntityCollectionFactoryNotSetException("Entity field factory not set.");
                }
                return instance.entityFieldFactory;
            }
            set
            {
                instance.entityFieldFactory = value;
            }
        }

        /// <summary>
        /// Gets or sets the Dionysos.Interfaces.IEntityCollectionFactory instance
        /// </summary>
        public static IEntityCollectionFactory EntityCollectionFactory
        {
            get
            {
                if (Instance.Empty(instance.entityCollectionFactory))
                {
                    throw new Dionysos.EntityCollectionFactoryNotSetException("Entity collection factory not set.");
                }
                return instance.entityCollectionFactory;
            }
            set
            {
                instance.entityCollectionFactory = value;
            }
        }

        /// <summary>
        /// Gets or sets the Dionysos.Interfaces.IEntityFactory instance
        /// </summary>
        public static IEntityFactory EntityFactory
        {
            get
            {
                if (Instance.Empty(instance.entityFactory))
                {
                    throw new Dionysos.EntityFactoryNotSetException("Entity factory not set.");
                }
                return instance.entityFactory;
            }
            set
            {
                instance.entityFactory = value;
            }
        }

        /// <summary>
        /// Gets or sets the Dionysos.Interfaces.IEntityUtil instance
        /// </summary>
        public static IEntityUtil EntityUtil
        {
            get
            {
                if (Instance.Empty(instance.entityUtil))
                {
                    throw new Dionysos.EntityUtilNotSetException("EntityUtil not set, check if EntityUtil is set in Global.asax.");
                }
                return instance.entityUtil;
            }
            set
            {
                instance.entityUtil = value;
            }
        }

        /// <summary>
        /// Gets or sets the Dionysos.Interfaces.IEntityCollectionUtil instance
        /// </summary>
        public static IEntityCollectionUtil EntityCollectionUtil
        {
            get
            {
                if (Instance.Empty(instance.entityCollectionUtil))
                {
                    throw new Dionysos.EntityCollectionUtilNotSetException("EntityCollection util not set.");
                }
                return instance.entityCollectionUtil;
            }
            set
            {
                instance.entityCollectionUtil = value;
            }
        }

        /// <summary>
        /// Gets or sets the Dionysos.Interfaces.IEntityPrefetchPathFactory instance
        /// </summary>
        public static IEntityPrefetchPathFactory EntityPrefetchPathFactory
        {
            get
            {
                if (Instance.Empty(instance.entityPrefetchPathFactory))
                {
                    throw new Dionysos.EntityCollectionUtilNotSetException("EntityCollection util not set.");
                }
                return instance.entityPrefetchPathFactory;
            }
            set
            {
                instance.entityPrefetchPathFactory = value;
            }
        }

        #endregion
    }
}