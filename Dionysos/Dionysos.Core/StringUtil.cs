﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections;
using Textile;
using Textile.States;
using Textile.Blocks;
using Dionysos.Textile;

namespace Dionysos
{
	/// <summary>
	/// Class providing static methods for string manipulation and analysing
	/// </summary>
	public static class StringUtil
	{
		// GK Done by me, I know it's not pretty, but only way to get a clean Dionysos project without references.
		#region Methods Dervied from StringUtilLight

		#region Comparison methods

		/// <summary>
		/// Returns a value indicating whether the specified System.String object occurs within this string.
		/// </summary>
		/// <param name="original">The original System.String object.</param>
		/// <param name="value">The System.String object to seek.</param>
		/// <param name="comparisionType">One of the System.StringComparison values.</param>
		/// <returns>true if the value parameter occurs within this string, or if value is the empty string (""); otherwise, false.</returns>
		public static bool Contains(string original, string value, StringComparison comparisionType)
		{
			return StringUtilLight.Contains(original, value, comparisionType);
		}

		/// <summary>
		/// Validate if a string is numeric (containing only numbers)
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static bool IsNumeric(string s)
		{
			return StringUtilLight.IsNumeric(s);
		}

		/// <summary>
		/// Indicates whether the specified System.String object is null, a System.String.Empty string or a System.String that only contains white space.
		/// </summary>
		/// <param name="value">A System.String reference.</param>
		/// <returns>true if the value parameter is null, an empty string ("") or contains only white space; otherwise, false.</returns>
		public static bool IsNullOrWhiteSpace(string value)
		{
			return StringUtilLight.IsNullOrWhiteSpace(value);
		}

		/// <summary>
		/// Indicates whether the specified System.String instance only contains white space.
		/// </summary>
		/// <param name="value">A System.String reference</param>
		/// <returns>True if the value only contains white space, false if not.</returns>
		public static bool IsOnlyWhiteSpace(string value)
		{
			return StringUtilLight.IsOnlyWhiteSpace(value);
		}

		/// <summary>
		/// Indicates whether the specified System.String object is null, a System.String.Empty string or a System.String that only contains white space.
		/// </summary>
		/// <param name="value">A System.String reference.</param>
		/// <returns>true if the value parameter is null, an empty string ("") or contains only whit espace; otherwise, false.</returns>
		[Obsolete("Use IsNullOrWhiteSpace instead.")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool IsNullOrBlank(string value)
		{
			return StringUtilLight.IsNullOrWhiteSpace(value);
		}

		#endregion

		#region Combiner methods

		/// <summary>
		/// Use this to combine infinite parts with a specified seperator
		/// </summary>
		/// <param name="seperator">Seperator</param>
		/// <param name="args">Elements to combine</param>
		/// <returns>Combined string with seperator if both a and b have a value</returns>
		public static string CombineWithSeperator(string seperator, params object[] args)
		{
			return StringUtilLight.CombineWithSeperator(seperator, args);
		}

		/// <summary>
		/// Use this to combine parts with a forward slash (eg. for URLs). When done all double sepeartors are removed.
		/// </summary>
		/// <param name="args">Elements to combine</param>
		/// <returns>Combined string with seperator if both a and b have a value</returns>
		public static string CombineWithForwardSlash(params object[] args)
		{
			return StringUtilLight.CombineWithForwardSlash(args);
		}

		/// <summary>
		/// Use this to combine infinite parts with a comma
		/// </summary>
		/// <param name="args">Elements to combine</param>
		/// <returns>Combined string with seperator if both a and b have a value</returns>
		public static string CombineWithComma(params object[] args)
		{
			return StringUtilLight.CombineWithComma(args);
		}

		/// <summary>
		/// Use this to combine infinite parts with a comma and space ', '
		/// </summary>
		/// <param name="args">Elements to combine</param>
		/// <returns>Combined string with seperator if both a and b have a value</returns>
		public static string CombineWithCommaSpace(params object[] args)
		{
			return StringUtilLight.CombineWithCommaSpace(args);
		}

		/// <summary>
		/// Use this to combine infinite parts with a space
		/// </summary>
		/// <param name="args">Elements to combine</param>
		/// <returns>Combined string with seperator if both a and b have a value</returns>
		public static string CombineWithSpace(params object[] args)
		{
			return StringUtilLight.CombineWithSpace(args);
		}

		/// <summary>
		/// Use this to combine 2 parts with a semicolon+space seperator
		/// string.Empty + "dummy" returns "dummy" (no seperator)
		/// "test" + "dummy" returns "test;[space]dummy"
		/// </summary>
		/// <param name="args">Elements to combine</param>
		/// <returns>Combined string with seperator if both a and b have a value</returns>
		public static string CombineWithSemicolon(params object[] args)
		{
			return StringUtilLight.CombineWithSemicolon(args);
		}

		/// <summary>
		/// Use this to combine infinite parts with a specified seperator (and additional seperator for the last item).
		/// </summary>
		/// <param name="seperator">The seperator text to add between the items.</param>
		/// <param name="seperatorLast">The seperator text to add before the last item.</param>
		/// <param name="args">The array of elements to combine.</param>
		/// <returns>Combined string with the specified seperators.</returns>
		public static string CombineWithAdditionalSeperator(string seperator, string seperatorLast, params object[] args)
		{
			return StringUtilLight.CombineWithAdditionalSeperator(seperator, seperatorLast, args);
		}

		#endregion

		#region String.Formating

		/// <summary>
		/// Replaces the format item in a specified <see cref="System.String"/> with the text equivalent of the value of a corresponding <see cref="System.Object"/> instance in a specified array.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An <see cref="System.Object"/> array containing zero or more objects to format.</param>
		/// <returns>A copy of <paramref name="format"/> in which the format items have been replaced by the <see cref="System.String"/> equivalent of the corresponding instances of <see cref="System.Object"/> in <paramref name="args"/>.</returns>
		public static string FormatWith(string format, params object[] args)
		{
			return StringUtilLight.FormatWith(format, args);
		}

		/// <summary>
		/// Replaces the format item in a specified <see cref="System.String"/> with the text equivalent of the value of a corresponding <see cref="System.Object"/> instance in a specified array.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="provider">An <see cref="System.IFormatProvider"/> that supplies culture-specific formatting information.</param>
		/// <param name="args">An <see cref="System.Object"/> array containing zero or more objects to format.</param>
		/// <returns>A copy of <paramref name="format"/> in which the format items have been replaced by the <see cref="System.String"/> equivalent of the corresponding instances of <see cref="System.Object"/> in <paramref name="args"/>.</returns>
		public static string FormatWith(string format, IFormatProvider provider, params object[] args)
		{
			return StringUtilLight.FormatWith(format, provider, args);
		}

		/// <summary>
		/// Replaces the format item in a specified <see cref="System.String"/> with the text equivalent of the value of a data-binding expression using a specified <see cref="System.Object"/> instance.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="arg0">An <see cref="System.Object"/> to format.</param>
		/// <returns>A copy of <paramref name="format"/> in which the format items have been replaced by the <see cref="System.String"/> equivalant of the corresponding instances of the data-binding expression using <paramref name="arg0"/>.</returns>
		public static string FormatWith(string format, object arg0)
		{
			return StringUtilLight.FormatWith(format, arg0);
		}

		/// <summary>
		/// Format a string using string.Format but with a fall back, if the arguments don't fit the format it won't throw an Exception
		/// </summary>
		/// <param name="format">Format</param>
		/// <param name="args">Arguments</param>
		/// <returns>
		/// The string.Format result, if it fails it concats the format with the arguments and prefixes it with: Invalid String Format:
		/// </returns>
		public static string FormatSafe(String format, params object[] args)
		{
			return StringUtilLight.FormatSafe(format, args);
		}

		/// <summary>
		/// Format a string using string.Format but with a fall back, if the arguments don't fit the format it won't throw an Exception
		/// </summary>
		/// <param name="format">Format</param>
		/// <param name="formatProvider">The format provider.</param>
		/// <param name="args">Arguments</param>
		/// <returns>
		/// The string.Format result, if it fails it concats the format with the arguments and prefixes it with: Invalid String Format:
		/// </returns>
		public static string FormatSafeWithProvider(String format, IFormatProvider formatProvider, params object[] args)
		{
			return StringUtilLight.FormatSafeWithProvider(format, formatProvider, args);
		}


		#endregion

		#endregion

		#region Array methods

		/// <summary>
		/// Convert a string Array to an int Array
		/// </summary>
		/// <param name="s">Input</param>
		/// <returns>Output</returns>
		public static int[] ToIntArray(string[] s)
		{
			int[] intArray = new int[s.Length];
			for (int index = 0; index < s.Length; index++)
			{
				intArray[index] = Convert.ToInt32(s[index]);
			}
			return intArray;
		}

		#endregion

		#region Casing methods

		/// <summary>
		/// Make whole string lower and make first letter upper
		/// </summary>
		/// <param name="input">String to have the first letter capitalized for</param>
		public static string TurnFirstToUpper(this string input)
		{
			return TurnFirstToUpper(input, true);
		}

		/// <summary>
		/// Make whole string lower and make first letter upper
		/// </summary>
		/// <param name="input">String to have the first letter capitalized for</param>
		/// <param name="applyToLower">Turn the rest of the string to Lower</param>
		public static string TurnFirstToUpper(this string input, bool applyToLower)
		{
			if (input.Length <= 1)
			{
				return input.ToUpper();
			}
			else
			{
				string temp = input.Substring(0, 1);

				if (applyToLower)
					temp = temp.ToUpper() + input.ToLower().Remove(0, 1);
				else
					temp = temp.ToUpper() + input.Remove(0, 1);

				return temp;
			}
		}

		/// <summary>
		/// Make whole string lower and make first letter upper
		/// </summary>
		/// <param name="input">String to have the first letter capitalized for</param>
		public static string TurnFirstToLower(this string input)
		{
			return TurnFirstToLower(input, true);
		}

		/// <summary>
		/// Make make first character lower
		/// </summary>
		/// <param name="input">String to have the first letter capitalized for</param>
		/// <param name="applyToLower">Turn the rest of the string to Upper</param>
		public static string TurnFirstToLower(this string input, bool applyToLower)
		{
			if (input.Length <= 1)
			{
				return input.ToLower();
			}
			else
			{
				string temp = input.Substring(0, 1);

				if (applyToLower)
					temp = temp.ToLower() + input.ToUpper().Remove(0, 1);
				else
					temp = temp.ToLower() + input.Remove(0, 1);

				return temp;
			}
		}

        /// <summary>
        /// Make the first two characters lower
        /// </summary>
        /// <param name="input">String to have the first two letters lower for</param>
        /// <param name="applyToLower">Turn the rest of the string to Upper</param>
        public static string TurnFirstTwoToLower(this string input, bool applyToLower)
        {
            if (input.Length <= 2)
            {
                return input.ToLower();
            }
            else
            {
                string temp = input.Substring(0, 2);

                if (applyToLower)
                    temp = temp.ToLower() + input.ToUpper().Remove(0, 2);
                else
                    temp = temp.ToLower() + input.Remove(0, 2);

                return temp;
            }
        }  

	    public static bool IsUpper(this string input)
	    {
	        for (int i = 0; i < input.Length; i++)
	        {
	            if (char.IsLower(input[i]))
	            {
	                return false;
	            }
	        }
	        return true;
	    }

	    #endregion

		#region Splitting methods

		/// <summary>
		/// Returns a string array that contains the substrings in this string that are delimited by elements of a specified string array. A parameter specifies whether to return empty array elements.
		/// </summary>
		/// <param name="input">The String to split.</param>
		/// <param name="seperator">The string that delimits the substrings in this string.</param>
		/// <param name="options">Specify System.StringSplitOptions.RemoveEmptyEntries to omit empty array elements from the array returned, or System.StringSplitOptions.None to include empty array elements in the array returned.</param>
		/// <returns>An array whose elements contain the substrings in this string that are delimited by the separator.</returns>
		public static string[] Split(this string input, string seperator, StringSplitOptions options)
		{
			return input.Split(new string[] { seperator }, options);
		}

		/// <summary>
		/// Returns a string array that contains the substrings in this string that are delimited by elements of a specified char array. A parameter specifies whether to return empty array elements.
		/// </summary>
		/// <param name="input">The String to split.</param>
		/// <param name="seperator">The char that delimits the substrings in this string.</param>
		/// <param name="options">Specify System.StringSplitOptions.RemoveEmptyEntries to omit empty array elements from the array returned, or System.StringSplitOptions.None to include empty array elements in the array returned.</param>
		/// <returns>An array whose elements contain the substrings in this string that are delimited by the separator.</returns>
		public static string[] Split(this string input, char seperator, StringSplitOptions options)
		{
			return input.Split(new char[] { seperator }, options);
		}

		/// <summary>
		/// Split a string in to it's seperate lines
		/// </summary>
		/// <param name="input">Text containing linebreaks</param>
		/// <returns>
		/// String Array with the lines
		/// </returns>
		public static string[] SplitLines(this string input)
		{
			return input.SplitLines(StringSplitOptions.RemoveEmptyEntries);
		}

		/// <summary>
		/// Split a string in to it's seperate lines
		/// </summary>
		/// <param name="input">Text containing linebreaks</param>
		/// <param name="options">The options.</param>
		/// <returns>
		/// String Array with the lines
		/// </returns>
		public static string[] SplitLines(this string input, StringSplitOptions options)
		{
			return input.Split(new string[] { "\r\n", "\r", "\n" }, options);
		}

		#endregion

		#region Formatting methods

		/// <summary>
		/// Converts a string to an HTML-encoded string
		/// </summary>
		/// <param name="input">The string to encode</param>
		/// <returns>
		/// An HTML-encoded string
		/// </returns>
		[Obsolete("Incorrect function name, use HtmlEncode instead.")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static string HtmlEntities(this string input)
		{
			return HttpUtility.HtmlEncode(input);
		}

		/// <summary>
		/// Converts a string to an HTML-encoded string
		/// </summary>
		/// <param name="input">The string to encode</param>
		/// <returns>
		/// An HTML-encoded string
		/// </returns>
		public static string HtmlEncode(this string input)
		{
			return HttpUtility.HtmlEncode(input);
		}

		/// <summary>
		/// Converts a string to an HTML-decoded string
		/// </summary>
		/// <param name="input">The string to decode</param>
		/// <returns>
		/// An HTML-decoded string
		/// </returns>
		public static string HtmlDecode(this string input)
		{
			return HttpUtility.HtmlDecode(input);
		}

		/// <summary>
		/// Converts a string with BBCode formatting to an HTML-encoded string
		/// </summary>
		/// <param name="input">The string with BBCode formatting to encode</param>
		/// <returns>
		/// An HTML-encoded string
		/// </returns>
		public static string BBCode(this string input)
		{
			return Dionysos.Text.BBCodeHelper.Format(input);
		}

		/// <summary>
		/// Converts a string with Textile formatting to an HTML-encoded string
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns>
		/// An HTML-encoded string
		/// </returns>
		public static string Textile(this string input)
		{
			return Textile(input, false, false);
		}

		/// <summary>
		/// Converts a string with Textile formatting to an HTML-encoded string
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="formatInline">If set to <c>true</c> only formats inline styles.</param>
		/// <param name="restricted">If set to <c>true</c> uses restricted styles.</param>
		/// <returns>
		/// An HTML-encoded string
		/// </returns>
		public static string Textile(this string input, bool formatInline = false, bool restricted = false)
		{
			// Create formatter and outputter
			StringBuilderOutputter outputter = new StringBuilderOutputter();
			CustomTextileFormatter formatter = new CustomTextileFormatter(outputter, formatInline, restricted);

			// Format input
			formatter.Format(input);

			// Get formatted text
			string output = outputter.GetFormattedText();

			// Post process output
			output = output.TextilePostProcess();

			return output;
		}

		/// <summary>
		/// Converts a string with basic Textile formatting to an HTML-encoded string
		/// </summary>
		/// <param name="input">The string with Textile formatting to encode</param>
		/// <param name="formatInline">If set to <c>true</c> only formats inline styles.</param>
		/// <returns>
		/// An HTML-encoded string
		/// </returns>
		public static string TextileRestricted(this string input, bool formatInline = false)
		{
			// Replace some HTML entities (do not replace double quotes, because of Textile markup) - this is to ignore HTML markup
			Dictionary<string, string> htmlEntities = new Dictionary<string, string>()
			{
				{ "&", "&amp;" },
				{ "<", "&lt;" },
				{ ">", "&gt;" }
			};
			foreach (KeyValuePair<string, string> htmlEntity in htmlEntities)
			{
				input = input.Replace(htmlEntity.Key, htmlEntity.Value);
			}

			return StringUtil.Textile(input, formatInline, true);
		}

		/// <summary>
		/// Post processed the Textile formatted text.
		/// </summary>
		/// <param name="formattedText">The formatted text.</param>
		/// <returns>
		/// The post processed text.
		/// </returns>
		private static string TextilePostProcess(this string formattedText)
		{
			// Fix lists that are within paragraphs
			return Regex.Replace(formattedText, @"<br />\s*(<[u|o]l(.*?)</[u|o]l>)\s*</p>", "</p>\r\n$1", RegexOptions.Compiled | RegexOptions.Singleline);
		}

		/// <summary>
		/// Adds an ellipsis (...) to a string, it is too long. Uses default ending characters and does not break words.
		/// </summary>
		/// <param name="input">The string to parse.</param>
		/// <param name="maxLength">The maximum length of the string, including the ellipsis.</param>
		/// <returns>
		/// An ellipsed string of the specified length.
		/// </returns>
		public static string AutoEllipse(this string input, int maxLength)
		{
			return AutoEllipse(input, maxLength, -1);
		}

		/// <summary>
		/// Adds an ellipsis (...) to a string, it is too long. Uses default ending characters.
		/// </summary>
		/// <param name="input">The string to parse.</param>
		/// <param name="maxLength">The maximum length of the string, including the ellipsis.</param>
		/// <param name="breakAfter">Breaks words if no ending characters is found within the specified length.</param>
		/// <returns>An ellipsed string of the specified length.</returns>
		public static string AutoEllipse(this string input, int maxLength, int breakAfter)
		{
			return AutoEllipse(input, maxLength, breakAfter, new char[] { ' ', '.', ',', '!', '?', ';', ':', '&' });
		}

		/// <summary>
		/// Adds an ellipsis (...) to a string, it is too long.
		/// </summary>
		/// <param name="input">The string to parse.</param>
		/// <param name="maxLength">The maximum length of the string, including the ellipsis.</param>
		/// <param name="breakAfter">Breaks words if no ending characters is found within the specified length.</param>
		/// <param name="endingCharacters">The ending characters to add the ellipsis after.</param>
		/// <returns>An ellipsed string of the specified length.</returns>
		public static string AutoEllipse(this string input, int maxLength, int breakAfter, char[] endingCharacters)
		{
			// Remove whitespace
			string toReturn = input.Trim();

			// Add ellipsis if input is too long
			if (toReturn.Length > maxLength)
			{
				// Set length to search (remove 3 characters for ellipsis)
				int length = maxLength - 3;

				// Set breakAfter to length of string, if not set (negative)
				if (breakAfter < 0)
					breakAfter = length;

				// Try to get the ellipse after index
				int ellipseAfter = toReturn.LastIndexOfAny(endingCharacters, length, breakAfter);

				// Get correct index
				if (ellipseAfter > 0)
					length = ellipseAfter;

				// Remove excess and ending characters
				toReturn = toReturn.Remove(length).TrimEnd(endingCharacters);

				// Add elipsis
				toReturn += "...";
			}

			return toReturn;
		}

		/// <summary>
		/// Highlights the specified input.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="format">The format ({0} for the highlighted keyword).</param>
		/// <param name="keywords">The keywords.</param>
		/// <returns>The highlighted input.</returns>
		public static string Highlight(this string input, string format, params string[] keywords)
		{
			StringBuilder highlightedInput = new StringBuilder(input.Length);

			// Create pattern and escape keywords
			string pattern = String.Join("|", keywords.Select(a => Regex.Escape(a.ReplaceDiacritics())).ToArray());

			// Match all keywords
			int index = 0;
			foreach (Match match in Regex.Matches(input.ReplaceDiacritics(), pattern, RegexOptions.IgnoreCase))
			{
				highlightedInput.Append(input.Substring(index, match.Index - index));
				highlightedInput.AppendFormat(format, input.Substring(match.Index, match.Length));
				index = match.Index + match.Length;
			}

			highlightedInput.Append(input.Substring(index));

			return highlightedInput.ToString();
		}

		/// <summary>
		/// Format a person initials, bng, becomes: B.N.G.
		/// </summary>
		/// <param name="initials">The initials to format.</param>
		/// <returns>The formatted initials.</returns>
		public static string FormatInitials(this string initials)
		{
			string output = String.Empty;

			foreach (char initial in initials.RemoveAllNonLetterCharacters().ToCharArray())
			{
				output += initial.ToString().ToUpper() + ".";
			}

			return output;
		}

		/// <summary>
		/// Format a String in PascalCase to a String with seperate words (eg. "PascalCase" becomes "Pascal Case").
		/// </summary>
		/// <param name="input">The String to format.</param>
		/// <returns>The formatted PascalCase String.</returns>
		public static string FormatPascalCase(this string input)
		{
			// return Regex.Replace(input, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", @" $0", RegexOptions.Compiled);

			// GK After testing implemented this code below instead of the RegEx
			// The Regex is ca. 6 times slower (see documentation at end of this method for more info)
			// http://stackoverflow.com/questions/272633/add-spaces-before-capital-letters

			if (String.IsNullOrEmpty(input)) return String.Empty;

			StringBuilder newText = new StringBuilder(input.Length * 2);
			newText.Append(input[0]);
			for (int i = 1; i < input.Length; i++)
			{
				if (Char.IsUpper(input[i]) && !Char.IsUpper(input[i - 1]))
				{
					// Add space before upper case character (if previous is not also upper case)
					newText.Append(' ');
				}
				else if (i < input.Length - 1 && Char.IsUpper(input[i]) && !Char.IsUpper(input[i + 1]))
				{
					// Add space after consecutive upper case characters
					newText.Append(' ');
				}

				newText.Append(input[i]);
			}

			return newText.ToString();

			#region Documentation

			// Previous an new method work the same:
			//Debug.WriteLine("TheLoneRanger" + " - " + "TheLoneRanger".FormatPascalCase());
			//Debug.WriteLine("MountMᶜKinleyNationalPark" + " - " + "MountMᶜKinleyNationalPark".FormatPascalCase());
			//Debug.WriteLine("ElÁlamoTejano" + " - " + "ElÁlamoTejano".FormatPascalCase());
			//Debug.WriteLine("TheÆvarArnfjörðBjarmason" + " - " + "TheÆvarArnfjörðBjarmason".FormatPascalCase());
			//Debug.WriteLine("IlCaffèMacchiato" + " - " + "IlCaffèMacchiato".FormatPascalCase());
			//Debug.WriteLine("Misterǅenanǈubović" + " - " + "Misterǅenanǈubović".FormatPascalCase());
			//Debug.WriteLine("OleKingHenryⅧ" + " - " + "OleKingHenryⅧ".FormatPascalCase());
			//Debug.WriteLine("CarlosⅤºElEmperador" + " - " + "CarlosⅤºElEmperador".FormatPascalCase());
			//Debug.WriteLine("TheNOXStartATDawnAWeekAgo" + " - " + "TheNOXStartATDawnAWeekAgo".FormatPascalCase());

			//Debug.WriteLine("TheLoneRanger" + " - " + "TheLoneRanger".FormatPascalCase2());
			//Debug.WriteLine("MountMᶜKinleyNationalPark" + " - " + "MountMᶜKinleyNationalPark".FormatPascalCase2());
			//Debug.WriteLine("ElÁlamoTejano" + " - " + "ElÁlamoTejano".FormatPascalCase2());
			//Debug.WriteLine("TheÆvarArnfjörðBjarmason" + " - " + "TheÆvarArnfjörðBjarmason".FormatPascalCase2());
			//Debug.WriteLine("IlCaffèMacchiato" + " - " + "IlCaffèMacchiato".FormatPascalCase2());
			//Debug.WriteLine("Misterǅenanǈubović" + " - " + "Misterǅenanǈubović".FormatPascalCase2());
			//Debug.WriteLine("OleKingHenryⅧ" + " - " + "OleKingHenryⅧ".FormatPascalCase2());
			//Debug.WriteLine("TheNOXStartATDawnAWeekAgo" + " - " + "TheNOXStartATDawnAWeekAgo".FormatPascalCase2());

			//TheLoneRanger - The Lone Ranger
			//MountMᶜKinleyNationalPark - Mount Mᶜ Kinley National Park
			//ElÁlamoTejano - El Álamo Tejano
			//TheÆvarArnfjörðBjarmason - The Ævar Arnfjörð Bjarmason
			//IlCaffèMacchiato - Il Caffè Macchiato
			//Misterǅenanǈubović - Misterǅenanǈubović
			//OleKingHenryⅧ - Ole King HenryⅧ
			//CarlosⅤºElEmperador - CarlosⅤº El Emperador
			//TheNOXStartATDawnAWeekAgo - The NOX Start AT Dawn A Week Ago
			//TheLoneRanger - The Lone Ranger
			//MountMᶜKinleyNationalPark - Mount Mᶜ Kinley National Park
			//ElÁlamoTejano - El Álamo Tejano
			//TheÆvarArnfjörðBjarmason - The Ævar Arnfjörð Bjarmason
			//IlCaffèMacchiato - Il Caffè Macchiato
			//Misterǅenanǈubović - Misterǅenanǈubović
			//OleKingHenryⅧ - Ole King HenryⅧ
			//TheNOXStartATDawnAWeekAgo - The NOX Start AT Dawn A Week Ago

			#endregion
		}

		/// <summary>
		/// Converts a String to PascalCase (eg. "pascal case" and "pascal-case" become "PascalCase").
		/// </summary>
		/// <param name="input">The String to convert.</param>
		/// <returns>The converted String.</returns>
		public static string ToPascalCase(string input)
		{
			return StringUtilLight.ToPascalCase(input);
		}

		/// <summary>
		/// Replaces the format item in a specified <see cref="System.String"/> with the text equivalent of the value of a data-binding expression using a specified <see cref="System.Object"/> instance.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="provider">An <see cref="System.IFormatProvider"/> that supplies culture-specific formatting information.</param>
		/// <param name="arg0">An <see cref="System.Object"/> to format.</param>
		/// <returns>A copy of <paramref name="format"/> in which the format items have been replaced by the <see cref="System.String"/> equivalant of the corresponding instances of the data-binding expression using <paramref name="arg0"/>.</returns>
		public static string FormatWith(this string format, IFormatProvider provider, object arg0)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}

			List<object> values = new List<object>();
			string rewrittenFormat = Regex.Replace(format, @"(?<Openings>\{)+(?<Property>[\w\.\[\]]+)(?<Length>,[^}]+)?(?<Format>:[^}]+)?(?<Closings>\})+", delegate(Match m)
			{
				int openings = m.Groups["Openings"].Captures.Count;
				int closings = m.Groups["Closings"].Captures.Count;

				if (openings > closings || openings % 2 == 0)
				{
					// Match is escaped, return original format string
					return m.Value;
				}
				else
				{
					string property = m.Groups["Property"].Value;

					if (property == "0")
					{
						// To add support for a single indexed format
						values.Add(arg0);
					}
					else
					{
						try
						{
							values.Add(DataBinder.Eval(arg0, property));
						}
						catch (HttpException e)
						{
							throw new FormatException(null, e);
						}
					}

					return new string('{', openings) + (values.Count - 1) + m.Groups["Length"].Value + m.Groups["Format"].Value + new string('}', closings);
				}
			}, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

			return rewrittenFormat.FormatSafeWithProvider(provider, values.ToArray());
		}

		/// <summary>
		/// Remove Trailing Zeroes
		/// </summary>
		/// <param name="input">String to work with</param>
		/// <returns></returns>
		public static string RemoveTrailingZeroes(this string input)
		{
			Regex reg1 = new Regex("\\.\\d*(?<1>0+)[^\\d]*$", RegexOptions.IgnoreCase);

			Match m = reg1.Match(input);
			if (m.Success)
			{
				input = input.Replace(m.Groups["1"].Value, "");
				Regex reg2 = new Regex("(?<1>\\.)[^\\d]*$", RegexOptions.IgnoreCase);
				m = reg2.Match(input);

				if (m.Success)
				{
					input = input.Replace(".", "");
				}
				Regex reg3 = new Regex("\\d", RegexOptions.IgnoreCase);
				m = reg3.Match(input);
				if (!m.Success)
				{
					input = "0" + input;

				}

			}
			if (input.StartsWith("."))
				input = "0" + input;

			return input;
		}

		/// <summary>
		/// Adds the tabs.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="tabs">The tabs.</param>
		/// <returns></returns>
		public static string AddTabs(this string input, int tabs)
		{
			for (int i = 0; i < tabs; i++)
			{
				input = "\t" + input;
			}
			return input;
		}

		#endregion

		#region Encoding Methods

		/// <summary>
		/// Convert a string in the Encoding.Default to ANSI
		/// </summary>
		/// <param name="input">String in the Default environment encoding</param>
		/// <returns>String in ANSI encoding</returns>
		public static string ToAnsiEncoding(this string input)
		{
			// Create two different encodings.
			Encoding ansi = Encoding.GetEncoding(1252);
			Encoding defaultEncoding = Encoding.Default;

			// Convert the string into a byte[].
			byte[] defaultEncodingBytes = defaultEncoding.GetBytes(input);

			// Perform the conversion from one encoding to the other.
			byte[] asciiBytes = Encoding.Convert(defaultEncoding, ansi, defaultEncodingBytes);

			// Convert the new byte[] into a char[] and then into a string.
			// This is a slightly different approach to converting to illustrate
			// the use of GetCharCount/GetChars.
			char[] asciiChars = new char[ansi.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
			ansi.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
			string asciiString = new string(asciiChars);

			return asciiString;
		}

		#endregion

		#region Replacement methods

		/// <summary>
		/// Replaces all occurrences of a specified System.String in this instance, with another specified System.String.
		/// </summary>
		/// <param name="original">The original System.String object.</param>
		/// <param name="oldValue">A System.String to be replaced.</param>
		/// <param name="newValue">A System.String to replace all occurrences of oldValue.</param>
		/// <param name="comparisionType">One of the System.StringComparison values.</param>
		/// <returns>A System.String equivalent to this instance but with all instances of oldValue replaced with newValue.</returns>
		public static string Replace(this string original, string oldValue, string newValue, StringComparison comparisionType)
		{
			if (!String.IsNullOrEmpty(original) &&
				!String.IsNullOrEmpty(oldValue))
			{
				int index = -1;
				int lastIndex = 0;

				StringBuilder buffer = new StringBuilder(original.Length);
				while ((index = original.IndexOf(oldValue, index + 1, comparisionType)) >= 0)
				{
					buffer.Append(original, lastIndex, index - lastIndex);
					buffer.Append(newValue);
					lastIndex = index + oldValue.Length;
				}
				buffer.Append(original, lastIndex, original.Length - lastIndex);

				original = buffer.ToString();
			}

			return original;
		}

		/// <summary>
		/// Replaces the value at the beginning of the string.
		/// </summary>
		/// <param name="original">The original.</param>
		/// <param name="oldValue">The old value.</param>
		/// <param name="newValue">The new value.</param>
		/// <returns>
		/// The string with the replaced value.
		/// </returns>
		public static string ReplaceLeft(this string original, string oldValue, string newValue)
		{
			return original.ReplaceLeft(oldValue, newValue, StringComparison.CurrentCulture);
		}

		/// <summary>
		/// Replaces the value at the beginning of the string.
		/// </summary>
		/// <param name="original">The original.</param>
		/// <param name="oldValue">The old value.</param>
		/// <param name="newValue">The new value.</param>
		/// <param name="comparisonType">The type of the comparison.</param>
		/// <returns>
		/// The string with the replaced value.
		/// </returns>
		public static string ReplaceLeft(this string original, string oldValue, string newValue, StringComparison comparisonType)
		{
			if (original.StartsWith(oldValue, comparisonType))
			{
				return original.Substring(oldValue.Length).Insert(0, newValue);
			}
			else
			{
				return original;
			}
		}

		/// <summary>
		/// Replaces the value at the end of the string.
		/// </summary>
		/// <param name="original">The original.</param>
		/// <param name="oldValue">The old value.</param>
		/// <param name="newValue">The new value.</param>
		/// <returns>
		/// The string with the replaced value.
		/// </returns>
		public static string ReplaceRight(this string original, string oldValue, string newValue)
		{
			return original.ReplaceRight(oldValue, newValue, StringComparison.CurrentCulture);
		}

		/// <summary>
		/// Replaces the value at the end of the string.
		/// </summary>
		/// <param name="original">The original.</param>
		/// <param name="oldValue">The old value.</param>
		/// <param name="newValue">The new value.</param>
		/// <param name="comparisonType">The type of the comparison.</param>
		/// <returns>
		/// The string with the replaced value.
		/// </returns>
		public static string ReplaceRight(this string original, string oldValue, string newValue, StringComparison comparisonType)
		{
			if (original.EndsWith(oldValue, comparisonType))
			{
				return original.Substring(0, original.Length - oldValue.Length) + newValue;
			}
			else
			{
				return original;
			}
		}

		/// <summary>
		/// Replaces are occurences of the pattern with the replacement.
		/// The pattern is searched for in a case-insenstive way.
		/// </summary>
		/// <param name="original">String to have the pattern replaced in</param>
		/// <param name="pattern">String to replace with the Replacement</param>
		/// <param name="replacement">Replacement of the Pattern</param>
		/// <returns>The string with all patterns replaced with the replacement</returns>
		[Obsolete("Use Replace with StringComparison.OrdinalIgnoreCase: twice as fast and less bugs (NullReferenceException/invariant string handling)!")]
		public static string ReplaceCaseInsensitive(this string original, string pattern, string replacement)
		{
			if (!String.IsNullOrEmpty(original) &&
				!String.IsNullOrEmpty(pattern))
			{
				int count = 0, position0 = 0, position1 = 0;
				string upperString = original.ToUpperInvariant(), upperPattern = pattern.ToUpperInvariant();
				int inc = (original.Length / pattern.Length) * (replacement.Length - pattern.Length);

				char[] chars = new char[original.Length + System.Math.Max(0, inc)];
				while ((position1 = upperString.IndexOf(upperPattern, position0)) != -1)
				{
					for (int i = position0; i < position1; ++i) chars[count++] = original[i];
					for (int i = 0; i < replacement.Length; ++i) chars[count++] = replacement[i];
					position0 = position1 + pattern.Length;
				}

				if (position0 == 0) return original;

				for (int i = position0; i < original.Length; ++i) chars[count++] = original[i];

				return new string(chars, 0, count);
			}
			else
			{
				return original;
			}
		}

		/// <summary>
		/// Replace the first occurrence of oldValue with newValue using current culture StringComparison.
		/// </summary>
		/// <param name="original">The original string.</param>
		/// <param name="oldValue">A System.String to be replaced.</param>
		/// <param name="newValue">A System.String to replace with.</param>
		/// <returns>String with first oldValue replaced with newValue.</returns>
		public static string ReplaceFirstOccurrence(this string original, string oldValue, string newValue)
		{
			return ReplaceFirstOccurrence(original, oldValue, newValue, StringComparison.CurrentCulture);
		}

		/// <summary>
		/// Replace the first occurrence of oldValue with newValue using the specified StringComparison.
		/// </summary>
		/// <param name="original">The original string.</param>
		/// <param name="oldValue">A System.String to be replaced.</param>
		/// <param name="newValue">A System.String to replace with.</param>
		/// <param name="comparisonType">The StringComparison to use.</param>
		/// <returns>String with first oldValue replaced with newValue.</returns>
		public static string ReplaceFirstOccurrence(this string original, string oldValue, string newValue, StringComparison comparisonType)
		{
			int location = original.IndexOf(oldValue, comparisonType);
			if (location < 0)
			{
				return original;
			}
			else
			{
				return original.Remove(location, oldValue.Length).Insert(location, newValue);
			}
		}

		/// <summary>
		/// Replace the last occurrence of oldValue with newValue using current culture StringComparison.
		/// </summary>
		/// <param name="original">The original string.</param>
		/// <param name="oldValue">A System.String to be replaced.</param>
		/// <param name="newValue">A System.String to replace with.</param>
		/// <returns>String with last oldValue replaced with newValue.</returns>
		public static string ReplaceLastOccurrence(this string original, string oldValue, string newValue)
		{
			return ReplaceLastOccurrence(original, oldValue, newValue, StringComparison.CurrentCulture);
		}

		/// <summary>
		/// Replace the last occurrence of oldValue with newValue using the specified StringComparison.
		/// </summary>
		/// <param name="original">The original string.</param>
		/// <param name="oldValue">A System.String to be replaced.</param>
		/// <param name="newValue">A System.String to replace with.</param>
		/// <param name="comparisonType">The StringComparison to use.</param>
		/// <returns>String with last oldValue replaced with newValue.</returns>
		public static string ReplaceLastOccurrence(this string original, string oldValue, string newValue, StringComparison comparisonType)
		{
			int location = original.LastIndexOf(oldValue, comparisonType);
			if (location < 0)
			{
				return original;
			}
			else
			{
				return original.Remove(location, oldValue.Length).Insert(location, newValue);
			}
		}

		/// <summary>
		/// Removes spaces from the specified string using String.Replace
		/// </summary>
		/// <param name="original">The original string</param>
		/// <returns>A System.String instance containing no spaces</returns>
		public static string RemoveSpaces(this string original)
		{
			return original.Replace(" ", String.Empty);
		}

		/// <summary>
		/// Replace all linebreaks (\r\n, \r or \n) with br-tags
		/// </summary>
		/// <param name="input">Text containing linebreaks</param>
		/// <returns>Linkbreaks replaced by br-tags</returns>
		public static string ReplaceLineBreakWithBR(this string input)
		{
			return Regex.Replace(input, "(\r\n|\n\r|\r|\n)", "<br />");
		}

		/// <summary>
		/// Replace all linebreaks with a definable string
		/// </summary>
		/// <param name="input">Text containing linebreaks</param>
		/// <param name="replaceWith">String to replace the linebreaks with</param>
		/// <returns>Linkbreaks replaced by br-tags</returns>
		public static string ReplaceLineBreaks(this string input, string replaceWith)
		{
			return Regex.Replace(input, "(\r\n|\r|\n)", replaceWith);
		}

		#endregion

		#region Remove methods

		/// <summary>
		/// Removes the value from the left side (beginning) of the string.
		/// </summary>
		/// <param name="input">The input string to remove the value from.</param>
		/// <param name="value">The value to remove.</param>
		/// <returns>The string with the value removed from the left side.</returns>
		public static string RemoveLeft(this string input, string value)
		{
			return input.RemoveLeft(value, StringComparison.CurrentCulture);
		}

		/// <summary>
		/// Removes the value from the left side (beginning) of the string.
		/// </summary>
		/// <param name="input">The input string to remove the value from.</param>
		/// <param name="value">The value to remove.</param>
		/// <param name="comparisonType">Type of the comparison.</param>
		/// <returns>
		/// The string with the value removed from the left side.
		/// </returns>
		public static string RemoveLeft(this string input, string value, StringComparison comparisonType)
		{
			if (input.StartsWith(value, comparisonType))
			{
				return input.Substring(value.Length);
			}
			else
			{
				return input;
			}
		}

		/// <summary>
		/// Removes the value from the right side (end) of the string.
		/// </summary>
		/// <param name="input">The input string to remove the value from.</param>
		/// <param name="value">The value to remove.</param>
		/// <returns>The string with the value removed from the right side.</returns>
		public static string RemoveRight(this string input, string value)
		{
			return input.RemoveRight(value, StringComparison.CurrentCulture);
		}

		/// <summary>
		/// Removes the value from the right side (end) of the string.
		/// </summary>
		/// <param name="input">The input string to remove the value from.</param>
		/// <param name="value">The value to remove.</param>
		/// <param name="comparisonType">Type of the comparison.</param>
		/// <returns>
		/// The string with the value removed from the right side.
		/// </returns>
		public static string RemoveRight(this string input, string value, StringComparison comparisonType)
		{
			if (input.EndsWith(value, comparisonType))
			{
				return input.Substring(0, input.Length - value.Length);
			}
			else
			{
				return input;
			}
		}

		/// <summary>
		/// Truncates the specified value.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="maxLength">The maximum length of the value.</param>
		/// <returns>
		/// The value truncated to the maximum length.
		/// </returns>
		public static string Truncate(this string value, int maxLength)
		{
			return value.Length <= maxLength ? value : value.Substring(0, maxLength);
		}

		#endregion

		#region Search methods

		/// <summary>
		/// Get the index of the first occurance of UpperCase character
		/// </summary>
		/// <param name="str">String to search in</param>
		/// <returns>Index of the first uppercase character, or -1 if not found</returns>
		public static int IndexOfFirstUpper(this string str)
		{
			int i;
			if (!string.IsNullOrEmpty(str))
			{
				bool found = false;
				for (i = 0; i < str.Length; i++)
				{
					if (char.IsUpper(str[i]))
					{
						found = true;
						break;
					}
				}

				if (!found)
				{
					i = -1;
				}
			}
			else
			{
				i = -1;
			}
			return i;
		}

		/// <summary>
		/// Count the amount of  times a certain character is included in the string
		/// </summary>
		/// <param name="input">String to count the character is</param>
		/// <param name="c">Character to count occurences of</param>
		/// <returns>Amount of occurences found</returns>
		public static int CountOccurencesOfChar(this string input, char c)
		{
			int retval = 0;
			for (int i = 0; i < input.Length; i++)
				if (c == input[i])
					retval++;
			return retval;
		}

		/// <summary>
		/// Counts the amount of times a certain string is included in the string
		/// </summary>
		/// <param name="input">String to count the occurences in</param>
		/// <param name="str">String to count occurences of</param>
		/// <returns>Amount of occurences found</returns>
		public static int CountOccurencesOfString(this string input, string str)
		{
			string[] strings = input.Split(str, StringSplitOptions.None);
			return strings.Length - 1;
		}

		/// <summary>
		/// Get the index of the first occurance of Non Numeric
		/// </summary>
		/// <param name="str">String to search in</param>
		/// <returns>Index of the first Non Numeric character, or -1 if not found</returns>
		public static int IndexOfNonNumeric(string str)
		{
			int i;
			if (!string.IsNullOrEmpty(str))
			{
				bool found = false;
				for (i = 0; i < str.Length; i++)
				{
					if (!char.IsNumber(str[i]))
					{
						found = true;
						break;
					}
				}

				if (!found)
				{
					i = -1;
				}
			}
			else
			{
				i = -1;
			}
			return i;
		}

		/// <summary>
		/// Get the index of the first occurance of nonLowerCase character
		/// </summary>
		/// <param name="str">String to search in</param>
		/// <returns>Index of the first uppercase character, or -1 if not found</returns>
		public static int IndexOfFirstNonLower(this string str)
		{
			int i;
			if (!string.IsNullOrEmpty(str))
			{
				bool found = false;
				for (i = 0; i < str.Length; i++)
				{
					if (!char.IsLower(str[i]))
					{
						found = true;
						break;
					}
				}

				if (!found)
				{
					i = -1;
				}
			}
			else
			{
				i = -1;
			}
			return i;
		}

		/// <summary>
		/// Gets all characters before first occurence of the specified value.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="valueToSearchFor">The value to search for.</param>
		/// <param name="returnEmptyString">If set to <c>true</c> returns an empty string if not found.</param>
		/// <returns>
		/// The characters before the first occurence of the specified value.
		/// </returns>
		public static string GetAllBeforeFirstOccurenceOf(this string input, char valueToSearchFor, bool returnEmptyString)
		{
			return GetAllBeforeFirstOccurenceOf(input, valueToSearchFor.ToString(), returnEmptyString);
		}

		/// <summary>
		/// Gets all characters before first occurence of of the specified value.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="valueToSearchFor">The value to search for.</param>
		/// <param name="returnEmptyString">If set to <c>true</c> returns an empty string if not found.</param>
		/// <returns>
		/// The characters before the first occurence of the specified value.
		/// </returns>
		public static string GetAllBeforeFirstOccurenceOf(this string input, string valueToSearchFor, bool returnEmptyString)
		{
			int indexOfSearched = input.IndexOf(valueToSearchFor);

			if (indexOfSearched > -1)
			{
				return input.Substring(0, indexOfSearched);
			}
			else
			{
				return returnEmptyString ? String.Empty : input;
			}
		}

		/// <summary>
		/// Gets all characters after first occurence of of the specified value.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="valueToSearchFor">The value to search for.</param>
		/// <returns>
		/// The characters after the first occurence of the specified value.
		/// </returns>
		public static string GetAllAfterFirstOccurenceOf(this string input, char valueToSearchFor)
		{
			return GetAllAfterFirstOccurenceOf(input, valueToSearchFor, true);
		}

		/// <summary>
		/// Gets all characters after first occurence of of the specified value.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="valueToSearchFor">The value to search for.</param>
		/// <param name="returnEmptyString">If set to <c>true</c> returns an empty string if not found.</param>
		/// <returns>
		/// The characters after the first occurence of the specified value.
		/// </returns>
		public static string GetAllAfterFirstOccurenceOf(this string input, char valueToSearchFor, bool returnEmptyString)
		{
			return GetAllAfterFirstOccurenceOf(input, valueToSearchFor.ToString(), returnEmptyString);
		}

		/// <summary>
		/// Gets all characters after first occurence of of the specified value.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="valueToSearchFor">The value to search for.</param>
		/// <returns>
		/// The characters after the first occurence of the specified value.
		/// </returns>
		public static string GetAllAfterFirstOccurenceOf(this string input, string valueToSearchFor)
		{
			return GetAllAfterFirstOccurenceOf(input, valueToSearchFor, true);
		}

		/// <summary>
		/// Gets all characters after first occurence of of the specified value.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="valueToSearchFor">The value to search for.</param>
		/// <param name="returnEmptyString">If set to <c>true</c> returns an empty string if not found.</param>
		/// <returns>
		/// The characters after the first occurence of the specified value.
		/// </returns>
		public static string GetAllAfterFirstOccurenceOf(this string input, string valueToSearchFor, bool returnEmptyString)
		{
			int indexOfSearched = input.IndexOf(valueToSearchFor);

			if (indexOfSearched > -1)
			{
				return input.Substring(indexOfSearched + valueToSearchFor.Length);
			}
			else
			{
				return returnEmptyString ? String.Empty : input;
			}
		}

		/// <summary>
		/// Reports the index of the first occurrence of the specified System.String in this instance or the default value.
		/// </summary>
		/// <param name="input">The System.String to search in.</param>
		/// <param name="value">The System.String to seek.</param>
		/// <param name="defaultValue">The default value to return if the value is not found.</param>
		/// <returns>The zero-based index position of value if that string is found, or the default value if it is not. If value is System.String.Empty, the return value is 0.</returns>
		public static int IndexOfOrDefault(this string input, string value, int defaultValue)
		{
			int index = input.IndexOf(value);

			return index < 0 ? defaultValue : index;
		}

		#endregion

		#region Stripping and trimming methods

		/// <summary>
		/// Remove HTML code from a string
		/// </summary>
		/// <param name="input">String containing HTML code</param>
		/// <returns>String without HTML code</returns>
		public static string StripHtml(this string input)
		{
            return StringUtilLight.StripHtml(input);
		}

		/// <summary>
		/// Filters the string to only contain the specified characters or removes the specified characters.
		/// </summary>
		/// <param name="input">The string to filter.</param>
		/// <param name="remove">If true, removes the specified characters; otherwise the output contains only the specified characters.</param>
		/// <param name="characters">The characters to filter or remove.</param>
		/// <returns>The string with the specified characters filtered or removed.</returns>
		public static string Filter(this string input, bool remove, params char[] characters)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}

			if (characters == null)
			{
				throw new ArgumentNullException("characters");
			}

			StringBuilder output = new StringBuilder(input.Length);
			foreach (char c in input)
			{
				if (characters.Contains(c) == remove)
				{
					output.Append(c);
				}
			}

			return output.ToString();
		}

		/// <summary>
		/// Removes all non-letter characters from the string.
		/// </summary>
		/// <param name="input">The string to remove non-letter characters from.</param>
		/// <returns>The string containing only letter characters.</returns>
		public static string RemoveAllNonLetterCharacters(this string input)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}

			StringBuilder output = new StringBuilder(input.Length);
			foreach (char c in input)
			{
				if (Char.IsLetter(c))
				{
					output.Append(c);
				}
			}

			return output.ToString();
		}

		/// <summary>
		/// Removes all non-digit characters from the string.
		/// </summary>
		/// <param name="input">The string to remove non-digit characters from.</param>
		/// <returns>The string containing only digit characters.</returns>
		public static string RemoveAllNonDigitCharacters(this string input)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}

			StringBuilder output = new StringBuilder(input.Length);
			foreach (char c in input)
			{
				if (Char.IsDigit(c))
				{
					output.Append(c);
				}
			}

			return output.ToString();
		}

		/// <summary>
		/// Returns the inputted string with the all lowercase characters before the first UpperCase character.
		/// </summary>
		/// <example>lblName returns Name</example>
		/// <returns>All Characters after first UpperCase Character</returns>
		public static string RemoveLeadingLowerCaseCharacters(this string input)
		{
			int upperCase = StringUtil.IndexOfFirstNonLower(input);
			string retval = String.Empty;

			if (upperCase >= 0)
			{
				retval = input.Substring(upperCase);
			}

			return retval;
		}

        /// <summary>
        /// Returns the input string without any non UTF characters.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string RemoveNonUTFCharacters(this string input)
        {
            char ch;
            for (int i = 0; i < input.Length; i++)
            {
                ch = input[i];

                // http://www.ascii.cl/htmlcodes.htm
                if ((ch > 0x00FD || ch < 0x001F) && ch != '\t' && ch != '\n' && ch != '\r' && !(ch > 8210 && ch < 8483))
                {
                    input = input.Replace(ch.ToString(), string.Empty);
                }                    
            }
            return input;
        }

		/// <summary>
		/// Create a safe URL part, not a full url but a addition
		/// </summary>
		/// <returns>Safe Url</returns>
		public static string RemoveAllNoneSafeUrlCharacters(this string url)
		{
			url = url.ReplaceDiacritics();
			string[] reservedCharacters = new string[] { "$", "&", "+", ",", ":", ";", "=", "?", "@", ">", "<", "#", "%", "{", "}", "^", "£" };
			string[,] replacementCharacters = { { " ", "-" },
												{ "/", "-" },
												{ "|", "-" },
												{ "\\", "-" },
												{ "À", "A" },
												{ "Á", "A" },
												{ "Â", "A" },
												{ "Ã", "a" },
												{ "Ä", "A" },
												{ "Å", "A" },
												{ "Æ", "AE" },
												{ "Ç", "C" },
												{ "È", "E" },
												{ "É", "E" },
												{ "Ê", "E" },
												{ "Ë", "E" },
												{ "Ì", "I" },
												{ "Í", "I" },
												{ "Î", "I" },
												{ "Ï", "I" },
												{ "Ð", "D" },
												{ "Ñ", "N" },
												{ "Ø", "O" },
												{ "Ò", "O" },
												{ "Ó", "O" },
												{ "Ô", "O" },
												{ "Õ", "O" },
												{ "Ö", "O" },
												{ "Ù", "U" },
												{ "Ú", "U" },
												{ "Û", "U" },
												{ "Ü", "U" },
												{ "Ý", "Y" },
												{ "ß", "S" },
												{ "à", "a" },
												{ "á", "a" },
												{ "â", "a" },
												{ "ã", "a" },
												{ "ä", "a" },
												{ "å", "a" },
												{ "æ", "ae" },
												{ "ç", "c" },
												{ "è", "e" },
												{ "é", "e" },
												{ "ê", "e" },
												{ "ë", "e" },
												{ "ì", "i" },
												{ "í", "i" },
												{ "î", "i" },
												{ "ï", "i" },
												{ "ø", "o" },
												{ "ð", "o" },
												{ "ñ", "n" },
												{ "ò", "o" },
												{ "ó", "o" },
												{ "ô", "o" },
												{ "õ", "o" },
												{ "ö", "o" },
												{ "ù", "u" },
												{ "ú", "u" },
												{ "û", "u" },
												{ "ü", "u" },
												{ "ý", "y" },
												{ "ÿ", "y" },
												{ "--", "-" },
												{ "---", "-" },
												{ "----", "-" },
												{ "-----", "-" },
												{ "------", "-" },
												{ "'", "" },
												{ "‘", "" },
												{ "’", "" },
												{ "\"", "" },
												{ "?", "" },
												{ ".", "" }};

			// Strip reserved characters

			for (int i = 0; i < reservedCharacters.Length; i++)
			{
				url = url.Replace(reservedCharacters[i], string.Empty);
			}

			for (int i = 0; i < replacementCharacters.GetLength(0); i++)
			{
				url = url.Replace(replacementCharacters[i, 0], replacementCharacters[i, 1]);
			}

			url.Replace("?", string.Empty);

			return url;
		}

		/// <summary>
		/// Trim a string to a max length
		/// </summary>
		/// <param name="input">Input</param>
		/// <param name="maxLength">Max length</param>
		/// <returns></returns>
		public static string TrimToLength(this string input, int maxLength)
		{
			return TrimToLength(input, maxLength, true);
		}

		/// <summary>
		/// Trim a string to a max length
		/// </summary>
		/// <param name="input">Input</param>
		/// <param name="maxLength">Max length</param>
		/// <param name="errorOnTooLong">Error on too long</param>
		/// <returns></returns>
		public static string TrimToLength(this string input, int maxLength, bool errorOnTooLong)
		{
			if (errorOnTooLong && input.Length > maxLength)
			{
				throw new FunctionalException("String too long: {0}", input);
			}

			return input.Length > maxLength ? input.Substring(0, maxLength) : input;
		}

		/// <summary>
		/// Returns <value>null</value> if the <see cref="System.String"/> instance is <value>null</value> or String.Empty; otherwise returns the original <see cref="System.String"/>.
		/// </summary>
		/// <param name="input">A <see cref="System.String"/> instance to nullify.</param>
		/// <returns>The nullified <see cref="System.String"/>.</returns>
		public static string Nullify(this string input)
		{
			return Nullify(input, String.IsNullOrEmpty);
		}

		/// <summary>
		/// Returns <value>null</value> if the predicate returns true; otherwise returns the original <see cref="System.String"/>.
		/// </summary>
		/// <param name="input">A <see cref="System.String"/> instance to nullify.</param>
		/// <param name="predicate">The predicate to determine if the string must be nullified.</param>
		/// <returns>The nullified <see cref="System.String"/>.</returns>
		public static string Nullify(this string input, Func<string, bool> predicate)
		{
			return predicate(input) ? null : input;
		}

		/// <summary>
		/// Generates a slug based on the input.
		/// </summary>
		/// <param name="input">The input to generate a slug from.</param>
		/// <returns>The generated slug.</returns>
		public static string GenerateSlug(this string input)
		{
			if (input.IsNullOrWhiteSpace())
			{
				return String.Empty;
			}

			// Convert to lowercase
			string slug = input.ToLowerInvariant();

			// Replace diacritics
			slug = slug.ReplaceDiacritics();

			// Replace special characters
			slug = slug.ReplaceSpecialCharacters();

			// Replace invalid characters with a dash
			slug = Regex.Replace(slug, @"[^a-z0-9-]", "-");

			// Remove multiple dashes and pre- and postfixes
			slug = Regex.Replace(slug, @"-+", "-").Trim('-');

			return slug;
		}

		/// <summary>
		/// Replace all accent characters to normal ones: Åäö to Aao
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public static string ReplaceDiacritics(this string input)
		{
			string normalizedString = input.Normalize(NormalizationForm.FormD);
			StringBuilder stringBuilder = new StringBuilder(normalizedString.Length);

			foreach (var c in normalizedString)
			{
				if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
				{
					stringBuilder.Append(c);
				}
			}

			return stringBuilder.ToString();
		}

		/// <summary>
		/// Replace all special characters to a normal representation (eg. ² to 2).
		/// </summary>
		/// <param name="input">The input string.</param>
		/// <returns>A string with all special characters replaced.</returns>
		public static string ReplaceSpecialCharacters(this string input)
		{
			// Internal dictionary of replacements
			Dictionary<string, string> replacements = new Dictionary<string, string>()
			{
				{ "²", "2" }, // Superscript Two
				{ "³", "3" }, // Superscript Three
				{ "×", "x" }, // Multiplication Sign
				{ "©", "(C)" }, // Copyright Sign
				{ "™", "(TM)" }, // Trade Mark Sign
				{ "®", "(R)" }, // Registered Sign
				{ "♥", "(L)" } // Black Heart Suit
			};

			// Replace special characters
			foreach (KeyValuePair<string, string> replacement in replacements)
			{
				input = input.Replace(replacement.Key, replacement.Value);
			}

			return input;
		}

		#endregion

		#region Fuzzy logic methods

		/// <summary>
		/// Compute the distance between two strings.
		/// </summary>
		public static int ComputeLevenshteinDistance(this string s, string t)
		{
			int n = s.Length;
			int m = t.Length;
			int[,] d = new int[n + 1, m + 1];

			// Step 1
			if (n == 0)
			{
				return m;
			}

			if (m == 0)
			{
				return n;
			}

			// Step 2
			for (int i = 0; i <= n; d[i, 0] = i++)
			{
			}

			for (int j = 0; j <= m; d[0, j] = j++)
			{
			}

			// Step 3
			for (int i = 1; i <= n; i++)
			{
				//Step 4
				for (int j = 1; j <= m; j++)
				{
					// Step 5
					int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

					// Step 6
					d[i, j] = System.Math.Min(
						System.Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
						d[i - 1, j - 1] + cost);
				}
			}
			// Step 7
			return d[n, m];
		}

		#endregion

		#region StringBuilder methods

		/// <summary>
		/// Appends the formatted line.
		/// </summary>
		/// <param name="sb">The StringBuilder.</param>
		/// <param name="format">The format.</param>
		/// <param name="args">The args.</param>
		/// <returns>The StringBuilder.</returns>
		public static StringBuilder AppendFormatLine(this StringBuilder sb, string format, params object[] args)
		{
			return sb.Append(format.FormatSafe(args)).AppendLine();
		}

		#endregion
	}
}
