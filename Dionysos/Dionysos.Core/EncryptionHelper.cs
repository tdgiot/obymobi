﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Web;
using System.Linq;

namespace Dionysos.Core
{
	/// <summary>
	/// Class which helps you encrypt strings
	/// </summary>
	public class EncryptionHelper
	{
		#region Fields

        static EncryptionHelper instance = null;
		const string passPhrase = "Cr@v3IsTheSh!zn!tpr@se";        // can be any string
		const string saltValue = "sh!zn!tCr@vEs@lt";        // can be any string
		const string hashAlgorithm = "SHA1";             // can be "MD5"
		const int passwordIterations = 2;                  // can be any number
		const string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
		const int keySize = 256;                // can be 192 or 128        

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the EncryptionHelper type if the instance has not been initialized yet
		/// </summary>
        static EncryptionHelper()
		{
			if (instance == null)
			{
				// first create the instance
                instance = new EncryptionHelper();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}

		#endregion

		#region Methods

		private void Init()
		{
		}

		public static string EncryptValue(string value)
		{
			return CrypographyUtil.Encrypt(value, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
		}

		public static string DecryptValue(string value)
		{
			return CrypographyUtil.Decrypt(value, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
		}

		#endregion
	}
}