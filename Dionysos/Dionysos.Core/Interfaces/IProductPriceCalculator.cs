using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
	/// <summary>
	/// The interface for calculating the prices
	/// </summary>
	public interface IProductPriceCalculator
	{
		/// <summary>
		/// Gets or sets the customer entity that will be used in the price calculation.
		/// </summary>
		SD.LLBLGen.Pro.ORMSupportClasses.IEntity Customer { get; set; }

		/// <summary>
		/// Gets or sets the product entity that will be used in the price calculation.
		/// </summary>
		SD.LLBLGen.Pro.ORMSupportClasses.IEntity Product { get; set; }

		/// <summary>
		/// Calculate thei price of the product for the specific customer
		/// </summary>
		decimal CalculatePrice();
	}
}
