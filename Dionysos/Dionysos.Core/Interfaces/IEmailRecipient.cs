﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Interfaces
{
	/// <summary>
	/// An interface for EmailRecipients.
	/// </summary>
	public interface IEmailRecipient : IEntity
	{
		/// <summary>
		/// Gets the email address of the recipient.
		/// </summary>
		/// <value>The email.</value>
		string Email { get; }

		/// <summary>
		/// Gets the fullname of the recipient.
		/// </summary>
		/// <value>The full name.</value>
		string FullName { get; }

		/// <summary>
		/// Gets a free-text field for the recipient type.
		/// <example>Contact, Employee, Customer, etc.</example>
		/// </summary>
		/// <value>The type of the email recipient.</value>
		string EmailRecipientType { get; }

		/// <summary>
		/// Gets or sets when the recipient opted out.
		/// </summary>
		/// <value>If not <c>null</c>, when the recipient opted out.</value>
		DateTime? EmailIsOptOut { get; set; }

		/// <summary>
		/// Gets or sets when the recipients address was bounced.
		/// </summary>
		/// <value>If not <c>null</c>, when the recipients address was bounced.</value>
		DateTime? EmailIsBounced { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="IEmailRecipient"/> is archived.
		/// </summary>
		/// <value><c>true</c> if archived; otherwise, <c>false</c>.</value>
		bool Archived { get; set; }
	}
}
