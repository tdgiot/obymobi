using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface which marks Web.UI.WebControls saveable if implemented
    /// </summary>
    public interface ISaveableControl
    {
        /// <summary>
        /// Method for Save functionality on the control(in general only used for UseDataBinding = false, then just use empty, return true, method)
        /// </summary>
        /// <returns>If the Save function was succesful</returns>
        bool Save();
    }
}
