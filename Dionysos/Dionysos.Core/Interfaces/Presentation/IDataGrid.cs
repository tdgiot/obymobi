using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for DataGrid controls
    /// </summary>
    public interface IDataGrid
    {
        #region Methods

        /// <summary>
        /// Binds the datasource to the IDataGrid instance
        /// </summary>
        void DataBind();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the datasource for the datagrid
        /// </summary>
        object DataSource
        {
            get;
            set;
        }

        #endregion
    }
}
