using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Implements ISubPanelEntity
    /// </summary>
    public interface ISubPanelEntity : ISubPanel
    {
        /// <summary>
        /// Gets or sets the EntityName        
        /// </summary>
        string EntityName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the DataSource 
        /// </summary>
        IEntity DataSource
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag indicating whether databinding should be used
        /// </summary>
        bool UseDataBinding
        {
            get;
            set;
        }

        /// <summary>
        /// Method for Save functionality on the Subpanel (in general only used for UseDataBinding = false, then just use empty, return true, method)
        /// </summary>
        /// <returns>If the Save function was succesful</returns>
        bool Save();

        /// <summary>
        /// Method for Validate functionality on the Subpanel (in general only used for UseDataBinding = false, then just use empty, return true, method)
        /// </summary>
        /// <returns>If the Validate function was succesful</returns>
        bool Validate();
    }
}
