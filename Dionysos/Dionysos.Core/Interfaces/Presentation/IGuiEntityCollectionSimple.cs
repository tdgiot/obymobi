﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Delegates.Web;

namespace Dionysos.Interfaces
{
	/// <summary>
	/// Simple version of the GuiEnityCollection (it's used as a base interface)
	/// </summary>
	public interface IGuiEntityCollectionSimple
	{
		#region Properties

		/// <summary>
		/// Gets or sets the entity name of the data collection of the gui
		/// </summary>
		string EntityName
		{
			get;
			set;
		}


		/// <summary>
		/// Gets or sets the url of the Edit Page of the displayed entitytype
		/// </summary>
		string EntityPageUrl
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the data collection of the gui
		/// </summary>
		object DataSource
		{
			get;
			set;
		}

		#endregion

        #region Events

        /// <summary>
        /// Event fired before loading the entity
        /// </summary>
        event DataSourceLoadHandler DataSourceLoad;

        /// <summary>
        /// Event fired after loading the entity
        /// </summary>
        event DataSourceLoadedHandler DataSourceLoaded;

        #endregion
    }
}
