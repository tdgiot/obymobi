using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface used for gui which contain a Lynx-media.Interfaces.DataGrid instance
    /// </summary>
    public interface IGuiDataGrid
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Lynx-media.Interfaces.IDataGrid instance
        /// </summary>
        IDataGrid DataGrid
        {
            get;
            set;
        }

        #endregion
    }
}
