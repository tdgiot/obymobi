using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface which implements subpanel classes
    /// </summary>
    public interface ISubPanel
    {
        /// <summary>
        /// Parent GuiEntitySimple which contains this SubPanel instance
        /// </summary>
        IGuiEntitySimple Parent
        {
            get;
        }
    }
}
