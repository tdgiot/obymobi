using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface used for implementations of translatable controls
    /// </summary>
    public interface ITranslatable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the text to translate of the ITranslatable instance
        /// </summary>
        string Text
        {
            get;
            set;
        }

        #endregion
    }
}
