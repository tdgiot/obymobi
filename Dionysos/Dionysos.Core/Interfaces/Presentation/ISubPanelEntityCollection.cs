using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Implements the ISubPanelEntityCollection
    /// </summary>
    public interface ISubPanelEntityCollection : ISubPanel
    {
        /// <summary>
        /// Gets or sets the EntityName        
        /// </summary>
        string EntityName 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Gets or sets the DataSource 
        /// </summary>
        object DataSource
        {
            get;
            set;
        }
    }
}
