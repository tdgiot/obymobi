﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface which is being used to implement menu factory classes
    /// </summary>
    public interface IMenuFactory
    {
        /// <summary>
        /// Loads the menu and return an object containing the menu
        /// </summary>
        /// <param name="typeName">The typename of the UIElement to load the menu for</param>
        /// <returns>An object containing the menu</returns>
        object LoadMenu(string typeName);
    }
}
