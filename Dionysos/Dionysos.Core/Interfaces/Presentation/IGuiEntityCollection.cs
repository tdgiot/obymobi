using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for gui instances with data collections
    /// </summary>
	public interface IGuiEntityCollection : IGuiEntityCollectionSimple
    {
        #region Methods

        /// <summary>
        /// Initializes the entity collection of the gui
        /// </summary>
        /// <returns>True if initialization was succesful, False if not</returns>
        bool InitializeEntityCollection();

        /// <summary>
        /// Refreshes the entity collection of the gui
        /// </summary>
        /// <returns>True if refreshing was succesful, False if not</returns>
        bool Refresh();

        /// <summary>
        /// View an data item of the entity collection of the gui
        /// </summary>
        /// <returns>True if viewing was succesful, False if not</returns>
        bool View(object id);

        /// <summary>
        /// Adds an data item to the entity collection of the gui
        /// </summary>
        /// <returns>True if adding was succesful, False if not</returns>
        bool Add();

        /// <summary>
        /// Edits the currently selected entity of the entity collection of the gui
        /// </summary>
        /// <returns>True if editing was succesful, False if not</returns>
        bool Edit(object id);

        /// <summary>
        /// Deletes the currently selected entity item from the entity collection of the gui
        /// </summary>
        /// <returns>True if deletion was successful, False if not</returns>
        bool Delete(object id);

        /// <summary>
        /// Closes the gui
        /// </summary>
        void Close();

        #endregion
    }
}
