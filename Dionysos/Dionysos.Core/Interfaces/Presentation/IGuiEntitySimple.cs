using Dionysos.Delegates.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Interfaces
{
	/// <summary>
	/// Interface for gui instances with data entities
	/// </summary>
	public interface IGuiEntitySimple
	{
		#region Methods

		/// <summary>
		/// Saves the current entity
		/// </summary>
		/// <returns>True if saving was successful, False if not</returns>
		bool Save();

		#endregion

		/// <summary>
		/// Event fired before loading the entity
		/// </summary>
		event DataSourceLoadHandler DataSourceLoad;

		/// <summary>
		/// Event fired after loading the entity
		/// </summary>
		event DataSourceLoadedHandler DataSourceLoaded;

		#region Properties		

		/// <summary>
		/// Gets or sets the entity name of the data collection of the gui
		/// </summary>
		string EntityName
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the data collection of the gui
		/// </summary>
		IEntity DataSource
		{
			get;
			set;
		}

		#endregion
	}
}

