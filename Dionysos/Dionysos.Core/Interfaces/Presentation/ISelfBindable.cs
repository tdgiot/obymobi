using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
	/// <summary>
	/// Interface which implements subpanel classes
	/// </summary>
	public interface ISelfBindable : IBindable
	{
		/// <summary>
		/// Run the Bind logic
		/// </summary>
		void Bind();
	}
}
