using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
	/// <summary>
	/// Interface which is being used for simple data binding on controls
	/// </summary>
	public interface IBindable
	{
		#region Properties

		/// <summary>
		/// Gets or sets the flag which indicated whether databinding should be used
		/// </summary>
		bool UseDataBinding { get; set; }

		#endregion
	}
}
