using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for translator class
    /// </summary>
    public interface ITranslator
    {
        /// <summary>
        /// Translates all the ITranslatable instances on a gui to the specified System.Globalization.CultureInfo instance
        /// </summary>
        /// <param name="cultureInfo">The System.Globalization.CultureInfo instance to translate to</param>
        void Translate(CultureInfo cultureInfo);
    }
}
