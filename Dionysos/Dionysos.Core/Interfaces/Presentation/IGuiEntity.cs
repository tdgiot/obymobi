namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for gui instances with data entities
    /// </summary>
    public interface IGuiEntity : IGuiEntitySimple
    {
        #region Methods

        /// <summary>
        /// Initializes the page
        /// </summary>
        /// <returns>True if initialization was succesful, False if not</returns>
        bool InitializeGui();

        /// <summary>
        /// Initializes the entity
        /// </summary>
        /// <returns>True if initialization was succesful, False if not</returns>
        bool InitializeEntity();

        /// <summary>
        /// Initializes the data bindings of the controls with the entity instance
        /// </summary>
        /// <returns>True if the initialisation was succesfull, False if not</returns>
        bool InitializeDataBindings();		

        /// <summary>
        /// Refreshes the gui
        /// </summary>
        /// <returns>True if refreshing was succesful, False if not</returns>
        bool Refresh();

        /// <summary>
        /// Adds a new entity
        /// </summary>
        /// <returns>True if adding was succesful, False if not</returns>
        bool Add();

        /// <summary>
        /// Edits the current entity
        /// </summary>
        /// <returns>True if editing was succesful, False if not</returns>
        bool Edit();

        /// <summary>
        /// Deletes the current entity
        /// </summary>
        /// <returns>True if deletion was successful, False if not</returns>
        bool Delete();

        /// <summary>
        /// Closes the gui
        /// </summary>
        void Close();

        #endregion
    }
}