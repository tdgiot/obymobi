using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface which is being used for controls
    /// </summary>
    public interface IControl
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name of the field for users
        /// </summary>
        string ControlName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag indicating whether
        /// a help message has to be displayed
        /// </summary>
        bool ShowHelp
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the help message of the control
        /// </summary>
        string HelpMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value of the control
        /// </summary>
        object Value
        {
            get;
            set;
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when the value changes between posts to the server
        /// </summary>
        event EventHandler ValueChanged;

        #endregion
    }
}
