using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for Lynx_media.Data.IEntityFactory type
    /// </summary>
    public interface IEntityFactory
    {
        /// <summary>
        /// Creates a new System.Object instance using the specified entityname from the default data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <returns>An System.Object instance</returns>
        object GetEntity(string entityName);

        ///// <summary>
        ///// Creates a new System.Object instance using the specified entityname from the default data assembly
        ///// </summary>
        ///// <param name="entityName">The name of the entity to create the dao for</param>
        ///// <returns>An System.Object instance</returns>
        //object GetDao(string entityName);

        /// <summary>
        /// Creates a new System.Object instance using the specified entityname from the specified data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <param name="assembly">The assembly which contains the entity type</param>
        /// <returns>An System.Object instance</returns>
        object GetEntity(string entityName, Assembly assembly);

        /// <summary>
        /// Gets an System.Object instance using the specified entityname and primary key value from the default data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <param name="primaryKeyValue">The primary key value of the entity to get</param>
        /// <returns>An System.Object instance</returns>
        object GetEntity(string entityName, object primaryKeyValue);

        /// <summary>
        /// Creates a new System.Object instance using the specified entityname and primary key value from the specified data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <param name="primaryKeyValue">The primary key value of the entity to get</param>
        /// <param name="assembly">The assembly which contains the entity type</param>
        /// <returns>An System.Object instance</returns>
        object GetEntity(string entityName, object primaryKeyValue, Assembly assembly);
    }
}
