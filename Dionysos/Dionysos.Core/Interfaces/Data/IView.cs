﻿using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Interfaces.Data
{
    /// <summary>
    /// Interface which is used to implement Views
    /// </summary>
    public interface IView
	{
		#region Methods

		/// <summary>
		/// Adds a ViewItem to this View instance
		/// </summary>
		/// <param name="fieldName">The name of the view of the view item</param>
		/// <param name="type">The type string of the view item</param>
		/// <param name="displayFormatString">Format string for the value (optional)</param>
		/// <param name="uiElementTypeNameFull">The full type name of the UI element</param>
		/// <returns>True if adding the view item was succesful, False if not</returns>
		bool AddViewItem(string fieldName, string type, string displayFormatString, string uiElementTypeNameFull);

		/// <summary>
		/// Removes a view item from the view
		/// </summary>
		/// <param name="fieldName">The name of the field from the view item to remove</param>
		/// <returns>True if the removal was succesful, false if not</returns>
		bool RemoveViewItem(string fieldName);

		/// <summary>
		/// Check whether a ViewItem for the specified fieldname already exists
		/// </summary>
		/// <param name="fieldName">The name of the field</param>
		/// <returns>True if the ViewItem already exists, False if not</returns>
		bool ContainsViewItem(string fieldName);

		#endregion

		#region Properties

		/// <summary>
		/// Name of the view in the system
		/// </summary>
		string Name { get; set; }

		/// <summary>
		/// Name of the entity in the system
		/// </summary>
		string EntityName { get; set; }

		/// <summary>
		/// Gets the view items
		/// </summary>
		IEntityCollection ViewItems { get; }

        /// <summary>
        /// Gets a value indicating whether this view is a custom view (vs default).
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is a custom view (vs default); otherwise, <c>false</c>.
        /// </value>
        bool IsCustomView { get; }

		#endregion
	}
}
