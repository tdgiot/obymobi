﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Class to dynamicly create FieldInfo
    /// </summary>
    public interface IEntityFieldFactory
    {
        /// <summary>Creates a new IEntityField instance, which represents the field objectName.fieldName</summary>
        /// <param name="objectName">the name of the object the field belongs to, like CustomerEntity or OrdersTypedView</param>
        /// <param name="fieldName">the name of the field to create</param>
        EntityField Create(string objectName, string fieldName);
    }
}
