using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for Lynx-media.Data.LLBLGen.v1.EntityCollectionUtil class
    /// </summary>
    public interface IEntityCollectionUtil
    {
        #region Methods

        /// <summary>
        /// Refreshes the SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to refresh</param>
        void Refresh(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection);

        /// <summary>
        /// Sort the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance using the specified field names
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to sort</param>
        /// <param name="fieldNames">The names of the fields to sort on (in the specified order)</param>
        void Sort(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection, params string[] fieldNames);

        /// <summary>
        /// Sets a filter on the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance using the specified filter items
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to set the filter on</param>
        /// <param name="filterItems">A string array of filter items</param>
        void SetFilter(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection, params string[] filterItems);

        /// <summary>
        /// Clears the current filter on the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to clear the filter from</param>
        void ClearFilter(ref SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection);

        /// <summary>
        /// Gets the entity name of the specified SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance
        /// </summary>
        /// <param name="entityCollection">The SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection instance to get the entity name from</param>
        /// <returns>A System.String containing the entity name</returns>
        string GetEntityName(SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection entityCollection);

        #endregion
    }
}
