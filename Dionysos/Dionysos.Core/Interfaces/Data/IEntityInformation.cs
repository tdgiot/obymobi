﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Dionysos.Interfaces.Data
{
	/// <summary>
	/// Entity Information
	/// </summary>
	public interface IEntityInformation
	{
		/// <summary>
		/// Name of the entity in the system
		/// </summary>
		string EntityName { get; set; }

		/// <summary>
		/// FieldName of the field's value to display a a default for this entity
		/// </summary>
		string ShowFieldName { get; set; }

		/// <summary>
		/// FieldName of the field's value to display when used in a breadcrumb
		/// </summary>
		string ShowFieldNameBreadCrumb { get; set; }

		/// <summary>
		/// Userfriendly name of the entity (singular)
		/// </summary>
		string FriendlyName { get; set; }

		/// <summary>
		/// Userfriendly name of the entity (plural)
		/// </summary>
		string FriendlyNamePlural { get; set; }

		/// <summary>
		/// Help Text for the Entity
		/// </summary>
		string HelpText { get; set; }

		/// <summary>
		/// Get the Title for BreadCrumb in format [Entity(optional:,ShowField]
		/// </summary>
		string TitleAsBreadCrumbHierarchy { get; set; }

		/// <summary>
		/// Get the relative Url to the EditPage of the Entity
		/// </summary>
		string DefaultEntityEditPage { get; set; }

		/// <summary>
		/// Get the relative Url to the CollectionPage of the Entity
		/// </summary>
		string DefaultEntityCollectionPage { get; set; }

		/// <summary>
		/// Determine if the WmsCacheDate should be updated when this entity is saved
		/// </summary>
		bool UpdateWmsCacheDateOnChange { get; set; }		

		/// <summary>
		/// All database fields of the entity
		/// </summary>
		Dictionary<string, IEntityFieldInformation> DatabaseFields { get; set; }

		/// <summary>
		/// All code (custom property) fields of the entity
		/// </summary>
		Dictionary<string, IEntityFieldInformation> CodeFields { get; set; }

		/// <summary>
		/// All code and database fields of the entity
		/// </summary>
		Dictionary<string, IEntityFieldInformation> AllFields { get; set; }

        /// <summary>
        /// Primary key fields of the entity
        /// </summary>
		Dictionary<string, IEntityFieldInformation> PrimaryKeyFields { get; set; }

        /// <summary>
        /// Foreign key fields of the entity
        /// </summary>
        Dictionary<string, IEntityFieldInformation> ForeignKeyFields { get; set; }

        /// <summary>
        /// Relations of the entity
        /// </summary>
		Dictionary<string, IEntityRelationInformation> RelationInformation { get; set; }		
	}
}
