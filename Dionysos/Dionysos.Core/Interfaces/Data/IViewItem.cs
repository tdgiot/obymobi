﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Interfaces.Data
{
	/// <summary>
	/// Interface which is used to implement ViewItems
	/// </summary>
	public interface IViewItem
	{
		/// <summary>
		/// Name of the entity in the system
		/// </summary>
		string EntityName { get; set; }

		/// <summary>
		/// Name of the field
		/// </summary>
		string FieldName { get; set; }

		/// <summary>
		/// Flag which indicates whether the field should be displayed
		/// </summary>
		bool ShowOnGridView { get; set; }

		/// <summary>
		/// Position of the field on a grid
		/// </summary>
		int DisplayPosition { get; set; }

		/// <summary>
		/// Gets or sets the sort position.
		/// </summary>
		/// <value>The sort position.</value>
		int SortPosition { get; set; }

		/// <summary>
		/// Gets or sets the sort operator (None = 0, Ascending = 1, Descending = 2).
		/// </summary>
		/// <value>The sort operator.</value>
		int SortOperator { get; set; }

		/// <summary>
		/// Width of a column (Unit)
		/// </summary>
		string Width { get; set; }

		/// <summary>
		/// Display format string
		/// </summary>
		string DisplayFormatString { get; set; }

		/// <summary>
		/// Type of a field or property
		/// </summary>
		string Type { get; set; }
	}
}
