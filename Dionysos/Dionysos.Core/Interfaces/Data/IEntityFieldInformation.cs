﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Interfaces.Data
{
	/// <summary>
	/// Entity Field Information
	/// </summary>
	public interface IEntityFieldInformation
	{
		/// <summary>
		/// Name of the entity in the system
		/// </summary>
		string EntityName { get; set; }

		/// <summary>
		/// Name of the field on the entity
		/// </summary>
		string FieldName { get; set; }

		/// <summary>
		/// Userfriendly name of the field
		/// </summary>
		string FriendlyName { get; set; }

		/// <summary>
		/// Show the column default on a gridview
		/// </summary>
		bool ShowOnDefaultGridView { get; set; }

		/// <summary>
		/// Default position on a Gridview
		/// </summary>
		int DefaultDisplayPosition { get; set; }

		/// <summary>
		/// DisplayFormatString to format the data
		/// </summary>
		string DisplayFormatString { get; set; }

		/// <summary>
		/// Gets or sets the default sort position.
		/// </summary>
		/// <value>The default sort position.</value>
		int DefaultSortPosition { get; set; }

		/// <summary>
		/// Gets or sets the default sort operator (None = 0, Ascending = 1, Descending = 2).
		/// </summary>
		/// <value>The default sort operator.</value>
		int DefaultSortOperator { get; set; }

		/// <summary>
		/// Allow the column to be shown on a GridView
		/// </summary>
		bool AllowShowOnGridView { get; set; }

		/// <summary>
		/// Help Text for the Entity
		/// </summary>
		string HelpText { get; set; }

		/// <summary>
		/// Type of the field or property
		/// </summary>
		string Type { get; set; }

		/// <summary>
		/// ExcludeForUpdateWmsCacheDateOnChange of the field or property
		/// </summary>
		bool ExcludeForUpdateWmsCacheDateOnChange { get; set; }

		/// <summary>
		/// The type of EntityField (EntityInformationUtil.EntityFieldTypeDatabase or EntityInformationUtil.EntityFieldTypeCode)
		/// </summary>
		string EntityFieldType { get; set; }

		/// <summary>
		/// If the field is a foreign key the relation is describbed here
		/// </summary>
		IEntityRelationInformation EntityRelationInformation { get; set; }
	}
}
