using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for entity collection factory classes
    /// </summary>
    public interface IEntityCollectionFactory
    {
        #region Methods

        /// <summary>
        /// Creates a new entity collection instance using the specified entityname from the default data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to get the data collection for</param>
        /// <returns>An System.Object instance containing a data collection</returns>
        object GetEntityCollection(string entityName);

        /// <summary>
        /// Creates a new entity collection instance using the specified entityname from the specified data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to get the data collection for</param>
        /// <param name="assembly">The assembly which contains the entity type</param>
        /// <returns>An System.Object instance containing a data collection</returns>
        object GetEntityCollection(string entityName, System.Reflection.Assembly assembly);

        #endregion
    }
}
