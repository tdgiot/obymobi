using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Interfaces.Data;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for Dionysos.Data.LLBLGen.EntityCollectionUtil class
    /// </summary>
    public interface IEntityUtil
    {
        #region Methods

        /// <summary>
        /// Returns the fields of the entity specified by the entity name
        /// </summary>
        /// <param name="entityName">The name of the entity to retrieve the fields from</param>
        /// <returns>A System.String array containing the fields of the specified entity</returns>
        string[] GetFields(string entityName);

        /// <summary>
        /// Deletes an entity
        /// </summary>
        /// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance that has to be deleted</param>
        /// <returns>True is the entity is deleted, False if not</returns>
        bool DeleteEntity(IEntity entity);

        /// <summary>
        /// Checks whether the specified entity can be deleted according to referential constraints
        /// </summary>
        /// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to delete the related entities for</param>
        string CheckForRestrictedRelatedEntities(IEntity entity);

        /// <summary>
        /// Deletes related entities from a entity instance
        /// </summary>
        /// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to delete the related entities for</param>
        void DeleteRelatedEntities(IEntity entity);

		/// <summary>
		/// Archives related entities from a entity instance
		/// </summary>
		/// <param name="entity">The SD.LLBLGen.Pro.ORMSupportClasses.IEntity instance to archive the related entities for</param>
		/// <param name="newArchivedValue">Sets which value should be set to the Archived field</param>
		void ArchiveRelatedEntities(IEntity entity, bool newArchivedValue);
		
        ///// <summary>
        ///// Get the information about a Entity
        ///// </summary>
        ///// <param name="entityName">EntityName</param>
        ///// <returns>EntityInformation for the Entity</returns>
        //IEntityInformation GetEntityInformation(string entityName);

        #endregion
    }
}
