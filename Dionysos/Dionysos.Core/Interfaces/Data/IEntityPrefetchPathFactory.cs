using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Interfaces
{
    /// <summary>
    /// Interface for Lynx_media.Data.IEntityFactory type
    /// </summary>
    public interface IEntityPrefetchPathFactory
    {
        /// <summary>
        /// Creates a new System.Object instance using the specified entityname from the default data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <returns>An System.Object instance</returns>
        object CreatePrefetchPath(string entityName);

        /// <summary>
        /// Creates a new System.Object instance using the specified entityname from the default data assembly
        /// </summary>
        /// <param name="entityName">The name of the entity to create</param>
        /// <returns>An System.Object instance</returns>
        object CreatePrefetchPathElement(string sourceEntityName, string destinationEntityName);

        ///// <summary>
        ///// Creates a new System.Object instance using the specified entityname from the default data assembly
        ///// </summary>
        ///// <param name="entityName">The name of the entity to create the dao for</param>
        ///// <returns>An System.Object instance</returns>
        object GetEntityType(string entityName);        
    }
}
