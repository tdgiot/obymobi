﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dionysos.Interfaces.Data
{
    /// <summary>
    /// Interface which is used to implement entity relation information classes
    /// </summary>
    public interface IEntityRelationInformation
    {
        /// <summary>
        /// Name of the entity in the system
        /// </summary>
        string RelatedEntityName { get; set; }

        /// <summary>
        /// Userfriendly name of the entity (singular)
        /// </summary>
        string RelatedEntityFriendlyName { get; set; }

        /// <summary>
        /// Type of the relation
        /// </summary>
        RelationType RelationType { get; set; }

        /// <summary>
        /// Name of the field the relation is mapped to
        /// </summary>
        string MappedFieldName { get; set; }
    }
}
