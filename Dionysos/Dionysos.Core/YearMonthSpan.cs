﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Class for working with the span between two YearMonth classes
	/// </summary>	
	public class YearMonthSpan
	{
		#region Fields

		YearMonth from;
		YearMonth till;
		int totalMonthsInSpan;

		#endregion

		#region Methods		

		/// <summary>
		/// Intialize a YearMonthSpan class for 1 year.
		/// </summary>
		/// <param name="year"></param>		
		public YearMonthSpan(int year)
		{
			this.from = new YearMonth(year, 1);
			this.till = new YearMonth(year, 12);
			totalMonthsInSpan = (till.Month - (from.Month - 1)) + ((till.Year - from.Year) * 12);
		}

		/// <summary>
		/// Intialize a YearMonthSpan class
		/// </summary>
		/// <param name="fromYear"></param>
		/// <param name="fromMonth"></param>
		/// <param name="tillYear"></param>
		/// <param name="tillMonth"></param>		
		public YearMonthSpan(int fromYear, int fromMonth, int tillYear, int tillMonth)
		{
			this.from = new YearMonth(fromYear, fromMonth);
			this.till = new YearMonth(tillYear, tillMonth);
			totalMonthsInSpan = (till.Month - (from.Month - 1)) + ((till.Year - from.Year) * 12);			
		}

		/// <summary>
		/// Intialize a YearMonthSpan class
		/// </summary>
		/// <param name="from"></param>
		/// <param name="till"></param>
		public YearMonthSpan(YearMonth from, YearMonth till)
		{
			this.from = from;
			this.till = till;
			totalMonthsInSpan = (till.Month - (from.Month - 1)) + ((till.Year - from.Year) * 12);			
		}

		/// <summary>
		/// Check if this YearMonthSpan is overlapped by the supplied period
		/// </summary>
		/// <param name="periodToOverlapWith">Period used to check overlapped by</param>
		/// <returns></returns>
		public bool IsOverlappedBy(YearMonthSpan periodToOverlapWith)
		{
			bool toReturn = false;
			toReturn = (this.From.CompareTo(periodToOverlapWith.Till) <= 0 && this.Till.CompareTo(periodToOverlapWith.From) >= 0);
			return toReturn;
		}

		/// <summary>
		/// Returns the period of the supplied parameter that overlaps with the period of this YearMonthSpan instance
		/// </summary>
		/// <param name="period">Period to get the common part for</param>
		/// <param name="commonPeriod">Common period between the two spans</param>
		/// <returns></returns>
		public bool GetOverlappedCommonPeriod(YearMonthSpan period, out YearMonthSpan commonPeriod)
		{
			bool toReturn;
			YearMonth commonFrom, commonTill;
			commonPeriod = null;
			if (this.IsOverlappedBy(period))
			{
				if (period.From.CompareTo(this.From) < 0)
					commonFrom = this.From;
				else
					commonFrom = period.From;

				if (period.Till.CompareTo(this.Till) < 0)
					commonTill = period.Till;
				else
					commonTill = this.Till;

				commonPeriod = new YearMonthSpan(commonFrom, commonTill);
				toReturn = true;
			}
			else
				toReturn = false;

			return toReturn;
		}

		/// <summary>
		/// To String 
		/// </summary>
		/// <returns></returns>
		public new string ToString()
		{
			return string.Format("{0} - {1}", this.from.ToString(), this.till.ToString());
		}

		/// <summary>
		/// Create a clone of this instance.
		/// </summary>
		/// <returns></returns>
		public YearMonthSpan Clone()
		{
			return new YearMonthSpan(this.From.Year, this.From.Month, this.Till.Year, this.Till.Month);
		}

		#endregion

		#region Event Handlers
		

		#endregion

		#region Properties

		/// <summary>
		/// Difference of full (12 month) years between the dates, so 14 months is 1 year (and 2 months)
		/// </summary>
		public int Years
		{
			get
			{
				return totalMonthsInSpan / 12;
			}
		}

		/// <summary>
		/// The number of months in the timespan, without the full years, so 14 months is 2 months (and 1 year)
		/// </summary>
		public int Months
		{
			get
			{
				return totalMonthsInSpan - (this.Years * 12);
			}
		}

		/// <summary>
		/// Total number of months in the Span (including full years, so 1 year and 2 months is 14 months
		/// </summary>
		public int MonthsTotal
		{
			get
			{
				return this.totalMonthsInSpan;
			}
		}

		/// <summary>
		/// Get the From date used for this YearMonthSpan
		/// </summary>
		public YearMonth From
		{
			get
			{
				return this.from;
			}
		}

		/// <summary>
		/// Get the Till date used for this YearMonthSpan
		/// </summary>
		public YearMonth Till
		{
			get
			{
				return this.till;
			}
		}

		#endregion

	}
}
