﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Dionysos
{
    /// <summary>
    /// CultureInfo utility class.
    /// </summary>
    public static class CultureInfoUtil
    {
        /// <summary>
        /// Get the EnglishName of the CultureInfo (without the country).
        /// </summary>
        /// <param name="cultureInfo">The CultureInfo to get the EnglishName of.</param>
        /// <returns>Returns the EnglishName of the CultureInfo.</returns>
        public static string GetEnglishName(this CultureInfo cultureInfo)
        {
            return (cultureInfo.IsNeutralCulture ? cultureInfo.EnglishName : cultureInfo.Parent.EnglishName).TurnFirstToUpper(false);
        }

        /// <summary>
        /// Get the NativeName of the CultureInfo (without the country).
        /// </summary>
        /// <param name="cultureInfo">The CultureInfo to get the NativeName of.</param>
        /// <returns>Returns the NativeName of the CultureInfo.</returns>
        public static string GetNativeName(this CultureInfo cultureInfo)
        {
            return (cultureInfo.IsNeutralCulture ? cultureInfo.NativeName : cultureInfo.Parent.NativeName).TurnFirstToUpper(false);
        }

    }
}
