using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Expressions
{
    /// <summary>
    /// Enumerator which represents a comparison operator
    /// </summary>
    public enum ComparisonOperator
    {
        /// <summary>
        /// Value to compare has to be lesser
        /// </summary>
        Lesser,
        /// <summary>
        /// Value to compare has to be lesser or equal
        /// </summary>
        LesserEqual,
        /// <summary>
        /// Value to compare has to be equal
        /// </summary>
        Equal,
        /// <summary>
        /// Value to compare has to be unequal
        /// </summary>
        NotEqual,
        /// <summary>
        /// Value to compare has to be greater or equal
        /// </summary>
        GreaterEqual,
        /// <summary>
        /// Value to compare has to be greater
        /// </summary>
        Greater
    }
}
