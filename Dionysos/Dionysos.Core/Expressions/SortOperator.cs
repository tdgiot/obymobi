using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Expressions
{
    /// <summary>
    /// Enumerator for setting sort orders with
    /// </summary>
    public enum SortOperator
    {
        /// <summary>
        /// Sorts a collection starting with the smallest item first
        /// </summary>
        Ascending,
        /// <summary>
        /// Sorts a collection starting with the greatest item first
        /// </summary>
        Descending
    }
}
