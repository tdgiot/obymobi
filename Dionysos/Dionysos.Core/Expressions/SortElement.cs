using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Dionysos.Expressions
{
    /// <summary>
    /// Class which represents an item in a Lynx-media.SortExpression instance
    /// </summary>
    public class SortElement
    {
        #region Fields

        private string fieldName = string.Empty;
        private SortOperator sortOperator = SortOperator.Ascending;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.SortItem type using the specified field name
        /// </summary>
        /// <param name="fieldName">The name of the field to sort on</param>
        public SortElement(string fieldName)
        {
            if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
            {
                // Argument 'fieldName' is empty
            }
            else
            {
                this.fieldName = fieldName;
            }
        }

        /// <summary>
        /// Constructs an instance of the Lynx-media.SortItem type 
        /// using the specified field name and sort operator
        /// </summary>
        /// <param name="fieldName">The name of the field to sort on</param>
        /// <param name="sortOperator">The sort operator to determine the order</param>
        public SortElement(string fieldName, SortOperator sortOperator)
        {
            if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
            {
                // Argument 'fieldName' is empty
            }
            else if (Instance.ArgumentIsEmpty(sortOperator, "sortOperator"))
            {
                // Argument 'sortOperator' is empty
            }
            else
            {
                this.fieldName = fieldName;
                this.sortOperator = sortOperator;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Parses a System.String representation of a SortItem instance
        /// </summary>
        /// <param name="sortItemString">The System.String representation of the SortItem instance</param>
        public void Parse(string sortItemString)
        {
            if (Instance.ArgumentIsEmpty(sortItemString, "sortItemString"))
            {
                // Parameter 'sortItemString' is empty
            }
            else
            {
                string[] parts = sortItemString.Split(' ');
                if (Instance.Empty(parts))
                {
                    throw new Dionysos.EmptyException("Variable 'parts' is empty");
                }
                else if (parts.Length != 2)
                {
                    throw new ApplicationException("Variable 'parts' does not have correct length");
                }
                else
                {
                    // Set the field name
                    this.fieldName = parts[0];

                    // Set the sort operator
                    switch(parts[1].ToUpper())
                    {
                        case "ASC":
                            this.sortOperator = SortOperator.Ascending;
                            break;
                        case "DESC":
                            this.sortOperator = SortOperator.Descending;
                            break;
                        default:
                            this.sortOperator = SortOperator.Ascending;
                            break;
                    }
                }
            }
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public XmlNode ToXml()
        //{
        //    return null;
        //}

        #endregion
    }
}
