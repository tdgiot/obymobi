using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Expressions
{
    /// <summary>
    /// Class which represents a filter on a single field
    /// </summary>
    public class FilterElement
    {
        #region Fields

        private string fieldName = string.Empty;
        private ComparisonOperator comparisonOperator = ComparisonOperator.Equal;
        private System.Type dataType = null;
        private object value = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.FilterItem type
        /// </summary>
        public FilterElement(string fieldName, ComparisonOperator comparisonOperator, object value)
        {
            if (Instance.ArgumentIsEmpty(fieldName, "fieldName"))
            {
                // Argument 'fieldName' is empty
            }
            else if (Instance.ArgumentIsEmpty(comparisonOperator, "comparisonOperator"))
            {
                // Argument 'comparisonOperator' is empty
            }
            else if (Instance.ArgumentIsEmpty(value, "value"))
            {
                // Argument 'value' is empty
            }
            else
            {
                this.fieldName = fieldName;
                this.comparisonOperator = comparisonOperator;
                this.value = value;
                this.dataType = value.GetType();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Parses a System.String representation of a FilterItem instance
        /// </summary>
        /// <param name="filterItemString">The System.String representation of the FilterItem instance</param>
        public void Parse(string filterItemString)
        {
            if (Instance.ArgumentIsEmpty(filterItemString, "filterItemString"))
            {
                // Parameter 'filterItemString' is empty
            }
            else
            {
                string[] parts = filterItemString.Split(' ');
                if (Instance.Empty(parts))
                {
                    throw new Dionysos.EmptyException("Variable 'parts' is empty");
                }
                else if (parts.Length < 3)
                {
                    throw new ApplicationException("Variable 'parts' does not have correct length");
                }
                else
                {
                    // Set the field name
                    this.fieldName = parts[0];

                    // Set the comparison operator
                    switch (parts[1])
                    {
                        case "<":
                            this.comparisonOperator = ComparisonOperator.Lesser;
                            break;
                        case "<=":
                            this.comparisonOperator = ComparisonOperator.LesserEqual;
                            break;
                        case "=":
                            this.comparisonOperator = ComparisonOperator.Equal;
                            break;
                        case "==":
                            this.comparisonOperator = ComparisonOperator.Equal;
                            break;
                        case "!=":
                            this.comparisonOperator = ComparisonOperator.NotEqual;
                            break;
                        case "<>":
                            this.comparisonOperator = ComparisonOperator.NotEqual;
                            break;
                        case ">=":
                            this.comparisonOperator = ComparisonOperator.GreaterEqual;
                            break;
                        case ">":
                            this.comparisonOperator = ComparisonOperator.Greater;
                            break;
                    }

                    // Parse the value from the filteritem string representation
                    // Value could be a string containing spaces, so walk through the other parts
                    string valueString = string.Empty;
                    for (int i = 4; i < parts.Length; i++)
			        {
			            valueString += parts[i];
                        if (i <= (parts.Length - 1))
                        {
                            valueString += " ";
                        }
			        }

                    // Set the data type before the value, 
                    // otherwise we don't know which parse method to use
                    switch(parts[3])
                    {
                        case "System.Boolean":
                            this.value = bool.Parse(valueString);
                            break;
                        case "System.Byte":
                            this.value = byte.Parse(valueString);
                            break;
                        case "System.SByte":
                            this.value = sbyte.Parse(valueString);
                            break;
                        case "System.Char":
                            this.value = char.Parse(valueString);
                            break;
                        case "System.Decimal":
                            this.value = decimal.Parse(valueString);
                            break;
                        case "System.Double":
                            this.value = double.Parse(valueString);
                            break;
                        case "System.Float":
                            this.value = float.Parse(valueString);
                            break;
                        case "System.Int32":
                            this.value = int.Parse(valueString);
                            break;
                        case "System.UInt32":
                            this.value = uint.Parse(valueString);
                            break;
                        case "System.Int64":
                            this.value = long.Parse(valueString);
                            break;
                        case "System.UInt64":
                            this.value = ulong.Parse(valueString);
                            break;
                        case "System.Int16":
                            this.value = short.Parse(valueString);
                            break;
                        case "System.UInt16":
                            this.value = ushort.Parse(valueString);
                            break;
                        case "System.String":
                            this.value = valueString;
                            break;
                    }
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the fieldname of the filter item
        /// </summary>
        public string FieldName
        {
            get
            {
                return this.fieldName;
            }
            set
            {
                this.fieldName = value;
            }
        }

        /// <summary>
        /// Gets or sets the comparison operator for the filter item
        /// </summary>
        public ComparisonOperator ComparisonOperator
        {
            get
            {
                return this.comparisonOperator;
            }
            set
            {
                this.comparisonOperator = value;
            }
        }

        /// <summary>
        /// Gets or sets the value of the filter item
        /// </summary>
        public object Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
                this.dataType = value.GetType();
            }
        }

        #endregion
    }
}
