using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Expressions
{
    /// <summary>
    /// Collection class used for storing Lynx_media.Expressions.FilterExpression instances in
    /// </summary>
    public class FilterExpressionCollection : ICollection<Dionysos.Expressions.FilterExpression>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the FilterExpressionCollection class
        /// </summary>	
        public FilterExpressionCollection()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Lynx_media.Expressions.FilterExpression instance to the collection
        /// </summary>
        /// <param name="filterExpression">The Lynx_media.Expressions.FilterExpression instance to add to the collection</param>
        public void Add(Dionysos.Expressions.FilterExpression filterExpression)
        {
            this.items.Add(filterExpression);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Lynx_media.Expressions.FilterExpression instance is already in the collection
        /// </summary>
        /// <param name="filterExpression">The Lynx_media.Expressions.FilterExpression instance to check</param>
        /// <returns>True if the Lynx_media.Expressions.FilterExpression instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Expressions.FilterExpression filterExpression)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Expressions.FilterExpression) == filterExpression)
                {
                    contains = true;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Expressions.FilterExpression[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Lynx_media.Expressions.FilterExpression instance from this collection
        /// </summary>
        /// <param name="filterExpression">The Lynx_media.Expressions.FilterExpression instance to remove</param>
        public bool Remove(Dionysos.Expressions.FilterExpression filterExpression)
        {
            this.items.Remove(filterExpression);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.FilterExpressionCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Expressions.FilterExpression> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Expressions.FilterExpression>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.FilterExpressionCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Expressions.FilterExpression instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx_media.Expressions.FilterExpression instance to get</param>
        /// <returns>An Lynx_media.Expressions.FilterExpression instance</returns>
        public Dionysos.Expressions.FilterExpression this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Expressions.FilterExpression;
            }
        }

        #endregion
    }
}