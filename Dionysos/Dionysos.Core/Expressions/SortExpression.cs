using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Expressions
{
    /// <summary>
    /// Class which represents a sort expression
    /// </summary>
    public class SortExpression
    {
        #region Fields

        private string name = string.Empty;
        private SortElementCollection sortItems = new SortElementCollection();

        #endregion

        #region Constructor

        /// <summary>
        /// Constructs an instance of the Lynx-media.SortExpression type
        /// </summary>
        public SortExpression()
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx-media.SortExpression type
        /// </summary>
        public SortExpression(string name)
        {
            this.name = name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the SortExpression instance
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Lynx-media.SortItemCollection 
        /// </summary>
        public SortElementCollection Items
        {
            get
            {
                return this.sortItems;
            }
            set
            {
                this.sortItems = value;
            }
        }

        #endregion
    }
}
