using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Expressions
{
    /// <summary>
    /// Class which represents a filter
    /// </summary>
    public class FilterExpression
    {
        #region Fields

        private string name = string.Empty;
        private FilterElementCollection filterItems = new FilterElementCollection();

        #endregion

        #region Constructor

        /// <summary>
        /// Constructs an instance of the Lynx-media.FilterExpression type
        /// </summary>
        public FilterExpression()
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx-media.FilterExpression type
        /// </summary>
        public FilterExpression(string name)
        {
            this.name = name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the FilterExpression instance
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Lynx-media.FilterItemCollection 
        /// </summary>
        public FilterElementCollection Items
        {
            get
            {
                return this.filterItems;
            }
            set
            {
                this.filterItems = value;
            }
        }

        #endregion
    }
}
