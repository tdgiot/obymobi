using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;
using Dionysos.Reflection;
using Dionysos.Interfaces;

namespace Dionysos
{
	/// <summary>
	/// Static class used for storing global application properties
	/// </summary>
	public class Global
	{
		#region Fields

		static Global instance = null;


		private AssemblyInfoCollection assemblyInfo = null;
		private IFieldCollectionPool fieldCollectionPool = null;
		private Dionysos.Security.IRightsProvider rightsProvider = null;
		private Dionysos.Globalization.ITranslationProvider translationProvider = null;
		private Dionysos.Globalization.IGlobalizationHelper globalizationHelper = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Global class if the instance has not been initialized yet
		/// </summary>
		static Global()
		{
			if (instance == null)
			{
				// first create the instance
				instance = new Global();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}

		#endregion

		#region Methods

		private void Init()
		{
			// Initialize the static members
			this.assemblyInfo = new AssemblyInfoCollection();
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the Dionysos.Interfaces.IApplicationInfo instance which contains information about the application
		/// </summary>
		public static IApplicationInfo ApplicationInfo
		{
			get
			{
				return GlobalLight.ApplicationInfo;
			}
			set
			{
				GlobalLight.ApplicationInfo = value;
			}
		}

		/// <summary>
		/// Gets the Dionysos.Reflection.AssemblyInfo instance which contains information about the application's assemblies
		/// </summary>
		public static AssemblyInfoCollection AssemblyInfo
		{
			get
			{
				return instance.assemblyInfo;
			}
		}

		/// <summary>
		/// Gets or sets the Dionysos.Globalization.ITranslationProvider instance which is used for translation of controls and strings
		/// </summary>
		public static Dionysos.Globalization.ITranslationProvider TranslationProvider
		{
			get
			{
				return instance.translationProvider;
			}
			set
			{
				instance.translationProvider = value;
			}
		}

		/// <summary>
		/// Gets or sets the Dionysos.Interfaces.FieldCollectionPool instance which contains information about the data fields
		/// </summary>
		public static IFieldCollectionPool FieldInfo
		{
			get
			{
				return instance.fieldCollectionPool;
			}
			set
			{
				instance.fieldCollectionPool = value;
			}
		}

		/// <summary>
		/// Gets or sets the Dionysos.Configuration.ConfigurationInfoCollection instance
		/// </summary>
		public static ConfigurationInfoCollection ConfigurationInfo
		{
			get
			{
				return GlobalLight.ConfigurationInfo;
			}
			set
			{
				GlobalLight.ConfigurationInfo = value;
			}
		}

		/// <summary>
		/// Gets or sets the Dionysos.Configuration.ConfigurationInfoCollection instance
		/// </summary>
		public static IConfigurationProvider ConfigurationProvider
		{
			get
			{
				return GlobalLight.ConfigurationProvider;
			}
			set
			{
				GlobalLight.ConfigurationProvider = value;
			}
		}

		/// <summary>
		/// Gets or sets the IRightsProvider instance
		/// </summary>
		public static Dionysos.Security.IRightsProvider RightsProvider
		{
			get
			{
				if (Instance.Empty(instance.rightsProvider))
				{
					throw new ConfigurationProviderNotSetException("RightsProvider provider not set!");
				}
				else
				{
					return instance.rightsProvider;
				}
			}
			set
			{
				instance.rightsProvider = value;
			}
		}

		/// <summary>
		/// Gets or sets the IGlobalizationHelper
		/// </summary>
		public static Dionysos.Globalization.IGlobalizationHelper GlobalizationHelper
		{
			get
			{
				if (Instance.Empty(instance.globalizationHelper))
				{
					return null;
				}
				else
				{
					return instance.globalizationHelper;
				}
			}
			set
			{
				instance.globalizationHelper = value;
			}
		}

		#endregion

	}
}