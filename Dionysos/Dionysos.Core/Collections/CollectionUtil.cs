﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Dionysos.Collections
{
    /// <summary>
    /// Singleton utility class which can be used to do operations on collections
    /// </summary>
    public class CollectionUtil
    {
        #region Fields

        static CollectionUtil instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a static instance of the Dionysos.Collections.CollectionUtil class
        /// </summary>
        static CollectionUtil()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new CollectionUtil();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
        }

        /// <summary>
        /// Converts an ArrayList instance to an Array instance
        /// </summary>
        /// <param name="arrayList">The ArrayList instance to convert to an Array</param>
        /// <returns>An Array instance containing the items of the specified ArrayList instance</returns>
        public static object[] ConvertArrayListToArray(ArrayList arrayList)
        {
            return (object[])arrayList.ToArray(typeof(object));
        }

        #endregion
    }
}