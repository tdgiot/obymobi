﻿using System.Collections.Concurrent;

namespace Dionysos.Collections
{
    /// <summary>
    /// Create a ConcurrentQueue with a fixed size. When adding an item while queue size limit is reached the oldest item will be dequeued
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FixedSizeConcurrentQueue<T> : ConcurrentQueue<T>
    {
        private readonly object lockObject = new object();

        /// <summary>
        /// Max size of the queue.
        /// </summary>
        public int Size { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        public FixedSizeConcurrentQueue(int size)
        {
            Size = size;
        }

        /// <summary>
        /// Enqueue new item, if queue size is reached the oldest item will be dequeued
        /// </summary>
        /// <param name="obj"></param>
        public new void Enqueue(T obj)
        {
            base.Enqueue(obj);
            lock (this.lockObject)
            {
                T overflow;
                while (Count > Size && TryDequeue(out overflow)) {}
            }
        }
    }
}
