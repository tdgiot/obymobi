﻿using System;
using System.Linq;

namespace Dionysos.Collections
{
	/// <summary>
	/// Util class to perform actions on a String Array
	/// </summary>
	public static class StringArrayUtil
	{
		/// <summary>
		/// Search for a string in the Array
		/// </summary>
		/// <param name="value">Value to search for</param>
		/// <param name="array">Array to search in</param>
		/// <returns>
		/// Index of the found element or -1 if not found
		/// </returns>
		public static int IndexOf(string value, string[] array)
		{
			return IndexOf(value, array, false);
		}

		/// <summary>
		/// Search for a string in the Array
		/// </summary>
		/// <param name="value">Value to search for</param>
		/// <param name="array">Array to search in</param>
		/// <param name="caseInsensitive">Search case insensitive</param>
		/// <returns>
		/// Index of the found element or -1 if not found
		/// </returns>
		public static int IndexOf(string value, string[] array, bool caseInsensitive)
		{
			int index = -1;

			for (int i = 0; i < array.Length; i++)
			{
				if (String.Equals(array[i], value, caseInsensitive ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal))
				{
					index = i;
					break;
				}
			}

			return index;
		}

		/// <summary>
		/// Remove an item from the array, if it's added multiple times all instances will be removed
		/// </summary>
		/// <param name="value">Value to remove</param>
		/// <param name="array">Array to remove from</param>
		/// <returns></returns>
		public static string[] Remove(string value, string[] array)
		{
			return Remove(value, array, false);
		}

		/// <summary>
		/// Remove an item from the array, if it's added multiple times all instances will be removed
		/// </summary>
		/// <param name="value">Value to remove</param>
		/// <param name="array">Array to remove from</param>
		/// <param name="caseInsensitive">Seek CaseInsensitve</param>
		/// <returns></returns>
		public static string[] Remove(string value, string[] array, bool caseInsensitive)
		{
			return array.Where(v => !String.Equals(v, value, caseInsensitive ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal)).ToArray();
		}

		/// <summary>
		/// Checks whether the strings in the arrays are equal, instead of the array objects itself.
		/// </summary>
		/// <param name="arrayA">The left array.</param>
		/// <param name="arrayB">The right array.</param>
		/// <returns>True if all values are equal; otherwise false.</returns>
		public static bool AreEqual(this string[] arrayA, string[] arrayB)
		{
			if (arrayA.Length != arrayB.Length)
			{
				return false;
			}

			for (int i = 0; i < arrayA.Length; i++)
			{
				if (arrayA[i] != arrayB[i])
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Trims the specified array.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <returns></returns>
		public static string[] Trim(this string[] array)
		{
			return array.Select(v => v.Trim()).ToArray();
		}

		/// <summary>
		/// Trims the specified array.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <param name="trimChars">The trim chars.</param>
		/// <returns></returns>
		public static string[] Trim(this string[] array, params char[] trimChars)
		{
			return array.Select(v => v.Trim(trimChars)).ToArray();
		}

		/// <summary>
		/// Trims the end.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <returns></returns>
		public static string[] TrimEnd(this string[] array)
		{
			return array.Select(v => v.TrimEnd()).ToArray();
		}

		/// <summary>
		/// Trims the end.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <param name="trimChars">The trim chars.</param>
		/// <returns></returns>
		public static string[] TrimEnd(this string[] array, params char[] trimChars)
		{
			return array.Select(v => v.TrimEnd(trimChars)).ToArray();
		}

		/// <summary>
		/// Trims the start.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <returns></returns>
		public static string[] TrimStart(this string[] array)
		{
			return array.Select(v => v.TrimStart()).ToArray();
		}

		/// <summary>
		/// Trims the start.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <param name="trimChars">The trim chars.</param>
		/// <returns></returns>
		public static string[] TrimStart(this string[] array, params char[] trimChars)
		{
			return array.Select(v => v.TrimStart(trimChars)).ToArray();
		}
	}
}
