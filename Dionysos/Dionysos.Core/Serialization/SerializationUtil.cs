﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;

namespace Dionysos
{
    /// <summary>
    /// Singleton utility class used to serialization and deserialization
    /// </summary>
    public class SerializationUtil
    {
        #region Fields

        static SerializationUtil instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the SerializationUtil class if the instance has not been initialized yet
        /// </summary>
        public SerializationUtil()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new SerializationUtil();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static SerializationUtil instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Serializes an object to a file
        /// </summary>
        /// <param name="serializableObject">The object to serialize</param>
        /// <param name="fileName">The name of the file to serialize to object to</param>
        public static void SerializeToFile(object serializableObject, string fileName)
        {
            if (Instance.ArgumentIsEmpty(serializableObject, "serializableObject"))
			{
				// Parameter 'serializableObject' is empty
			}
            else if (Instance.ArgumentIsEmpty(fileName, "fileName"))
            {
                // Parameter 'fileName' is empty
            }
            else
            {
                // Insert code to set properties and fields of the object.
                XmlSerializer xmlSerializer = new XmlSerializer(serializableObject.GetType());

                // To write to a file, create a StreamWriter object.
                StreamWriter streamWriter = new StreamWriter(fileName);
                xmlSerializer.Serialize(streamWriter, serializableObject);
                streamWriter.Close();
            }
        }

        /// <summary>
        /// Deserializes a file to an object
        /// </summary>
        /// <param name="fileName">The name of the file to deserialize the object from</param>
        /// <param name="type">The type of the deserialized object</param>
        /// <returns>An deserialized object of the specified type</returns>
        public static object DeserializeFromFile(string fileName, Type type)
        {
            object serializableObject = null;

            if (Instance.ArgumentIsEmpty(fileName, "fileName"))
            {
                // Parameter 'fileName' is empty
            }
            else
            {
                // Construct an instance of the XmlSerializer with the type
                // of object that is being deserialized.
                XmlSerializer xmlSerializer = new XmlSerializer(type);

                // To read the file, create a FileStream.
                FileStream fileStream = new FileStream(fileName, FileMode.Open);

                // Call the Deserialize method and cast to the object type.
                serializableObject = xmlSerializer.Deserialize(fileStream);
            }

            return serializableObject;
        }

        /// <summary>
        /// Serializes the specified object to a string using a binary formatter
        /// </summary>
        /// <param name="serializableObject">The object to serialize to string</param>
        /// <returns>A System.String instance representing the serialized object</returns>
        public static string SerializeToString(object serializableObject)
        {
            string serializedString = string.Empty;

            if (Instance.ArgumentIsEmpty(serializableObject, "serializableObject"))
            {
                // Parameter 'serializableObject' is empty
            }
            else
            {
                // Create and initialize a formatter and a memory stream
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream memoryStream = new MemoryStream();

                // Serialize the object
                formatter.Serialize(memoryStream, serializableObject);

                // And get the string
                serializedString = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
            }

            return serializedString;
        }

        /// <summary>
        /// Deserializes a string to an object 
        /// </summary>
        /// <param name="serializedString">The string to deserialize</param>
        /// <returns>An System.Object instance containing the deserialized object</returns>
        public static object DeserializeFromString(string serializedString)
        {
            object deserializedObject = null;

            if (Instance.ArgumentIsEmpty(serializedString, "serializedString"))
            {
                // Parameter 'serializedString' is empty
            }
            else
            {
                // Create and initialize a formattter and a memory stream
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream memoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(serializedString));

                // Deserialze the string
                deserializedObject = formatter.Deserialize(memoryStream);
            }

            return deserializedObject;
        }

        #endregion
    }
}