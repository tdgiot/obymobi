﻿using System;
using SendGrid;
using SendGrid.Helpers.Reliability;

namespace Dionysos.Net.Mail
{
	public class CraveSendGridClientOptions : SendGridClientOptions
	{
		public CraveSendGridClientOptions(string apiKey)
		{
			ApiKey = apiKey;
			ReliabilitySettings = new ReliabilitySettings(2, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(3));
		}
	}
}
