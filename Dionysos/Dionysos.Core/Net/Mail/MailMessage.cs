﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;

namespace Dionysos.Net.Mail
{
	/// <summary>
	/// Represents an e-mail message that can be sent using the SmtpClient class.
	/// </summary>
	public class MailMessage : System.Net.Mail.MailMessage
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MailMessage"/> class.
		/// </summary>
		public MailMessage()
		{ }

		#endregion

		#region Methods

		/// <summary>
		/// Adds an attachment to this MailMessage instance
		/// </summary>
		/// <param name="fileName">The path to the file to attach</param>
		/// <returns>True if the file was succesfully attached, False if not</returns>
		public bool AddAttachment(string fileName)
		{
			return this.AddAttachment(fileName, fileName, null);
		}

		/// <summary>
		/// Adds an attachment to this MailMessage instance
		/// </summary>
		/// <param name="fileName">The path to the file to attach</param>
		/// <param name="displayFileName">The filename to show in the email</param>
		/// <returns>True if the file was succesfully attached, False if not</returns>
		public bool AddAttachment(string fileName, string displayFileName)
		{
			return this.AddAttachment(fileName, fileName, null);
		}

		/// <summary>
		/// Adds an attachment to this MailMessage instance
		/// </summary>
		/// <param name="fileName">The path to the file to attach</param>
		/// <param name="displayFileName">The filename to show in the email</param>
		/// <param name="contentType">Type of the content.</param>
		/// <returns>
		/// True if the file was succesfully attached, False if not
		/// </returns>
		public bool AddAttachment(string fileName, string displayFileName, ContentType contentType)
		{
			bool succes = false;

			if (Instance.ArgumentIsEmpty(fileName, "fileName"))
			{
				// Parameter 'fileName' is empty
			}
			else if (File.Exists(fileName))
			{
				Attachment attachment = new Attachment(fileName);
				attachment.Name = displayFileName;
				attachment.ContentType = contentType ?? Dionysos.MimeTypeHelper.GetContentType(fileName);
				this.Attachments.Add(attachment);

				succes = true;
			}

			return succes;
		}

		/// <summary>
		/// Adds an Bcc address to this MailMessage instance
		/// </summary>
		/// <param name="address">The bcc e-mail address to add</param>
		/// <returns>True if the address was succesfully added, False if not</returns>
		public bool AddBccAddress(string address)
		{
			return this.AddBccAddress(address, address);
		}

		/// <summary>
		/// Adds an Bcc address to this MailMessage instance
		/// </summary>
		/// <param name="address">The bcc e-mail address to add</param>
		/// <param name="displayName">Name to display</param>
		/// <returns>True if the address was succesfully added, False if not</returns>
		public bool AddBccAddress(string address, string displayName)
		{
			bool succes = false;

			if (Instance.ArgumentIsEmpty(address, "address"))
			{
				// Parameter 'address' is empty
			}
			else
			{
				EmailAddress emailAddress;
				if (EmailAddress.TryParse(address, out emailAddress))
				{
					MailAddress mailAddress = new MailAddress(address, displayName);
					this.Bcc.Add(mailAddress);

					succes = true;
				}
			}

			return succes;
		}

		/// <summary>
		/// Adds an Cc address to this MailMessage instance
		/// </summary>
		/// <param name="address">The cc e-mail address to add</param>
		/// <returns>True if the address was succesfully added, False if not</returns>
		public bool AddCcAddress(string address)
		{
			return this.AddCcAddress(address, address);
		}

		/// <summary>
		/// Adds an Cc address to this MailMessage instance
		/// </summary>
		/// <param name="address">The cc e-mail address to add</param>
		/// <param name="displayName">Name to display</param>
		/// <returns>True if the address was succesfully added, False if not</returns>
		public bool AddCcAddress(string address, string displayName)
		{
			bool succes = false;

			if (Instance.ArgumentIsEmpty(address, "address"))
			{
				// Parameter 'address' is empty
			}
			else
			{
				EmailAddress emailAddress;
				if (EmailAddress.TryParse(address, out emailAddress))
				{
					MailAddress mailAddress = new MailAddress(address, displayName);
					this.CC.Add(mailAddress);

					succes = true;
				}
			}

			return succes;
		}

		/// <summary>
		/// Adds an recipient to this MailMessage instance
		/// </summary>
		/// <param name="address">The e-mail address to add</param>
		/// <returns>True if the address was succesfully added, False if not</returns>
		public bool AddRecipient(string address)
		{
			return this.AddRecipient(address, address);
		}

		/// <summary>
		/// Adds an recipient to this MailMessage instance
		/// </summary>
		/// <param name="address">The e-mail address to add</param>
		/// <param name="displayName">Name to display</param>
		/// <returns>True if the address was succesfully added, False if not</returns>
		public bool AddRecipient(string address, string displayName)
		{
			bool succes = false;

			if (Instance.ArgumentIsEmpty(address, "address"))
			{
				// Parameter 'address' is empty
			}
			else
			{
				EmailAddress emailAddress;
				if (EmailAddress.TryParse(address, out emailAddress))
				{
					MailAddress mailAddress = new MailAddress(address, displayName);
					this.To.Add(mailAddress);

					succes = true;
				}
			}

			return succes;
		}

		/// <summary>
		/// Parses recipients from a semicolumn separated list of e-mail addresses 
		/// and adds them to this MailMessage instance
		/// </summary>
		/// <param name="addressList">The semicolumn separated list of e-mail addresses</param>
		public void ParseRecipients(string addressList)
		{
			if (Instance.ArgumentIsEmpty(addressList, "addresses"))
			{
				// Parameter 'addresses' is empty
			}
			else
			{
				addressList = addressList.Replace(" ", "");
				string[] addresses = addressList.Split(';');

				for (int i = 0; i < addresses.Length; i++)
				{
					string address = addresses[i];

					EmailAddress emailAddress;
					if (EmailAddress.TryParse(address, out emailAddress))
					{
						MailAddress mailAddress = new MailAddress(address);
						this.To.Add(mailAddress);
					}
				}
			}
		}

		/// <summary>
		/// Adds a sender to this MailMessage instance
		/// </summary>
		/// <param name="address">The e-mail address of the sender</param>
		/// <returns>True if the address was sucesfully added, False if not</returns>
		public bool AddSender(string address)
		{
			return AddSender(address, address);
		}

		/// <summary>
		/// Adds a sender to this MailMessage instance
		/// </summary>
		/// <param name="address">The e-mail address of the sender</param>
		/// <param name="displayName">Name to display</param>
		/// <returns>True if the address was sucesfully added, False if not</returns>
		public bool AddSender(string address, string displayName)
		{
			bool succes = false;

			if (Instance.ArgumentIsEmpty(address, "address"))
			{
				// Parameter 'address' is empty
			}
			else
			{
				EmailAddress emailAddress;
				if (EmailAddress.TryParse(address, out emailAddress))
				{
					MailAddress mailAddress = new MailAddress(address, displayName);
					this.From = mailAddress;

					succes = true;
				}
			}

			return succes;
		}

		#endregion
	}
}
