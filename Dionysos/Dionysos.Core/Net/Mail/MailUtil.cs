﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using Dionysos.Parsing;
using System.Web;

namespace Dionysos.Net.Mail
{
	/// <summary>
	/// Singleton utility class which can be used to send e-mail messages
	/// </summary>
	public class MailUtil
	{
		#region Fields

		static MailUtil instance = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs a static instance of the Dionysos.Net.Mail.MailUtil class
		/// </summary>
		static MailUtil()
		{
			if (instance == null)
			{
				// first create the instance
				instance = new MailUtil();
				// Then init the instance (the init needs the instance to fill it)
				instance.Init();
			}
		}


		#endregion

		#region Methods

		private void Init()
		{
		}

		/// <summary>
		/// Retrieves a SmtpClient using the SmtpSettings via the ConfigurationProvider 
		/// </summary>
		/// <returns></returns>
		public static SmtpClient GetSmtpClient()
		{
			SmtpClient smtpClient = new SmtpClient();
			smtpClient.Host = Dionysos.Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.SmtpHost);
			smtpClient.Port = Dionysos.Global.ConfigurationProvider.GetInt(DionysosConfigurationConstants.SmtpPort);
			smtpClient.Timeout = Global.ConfigurationProvider.GetInt(DionysosConfigurationConstants.SmtpTimeout);
			smtpClient.EnableSsl = ConfigurationManager.GetBool(DionysosConfigurationConstants.SmtpEnableSsl);

			// Optional values
			string smtpUserName = Dionysos.Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.SmtpUsername);
			string smtpPassword = Dionysos.Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.SmtpPassword);
			if (!String.IsNullOrEmpty(smtpUserName) || !String.IsNullOrEmpty(smtpPassword))
			{
				smtpClient.Credentials = new System.Net.NetworkCredential(smtpUserName, smtpPassword);
			}

			return smtpClient;
		}

		/// <summary>
		/// Sends an e-mail message using the SmtpSettings via the ConfigurationProvider
		/// </summary>
		/// <param name="message">The message to be sent</param>
		/// <returns>True if sending the mailmessage was succesful, False if not</returns>
		public static bool SendMail(MailMessage message)
		{
			return SendMail(message, string.Empty, string.Empty);
		}

		/// <summary>
		/// Sends an e-mail message using the supplied SmtpServer
		/// </summary>
		/// <param name="message">The message to be sent</param>
		/// <param name="smtpClient">The SMTP client.</param>
		/// <returns>
		/// True if sending the mailmessage was succesful, False if not
		/// </returns>
		public static bool SendMail(MailMessage message, SmtpClient smtpClient)
		{
			return SendMail(message, string.Empty, string.Empty, smtpClient);
		}

		/// <summary>
		/// Sends an e-mail message using the SmtpSettings via the ConfigurationProvider
		/// If no recipient or sender is filled is loaded also from the settings
		/// </summary>
		/// <param name="message">The message to be sent</param>
		/// <param name="toAddress">Leave blank for default</param>
		/// <param name="toDisplayName">Leave blank for default</param>        
		/// <returns>
		/// True if sending the mailmessage was succesful, False if not
		/// </returns>
		public static bool SendMail(MailMessage message, string toAddress, string toDisplayName)
		{
			return SendMail(message, toAddress, toDisplayName, null);
		}

		/// <summary>
		/// Sends an e-mail message using the SmtpSettings via the ConfigurationProvider
		/// If no recipient or sender is filled is loaded also from the settings
		/// </summary>
		/// <param name="message">The message to be sent</param>
		/// <param name="toAddress">Leave blank for default</param>
		/// <param name="toDisplayName">Leave blank for default</param>
		/// <param name="smtpClient">The SMTP client to send with or null to get a new instance.</param>
		/// <returns>
		/// True if sending the mailmessage was succesful, False if not
		/// </returns>
		public static bool SendMail(MailMessage message, string toAddress, string toDisplayName, SmtpClient smtpClient)
		{
			if (smtpClient == null)
				smtpClient = GetSmtpClient();

			bool success = false;

			// Add default recipient if not filled
			if (message.To.Count == 0)
			{
				string recipientAddress, recipientName = null;
				if (String.IsNullOrEmpty(toAddress))
				{
					recipientAddress = Dionysos.Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.EmailRecipientAddress);
					recipientName = Dionysos.Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.EmailRecipientName);
				}
				else
				{
					recipientAddress = toAddress;
					if (String.IsNullOrEmpty(toDisplayName)) recipientName = toDisplayName;
				}

				MailAddress address = new MailAddress(recipientAddress, recipientName);
				message.To.Add(address);
			}

			// Add default sender if not filled
			if (message.From == null || String.IsNullOrEmpty(message.From.Address))
			{
				string fromAddress = Dionysos.Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.EmailSenderAddress);
				string fromName = Dionysos.Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.EmailSenderName);
				MailAddress address = new MailAddress(fromAddress, fromName);
				message.From = address;
			}

			// Only do footer if not using multiple views
			string orgBody = message.Body;
			if (message.AlternateViews.Count == 0)
			{
				string emailFooter = Dionysos.Global.ConfigurationProvider.GetString(DionysosConfigurationConstants.EmailFooter);
				orgBody = message.Body;
				if (emailFooter.Length > 0)
				{
					if (message.IsBodyHtml)
						message.Body += "<br><br><br>" + emailFooter;
					else
						message.Body += "\r\n\r\n\r\n" + emailFooter;
				}
			}

			smtpClient.Send(message);
			success = true;

			// Return org body without the footer
			if (message.AlternateViews.Count == 0)
			{
				message.Body = orgBody;
			}

			return success;
		}

		/// <summary>
		/// Sends an e-mail message using the SmtpSettings via the ConfigurationProvider
		/// If no recipient or sender is filled is loaded also from the settings
		/// </summary>
		/// <param name="subject">The subject of the message to be sent</param>
		/// <param name="body">The body of the message to be sent</param>
		/// <param name="toAddress">Leave blank for default</param>
		/// <param name="toDisplayName">Leave blank for default</param>
		/// <returns>True if sending the mailmessage was succesful, False if not</returns>
		public static bool SendMail(string subject, string body, string toAddress, string toDisplayName)
		{
			return SendMail(subject, body, toAddress, toDisplayName, false);
		}

		/// <summary>
		/// Sends an e-mail message using the SmtpSettings via the ConfigurationProvider
		/// If no recipient or sender is filled is loaded also from the settings
		/// </summary>
		/// <param name="subject">The subject of the message to be sent</param>
		/// <param name="body">The body of the message to be sent</param>
		/// <param name="toAddress">Leave blank for default</param>
		/// <param name="toDisplayName">Leave blank for default</param>
		/// <param name="isBodyHtml">Flag which indicates if the body is in html format</param>
		/// <param name="alternateViews">The attachment collection used to store alternate forms of the message body</param>
		/// <returns>True if sending the mailmessage was succesful, False if not</returns>
		public static bool SendMail(string subject, string body, string toAddress, string toDisplayName, bool isBodyHtml, params AlternateView[] alternateViews)
		{
			// Create a MailMessage instance
			MailMessage mailMessage = new MailMessage();
			mailMessage.Subject = subject;
			mailMessage.Body = body;
			mailMessage.IsBodyHtml = isBodyHtml;
			for (int i = 0; i < alternateViews.Length; i++)
			{
				mailMessage.AlternateViews.Add(alternateViews[i]);
			}

			// and send it
			return SendMail(mailMessage, toAddress, toDisplayName);
		}

		/// <summary>
		/// Sends an e-mail message using the SmtpSettings via the ConfigurationProvider
		/// If no recipient or sender is filled is loaded also from the settings
		/// </summary>
		/// <param name="subject">The subject of the message to be sent</param>
		/// <param name="body">The body of the message to be sent</param>
		/// <param name="toAddress">Leave blank for default</param>
		/// <param name="toDisplayName">Leave blank for default</param>
		/// <param name="isBodyHtml">Flag which indicates if the body is in html format</param>
		/// <returns>True if sending the mailmessage was succesful, False if not</returns>
		public static bool SendMail(string subject, string body, string toAddress, string toDisplayName, bool isBodyHtml)
		{
			// Create a MailMessage instance
			MailMessage mailMessage = new MailMessage();
			mailMessage.Subject = subject;
			mailMessage.Body = body;
			mailMessage.IsBodyHtml = isBodyHtml;

			// Check whether the body is in html
			if (isBodyHtml)
			{
				List<string[]> resources = new List<string[]>();

				mailMessage.Body = ParseHTMLHelper.ParseSrcElements(mailMessage.Body, string.Empty, string.Empty);
				mailMessage.Body = ParseHTMLHelper.ParseImgSrcToCid(mailMessage.Body, out resources);

				AlternateView alternateView = AlternateView.CreateAlternateViewFromString(mailMessage.Body, null, "text/html");

				for (int i = 0; i < resources.Count; i++)
				{
					string[] resource = resources[i];

					LinkedResource linkedResource = new LinkedResource(resource[1]);
					linkedResource.ContentId = resource[0];
					linkedResource.ContentType = Dionysos.MimeTypeHelper.GetContentType(resource[1]);
					linkedResource.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
					alternateView.LinkedResources.Add(linkedResource);
				}

				mailMessage.AlternateViews.Add(alternateView);
			}

			// and send it
			return SendMail(mailMessage, toAddress, toDisplayName);
		}

		/// <summary>
		/// Sends an e-mail message using the specified parameters
		/// </summary>
		/// <param name="host">The host which has to sent the e-mail message</param>
		/// <param name="senderAddress">The e-mail address of the sender</param>
		/// <param name="text">The text to send in the e-mail message</param>
		/// <param name="subject">the subject of the e-mail message</param>
		/// <param name="recipientAddress">The address of the recipient. This can also be a semicolumn separated list of e-mail addresses.</param>
		/// <param name="attachmentFileNames">The filenames of the attachments</param>
		/// <returns>True if sending the mailmessage was succesful, False if not</returns>
		public static bool SendMail(string host, string senderAddress, string text, string subject, string recipientAddress, params string[] attachmentFileNames)
		{
			bool success = false;

			if (Instance.ArgumentIsEmpty(host, "host"))
			{
				// Parameter 'host' is empty
			}
			else if (Instance.ArgumentIsEmpty(senderAddress, "senderAddress"))
			{
				// Parameter 'senderAddress' is empty
			}
			else if (Instance.ArgumentIsEmpty(text, "text"))
			{
				// Parameter 'text' is empty
			}
			else if (Instance.ArgumentIsEmpty(subject, "subject"))
			{
				// Parameter 'subject' is empty
			}
			else if (Instance.ArgumentIsEmpty(recipientAddress, "recipientAddress"))
			{
				// Parameter 'recipientAddress' is empty
			}
			else
			{
				SmtpClient smtpClient = new SmtpClient();
				smtpClient.Host = host;

				MailMessage mailMessage = new MailMessage();
				mailMessage.AddSender(senderAddress);
				mailMessage.Subject = subject;
				mailMessage.Body = text;
				mailMessage.ParseRecipients(recipientAddress);

				if (!Instance.Empty(attachmentFileNames) && attachmentFileNames.Length > 0)
				{
					for (int i = 0; i < attachmentFileNames.Length; i++)
					{
						mailMessage.AddAttachment(attachmentFileNames[i]);
					}
				}

				smtpClient.Send(mailMessage);
				success = true;
			}

			return success;
		}

		/// <summary>
		/// Sends an e-mail message asynchronously using the specified parameters
		/// </summary>
		/// <param name="host">The host which has to sent the e-mail message</param>
		/// <param name="senderAddress">The e-mail address of the sender</param>
		/// <param name="text">The text to send in the e-mail message</param>
		/// <param name="subject">the subject of the e-mail message</param>
		/// <param name="recipientAddress">The address of the recipient. This can also be a semicolumn separated list of e-mail addresses.</param>
		/// <param name="attachmentFileNames">The filenames of the attachments</param>
		/// <returns>True if sending the mailmessage was succesful, False if not</returns>
		public static void SendMailAsync(string host, string senderAddress, string text, string subject, string recipientAddress, params string[] attachmentFileNames)
		{
			if (Instance.ArgumentIsEmpty(host, "host"))
			{
				// Parameter 'host' is empty
			}
			else if (Instance.ArgumentIsEmpty(senderAddress, "senderAddress"))
			{
				// Parameter 'senderAddress' is empty
			}
			else if (Instance.ArgumentIsEmpty(text, "text"))
			{
				// Parameter 'text' is empty
			}
			else if (Instance.ArgumentIsEmpty(subject, "subject"))
			{
				// Parameter 'subject' is empty
			}
			else if (Instance.ArgumentIsEmpty(recipientAddress, "recipientAddress"))
			{
				// Parameter 'recipientAddress' is empty
			}
			else
			{
				SmtpClient smtpClient = new SmtpClient();
				smtpClient.Host = host;

				MailMessage mailMessage = new MailMessage();
				mailMessage.AddSender(senderAddress);
				mailMessage.Subject = subject;
				mailMessage.Body = text;
				mailMessage.ParseRecipients(recipientAddress);

				if (!Instance.Empty(attachmentFileNames) && attachmentFileNames.Length > 0)
				{
					for (int i = 0; i < attachmentFileNames.Length; i++)
					{
						mailMessage.AddAttachment(attachmentFileNames[i]);
					}
				}

				smtpClient.SendAsync(mailMessage, null);
			}
		}

		#endregion
	}
}
