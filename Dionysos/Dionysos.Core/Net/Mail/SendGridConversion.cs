﻿using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Dionysos.Net.Mail
{
	public static class SendGridConversion
	{
		public static SendGrid.Helpers.Mail.EmailAddress ToSendGridEmailAddress(this MailAddress self) => new SendGrid.Helpers.Mail.EmailAddress(self.Address)
		{
			Name = self.DisplayName
		};

		public static IEnumerable<SendGrid.Helpers.Mail.EmailAddress> ToSendGridEmailAddress(this MailAddressCollection self) => self.Select(address => address.ToSendGridEmailAddress());

		public static async Task<SendGridMessage> ToSendGridAttachmentsAsync(this SendGridMessage self, AttachmentCollection attachments)
		{
			foreach (var attachment in attachments)
			{
				await self.AddAttachmentAsync(attachment);
			}

			return self;
		}

		public static async Task AddAttachmentAsync(this SendGridMessage self, System.Net.Mail.Attachment attachment) => await self.AddAttachmentAsync(attachment.Name, attachment.ContentStream, attachment.ContentType.MediaType);

		public static async Task<SendGridMessage> ToSendGridMessage(this MailMessage self)
		{
			string plainTextContent = string.Empty;
			string htmlContent = string.Empty;
			LinkedResourceCollection linkedResources = null;

			if (self.IsBodyHtml)
			{
				htmlContent = self.Body;
			}
			else
			{
				plainTextContent = self.Body;

				AlternateView htmlView = self.AlternateViews.FirstOrDefault(view => view.ContentType.MediaType == "text/html");
				if (htmlView != null)
				{
					Stream contentStream = new MemoryStream();
					htmlView.ContentStream.CopyTo(contentStream);
					contentStream.Position = 0;
					using (var streamReader = new StreamReader(contentStream))
					{
						htmlContent = await streamReader.ReadToEndAsync();
					}

					linkedResources = htmlView.LinkedResources;
				}
			}

			SendGridMessage sendGridMessage = MailHelper.CreateSingleEmailToMultipleRecipients(
				self.From.ToSendGridEmailAddress(),
				self.To.ToSendGridEmailAddress().ToList(),
				self.Subject,
				plainTextContent,
				htmlContent);

			AddIfAny(sendGridMessage.AddCcs, self.CC);
			AddIfAny(sendGridMessage.AddBccs, self.Bcc);

			return await (await sendGridMessage
				.ToSendGridAttachmentsAsync(linkedResources))
				.ToSendGridAttachmentsAsync(self.Attachments);
		}

		private static async Task<SendGridMessage> ToSendGridAttachmentsAsync(this SendGridMessage self, LinkedResourceCollection linkedResources)
		{
			if (linkedResources != null)
			{
				foreach (LinkedResource linkedResource in linkedResources)
				{
					await self.AddAttachmentAsync(linkedResource.ContentId, linkedResource.ContentStream, linkedResource.ContentType.MediaType, "inline", linkedResource.ContentId);
				}
			}

			return self;
		}

		private static void AddIfAny(Action<List<SendGrid.Helpers.Mail.EmailAddress>, int, Personalization> add, MailAddressCollection emailAddresses)
		{
			if (emailAddresses.Any())
			{
				add(emailAddresses.ToSendGridEmailAddress().ToList(), 0, null);
			}
		}
	}
}
 