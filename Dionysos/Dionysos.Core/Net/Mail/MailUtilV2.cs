﻿using Dionysos.Parsing;
using SendGrid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Dionysos.Net.Mail
{
	/// <summary>
	/// Class for sending emails with HTML content and template variables.
	/// </summary>
	public class MailUtilV2
	{
		/// <summary>
		/// The recipient addresses.
		/// </summary>
		private MailAddressCollection to;

		/// <summary>
		/// The carbon copy recipient addresses.
		/// </summary>
		private MailAddressCollection cc;

		/// <summary>
		/// The blind carbon copy recipient addresses.
		/// </summary>
		private MailAddressCollection bcc;

		/// <summary>
		/// The attachments.
		/// </summary>
		private List<System.Net.Mail.Attachment> attachments;

		/// <summary>
		/// The plain text body of the last sent mail.
		/// </summary>
		private string lastSentBodyPlainText = String.Empty;

		/// <summary>
		/// The HTML body of the last sent mail.
		/// </summary>
		private string lastSentBodyHtml = String.Empty;

		/// <summary>
		/// The SMTP client.
		/// </summary>
		private System.Net.Mail.SmtpClient smtpClient = null;

		/// <summary>
		/// Streams for the images of the email (can be used instead of file in the file system)
		/// </summary>
		Dictionary<string, MemoryStream> fileMemoryStreams = null;

		/// <summary>
		/// Gets or sets sender address.
		/// </summary>
		/// <value>
		/// The sender address.
		/// </value>
		public MailAddress From { get; set; }

		/// <summary>
		/// Gets or sets the recipient addresses.
		/// </summary>
		/// <value>
		/// The recipient addresses.
		/// </value>
		public MailAddressCollection To
		{
			get
			{
				return this.to ?? (this.to = new MailAddressCollection());
			}
			set
			{
				this.to = value;
			}
		}

		/// <summary>
		/// Gets or sets the carbon copy recipient addresses.
		/// </summary>
		/// <value>
		/// The carbon copy recipient addresses.
		/// </value>
		public MailAddressCollection CC
		{
			get
			{
				return this.cc ?? (this.cc = new MailAddressCollection());
			}
			set
			{
				this.cc = value;
			}
		}

		/// <summary>
		/// Gets or sets the blind carbon copy recipient addresses.
		/// </summary>
		/// <value>
		/// The blind carbon copy recipient addresses.
		/// </value>
		public MailAddressCollection Bcc
		{
			get
			{
				return this.bcc ?? (this.bcc = new MailAddressCollection());
			}
			set
			{
				this.bcc = value;
			}
		}

		/// <summary>
		/// Gets or sets the subject.
		/// </summary>
		public string Subject { get; set; }

		/// <summary>
		/// Gets or sets the reply to address.
		/// </summary>
		/// <value>
		/// The reply to address.
		/// </value>
		public MailAddress ReplyTo { get; set; }

		/// <summary>
		/// Gets or sets the attachments.
		/// </summary>
		public List<System.Net.Mail.Attachment> Attachments
		{
			get => this.attachments ?? (this.attachments = new List<System.Net.Mail.Attachment>());
			set => this.attachments = value;
		}

		/// <summary>
		/// Gets or sets the plain text of the body.
		/// </summary>
		public string BodyPlainText { get; set; }

		/// <summary>
		/// Gets or sets the HTML of the body.
		/// </summary>
		/// <remarks>
		/// If the HTML includes images to be parsed as inline attachments, use ImagesSrcRootPath to set a relative or physical path to the images root folder.
		/// </remarks>
		public string BodyHtml { get; set; }

		/// <summary>
		/// Gets or sets the source root path (relative of physical) form the parsed inline attachments.
		/// </summary>
		public string ImagesSrcRootPath { get; set; }

		/// <summary>
		/// Gets the plain text body of the last sent mail.
		/// </summary>
		public string LastSentBodyPlainText
		{
			get
			{
				return this.lastSentBodyPlainText;
			}
		}

		/// <summary>
		/// Gets the HTML body of the last sent mail.
		/// </summary>
		public string LastSentBodyHtml
		{
			get
			{
				return this.lastSentBodyHtml;
			}
		}

		/// <summary>
		/// Gets or sets the SMTP client.
		/// </summary>
		/// <value>
		/// The SMTP client.
		/// </value>
		public System.Net.Mail.SmtpClient SmtpClient
		{
			get
			{
				if (this.smtpClient == null)
				{
					this.smtpClient = new System.Net.Mail.SmtpClient();
					this.smtpClient.Host = ConfigurationManager.GetString(DionysosConfigurationConstants.SmtpHost);
					this.smtpClient.Port = ConfigurationManager.GetInt(DionysosConfigurationConstants.SmtpPort);
					this.smtpClient.EnableSsl = ConfigurationManager.GetBool(DionysosConfigurationConstants.SmtpEnableSsl);
					this.smtpClient.Timeout = ConfigurationManager.GetInt(DionysosConfigurationConstants.SmtpTimeout);

					string username = ConfigurationManager.GetString(DionysosConfigurationConstants.SmtpUsername),
						   password = ConfigurationManager.GetString(DionysosConfigurationConstants.SmtpPassword);
					if (!username.IsNullOrWhiteSpace() &&
						!password.IsNullOrWhiteSpace())
					{
						this.smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
					}
				}

				return this.smtpClient;
			}
			set
			{
				this.smtpClient = value;
			}
		}

		/// <summary>
		/// Gets or sets the SentGrid client.
		/// </summary>
		/// <value>
		/// The SendGrid client.
		/// </value>
		private ISendGridClient SendGridClient => CreateSendGridClient();

		private ISendGridClient CreateSendGridClient()
		{
			string apiKey = System.Configuration.ConfigurationManager.AppSettings[DionysosConfigurationConstants.SendGridApiKey];
			return !string.IsNullOrEmpty(apiKey) ? (ISendGridClient)new SendGridClient(new CraveSendGridClientOptions(apiKey)) : null;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MailUtilV2" /> class.
		/// </summary>
		/// <param name="fromAddress">The sender address.</param>
		/// <param name="toAddress">The recipient address.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="fileMemoryStreams">Streams to use as embedded images or attachments. If they are used as images they are embedded, otherwise added as attachments. When this is filled the images will NOT be retrieved from the file system, only from these streams.</param>
		public MailUtilV2(string fromAddress, string toAddress, string subject, Dictionary<string, MemoryStream> fileMemoryStreams = null)
			: this(fromAddress, null, toAddress, null, subject, fileMemoryStreams)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="MailUtilV2"/> class.
		/// </summary>
		/// <param name="fromAddress">The sender address.</param>
		/// <param name="fromDisplayName">The sender display name.</param>
		/// <param name="toAddress">The recipient address.</param>
		/// <param name="toDisplayName">The recipient display name.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="fileMemoryStreams">Streams to use as embedded images or attachments. If they are used as images they are embedded, otherwise added as attachments. When this is filled the images will NOT be retrieved from the file system, only from these streams.</param>
		public MailUtilV2(string fromAddress, string fromDisplayName, string toAddress, string toDisplayName, string subject, Dictionary<string, MemoryStream> fileMemoryStreams = null)
			: this(new MailAddress(fromAddress, fromDisplayName), new MailAddress(toAddress, toDisplayName), subject, fileMemoryStreams)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="MailUtilV2"/> class.
		/// </summary>
		/// <param name="from">The sender address.</param>
		/// <param name="to">The recipient address.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="fileMemoryStreams">Streams to use as embedded images or attachments. If they are used as images they are embedded, otherwise added as attachments. When this is filled the images will NOT be retrieved from the file system, only from these streams.</param>
		public MailUtilV2(MailAddress from, MailAddress to, string subject, Dictionary<string, MemoryStream> fileMemoryStreams = null)
		{
			this.From = from;
			this.To.Add(to);
			this.Subject = subject;
			this.fileMemoryStreams = fileMemoryStreams;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MailUtilV2"/> class.
		/// </summary>
		/// <param name="from">The sender address.</param>
		/// <param name="to">The recipients addresses.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="fileMemoryStreams">Streams to use as embedded images or attachments. If they are used as images they are embedded, otherwise added as attachments. When this is filled the images will NOT be retrieved from the file system, only from these streams.</param>
		public MailUtilV2(MailAddress from, MailAddressCollection to, string subject, Dictionary<string, MemoryStream> fileMemoryStreams = null)
		{
			this.From = from;
			this.To = to;
			this.Subject = subject;
			this.fileMemoryStreams = fileMemoryStreams;
		}

		/// <summary>
		/// Determine the estimated size of the email to be sent in kilo bytes
		/// </summary>
		/// <param name="mailMessage"></param>
		/// <returns></returns>
		public static long GetMailSizeInKbytes(MailMessage mailMessage)
		{
			decimal size = 0;

			// Get sizes of Alternate Views & their linked resources
			foreach (var altview in mailMessage.AlternateViews)
			{
				size += altview.ContentStream.Length / 0.75m; // .75 for BASE64 encoding

				foreach (var linkedResource in altview.LinkedResources)
				{
					size += linkedResource.ContentStream.Length / 0.75m;
				}
			}

			// Get Sizes of attachments
			foreach (var att in mailMessage.Attachments)
			{
				size += att.ContentStream.Length / 0.75m; // .75 for BASE64 encoding
			}

			// We assume body is not used but only AlternateViews, don't add that size.
			return (long)(size / 1024L);
		}

		/// <summary>
		/// Sends the mail.
		/// </summary>
		public async Task SendMail() => await this.SendMail(null);

		/// <summary>
		/// Sends the mail.
		/// </summary>
		/// <param name="templateVariables">The template variables.</param>
		public async Task SendMail(StringDictionary templateVariables)
		{
			// Create mail message and dispose after use
			using (MailMessage mail = this.CreateMailMessage(templateVariables))
			{
				ISendGridClient mailClient = this.SendGridClient;
				if (mailClient != null)
				{
					SendGrid.Helpers.Mail.SendGridMessage sendGridMessage = await mail.ToSendGridMessage().ConfigureAwait(false);
					var result = await mailClient.SendEmailAsync(sendGridMessage).ConfigureAwait(false);
					if (result.StatusCode != HttpStatusCode.Accepted)
					{
						throw new InvalidOperationException($"Failed to send email message ({result.StatusCode}): {await result.Body.ReadAsStringAsync()}");
					}
				}
				else
				{
					this.SmtpClient.Send(mail);
				}
			}
		}

		/// <summary>
		/// Creates the mail message.
		/// </summary>
		/// <param name="templateVariables">The template variables.</param>
		/// <returns>
		/// The mail message.
		/// </returns>
		private MailMessage CreateMailMessage(StringDictionary templateVariables)
		{
			// Create the mail message
			MailMessage mail = new MailMessage();

			// Set sender and recipients
			mail.From = this.From;
			foreach (MailAddress address in this.To)
			{
				mail.To.Add(address);
			}
			foreach (MailAddress address in this.CC)
			{
				mail.CC.Add(address);
			}
			foreach (MailAddress address in this.Bcc)
			{
				mail.Bcc.Add(address);
			}

			if (this.ReplyTo != null) // GK Null check is required.
			{
				mail.ReplyToList.Add(this.ReplyTo);
			}

			string subject = this.Subject;
			if (!String.IsNullOrEmpty(subject) &&
				templateVariables != null &&
				templateVariables.Count > 0)
			{
				// Replace template variables
				subject = this.ReplaceTemplateVariables(subject, templateVariables);
			}
			mail.Subject = subject;			

			// Arrange the body
			string handledBodyHtml = this.BodyHtml, handledBodyPlainText = this.BodyPlainText;
			Dictionary<string, Stream> additionalAttachments = new Dictionary<string, Stream>();
			if (!String.IsNullOrEmpty(handledBodyHtml))
			{
				// Replace template variables
				if (templateVariables != null &&
					templateVariables.Count > 0)
				{
					handledBodyHtml = this.ReplaceTemplateVariables(handledBodyHtml, templateVariables);
				}
				
				List<string[]> resources = new List<string[]>();

				// Create HTML View
				AlternateView htmlView;                
				htmlView = CreateHtmlViewAndLinkedResources(ref handledBodyHtml, ref resources, out additionalAttachments);				
				mail.AlternateViews.Add(htmlView);

				// Add plain text view
				if (!String.IsNullOrEmpty(handledBodyPlainText))
				{
					// Replace template variables
					if (templateVariables != null &&
						templateVariables.Count > 0)
					{
						handledBodyPlainText = this.ReplaceTemplateVariables(handledBodyPlainText, templateVariables);
					}

					AlternateView plainTextView = AlternateView.CreateAlternateViewFromString(handledBodyPlainText, null, "text/plain");
					mail.AlternateViews.Add(plainTextView);
				}
			}
			else if (!String.IsNullOrEmpty(handledBodyPlainText))
			{
				// Replace template variables
				if (templateVariables != null &&
					templateVariables.Count > 0)
				{
					handledBodyPlainText = this.ReplaceTemplateVariables(handledBodyPlainText, templateVariables);
				}

				// Add plain text body
				mail.Body = handledBodyPlainText;
			}
			else
			{
				throw new InvalidOperationException("No plain text or HTML body set.");
			}
			
			// Add the attachments from the .Attachments property and the additional attachments from the .fileMemoryStreams
			this.AddAttachments(mail, additionalAttachments);

			// Set last sent bodies
			this.lastSentBodyHtml = handledBodyHtml;
			this.lastSentBodyPlainText = handledBodyPlainText;

			return mail;
		}

		private void AddAttachments(MailMessage mail, Dictionary<string, Stream> additionalAttachments)
		{
			// Add 'standard' attachments
			foreach (System.Net.Mail.Attachment attachment in this.Attachments)
			{
				// Seek to the begin of the original stream
				if (attachment.ContentStream.CanSeek)
				{
					attachment.ContentStream.Seek(0, SeekOrigin.Begin);
				}

				// Copy stream so attachments can be sent multiple times (stream is disposed by MailMessage)
				Stream stream = new MemoryStream();
				attachment.ContentStream.Copy(stream);
				stream.Seek(0, SeekOrigin.Begin);

				// Add attachment
				mail.Attachments.Add(new System.Net.Mail.Attachment(stream, attachment.Name)
				{
					ContentId = attachment.ContentId,
					ContentType = attachment.ContentType,
					NameEncoding = attachment.NameEncoding,
					TransferEncoding = attachment.TransferEncoding
				});
			}

			// Add additional Attachments from the fileMemoryStreams
			foreach (var attachment in additionalAttachments)
			{
				Stream stream = new MemoryStream();
				attachment.Value.Copy(stream);
				stream.Seek(0, SeekOrigin.Begin);

				// Add attachment                
				mail.Attachments.Add(new System.Net.Mail.Attachment(stream, attachment.Key)
				{
					ContentType = MimeTypeHelper.GetContentType(attachment.Key)
				});
			}
		}

		private AlternateView CreateHtmlViewAndLinkedResources(ref string handledBodyHtml, ref List<string[]> resources, out Dictionary<string, Stream> additionalAttachments)
		{
			additionalAttachments = new Dictionary<string, Stream>();
			AlternateView htmlView;
			if (this.fileMemoryStreams != null)
			{
				// Parse HTML for images based on supplied image memory streams
				handledBodyHtml = ParseHTMLHelper.ParseImgSrcToCid(handledBodyHtml, false, string.Empty, out resources);
				htmlView = AlternateView.CreateAlternateViewFromString(handledBodyHtml, null, "text/html");

				List<string> usedFileMemoryStreams = new List<string>();
				for (int i = 0; i < resources.Count; i++)
				{
					string[] resource = resources[i];

					LinkedResource linkedResource;
					string memoryStreamKey = null;
					if (this.fileMemoryStreams != null)
					{
						memoryStreamKey = this.fileMemoryStreams.Keys.FirstOrDefault(x => x.Equals(resource[1], StringComparison.InvariantCultureIgnoreCase));
					}

					if (!memoryStreamKey.IsNullOrWhiteSpace())
					{
						try
						{
							// Copy because otherwise the stream will be closed i the message is sent and can't be reused.
							usedFileMemoryStreams.Add(memoryStreamKey);
							MemoryStream msCopy = new MemoryStream((int)this.fileMemoryStreams[memoryStreamKey].Length);
							this.fileMemoryStreams[memoryStreamKey].Position = 0;
							this.fileMemoryStreams[memoryStreamKey].CopyTo(msCopy);
							msCopy.Position = 0;
							linkedResource = new LinkedResource(msCopy);
							linkedResource.ContentId = resource[0];
							linkedResource.ContentType = MimeTypeHelper.GetContentType(resource[1]);
							linkedResource.TransferEncoding = TransferEncoding.Base64;
							htmlView.LinkedResources.Add(linkedResource);
						}
						catch (Exception ex)
						{
							// GK Best to send an email any way, even if the images fail, therefore this catch.
							Debug.WriteLine("MailUtilV2.CreateHtmlViewAndLinkedResouces - ERROR: LinkedResources addition failed: {0}\r\n{1}", ex.Message, ex.StackTrace);
							if (TestUtil.IsPcDeveloper)
							{
								throw;
							}
						}
					}
				}

				// Add non-used streams as Attachments
				foreach (var stream in this.fileMemoryStreams)
				{
					if (!usedFileMemoryStreams.Contains(stream.Key))
					{ 
						// Not used, so use as an Attachment
						additionalAttachments.Add(stream.Key, stream.Value);
					}
				}
			}
			else
			{
				// Pars HTML for images based on images on the file system.
				handledBodyHtml = ParseHTMLHelper.ParseImgSrcToCid(handledBodyHtml, true, this.ImagesSrcRootPath, out resources);
				htmlView = AlternateView.CreateAlternateViewFromString(handledBodyHtml, null, "text/html");

				for (int i = 0; i < resources.Count; i++)
				{
					string[] resource = resources[i];

					try
					{
						LinkedResource linkedResource = new LinkedResource(resource[1]);
						linkedResource.ContentId = resource[0];
						linkedResource.ContentType = MimeTypeHelper.GetContentType(resource[1]);
						linkedResource.TransferEncoding = TransferEncoding.Base64;
						htmlView.LinkedResources.Add(linkedResource);
					}
					catch (Exception ex)
					{
						// GK Best to send an email any way, even if the images fail, therefore this catch.
						Debug.WriteLine("MailUtilV2.CreateHtmlViewAndLinkedResouces - ERROR: LinkedResources addition failed: {0}\r\n{1}", ex.Message, ex.StackTrace);
						if (TestUtil.IsPcDeveloper)
						{
							throw;
						}
					}
				}
			}
			return htmlView;
		}

		/// <summary>
		/// Replaces the template variables.
		/// </summary>
		/// <param name="template">The template.</param>
		/// <param name="templateVariables">The template variables.</param>
		/// <returns>The template with all template variables replaced.</returns>
		private string ReplaceTemplateVariables(string template, StringDictionary templateVariables)
		{
			foreach (DictionaryEntry de in templateVariables)
			{
				template = StringUtil.Replace(template, String.Concat("[[", de.Key, "]]"), (string)de.Value, StringComparison.OrdinalIgnoreCase);
			}

			return template;
		}
	}
}
 