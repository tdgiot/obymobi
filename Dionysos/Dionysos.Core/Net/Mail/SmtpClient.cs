﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace Dionysos.Net.Mail
{
    /// <summary>
    /// Class which is being used for sending e-mail messages
    /// </summary>
    public class SmtpClient : System.Net.Mail.SmtpClient
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Net.Mail.SmtpClient class
        /// </summary>
        public SmtpClient()
        {            
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sends an e-mail message using this SmtpClient instance
        /// </summary>
        /// <param name="mailMessage">The System.Net.Mail.MailMessage instance to send</param>
        public new void Send(System.Net.Mail.MailMessage mailMessage)
        {
            bool succes = true;

            if (Instance.ArgumentIsEmpty(mailMessage, "mailMessage"))
            {
                // Parameter 'mailMessage' is empty
                throw new EmptyException("MailMessage is Empty");
            }
            else
            {
                if (Instance.Empty(this.Host))
                {
                    throw new EmptyException("SmtpHost is Empty");                    
                }
                else if (Instance.Empty(mailMessage.To) || mailMessage.To.Count == 0)
                {
                    throw new EmptyException("To is empty (recipients)");
                }
                else if (Instance.Empty(mailMessage.From) || Instance.Empty(mailMessage.From.Address))
                {
                    throw new EmptyException("From is empty");
                }

                if (succes)
                {
                    base.Send(mailMessage);
                }
            }
        }

        #endregion
    }
}
