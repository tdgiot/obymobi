﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;

namespace Dionysos.Net
{
	#region Managed IP Helper API

	/// <summary>
	/// Managed IP helper.
	/// </summary>
	public static class ManagedIpHelper
	{
		#region Public Methods

		/// <summary>
		/// Gets the extended TCP table.
		/// </summary>
		/// <param name="sorted">If set to <c>true</c> sorts the table.</param>
		/// <returns>
		/// The extended TCP table.
		/// </returns>
		public static TcpTable GetExtendedTcpTable(bool sorted)
		{
			var tcpRows = new List<TcpRow>();

			IntPtr tcpTable = IntPtr.Zero;
			int tcpTableLength = 0;

			if (IpHelper.GetExtendedTcpTable(tcpTable, ref tcpTableLength, sorted, IpHelper.AF_INET, IpHelper.TcpTableType.OwnerPidAll, 0) != 0)
			{
				try
				{
					tcpTable = Marshal.AllocHGlobal(tcpTableLength);
					if (IpHelper.GetExtendedTcpTable(tcpTable, ref tcpTableLength, true, IpHelper.AF_INET, IpHelper.TcpTableType.OwnerPidAll, 0) == 0)
					{
						var table = (IpHelper.TcpTable)Marshal.PtrToStructure(tcpTable, typeof(IpHelper.TcpTable));

						var rowPtr = (IntPtr)((long)tcpTable + Marshal.SizeOf(table.length));
						for (int i = 0; i < table.length; ++i)
						{
							tcpRows.Add(new TcpRow((IpHelper.TcpRow)Marshal.PtrToStructure(rowPtr, typeof(IpHelper.TcpRow))));
							rowPtr = (IntPtr)((long)rowPtr + Marshal.SizeOf(typeof(IpHelper.TcpRow)));
						}
					}
				}
				finally
				{
					if (tcpTable != IntPtr.Zero)
					{
						Marshal.FreeHGlobal(tcpTable);
					}
				}
			}

			return new TcpTable(tcpRows);
		}

		#endregion
	}

	/// <summary>
	/// A TCP row.
	/// </summary>
	public class TcpRow
	{
		#region Private Fields

		private readonly IPEndPoint localEndPoint;
		private readonly int processId;
		private readonly IPEndPoint remoteEndPoint;
		private readonly TcpState state;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TcpRow"/> class.
		/// </summary>
		/// <param name="tcpRow">The TCP row.</param>
		public TcpRow(IpHelper.TcpRow tcpRow)
		{
			state = tcpRow.state;
			processId = tcpRow.owningPid;

			int localPort = (tcpRow.localPort1 << 8) + (tcpRow.localPort2) + (tcpRow.localPort3 << 24) + (tcpRow.localPort4 << 16);
			long localAddress = tcpRow.localAddr;
			localEndPoint = new IPEndPoint(localAddress, localPort);

			int remotePort = (tcpRow.remotePort1 << 8) + (tcpRow.remotePort2) + (tcpRow.remotePort3 << 24) + (tcpRow.remotePort4 << 16);
			long remoteAddress = tcpRow.remoteAddr;
			remoteEndPoint = new IPEndPoint(remoteAddress, remotePort);
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets the local end point.
		/// </summary>
		/// <value>
		/// The local end point.
		/// </value>
		public IPEndPoint LocalEndPoint
		{
			get { return localEndPoint; }
		}

		/// <summary>
		/// Gets the remote end point.
		/// </summary>
		/// <value>
		/// The remote end point.
		/// </value>
		public IPEndPoint RemoteEndPoint
		{
			get { return remoteEndPoint; }
		}

		/// <summary>
		/// Gets the state.
		/// </summary>
		/// <value>
		/// The state.
		/// </value>
		public TcpState State
		{
			get { return state; }
		}

		/// <summary>
		/// Gets the process unique identifier.
		/// </summary>
		/// <value>
		/// The process unique identifier.
		/// </value>
		public int ProcessId
		{
			get { return processId; }
		}

		#endregion
	}

	/// <summary>
	/// A TCP table.
	/// </summary>
	public class TcpTable : IEnumerable<TcpRow>
	{
		#region Private Fields

		private readonly IEnumerable<TcpRow> tcpRows;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TcpTable"/> class.
		/// </summary>
		/// <param name="tcpRows">The TCP rows.</param>
		public TcpTable(IEnumerable<TcpRow> tcpRows)
		{
			this.tcpRows = tcpRows;
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets the rows.
		/// </summary>
		/// <value>
		/// The rows.
		/// </value>
		public IEnumerable<TcpRow> Rows
		{
			get { return tcpRows; }
		}

		#endregion

		#region IEnumerable<TcpRow> Members

		/// <summary>
		/// Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
		/// </returns>
		public IEnumerator<TcpRow> GetEnumerator()
		{
			return tcpRows.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		/// <summary>
		/// Returns an enumerator that iterates through a collection.
		/// </summary>
		/// <returns>
		/// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
		/// </returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return tcpRows.GetEnumerator();
		}

		#endregion
	}

	#endregion

	#region P/Invoke IP Helper API

	/// <summary>
	/// http://msdn2.microsoft.com/en-us/library/aa366073.aspx
	/// </summary>
	public static class IpHelper
	{
		#region Public Fields

		/// <summary>
		/// The dialog l_ name
		/// </summary>
		public const string DLL_NAME = "iphlpapi.dll";

		/// <summary>
		/// The aggregate f_ inet
		/// </summary>
		public const int AF_INET = 2;

		#endregion

		#region Public Methods

		/// <summary>
		/// http://msdn2.microsoft.com/en-us/library/aa365928.aspx
		/// </summary>
		/// <param name="tcpTable">The TCP table.</param>
		/// <param name="tcpTableLength">Length of the TCP table.</param>
		/// <param name="sort">if set to <c>true</c> [sort].</param>
		/// <param name="ipVersion">The ip version.</param>
		/// <param name="tcpTableType">Type of the TCP table.</param>
		/// <param name="reserved">The reserved.</param>
		/// <returns></returns>
		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern uint GetExtendedTcpTable(IntPtr tcpTable, ref int tcpTableLength, bool sort, int ipVersion, TcpTableType tcpTableType, int reserved);

		#endregion

		#region Public Enums

		/// <summary>
		/// http://msdn2.microsoft.com/en-us/library/aa366386.aspx
		/// </summary>
		public enum TcpTableType
		{
			/// <summary>
			/// The basic listener
			/// </summary>
			BasicListener,
			/// <summary>
			/// The basic connections
			/// </summary>
			BasicConnections,
			/// <summary>
			/// The basic all
			/// </summary>
			BasicAll,
			/// <summary>
			/// The owner pid listener
			/// </summary>
			OwnerPidListener,
			/// <summary>
			/// The owner pid connections
			/// </summary>
			OwnerPidConnections,
			/// <summary>
			/// The owner pid all
			/// </summary>
			OwnerPidAll,
			/// <summary>
			/// The owner module listener
			/// </summary>
			OwnerModuleListener,
			/// <summary>
			/// The owner module connections
			/// </summary>
			OwnerModuleConnections,
			/// <summary>
			/// The owner module all
			/// </summary>
			OwnerModuleAll,
		}

		#endregion

		#region Public Structs

		/// <summary>
		/// http://msdn2.microsoft.com/en-us/library/aa366913.aspx
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct TcpRow
		{
			/// <summary>
			/// The state
			/// </summary>
			public TcpState state;
			/// <summary>
			/// The local addr
			/// </summary>
			public uint localAddr;
			/// <summary>
			/// The local port1
			/// </summary>
			public byte localPort1;
			/// <summary>
			/// The local port2
			/// </summary>
			public byte localPort2;
			/// <summary>
			/// The local port3
			/// </summary>
			public byte localPort3;
			/// <summary>
			/// The local port4
			/// </summary>
			public byte localPort4;
			/// <summary>
			/// The remote addr
			/// </summary>
			public uint remoteAddr;
			/// <summary>
			/// The remote port1
			/// </summary>
			public byte remotePort1;
			/// <summary>
			/// The remote port2
			/// </summary>
			public byte remotePort2;
			/// <summary>
			/// The remote port3
			/// </summary>
			public byte remotePort3;
			/// <summary>
			/// The remote port4
			/// </summary>
			public byte remotePort4;
			/// <summary>
			/// The owning pid
			/// </summary>
			public int owningPid;
		}

		/// <summary>
		/// http://msdn2.microsoft.com/en-us/library/aa366921.aspx
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct TcpTable
		{
			/// <summary>
			/// The length
			/// </summary>
			public uint length;
			/// <summary>
			/// The row
			/// </summary>
			public TcpRow row;
		}

		#endregion
	}

	#endregion
}
