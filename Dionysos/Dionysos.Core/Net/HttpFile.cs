﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Dionysos.Net
{
	/// <summary>
	/// Represents a file that is recieved or can be send over HTTP.
	/// </summary>
	public class HttpFile
	{
		#region Fields

		/// <summary>
		/// The stream.
		/// </summary>
		private readonly Stream stream;

		/// <summary>
		/// The name of the file.
		/// </summary>
		private readonly string fileName;

		/// <summary>
		/// The MIME content type of the file.
		/// </summary>
		private readonly string contentType;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the stream.
		/// </summary>
		public Stream Stream
		{
			get
			{
				return this.stream;
			}
		}

		/// <summary>
		/// Gets the name of the file.
		/// </summary>
		/// <value>
		/// The name of the file.
		/// </value>
		public string FileName
		{
			get
			{
				return this.fileName;
			}
		}

		/// <summary>
		/// Gets the MIME content type of the file.
		/// </summary>
		/// <value>
		/// The MIME content type of the file.
		/// </value>
		public string ContentType
		{
			get
			{
				return this.contentType;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="HttpFile"/> class.
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="contentType">Type of the content.</param>
		public HttpFile(Stream stream, string fileName, string contentType)
		{
			if (stream == null) throw new ArgumentNullException("stream");

			this.stream = stream;
			this.fileName = fileName;
			this.contentType = contentType;
		}

		#endregion
	}
}
