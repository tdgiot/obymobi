﻿using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Text;
using Dionysos;

namespace Dionysos.Net
{
    /// <summary>
    /// Singleton helper class which can be used to execute http requests 
    /// </summary>
    public class HttpRequestHelper
    {
        #region Fields

        static HttpRequestHelper instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the HttpRequestHelper class if the instance has not been initialized yet
        /// </summary>
        public HttpRequestHelper()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new HttpRequestHelper();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static HttpRequestHelper instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Executes a HttpRequest and returns the results as a string
        /// </summary>
        /// <param name="address">The address to get the response from</param>
        /// <returns>A System.String instance representing the response from the HttpRequest</returns>
        public static string GetResponseAsString(string address)
        {
            string result = string.Empty;

            if (Instance.ArgumentIsEmpty(address, "address"))
            {
                // Parameter 'address' is empty
            }
            else
            {
                // Create the web request   
                HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
                request.Proxy = null;
                if (Instance.Empty(request))
                {
                    throw new EmptyException("Variable 'request' is empty!");
                }
                else
                {
                    // Get response   
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        // Get the response stream   
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        if (Instance.Empty(reader))
                        {
                            throw new EmptyException("Variable 'reader' is empty!");
                        }
                        else
                        {
                            // Read the whole contents and return as a string   
                            result = reader.ReadToEnd();
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Executes a HttpRequest and returns the results as a XmlReader instance
        /// </summary>
        /// <param name="address">The address to get the response from</param>
        /// <returns>A System.Xml.XmlReader instance containing the response from the HttpRequest</returns>
        public static XmlReader GetResponseAsXmlReader(string address)
        {
            XmlTextReader reader = null;

            if (Instance.ArgumentIsEmpty(address, "address"))
            {
                // Parameter 'address' is empty
            }
            else
            {
                // Retrieve XML document   
                reader = new XmlTextReader(address);

                // Skip non-significant whitespace   
                reader.WhitespaceHandling = WhitespaceHandling.Significant;
            }

            return reader;
        }

        /// <summary>
        /// Executes a HttpRequest and returns the results as a XmlDocument instance
        /// </summary>
        /// <param name="address">The address to get the response from</param>
        /// <returns>A System.Xml.XmlDocument instance containing the response from the HttpRequest</returns>
        public static XmlDocument GetResponseAsXmlDocument(string address)
        {
            XmlDocument doc = null;

            if (Instance.ArgumentIsEmpty(address, "address"))
            {
                // Parameter 'address' is empty
            }
            else
            {
                doc = new XmlDocument();
                doc.Load(address);
            }

            return doc;
        }

        /// <summary>
        /// Executes a HttpRequest and returns the results as a DataSet instance
        /// </summary>
        /// <param name="address">The address to get the response from</param>
        /// <returns>A System.Data.DataSet instance containing the response from the HttpRequest</returns>
        public static DataSet GetResponseAsDataSet(string address)
        {
            DataSet dataSet = null;

            if (Instance.ArgumentIsEmpty(address, "address"))
            {
                // Parameter 'address' is empty
            }
            else
            {
                // Create the web request   
                HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
                request.Proxy = null;
                if (Instance.Empty(request))
                {
                    throw new EmptyException("Variable 'request' is empty!");
                }
                else
                {
                    // Get response   
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        // Load data into a dataset   
                        dataSet = new DataSet();
                        dataSet.ReadXml(response.GetResponseStream());
                    }
                }
            }

            return dataSet;
        }

        /// <summary>
        /// Executes a Http request for the specified url
        /// </summary>
        /// <param name="url">The url to execute the Http request for</param>
        /// <returns>A System.String instance containing the response</returns>
        public static string ExecuteHttpRequest(string url)
        {
            // used to build entire input
            StringBuilder sb = new StringBuilder();

            // used on each read operation
            byte[] buf = new byte[8192];

            // prepare the web page we will be asking for
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Proxy = null;

            // execute the request
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // we will read data via the response stream
            Stream resStream = response.GetResponseStream();

            string tempString = null;
            int count = 0;

            do
            {
                // fill the buffer with data
                count = resStream.Read(buf, 0, buf.Length);

                // make sure we read some data
                if (count != 0)
                {
                    // translate from bytes to ASCII text
                    tempString = Encoding.ASCII.GetString(buf, 0, count);

                    // continue building the string
                    sb.Append(tempString);
                }
            }
            while (count > 0); // any more data to read?

            // print out page source
            return sb.ToString();
        }

        #endregion
    }
}