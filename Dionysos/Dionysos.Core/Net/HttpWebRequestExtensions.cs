using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using System.Web;
using System.Threading;

namespace Dionysos.Net
{
	/// <summary>
	/// Extension methods for the HttpWebRequest class.
	/// </summary>
	public static class HttpWebRequestExtensions
	{
		/// <summary>
		/// Posts the multipart form data.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="form">The form.</param>
		/// <param name="files">The files.</param>
		public static void PostMultipartFormData(this HttpWebRequest request, NameValueCollection form, IDictionary<string, HttpFile> files)
		{
			// Generate boundary
			string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");

			List<MimePart> mimeParts = new List<MimePart>();

			// Add form fields
			foreach (string key in form.AllKeys)
			{
				StringMimePart part = new StringMimePart(boundary, form[key]);
				part.Headers["Content-Disposition"] = "form-data; name=\"" + key + "\"";
				mimeParts.Add(part);
			}

			// Add files
			foreach (KeyValuePair<string, HttpFile> file in files)
			{
				StreamMimePart part = new StreamMimePart(boundary, file.Value.Stream);
				part.Headers["Content-Disposition"] = "form-data; name=\"" + file.Key + "\"; filename=\"" + file.Value.FileName + "\"";
				part.Headers["Content-Type"] = file.Value.ContentType;
				mimeParts.Add(part);
			}

			// Get footer
			byte[] footer = Encoding.UTF8.GetBytes("--" + boundary + "--");

			// Set content length
			long contentLength = footer.Length;
			foreach (MimePart part in mimeParts)
			{
				contentLength += part.Length;
			}

			// Set request variables
			request.ContentType = "multipart/form-data; boundary=" + boundary;
			request.Method = "POST";
			request.ContentLength = contentLength;

			// Write data to request stream
			Stream requestStream = request.GetRequestStream();
			foreach (MimePart part in mimeParts)
			{
				requestStream.Write(part.Header, 0, part.Header.Length);

				using (part.Data)
				{
					part.Data.Copy(requestStream);
				}

				requestStream.Write(part.Footer, 0, part.Footer.Length);
			}
			requestStream.Write(footer, 0, footer.Length);
			requestStream.Close();
		}

		/// <summary>
		/// Represents a MIME part.
		/// </summary>
		private abstract class MimePart
		{
			#region Fields

			/// <summary>
			/// The boundary.
			/// </summary>
			private readonly string boundary;

			/// <summary>
			/// The headers.
			/// </summary>
			private NameValueCollection headers = new NameValueCollection();

			/// <summary>
			/// The header data.
			/// </summary>
			private byte[] header;

			/// <summary>
			/// The footer data.
			/// </summary>
			private byte[] footer = Encoding.UTF8.GetBytes("\r\n");

			#endregion

			#region Properties

			/// <summary>
			/// Gets the headers.
			/// </summary>
			public NameValueCollection Headers
			{
				get
				{
					return this.headers;
				}
			}

			/// <summary>
			/// Gets the header data.
			/// </summary>
			public byte[] Header
			{
				get
				{
					if (this.header == null &&
						!String.IsNullOrEmpty(this.boundary))
					{
						StringBuilder sb = new StringBuilder();

						sb.Append("--" + this.boundary + "\r\n");
						foreach (string key in this.headers.AllKeys)
						{
							sb.Append(key + ": " + this.headers[key] + "\r\n");
						}
						sb.Append("\r\n");

						this.header = Encoding.UTF8.GetBytes(sb.ToString());
					}

					return this.header;
				}
			}

			/// <summary>
			/// Gets the footer data.
			/// </summary>
			public byte[] Footer
			{
				get
				{
					return this.footer;
				}
			}

			/// <summary>
			/// Gets the data.
			/// </summary>
			public abstract Stream Data { get; }

			/// <summary>
			/// Gets the length.
			/// </summary>
			public long Length
			{
				get
				{
					long length = 0;

					if (this.Header != null) length += this.Header.Length;
					if (this.Data != null) length += this.Data.Length;
					if (this.Footer != null) length += this.Footer.Length;

					return length;
				}
			}

			#endregion

			#region Constructors

			/// <summary>
			/// Initializes a new instance of the <see cref="MimePart"/> class.
			/// </summary>
			/// <param name="boundary">The boundary.</param>
			public MimePart(string boundary)
			{
				this.boundary = boundary;
			}

			#endregion
		}

		/// <summary>
		/// Represents a stream MIME part.
		/// </summary>
		private class StreamMimePart : MimePart
		{
			#region Fields

			/// <summary>
			/// The data.
			/// </summary>
			private readonly Stream data;

			#endregion

			#region Properties

			/// <summary>
			/// Gets the data.
			/// </summary>
			public override Stream Data
			{
				get
				{
					return this.data;
				}
			}

			#endregion

			#region Constructors

			/// <summary>
			/// Initializes a new instance of the <see cref="StringMimePart"/> class.
			/// </summary>
			/// <param name="boundary">The boundary.</param>
			/// <param name="data">The data.</param>
			public StreamMimePart(string boundary, Stream data)
				: base(boundary)
			{
				this.data = data;
			}

			#endregion
		}

		/// <summary>
		/// Represents a string MIME part.
		/// </summary>
		private class StringMimePart : MimePart
		{
			#region Fields

			/// <summary>
			/// The data.
			/// </summary>
			private readonly Stream data;

			#endregion

			#region Properties

			/// <summary>
			/// Gets the data.
			/// </summary>
			public override Stream Data
			{
				get
				{
					return this.data;
				}
			}

			#endregion

			#region Constructors

			/// <summary>
			/// Initializes a new instance of the <see cref="StringMimePart"/> class.
			/// </summary>
			/// <param name="boundary">The boundary.</param>
			/// <param name="data">The data.</param>
			public StringMimePart(string boundary, string data)
				: base(boundary)
			{
				this.data = new MemoryStream(Encoding.UTF8.GetBytes(data));
			}

			#endregion
		}
	}
}