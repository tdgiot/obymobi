﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;

namespace Dionysos.Net
{
    /// <summary>
    /// Class for Ping related functions
    /// </summary>
    public static class PingUtil
    {
        /// <summary>
        /// Pings the specified address to ping.
        /// </summary>
        /// <param name="hostNameOrAddress">The address to ping.</param>
        /// <param name="responseTimeMs">The response time in ms.</param>
        /// <returns></returns>
        public static bool Ping(string hostNameOrAddress, out long responseTimeMs)
        {
            bool retval = false;
            responseTimeMs = 0;
            Ping pingSender = new Ping();
            PingOptions options = new PingOptions();

            // Create a buffer of 32 bytes of data to be transmitted.  
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 1000;

            try
            {
                PingReply reply = pingSender.Send(hostNameOrAddress, timeout, buffer, options);
                if (reply.Status == IPStatus.Success)
                {
                    responseTimeMs = reply.RoundtripTime;
                    retval = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return retval;
        }
    }
}
