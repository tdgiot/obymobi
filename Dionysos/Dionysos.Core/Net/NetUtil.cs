﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.IO;

namespace Dionysos.Net
{
    /// <summary>
    /// Misc. Utility methods related to Net
    /// </summary>
    public static class NetUtil
    {
        /// <summary>
        /// Gets the local ips (those that belong to the AddressFamiliy "InterNetwork").
        /// </summary>
        /// <returns>List of local ips, empty list when none are found or error.</returns>
        public static List<string> GetLocalIps()
        {
            List<string> localIPs = new List<string>();
            try
            {
                IPHostEntry host;
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily.ToString() == "InterNetwork")
                    {
                        localIPs.Add(ip.ToString());
                    }
                }
            }
            catch
            {
                // No results.
            }

            return localIPs;
        }

        /// <summary>
        /// Gets the name of the machine.
        /// </summary>
        /// <returns></returns>
        public static string GetMachineName()
        {
            return System.Environment.MachineName;
        }

        /// <summary>
        /// Gets the local ips (those that belong to the AddressFamiliy "InterNetwork").
        /// </summary>
        /// <returns>List of local ips, empty list when none are found or error.</returns>
        public static string GetLocalIpsJoined()
        {
            string localIPs = string.Empty;
            var ips = NetUtil.GetLocalIps();
            foreach (string ip in ips)
            {
                localIPs = StringUtil.CombineWithComma(localIPs, ip.ToString());
            }
            return localIPs;
        }

        /// <summary>
        /// Gets the remote ip.
        /// </summary>
        /// <returns>Remote IP or string.Empty if couldn't be retrieved</returns>
        public static string GetRemoteIp(List<string> ipAddressProviders = null)
        {
            string toReturn = string.Empty;

            var externalIpAddressProviders = new List<string>();
            //externalIpAddressProviders.Add("http://checkip.dyndns.org");
            externalIpAddressProviders.Add("http://ipecho.net/plain");
            externalIpAddressProviders.Add("http://icanhazip.com");
            externalIpAddressProviders.Add("http://bot.whatismyipaddress.com");
            externalIpAddressProviders.Add("http://ipinfo.io/ip");
            //externalIpAddressProviders.Add("http://ifconfig.me/ip"); // Slow connecting
            externalIpAddressProviders.Add("http://ident.me/");            

            // Random to prevent over load
            var randomizer = new Random();
            IOrderedEnumerable<string> randomSorted = externalIpAddressProviders.OrderBy(x => randomizer.Next());

            if (ipAddressProviders == null)
                ipAddressProviders = new List<string>();

            // Append external providers, so parameter providers have priority
            ipAddressProviders.AddRange(randomSorted);

            foreach (string url in ipAddressProviders)
            {
                try
                {
                    using (var webClient = new WebClient())
                    {
                        webClient.Proxy = null;
                        string text = webClient.DownloadString(url);
                        if (url == "http://checkip.dyndns.org/")
                        {
                            int first = text.IndexOf("Address: ", System.StringComparison.OrdinalIgnoreCase) + "Address: ".Length;
                            int last = text.LastIndexOf("</body>", System.StringComparison.OrdinalIgnoreCase);
                            text = text.Substring(first, last - first);
                        }

                        IPAddress address;
                        if (IPAddress.TryParse(text, out address))
                        {
                            toReturn = text;
                            break;
                        }
                    }
                }
                catch
                {
                    // Nothing retrieved.
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Get remote ip
        /// </summary>
        /// <param name="urls">Urls to use to get remote ip</param>
        /// <returns>Remote IP or string.Empty</returns>
        public static string GetRemoteIp(IEnumerable<string> urls)
        {
            string toReturn = string.Empty;

            foreach (var url in urls)
            {
                try
                {
                    WebRequest request = WebRequest.Create(url);
                    request.Proxy = null;
                    using (WebResponse response = request.GetResponse())
                    {
                        using (var stream = new StreamReader(response.GetResponseStream()))
                        {
                            toReturn = stream.ReadToEnd();

                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }

            return toReturn;
        }

    }
}
