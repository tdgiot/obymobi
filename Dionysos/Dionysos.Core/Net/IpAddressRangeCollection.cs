﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;

namespace Dionysos.Net
{
    /// <summary>
    /// 
    /// </summary>
    public class IpAddressRangeCollection : Collection<IPAddressRange>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddressRanges"></param>
        public void AddRange(List<string> ipAddressRanges)
        {
            foreach (string ipAddressRangeString in ipAddressRanges)
            {
                this.Add(new IPAddressRange(ipAddressRangeString));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddressRange"></param>
        public void Add(string ipAddressRange)
        {
            this.Add(new IPAddressRange(ipAddressRange));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddressString"></param>
        /// <returns></returns>
        public bool Contains(string ipAddressString)
        { 
            // Yes, can throw exceptions, that's fine.
            IPAddress ipAddress = IPAddress.Parse(ipAddressString);

            // Does any of our ranges contain the ipaddres?
            return this.Any(x => x.Contains(ipAddress));
        }
    }
}
