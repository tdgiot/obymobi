﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Net.Configuration;
using System.Web;
using System.Web.Configuration;

namespace Dionysos.Configuration
{
    /// <summary>
    /// Singleton utility class which can be used to retrieve e-mail settings from a configuration file
    /// </summary>
    public class MailSettings
    {
        #region Fields

        static MailSettings instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the MailSettings class if the instance has not been initialized yet
        /// </summary>
        public MailSettings()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new MailSettings();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static MailSettings instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Retrieves the mailsettings from the configuration file
        /// </summary>
        /// <returns>A System.Net.Configuration.MailSettingsSectionGroup instance which contains the mail settings</returns>
        public static MailSettingsSectionGroup GetMailSettings()
        {
            System.Configuration.Configuration configuration = WebConfigurationManager.OpenWebConfiguration("~");
            return configuration.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup; 
        }

        #endregion
    }
}