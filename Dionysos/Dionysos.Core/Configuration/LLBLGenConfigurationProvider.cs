﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Caching;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Globalization;
using System.Linq;

namespace Dionysos.Configuration
{
	/// <summary>
	/// Configuration provider class which is used for reading/writing configuration values from a database.
	/// </summary>
	public class LLBLGenConfigurationProvider : IConfigurationProvider
	{
		#region Fields

		private object syncRoot = new object();
		private string entityName = "Configuration";

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name of the entity.
		/// </summary>
		public string EntityName
		{
			get
			{
				return this.entityName;
			}
			set
			{
				this.entityName = value;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets configuration information for the specified configuration item.
		/// </summary>
		/// <param name="name">The name of the configuration item.</param>
		/// <returns>A ConfigurationItem instance containing information about the configuration item.</returns>
		private ConfigurationItem GetConfigurationItem(string name)
		{
			ConfigurationItem configurationItem = null;

			ConfigurationInfoCollection configurationInfoCollection = Global.ConfigurationInfo;
			for (int i = 0, iCount = configurationInfoCollection.Count; i < iCount; i++)
			{
				ConfigurationItemCollection configurationItemCollection = Global.ConfigurationInfo[i];
				for (int j = 0, jCount = configurationItemCollection.Count; j < jCount; j++)
				{
					ConfigurationItem currentItem = configurationItemCollection[j];
					if (currentItem.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
					{
						// Found configuration item, so stop further processing of ConfigurationItems
						configurationItem = currentItem;
						break;
					}
				}

				if (configurationItem != null)
				{
					// Found configuration item, so stop further processing of ConfurationItemCollections
					break;
				}
			}

			if (configurationItem != null)
			{
				if (String.IsNullOrEmpty(configurationItem.Name))
				{
					throw new EmptyException("Configuration item '{0}' does not have a name.", name);
				}
				else if (String.IsNullOrEmpty(configurationItem.Section))
				{
					throw new EmptyException("Configuration item '{0}' does not have a section.", name);
				}
			}

			return configurationItem;
		}

		/// <summary>
		/// Get's if the setting is defined for the current running application.
		/// This might not be the case when a certain set of ConfigInfo is not initialized in the application start.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>
		/// bool
		/// </returns>
		public bool IsDefined(string name)
		{
			ConfigurationItem configurationItem = this.GetConfigurationItem(name);

			// Check whether the configuration item information has been supplied
			return (configurationItem != null);
		}

		/// <summary>
		/// Gets an entity containing the configuration for the specified configuration item.
		/// </summary>
		/// <param name="name">The name of the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		/// <param name="fromCache">Indicates whether retrieval from the cache is allowed.</param>
		/// <returns>An IEntity instance containing the configuration if the configuration value exists; otherwise null.</returns>
		private IEntity GetConfigurationEntity(string name, bool? applicationSpecific, bool fromCache)
		{
			IEntity configurationEntity = null;

			// Get configuration item
			ConfigurationItem configurationItem = this.GetConfigurationItem(name);

			// Check whether the configuration item information has been supplied
			if (configurationItem == null)
			{
				throw new EmptyException("No configuration item found for configuration constant '{0}'. Check the initialization of the configuration info in the Global Application class.", name);
			}

			// Try to get entity from cache
			if (fromCache)
			{
				configurationEntity = ConfigurationValueCache.Instance.GetValue(configurationItem, applicationSpecific) as IEntity;
			}

			// Try to get entity from database
			if (configurationEntity == null)
			{
				// Create and initialize an IEntity instance in order to dynamically get the EntityField instances
				IEntity entity = DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;

				if (entity.Fields["Section"] == null)
				{
					throw new EmptyException("Entity '{0}' does not have a field 'Section'.", this.EntityName);
				}
				else if (entity.Fields["Name"] == null)
				{
					throw new EmptyException("Entity '{0}' does not have a field 'Name'.", this.EntityName);
				}
				else if (entity.Fields["Value"] == null)
				{
					throw new EmptyException("Entity '{0}' does not have a field 'Value'.", this.EntityName);
				}
				else if (entity.Fields["ApplicationName"] == null)
				{
					throw new EmptyException("Entity '{0}' does not have a field 'ApplicationName'.", this.EntityName);
				}

				// Create and initialize an IEntityCollection instance
				IEntityCollection entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as IEntityCollection;

				PredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(entity.Fields["Section"], ComparisonOperator.Equal, configurationItem.Section));
				filter.Add(new FieldCompareValuePredicate(entity.Fields["Name"], ComparisonOperator.Equal, configurationItem.Name));

				bool isApplicationSpecific = applicationSpecific.HasValue ? applicationSpecific.Value : configurationItem.ApplicationSpecific;
				if (!isApplicationSpecific)
				{
					// Not application specific
					PredicateExpression filterEmptyApplicationName = new PredicateExpression();
					filterEmptyApplicationName.Add(new FieldCompareValuePredicate(entity.Fields["ApplicationName"], ComparisonOperator.Equal, ""));
					filterEmptyApplicationName.AddWithOr(new FieldCompareNullPredicate(entity.Fields["ApplicationName"]));
					filter.Add(filterEmptyApplicationName);
				}
				else
				{
					// Application specific
					filter.Add(new FieldCompareValuePredicate(entity.Fields["ApplicationName"], ComparisonOperator.Equal, Global.ApplicationInfo.ApplicationName));
				}

				entityCollection.GetMulti(filter, 2);

				if (entityCollection.Count == 1)
				{
					// Configuration entity found
					configurationEntity = entityCollection[0];

					// Save to cache
					ConfigurationValueCache.Instance.SetValue(configurationItem, configurationEntity, applicationSpecific);
				}
				else if (entityCollection.Count > 1)
				{
					throw new FunctionalException("Multiple configuration values found for section '{0}', name '{1}'{2}.",
						configurationItem.Section,
						configurationItem.Name,
						(isApplicationSpecific ? String.Format(", application name '{0}'", Global.ApplicationInfo.ApplicationName) : String.Empty));
				}
			}

			return configurationEntity;
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		private T GetValue<T>(string name)
		{
			return this.GetValue<T>(name, null);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		private T GetValue<T>(string name, bool? applicationSpecific)
		{
			object value = null;

			// Get the value from the database
			IEntity configurationEntity = this.GetConfigurationEntity(name, applicationSpecific, true);

			if (configurationEntity != null)
			{
				// Get value from configuration entity
				value = configurationEntity.Fields["Value"].CurrentValue;
			}

			if (value == null)
			{
				// Get default value from configuration item
				ConfigurationItem configurationItem = this.GetConfigurationItem(name);

				if (configurationItem == null)
				{
					throw new FunctionalException("No configuration item could be found with name '{0}'.", name);
				}

				// Get value from configuration item
				value = configurationItem.DefaultValue;

				// Set value to default
				if (!this.SetValue<object>(name, value, applicationSpecific))
				{
					throw new FunctionalException("Configuration item with name '{0}' could not be set to default value '{1}'.", name, value);
				}
			}

			// Value is found, try to change the type
			try
			{
				try
				{
					return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
				}
				catch (FormatException)
				{
					// Value could be stored in culture specific format, resave and return current value
					T invalidFormattedValue = (T)Convert.ChangeType(value, typeof(T), CultureInfo.CurrentCulture);
					this.SetValue(name, invalidFormattedValue, applicationSpecific);

					return invalidFormattedValue;
				}
			}
			catch (Exception ex)
			{
				// Type could not be changed
				throw new FunctionalException(ex, "Value '{0}' for configuration item '{1}' could not be converted to type '{2}'.", value, name, typeof(T).FullName);
			}
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		private T GetValueRefreshed<T>(string name)
		{
			return this.GetValueRefreshed<T>(name, null);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		private T GetValueRefreshed<T>(string name, bool? applicationSpecific)
		{
			// Refresh the value
			this.GetConfigurationEntity(name, applicationSpecific, false);

			return this.GetValue<T>(name, applicationSpecific);
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <returns>Returns true if the value if set; otherwise false.</returns>
		private bool SetValue<T>(string name, T value)
		{
			return this.SetValue<T>(name, value, null);
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		/// <returns>Returns true if the value if set; otherwise false.</returns>
		private bool SetValue<T>(string name, T value, bool? applicationSpecific)
		{
			bool isSet = false;

			// Get configuration item
			ConfigurationItem configurationItem = this.GetConfigurationItem(name);

			// Check whether the configuration item information has been supplied
			if (configurationItem == null)
			{
				throw new EmptyException("No configuration item found for configuration constant '{0}'. Check the initialization of the configuration info in the Global Application class.", name);
			}

			// Lock while updating or adding configuration value
			lock (this.syncRoot)
			{
				// Get the configuration entity from the database
				IEntity configurationEntity = this.GetConfigurationEntity(name, applicationSpecific, false);

				if (configurationEntity != null)
				{
					// Set new value
					configurationEntity.SetNewFieldValue("Value", String.Format(CultureInfo.InvariantCulture, "{0}", value));
				}
				else
				{
					// No configuration entity found, create new one
					configurationEntity = DataFactory.EntityFactory.GetEntity(this.entityName) as IEntity;

					// Set values
					configurationEntity.SetNewFieldValue("Name", configurationItem.Name);
					configurationEntity.SetNewFieldValue("Section", configurationItem.Section);

					bool isApplicationSpecific = applicationSpecific ?? configurationItem.ApplicationSpecific;
					if (isApplicationSpecific)
					{
						configurationEntity.SetNewFieldValue("ApplicationName", Global.ApplicationInfo.ApplicationName);
					}

					object newValue = (object)value ?? configurationItem.DefaultValue;
					configurationEntity.SetNewFieldValue("Value", String.Format(CultureInfo.InvariantCulture, "{0}", newValue));
					configurationEntity.SetNewFieldValue("Type", configurationItem.Type.ToString());
					configurationEntity.SetNewFieldValue("FriendlyName", configurationItem.FriendlyName);
				}

				// Save value to database
				isSet = configurationEntity.Save();

				// Save value to cache
				ConfigurationValueCache.Instance.SetValue(configurationItem, configurationEntity, applicationSpecific);
			}

			return isSet;
		}

		#endregion

		#region IConfigurationProvider Members

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetValue(string name)
		{
			return this.GetValue<string>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetValue(string name, bool applicationSpecific)
		{
			return this.GetValue<string>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetValueRefreshed(string name)
		{
			return this.GetValueRefreshed<string>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetValueRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<string>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public bool GetBool(string name)
		{
			return this.GetValue<bool>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public bool GetBool(string name, bool applicationSpecific)
		{
			return this.GetValue<bool>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public bool GetBoolRefreshed(string name)
		{
			return this.GetValueRefreshed<bool>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public bool GetBoolRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<bool>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public byte GetByte(string name)
		{
			return this.GetValue<byte>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public byte GetByte(string name, bool applicationSpecific)
		{
			return this.GetValue<byte>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public byte GetByteRefreshed(string name)
		{
			return this.GetValueRefreshed<byte>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public byte GetByteRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<byte>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public char GetChar(string name)
		{
			return this.GetValue<char>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public char GetChar(string name, bool applicationSpecific)
		{
			return this.GetValue<char>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public char GetCharRefreshed(string name)
		{
			return this.GetValueRefreshed<char>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public char GetCharRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<char>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public DateTime GetDateTime(string name)
		{
			return this.GetValue<DateTime>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public DateTime GetDateTime(string name, bool applicationSpecific)
		{
			return this.GetValue<DateTime>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public DateTime GetDateTimeRefreshed(string name)
		{
			return this.GetValueRefreshed<DateTime>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public DateTime GetDateTimeRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<DateTime>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public decimal GetDecimal(string name)
		{
			return this.GetValue<decimal>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public decimal GetDecimal(string name, bool applicationSpecific)
		{
			return this.GetValue<decimal>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public decimal GetDecimalRefreshed(string name)
		{
			return this.GetValueRefreshed<decimal>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public decimal GetDecimalRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<decimal>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public double GetDouble(string name)
		{
			return this.GetValue<double>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public double GetDouble(string name, bool applicationSpecific)
		{
			return this.GetValue<double>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public double GetDoubleRefreshed(string name)
		{
			return this.GetValueRefreshed<double>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public double GetDoubleRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<double>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public float GetFloat(string name)
		{
			return this.GetValue<float>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public float GetFloat(string name, bool applicationSpecific)
		{
			return this.GetValue<float>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public float GetFloatRefreshed(string name)
		{
			return this.GetValueRefreshed<float>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public float GetFloatRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<float>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public int GetInt(string name)
		{
			return this.GetValue<int>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public int GetInt(string name, bool applicationSpecific)
		{
			return this.GetValue<int>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public int GetIntRefreshed(string name)
		{
			return this.GetValueRefreshed<int>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public int GetIntRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<int>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public long GetLong(string name)
		{
			return this.GetValue<long>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public long GetLong(string name, bool applicationSpecific)
		{
			return this.GetValue<long>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public long GetLongRefreshed(string name)
		{
			return this.GetValueRefreshed<long>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public long GetLongRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<long>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public short GetShort(string name)
		{
			return this.GetValue<short>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public short GetShort(string name, bool applicationSpecific)
		{
			return this.GetValue<short>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public short GetShortRefreshed(string name)
		{
			return this.GetValueRefreshed<short>(name);
		}

		/// <summary>
		/// Gets the value after is has been refreshed by a re-read from the persistent storage.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public short GetShortRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<short>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetString(string name)
		{
			return this.GetValue<string>(name);
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetString(string name, bool applicationSpecific)
		{
			return this.GetValue<string>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <returns>An System.String instance containing the current value</returns>
		public string GetStringRefreshed(string name)
		{
			return this.GetValueRefreshed<string>(name);
		}

		/// <summary>
		/// Gets the current value of this configuration item
		/// </summary>
		/// <param name="name">The name of the configuration item</param>
		/// <param name="applicationSpecific">The flag which indicates whether the configuration value is application specific</param>
		/// <returns>An System.Boolean instance containing the current value</returns>
		public string GetStringRefreshed(string name, bool applicationSpecific)
		{
			return this.GetValueRefreshed<string>(name, applicationSpecific);
		}

		/// <summary>
		/// Gets a password
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetPassword(string name)
		{
			return GetPassword(name, false);
		}

		/// <summary>
		/// Gets a password
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific.</param>
		/// <returns>Returns the value if found and type matches; otherwise returns default value defined for configuration item.</returns>
		public string GetPassword(string name, bool applicationSpecific)
		{
			string value = this.GetValue(name, applicationSpecific);
			if (value.Length > 0)
				value = Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(value);

			return value;
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		public void SetValue(string name, object value)
		{
			this.SetValue<object>(name, value);
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		public void SetValue(string name, object value, bool applicationSpecific)
		{
			this.SetValue<object>(name, value, applicationSpecific);
		}

		/// <summary>
		/// Sets a password value
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		public void SetPassword(string name, string value)
		{
			SetPassword(name, value, false);
		}

		/// <summary>
		/// Sets a password value
		/// </summary>
		/// <param name="name">The name of the configuration value.</param>
		/// <param name="value">The new value for the configuration item.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		public void SetPassword(string name, string value, bool applicationSpecific)
		{
			if (value.Length > 0)
				value = Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(value);

			this.SetValue(name, value);
		}

		#endregion
	}
}
