﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.Caching;
using System.Web;

namespace Dionysos.Configuration
{
	/// <summary>
	/// Static class for caching configuration values.
	/// </summary>
	public sealed class ConfigurationValueCache
	{
		#region Fields

		private static volatile ConfigurationValueCache instance;
		private static readonly object syncRoot = new object();
		private Hashtable values;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the single instance of ConfigurationValueCache.
		/// </summary>
		/// <remarks>
		/// See http://msdn.microsoft.com/en-us/library/ff650316.aspx for more information about implementing singletons.
		/// </remarks>
		public static ConfigurationValueCache Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
						{
							instance = new ConfigurationValueCache();
						}
					}
				}

				return instance;
			}
		}

		#endregion

		#region Contructors

		/// <summary>
		/// Initializes a new instance of the ConfigurationValueCache class.
		/// </summary>
		private ConfigurationValueCache()
		{
			this.values = new Hashtable();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Generates a unique cache key.
		/// </summary>
		/// <param name="configurationItem">The configuration item to generate the key for.</param>
		/// <param name="applicationSpecific">Indicates whether the key must be application specific.</param>
		/// <returns>The unique cache key.</returns>
		private string GetCacheKey(ConfigurationItem configurationItem, bool applicationSpecific)
		{
			return StringUtil.CombineWithSeperator("-", "ConfigurationValueCache", configurationItem.Section, configurationItem.Name, applicationSpecific ? Global.ApplicationInfo.ApplicationName : null);
		}

		/// <summary>
		/// Get a configuration value from the cache.
		/// </summary>
		/// <param name="configurationItem">The configuration item to get.</param>
		/// <returns>The configuration value if found; otherwise null.</returns>
		public object GetValue(ConfigurationItem configurationItem)
		{
			return this.GetValue(configurationItem, null);
		}

		/// <summary>
		/// Get a configuration value from the cache.
		/// </summary>
		/// <param name="configurationItem">The configuration item to get.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		/// <returns>The configuration value if found; otherwise null.</returns>
		public object GetValue(ConfigurationItem configurationItem, bool? applicationSpecific)
		{
			object value = null;

			bool isApplicationSpecific = applicationSpecific.HasValue ? applicationSpecific.Value : configurationItem.ApplicationSpecific;
			string cacheKey = this.GetCacheKey(configurationItem, isApplicationSpecific);

			HttpContext context = HttpContext.Current;
			if (context != null)
			{
				// Retrieve from HttpContext cache
				value = context.Cache[cacheKey];
			}
			else
			{
				// Retrieve from static field
				value = this.values[cacheKey];
			}

			return value;
		}

		/// <summary>
		/// Save a configuration value to value to cache.
		/// </summary>
		/// <param name="configurationItem">The configuration item to set.</param>
		/// <param name="value">The configuration value to save.</param>
		public void SetValue(ConfigurationItem configurationItem, object value)
		{
			this.SetValue(configurationItem, value, null);
		}

		/// <summary>
		/// Save a configuration value to value to cache.
		/// </summary>
		/// <param name="configurationItem">The configuration item to set.</param>
		/// <param name="value">The configuration value to save.</param>
		/// <param name="applicationSpecific">Indicates whether the configuration item is application specific. If null, uses the default value for the configuration item.</param>
		public void SetValue(ConfigurationItem configurationItem, object value, bool? applicationSpecific)
		{
			bool isApplicationSpecific = applicationSpecific.HasValue ? applicationSpecific.Value : configurationItem.ApplicationSpecific;
			string cacheKey = this.GetCacheKey(configurationItem, isApplicationSpecific);

			HttpContext context = HttpContext.Current;
			if (context != null)
			{
				context.Cache.Insert(cacheKey, value, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
			}
			else
			{
				this.values[cacheKey] = value;
			}
		}

		#endregion
	}
}
