namespace Dionysos
{
    /// <summary>
    /// Configuration class which contains constants for the names of configuration items
    /// </summary>
    public class DionysosConfigurationConstants
    {
        /// <summary>
        /// License name
        /// </summary>
        public const string LicenseName = "LicenseName";

        /// <summary>
        /// Default Recipient Address of Email messages (default: string.Empty)
        /// </summary>
        public const string EmailRecipientAddress = "EmailRecipientAddress";

        /// <summary>
        /// Default Recipient Name of Email messages (default: string.Empty)
        /// </summary>
        public const string EmailRecipientName = "EmailRecipientName";

        /// <summary>
        /// Default Sender Address of Email messages (default: no-reply@non-valid-address.com)
        /// </summary>
        public const string EmailSenderAddress = "EmailSenderAddress";

        /// <summary>
        /// Default Sender Name of Email messages  (default: System Generated Message)
        /// </summary>
        public const string EmailSenderName = "EmailSenderName";

        /// <summary>
        /// SMTP host to deliver mail messages to.
        /// </summary>
        public const string SmtpHost = "SmtpHost";

        /// <summary>
        /// SMTP password used for authentication.
        /// </summary>
        public const string SmtpPassword = "SmtpPassword";

        /// <summary>
        /// SMTP username used for authentication.
        /// </summary>
        public const string SmtpUsername = "SmtpUsername";

        /// <summary>
        /// SMTP timeout.
        /// </summary>
        public const string SmtpTimeout = "SmtpTimeout";

        /// <summary>
        /// SMTP port.
        /// </summary>
        public const string SmtpPort = "SmtpPort";

        /// <summary>
        /// Enable SSL for SMTP.
        /// </summary>
        public const string SmtpEnableSsl = "SmtpEnableSsl";

        /// <summary>
        /// Email Footer / Disclaimer
        /// </summary>
        public const string EmailFooter = "EmailFooter";

        /// <summary>
        /// Email subject for retrieving lost passwords
        /// </summary>
        public const string PasswordRetrievalEmailBody = "PasswordRetrievalEmailBody";

        /// <summary>
        /// Email body for retrieving lost passwords
        /// </summary>
        public const string PasswordRetrievalEmailSubject = "PasswordRetrievalEmailSubject";

        /// <summary>
        /// Error logs folder.
        /// </summary>
        public const string ErrorFileFolder = "ErrorFileFolder";

        /// <summary>
        /// Logs folder.
        /// </summary>
        public const string LogFileFolder = "LogFileFolder";

        /// <summary>
        /// Temporary file folder.
        /// </summary>
        public const string TempFileFolder = "TempFileFolder";

        /// <summary>
        /// CompanyLogoInHighResJpgRelativePath
        /// </summary>
        public const string CompanyLogoInHighResJpgRelativePath = "CompanyLogoInHighResJpgRelativePath";

        /// <summary>
        /// The default ImageSizeMode to use when resizing images.
        /// </summary>
        public const string DefaultImageSizeMode = "DefaultImageSizeMode";

        /// <summary>
        /// The default Color to use when filling images.
        /// </summary>
        public const string DefaultImageFillColor = "DefaultImageFillColor";

        /// <summary>
        /// The default overlay to use when getting the full image.
        /// </summary>
        public const string DefaultImageOverlay = "DefaultImageOverlay";

        /// <summary>
        /// The default overlay position to use when getting the full image.
        /// </summary>
        public const string DefaultImageOverlayPosition = "DefaultImageOverlayPosition";

        /// <summary>
        /// The applications base url.
        /// </summary>
        public const string BaseUrl = "BaseUrl";

        /// <summary>
        /// The current cloud environment.
        /// </summary>
        public const string CloudEnvironment = "CloudEnvironment";

        /// <summary>
        /// The API key for sending emails via SendGrid.
        /// </summary>
        public const string SendGridApiKey = "SendGridApiKey";

        /// <summary>
        /// The license key for service stack.
        /// </summary>
        public const string ServiceStackLicense = "servicestack:license";

        /// <summary>
        /// The url for the public gateway.
        /// </summary>
        public const string CraveGatewayPublicUrl = "CraveGatewayPublicUrl";

        /// <summary>
        /// The url for remote control tablets.
        /// </summary>
        public const string RemoteControlUrl = "RemoteControlUrl";

        /// The url for auditing user activity.
        /// <summary>
        /// </summary>
        public const string AuditServiceUrl = "AuditServiceUrl";

        /// <summary>
        /// The url for Cognito oauth.
        /// </summary>
        public const string CognitoBaseUrl = "CognitoBaseUrl";

        /// <summary>
        /// The client id for Cognito oauth.
        /// </summary>
        public const string CognitoClientId = "CognitoClientId";

        /// <summary>
        /// The secret for Cognito oauth.
        /// </summary>
        public const string CognitoSecret = "CognitoSecret";
    }
}
