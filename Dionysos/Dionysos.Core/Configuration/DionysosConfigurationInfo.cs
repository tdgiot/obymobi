﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;
using Dionysos.Drawing;
using System.Drawing;

namespace Dionysos
{
	/// <summary>
	/// Configuration class for Dionysos
	/// </summary>
	public class DionysosConfigurationInfo : ConfigurationItemCollection
	{
		/// <summary>
		/// Constructs an instance of the B2BConfiguration class
		/// </summary>
		public DionysosConfigurationInfo()
		{
			string sectionName = "Dionysos";
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.LicenseName, "Licentie Naam", "NoLicenseName", typeof(string)));

			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.EmailRecipientAddress, "Standaard e-mail ontvanger adres", string.Empty, typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.EmailRecipientName, "Standaard e-mail ontvanger naam", string.Empty, typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.EmailSenderAddress, "Standaard e-mail verzend adres", "no-reply@non-valid-address.com", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.EmailSenderName, "Standaard e-mail verzend naam", "Automatisch gegenereerd e-mailbericht.", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.EmailFooter, "Email voet / disclaimer", string.Empty, typeof(string)));

			//this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.SmtpHeloHost, "SMTP HELO-hostname", String.Empty, typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.SmtpHost, "SMTP-server", "localhost", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.SmtpPort, "SMTP-poort", 25, typeof(int)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.SmtpEnableSsl, "SMTP via SSL", false, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.SmtpUsername, "SMTP gebruikersnaam", String.Empty, typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.SmtpPassword, "SMTP wachtwoord", String.Empty, typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.SmtpTimeout, "SMTP-timeout", 5000, typeof(int)));

			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.PasswordRetrievalEmailSubject, "E-mail onderwerp voor ophalen wachtwoord", "Ophalen wachtwoord", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.PasswordRetrievalEmailBody, "E-mail body voor ophalen wachtwoord", "Geachte heer/mevrouw,<br /><br />in dit e-mailbericht vindt u uw gebruikersnaam en wachtwoord.<br /><br />Gebruikersnaam: <% UserName %><br />Wachtwoord:     <% Password %><br /><br />Met vriendelijke groet,<br />Afzender", typeof(string)));

			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.ErrorFileFolder, "Map voor error logs.", "~/App_Data/Error/", typeof(string), true));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.LogFileFolder, "Map voor logs.", "~/App_Data/Log/", typeof(string), true));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.TempFileFolder, "Map voor tijdelijke bestanden (wordt geleegd bij elke Application_End).", "~/App_Data/Temp/", typeof(string), true));

			//this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.AllowCustomViews, "Gebruikers mogen zelf kolommen instellen", true, typeof(bool)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.CompanyLogoInHighResJpgRelativePath, "Relatieve locatie van bedrijfslogo in hoge kwaliteit JPG", "~/logo.jpg", typeof(string)));

			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.DefaultImageSizeMode, "De standaard methode voor het vergroten/verkleinen van afbeelingen.", (int)ImageSizeMode.Zoom, typeof(ImageSizeMode)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.DefaultImageFillColor, "De standaard kleur voor het opvullen van afbeelingen.", Color.White.ToArgb(), typeof(int)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.DefaultImageOverlay, "Het relative pad (vanuit Media map of ~/) naar de standaard overlay voor volledige afbeeldingen.", "", typeof(string)));
			this.Add(new ConfigurationItem(sectionName, DionysosConfigurationConstants.DefaultImageOverlayPosition, "De positie van de geschaalde standaard overlay voor volledige afbeeldingen.", "BottomRight", typeof(ImageCropPosition)));
		}
	}
}
