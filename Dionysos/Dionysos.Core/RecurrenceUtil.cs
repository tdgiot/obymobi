﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Utility class for working with Recurrencesa
	/// </summary>
	public class RecurrenceUtil
	{
		/// <summary>
		/// Retrieve the Dutch names and integer values of the RecurrenceType to be used for example for a DropDownList
		/// </summary>
		public static Dictionary<string, int> GetRecurrenceTypesAsDictionary(RecurrenceType startingFrom)
		{
			Dictionary<string, int> toReturn = new Dictionary<string, int>();

			if ((int)RecurrenceType.Minutely >= (int)startingFrom)
				toReturn.Add("minuten", (int)RecurrenceType.Minutely);

			if ((int)RecurrenceType.Hourly >= (int)startingFrom)
				toReturn.Add("uren", (int)RecurrenceType.Hourly);

			if ((int)RecurrenceType.Daily >= (int)startingFrom)
				toReturn.Add("dagen", (int)RecurrenceType.Daily);

			if ((int)RecurrenceType.Weekly >= (int)startingFrom)
				toReturn.Add("weken", (int)RecurrenceType.Weekly);

			if ((int)RecurrenceType.Monthly >= (int)startingFrom)
				toReturn.Add("maanden", (int)RecurrenceType.Monthly);

			if ((int)RecurrenceType.Quarterly >= (int)startingFrom)
				toReturn.Add("kwartalen", (int)RecurrenceType.Quarterly);

			if ((int)RecurrenceType.Yearly >= (int)startingFrom)
				toReturn.Add("jaren", (int)RecurrenceType.Yearly);

			return toReturn;
		}

		/// <summary>
		/// Get the Dutch string name for a RecurrenceType, taken in account singular / plural
		/// </summary>
		/// <param name="type">Type</param>
		/// <param name="value">Value to determine singular / plural</param>
		public static string GetRecurrenceTypeStringName(RecurrenceType type, int value)
		{
			string toReturn = "ERROR";
			switch (type)
			{
				case RecurrenceType.Minutely:
					if (value == 1)
						toReturn = "minuut";
					else
						toReturn = "minuten";
					break;
				case RecurrenceType.Hourly:
					toReturn = "uur";
					break;
				case RecurrenceType.Daily:
					if (value == 1)
						toReturn = "dag";
					else
						toReturn = "dagen";
					break;
				case RecurrenceType.Weekly:
					if (value == 1)
						toReturn = "week";
					else
						toReturn = "weken";
					break;
				case RecurrenceType.Monthly:
					if (value == 1)
						toReturn = "maand";
					else
						toReturn = "maanden";
					break;
				case RecurrenceType.Quarterly:
					if (value == 1)
						toReturn = "kwartaal";
					else
						toReturn = "kwartalen";
					break;					
				case RecurrenceType.Yearly:
					toReturn = "jaar";
					break;
				default:
					throw new Dionysos.TechnicalException("There's no implementation for GetRecurrenceTypeStringName({0})", type.ToString());					
			}

			return toReturn;
		}

		/// <summary>
		/// Add a periode based on a quantity of recurrence types
		/// </summary>
		/// <param name="input">Date to add the period to</param>
		/// <param name="type">Type of recurrence</param>
		/// <param name="quantity">Quantity recurrence periods to add</param>
		/// <returns></returns>
		public static DateTime AddPeriodBasedOnRecurrenceTypeToDateTime(DateTime input, RecurrenceType type, int quantity)
		{
			DateTime toReturn;

			switch (type)
			{
				case RecurrenceType.Minutely:
					toReturn = input.AddMinutes(quantity);
					break;
				case RecurrenceType.Hourly:
					toReturn = input.AddHours(quantity);
					break;
				case RecurrenceType.Daily:
					toReturn = input.AddDays(quantity);
					break;
				case RecurrenceType.Weekly:
					toReturn = input.AddDays(quantity * 7);
					break;
				case RecurrenceType.Monthly:
					toReturn = input.AddMonths(quantity);
					break;
				case RecurrenceType.Quarterly:
					toReturn = input.AddMonths(quantity * 3);
					break;
				case RecurrenceType.Yearly:
					toReturn = input.AddYears(quantity);
					break;
				default:
					throw new Dionysos.TechnicalException("There's no implementation for AddPeriodBasedOnRecurrenceTypeToDateTime({0})", type.ToString());										
			}

			return toReturn;
		}
	}
}
