﻿//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Dionysos.Security
//{
//    public class LLBLGenRightsProvider : IRightsProvider
//    {
//        #region Fields
//        #endregion

//        #region Methods

//        ///// <summary>
//        ///// Check if a user can access/read a certain object
//        ///// </summary>
//        ///// <param name="objectType">Type of object (i.e. UIElement or Entity)</param>
//        ///// <param name="objectId">Identiefier of the object(i.e. UIElementId or (int)EntityType</param>
//        ///// <param name="userId">UserId of the user who wants to access</param>
//        ///// <returns>Boolean indicating if the user can access/read the object</returns>
//        //public bool CanRead(string objectType, int objectId, int userId)
//        //{
//        //    return Dionysos.Global.RightsProvider.CanRead(objectType, objectId, userId);
//        //}

//        ///// <summary>
//        ///// Check if a user can update a certain object
//        ///// </summary>
//        ///// <param name="objectType">Type of object (i.e. UIElement or Entity)</param>
//        ///// <param name="objectId">Identiefier of the object(i.e. UIElement of x.WebApplication.OrderView or EntityId of OrderEntity)</param>
//        ///// <param name="userId">UserId of the user who wants to access</param>
//        ///// <returns>Boolean indicating if the user can access/read the object</returns>
//        //public bool CanUpdate(string objectType, int objectId, int userId)
//        //{
//        //    return Dionysos.Global.RightsProvider.CanUpdate(objectType, objectId, userId);
//        //}

//        ///// <summary>
//        ///// Check if a user can create a certain object
//        ///// </summary>
//        ///// <param name="objectType">Type of object (i.e. UIElement or Entity)</param>
//        ///// <param name="objectId">Identiefier of the object(i.e. x.WebApplication.OrderView or OrderEntity</param>
//        ///// <param name="userId">UserId of the user who wants to access</param>
//        ///// <returns>Boolean indicating if the user can access/read the object</returns>
//        //public bool CanCreate(string objectType, int objectId, int userId)
//        //{
//        //    return Dionysos.Global.RightsProvider.CanCreate(objectType, objectId, userId);
//        //}

//        ///// <summary>
//        ///// Check if a user can delete a certain object
//        ///// </summary>
//        ///// <param name="objectType">Type of object (i.e. UIElement or Entity)</param>
//        ///// <param name="objectId">Identiefier of the object(i.e. x.WebApplication.OrderView or OrderEntity</param>
//        ///// <param name="userId">UserId of the user who wants to access</param>
//        ///// <returns>Boolean indicating if the user can access/read the object</returns>
//        //public bool CanDelete(string objectType, int objectId, int userId)
//        //{
//        //    return Dionysos.Global.RightsProvider.CanDelete(objectType, objectId, userId);
//        //}

//        #endregion

//        #region Event Handlers
//        #endregion

//        #region Properties
//        #endregion

//    }
//}
