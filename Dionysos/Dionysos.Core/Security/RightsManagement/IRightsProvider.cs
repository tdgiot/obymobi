﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Security
{
    /// <summary>
    /// Interface for user right management
    /// </summary>
    public interface IRightsProvider
    {
        #region Methods
        /// <summary>
        /// Check if a user can access/read a certain object
        /// </summary>
        /// <param name="objectType">Type of object (i.e. UIElement or Entity)</param>
        /// <param name="objectId">Identiefier of the object(i.e. x.WebApplication.OrderView or OrderEntity</param>
        /// <param name="userId">UserId of the user who wants to access</param>
        /// <returns>Boolean indicating if the user can access/read the object</returns>
        bool CanRead(string objectType, int objectId, int userId);

        /// <summary>
        /// Check if a user can update a certain object
        /// </summary>
        /// <param name="objectType">Type of object (i.e. UIElement or Entity)</param>
        /// <param name="objectId">Identiefier of the object(i.e. UIElement of x.WebApplication.OrderView or EntityId of OrderEntity)</param>
        /// <param name="userId">UserId of the user who wants to access</param>
        /// <returns>Boolean indicating if the user can access/read the object</returns>
        bool CanUpdate(string objectType, int objectId, int userId);

        /// <summary>
        /// Check if a user can create a certain object
        /// </summary>
        /// <param name="objectType">Type of object (i.e. UIElement or Entity)</param>
        /// <param name="objectId">Identiefier of the object(i.e. x.WebApplication.OrderView or OrderEntity</param>
        /// <param name="userId">UserId of the user who wants to access</param>
        /// <returns>Boolean indicating if the user can access/read the object</returns>
        bool CanCreate(string objectType, int objectId, int userId);

        /// <summary>
        /// Check if a user can delete a certain object
        /// </summary>
        /// <param name="objectType">Type of object (i.e. UIElement or Entity)</param>
        /// <param name="objectId">Identiefier of the object(i.e. x.WebApplication.OrderView or OrderEntity</param>
        /// <param name="userId">UserId of the user who wants to access</param>
        /// <returns>Boolean indicating if the user can access/read the object</returns>
        bool CanDelete(string objectType, int objectId, int userId);

        #endregion
    }
}
