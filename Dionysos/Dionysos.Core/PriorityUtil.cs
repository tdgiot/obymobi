﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Collections;

namespace Dionysos
{
	/// <summary>
	/// Utility class for working with Priority
	/// </summary>
	public class PriorityUtil
	{
		/// <summary>
		/// Retrieve the Dutch names and integer values of the Priority to be used for example for a DropDownList
		/// </summary>
		public static OrderedDictionary<string, int> GetPriorityAsDictionary()
		{
			OrderedDictionary<string, int> toReturn = new OrderedDictionary<string, int>();

			toReturn.Add("Normaal", (int)Priority.Normal);
			toReturn.Add("Laag", (int)Priority.Low);			
			toReturn.Add("Hoog", (int)Priority.High);

			return toReturn;
		}

		/// <summary>
		/// Retrieve the textual representation of the priority value
		/// </summary>
		/// <param name="priority">Priority value</param>
		/// <returns></returns>
		public static string GetTextValue(int priority)
		{
			if (priority == (int)Priority.Normal)
				return "Normaal";
			else if (priority == (int)Priority.High)
				return "Hoog";
			else if (priority == (int)Priority.Low)
				return "Laag";
			else
				return "Onbekend";
		}

	}
}
