﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
    public class TimeRange
    {
        #region Constructors

        public TimeRange(Time start, Time end)
        {
            this.Start = start;
            this.End = end;
        }

        #endregion

        #region Properties

        public Time Start
        { get; set; }

        public Time End
        { get; set; }

        #endregion
    }
}
