using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Dionysos
{
    /// <summary>
    /// Class which represents an Dutch telephone number
    /// </summary>
    public class TelephoneNumber
    {
        #region Fields

        private static TelephoneNumber instance = null;
        private string value = string.Empty; 

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.TelephoneNumber type
        /// </summary>
        public TelephoneNumber()
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx-media.TelephoneNumber type
        /// using the specified email address
        /// </summary>
        public TelephoneNumber(string telephoneNumber)
        {
            this.value = telephoneNumber;
        }

        /// <summary>
        /// Constructs a static instance of the Lynx-media.TelephoneNumber type
        /// </summary>
        static TelephoneNumber()
        {
            if (instance == null)
            {
                instance = new TelephoneNumber();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Converts a string representation to a Lynx-media.TelephoneNumber instance
        /// </summary>
        /// <param name="s">The System.String instance to convert to a Lynx-media.TelephoneNumber instance</param>
        /// <param name="telephoneNumber">The Lynx-media.TelephoneNumber instance to set</param>
        /// <returns>True if conversion was successful, False if not</returns>
        public static bool TryParse(string s, out TelephoneNumber telephoneNumber)
        {
            bool success = true;

            telephoneNumber = new TelephoneNumber();
            if (!Instance.Empty(s))
            {
                Regex emailExpression = new Regex(@"^0(\d{2}-\d{7})|(\d{2}\s\d{7})|(\d{3}-\d{6})|(\d{10})|(\d{3}\s\d{6})|(6-\d{8})|(6\s\d{8})|(d{9})$");
                if (emailExpression.IsMatch(s))
                {
                    telephoneNumber.Value = s;
                }
                else
                {
                    success = false;
                }
            }

            return success;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the phone number
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        #endregion
    }
}
