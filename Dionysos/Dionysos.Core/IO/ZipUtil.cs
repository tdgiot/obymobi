﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using System.Windows.Forms;

namespace Dionysos.IO
{
	/// <summary>
	/// Utility class for ZIP file operations.
	/// </summary>
	public class ZipUtil
	{
		#region Methods

		/// <summary>
		/// Creates an ZipOutputStream instance for the specified filepath
		/// </summary>
		/// <param name="filePath">The path of the file to create the ZipOutputStream instance for</param>
		/// <returns>A ZipOutputStream instance</returns>
		public static ZipOutputStream CreateZipOutputStream(string filePath)
		{
			return CreateZipOutputStream(filePath, true, 9);
		}

		/// <summary>
		/// Creates an ZipOutputStream instance for the specified filepath
		/// </summary>
		/// <param name="filePath">The path of the file to create the ZipOutputStream instance for</param>
		/// <param name="compressionLevel">The level of compression for the zip file (0 is store only, 9 is best compression level)</param>
		/// <returns>A ZipOutputStream instance</returns>
		public static ZipOutputStream CreateZipOutputStream(string filePath, int compressionLevel)
		{
			return CreateZipOutputStream(filePath, true, compressionLevel);
		}

		/// <summary>
		/// Creates an ZipOutputStream instance for the specified filepath
		/// </summary>
		/// <param name="filePath">The path of the file to create the ZipOutputStream instance for</param>
		/// <param name="overwrite">Flag which indicates if an existing file should be overwritten</param>
		/// <returns>A ZipOutputStream instance</returns>
		public static ZipOutputStream CreateZipOutputStream(string filePath, bool overwrite)
		{
			return CreateZipOutputStream(filePath, overwrite, 9);
		}

		/// <summary>
		/// Creates an ZipOutputStream instance for the specified filepath
		/// </summary>
		/// <param name="filePath">The path of the file to create the ZipOutputStream instance for</param>
		/// <param name="overwrite">Flag which indicates if an existing file should be overwritten</param>
		/// <param name="compressionLevel">The level of compression for the zip file (0 is store only, 9 is best compression level)</param>
		/// <returns>A ZipOutputStream instance</returns>
		public static ZipOutputStream CreateZipOutputStream(string filePath, bool overwrite, int compressionLevel)
		{
			ZipOutputStream zipOutputStream = null;

			if (Instance.ArgumentIsEmpty(filePath, "filePath"))
			{
				// Parameter 'filePath' is empty
			}
			else
			{
				// Check if the zip file exists
				if (File.Exists(filePath) && overwrite)
				{
					// First delete the zipfile.
					File.Delete(filePath);
				}

				// Now create the file and open the zip out put stream.
				FileStream fileStream = File.Create(filePath);
				zipOutputStream = new ZipOutputStream(fileStream);

				// Check whether the specified compression level is valid
				if (compressionLevel < 0)
					compressionLevel = 0;
				if (compressionLevel > 9)
					compressionLevel = 9;

				// Set the compression level
				zipOutputStream.SetLevel(compressionLevel); // 0 - store only to 9 - means best compression
			}

			return zipOutputStream;
		}

		/// <summary>
		/// Unzips the file to the same directory (without overwriting existing files).
		/// </summary>
		/// <param name="zipFilePath">The path of the file to unzip.</param>
		/// <returns>
		///   <c>true</c> if unzipping was successful; otherwise, <c>false</c>.
		/// </returns>
		public static bool Unzip(string zipFilePath)
		{
			return Unzip(zipFilePath, null, false);
		}

		/// <summary>
		/// Unzips the file to the same directory.
		/// </summary>
		/// <param name="zipFilePath">The path of the zip file to unzip.</param>
		/// <param name="overwrite"><c>true</c> if existing files should be overwritten; otherwise, <c>false</c>.</param>
		/// <returns>
		///   <c>true</c> if unzipping was successful; otherwise, <c>false</c>.
		/// </returns>
		public static bool Unzip(string zipFilePath, bool overwrite)
		{
			return Unzip(zipFilePath, null, overwrite);
		}

		/// <summary>
		/// Unzips the file to the specified directory (without overwriting existing files).
		/// </summary>
		/// <param name="zipFilePath">The path of the zip file to unzip.</param>
		/// <param name="outputDirectory">The directory to extract the file to.</param>
		/// <returns>
		///   <c>true</c> if unzipping was successful; otherwise, <c>false</c>.
		/// </returns>
		public static bool Unzip(string zipFilePath, string outputDirectory)
		{
			return Unzip(zipFilePath, outputDirectory, false);
		}

		/// <summary>
		/// Unzips the file to the specified directory.
		/// </summary>
		/// <param name="zipFilePath">The path of the zip file to unzip.</param>
		/// <param name="outputDirectory">The directory to extract the file to.</param>
		/// <param name="overwrite"><c>true</c> if existing files should be overwritten; otherwise, <c>false</c>.</param>
		/// <returns>
		///   <c>true</c> if unzipping was successful; otherwise, <c>false</c>.
		/// </returns>
		public static bool Unzip(string zipFilePath, string outputDirectory, bool overwrite)
		{
			List<string> unzippedFiles;
			return Unzip(zipFilePath, outputDirectory, overwrite, out unzippedFiles);
		}

		/// <summary>
		/// Unzips the file to the specified directory.
		/// </summary>
		/// <param name="zipFilePath">The path of the zip file to unzip.</param>
		/// <param name="outputDirectory">The directory to extract the file to.</param>
		/// <param name="overwrite"><c>true</c> if existing files should be overwritten; otherwise, <c>false</c>.</param>
		/// <param name="unzippedFiles">All unzipped files from the file, also the non-overwritten.</param>
		/// <returns>
		///   <c>true</c> if unzipping was successful; otherwise, <c>false</c>.
		/// </returns>
		public static bool Unzip(string zipFilePath, string outputDirectory, bool overwrite, out List<string> unzippedFiles)
		{
			if (zipFilePath == null) throw new ArgumentNullException("zipFilePath");
			if (String.IsNullOrEmpty(zipFilePath)) throw new ArgumentException("zipFilePath");

			if (String.IsNullOrEmpty(outputDirectory))
			{
				outputDirectory = Path.GetDirectoryName(zipFilePath);
			}

			FileStream zipFileStream = File.OpenRead(zipFilePath);

			return Unzip(zipFileStream, outputDirectory, overwrite, out unzippedFiles);
		}

		/// <summary>
		/// Unzips the file stream to the specified directory (without overwriting existing files).
		/// </summary>
		/// <param name="zipFileStream">The stream of the zip file to unzip.</param>
		/// <param name="outputDirectory">The directory to extract the file to.</param>
		/// <returns>
		///   <c>true</c> if unzipping was successful; otherwise, <c>false</c>.
		/// </returns>
		public static bool Unzip(Stream zipFileStream, string outputDirectory)
		{
			return Unzip(zipFileStream, outputDirectory, false);
		}

		/// <summary>
		/// Unzips the file stream to the specified directory.
		/// </summary>
		/// <param name="zipFileStream">The stream of the zip file to unzip.</param>
		/// <param name="outputDirectory">The directory to extract the file to.</param>
		/// <param name="overwrite"><c>true</c> if existing files should be overwritten; otherwise, <c>false</c>.</param>
		/// <returns>
		///   <c>true</c> if unzipping was successful; otherwise, <c>false</c>.
		/// </returns>
		public static bool Unzip(Stream zipFileStream, string outputDirectory, bool overwrite)
		{
			List<string> unzippedFiles;
			return Unzip(zipFileStream, outputDirectory, overwrite, out unzippedFiles);
		}

        /// <summary>
        /// Unzips the first entry of the zip file as text.
        /// </summary>
        /// <param name="zipFileStream">The zip file stream.</param>
        /// <returns>
        /// The text of the first entry in the zip file.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">zipFileStream</exception>
	    public static string UnzipText(Stream zipFileStream)
	    {
	        String filename;
	        return UnzipText(zipFileStream, out filename);
	    }

	    /// <summary>
	    /// Unzips the first entry of the zip file as text.
	    /// </summary>
	    /// <param name="zipFileStream">The zip file stream.</param>
	    /// <param name="filename"></param>
	    /// <returns>
	    /// The text of the first entry in the zip file.
	    /// </returns>
	    /// <exception cref="System.ArgumentNullException">zipFileStream</exception>
	    public static string UnzipText(Stream zipFileStream, out string filename)
	    {
	        filename = "";

			if (zipFileStream == null) throw new ArgumentNullException("zipFileStream");

			string text = null;
			using (MemoryStream ms = new MemoryStream())
			{
				using (ZipInputStream zipInputStream = new ZipInputStream(zipFileStream))
				{
					ZipEntry entry = zipInputStream.GetNextEntry();
					if (entry != null)
					{
						zipInputStream.Copy(ms);
					    filename = entry.Name;
					}
				}

				// Set memory stream to beginning
				ms.Position = 0;

				// Convert to string
				using (StreamReader sr = new StreamReader(ms))
				{
					text = sr.ReadToEnd();
				}
			}

			return text;
		}

		/// <summary>
		/// Unzips the file stream to the specified directory.
		/// </summary>
		/// <param name="zipFileStream">The stream of the zip file to unzip.</param>
		/// <param name="outputDirectory">The directory to extract the file to.</param>
		/// <param name="overwrite"><c>true</c> if existing files should be overwritten; otherwise, <c>false</c>.</param>
		/// <param name="unzippedFiles">All unzipped files from the file, also the non-overwritten.</param>
		/// <returns>
		///   <c>true</c> if unzipping was successful; otherwise, <c>false</c>.
		/// </returns>
		public static bool Unzip(Stream zipFileStream, string outputDirectory, bool overwrite, out List<string> unzippedFiles)
		{
			if (zipFileStream == null) throw new ArgumentNullException("zipFileStream");
			if (outputDirectory == null) throw new ArgumentNullException("outputDirectory");
			if (String.IsNullOrEmpty(outputDirectory)) throw new ArgumentException("outputDirectory");

			unzippedFiles = new List<string>();

			// Create and initialize a zip input stream using the file stream
			using (ZipInputStream zipInputStream = new ZipInputStream(zipFileStream))
			{
				// We will read all the entry's from the zip file.
				ZipEntry zipEntry;
				while ((zipEntry = zipInputStream.GetNextEntry()) != null)
				{
					// Get the directory name and path
					string directoryName = Path.GetDirectoryName(zipEntry.Name);
					string directoryPath = Path.Combine(outputDirectory, directoryName);
					if (!Directory.Exists(directoryPath))
					{
						// Create directory
						Directory.CreateDirectory(directoryPath);
					}

					// Get the file name and path
					string fileName = Path.GetFileName(zipEntry.Name);
					string filePath = Path.Combine(directoryPath, fileName);
					if (!String.IsNullOrEmpty(fileName) &&
						(!File.Exists(filePath) || overwrite))
					{
						// Create or overwrite the file
						using (FileStream writeStream = File.Create(filePath))
						{
							zipInputStream.Copy(writeStream);
						}

						unzippedFiles.Add(filePath);
					}
				}
			}

			return true;
		}

        private const int defaultCompressionLevel = 7;

		/// <summary>
		/// Creates a zip file for the files in the specified directory
		/// </summary>
		/// <param name="inputFolderPath">The directory to create the zipfile from</param>
		/// <param name="outputPathAndFile">The file path to the zip file</param>
		public static void ZipDirectory(string inputFolderPath, string outputPathAndFile, int compressionLevel = defaultCompressionLevel)
		{
			ZipDirectory(inputFolderPath, outputPathAndFile, true, null, compressionLevel);
		}

        /// <summary>
        /// Creates a zip file for the files in the specified directory
        /// </summary>
        /// <param name="inputFolderPath">The directory to create the zipfile from</param>
        /// <param name="outputPathAndFile">The file path to the zip file</param>
        public static void ZipDirectory(string inputFolderPath, Stream outputStream, int compressionLevel = defaultCompressionLevel)
        {
            ZipFiles(inputFolderPath, outputStream: outputStream, compressionLevel: compressionLevel, pathToTrim: inputFolderPath);
        }

		/// <summary>
		/// Creates a zip file for the files in the specified directory
		/// </summary>
		/// <param name="inputFolderPath">The directory to create the zipfile from</param>
		/// <param name="outputPathAndFile">The file path to the zip file</param>
		/// <param name="includeSourceFolder">if set to <c>true</c> include the source folder in the zip file.</param>
		public static void ZipDirectory(string inputFolderPath, string outputPathAndFile, bool includeSourceFolder)
		{
			ZipDirectory(inputFolderPath, outputPathAndFile, includeSourceFolder, null);
		}

		/// <summary>
		/// Creates a zip file for the files in the specified directory
		/// </summary>
		/// <param name="inputFolderPath">The directory to create the zipfile from</param>
		/// <param name="outputPathAndFile">The file path to the zip file</param>
		/// <param name="includeSourceFolder">if set to <c>true</c> include the source folder in the zip file.</param>
		/// <param name="progressBar">The progress bar control instance to display the progress on</param>
        public static void ZipDirectory(string inputFolderPath, string outputPathAndFile, bool includeSourceFolder, object progressBar, int compressionLevel = defaultCompressionLevel)
		{
			List<string> filePaths = GenerateFileList(inputFolderPath); // generate file list

			if (includeSourceFolder)
				ZipFiles(filePaths, outputPathAndFile, null, Directory.GetParent(inputFolderPath).ToString(), progressBar);
			else
				ZipFiles(filePaths, outputPathAndFile, null, inputFolderPath, progressBar);
		}

		/// <summary>
		/// Creates a zip file for the files specified
		/// </summary>
		/// <param name="filePaths">The list containing the paths to the files to zip</param>
		/// <param name="outputPathAndFile">The file path to the zip file</param>
		/// <param name="pathToTrim">The trailing path to remove from the file paths in the zip</param>
		public static void ZipFiles(List<string> filePaths, string outputPathAndFile, string pathToTrim)
		{
			ZipFiles(filePaths, outputPathAndFile, null, pathToTrim, null);
		}

        public static void ZipFiles(string inputFolderPath, string outputPathAndFile = "", Stream outputStream = null, string pathToTrim = "", object progressBar = null, int compressionLevel = defaultCompressionLevel)
        {
            List<string> filePaths = GenerateFileList(inputFolderPath); // generate file list
            ZipUtil.ZipFiles(filePaths, outputPathAndFile, outputStream, pathToTrim, progressBar, compressionLevel);
        }

		/// <summary>
		/// Creates a zip file for the files specified
		/// </summary>
		/// <param name="filePaths">The list containing the paths to the files to zip</param>
		/// <param name="outputPathAndFile">The file path to the zip file</param>
		/// <param name="pathToTrim">The trailing path to remove from the file paths in the zip</param>
		/// <param name="progressBar">The progress bar.</param>
        public static void ZipFiles(List<string> filePaths, string outputPathAndFile = "", Stream outputStream = null, string pathToTrim = "", object progressBar = null, int compressionLevel = defaultCompressionLevel)
		{
            if (!outputPathAndFile.IsNullOrWhiteSpace() && outputStream != null)
                throw new FunctionalException("You can not provide an outputPathAndFile AND an outputStream.");
            else if(outputPathAndFile.IsNullOrWhiteSpace() && outputStream == null)
                throw new FunctionalException("You must provide an outputPathAndFile or an outputStream.");

			int TrimLength = pathToTrim.Length;

			// find number of chars to remove     // from orginal file path
			TrimLength += 1; //remove '\'
			FileStream ostream;
			byte[] obuffer;
			ZipOutputStream oZipStream;
            if (!outputPathAndFile.IsNullOrWhiteSpace())
                oZipStream = new ZipOutputStream(File.Create(outputPathAndFile)); // create zip stream
            else
                oZipStream = new ZipOutputStream(outputStream);

            oZipStream.SetLevel(compressionLevel); // maximum compression
			ZipEntry oZipEntry;

			ProgressBar winFormsProgressBar = null;
			if (progressBar != null && progressBar is ProgressBar)
			{
				winFormsProgressBar = progressBar as ProgressBar;
				winFormsProgressBar.Minimum = 1;
				if (filePaths.Count > 1)
					winFormsProgressBar.Maximum = filePaths.Count;
				else
					winFormsProgressBar.Maximum = 1;
				winFormsProgressBar.Value = 1;
			}

			foreach (string Fil in filePaths) // for each file, generate a zipentry
			{
				oZipEntry = new ZipEntry(Fil.Remove(0, TrimLength));
				oZipStream.PutNextEntry(oZipEntry);

				if (!Fil.EndsWith(@"/")) // if a file ends with '/' its a directory
				{
					ostream = File.OpenRead(Fil);
					obuffer = new byte[ostream.Length];
					ostream.Read(obuffer, 0, obuffer.Length);
					oZipStream.Write(obuffer, 0, obuffer.Length);
					ostream.Dispose();
				}

				if (winFormsProgressBar != null && winFormsProgressBar.Value < winFormsProgressBar.Maximum)
				{
					winFormsProgressBar.Value++;
					System.Windows.Forms.Application.DoEvents();
				}
			}
			oZipStream.Finish();
			oZipStream.Close();
		}

        /// <summary>
        /// In memory zipping - Combine & compress file streams into one zip file stream
        /// </summary>
        /// <param name="files"></param>
        /// <param name="outputStream"></param>
        /// <param name="pathToTrim"></param>
        /// <param name="progressBar"></param>
        /// <param name="compressionLevel"></param>
        public static void ZipByteArrays(Dictionary<String,byte[]> files, Stream outputStream, int compressionLevel = defaultCompressionLevel)
        {
            // find number of chars to remove     // from orginal file path                        
            ZipOutputStream zipStream = new ZipOutputStream(outputStream);
            zipStream.SetLevel(compressionLevel); // maximum compression            
            
            foreach (var file in files) // for each file, generate a zipentry
            {
                ZipEntry zipEntry = new ZipEntry(file.Key);
                zipEntry.DateTime = DateTime.UtcNow;
                zipStream.PutNextEntry(zipEntry);
                using (MemoryStream ms = new MemoryStream(file.Value))
                {
                    StreamUtil.Copy(ms, zipStream);
                }
                zipStream.CloseEntry();
                
            }

            zipStream.IsStreamOwner = false;
            zipStream.Close();
        }

        /// <summary>
        /// Generates a list of files (including subdirectories) for the specified directory
        /// </summary>
        /// <param name="directory">The directory to get the list of files for</param>
        /// <returns>A System.Collections.Generic.List instance containing the files</returns>
        private static List<string> GenerateFileList(string directory)
		{
			List<string> filePaths = new List<string>();
			bool empty = true;
			foreach (string filePath in Directory.GetFiles(directory)) // add each file in directory
			{
				filePaths.Add(filePath);
				empty = false;
			}

			if (empty)
			{
				if (Directory.GetDirectories(directory).Length == 0)
				// if directory is completely empty, add it
				{
					filePaths.Add(directory + @"/");
				}
			}

			foreach (string dirs in Directory.GetDirectories(directory)) // recursive
			{
				filePaths.AddRange(GenerateFileList(dirs));
			}

			return filePaths; // return file list
		}

		#endregion
	}
}
