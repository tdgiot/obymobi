using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Dionysos
{
    /// <summary>
    /// Singleton utility class used for paths in the file system
    /// </summary>
    public class PathUtil
    {
        #region Fields

        static PathUtil instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Path class if the instance has not been initialized yet
        /// </summary>
        public PathUtil()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new PathUtil();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
        }

        /// <summary>
        /// Resolves a path to a directory in the file system
        /// </summary>
        /// <param name="path">The path to resolve</param>
        /// <returns>A System.String instance containing the resolved path</returns>
        public static string ResolvePath(string path)   
        {
            string resolvedPath = string.Empty;
            
            if (Instance.ArgumentIsEmpty(path, "path"))
            {
                // Argument 'path' is empty
            }
            else
            {
                if (System.IO.Path.IsPathRooted(path))
                {
                    resolvedPath = path;
                }
                else if (path.Substring(0, 2) == "\\")
                {
                    resolvedPath = path;
                }
                else if (path.StartsWith("~"))
                {
                    resolvedPath = Path.Combine(Application.StartupPath, path.Substring(2));
                }
                else
                {
                    resolvedPath = Path.Combine(Application.StartupPath, path);
                }
            }

            return resolvedPath;
        }

        /// <summary>
        /// Checks a path for invalid characters
        /// </summary>
        /// <param name="path">The path to check</param>
        /// <returns>True if the path contains invalid path characters</returns>
        public static bool ContainsInvalidPathChars(String path)
        {
            bool contains = false;
            foreach(char invalidChar in System.IO.Path.GetInvalidPathChars())
            {
                if (path.Contains(invalidChar.ToString()))
                {
                    contains = true;
                    break;
                }
            }
            return contains;
        }

        /// <summary>
        /// Removes the illegal characters in a path
        /// </summary>
        /// <param name="path">The path to check</param>
        /// <returns>The new path with the illegal characters removed</returns>
        public static string RemoveInvalidPathChars(String path)
        {
            char[] invalidChars = System.IO.Path.GetInvalidPathChars();

            foreach (char c in invalidChars)
            {
                path = path.Replace(c.ToString(), string.Empty);
            }

            return path;
        }

        #endregion
    }
}