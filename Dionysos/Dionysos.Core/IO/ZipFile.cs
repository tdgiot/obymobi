﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Collections;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using ICSharpCode.SharpZipLib.Checksum;

namespace Dionysos.IO
{
	/// <summary>
	/// Summary description for ZipFile.
	/// </summary>
	public class ZipFile
	{
		#region Fields

		private string mExtractDirOverride = string.Empty;
		//private string mSubDir;
		private ZipInputStream mZipInputStream = null;
		private ZipOutputStream mZipOutputStream = null;
		private string mZipFilePath = string.Empty;
		private string mSourceDirectory = string.Empty;
		private string mSourceFile = string.Empty;
		private string[] mSourceFiles;
		private bool mOverrideZipFile = true;
		private bool mRemoveSourceDirectoryPath = true;
		private bool mRemoveSourceDirectoryName = false;
		private bool mUseTempDirectory = false;
		private int mCompressionLevel;
		//private int mReadWriteSize;
		private string mFileFilter = string.Empty;
		//private bool mHoldDirectoryStructure;
		private string[] mFileFilters = null;
		private bool mIncludeSubFolders = true;
		private ArrayList mFileNames = new ArrayList();

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Dionysos.IO.ZipFile type
		/// </summary>
		public ZipFile()
		{
		}

		#endregion

		#region Methods

		/// <summary>
		/// Extracts the ZipFile.ZipFileFullPath to the current directory or ZipFile.ExtractDirOverride.
		/// </summary>
		/// <returns>extraction successfull.</returns>
		public bool ExtractFile()
		{
			// Extract the file

			bool lReturnValue = false;
			string sDirectoryTarget = "";
			ZipEntry oCurrentEntry = null;

			// Check if the file exists
			if (File.Exists(this.mZipFilePath))
			{
				// TODO: progress
				//if (this.mProgressBar != null)
				//{
				//    FileInfo oFileInfo = new FileInfo(this.mZipFilePath);
				//    // Init the progressbar
				//    this.mProgressBar.Value = 0;
				//    this.mProgressBar.Maximum = Convert.ToInt32(oFileInfo.Length);
				//    oFileInfo = null;
				//}

				// Then we can create the input stream form the file.
				this.mZipInputStream = new ZipInputStream(File.OpenRead(this.mZipFilePath));

				// We will read all the entry's from the zip file.
				while ((oCurrentEntry = this.mZipInputStream.GetNextEntry()) != null)
				{
					// Get the subdirname and the filename of the entry.
					string sDirectoryName = Path.GetDirectoryName(oCurrentEntry.Name);
					string sFileName = Path.GetFileName(oCurrentEntry.Name);

					// If we don't have a override dir
					if (this.mExtractDirOverride.Length == 0)
					{
						// we will use them from the entry
						if ((sDirectoryName.Length > 0) && (!Directory.Exists(sDirectoryName)))
						{
							// create directory
							Directory.CreateDirectory(sDirectoryName);
						}

						// Set the target directory to the entry directory.
						sDirectoryTarget = sDirectoryName;
					}
					else
					{
						// we will use them from the entry
						if (!Directory.Exists(this.mExtractDirOverride))
						{
							// create directory
							Directory.CreateDirectory(this.mExtractDirOverride);
						}

						// Set the target directory to the override directory
						sDirectoryTarget = this.mExtractDirOverride;
					}

					// is the entry a file or a directory
					// Set the target directory to the override directory
					sDirectoryTarget = Path.Combine(this.ExtractDirOverride, sDirectoryName);

					// When the filename is empty, we have a directory
					if (sFileName == String.Empty)
					{
						if (!Directory.Exists(Path.Combine(this.ExtractDirOverride, sDirectoryName)))
						{
							// The directory doesn't exist
							Directory.CreateDirectory(Path.Combine(this.ExtractDirOverride, sDirectoryName));
						}
					}
					else
					{
						// The entry is a file so we will create is it
						FileStream oStreamWriter;
						int nSize;
						byte[] aData;

						// Create the file.
						oStreamWriter = File.Create(Path.Combine(sDirectoryTarget, sFileName));

						// Set the Buffer size and buffer array.
						nSize = 2048;
						aData = new byte[nSize];

						// Repeat until we can stop the extraction.
						while (true)
						{
							// Read the data.
							nSize = this.mZipInputStream.Read(aData, 0, aData.Length);

							// TODO: progress
							//if (this.mProgressBar != null)
							//{
							//    // Update the progress bar.
							//    this.mProgressBar.Step = nSize;
							//    this.mProgressBar.PerformStep();
							//}

							// Is there data
							if (nSize > 0)
							{
								// Write the data
								oStreamWriter.Write(aData, 0, nSize);
							}
							else
							{
								// else stop writing and stop the loop
								break;
							}
						}

						// Close the file that we have written.
						oStreamWriter.Close();
						lReturnValue = true;
					}
				}

				// Close the zipfile.
				this.mZipInputStream.Close();
				lReturnValue = true;
			}

			// Return the result.
			return lReturnValue;
		}
		// end of ExtractFile

		/// <summary>
		/// 
		/// </summary>
		protected void CreateZipFile()
		{
			FileStream oZipFile;
			// Check if the zip file exists
			if (File.Exists(this.mZipFilePath) && this.mOverrideZipFile)
			{
				// First delete the zipfile.
				File.Delete(this.mZipFilePath);
			}

			if (!Directory.Exists(Path.GetDirectoryName(this.mZipFilePath)))
			{
				Directory.CreateDirectory(Path.GetDirectoryName(this.mZipFilePath));
			}

			// Now create the file and open the zip out put stream.
			oZipFile = System.IO.File.Create(this.mZipFilePath);
			this.mZipOutputStream = new ZipOutputStream(oZipFile);

			// Set the compression level
			this.mZipOutputStream.SetLevel(9); // 0 - store only to 9 - means best compression
		}

		/// <summary>
		/// Get the name of the entry of the file in the zip.
		/// </summary>
		/// <param name="sFileName">full path of the file name.</param>
		/// <returns>The entry name</returns>
		protected string GetEntryName(string sFileName)
		{
			// Example:
			// sFileName				C:\Program Files\Update-it\UpdateCentrum.exe
			// SourceDirectoryPath		C:\Program Files\Update-it
			// SourceDirectoryName		Update-it
			// this.mSourceDirectory	D:\__Test

			if (this.mIncludeSubFolders)
			{
				// We want to include the subfolder

				// Remove the leading source directory name
				if (this.mRemoveSourceDirectoryPath)
				{
					if (this.mRemoveSourceDirectoryName)
					{
						// UpdateCentrum.exe

						sFileName = sFileName.Replace(this.mSourceDirectory, "");
					}
					else
					{
						// Update-it\UpdateCentrum.exe
						if (this.mSourceDirectory.EndsWith(@"\"))
						{
							this.mSourceDirectory = this.mSourceDirectory.Remove(this.mSourceDirectory.Length - 1, 1);
						}
						int nLastBackslash = this.mSourceDirectory.LastIndexOf(@"\");
						if (nLastBackslash != -1)
						{
							string sSourceDirTemp = this.mSourceDirectory.Substring(nLastBackslash + 1, this.mSourceDirectory.Length - nLastBackslash - 1);
							sFileName = sFileName.Replace(this.mSourceDirectory, "");
							if (sFileName.StartsWith(@"\"))
							{
								sFileName = sSourceDirTemp + sFileName;
							}
							else
							{
								sFileName = Path.Combine(sSourceDirTemp, sFileName);
							}
						}
					}
				}
				else
				{
					// C:\Program Files\Update-it\UpdateCentrum.exe
				}
			}
			else
			{
				sFileName = Path.GetFileName(sFileName);
			}

			return sFileName;
		}

		private string[] GetFileNames()
		{
			this.mFileNames = new ArrayList();

			string[] aFilenames = null;

			if ((this.mSourceDirectory != null) && (this.mSourceDirectory.Length > 0))
			{
				// Get the files from that source directory.
				if (this.mFileFilter.Length > 0)
				{
					if (this.mIncludeSubFolders)
					{
						this.PopulateFiles(this.mSourceDirectory, this.mFileFilter);
					}
					else
					{
						aFilenames = Directory.GetFiles(this.mSourceDirectory, this.mFileFilter);
					}
				}
				else if ((this.mFileFilters != null) && (this.mFileFilters.Length > 0))
				{
					foreach (string sCurrentFilter in this.mFileFilters)
					{
						if (aFilenames != null)
						{
							if (this.mIncludeSubFolders)
							{
								foreach (string sFileName in aFilenames)
								{
									this.mFileNames.Add((string)sFileName);
									this.PopulateFiles(this.mSourceDirectory, sCurrentFilter);
								}
							}
							else
							{
								aFilenames = MergArrayOfString(aFilenames, Directory.GetFiles(this.mSourceDirectory, sCurrentFilter));
							}
						}
						else
						{
							if (this.mIncludeSubFolders)
							{
								this.PopulateFiles(this.mSourceDirectory, sCurrentFilter);
							}
							else
							{
								aFilenames = Directory.GetFiles(this.mSourceDirectory, sCurrentFilter);
							}
						}
					}
				}
				else
				{
					if (this.mIncludeSubFolders)
					{
						this.PopulateFiles(this.mSourceDirectory);
					}
					else
					{
						aFilenames = Directory.GetFiles(this.mSourceDirectory);
					}
				}
			}

			if (this.mIncludeSubFolders)
			{
				aFilenames = new string[this.mFileNames.Count];
				for (int i = 0; i < this.mFileNames.Count; i++)
				{
					aFilenames[i] = (string)this.mFileNames[i];
				}
			}

			return aFilenames;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="directoryValue"></param>
		protected void PopulateFiles(string directoryValue)
		{
			// Works!
			string[] aDirectories = Directory.GetDirectories(directoryValue);
			string[] aFiles = Directory.GetFiles(directoryValue);

			if (aFiles.Length != 0)
			{
				foreach (string sFile in aFiles)
				{
					this.mFileNames.Add((string)sFile);
				}
			}

			if (aDirectories.Length != 0)
			{
				foreach (string sDirectory in aDirectories)
				{
					//foreach (string sFile in Directory.GetFiles(sDirectory))
					//{
					//	this.mFileNames.Add((string)sFile);
					//}

					this.PopulateFiles(sDirectory);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="directoryValue"></param>
		/// <param name="sFilter"></param>
		protected void PopulateFiles(string directoryValue, string sFilter)
		{
			// Works!
			string[] aDirectories = Directory.GetDirectories(directoryValue);
			string[] aFiles = Directory.GetFiles(directoryValue, sFilter);

			if (aFiles.Length != 0)
			{
				foreach (string sFile in aFiles)
				{
					this.mFileNames.Add((string)sFile);
				}
			}

			if (aDirectories.Length != 0)
			{
				foreach (string sDirectory in aDirectories)
				{
					//foreach (string sFile in Directory.GetFiles(sDirectory))
					//{
					//	this.mFileNames.Add((string)sFile);
					//}

					this.PopulateFiles(sDirectory);
				}
			}
		}

		/// <summary>
		/// Zips the ZipFile.SourceDirectory files in ZipFile.ZipFileFullPath.
		/// </summary>
		/// <returns>Zip successfull.</returns>
		public bool ZipDirectory()
		{
			bool lReturnValue;
			string[] aFilenames;
			//string[]		aDirectories;
			Crc32 oCrc;
			FileStream oInputFileStream;
			byte[] aBuffer;
			ZipEntry oCurrentEntry;
			string sSourceDirectoryTemp;

			// Check if there is a source directory.
			if (this.mSourceDirectory.Length > 0)
			{
				// Get the files to zip
				aFilenames = this.GetFileNames();

				if (this.mUseTempDirectory)
				{
					// Set the path for a temp directory, we need to have full access to the file
					sSourceDirectoryTemp = Application.StartupPath + @"\tempzip\";

					// Check if the temp dir exits
					if (Directory.Exists(sSourceDirectoryTemp))
					{
						// Delete the old dir
						Directory.Delete(sSourceDirectoryTemp, true);
					}
					// Create a new temp dir
					Directory.CreateDirectory(sSourceDirectoryTemp);

					// TODO: progress
					//// Check if there is a progress bar
					//if (this.mProgressBar != null)
					//{
					//    // Init the progressbar
					//    this.mProgressBar.Value = 0;
					//    this.mProgressBar.Maximum = aFilenames.Length;
					//    // Step is 1 we wil progress per file
					//    this.mProgressBar.Step = 1;
					//    this.mProgressBar.Parent.Refresh();
					//}

					// Copy all the files
					foreach (string sCurrentFile in aFilenames)
					{
						// TODO: progress
						//if (this.mProgressBar != null)
						//{
						//    // Update the progress bar.
						//    this.mProgressBar.PerformStep();
						//}

						File.Copy(sCurrentFile, sSourceDirectoryTemp + Path.GetFileName(sCurrentFile));
					}

					// Creat a new list of file, but now from the temp directory
					aFilenames = Directory.GetFiles(sSourceDirectoryTemp);

					Application.DoEvents();
				}

				// Create a crc object.
				oCrc = new Crc32();

				this.CreateZipFile();

				// TODO: progress
				//if (this.mProgressBar != null)
				//{
				//    // Init the progressbar
				//    this.mProgressBar.Value = 0;
				//    this.mProgressBar.Maximum = aFilenames.Length;
				//    // Step is 1 we wil progress per file
				//    this.mProgressBar.Step = 1;
				//    this.mProgressBar.Parent.Refresh();
				//}

				try
				{
					// Put all the files in the zipfile.
					foreach (string sCurrentFile in aFilenames)
					{
						// TODO: progress
						//if (this.mProgressBar != null)
						//{
						//    // Update the progress bar.
						//    this.mProgressBar.PerformStep();
						//}

						// Open the first file for read with a inputstream
						oInputFileStream = File.OpenRead(sCurrentFile);

						// Create a buffer and read the data from the file.
						aBuffer = new byte[oInputFileStream.Length];
						oInputFileStream.Read(aBuffer, 0, aBuffer.Length);

						// Create a new entry for the zip file with the name of the source file.
						oCurrentEntry = new ZipEntry(this.GetEntryName(sCurrentFile));

						// Set the current time stamp
						oCurrentEntry.DateTime = DateTime.Now;

						// set Size and the crc, because the information
						// about the size and crc should be stored in the header
						// if it is not set it is automatically written in the footer.
						// (in this case size == crc == -1 in the header)
						// Some ZIP programs have problems with zip files that don't store
						// the size and crc in the header.
						oCurrentEntry.Size = oInputFileStream.Length;

						// Close the source file.
						oInputFileStream.Close();

						// Reset the crc and set the file size in the crc.
						oCrc.Reset();
						oCrc.Update(aBuffer);

						// Set the crc on the entry
						oCurrentEntry.Crc = oCrc.Value;

						// Add the entyr tot te zip file.
						this.mZipOutputStream.PutNextEntry(oCurrentEntry);

						// And write the entry to the zip file.
						this.mZipOutputStream.Write(aBuffer, 0, aBuffer.Length);

						Application.DoEvents();
					}

					// Finish and close the zip file.
					this.mZipOutputStream.Finish();
					this.mZipOutputStream.Close();

					lReturnValue = true;
				}
				catch
				{
					MessageBox.Show("Fout bij het inpakken van de bestanden!\r\n\r\nWeet u zeker dat de bestanden die u probeert in te pakken niet in gebruik zijn?", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
					lReturnValue = false;
				}
			}
			else
			{
				lReturnValue = false;
			}

			if (ZipDone != null)
			{
				ZipDone(this, null);
			}

			return lReturnValue;
		}
		// End of ZipDirectory

		/// <summary>
		/// Zips the ZipFile.SourceFile in ZipFile.ZipFileFullPath.
		/// </summary>
		/// <returns>Zip successfull.</returns>
		public bool Zip()
		{
			bool lReturnValue;
			FileStream oStreamToZip;
			ZipEntry oZipEntry;
			byte[] aBuffer;
			int nSize;
			int nSizeRead;

			// Check if the source file exists
			if (File.Exists(this.mSourceFile))
			{
				// Create a input stream for reading the file.
				oStreamToZip = new FileStream(this.mSourceFile, FileMode.Open, FileAccess.Read);

				// Create the output zip file.
				this.CreateZipFile();

				// Create the zip entry for the file.
				oZipEntry = new ZipEntry(this.GetEntryName(this.mSourceFile));
				this.mZipOutputStream.PutNextEntry(oZipEntry);

				aBuffer = new byte[2048];
				nSize = oStreamToZip.Read(aBuffer, 0, aBuffer.Length);
				this.mZipOutputStream.Write(aBuffer, 0, nSize);

				// TODO: progress
				//if (this.mProgressBar != null)
				//{
				//    this.mProgressBar.Minimum = 0;
				//    this.mProgressBar.Maximum = 100;
				//}

				long nTotalSize = oStreamToZip.Length;
				long nBytes = 0;

				while (nSize < oStreamToZip.Length)
				{
					nSizeRead = oStreamToZip.Read(aBuffer, 0, aBuffer.Length);
					this.mZipOutputStream.Write(aBuffer, 0, nSizeRead);
					nSize += nSizeRead;
					nBytes += nSizeRead;

					float fPercentage = ((float)nBytes / (float)nTotalSize) * 100;
					int nPercentage = (int)fPercentage;

					// TODO: progress
					//if (this.mProgressBar != null)
					//{
					//    this.mProgressBar.Value = nPercentage;
					//}
				}

				//this.mProgressBar.Value = 100;
				this.mZipOutputStream.Finish();
				this.mZipOutputStream.Close();
				oStreamToZip.Close();

				Application.DoEvents();

				lReturnValue = true;
			}
			else
			{
				lReturnValue = false;
			}

			if (ZipDone != null)
			{
				ZipDone(this, null);
			}

			return lReturnValue;
		}
		// End of ZipFile

		/// <summary>
		/// Zips the ZipFile.SourceFiles files in ZipFile.ZipFileFullPath.
		/// </summary>
		/// <returns>Zip successfull.</returns>
		public bool ZipFiles()
		{
			bool lReturnValue;
			string[] aFilenames;
			Crc32 oCrc;
			FileStream oInputFileStream;
			byte[] aBuffer;
			ZipEntry oCurrentEntry;

			// Check if there is a source directory.
			if (this.mSourceFiles.Length > 0)
			{
				// Get the files to zip
				aFilenames = this.mSourceFiles;

				// Create a crc object.
				oCrc = new Crc32();

				this.CreateZipFile();

				// TODO: progress
				//if (this.mProgressBar != null)
				//{
				//    // Init the progressbar
				//    this.mProgressBar.Value = 0;
				//    this.mProgressBar.Maximum = aFilenames.Length;
				//    // Step is 1 we wil progress per file
				//    this.mProgressBar.Step = 1;
				//    this.mProgressBar.Parent.Refresh();
				//}

				try
				{
					// Put all the files in the zipfile.
					foreach (string sCurrentFile in aFilenames)
					{
						// TODO: progress
						//if (this.mProgressBar != null)
						//{
						//    // Update the progress bar.
						//    this.mProgressBar.PerformStep();
						//}

						// Open the first file for read with a inputstream
						oInputFileStream = File.OpenRead(sCurrentFile);

						// Create a buffer and read the data from the file.
						aBuffer = new byte[oInputFileStream.Length];
						oInputFileStream.Read(aBuffer, 0, aBuffer.Length);

						// Create a new entry for the zip file with the name of the source file.
						oCurrentEntry = new ZipEntry(this.GetEntryName(sCurrentFile));

						// Set the current time stamp
						oCurrentEntry.DateTime = DateTime.Now;

						// set Size and the crc, because the information
						// about the size and crc should be stored in the header
						// if it is not set it is automatically written in the footer.
						// (in this case size == crc == -1 in the header)
						// Some ZIP programs have problems with zip files that don't store
						// the size and crc in the header.
						oCurrentEntry.Size = oInputFileStream.Length;

						// Close the source file.
						oInputFileStream.Close();

						// Reset the crc and set the file size in the crc.
						oCrc.Reset();
						oCrc.Update(aBuffer);

						// Set the crc on the entry
						oCurrentEntry.Crc = oCrc.Value;

						// Add the entyr tot te zip file.
						this.mZipOutputStream.PutNextEntry(oCurrentEntry);

						// And write the entry to the zip file.
						this.mZipOutputStream.Write(aBuffer, 0, aBuffer.Length);

						Application.DoEvents();
					}

					// Finish and close the zip file.
					this.mZipOutputStream.Finish();
					this.mZipOutputStream.Close();

					lReturnValue = true;
				}
				catch
				{
					MessageBox.Show("Fout bij het inpakken van de bestanden!\r\n\r\nWeet u zeker dat de bestanden die u probeert in te pakken niet in gebruik zijn?", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
					lReturnValue = false;
				}
			}
			else
			{
				lReturnValue = false;
			}

			if (ZipDone != null)
			{
				ZipDone(this, null);
			}

			return lReturnValue;
		}
		// End of ZipFiles



		private string[] MergArrayOfString(string[] ArrayA, string[] ArrayB)
		{
			StringCollection MyCollection = new StringCollection();
			MyCollection.AddRange(ArrayA);
			MyCollection.AddRange(ArrayB);

			if (MyCollection.Count > 0)
			{
				string[] c = new string[MyCollection.Count];
				MyCollection.CopyTo(c, 0);
				return c;
			}
			else
			{
				return null;
			}
		}
		// End Of MergArrayOfString

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets Extract directory that overrides the entry directory.
		/// </summary>
		public string ExtractDirOverride
		{
			get
			{
				return this.mExtractDirOverride;
			}
			set
			{
				this.mExtractDirOverride = value;
			}
		}

		/// <summary>
		/// Gets or sets the remove source directory path from the zip entry.
		/// </summary>
		public bool RemoveSourceDirectoryPath
		{
			get
			{
				return this.mRemoveSourceDirectoryPath;
			}
			set
			{
				this.mRemoveSourceDirectoryPath = value;
			}
		}

		/// <summary>
		/// Gets or sets the remove source directory name from the zip entry.
		/// </summary>
		public bool RemoveSourceDirectoryName
		{
			get
			{
				return this.mRemoveSourceDirectoryName;
			}
			set
			{
				this.mRemoveSourceDirectoryName = value;
			}
		}

		/// <summary>
		/// Gets or sets the full path loction of the zip file.
		/// </summary>
		public string ZipFileFullPath
		{
			get
			{
				return this.mZipFilePath;
			}
			set
			{
				this.mZipFilePath = value;
			}
		}

		/// <summary>
		/// Gets or sets the source directory for the zip file.
		/// </summary>
		public string SourceDirectory
		{
			get
			{
				return this.mSourceDirectory;
			}
			set
			{
				//this.mSourceDirectory = Path.GetDirectoryName(value);
				if (!value.EndsWith(@"\"))
				{
					this.mSourceDirectory = value + @"\";
				}
				else
				{
					this.mSourceDirectory = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the source file for the zip file.
		/// </summary>
		public string SourceFile
		{
			get
			{
				return this.mSourceFile;
			}
			set
			{
				this.mSourceFile = value;
			}
		}

		/// <summary>
		/// Gets or sets the source file for the zip file.
		/// </summary>
		public string[] SourceFiles
		{
			get
			{
				return this.mSourceFiles;
			}
			set
			{
				this.mSourceFiles = value;
			}
		}

		/// <summary>
		/// Allow override of the destination zipfile.
		/// </summary>
		public bool OverrideZipfile
		{
			get
			{
				return this.mOverrideZipFile;
			}
			set
			{
				this.mOverrideZipFile = value;
			}

		}

		/// <summary>
		/// Gets or sets the compression level of the zip file.
		/// 1 to 9 -> 1 is only pack, 9 is best compression.
		/// default = 9
		/// </summary>
		public int CompressionLevel
		{
			get
			{
				return this.mCompressionLevel;
			}
			set
			{
				this.mCompressionLevel = value;
			}

		}

		/// <summary>
		/// A array of filter strings:
		/// FileFilters[0] = "*.dbf"
		/// FileFilters[1] = "*.fpt"
		/// </summary>
		public string[] FileFilters
		{
			get
			{
				return this.mFileFilters;
			}
			set
			{
				this.mFileFilters = value;
			}
		}

		/// <summary>
		/// set the file filter standard windows filters like: *.dbf
		/// </summary>
		public string FileFilter
		{
			get
			{
				return this.mFileFilter;
			}
			set
			{
				this.mFileFilter = value;
			}
		}

		/// <summary>
		/// Use a temp directory for zipping a directory or multiple files
		/// [applicationdir]\tempzip\ will be created
		/// </summary>
		public bool UseTempDirectory
		{
			get
			{
				return this.mUseTempDirectory;
			}
			set
			{
				this.mUseTempDirectory = value;
			}
		}

		/// <summary>
		/// When a folder is being zipped include the subfolders or not
		/// </summary>
		public bool IncludeSubFolders
		{
			get
			{
				return this.mIncludeSubFolders;
			}
			set
			{
				this.mIncludeSubFolders = value;
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Event which is being fired when zipping is done
		/// </summary>
		public event System.EventHandler ZipDone;

		#endregion
	}
}

