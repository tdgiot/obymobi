﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ionic.Zip;
using System.IO;

namespace Dionysos
{
    public static class ZipUtil2
    {
        public static void ZipDirectory(string inputPath, Stream outputStream, 
            Ionic.Zlib.CompressionLevel compressionLevel = Ionic.Zlib.CompressionLevel.Default, 
            CompressionMethod compressionMethod = CompressionMethod.Deflate)
        {
            using (ZipFile zip = new ZipFile())
            {
                zip.CompressionLevel = compressionLevel;
                zip.CompressionMethod = compressionMethod;
                zip.AddDirectory(inputPath, string.Empty);
                zip.Save(outputStream);
            }            
        }

        public static void Unzip(Stream inputStream, string outputPath)
        {
            using (ZipFile zip = ZipFile.Read(inputStream))
            {                
                zip.ExtractAll(outputPath);
            }
        }
    }
}
