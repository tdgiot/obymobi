﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Dionysos.Text.RegularExpressions
{
    /// <summary>
    /// Collection of common patterns
    /// </summary>
    public class CommonRegExPatterns
    {
        /// <summary>
        /// Regex Pattern for E-mailaddresses
        /// </summary>        
        public const string EmailPattern = @"\w+([-+.']\w+)*[.]*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

        // GK Old: \w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*
        // GK New: \w+([-+.']\w+)*[.]*@\w+([-.]\w+)*\.\w+([-.]\w+)* (to support gabriel.@amteam.nl

        /// <summary>
        /// Regex Pattern for E-mailaddresses
        /// </summary>
        public const string IntegerPattern = @"[0-9]*";

        /// <summary>
        /// Regex Pattern for Url's
        /// </summary>
        public const string UrlPattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

        /// <summary>
        /// Regex Pattern for Url's allows much more, but used for correcting urls.
        /// </summary>
        public const string AdvancedUrlPattern = @"^((?<scheme>[A-Za-z0-9_+.]{1,8}):)?(//)?" + // Old URL match: ;
                                          @"((?<userinfo>[!-~]+)@)?" +
                                          @"(?<host>[^/?#:]*)(:(?<port>[0-9]*))?" +
                                          @"(/(?<path>[^?#]*))?" +
                                          @"(\?(?<query>[^#]*))?" +
                                          @"(#(?<fragment>.*))?";

        /// <summary>
        /// Regex Pattern for Time in the hh:mm format (00:00 till 23:59)
        /// </summary>
        public const string Time24h = "^([0-1][0-9]|[2][0-3]):([0-5][0-9])$";

    }
}
