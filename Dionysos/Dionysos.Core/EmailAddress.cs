using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Dionysos.Text.RegularExpressions;

namespace Dionysos
{
    /// <summary>
    /// Class which represents an email address
    /// </summary>
    public class EmailAddress
    {
        #region Fields

        private static EmailAddress instance = null;
        private string value = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.EmailAddress type
        /// </summary>
        public EmailAddress()
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx-media.EmailAddress type
        /// using the specified email address
        /// </summary>
        public EmailAddress(string emailAddress)
        {
            this.value = emailAddress;
        }

        /// <summary>
        /// Constructs a static instance of the Lynx-media.EmailAddress type
        /// </summary>
        static EmailAddress()
        {
            if (instance == null)
            {
                instance = new EmailAddress();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Converts a string representation to a Dionysos.EmailAddress instance
        /// </summary>
        /// <param name="s">The System.String instance to convert to a Dionysos.EmailAddress instance</param>
        /// <param name="emailAddress">The Dionysos.EmailAddress instance to set</param>
        /// <returns>True if conversion was successful, False if not</returns>
        public static bool TryParse(string s, out EmailAddress emailAddress)
        {
            bool success = true;

            emailAddress = new EmailAddress();
            Regex emailExpression = new Regex(CommonRegExPatterns.EmailPattern);
            if (emailExpression.IsMatch(s))
            {
                emailAddress.Value = s;
            }
            else
            {
                success = false;
            }

            return success;
        }



        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the email address
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        #endregion
    }
}
