﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Collections;

namespace Dionysos
{
	/// <summary>
	/// Utility class for working with Status
	/// </summary>
	public class StatusUtil
	{
		/// <summary>
		/// Retrieve the Dutch names and integer values of the Status to be used for example for a DropDownList
		/// </summary>
		public static OrderedDictionary<string, int> GetStatusAsDictionary()
		{
			OrderedDictionary<string, int> toReturn = new OrderedDictionary<string, int>();

			toReturn.Add("Niet gestart", (int)Status.NotStarted);
			toReturn.Add("Wordt uitgevoerd", (int)Status.InProgress);
			toReturn.Add("Voltooid", (int)Status.Completed);
			toReturn.Add("Wacht op input", (int)Status.WaitingForOther);
			toReturn.Add("Uitgesteld", (int)Status.Postponed);

			return toReturn;
		}

		/// <summary>
		/// Retrieve the textual representation of the status value
		/// </summary>
		/// <param name="status">Priority value</param>
		/// <returns></returns>
		public static string GetTextValue(int status)
		{
			if (status == (int)Status.Completed)
				return "Afgerond";
			else if (status == (int)Status.InProgress)
				return "In behandeling";
			else if (status == (int)Status.NotStarted)
				return "Niet gestart";
			else if (status == (int)Status.Postponed)
				return "Uitgesteld";
			else if (status == (int)Status.WaitingForOther)
				return "Wachten op ander";
			else
				return "Onbekend";
		}
	}
}
