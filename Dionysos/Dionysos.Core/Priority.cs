﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Priority
	/// </summary>
	public enum Priority : int
	{
		/// <summary>
		/// Low
		/// </summary>
		Low = 100,
		/// <summary>
		/// Normal
		/// </summary>
		Normal = 500,
		/// <summary>
		/// High
		/// </summary>
		High = 1000
	}
}
