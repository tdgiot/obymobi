﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Range of a Recurrence pattern
	/// </summary>
	public enum RecurrenceRange : int
	{
		/// <summary>
		/// A recurring appointment will end after the date specified 
		/// </summary>
		EndByDate = 100,
		/// <summary>
		/// A recurring appointment will not have an end date. 
		/// </summary>
		NoEndDate = 200,
		/// <summary>
		/// A recurring appointment will end after its recurrence count exceeds the value specified by the RecurrenceInfo.OccurrenceCount property. 
		/// </summary>
		OccurenceCount = 300
	}
}
