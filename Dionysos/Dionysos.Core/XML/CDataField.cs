﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;

namespace Dionysos.XML
{
	/// <summary>
	/// Serializable CDATA field
	/// </summary>
	public class CDataField : IXmlSerializable
	{
		private string elementName;
		private string elementValue;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="elementName"></param>
		/// <param name="elementValue"></param>
		public CDataField(string elementName, string elementValue)
		{
			this.elementName = elementName;
			this.elementValue = elementValue;
		}

		/// <summary>
		/// Get Schema (null)
		/// </summary>
		/// <returns></returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		/// <summary>
		/// Write the XML
		/// </summary>
		/// <param name="w"></param>
		public void WriteXml(XmlWriter w)
		{
			w.WriteStartElement(this.elementName);
			w.WriteCData(this.elementValue);
			w.WriteEndElement();
		}

		/// <summary>
		/// Read the XML (not implemented)
		/// </summary>
		/// <param name="r"></param>
		public void ReadXml(XmlReader r)
		{
			throw new NotImplementedException("This method has not been implemented");
		}
	}

}
