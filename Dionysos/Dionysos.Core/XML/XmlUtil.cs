using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml;
using System.IO;

namespace Dionysos
{
    /// <summary>
    /// Single utility class used for reading/writing Xml documents
    /// </summary>
    public class XmlUtil
    {
        #region Fields

        static XmlUtil instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the XmlUtil class if the instance has not been initialized yet
        /// </summary>
        public XmlUtil()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new XmlUtil();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
        }

        /// <summary>
        /// Creates an XmlNode instance using the specified name
        /// </summary>
        /// <param name="xmlDoc">The XmlDocument instance to create the XmlNode for</param>
        /// <param name="name">The name of the XmlNode instance</param>
        /// <returns>An XmlNode instance</returns>
        public static XmlNode CreateXmlNode(ref XmlDocument xmlDoc, string name)
        {
            XmlNode node = null;
            
            if (Instance.ArgumentIsEmpty(xmlDoc, "xmlDoc"))
            {
                // Argument 'xmldoc' is empty
            }
            else if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Argument 'name' is empty
            }
            else
            {
                node = xmlDoc.CreateNode(XmlNodeType.Element, name, string.Empty);
            }

            return node;
        }

        /// <summary>
        /// Creates an XmlNode instance using the specified name and value
        /// </summary>
        /// <param name="xmlDoc">The XmlDocument instance to create the XmlNode for</param>
        /// <param name="name">The name of the XmlNode instance</param>
        /// <param name="value">The value of the XmlNode instance</param>
        /// <returns>An XmlNode instance</returns>
        public static XmlNode CreateXmlNode(ref XmlDocument xmlDoc, string name, object value)
        {
            XmlNode node = null;

            if (Instance.ArgumentIsEmpty(xmlDoc, "xmlDoc"))
            {
                // Argument 'xmldoc' is empty
            }
            else if (Instance.ArgumentIsEmpty(name, "name"))
            {
                // Argument 'name' is empty
            }
            else if (Instance.ArgumentIsEmpty(value, "value"))
            {
                // Argument 'value' is empty
            }
            else
            {
                node = xmlDoc.CreateNode(XmlNodeType.Element, name, string.Empty);
                node.InnerText = value.ToString();
            }

            return node;
        }

        /// <summary>
        /// Creates an XmlWriter instance using the specified file name
        /// </summary>
        /// <param name="fileName">The name of the file to create the XmlWriter instance for</param>
        /// <returns>An XmlWriter instance</returns>
        public static XmlWriter CreateXmlWriter(string fileName)
        {
            XmlWriter xmlWriter = null;
            XmlWriterSettings writerSettings = new XmlWriterSettings();

            writerSettings.Indent = true;
            writerSettings.IndentChars = "\t";
            writerSettings.OmitXmlDeclaration = true;

            //if (!File.Exists(fileName))
            //{
                xmlWriter = XmlWriter.Create(fileName, writerSettings);
            //}

            return xmlWriter;
        }

        #endregion
    }
}