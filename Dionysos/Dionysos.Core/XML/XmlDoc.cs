﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace Dionysos.XML
{
	/// <summary>
	/// XML document.
	/// </summary>
	public class XmlDoc : Dionysos.XML.XmlNode
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="XmlDoc"/> class.
		/// </summary>
		public XmlDoc()
		{
		}

		#endregion

		#region Methods

		/// <summary>
		/// Parses the XML.
		/// </summary>
		/// <param name="xml">The XML.</param>
		public void ParseXml(string xml)
		{
			try
			{
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.Load(new StringReader(xml));

				this.Name = xmlDocument.DocumentElement.Name;
				this.InnerXml = xmlDocument.DocumentElement.InnerXml;

				if (xmlDocument.DocumentElement.HasChildNodes)
					this.ParseChildNodes(this, xmlDocument.DocumentElement.ChildNodes);
				else
					this.Value = xmlDocument.DocumentElement.InnerText;
			}
			catch
			{
			}
		}

		/// <summary>
		/// Adds the specified XML node as the root node
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		public new XmlNode Add(XmlNode xmlNode)
		{
			this.Name = xmlNode.Name;
			this.Value = xmlNode.Value;
			this.InnerXml = xmlNode.InnerXml;
			this.ChildNodes = xmlNode.ChildNodes;

			return this;
		}

		/// <summary>
		/// Adds the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		public new XmlNode Add(string name)
		{
			this.Name = name;

			return this;
		}

		/// <summary>
		/// Adds the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		public new XmlNode Add(string name, string value)
		{
			this.Name = name;
			this.Value = value;

			return this;
		}

		/// <summary>
		/// Adds the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		public new XmlNode Add(string name, object value)
		{
			this.Name = name;
			this.Value = value.ToString();

			return this;
		}

		/// <summary>
		/// Parses the child nodes.
		/// </summary>
		/// <param name="parent">The parent.</param>
		/// <param name="childNodes">The child nodes.</param>
		private void ParseChildNodes(XmlNode parent, XmlNodeList childNodes)
		{
			foreach (System.Xml.XmlNode childNode in childNodes)
			{
				if (childNode is XmlElement)
				{
					Dionysos.XML.XmlNode node = new XmlNode();
					node.Name = childNode.Name;

					node.InnerXml = childNode.InnerXml;

					if (childNode.HasChildNodes)
						this.ParseChildNodes(node, childNode.ChildNodes);
					else
						node.Value = childNode.InnerText;

					// Attributes
					node.Attributes = childNode.Attributes;

					parent.ChildNodes.Add(node);
				}
				else if (childNode is XmlText)
				{
					parent.Value = childNode.InnerText;
				}
			}
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return this.WriteXml(this);
		}

		/// <summary>
		/// Writes the XML.
		/// </summary>
		/// <param name="node">The node.</param>
		/// <returns></returns>
		private string WriteXml(XmlNode node)
		{
			return WriteXml(node, 0);
		}

		/// <summary>
		/// Writes the XML.
		/// </summary>
		/// <param name="node">The node.</param>
		/// <param name="level">The level.</param>
		/// <returns></returns>
		private string WriteXml(XmlNode node, int level)
		{
			string xml = string.Empty;

			xml += string.Format("<{0}>", node.Name).AddTabs(level);

			if (node.ChildNodes.Count > 0)
			{
				xml += Environment.NewLine;
				level++;

				foreach (XmlNode childNode in node.ChildNodes)
				{
					xml += WriteXml(childNode, level) + Environment.NewLine;
				}

				level--;
			}
			else if (node.Value.Length > 0)
				xml += node.Value;

			if (node.ChildNodes.Count > 0)
				xml += string.Format("</{0}>", node.Name).AddTabs(level);
			else
				xml += string.Format("</{0}>", node.Name);

			return xml;
		}

		#endregion
	}
}
