﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Globalization;

namespace Dionysos.XML
{
	/// <summary>
	/// XML node.
	/// </summary>
	public class XmlNode
	{
		#region Fields

		private string name = string.Empty;
		private string value = string.Empty;
		private string innerXml = string.Empty;
		private List<XmlNode> childNodes = new List<XmlNode>();
		private XmlAttributeCollection attributes = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="XmlNode"/> class.
		/// </summary>
		public XmlNode()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="XmlNode"/> class.
		/// </summary>
		/// <param name="name">The name.</param>
		public XmlNode(string name)
		{
			this.name = name;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="XmlNode"/> class.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		public XmlNode(string name, string value)
		{
			this.name = name;
			this.value = value;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds the specified XML node.
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		/// <returns></returns>
		public XmlNode Add(XmlNode xmlNode)
		{
			this.childNodes.Add(xmlNode);
			return xmlNode;
		}

		/// <summary>
		/// Adds the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public XmlNode Add(string name)
		{
			XmlNode xmlNode = new XmlNode(name);
			this.Add(xmlNode);
			return xmlNode;
		}

		/// <summary>
		/// Adds the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public XmlNode Add(string name, string value)
		{
			XmlNode xmlNode = new XmlNode(name, value);
			this.Add(xmlNode);
			return xmlNode;
		}

		/// <summary>
		/// Adds the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public XmlNode Add(string name, object value)
		{
			XmlNode xmlNode = new XmlNode(name, value.ToString());
			this.Add(xmlNode);
			return xmlNode;
		}

		/// <summary>
		/// Inserts the specified index.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="xmlNode">The XML node.</param>
		public void Insert(int index, XmlNode xmlNode)
		{
			this.childNodes.Insert(index, xmlNode);
		}

		/// <summary>
		/// Gets the name of the child nodes by.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public List<XmlNode> GetChildNodesByName(string name)
		{
			List<XmlNode> childNodes = new List<XmlNode>();

			foreach (var childNode in this.childNodes)
			{
				if (childNode.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
					childNodes.Add(childNode);
			}

			return childNodes;
		}

		/// <summary>
		/// Gets the name of the single child node by.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public XmlNode GetSingleChildNodeByName(string name)
		{
			XmlNode node = null;

			List<XmlNode> childNodes = this.GetChildNodesByName(name);
			if (childNodes.Count == 1)
				node = childNodes[0];

			return node;
		}

		/// <summary>
		/// Tries to get the value from the node.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
		/// <returns>true if the key and corresponding value exists in the QueryString; otherwise false.</returns>
		public bool TryGetValue<T>(out T value)
		{
			return TryGetValue<T>(out value, CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Tries to get the value from the node
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>true if the key and corresponding value exists in the QueryString; otherwise false.</returns>
		public bool TryGetValue<T>(out T value, IFormatProvider provider)
		{
			if (!this.value.IsNullOrWhiteSpace())
			{
				// Value is found, try to change the type
				try
				{
					value = (T)Convert.ChangeType(this.value, typeof(T), provider);
					return true;
				}
				catch
				{
					// Type could not be changed
				}
			}

			// Value is not found, return default
			value = default(T);
			return false;
		}

		/// <summary>
		/// Gets the value from the node.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <returns>
		/// Returns the value, if found and type matches; otherwise returns null.
		/// </returns>
		public T? GetValue<T>()
			where T : struct
		{
			return GetValue<T>(CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Gets the value from the node.
		/// </summary>
		/// <typeparam name="T">The type of the value to get.</typeparam>
		/// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
		/// <returns>
		/// Returns the value, if found and type matches; otherwise returns null.
		/// </returns>
		public T? GetValue<T>(IFormatProvider provider)
			where T : struct
		{
			T value;
			if (TryGetValue(out value, provider))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public string Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		/// <summary>
		/// Gets or sets the inner xml.
		/// </summary>
		/// <value>The inner xml.</value>
		public string InnerXml
		{
			get
			{
				return this.innerXml;
			}
			set
			{
				this.innerXml = value;
			}
		}

		/// <summary>
		/// Gets or sets the child nodes.
		/// </summary>
		/// <value>The child nodes.</value>
		public List<XmlNode> ChildNodes
		{
			get
			{
				return this.childNodes;
			}
			set
			{
				this.childNodes = value;
			}
		}

		/// <summary>
		/// Gets the XmlNodes with the specified name.
		/// </summary>
		public List<XmlNode> this[string name]
		{
			get
			{
				return this.GetChildNodesByName(name);
			}
		}

		/// <summary>
		/// Gets the <see cref="Dionysos.XML.XmlNode"/> at the specified index.
		/// </summary>
		/// <value></value>
		public XmlNode this[int index]
		{
			get
			{
				XmlNode xmlNode = null;
				if (index >= (this.childNodes.Count - 1))
					xmlNode = this.childNodes[index];
				return xmlNode;
			}
		}

		/// <summary>
		/// Gets or sets the attributes.
		/// </summary>
		/// <value>The attributes.</value>
		public XmlAttributeCollection Attributes
		{
			get
			{
				return this.attributes;
			}
			set
			{
				this.attributes = value;
			}
		}

		#endregion
	}
}
