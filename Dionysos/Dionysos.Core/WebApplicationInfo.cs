using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;

namespace Dionysos.Web
{
    /// <summary>
    /// Class which contains information about a webapplication
    /// </summary>
    public class WebApplicationInfo : IApplicationInfo
    {
        #region Fields

        private string applicationName = string.Empty;
        private string applicationVersion = string.Empty;
        private string baseUrl = string.Empty;
        private string basePath = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Web.WebApplicationInfo type
        /// </summary>
        public WebApplicationInfo()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the application
        /// </summary>
        public string ApplicationName
        {
            get
            {
                if (Instance.Empty(this.applicationName))
                {
                    throw new ApplicationNameNotSetException("Application name not set in Global.asax! Check the initialization of the Global.ApplicationInfo.");
                }
                return this.applicationName;
            }
            set
            {
                this.applicationName = value;
            }
        }

        /// <summary>
        /// Gets or sets the version of the application
        /// </summary>
        public string ApplicationVersion
        {
            get
            {
                return this.applicationVersion;
            }
            set
            {
                this.applicationVersion = value;
            }
        }

        /// <summary>
        /// Gets or sets the base url for this webapplication
        /// </summary>
        public string BaseUrl
        {
            get
            {
                return this.baseUrl;
            }
            set
            {
                this.baseUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the base path for this webapplication
        /// </summary>
        public string BasePath
        {
            get
            {
                return this.basePath;
            }
            set
            {
                this.basePath = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a generic string instance containing information about a webapplication
        /// </summary>
        /// <returns>A string instance containg webappllication information</returns>
        public new string ToString()
        {
            return string.Empty;
        }

        /// <summary>
        /// Converts the content of this WebApplicationInfo instance to XML
        /// </summary>
        public void ToXML()
        {
        }

        #endregion

        #region Events



        #endregion
    }
}
