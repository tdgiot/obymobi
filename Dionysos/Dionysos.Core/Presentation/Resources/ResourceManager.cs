using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Presentation.Logic.Resources
{
    /// <summary>
    /// Single utility class used for retrieving (embedded) resources
    /// </summary>
    public class ResourceManager
    {
        #region Fields

        static ResourceManager instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ResourceManager class if the instance has not been initialized yet
        /// </summary>
        public ResourceManager()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new ResourceManager();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
        }

        #endregion
    }
}