using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Presentation
{
    /// <summary>
    /// Singleton utility class used for data columns
    /// </summary>
    public class DataColumnUtil
    {
        #region Fields

        static DataColumnUtil instance = null;

        private DataColumnCollectionPool dataColumnCollectionPool = null;
        private const string configDirName = "Config";
        private const string configFileName = "datacolumns.xml";

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the DataColumnUtil class if the instance has not been initialized yet
        /// </summary>
        static DataColumnUtil()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new DataColumnUtil();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
            this.dataColumnCollectionPool = new DataColumnCollectionPool();
        }

        /// <summary>
        /// Gets the datacolumn collection from the datacolumn collection pool
        /// using the specified entity name
        /// </summary>
        /// <param name="entityName">The name of the entity to get the datacolumn collection for</param>
        public static DataColumnCollection GetDataColumnCollection(string entityName)
        {
            // Create a DataColumnCollection instance as return value
            DataColumnCollection dataColumnCollection = null;

            // Create the path to the datacolumns configuration file
            string configPath = System.IO.Path.Combine(Global.ApplicationInfo.BasePath, configDirName);
            if (!Directory.Exists(configPath))
            {
                Directory.CreateDirectory(configPath);
            }
            configPath = System.IO.Path.Combine(configPath, configFileName);

            // 1. Get the datacolumn collection from the pool
            dataColumnCollection = instance.dataColumnCollectionPool[entityName];

            // 2. If not available in the pool, read the collection from disk
            if (Instance.Empty(dataColumnCollection))
            {
                // Check whether the file exists on disk
                if (File.Exists(configPath))
                {
                    instance.dataColumnCollectionPool.ReadXml(configPath);
                    dataColumnCollection = instance.dataColumnCollectionPool[entityName];
                }
            }

            // 3. No data column collection is found in the pool or on disk
            // So create a new one using the specified entity name
            if (Instance.Empty(dataColumnCollection))
            {
                // Initialize the data column collection
                dataColumnCollection = new DataColumnCollection();
                dataColumnCollection.EntityName = entityName;
                dataColumnCollection.Initialize();

                // Finally, add the data column collection to the pool
                instance.dataColumnCollectionPool.Add(dataColumnCollection);
                instance.dataColumnCollectionPool.WriteXml(configPath);
            }

            return dataColumnCollection;
        }

        /// <summary>
        /// Gets the datacolumn collection from the datacolumn collection pool
        /// using the specified entity name
        /// </summary>
        /// <param name="entityName">The name of the entity to get the datacolumn collection for</param>
        /// <param name="userName">The name of the user to get the datacolumn collection for</param>
        public static DataColumnCollection GetDataColumnCollection(string entityName, string userName)
        {
            // Create a DataColumnCollection instance as return value
            DataColumnCollection dataColumnCollection = null;

            // Create the path to the datacolumns configuration file
            string configPath = System.IO.Path.Combine(Global.ApplicationInfo.BasePath, configDirName);
            if (!Directory.Exists(configPath))
            {
                Directory.CreateDirectory(configPath);
            }
            configPath = System.IO.Path.Combine(configPath, configFileName);
            
            // 1. Get the datacolumn collection from the pool
            dataColumnCollection = instance.dataColumnCollectionPool[entityName, userName];

            // 2. If not available in the pool, read the collection from disk
            if (Instance.Empty(dataColumnCollection))
            {
                // Check whether the file exists on disk
                if (File.Exists(configPath))
                {
                    instance.dataColumnCollectionPool.ReadXml(configPath);
                    dataColumnCollection = instance.dataColumnCollectionPool[entityName, userName];
                }
            }

            // 3. No data column collection is found in the pool or on disk
            // So create a new one using the specified entity name
            if (Instance.Empty(dataColumnCollection))
            {
                // Initialize the data column collection
                dataColumnCollection = new DataColumnCollection();
                dataColumnCollection.EntityName = entityName;
                if (!Instance.Empty(userName))
                {
                    dataColumnCollection.UserName = userName;
                }

                dataColumnCollection.Initialize();

                // Finally, add the data column collection to the pool
                instance.dataColumnCollectionPool.Add(dataColumnCollection);
                instance.dataColumnCollectionPool.WriteXml(configPath);
            }

            return dataColumnCollection;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the data column collection pool
        /// </summary>
        public static DataColumnCollectionPool DataColumnCollectionPool
        {
            get
            {
                return instance.dataColumnCollectionPool;
            }
        }

        #endregion
    }
}