using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Dionysos.Presentation
{
    /// <summary>
    /// Collection class used for storing Lynx_media.Presentation.Logic.DataColumn instances in
    /// </summary>
    public class DataColumnCollection : ICollection<Dionysos.Presentation.DataColumn>
    {
        #region Fields

        private ArrayList items;
        private string entityName = string.Empty;
        private string userName = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the DataColumnCollection class
        /// </summary>	
        public DataColumnCollection()
        {
            this.items = new ArrayList();
        }

        /// <summary>
        /// Constructs an instance of the DataColumnCollection class
        /// </summary>	
        public DataColumnCollection(string entityName)
        {
            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else
            {
                this.entityName = entityName;
                this.items = new ArrayList();

                this.Initialize();
            }
        }

        /// <summary>
        /// Constructs an instance of the DataColumnCollection class
        /// </summary>	
        public DataColumnCollection(string entityName, string userName)
        {
            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else if (Instance.ArgumentIsEmpty(userName, "userName"))
            {
                // Parameter 'userName' is empty
            }
            else
            {
                this.entityName = entityName;
                this.userName = userName;
                this.items = new ArrayList();

                this.Initialize();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the Lynx_media.Presentation.Logic.DataColumnCollection instance
        /// by retrieving the field information and filling it into the collection
        /// </summary>
        public void Initialize()
        {
            if (Instance.Empty(this.entityName))
            {
                throw new Dionysos.EmptyException("Variable 'entityName' is empty.");
            }
            else
            {
                // Clear the current items from the datacolumn collection
                this.Clear();

                // Get the field information for the specified entity
                Dionysos.Interfaces.IFieldCollection fields = Global.FieldInfo[this.entityName];

                // Walk through the field information
                // and create data column instances
                for (int i = 0; i < fields.Count; i++)
                {
                    // Get a field from the field collection
                    Dionysos.Interfaces.IField field = fields[i];

                    // Create and initialize a DataColumn instance
                    // and set the properties
                    DataColumn dataColumn = new DataColumn();
                    dataColumn.Name = field.Name;
                    dataColumn.HeaderText = field.Name;
                    dataColumn.DataType = field.DataType;

                    // Add the data column to the collection
                    this.Add(dataColumn);
                }
            }
        }

        /// <summary>
        /// Adds an Lynx_media.Presentation.Logic.DataColumn instance to the collection
        /// </summary>
        /// <param name="dataColumn">The Lynx_media.Presentation.Logic.DataColumn instance to add to the collection</param>
        public void Add(Dionysos.Presentation.DataColumn dataColumn)
        {
            this.items.Add(dataColumn);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Lynx_media.Presentation.Logic.DataColumn instance is already in the collection
        /// </summary>
        /// <param name="dataColumn">The Lynx_media.Presentation.Logic.DataColumn instance to check</param>
        /// <returns>True if the Lynx_media.Presentation.Logic.DataColumn instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Presentation.DataColumn dataColumn)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Presentation.DataColumn) == dataColumn)
                {
                    contains = true;
                    break;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Presentation.DataColumn[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Lynx_media.Presentation.Logic.DataColumn instance from this collection
        /// </summary>
        /// <param name="dataColumn">The Lynx_media.Presentation.Logic.DataColumn instance to remove</param>
        public bool Remove(Dionysos.Presentation.DataColumn dataColumn)
        {
            this.items.Remove(dataColumn);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.DataColumnCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Presentation.DataColumn> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Presentation.DataColumn>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.DataColumnCollection
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets a DataColumn instance from the collection using the specified name
        /// </summary>
        /// <param name="name">The name of the DataColumn instance to get</param>
        /// <returns>A Lynx-media.Presentation.Logic.DataColumn instance</returns>
        private DataColumn GetDataColumn(string name)
        {
            DataColumn dataColumn = null;

            // Walk through the DataColumn instances of this collection
            for (int i = 0; i < this.Count; i++)
            {
                DataColumn temp = this[i];
                if (temp.Name == name)
                {
                    dataColumn = temp;
                    break;
                }
            }

            return dataColumn;
        }

        /// <summary>
        /// Writes the current DataColumnCollection instance to Xml
        /// </summary>
        public void WriteXml(XmlWriter xmlWriter)
        {
            if (Instance.ArgumentIsEmpty(xmlWriter, "xmlWriter"))
            {
                // Parameter 'xmlWriter' is empty
            }
            else
            {
                // Write a start element
                xmlWriter.WriteStartElement("DataColumnCollection");

                // Write the element strings
                xmlWriter.WriteElementString("EntityName", this.entityName);
                xmlWriter.WriteElementString("UserName", this.userName);

                for (int i = 0; i < this.Count; i++)
                {
                    DataColumn dataColumn = this[i];
                    dataColumn.WriteXml(xmlWriter);
                }

                // Write a end element
                xmlWriter.WriteEndElement();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Presentation.Logic.DataColumn instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx_media.Presentation.Logic.DataColumn instance to get</param>
        /// <returns>An Lynx_media.Presentation.Logic.DataColumn instance</returns>
        public Dionysos.Presentation.DataColumn this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Presentation.DataColumn;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Presentation.Logic.DataColumn instance from the collection from the specified index
        /// </summary>
        /// <param name="name">The name of the Lynx_media.Presentation.Logic.DataColumn instance to get</param>
        /// <returns>An Lynx_media.Presentation.Logic.DataColumn instance</returns>
        public Dionysos.Presentation.DataColumn this[string name]
        {
            get
            {
                return this.GetDataColumn(name);
            }
        }

        /// <summary>
        /// Gets or sets the entity name
        /// </summary>
        public string EntityName
        {
            get
            {
                return this.entityName;
            }
            set
            {
                this.entityName = value;
            }
        }

        /// <summary>
        /// Gets or sets the user name
        /// </summary>
        public string UserName
        {
            get
            {
                return this.userName;
            }
            set
            {
                this.userName = value;
            }
        }

        #endregion
    }
}