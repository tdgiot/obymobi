using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Dionysos;

namespace Dionysos.Presentation
{
    /// <summary>
    /// Class which represents a datacolumn
    /// </summary>
    public class DataColumn
    {
        #region Fields

        private string name = string.Empty;
        private string headerText = string.Empty;
        private int width = 0;
        private System.Type dataType = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the DataColumn class
        /// </summary>	
        public DataColumn()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Writes the current DataColumn instance to XML
        /// </summary>
        public void WriteXml(XmlWriter xmlWriter)
        {
            if (Instance.ArgumentIsEmpty(xmlWriter, "xmlWriter"))
            {
                // Parameter 'xmlWriter' is empty
            }
            else
            {
                // Write a start element
                xmlWriter.WriteStartElement("DataColumn");

                // Write the element strings
                xmlWriter.WriteElementString("Name", this.name);
                xmlWriter.WriteElementString("HeaderText", this.headerText);
                xmlWriter.WriteElementString("Width", this.width.ToString());
                xmlWriter.WriteElementString("DataType", this.dataType.ToString());

                // Write a end element
                xmlWriter.WriteEndElement();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the data column
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the header text for this datacolumn
        /// </summary>
        public string HeaderText
        {
            get
            {
                return this.headerText;
            }
            set
            {
                this.headerText = value;
            }
        }

        /// <summary>
        /// Gets or sets the width for this datacolumn
        /// </summary>
        public int Width
        {
            get
            {
                return this.width;
            }
            set
            {
                this.width = value;
            }
        }

        /// <summary>
        /// Gets or sets the datatype for this datacolumn
        /// </summary>
        public System.Type DataType
        {
            get
            {
                return this.dataType;
            }
            set
            {
                this.dataType = value;
            }
        }

        #endregion

        #region Event handlers



        #endregion
    }
}
