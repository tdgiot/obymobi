using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Dionysos.Presentation
{
    /// <summary>
    /// Collection class used for storing Lynx_media.Presentation.Logic.DataColumnCollection instances in
    /// </summary>
    public class DataColumnCollectionPool : ICollection<Dionysos.Presentation.DataColumnCollection>
    {
        #region Fields

        private ArrayList items;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the DataColumnCollectionPool class
        /// </summary>	
        public DataColumnCollectionPool()
        {
            this.items = new ArrayList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an Lynx_media.Presentation.Logic.DataColumnCollection instance to the collection
        /// </summary>
        /// <param name="dataColumnCollection">The Lynx_media.Presentation.Logic.DataColumnCollection instance to add to the collection</param>
        public void Add(Dionysos.Presentation.DataColumnCollection dataColumnCollection)
        {
            this.items.Add(dataColumnCollection);
        }

        /// <summary>
        /// Clears all the items in the collection
        /// </summary>
        public void Clear()
        {
            this.items.Clear();
        }

        /// <summary>
        /// Checks whether the specified Lynx_media.Presentation.Logic.DataColumnCollection instance is already in the collection
        /// </summary>
        /// <param name="dataColumnCollection">The Lynx_media.Presentation.Logic.DataColumnCollection instance to check</param>
        /// <returns>True if the Lynx_media.Presentation.Logic.DataColumnCollection instance is in the collection, False if not</returns>
        public bool Contains(Dionysos.Presentation.DataColumnCollection dataColumnCollection)
        {
            bool contains = false;

            for (int i = 0; i < this.items.Count; i++)
            {
                if ((this.items[i] as Dionysos.Presentation.DataColumnCollection) == dataColumnCollection)
                {
                    contains = true;
                }
            }

            return contains;
        }

        /// <summary>
        /// Copies the items from this collection to an array at the specified index
        /// </summary>
        /// <param name="array">The array to copy the items to</param>
        /// <param name="index">The index to copy the items at</param>
        public void CopyTo(Dionysos.Presentation.DataColumnCollection[] array, int index)
        {
            this.items.CopyTo(array, index);
        }

        /// <summary>
        /// Removes the specified Lynx_media.Presentation.Logic.DataColumnCollection instance from this collection
        /// </summary>
        /// <param name="dataColumnCollection">The Lynx_media.Presentation.Logic.DataColumnCollection instance to remove</param>
        public bool Remove(Dionysos.Presentation.DataColumnCollection dataColumnCollection)
        {
            this.items.Remove(dataColumnCollection);
            return true;
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.DataColumnCollectionPool
        /// </summary>
        /// <returns>The enumerator instance</returns>
        public IEnumerator<Dionysos.Presentation.DataColumnCollection> GetEnumerator()
        {
            return (IEnumerator<Dionysos.Presentation.DataColumnCollection>)this.items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for a range of elements in the Lynx-media.Web.Collections.DataColumnCollectionPool
        /// </summary>
        /// <returns>The enumerator instance</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private DataColumnCollection GetDataColumnCollection(string entityName, string userName)
        {
            DataColumnCollection dataColumnCollection = null;

            // Walk through the Lynx_media.Presentation.Logic.DataColumnCollection instances of this collection
            for (int i = 0; i < this.Count; i++)
            {
                DataColumnCollection temp = this[i];
                if (temp.EntityName == entityName)
                {
                    if (Instance.Empty(userName))
                    {
                        dataColumnCollection = temp;
                        break;
                    }
                    else
                    {
                        if (temp.UserName == userName)
                        {
                            dataColumnCollection = temp;
                            break;
                        }
                    }
                }
            }

            return dataColumnCollection;
        }

        /// <summary>
        /// Writes the current DataColumnCollectionPool instance to Xml
        /// </summary>
        public void WriteXml(string fileName)
        {
            if (Instance.ArgumentIsEmpty(fileName, "fileName"))
            {
                // Parameter 'fileName' is empty
            }
            else
            {
                // Create and initialize a XmlWriter instance
                XmlWriter xmlWriter = XmlUtil.CreateXmlWriter(fileName);

                // Write the start element
                xmlWriter.WriteStartElement("DataColumnCollections");

                // Walk through the datacolumn collection
                // and write them to xml
                for (int i = 0; i < this.Count; i++)
                {
                    DataColumnCollection dataColumnCollection = this[i];
                    dataColumnCollection.WriteXml(xmlWriter);
                }

                // Write the end element
                xmlWriter.WriteEndElement();
                xmlWriter.Flush();
                xmlWriter.Close();
            }
        }

        /// <summary>
        /// Reads the DataColumnCollectionPool instance from an Xml file
        /// </summary>
        public void ReadXml(string fileName)
        {
            if (Instance.ArgumentIsEmpty(fileName, "fileName"))
            {
                // Parameter 'fileName' is empty
            }
            else
            {

            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the amount of items in this collection
        /// </summary>
        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Boolean value indicating whether this collection is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.items.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Presentation.Logic.DataColumnCollection instance from the collection from the specified index
        /// </summary>
        /// <param name="index">The index of the Lynx_media.Presentation.Logic.DataColumnCollection instance to get</param>
        /// <returns>An Lynx_media.Presentation.Logic.DataColumnCollection instance</returns>
        public Dionysos.Presentation.DataColumnCollection this[int index]
        {
            get
            {
                return this.items[index] as Dionysos.Presentation.DataColumnCollection;
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Presentation.Logic.DataColumnCollection instance from the collection from the specified index
        /// </summary>
        /// <param name="entityName">The name of the entity to get the Lynx_media.Presentation.Logic.DataColumnCollection for</param>
        /// <returns>An Lynx_media.Presentation.Logic.DataColumnCollection instance</returns>
        public Dionysos.Presentation.DataColumnCollection this[string entityName]
        {
            get
            {
                return this.GetDataColumnCollection(entityName, string.Empty);
            }
        }

        /// <summary>
        /// Gets an Lynx_media.Presentation.Logic.DataColumnCollection instance from the collection from the specified index
        /// </summary>
        /// <param name="entityName">The name of the entity to get the Lynx_media.Presentation.Logic.DataColumnCollection for</param>
        /// <param name="userName">The name of the user to get the Lynx_media.Presentation.Logic.DataColumnCollection for</param>
        /// <returns>An Lynx_media.Presentation.Logic.DataColumnCollection instance</returns>
        public Dionysos.Presentation.DataColumnCollection this[string entityName, string userName]
        {
            get
            {
                return this.GetDataColumnCollection(entityName, userName);
            }
        }

        #endregion
    }
}