﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Utilities for parsing types.
	/// </summary>
	public static class ParseUtil
	{
		/// <summary>
		/// Indicates whether the specified String can be converted to a Boolean.
		/// </summary>
		/// <param name="value">The value to convert to Boolean.</param>
		/// <returns>True if converting was successful, False if not</returns>
		public static bool TryParseBool(this string value)
		{
			bool dummy;
			return bool.TryParse(value, out dummy);
		}

		/// <summary>
		/// Indicates whether the specified String can be converted to a Char.
		/// </summary>
		/// <param name="value">The value to convert to Char.</param>
		/// <returns>True if converting was successful, False if not</returns>
		public static bool TryParseChar(this string value)
		{
			char dummy;
			return char.TryParse(value, out dummy);
		}

		/// <summary>
		/// Indicates whether the specified String can be converted to a DateTime.
		/// </summary>
		/// <param name="value">The value to convert to DateTime.</param>
		/// <returns>True if converting was successful, False if not</returns>
		public static bool TryParseDate(this string value)
		{
			DateTime dummy;
			return DateTime.TryParse(value, out dummy);
		}

		/// <summary>
		/// Indicates whether the specified String can be converted to a Decimal.
		/// </summary>
		/// <param name="value">The value to convert to Decimal.</param>
		/// <returns>True if converting was successful, False if not</returns>
		public static bool TryParseDecimal(this string value)
		{
			decimal dummy;
			return decimal.TryParse(value, out dummy);
		}

		/// <summary>
		/// Indicates whether the specified String can be converted to a Double.
		/// </summary>
		/// <param name="value">The value to convert to Double.</param>
		/// <returns>True if converting was successful, False if not</returns>
		public static bool TryParseDouble(this string value)
		{
			double dummy;
			return double.TryParse(value, out dummy);
		}

		/// <summary>
		/// Indicates whether the specified String can be converted to an Integer.
		/// </summary>
		/// <param name="value">The value to convert to Integer.</param>
		/// <returns>True if converting was successful, False if not</returns>
		public static bool TryParseInt(this string value)
		{
			int dummy;
			return int.TryParse(value, out dummy);
		}

		/// <summary>
		/// Indicates whether the specified String can be converted to a Long.
		/// </summary>
		/// <param name="value">The value to convert to Long.</param>
		/// <returns>True if converting was successful, False if not</returns>
		public static bool TryParseLong(this string value)
		{
			long dummy;
			return long.TryParse(value, out dummy);
		}

		/// <summary>
		/// Indicates whether the specified String can be converted to a Short.
		/// </summary>
		/// <param name="value">The value to convert to Short.</param>
		/// <returns>True if converting was successful, False if not</returns>
		public static bool TryParseShort(this string value)
		{
			short dummy;
			return short.TryParse(value, out dummy);
		}
	}
}
