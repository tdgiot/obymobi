﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace System
{
    /// <summary>
    /// Class for logic related to numerics (decimal, float, int, long, etc).
    /// </summary>
    public static class NumericsUtil
    {
        /// <summary>
        /// Converts a decimal to the currency value of the specified culture.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="cultureName">The name of the culture.</param>
        /// <returns>
        /// The currency value.
        /// </returns>
        public static string ToStringMoney(this decimal amount, string cultureName)
        {
            return amount.ToString("C", CultureInfo.GetCultureInfo(cultureName));
        }

        /// <summary>
        /// Converts a decimal to a string using the specified format using the invariant culture.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The formatted value.
        /// </returns>
        public static string ToStringInvariantCulture(this decimal amount, string format)
        {
            return amount.ToString(format, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts a decimal to a string using the invariant culture.
        /// </summary>
        /// <param name="amount">The amount.</param>        
        /// <returns>
        /// The formatted value.
        /// </returns>
        public static string ToStringInvariantCulture(this decimal amount)
        {
            return amount.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Parses the string to a decimal using the InvariantCulture
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static decimal ParseInvariantCulture(string value)
        {
            return Decimal.Parse(value, Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Returns if the int is null or zero
        /// </summary>
        /// <param name="value">Integer value</param>
        /// <returns></returns>
        public static bool IsNullOrZero(this int? value)
        {
            return (!value.HasValue || value == 0);
        }

        /// <summary>
        /// Returns if the int is null or zero
        /// </summary>
        /// <param name="value">Integer value</param>
        /// <returns></returns>
        public static bool IsNullOrZero(this decimal? value)
        {
            return (!value.HasValue || value == 0);
        }
    }
}
