using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Dionysos.Text.RegularExpressions;
using System.Globalization;

namespace Dionysos
{
    /// <summary>
    /// Class which represents an Dutch zipcode
    /// </summary>
    public class ZipcodeDutch
    {
        #region Fields

        private string value = string.Empty;
        private string characters = string.Empty;
        private string numbers = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.ZipcodeDutch type
        /// using the specified zipcode
        /// </summary>
        protected ZipcodeDutch()
        {
        }

        /// <summary>
        /// Constructs an instance of the Dionysos.ZipcodeDutch type        
        /// </summary>
        public ZipcodeDutch(string zipcode)
        {
            var culture = CultureInfo.GetCultureInfo("nl-NL");
            if (ZipcodeDutch.IsZipcodeDutch(zipcode))
            {
                this.value = zipcode;
                var zipRegEx = RegEx.ZipcodeRegEx(culture);
                Match result = zipRegEx.Match(this.value);
                if (!result.Groups["Numbers"].Success || !result.Groups["Characters"].Success)
                    throw new Dionysos.TechnicalException("The RegEx used for Dutch zip codes doesn't have the Numbers and/or Characters group");

                this.numbers = result.Groups["Numbers"].Value;
                this.characters = result.Groups["Characters"].Value;
            }
            else
                throw new Dionysos.FunctionalException("String is in the wrong format: {0}, must be '0000AA' or '0000 AA'", zipcode);
        }

        /// <summary>
        /// Validate if the format is ZipcodeDutch-format
        /// </summary>
        /// <param name="zipcode"></param>
        /// <returns></returns>
        public static bool IsZipcodeDutch(string zipcode)
        {
            var culture = CultureInfo.GetCultureInfo("nl-NL");
            return RegEx.IsZipcode(zipcode, culture);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Try to get a ZipcodeDutch from a string containing a zipcode
        /// </summary>
        /// <param name="s">The System.String instance to parse to a Dionysos.Zipcode instance</param>
        /// <param name="zipcode">The Lynx-media.Zipcode instance to set</param>
        /// <returns>True if conversion was successful, False if not</returns>
        public static bool TryParse(string s, out ZipcodeDutch zipcode)
        {
            bool success = true;

            zipcode = null;
            // GK Come on guys, what is 'Try to Parse' if you are as unforgiving as the first method...
            if (ZipcodeDutch.IsZipcodeDutch(s))
            {
                zipcode = new ZipcodeDutch(s);
            }
            else
            {
                // GK Mannen-Parse geef me 4 aan ��n gesloten cijfers en 2 aan een gesloten letters


                success = false;
            }

            return success;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the zipcode in the original format
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }
        }

        /// <summary>
        /// Gets the string formatted like: [numbers]%[characters]
        /// </summary>
        public string LikePredicateValue
        {
            get
            {
                return string.Format("{0}%{1}", this.Numbers, this.Characters);
            }
        }

        /// <summary>
        /// Gets the numbers of the zipcode
        /// </summary>
        public string Numbers
        {
            get
            {
                return this.numbers;
            }
        }

        /// <summary>
        /// Gets the characters of the zipcode
        /// </summary>
        public string Characters
        {
            get
            {
                return this.characters;
            }
        }

        #endregion
    }
}
