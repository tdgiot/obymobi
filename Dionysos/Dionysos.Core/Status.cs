﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Enumeartion for Task status'
	/// </summary>
	public enum Status : int
	{
		/// <summary>
		/// Not started
		/// </summary>
		NotStarted = 100,
		/// <summary>
		/// In progress
		/// </summary>
		InProgress = 200,
		/// <summary>
		/// Waiting for other
		/// </summary>
		WaitingForOther = 300,
		/// <summary>
		/// Postponed
		/// </summary>
		Postponed = 400,
		/// <summary>
		/// Completed
		/// </summary>
		Completed = 500
	}
}
