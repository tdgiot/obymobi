﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Dionysos
{
	/// <summary>
	/// Class providing extension methods for collections and lists
	/// </summary>
	public static class CollectionUtil
	{
		/// <summary>
		/// Returns <value>NULL</value> if the <see cref="System.Collections.Generic.ICollection{T}"/> instance is <value>NULL</value> or Count = 0; otherwise returns the original <see cref="System.Collections.Generic.ICollection{T}"/>.
		/// </summary>
		/// <typeparam name="T">The type of elements in the collection.</typeparam>
		/// <param name="collection">A <see cref="System.Collections.Generic.ICollection{T}"/> instance to nullify.</param>
		/// <returns>
		/// Nillified <see cref="System.Collections.Generic.ICollection{T}"/>.
		/// </returns>
		public static ICollection<T> Nullify<T>(this ICollection<T> collection)
		{
			if (collection != null && collection.Count == 0)
			{
				collection = null;
			}

			return collection;
		}

		/// <summary>
		/// Determines whether the specified list contains the matching string value
		/// </summary>
		/// <param name="list">The list.</param>
		/// <param name="value">The value to match.</param>
		/// <param name="ignoreCase">if set to <c>true</c> the case is ignored.</param>
		/// <returns>
		///   <c>true</c> if the specified list contais the matching string; otherwise, <c>false</c>.
		/// </returns>
		public static bool Contains(this List<string> list, string value, bool ignoreCase)
		{
			return ignoreCase ?
				list.Any(s => s.Equals(value, StringComparison.OrdinalIgnoreCase)) :
				list.Contains(value);
		}

		/// <summary>
		/// Determines whether the specified list contains the matching string value
		/// </summary>
		/// <param name="list">The list.</param>
		/// <param name="value">The value to match.</param>
		/// <param name="comparisionType">Type of the comparision.</param>
		/// <returns>
		///   <c>true</c> if the specified list contais the matching string; otherwise, <c>false</c>.
		/// </returns>
		public static bool Contains(this List<string> list, string value, StringComparison comparisionType)
		{
			return list.Any(s => s.Equals(value, comparisionType));
		}
	}
}
