﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace System
{
	/// <summary>
	/// Utility class for Enums
	/// </summary>
	public static class BoolUtil
	{
		/// <summary>
		/// Returns Ja, Nee, Onbekend for: True, False, Null
		/// </summary>
		/// <param name="value">The Bool value to use.</param>
		/// <returns>Ja, Nee or Onbekend</returns>
		public static string ToStringDutch(this bool? value)
		{
			// GK TODO: Transalation
			if (value == null)
				return "Onbekend";
			else if (value.Value)
				return "Ja";
			else
				return "Nee";
		}

		/// <summary>
		/// Returns Ja, Nee, Onbekend for: True, False, Null
		/// </summary>
		/// <param name="value">The Bool value to use.</param>
		/// <returns>Ja, Nee or Onbekend</returns>
		public static string ToStringDutch(this bool value)
		{
			// GK TODO: Transalation
			if (value)
				return "Ja";
			else
				return "Nee";
		}

		/// <summary>
		/// Returns Ja, Nee, Onbekend for: True, False, Null
		/// </summary>
		/// <param name="value">The Bool value to use.</param>
		/// <returns>Ja, Nee or Onbekend</returns>
		public static string ToStringDutch(int? value)
		{
			// GK TODO: Transalation
			if (value == null)
				return "Onbekend";
			else if (value.Value == 1)
				return "Ja";
			else if (value.Value == 2)
				return "N.v.t.";
			else
				return "Nee";
		}
	}
}