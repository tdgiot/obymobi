﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Diagnostics
{
	/// <summary>
	/// Process diagnostics.
	/// </summary>
	public class Process
	{
		/// <summary>
		/// Determines whether a process is running with the specified process name
		/// </summary>
		/// <param name="processName">Name of the process.</param>
		/// <returns>
		/// 	<c>true</c> if a process is running with the specified process name; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsProcessRunning(string processName)
		{
			bool isRunning = false;

			foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
			{
				if (process.ProcessName.Equals(processName, StringComparison.OrdinalIgnoreCase))
				{
					isRunning = true;
					break;
				}
			}

			return isRunning;
		}

		/// <summary>
		/// Kills the process.
		/// </summary>
		/// <param name="processName">Name of the process.</param>
		/// <returns>
		/// 	<c>true</c> if the process is killed with the specified process name; otherwise, <c>false</c>.
		/// </returns>
		public static bool KillProcess(string processName)
		{
			bool success = true;

			try
			{
				// Kill the process
				foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
				{
					if (process.ProcessName.Equals(processName, StringComparison.OrdinalIgnoreCase))
					{
						process.Kill();
						break;
					}
				}
			}
			catch
			{
				success = false;
			}

			// Check whether the process is killed
			foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
			{
				if (process.ProcessName.Equals(processName, StringComparison.OrdinalIgnoreCase))
				{
					success = false;
					break;
				}
			}

			return success;
		}
	}
}
