using System;
using System.Collections.Generic;
using System.Text;
using Dionysos;

namespace Dionysos.Diagnostics
{
    /// <summary>
    /// Static class used for outputting debug messages
    /// </summary>
    public class Debug
    {
        #region Fields

        static Debug instance = null;
        private TimeStamp lastTimeStamp = null;
        private TimeStamp timeResetPointTimeStamp = null;
        private long lastWorkingSet = 0;
        private long memoryResetPoint = 0;
        private long maxMemoryUsage = 0;
        private long maxMemoryUsageFromResetPoint = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Debug class if the instance has not been initialized yet
        /// </summary>
        static Debug()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new Debug();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
            instance.lastTimeStamp = new TimeStamp();
            instance.timeResetPointTimeStamp = instance.lastTimeStamp;
            if (instance.lastWorkingSet == 0)
            {
                instance.lastWorkingSet = Environment.WorkingSet;
                instance.memoryResetPoint = Environment.WorkingSet;
            }
        }

        /// <summary>
        /// Reset a Memory Starting point to measure from there on
        /// </summary>
        public static void ResetMemoryStartingPoint()
        {
            instance.memoryResetPoint = Environment.WorkingSet;
        }

        /// <summary>
        /// Reset a Memory Starting point to measure from there on
        /// </summary>
        public static void ResetTimeStartingPoint()
        {
            instance.timeResetPointTimeStamp = new TimeStamp();
        }

        /// <summary>
        /// Return the current memory usage after last startingpoint reset
        /// </summary>
        /// <returns>MB of memory used</returns>
        public static long GetMemoryUseSinceStartingPoint()
        {
            long differenceMemoryResetPoint = Environment.WorkingSet - instance.memoryResetPoint;
            differenceMemoryResetPoint = differenceMemoryResetPoint / (1024 * 1024);
            return differenceMemoryResetPoint;
        }

        /// <summary>
        /// Return the current memory usage after last startingpoint reset
        /// </summary>
        /// <returns>MB of memory used</returns>
        public static TimeSpan GetTimeSpanSinceStartingPoint()
        {
            DateTime current = DateTime.Now;
            TimeSpan ts = current - instance.timeResetPointTimeStamp.GetDateTime();
            return ts;
        }

        /// <summary>
        /// Outputs a line to the debug information
        /// </summary>
        /// <param name="message">A message to output</param>
        public static void WriteLine(string message)
        {
            //System.Diagnostics.Debug.WriteLine(message);
            TimeStamp currentTimeStamp = new TimeStamp();

            long currentTime = long.Parse(currentTimeStamp.ToString());
            long lastTime = long.Parse(instance.lastTimeStamp.ToString());
            long difference = currentTime - lastTime;

            long differenceWorkingSet = Environment.WorkingSet - instance.lastWorkingSet;
            instance.lastWorkingSet = Environment.WorkingSet;
            differenceWorkingSet = differenceWorkingSet / (1024 * 1024);
            long differenceMemoryResetPoint = Environment.WorkingSet - instance.memoryResetPoint;

            if (differenceMemoryResetPoint > instance.maxMemoryUsageFromResetPoint)
                instance.maxMemoryUsageFromResetPoint = differenceMemoryResetPoint;

            differenceMemoryResetPoint = differenceMemoryResetPoint / (1024 * 1024);

            if (Environment.WorkingSet > instance.maxMemoryUsage)
                instance.maxMemoryUsage = Environment.WorkingSet;

            DateTime current = DateTime.Now;
            TimeSpan ts = current - instance.timeResetPointTimeStamp.GetDateTime();

            string time = string.Format("{0}:{1}:{2}.{3}", ts.Hours, ts.Minutes.ToString("00"), ts.Seconds.ToString("00"), ts.Milliseconds.ToString("000"));

            // Output the debug message
            System.Diagnostics.Debug.WriteLine(string.Format("[T: {1} / {6}]\t[M: {2} / {3} / {5} MB] {4}",
                currentTimeStamp.ToString(),
                difference.ToString("000000"),
                (Environment.WorkingSet / (1024 * 1024)),
                differenceWorkingSet,
                message,
                differenceMemoryResetPoint,
                time));

            instance.lastTimeStamp = currentTimeStamp;
        }

        /// <summary>
        /// Outputs a line to the debug information
        /// </summary>
        /// <param name="category">The category of the debug message</param>
        /// <param name="message">A message to output</param>
        public static void WriteLine(string category, string message)
        {
            //System.Diagnostics.Debug.WriteLine(category, message);
            TimeStamp currentTimeStamp = new TimeStamp();

            long currentTime = long.Parse(currentTimeStamp.ToString());
            long lastTime = long.Parse(instance.lastTimeStamp.ToString());
            long difference = currentTime - lastTime;

            long differenceWorkingSet = Environment.WorkingSet - instance.lastWorkingSet;

            // Output the debug message
            System.Diagnostics.Debug.WriteLine(string.Format("[T: {0} / {1}]\t[M: {2} / {3} MB] {4}: {5}", currentTimeStamp.ToString(), difference.ToString("000000"), Environment.WorkingSet.ToString("0000000000"), differenceWorkingSet.ToString("0000000000"), category, message));

            instance.lastTimeStamp = currentTimeStamp;
        }

        #endregion

        #region Properties

        /// <summary>
        /// MaxMemoryUsage
        /// </summary>
        public static long MaxMemoryUsage
        {
            get
            {
                return instance.maxMemoryUsage;
            }
        }

        /// <summary>
        /// MaxMemoryUsageFromResetPoint
        /// </summary>
        public static long MaxMemoryUsageFromResetPoint
        {
            get
            {
                return instance.maxMemoryUsageFromResetPoint;
            }
        }

        /// <summary>
        /// TimeSpanSinceStartingPointText
        /// </summary>
        public static string TimeSpanSinceStartingPointText
        {
            get
            {
                TimeSpan ts = Dionysos.Diagnostics.Debug.GetTimeSpanSinceStartingPoint();
                string time = string.Format("{0}:{1}:{2}.{3}", ts.Hours, ts.Minutes.ToString("00"), ts.Seconds.ToString("00"), ts.Milliseconds.ToString("000"));
                return time;
            }
        }

        /// <summary>
        /// MemoryAndTimeStatusSinceStartingPointText
        /// </summary>
        public static string MemoryAndTimeStatusSinceStartingPointText
        {
            get
            {
                TimeSpan ts = Dionysos.Diagnostics.Debug.GetTimeSpanSinceStartingPoint();
                long mem = Dionysos.Diagnostics.Debug.GetMemoryUseSinceStartingPoint();
                long maxMem = Dionysos.Diagnostics.Debug.MaxMemoryUsageFromResetPoint / (1024 * 1024);
                string time = string.Format("{0}:{1}:{2}.{3}", ts.Hours, ts.Minutes.ToString("00"), ts.Seconds.ToString("00"), ts.Milliseconds.ToString("000"));
                string status = string.Format("{0}MB (Max. {2}MB) | {1})", mem, time, maxMem);
                return status;
            }
        }

        #endregion
    }
}