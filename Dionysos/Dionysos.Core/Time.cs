using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Dionysos
{
    /// <summary>
    /// Class which represents a time
    /// </summary>
    public class Time
    {
        #region Fields

        private static Time instance = null;

        private int hour = 0;
        private int minute = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx-media.Time type
        /// </summary>
        public Time()
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx-media.Time type
        /// using the specified amount of hours and minutes
        /// </summary>
        public Time(int hour, int minute)
        {
            this.hour = hour;
            this.minute = minute;
        }

        /// <summary>
        /// Constructs a static instance of the Lynx-media.Time type
        /// </summary>
        static Time()
        {
            if (instance == null)
            {
                instance = new Time();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Converts the string representation into a Time instance
        /// </summary>
        /// <param name="s">The System.String instance to convert to a Lynx-media.Time instance</param>
        /// <param name="time">The Lynx-media.Time instance to set</param>
        /// <returns>True if conversion was successful, False if not</returns>
        public static bool TryParse(string s, out Time time)
        {
            bool success = true;
            time = new Time();

            Regex timeExpression = new Regex("^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$");
            if (timeExpression.IsMatch(s))
            {
                int index = s.IndexOf(":");
                time.Hour = Int32.Parse(s.Substring(0, index));
                time.Minute = Int32.Parse(s.Substring(index + 1));
            }
            else
            {
                success = false;
            }

            return success;
        }

        /// <summary>
        /// Gets a string representation of the Lynx-media.Time instance
        /// </summary>
        /// <returns>A System.String instance representing this Lynx-media.Time instance</returns>
        public override string ToString()
        {
            return this.hour.ToString("00") + ":" + this.minute.ToString("00");
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the amount of hours of the Lynx-media.Time instance
        /// </summary>
        public int Hour
        {
            get
            {
                return this.hour;
            }
            set
            {
                this.hour = value;
            }
        }

        /// <summary>
        /// Gets or sets the amount of minutes of the Lynx-media.Time instance
        /// </summary>
        public int Minute
        {
            get
            {
                return this.minute;
            }
            set
            {
                this.minute = value;
            }
        }

        #endregion
    }
}
