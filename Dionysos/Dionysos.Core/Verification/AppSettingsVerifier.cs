﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using Dionysos.Interfaces;

namespace Dionysos.Verification
{
	/// <summary>
	/// Generic Verifier to Check Write Permissions
	/// </summary>
	public class AppSettingsVerifier : IVerifier
	{
		#region Fields

		private string errorMessage = string.Empty;

        private readonly string key = string.Empty;
        private readonly string value = string.Empty;
        private readonly bool contains = false;

        private readonly IEnumerable<string> checkedSettings;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="setting"></param>
		public AppSettingsVerifier(params string[] setting)
		{
			checkedSettings = setting;
		}

		/// <summary>
		/// Constructs an instance of the AppSettingsVerifier class
		/// </summary>
		public AppSettingsVerifier(IEnumerable<string> settings)
		{
			checkedSettings = settings;
		}

        /// <summary>
        /// Constructs an instance of the AppSettingsVerifier class
        /// </summary>
        public AppSettingsVerifier(string key, string value, bool contains)
        {
            this.key = key;
            this.value = value;
            this.contains = contains;
        }

		#endregion

		#region Methods

		/// <summary>
		/// Executes the verifier and checks whether supplied AppSettings are filled in
		/// </summary>
		/// <returns>True if verification was succesful, False if not</returns>
		public bool Verify()
		{
            if (!this.key.IsNullOrWhiteSpace())
            {
                var value = System.Configuration.ConfigurationManager.AppSettings[this.key];
                if (value.IsNullOrWhiteSpace())
                {
                    this.errorMessage += String.Format("Could not find key '{0}' in AppSettings.", this.key);
                }
                else  
                {
                    if (contains && !value.Contains(this.value, StringComparison.InvariantCultureIgnoreCase))
                        this.errorMessage += String.Format("Value '{0}' of key '{1}' in AppSettings does not contain specified value '{2}'.", this.value, this.key, value);
                    else if (!contains && !value.Equals(this.value, StringComparison.InvariantCultureIgnoreCase))
                        this.errorMessage += String.Format("Value '{0}' of key '{1}' in AppSettings does not match specified value '{2}'.", this.value, this.key, value);
                }
            }
            else
            {
                foreach (string setting in checkedSettings)
                {
                    var value = System.Configuration.ConfigurationManager.AppSettings[setting];
                    if (value.IsNullOrWhiteSpace())
                    {
                        this.errorMessage += String.Format("Could not find key '{0}' in AppSettings.", setting);
                    }
                }
            }

			return this.errorMessage.Length == 0;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the error message if verification failed
		/// </summary>
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		#endregion
	}
}
