﻿using System;
using System.Security.Permissions;
using System.Security;
using System.IO;
using Dionysos.Interfaces;

namespace Dionysos.Verification
{
    /// <summary>
    /// Generic Verifier to Check Write Permissions
    /// </summary>
    public class WritePermissionVerifier : IVerifier
    {
        #region Fields

        private string errorMessage = string.Empty;
        private string fullPath = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ExtraVestiging.Logic.WritePermissionEVAdminVerifier class
        /// </summary>
        public WritePermissionVerifier(string fullPath)
        {
            this.fullPath = fullPath;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes the verifier and checks whether there are permissions to write files to the archive directories
        /// </summary>
        /// <returns>True if verification was succesful, False if not</returns>
        public bool Verify()
        {
            this.ValidatePath(this.fullPath);
            return this.errorMessage.Length == 0;
        }

        private void ValidatePath(string path)
        {
            try
            {
                // Check if directory exists
                if (!Directory.Exists(path))
                {
                    // Create it
                    Directory.CreateDirectory(path);
                }

                // Double check if it really exists now
                if (Directory.Exists(path))
                {

                    // Check if we have read & write permissions - very simply by actually executing that                     
                    string testPath = Path.Combine(path, "writePermissionVerifier.txt");

                    if(File.Exists(testPath))
                        File.Delete(testPath);

                    try
                    {
                        File.WriteAllText(testPath, DateTime.Now.ToString());
                    }
                    catch
                    {
                        throw new Exception("Couldn't write file: " + testPath);
                    }

                    if (!File.Exists(testPath))
                        throw new Exception("File didn't exist after it was tried to be created: " + testPath);
                    
                    File.Delete(testPath);

                    // The funky way, but I like not to know if it's possible according to the rules, but if it simply works.
                    //var permissionSet = new PermissionSet(PermissionState.None);
                    //permissionSet.AddPermission(readWritePermission);
                    //if (!permissionSet.IsSubsetOf(AppDomain.CurrentDomain.PermissionSet))
                    //{
                    //    this.errorMessage += String.Format("No write permissions on directory '{0}'.", path);
                    //}
                }
                else
                {
                    this.errorMessage += String.Format("Could not create directory '{0}'.", path);
                }
            }
            catch (Exception ex)
            {
                this.errorMessage += String.Format("No write permissions on directory '{0}' ({1}.", path, ex.Message);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the error message if verification failed
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}

