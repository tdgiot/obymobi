﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Interfaces;

namespace Dionysos.Verification
{
	/// <summary>
	/// Base verifier class.
	/// </summary>
	public abstract class VerifierBase : IVerifier
	{
		#region Fields

		/// <summary>
		/// The error message.
		/// </summary>
		private StringBuilder errorMessage = new StringBuilder();

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating whether the verification was successful.
		/// </summary>
		/// <value><c>true</c> if verification was successful; otherwise, <c>false</c>.</value>
		protected bool Successful
		{
			get
			{
				return this.errorMessage.Length == 0;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds the error message.
		/// </summary>
		/// <param name="value">The value.</param>
		protected void AddErrorMessage(string value)
		{
			if (this.errorMessage.Length != 0)
			{
				this.errorMessage.AppendLine();
			}

			this.errorMessage.Append(value);
		}

		/// <summary>
		/// Adds the error message.
		/// </summary>
		/// <param name="format">The format.</param>
		/// <param name="args">The args.</param>
		protected void AddErrorMessage(string format, params object[] args)
		{
			if (this.errorMessage.Length != 0)
			{
				this.errorMessage.AppendLine();
			}

			this.errorMessage.AppendFormat(format, args);
		}

		/// <summary>
		/// Adds the error message.
		/// </summary>
		/// <param name="ex">The exception.</param>
		protected void AddErrorMessage(Exception ex)
		{
			if (this.errorMessage.Length != 0)
			{
				this.errorMessage.AppendLine();
			}

			this.errorMessage.Append(ex.Message);
		}

		#endregion

		#region IVerifier Members

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>The error message.</value>
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage.ToString();
			}
			set
			{
				this.errorMessage = new StringBuilder(value);
			}
		}

		/// <summary>
		/// Executes the verifier.
		/// </summary>
		/// <returns><c>true</c> if verification was succesful; otherwise <c>false</c>.</returns>
		public abstract bool Verify();

		#endregion
	}
}
