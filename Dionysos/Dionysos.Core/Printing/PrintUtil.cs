﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;
using System.Printing;

namespace Dionysos.Printing
{
    /// <summary>
    /// Utility class which can be used to help with printing
    /// </summary>
    public class PrintUtil
    {
        /// <summary>
        /// Gets a flag which indicates whether the specified printer name is valid
        /// </summary>
        /// <param name="printerName">The name of the printer</param>
        /// <returns>True if the specified printer name is valid, False if not</returns>
        public static bool IsValidPrinterName(string printerName)
        {
            PrinterSettings printerSettings = new PrinterSettings();
            printerSettings.PrinterName = printerName;
            return printerSettings.IsValid;
        }

        /// <summary>
        /// Gets the print queue from the local print server for the specified name
        /// </summary>
        /// <param name="printQueueName">The name of the print queue to get</param>
        /// <returns>A System.Printing.PrintQueue instance</returns>
        public static PrintQueue GetPrintQueue(string printQueueName)
        {
            LocalPrintServer localPrintServer = new LocalPrintServer();
            return localPrintServer.GetPrintQueue(printQueueName);
        }

        /// <summary>
        /// Gets the printer error from a print queue
        /// </summary>
        /// <param name="printQueueName">The name of the print queue (printername)</param>
        /// <returns>A System.String instance containing the error or empty if the printer has no error</returns>
        public static string GetPrinterError(string printQueueName)
        {
            string error = string.Empty;

            PrintQueue printQueue = GetPrintQueue(printQueueName);
            if (printQueue.HasPaperProblem)
                error = "Printer has a paper problem.";
            else if (!(printQueue.HasToner))
                error = "Printer is out of toner.";
            else if (printQueue.IsDoorOpened)
                error = "Printer has an open door.";
            else if (printQueue.IsInError)
                error = "Printer is in an error state.";
            else if (printQueue.IsNotAvailable)
                error = "Printer is not available.";
            else if (printQueue.IsOffline)
                error = "Printer is off line.";
            else if (printQueue.IsOutOfMemory)
                error = "Printer is out of memory.";
            else if (printQueue.IsOutOfPaper)
                error = "Printer is out of paper.";
            else if (printQueue.IsOutputBinFull)
                error = "Printer has a full output bin.";
            else if (printQueue.IsPaperJammed)
                error = "Printer has a paper jam.";
            else if (printQueue.IsPaused)
                error = "Printer is paused.";
            else if (printQueue.NeedUserIntervention)
                error = "Printer needs user intervention.";

            return error;
        }
    }
}
