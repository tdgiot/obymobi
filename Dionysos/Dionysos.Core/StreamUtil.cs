﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Dionysos
{
	/// <summary>
	/// Helper class for Stream instances.
	/// </summary>
	public static class StreamUtil
	{
		/// <summary>
		/// Copies the contents of the input Stream to the output Stream.
		/// </summary>
		/// <param name="input">The input Stream to read from.</param>
		/// <param name="output">The output Stream to write to.</param>
		public static void Copy(this Stream input, Stream output)
		{
			if (input.CanSeek)
			{
				input.Position = 0;
			}

			byte[] buffer = new byte[8192];
			int len;
			while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
			{
				output.Write(buffer, 0, len);
			}
		}
	}
}
