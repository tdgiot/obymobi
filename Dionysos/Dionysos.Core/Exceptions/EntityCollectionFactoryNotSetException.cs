using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for when the entity collection factory has not been set
    /// </summary>
    public class EntityCollectionFactoryNotSetException : TechnicalException
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Exceptions.EntityCollectionFactoryNotSetException class
        /// </summary>	
        public EntityCollectionFactoryNotSetException(string message) : base(message)
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties



        #endregion

        #region Event handlers



        #endregion
    }
}

