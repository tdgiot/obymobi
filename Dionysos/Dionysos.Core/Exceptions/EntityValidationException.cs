using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for when a entity is not saved
    /// </summary>
    public class EntityValidationException : FunctionalException
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.EntitySaveException class
        /// </summary>	
        public EntityValidationException(string message)
            : base(message)
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties



        #endregion

        #region Event handlers



        #endregion
    }
}

