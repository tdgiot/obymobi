﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for any problem where an invalid type is used for casting
    /// </summary>
    public class InvalidTypeException : TechnicalException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.InvalidTypeException class
        /// </summary>	
        public InvalidTypeException(string message) : base(message)
        {
        }

        #endregion
    }
}

