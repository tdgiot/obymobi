﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for when a payment provider has not been set
    /// </summary>
    public class PaymentProviderNotSetException : TechnicalException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.PaymentProviderNotSetException class
        /// </summary>	
        public PaymentProviderNotSetException(string message) : base(message)
        {
        }

        #endregion
    }
}

