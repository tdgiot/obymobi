using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for when a entity is not saved
    /// </summary>
    public class EntitySaveException : TechnicalException
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.EntitySaveException class
        /// </summary>	
        public EntitySaveException(string message)
            : base(message)
        {
        }

        /// <summary>
		/// Constructs an instance of the Dionysos.EntitySaveException class
        /// </summary>
        /// <param name="message">Message format</param>
        /// <param name="args">Elements to format</param>
        public EntitySaveException(string message, params object[] args)
            : base(message.FormatSafe(args))
        {             
        }

		/// <summary>
		/// Constructs an instance of the Dionysos.EntitySaveException class
		/// </summary>
		/// <param name="innerException">Inner Exception</param>
		/// <param name="message">Message format</param>
		/// <param name="args">Elements to format</param>
		public EntitySaveException(Exception innerException, string message, params object[] args)
			: base(message.FormatSafe(args), innerException)
		{
		}

        #endregion

        #region Methods



        #endregion

        #region Properties



        #endregion

        #region Event handlers



        #endregion
    }
}

