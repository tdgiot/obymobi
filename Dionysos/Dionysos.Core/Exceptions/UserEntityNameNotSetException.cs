﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for when the entity factory has not been set
    /// </summary>
    public class UserEntityNameNotSetException : TechnicalException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Exceptions.UserEntityNameNotSetException class
        /// </summary>	
        public UserEntityNameNotSetException(string message) : base(message)
        {
        }

        #endregion
    }
}

