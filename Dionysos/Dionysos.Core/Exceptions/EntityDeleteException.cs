using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for when a entity is not deleted
    /// </summary>
    public class EntityDeleteException : FunctionalException
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Exceptions.EntityDeleteException class
        /// </summary>	
        public EntityDeleteException(string message) : base(message)
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="message">Message format</param>
        /// <param name="args">Elements to format</param>
        public EntityDeleteException(string message, params object[] args)
            : base(string.Format(message, args))
        {             
        }

        #endregion

        #region Methods



        #endregion

        #region Properties



        #endregion

        #region Event handlers



        #endregion
    }
}

