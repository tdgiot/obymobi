using System;
using System.Collections.Generic;
using System.Text;
//using System.Web.Services.Protocols;
using System.Security;

namespace Dionysos.Exceptions
{
	/// <summary>
	/// The ExceptionManager will use the configured EntLib Exception Handling for logging exceptions
	/// </summary>
	public class ExceptionManager
	{
		private const string GeneralPolicy = "General Exception Policy";
		private const string SoapPolicy = "Soap Exception Policy";
		private const string UnhandledPolicy = "Unhandled Exception Policy";

		/// <summary>
		/// This struct contains all exception policies, which must be defined in the web.config.
		/// This struct is the translation between an internal name and the actual policy that is used:
		/// </summary>
		/// <remarks>
		/// This is an internal struct to use only the policies that are defined in the web.config
		/// and described in the general technical design.
		/// </remarks>
		private struct Policy
		{
			public static readonly string General = GeneralPolicy;
			public static readonly string Soap = SoapPolicy;
			public static readonly string Unhandled = UnhandledPolicy;
		}

		/// <summary>
		/// The main entry point into the Exception Handling Application Block.
		/// Handles the specified <see cref="Exception"/>
		/// object according to the given.
		/// </summary>
		/// <param name="ex">An <see cref="Exception"/> object.</param>      
		/// <returns>
		/// Whether or not a rethrow is recommended.
		/// </returns>
		/// <example>
		/// The following code shows the usage of the 
		/// exception handling framework.
		/// <code>
		/// try
		///	{
		///		Foo();
		///	}
		///	catch (Exception e)
		///	{
		///		if (ExceptionManager.HandleException(e, name)) throw;
		///	}
		/// </code>
		/// </example>
		/// <remarks>
		/// Other specific exceptions could be added later to have a uniform way of handling exceptions.
		/// </remarks>
		public static bool HandleException(Exception ex)
		{
            //bool rethrow = ExceptionPolicy.HandleException(ex, Policy.General);
            //return rethrow;
            return true;
		}

		/// <summary>
		/// Handles exception according to the provided policy
		/// </summary>
		/// <param name="ex">The <see cref="Exception"/> to handle.</param>
		/// <returns><c>true</c> if a rethrow is needed, <c>false</c> otherwise.</returns>
		private static bool HandleUncaughtException(Exception ex)
		{            
			//bool rethrow = ExceptionPolicy.HandleException(ex, Policy.Unhandled);
			//return rethrow;
            return true;
		}

        ///// <summary>
        ///// The main entry point into the Exception Handling Application Block.
        ///// Handles a specific <see cref="SoapException"/>
        ///// object according to the given <paramref name="configurationContext"></paramref>.
        ///// </summary>
        ///// <param name="ex">A <see cref="SoapException"/> object that occured.</param>
        ///// <returns>
        ///// Whether or not a rethrow is recommended.
        ///// </returns>
        ///// <example>
        ///// The following code shows the usage of the exception handling framework for a SoapException.
        ///// <code>
        ///// try
        ///// {
        /////       Foo();
        ///// }
        ///// catch (SoapException ex)
        ///// {
        /////       if (ExceptionManager.HandleException(ex)) throw;
        ///// }
        ///// </code>
        ///// It is not necessary to catch a specific SoapException, because the correct overload will be chosen
        ///// if the Exception that is caught is a SoapException.
        ///// </example>
        //public static bool HandleException(SoapException ex)
        //{
        //    return ExceptionPolicy.HandleException(ex, Policy.Soap);
        //}

	} // public class ExceptionManager
}
