using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class for when the entity factory has not been set
    /// </summary>
    public class EntityFactoryNotSetException : TechnicalException
    {
        #region Fields



        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Lynx_media.Exceptions.EntityFactoryNotSetException class
        /// </summary>	
        public EntityFactoryNotSetException(string message) : base(message)
        {
        }

        #endregion

        #region Methods



        #endregion

        #region Properties



        #endregion

        #region Event handlers



        #endregion
    }
}

