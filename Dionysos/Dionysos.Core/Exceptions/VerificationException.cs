﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos
{
    /// <summary>
    /// Exception class which is thrown if verification fails 
    /// </summary>
    public class VerificationException : TechnicalException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Dionysos.Exceptions.VerificationException class
        /// </summary>	
        public VerificationException(string message) : base(message)
        {
        }

        #endregion
    }
}
