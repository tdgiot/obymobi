﻿namespace Dionysos
{
	/// <summary>
	/// Type of recurrence.
	/// </summary>
	public enum RecurrenceType : int
	{
		/// <summary>
		/// Minutely
		/// </summary>
		[StringValue("Per minuut")]
		Minutely = 100,
		/// <summary>
		/// Hourly
		/// </summary>
		[StringValue("Per uur")]
		Hourly = 200,
		/// <summary>
		/// Daily
		/// </summary>
		[StringValue("Dagelijks")]
		Daily = 300,
		/// <summary>
		/// Weekly
		/// </summary>
		[StringValue("Wekelijks")]
		Weekly = 400,
		/// <summary>
		/// Monthly
		/// </summary>
		[StringValue("Maandelijks")]
		Monthly = 500,
		/// <summary>
		/// Quarterly
		/// </summary>
		[StringValue("Kwartaal")]
		Quarterly = 600,
		/// <summary>
		/// Yearly
		/// </summary>
		[StringValue("Jaarlijks")]
		Yearly = 700
	}
}
