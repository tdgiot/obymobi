using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Dionysos
{
	/// <summary>
	/// Static class used for mathematical operations
	/// </summary>
	public static class Math
	{
		#region Methods

		/// <summary>
		/// Rounds a System.Decimal value to another System.Decimal value with zero decimals
		/// </summary>
		/// <param name="value">The System.Decimal value to round</param>
		/// <returns>A rounded System.Decimal value with zero decimals</returns>
		public static decimal Round(this decimal value)
		{
			return Round(value, 0);
		}

		/// <summary>
		/// Rounds a System.Decimal value to another System.Decimal value with the same amount of decimals as defined in: CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits
		/// </summary>
		/// <param name="value">The System.Decimal value to round</param>
		/// <returns>A rounded System.Decimal value</returns>
		public static decimal RoundCurrencyToCultureSetting(this decimal value)
		{
			return Round(value, CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits);
		}

		/// <summary>
		/// Rounds a System.Single value to another System.Single value with zero decimals
		/// </summary>
		/// <param name="value">The System.Single value to round</param>
		/// <returns>A rounded System.Single value with zero decimals</returns>
		public static float Round(float value)
		{
			return Round(value, 0);
		}

		/// <summary>
		/// Rounds a System.Decimal value to another System.Decimal value with the specified amount of decimals
		/// </summary>
		/// <param name="value">The System.Decimal value to round</param>
		/// <param name="decimals">The amount of decimals in the new value</param>
		/// <returns>A rounded System.Decimal value with the specified amount of decimals</returns>
		public static decimal Round(this decimal value, int decimals)
		{
			decimal result = decimal.MinValue;

			double power = Convert.ToDouble(decimals);
			double price = Convert.ToDouble(value);
			double helper;

			helper = price * System.Math.Pow(10, decimals);
			helper = System.Math.Sign(helper) * System.Math.Abs(System.Math.Floor(helper + .5));

			result = Convert.ToDecimal(helper / System.Math.Pow(10, decimals));

			return result;
		}

		/// <summary>
		/// Rounds a System.Single value to another System.Single value with the specified amount of decimals
		/// </summary>
		/// <param name="value">The System.Single value to round</param>
		/// <param name="decimals">The amount of decimals in the new value</param>
		/// <returns>A rounded System.Single value with the specified amount of decimals</returns>
		public static float Round(float value, int decimals)
		{
			float result = float.MinValue;

			double power = Convert.ToDouble(decimals);
			double price = Convert.ToDouble(value);
			double helper;

			helper = price * System.Math.Pow(10, decimals);
			helper = System.Math.Sign(helper) * System.Math.Abs(System.Math.Floor(helper + .5));

			result = Convert.ToSingle(helper / System.Math.Pow(10, decimals));

			return result;
		}

        /// <summary>
        /// Calculates the percentage
        /// </summary>
        /// <param name="current">The current amount.</param>
        /// <param name="total">The total amount.</param>
        /// <returns>A integer representing the percentage.</returns>
        public static int Percentage(int current, int total)
        {
            return Math.Percentage(current, total, false);
        }

        /// <summary>
        /// Calculates the percentage
        /// </summary>
        /// <param name="current">The current amount.</param>
        /// <param name="total">The total amount.</param>
        /// <param name="round">Flag which indicates whether rounding should be applied.</param>
        /// <returns>A integer representing the percentage.</returns>
        public static int Percentage(int current, int total, bool round)
        {
            int percentage = 0;

            if (current > 0 && total > 0)
            {
                if (round)
                    percentage = (int)Math.Round((float)(((double)current / (double)total) * 100));
                else
                    percentage = (int)(((double)current / (double)total) * 100);
            }

            return percentage;
        }

		#endregion
	}
}