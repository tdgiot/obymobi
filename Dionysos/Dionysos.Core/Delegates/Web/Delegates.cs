﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Delegates.Web
{
		/// <summary>
		/// Fired before loading the entity, can cancel loading to load your own entity
		/// </summary>
		/// <param name="preventLoading">If true the Page will not perform it's datasource loading logic</param>
		/// <param name="sender">PageEntity sending the event datasource</param>
		public delegate void DataSourceLoadHandler(object sender, ref bool preventLoading);

		/// <summary>
		/// Filed after loading, also when manually loaded, contains loaded datasource
		/// </summary>
		/// <param name="sender">PageEntity sending the event datasource</param>
		public delegate void DataSourceLoadedHandler(object sender);

        /// <summary>
        /// Fired when a custom callback is received from a treelist for example
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="command"></param>
        public delegate void CustomCallbackHandler(object sender, string command);
}
