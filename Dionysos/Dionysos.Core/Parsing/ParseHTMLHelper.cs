﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using Dionysos.Net;
using System.IO;
using System.Web;

namespace Dionysos.Parsing
{
	/// <summary>
	/// Helper class for much used Parser uses
	/// </summary>
	public class ParseHTMLHelper
	{
		/// <summary>
		/// Parse all elements containing "src"-attribute to make them relative with the supplied relativeUrlPrefix
		/// </summary>
		/// <param name="input">String to parse</param>
		/// <param name="relativeUrlPrefix">String containing prefix for src's</param>
		/// <param name="preventClass">Element's containg a src which are of this class type are not replaced</param>
		/// <returns>Parsed input string with complete url's based on the supplied relativeUrlPrefix</returns>
		public static string ParseSrcElements(string input, string relativeUrlPrefix, string preventClass)
		{
			ParseHTML parser = new ParseHTML();
			parser.Source = input;

			while (!parser.Eof())
			{
				char ch = parser.Parse();
				if (ch == 0)
				{
					AttributeList tag = parser.GetTag();
					if (tag["src"] != null)
					{
						// Don't do any elements holding the prevent class or absolute paths
						if ((tag["class"] == null || !tag["class"].Value.Contains(preventClass)) &&
							!Uri.IsWellFormedUriString(tag["src"].Value, UriKind.Absolute))
						{
							string fullSrc = "\"" + Path.Combine(relativeUrlPrefix, tag["src"].Value.Trim()) + "\"";
							input = input.Replace(string.Format("\"{0}\"", tag["src"].Value), fullSrc);
						}
					}
				}
			}

			return input;
		}

		/// <summary>
		/// Parse a input containing physical or relative paths in the 'src' attributes to cid attributes and add
		/// the LinkedResources to the supplied LinkedResourceCollection
		/// </summary>
		/// <param name="input">Html containing the img's</param>
		/// <param name="resources">Two dimensional array [resourcename,physicalpath]</param>
		/// <returns></returns>
		public static string ParseImgSrcToCid(string input, out List<string[]> resources)
		{
			return ParseImgSrcToCid(input, false, string.Empty, out resources);
		}

		/// <summary>
		/// Parse a input containing physical or relative paths in the 'src' attributes to cid attributes and add
		/// the LinkedResources to the supplied LinkedResourceCollection
		/// </summary>
		/// <param name="input">Html containing the img's</param>
		/// <param name="parseRelativePaths">Indicates if paths starting with ~/ should be mapped to their physicalpaths</param>
		/// <param name="rootPathOfSrcs">If this parameters has a value all src values are prefixed with the rootpath, the rootpath is allowed to be apprelative using ~/</param>
		/// <param name="resources">Two dimensional array [resourcename,physicalpath]</param>
		/// <returns></returns>
		public static string ParseImgSrcToCid(string input, bool parseRelativePaths, string rootPathOfSrcs, out List<string[]> resources)
		{
            if (rootPathOfSrcs == null)
            {
                rootPathOfSrcs = string.Empty;
            }

			ParseHTML parser = new ParseHTML();
			parser.Source = input;
			resources = new List<string[]>();

			int resourceNo = 0;
			while (!parser.Eof())
			{
				char ch = parser.Parse();
				if (ch == 0)
				{
					AttributeList tag = parser.GetTag();
					if (tag["src"] != null)
					{
						string physicalSource = tag["src"].Value.Trim();

                        if (physicalSource.StartsWith("http"))
                        {
                            continue;
                        }

						// Combine with root path
						if (!string.IsNullOrEmpty(rootPathOfSrcs))
						{
							physicalSource = StringUtil.CombineWithForwardSlash(rootPathOfSrcs, physicalSource);
						}

						// Parse relative paths
						if (parseRelativePaths &&
							physicalSource.StartsWith("~/"))
						{
							physicalSource = System.Web.Hosting.HostingEnvironment.MapPath(physicalSource);
						}

						string contentId = GuidUtil.CreateGuid();

						// Check if it's a new resource, or an earlier one reused
						bool newResource = true;
						for (int i = 0; i < resources.Count; i++)
						{
							if (resources[i][1] == physicalSource)
							{
								contentId = resources[i][0];
								newResource = false;
							}
						}

						if (newResource)
						{
							// Add the NEW resource to the collection                        
							resources.Add(new string[] { contentId, physicalSource });
							resourceNo++;
						}

						// Replace physicalpath with the resource ID
						string cidReference = string.Format("cid:{0}", contentId);
						input = input.Replace(string.Format("\"{0}\"", tag["src"].Value), "\"" + cidReference + "\"");
					}
				}
			}

			return input;
		}

		/// <summary>
		/// Replace some characters to make it showable in Outlook 07
		/// </summary>
		/// <param name="fileName">Filename</param>
		/// <returns>Replace name</returns>
		private static string CreateEmailSafeFileName(string fileName)
		{
			string toReturn = "";
			toReturn = fileName.Replace(" ", "_");
			toReturn = toReturn.Replace(")", "_");
			toReturn = toReturn.Replace("(", "_");
			return toReturn;
		}

		/// <summary>
		/// Parse a input containing links and return all url's found in href elements
		/// </summary>
		/// <param name="input">Html containing the links</param>
		/// <param name="excludeMailtoLinks">Exclude mailto: links</param>
		/// <returns></returns>
		public static List<string> ParseLinkHrefs(string input, bool excludeMailtoLinks)
		{
			ParseHTML parser = new ParseHTML();
			parser.Source = input;
			List<string> links = new List<string>();

			while (!parser.Eof())
			{
				char ch = parser.Parse();
				if (ch == 0)
				{
					AttributeList tag = parser.GetTag();
					if (tag["href"] != null)
					{
						if (excludeMailtoLinks &&
							tag["href"].Value.StartsWith("mailto:", StringComparison.OrdinalIgnoreCase))
						{
							// skip
						}
						else
						{
							links.Add(tag["href"].Value.Trim());
						}
					}
				}
			}

			return links;
		}

		/// <summary>
		/// Parse a input containing imgs and return all src's found in img elements
		/// </summary>
		/// <param name="input">Html containing the links</param>		
		/// <returns></returns>
		public static List<string> ParseImageSrcs(string input)
		{
			ParseHTML parser = new ParseHTML();
			parser.Source = input;
			List<string> imageSrcs = new List<string>();

			while (!parser.Eof())
			{
				char ch = parser.Parse();
				if (ch == 0)
				{
					AttributeList tag = parser.GetTag();
					if (tag.Name == "img" && tag["src"] != null)
					{
						if (!imageSrcs.Contains(tag["src"].Value.Trim()))
							imageSrcs.Add(tag["src"].Value.Trim());
					}
				}
			}

			return imageSrcs;
		}

		/// <summary>
		/// Retrieve a attribute from a input string containing one element (i.e img, a, etc.)
		/// </summary>
		/// <param name="input">one element (i.e img, a, etc.)</param>
		/// <param name="attribute">Attribute to fetch, href, src, etc.</param>
		/// <returns>Value or string.Empty</returns>
		public static string GetAttributeFromElement(string input, string attribute)
		{
			string toReturn = string.Empty;

			ParseHTML parser = new ParseHTML();
			parser.Source = input;

			int results = 0;

			while (!parser.Eof())
			{
				char ch = parser.Parse();
				if (ch == 0)
				{
					AttributeList tag = parser.GetTag();
					if (tag[attribute] != null)
					{
						results++;
						toReturn = tag[attribute].Value.Trim();
					}
				}
			}

			if (results > 1)
			{
				throw new Dionysos.TechnicalException("Parameter input ('{0}') of GetAttributeFromElement was containing more than 1 element or 1 element with multiple times the attribute '{1}'", input, attribute);
			}

			return toReturn;
		}
	}
}
