﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Dionysos.Geo
{
	/// <summary>
	/// A geographic coordinate.
	/// </summary>
	public struct GeoCoordinate
		: IEquatable<GeoCoordinate>
	{
		#region Fields

		/// <summary>
		/// The latitude.
		/// </summary>
		private readonly decimal latitude;

		/// <summary>
		/// The longitude.
		/// </summary>
		private readonly decimal longitude;

		/// <summary>
		/// Represents the empty geographic coordinate. This field is read-only.
		/// </summary>
		public static readonly GeoCoordinate Empty = new GeoCoordinate();

		#endregion

		#region Properties

		/// <summary>
		/// Gets the latitude.
		/// </summary>
		public decimal Latitude
		{
			get
			{
				return this.latitude;
			}
		}

		/// <summary>
		/// Gets the longitude.
		/// </summary>
		public decimal Longitude
		{
			get
			{
				return this.longitude;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="GeoCoordinate"/> struct.
		/// </summary>
		/// <param name="latitude">The latitude.</param>
		/// <param name="longitude">The longitude.</param>
		public GeoCoordinate(decimal latitude, decimal longitude)
		{
			this.latitude = latitude;
			this.longitude = longitude;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		public override bool Equals(object obj)
		{
			return obj is GeoCoordinate && this.Equals((GeoCoordinate)obj);
		}

		/// <summary>
		/// Determines whether the specified <see cref="GeoCoordinate"/> is equal to this instance.
		/// </summary>
		/// <param name="other">The <see cref="GeoCoordinate"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="GeoCoordinate"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		public bool Equals(GeoCoordinate other)
		{
			return this.Latitude == other.Latitude && this.Longitude == other.Longitude;
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			return this.Latitude.GetHashCode() ^ this.Longitude.GetHashCode();
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return this.Latitude.ToString(CultureInfo.InvariantCulture) + "," + this.Longitude.ToString(CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Creates a new <see cref="GeoCoordinate" /> instance using the specified coordinate.
		/// </summary>
		/// <param name="coordinate">The coordinate.</param>
		/// <param name="geoCoordinate">The geo coordinate.</param>
		/// <returns>
		/// A <see cref="System.Boolean" /> value that is <c>true</c> if the <see cref="GeoCoordinate" /> was successfully created; otherwise, <c>false</c>.
		/// </returns>
		public static bool TryCreate(string coordinate, out GeoCoordinate geoCoordinate)
		{
			if (!String.IsNullOrEmpty(coordinate))
			{
				string[] coordinates = coordinate.Split(',');

				decimal latitude, longitude;
				if (coordinates.Length == 2 &&
					Decimal.TryParse(coordinates[0], NumberStyles.Number, CultureInfo.InvariantCulture, out latitude) &&
					Decimal.TryParse(coordinates[1], NumberStyles.Number, CultureInfo.InvariantCulture, out longitude))
				{
					geoCoordinate = new GeoCoordinate(latitude, longitude);
					return true;
				}
			}

			geoCoordinate = default(GeoCoordinate);
			return false;
		}

		#endregion

		#region Operators

		/// <summary>
		/// Implements the operator ==.
		/// </summary>
		/// <param name="objA">The obj A.</param>
		/// <param name="objB">The obj B.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static bool operator ==(GeoCoordinate objA, GeoCoordinate objB)
		{
			return objA.Equals(objB);
		}

		/// <summary>
		/// Implements the operator !=.
		/// </summary>
		/// <param name="objA">The obj A.</param>
		/// <param name="objB">The obj B.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static bool operator !=(GeoCoordinate objA, GeoCoordinate objB)
		{
			return !(objA == objB);
		}

		#endregion
	}

}
