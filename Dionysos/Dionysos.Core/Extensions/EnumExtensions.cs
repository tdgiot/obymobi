﻿using System;
using System.Linq;

namespace Dionysos.Extensions
{
    public static class EnumExtensions
    {
        public static TAttribute GetCustomAttributeFromEnumField<TEnum, TAttribute>(this TEnum enumerator)
            where TEnum : Enum
            where TAttribute : Attribute
        {
            Type enumeratorType = typeof(TEnum);
            Type attributeType = typeof(TAttribute);

            string fieldName = Enum.GetName(enumeratorType, enumerator);

            return enumeratorType.GetField(fieldName).GetCustomAttributes(true)
                .FirstOrDefault(customAttribute => customAttribute.GetType() == attributeType) as TAttribute;
        }
    }
}
