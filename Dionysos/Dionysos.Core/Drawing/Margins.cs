﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Drawing
{
	/// <summary>
	/// Struct representing margins around a object
	/// </summary>
	public class Margins
	{
		/// <summary>
		/// Top margin
		/// </summary>
		public int Top;

		/// <summary>
		/// Right margin
		/// </summary>
		public int Right;

		/// <summary>
		/// Bottom margin
		/// </summary>
		public int Bottom;

		/// <summary>
		/// Left margin
		/// </summary>
		public int Left;

		/// <summary>
		/// Returns if the marings are Empty, meaning all four are 0 (zero)
		/// </summary>
		/// <returns></returns>
		public bool IsEmpty()
		{
			bool toReturn = false;
			if (Top == 0 && Right == 0 && Bottom == 0 && Left == 0)
			{
				toReturn = true;
			}
			return toReturn;
		}

		/// <summary>
		/// Margins Constructor
		/// </summary>
		/// <param name="top">Top</param>
		/// <param name="right">Right</param>
		/// <param name="bottom">Bottom</param>
		/// <param name="left">Left</param>
		public Margins(int top, int right, int bottom, int left)
		{
			this.Top = top;
			this.Right = right;
			this.Bottom = bottom;
			this.Left = left;
		}
	}
}
