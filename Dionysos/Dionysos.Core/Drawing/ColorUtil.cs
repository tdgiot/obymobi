using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Dionysos.Drawing
{
	/// <summary>
	/// Helper class which can be used to do operations on System.Drawing.Color instances.
	/// </summary>
	public static class ColorUtil
	{
		/// <summary>
		/// Returns the hexadecimal string representation of the color. Does not use the alpha channel.
		/// </summary>
		/// <param name="color">The color to convert.</param>
		/// <returns>The hexidecimal string representation if the color converted sucessfuly; otherwise null.</returns>
		public static string ToString(Color color)
		{
			if (color == null ||
				color.IsEmpty)
			{
				return null;
			}

			return String.Format("{0:x2}{1:x2}{2:x2}", color.R, color.G, color.B);
		}

		/// <summary>
		/// Returns the Color instance of the string representation of the color. Does not use the alpha channel.
		/// </summary>
		/// <param name="hexadecimalString">The hexadecimal string to convert.</param>
		/// <returns>The Color instance if the hexadecimal string converted sucessfuly; otherwise Color.Empty.</returns>
		public static Color ToColor(string hexadecimalString)
		{
			int argb;
			if (String.IsNullOrEmpty(hexadecimalString) ||
				(hexadecimalString.Length != 6 && hexadecimalString.Length != 3) ||
				!Int32.TryParse((hexadecimalString.Length == 6) ? hexadecimalString : String.Concat(hexadecimalString, hexadecimalString), NumberStyles.HexNumber, null, out argb))
			{
				return Color.Empty;
			}

			// Create color with alpha set to 255
			return Color.FromArgb(255, Color.FromArgb(argb));
		}

		#region Old shit

		/// <summary>
		/// Hexadecimal lookup table used to convert to a hexadecimal string.
		/// </summary>
		private static char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		/// <summary>
		/// Converts a Color instance to a hexadecimal string.
		/// </summary>
		/// <param name="color">The Color instance to convert.</param>
		/// <returns>Returns the hexadecimal representation of the Color instance (eg. "FFFFFF", "AB12E9").</returns>
		[Obsolete("Use ToString instead.")]
		public static string ColorToHexString(Color color)
		{
			byte[] bytes = new byte[4];
			bytes[0] = color.A;
			bytes[1] = color.R;
			bytes[2] = color.G;
			bytes[3] = color.B;

			char[] chars = new char[bytes.Length * 2];
			for (int i = 0; i < bytes.Length; i++)
			{
				int b = bytes[i];
				chars[i * 2] = hexDigits[b >> 4];
				chars[i * 2 + 1] = hexDigits[b & 0xF];
			}

			return new string(chars);
		}

		/// <summary>
		/// Extract only the hexadecimal digits from a string.
		/// </summary>
		/// <param name="input">The string to get the hexadecimal digits from.</param>
		/// <returns>Returns the hexadecimal digits found in the input string.</returns>
		[Obsolete("Parse to Int32 and format to hexadecimal instead.")]
		public static string ExtractHexDigits(string input)
		{
			string hexDigits = String.Empty;

			foreach (char c in input)
			{
				if (Regex.IsMatch(c.ToString(), "[abcdefABCDEF\\d]+", RegexOptions.Compiled))
				{
					hexDigits += c.ToString();
				}
			}

			return hexDigits;
		}

		#endregion
	}
}