﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Printing.IndexedProperties;
using Dionysos.Drawing.Quantization;

namespace Dionysos.Drawing
{
	/// <summary>
	/// Image utility class.
	/// </summary>
	public partial class ImageUtil
	{
		#region Methods

		/// <summary>
		/// Adds the margins.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="margins">The margins.</param>
		/// <param name="backgroundColor">Color of the background.</param>
		/// <exception cref="System.IO.FileNotFoundException">Could not find file to add margins.</exception>
		public static void AddMargins(string filePath, Margins margins, Color backgroundColor)
		{
			if (!File.Exists(filePath))
			{
				throw new FileNotFoundException("Could not find file to add margins.", filePath);
			}

			using (Bitmap sourceBitmap = new Bitmap(filePath))
			using (Bitmap targetBitmap = new Bitmap(sourceBitmap.Width + margins.Left + margins.Right, sourceBitmap.Height + margins.Top + margins.Bottom))
			using (Graphics g = Graphics.FromImage(targetBitmap))
			{
				// Draw background
				g.Clear(backgroundColor);

				// Draw source image
				g.DrawImage(sourceBitmap, margins.Left, margins.Top, sourceBitmap.Width, sourceBitmap.Height);

				// Get image format
				ImageFormat imageFormat = sourceBitmap.RawFormat;

				// Dispose sourceBitmap first, so we can overwrite
				sourceBitmap.Dispose();

				// Save image
				ImageUtil.SaveImage(targetBitmap, filePath, imageFormat);
			}
		}

		/// <summary>
		/// Adds the overlay.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="overlayFilePath">The overlay file path.</param>
		/// <exception cref="System.IO.FileNotFoundException">Could not find file to add overlay.
		/// or
		/// Could not find overlay file.</exception>
		public static void AddOverlay(string filePath, string overlayFilePath)
		{
			ImageUtil.AddOverlay(filePath, overlayFilePath, filePath, ImageSizeMode.Zoom, ImageCropPosition.Center);
		}

		/// <summary>
		/// Adds the overlay.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="overlayFilePath">The overlay file path.</param>
		/// <param name="targetFilePath">The target file path.</param>
		/// <param name="overlaySizeMode">The overlay size mode.</param>
		/// <param name="overlayPosition">The overlay position.</param>
		/// <exception cref="System.IO.FileNotFoundException">Could not find file to add overlay.
		/// or
		/// Could not find overlay file.</exception>
		public static void AddOverlay(string filePath, string overlayFilePath, string targetFilePath, ImageSizeMode overlaySizeMode, ImageCropPosition overlayPosition)
		{
			if (!File.Exists(filePath))
			{
				throw new FileNotFoundException("Could not find file to add overlay.", filePath);
			}
			if (!File.Exists(overlayFilePath))
			{
				throw new FileNotFoundException("Could not find overlay file.", overlayFilePath);
			}

			using (Bitmap sourceBitmap = new Bitmap(filePath))
			using (Bitmap overlayBitmap = new Bitmap(overlayFilePath))
			using (Bitmap targetBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height))
			using (Graphics g = Graphics.FromImage(targetBitmap))
			{
				// Draw source image
				g.DrawImage(sourceBitmap, 0, 0, targetBitmap.Width, targetBitmap.Height);

				// Draw overlay
				Rectangle overlayCropArea = ImageUtil.CalculateCropArea(new Size(targetBitmap.Width, targetBitmap.Height), overlayBitmap.Size, overlaySizeMode, overlayPosition);
				g.DrawImage(overlayBitmap, overlayCropArea);

				// Get image format
				ImageFormat imageFormat = sourceBitmap.RawFormat;

				// Dispose sourceBitmap first, so we can overwrite
				sourceBitmap.Dispose();

				// Save image
				ImageUtil.SaveImage(targetBitmap, targetFilePath, imageFormat);
			}
		}

		/// <summary>
		/// Adds rounded corners to an byte array of an image.
		/// </summary>
		/// <param name="imageBytes">The byte array of an image.</param>
		/// <param name="cornerRadius">The corner radius in pixels.</param>
		/// <param name="backgroundColor">The color of the rounded corners which will overlay the image.</param>
		/// <returns>
		/// Byte array of the image with the overlaying rectangle.
		/// </returns>
		public static byte[] AddRoundedCorners(byte[] imageBytes, int cornerRadius, Color backgroundColor)
		{
			byte[] targetImage = null;

			using (MemoryStream oldImageMS = new MemoryStream(imageBytes))
			using (Image oldImage = Image.FromStream(oldImageMS))
			using (Bitmap bitmap = new Bitmap(oldImage.Width, oldImage.Height))
			using (Graphics g = Graphics.FromImage(bitmap))
			{
				g.Clear(backgroundColor);
				g.SmoothingMode = SmoothingMode.AntiAlias;

				using (GraphicsPath gp = new GraphicsPath())
				using (Brush brush = new TextureBrush(oldImage))
				{
					Rectangle r = new Rectangle(0, 0, oldImage.Width, oldImage.Height);

					gp.AddArc(-1, -1, cornerRadius, cornerRadius, 180, 90);
					gp.AddArc(r.X + r.Width - cornerRadius, -1, cornerRadius, cornerRadius, 270, 90);
					gp.AddArc(r.X + r.Width - cornerRadius, r.Y + r.Height - cornerRadius, cornerRadius, cornerRadius, 0, 90);
					gp.AddArc(-1, r.Y + r.Height - cornerRadius, cornerRadius, cornerRadius, 90, 90);

					g.FillPath(brush, gp);
				}

				using (MemoryStream memoryStream = new MemoryStream())
				{
					bitmap.Save(memoryStream, oldImage.RawFormat);
					targetImage = memoryStream.ToArray();
				}
			}

			return targetImage;
		}

	    /// <summary>
	    /// Converts an byte array of an image to a .jpg byte array
	    /// </summary>
	    /// <param name="oldBytes">Old image byte array</param>
	    /// <param name="quality"></param>
	    /// <returns>
	    /// .jpg byte array
	    /// </returns>
	    public static byte[] ConvertToJpg(byte[] oldBytes, int quality = 70)
		{
			byte[] newBytes = null;

			// Create the old memory stream
			using (MemoryStream oldMemoryStream = new MemoryStream(oldBytes))
			using (Image oldImage = Image.FromStream(oldMemoryStream))
			using (Bitmap newBitmap = new Bitmap(oldImage.Width, oldImage.Height))
			using (Graphics g = Graphics.FromImage(newBitmap))
			{
				// Set the image to the temp bitmap
				g.DrawImage(oldImage, 0, 0, oldImage.Width, oldImage.Height);

				// Create a new memory stream
				using (MemoryStream newMemoryStream = new MemoryStream())
				{
					// Save JPEG with maximum quality
					ImageCodecInfo encoder = ImageUtil.GetEncoderInfo("image/jpeg");
					EncoderParameters encoderParameters = new EncoderParameters(1);
					encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);

					// Save the bitmap
					newBitmap.Save(newMemoryStream, encoder, encoderParameters);

					// Get the new byte array
					newBytes = newMemoryStream.ToArray();
				}
			}

			// Return the byte array of the new image
			return newBytes;
		}

		/// <summary>
		/// Crops and saves the sourceFilePath to the specified targetFilePath using the crop area and target size (uses the same ImageFormat as the source).
		/// </summary>
		/// <param name="sourceFilePath">The source filepath.</param>
		/// <param name="targetFilePath">The destination filepath.</param>
		/// <param name="cropArea">The area to copy to the target image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		public static void CropAndSave(string sourceFilePath, string targetFilePath, Rectangle cropArea, Size targetSize)
		{
			ImageUtil.CropAndSave(sourceFilePath, targetFilePath, cropArea, targetSize, null);
		}

		/// <summary>
		/// Crops and saves the sourceFilePath to the specified targetFilePath in the specified ImageFormat using the crop area and target size.
		/// </summary>
		/// <param name="sourceFilePath">The source filepath.</param>
		/// <param name="targetFilePath">The destination filepath.</param>
		/// <param name="cropArea">The area to copy to the target image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		/// <param name="targetFormat">The image format of the target image; if NULL, uses the same format as the source image.</param>
		public static void CropAndSave(string sourceFilePath, string targetFilePath, Rectangle cropArea, Size targetSize, ImageFormat targetFormat)
		{
			ImageUtil.CropAndSave(sourceFilePath, targetFilePath, cropArea, targetSize, targetFormat, Color.White);
		}

		/// <summary>
		/// Crops and saves the sourceFilePath to the specified targetFilePath in the specified ImageFormat using the crop area and target size.
		/// </summary>
		/// <param name="sourceFilePath">The source filepath.</param>
		/// <param name="targetFilePath">The destination filepath.</param>
		/// <param name="cropArea">The area to copy to the target image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		/// <param name="targetFormat">The image format of the target image; if NULL, uses the same format as the source image.</param>
		/// <param name="fillColor">The color to fill the image with.</param>
		/// <exception cref="System.IO.FileNotFoundException">Could not find file to crop and save.</exception>
		public static void CropAndSave(string sourceFilePath, string targetFilePath, Rectangle cropArea, Size targetSize, ImageFormat targetFormat, Color fillColor)
		{
			if (!File.Exists(sourceFilePath))
			{
				throw new FileNotFoundException("Could not find file to crop and save.", sourceFilePath);
			}

			using (Bitmap sourceBitmap = new Bitmap(sourceFilePath))
			using (Bitmap targetBitmap = new Bitmap(targetSize.Width, targetSize.Height))
			{
				if (sourceBitmap.Size.Equals(targetBitmap.Size) && sourceBitmap.Size.Equals(cropArea.Size) && cropArea.Location.IsEmpty)
				{
					// Same size and no cropping, so copy image
					File.Copy(sourceFilePath, targetFilePath, true);
				}
				else
				{
					// Crop and save the image
					using (Graphics g = Graphics.FromImage(targetBitmap))
					{
						// Set quality parameters
						g.SmoothingMode = SmoothingMode.HighQuality;
						g.InterpolationMode = InterpolationMode.HighQualityBicubic;
						g.PixelOffsetMode = PixelOffsetMode.HighQuality;
						g.CompositingQuality = CompositingQuality.HighQuality;

						// Fill with the specified color
						g.Clear(fillColor);

						// Draw cropped Bitmap to targetBitmap (with 1px overlap to remove jaggy edges)
						g.DrawImage(sourceBitmap, new Rectangle(-1, -1, targetSize.Width + 2, targetSize.Height + 2), cropArea, GraphicsUnit.Pixel);

						// If targetFormat is not set, get if from the targetFilePath or sourceBitmap
						if (targetFormat == null)
						{
							targetFormat = ImageUtil.GetImageFormat(targetFilePath) ?? sourceBitmap.RawFormat;
						}

						// Save the image
						ImageUtil.SaveImage(targetBitmap, targetFilePath, targetFormat);
					}
				}
			}
		}

		/// <summary>
		/// Returns the size of the image at the specified path.
		/// </summary>
		/// <param name="filePath">The path of the image to get the size for.</param>
		/// <returns>
		/// A Size instance which represents the size of the image.
		/// </returns>
		public static Size GetImageSize(string filePath)
		{
			Size imageSize;

			// Load the image from disk, get size and dispose correctly
			using (Image image = Image.FromFile(filePath))
			{
				imageSize = image.Size;
			}

			return imageSize;
		}

		/// <summary>
		/// Crops and saves the sourceFilePath to the specified targetFilePath in the specified ImageFormat using the crop area and target size.
		/// </summary>
		/// <param name="sourceBitmap">The source bitmap to resize.</param>
		/// <param name="cropArea">The area to copy to the target image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		/// <param name="targetFormat">The image format of the target image; if NULL, uses the same format as the source image.</param>
		/// <param name="fillColor">The color to fill the image with.</param>
		/// <returns></returns>
		public static Bitmap Resize(Bitmap sourceBitmap, Rectangle cropArea, Size targetSize, ImageFormat targetFormat, Color fillColor)
		{
			Bitmap targetBitmap = new Bitmap(targetSize.Width, targetSize.Height);

			if (sourceBitmap.Size.Equals(targetBitmap.Size) && sourceBitmap.Size.Equals(cropArea.Size) && cropArea.Location.IsEmpty)
			{
				// Same size and no cropping, so copy image
				targetBitmap = sourceBitmap;
			}
			else
			{
				// Crop and save the image
				using (Graphics g = Graphics.FromImage(targetBitmap))
				{
					// Set quality parameters
					g.SmoothingMode = SmoothingMode.HighQuality;
					g.InterpolationMode = InterpolationMode.HighQualityBicubic;
					g.PixelOffsetMode = PixelOffsetMode.HighQuality;
					g.CompositingQuality = CompositingQuality.HighQuality;

					// Fill with the specified color
					//g.Clear(fillColor);

					// Draw cropped Bitmap to targetBitmap (with 1px overlap to remove jaggy edges)
					g.DrawImage(sourceBitmap, new Rectangle(-1, -1, targetSize.Width + 2, targetSize.Height + 2), cropArea, GraphicsUnit.Pixel);

					// If targetFormat is not set, get if from the targetFilePath or sourceBitmap
					if (targetFormat == null)
					{
						targetFormat = sourceBitmap.RawFormat;
					}
				}
			}

			return targetBitmap;
		}

        /// <summary>
        /// Resizes the specified image to the specified target size and returns the new image as byte array
        /// </summary>
        /// <param name="imageFile">The bytes of the image to resize</param>
        /// <param name="maximumSize">The maximum size of the new image</param>
        /// <param name="fitInDimensions">If set to <c>true</c> fits the image in the specified dimensions.</param>
        /// <param name="imageQuality">The image quality (0 - 100).</param>
        /// <returns>
        /// An array of Bytes containing the resized image
        /// </returns>
	    public static byte[] ResizeAndReturn(byte[] imageFile, Size maximumSize, bool fitInDimensions = false, int imageQuality = 80)
	    {
            Size originalSize;
            Size targetSize = new Size(0, 0);
            ImageFormat originalRawFormat;

            using (Image original = Image.FromStream(new MemoryStream(imageFile)))
            {
                originalSize = new Size(original.Width, original.Height);
                originalRawFormat = original.RawFormat;
            }

            if (maximumSize.Height == 0)
                maximumSize.Height = originalSize.Height * (maximumSize.Width / originalSize.Width);

            if (maximumSize.Width == 0)
                maximumSize.Width = originalSize.Width * (maximumSize.Height / originalSize.Height);

            if (fitInDimensions)
            {
                Decimal targetRatio = Decimal.Divide(maximumSize.Width, maximumSize.Height);
                Decimal sourceRatio = Decimal.Divide(originalSize.Width, originalSize.Height);
                // Target 300 x 400 = W/H .75
                // 500 x 250 = W / H = 2
                // 250 x 500 = W / H = .5

                if (targetRatio > sourceRatio)
                {
                    // Target is wider than source, use height as limiting factor
                    targetSize.Height = maximumSize.Height;
                    targetSize.Width = Convert.ToInt32(originalSize.Width * Decimal.Divide(maximumSize.Height, originalSize.Height));
                }
                else
                {
                    // Target is higher than source, use width as limiting factor.
                    targetSize.Width = maximumSize.Width;
                    targetSize.Height = Convert.ToInt32(originalSize.Height * Decimal.Divide(maximumSize.Width, originalSize.Width));
                }
            }
            else
            {
                if (originalSize.Height < originalSize.Width) // landscape
                {
                    targetSize.Height = maximumSize.Height;
                    targetSize.Width = (int)(originalSize.Width * ((float)maximumSize.Height / originalSize.Height));
                    if (targetSize.Width < maximumSize.Width)
                    {
                        targetSize.Width = maximumSize.Width;
                        targetSize.Height = (int)(originalSize.Height * ((float)maximumSize.Width / originalSize.Width));
                    }
                }
                else // portrait
                {
                    targetSize.Width = maximumSize.Width;
                    targetSize.Height = (int)(originalSize.Height * ((float)maximumSize.Width / originalSize.Width));
                    if (targetSize.Height < maximumSize.Height)
                    {
                        targetSize.Height = maximumSize.Height;
                        targetSize.Width = (int)(originalSize.Width * ((float)maximumSize.Height / originalSize.Height));
                    }
                }
            }

            byte[] returnBuffer;

            using (Image imgPhoto = Image.FromStream(new MemoryStream(imageFile)))
            using (Bitmap bmPhoto = new Bitmap(targetSize.Width, targetSize.Height))
            {
                // Create a new blank canvas.  The resized image will be drawn on this canvas.
                bmPhoto.SetResolution(72, 72);

                using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                {
                    grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                    grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    grPhoto.DrawImage(imgPhoto, new Rectangle(0, 0, targetSize.Width, targetSize.Height), 0, 0, originalSize.Width, originalSize.Height, GraphicsUnit.Pixel);

                    // Save out to memory and then to a file.  We dispose of all objects to make sure the files don't stay locked.
                    ImageCodecInfo encoder;
                    if (originalRawFormat.Equals(ImageFormat.Png))
                        encoder = GetEncoderInfo("image/png");
                    else
                        encoder = GetEncoderInfo("image/jpeg");

                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, imageQuality);

                    MemoryStream mm = new MemoryStream();
                    bmPhoto.Save(mm, encoder, encoderParameters);

                    returnBuffer = mm.GetBuffer();
                }
            }

            return returnBuffer;
	    }

		/// <summary>
		/// Resizes and saves the sourceFilePath to the specified targetFilePath using the crop position and target size (uses the same ImageFormat as the source);
		/// </summary>
		/// <param name="sourceFilePath">The source filepath.</param>
		/// <param name="targetFilePath">The destination filepath.</param>
		/// <param name="imageCropPosition">The positions to copy to the target image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		public static void ResizeAndSave(string sourceFilePath, string targetFilePath, ImageCropPosition imageCropPosition, Size targetSize)
		{
			ImageUtil.ResizeAndSave(sourceFilePath, targetFilePath, imageCropPosition, targetSize, null);
		}

		/// <summary>
		/// Resizes and saves the sourceFilePath to the specified targetFilePath in the specified ImageFormat using the crop position and target size.
		/// </summary>
		/// <param name="sourceFilePath">The source filepath.</param>
		/// <param name="targetFilePath">The destination filepath.</param>
		/// <param name="imageCropPosition">The positions to copy to the target image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		/// <param name="targetFormat">The image format of the target image; if NULL, uses the same format as the source image.</param>
		public static void ResizeAndSave(string sourceFilePath, string targetFilePath, ImageCropPosition imageCropPosition, Size targetSize, ImageFormat targetFormat)
		{
			ImageUtil.ResizeAndSave(sourceFilePath, targetFilePath, imageCropPosition, targetSize, targetFormat, ImageSizeMode.Zoom, Color.White);
		}

		/// <summary>
		/// Resizes and saves the sourceFilePath to the specified targetFilePath in the specified ImageFormat using the crop position and target size.
		/// </summary>
		/// <param name="sourceFilePath">The source filepath.</param>
		/// <param name="targetFilePath">The destination filepath.</param>
		/// <param name="imageCropPosition">The positions to copy to the target image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		/// <param name="targetFormat">The image format of the target image; if NULL, uses the same format as the source image.</param>
		/// <param name="imageSizeMode">Specifies the size mode of the image.</param>
		/// <param name="fillColor">The color to fill the image with.</param>
		public static void ResizeAndSave(string sourceFilePath, string targetFilePath, ImageCropPosition imageCropPosition, Size targetSize, ImageFormat targetFormat, ImageSizeMode imageSizeMode, Color fillColor)
		{
			// Get the sourceFilePath Size
			Size sourceSize = GetImageSize(sourceFilePath);

			// Check targetSize
			if (targetSize.IsEmpty ||
				(targetSize.Width == 0 && targetSize.Height == sourceSize.Height) ||
				(targetSize.Height == 0 && targetSize.Width == sourceSize.Width))
			{
				// No target size specified or target size equals source size
				targetSize = sourceSize;
			}
			else if (targetSize.Width == 0)
			{
				// Use full width (and keep source ratio)
				targetSize.Width = (int)System.Math.Ceiling(targetSize.Height * ((decimal)sourceSize.Width / sourceSize.Height));
			}
			else if (targetSize.Height == 0)
			{
				// Use full height (and keep source ratio)
				targetSize.Height = (int)System.Math.Ceiling(targetSize.Width * ((decimal)sourceSize.Height / sourceSize.Width));
			}

			//// Calculate aspect ratios
			//decimal sourceAspectRatio = (decimal)sourceSize.Width / sourceSize.Height;
			//decimal targetAspectRatio = (decimal)targetSize.Width / targetSize.Height;

			// Calcuate the crop area
			Rectangle cropArea = CalculateCropArea(sourceSize, targetSize, imageSizeMode, imageCropPosition);

			// Crop and save image
			ImageUtil.CropAndSave(sourceFilePath, targetFilePath, cropArea, targetSize, targetFormat, fillColor);
		}

        public static Size GetFittingDimenions(int width, int height, int maxWidth, int maxHeight)
        {
            int targetH, targetW;

            if (maxHeight == 0)
                maxHeight = height * (maxWidth / width);

            if (maxWidth == 0)
                maxWidth = width * (maxHeight / height);

            targetW = maxWidth;
            targetH = maxHeight;

            Decimal targetRatio = Decimal.Divide(maxWidth, maxHeight);
            Decimal sourceRatio = Decimal.Divide(width, height);
            // Target 300 x 400 = W/H .75
            // 500 x 250 = W / H = 2
            // 250 x 500 = W / H = .5

            if (targetRatio > sourceRatio)
            {
                // Target is wider than source, use height as limiting factor
                targetH = maxHeight;
                targetW = Convert.ToInt32(width * Decimal.Divide(maxHeight, height));
            }
            else
            {
                // Target is higher than source, use width as limiting factor.
                targetW = maxWidth;
                targetH = Convert.ToInt32(height * Decimal.Divide(maxWidth, width));
            }

            return new Size(targetW, targetH);
        }

		/// <summary>
		/// Resizes image to specified dimensions
		/// </summary>
		/// <param name="filePath">The filepath of the image</param>
		/// <param name="newSize">The new size of the image</param>
		public static void ResizeImage(string filePath, Size newSize)
		{
			using (Bitmap sourceBitmap = new Bitmap(filePath))
			{
				Bitmap targetBitmap = ImageUtil.ResizeImage(sourceBitmap, newSize);

				// Get image format
				ImageFormat imageFormat = sourceBitmap.RawFormat;

				// Dispose sourceBitmap first, so we can overwrite
				sourceBitmap.Dispose();

				// Save image
				ImageUtil.SaveImage(targetBitmap, filePath, imageFormat);
			}
		}

		/// <summary>
		/// Resizes the image.
		/// </summary>
		/// <param name="bitmap">The bitmap.</param>
		/// <param name="targetSize">The new size of the image</param>
		/// <returns>
		/// The resized bitmap.
		/// </returns>
		public static Bitmap ResizeImage(Image bitmap, Size targetSize)
		{
			// Keep aspect ratio
			decimal ratio;
			int newWidth = 0, newHeight = 0;
			if ((bitmap.Width > bitmap.Height && targetSize.Width != 0) || targetSize.Height == 0)
			{
				ratio = (decimal)targetSize.Width / bitmap.Width;
				newWidth = targetSize.Width;
				newHeight = (int)(bitmap.Height * ratio);
			}
			else
			{
				ratio = (decimal)targetSize.Height / bitmap.Height;
				newHeight = targetSize.Height;
				newWidth = (int)(bitmap.Width * ratio);
			}

			var targetBitmap = new Bitmap(newWidth, newHeight);

			using (Graphics g = Graphics.FromImage(targetBitmap))
			{
				// Keep the highest quality
				g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

				// White background to prevent bleed-through
				g.Clear(Color.White);

				// Draw 1 pixel outside target to prevent half opaque border
				g.DrawImage(bitmap, -1, -1, newWidth + 2, newHeight + 2);
			}

			return targetBitmap;
		}

		/// <summary>
		/// Calculates the crop area using a source and target size.
		/// </summary>
		/// <param name="sourceSize">The size of the source image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		/// <param name="imageSizeMode">Specifies the size mode of the image.</param>
		/// <param name="imageCropPosition">The position to copy to the target image.</param>
		/// <returns>
		/// Returns the crop area as a Rectangle.
		/// </returns>
		/// <exception cref="System.NotImplementedException">The specified ImageSizeMode is not implemented.
		/// or
		/// The specified ImageCropPosition is not implemented.</exception>
		private static Rectangle CalculateCropArea(Size sourceSize, Size targetSize, ImageSizeMode imageSizeMode, ImageCropPosition imageCropPosition)
		{
			Rectangle cropArea = Rectangle.Empty;

			// Calculate aspect ratios
			decimal sourceAspectRatio = (decimal)sourceSize.Width / sourceSize.Height;
			decimal targetAspectRatio = (decimal)targetSize.Width / targetSize.Height;

			// Check size
			bool isEnlarged = (sourceSize.Width < targetSize.Width) && (sourceSize.Height < targetSize.Height);

			// Get crop area
			switch (imageSizeMode)
			{
				case ImageSizeMode.Zoom:
					if (sourceAspectRatio == targetAspectRatio)
					{
						// Same aspect ratio
						cropArea.Width = sourceSize.Width;
						cropArea.Height = sourceSize.Height;
					}
					else if (sourceAspectRatio < targetAspectRatio)
					{
						// Source is wider, use full height
						cropArea.Width = sourceSize.Width;
						cropArea.Height = (int)(cropArea.Width * ((decimal)targetSize.Height / targetSize.Width));
					}
					else
					{
						// Source is taller, use full width
						cropArea.Height = sourceSize.Height;
						cropArea.Width = (int)(cropArea.Height * ((decimal)targetSize.Width / targetSize.Height));
					}
					break;
				case ImageSizeMode.Squeeze:
					if (sourceAspectRatio == targetAspectRatio)
					{
						// Same aspect ratio
						cropArea.Width = isEnlarged ? targetSize.Width : sourceSize.Width;
						cropArea.Height = isEnlarged ? targetSize.Height : sourceSize.Height;
					}
					else if (sourceAspectRatio < targetAspectRatio)
					{
						// Source is wider, use full width
						cropArea.Height = isEnlarged ? targetSize.Height : sourceSize.Height;
						cropArea.Width = (int)(cropArea.Height * ((decimal)targetSize.Width / targetSize.Height));
					}
					else
					{
						// Source is taller, use full height
						cropArea.Width = isEnlarged ? targetSize.Width : sourceSize.Width;
						cropArea.Height = (int)(cropArea.Width * ((decimal)targetSize.Height / targetSize.Width));
					}
					break;
				default:
					throw new NotImplementedException("The specified ImageSizeMode is not implemented.");
			}

			// Get crop position
			switch (imageCropPosition)
			{
				case ImageCropPosition.TopLeft:
					cropArea.X = 0;
					cropArea.Y = 0;
					break;
				case ImageCropPosition.TopCenter:
					cropArea.X = (sourceSize.Width - cropArea.Width) / 2;
					cropArea.Y = 0;
					break;
				case ImageCropPosition.TopRight:
					cropArea.X = sourceSize.Width - cropArea.Width;
					cropArea.Y = 0;
					break;
				case ImageCropPosition.CenterLeft:
					cropArea.X = 0;
					cropArea.Y = (sourceSize.Height - cropArea.Height) / 2;
					break;
				case ImageCropPosition.Center:
					cropArea.X = (sourceSize.Width - cropArea.Width) / 2;
					cropArea.Y = (sourceSize.Height - cropArea.Height) / 2;
					break;
				case ImageCropPosition.CenterRight:
					cropArea.X = sourceSize.Width - cropArea.Width;
					cropArea.Y = (sourceSize.Height - cropArea.Height) / 2;
					break;
				case ImageCropPosition.BottomLeft:
					cropArea.X = 0;
					cropArea.Y = sourceSize.Height - cropArea.Height;
					break;
				case ImageCropPosition.BottomCenter:
					cropArea.X = (sourceSize.Width - cropArea.Width) / 2;
					cropArea.Y = sourceSize.Height - cropArea.Height;
					break;
				case ImageCropPosition.BottomRight:
					cropArea.X = sourceSize.Width - cropArea.Width;
					cropArea.Y = sourceSize.Height - cropArea.Height;
					break;
				default:
					throw new NotImplementedException("The specified ImageCropPosition is not implemented.");
			}

			return cropArea;
		}

		/// <summary>
		/// Gets the ImageCodecInfo for the specified MIME type.
		/// </summary>
		/// <param name="mimeType">The MIME type to get the ImageCodecInfo for.</param>
		/// <returns>
		/// The ImageCodecInfo of the specified MIME type; otherwise <value>null</value>.
		/// </returns>
		private static ImageCodecInfo GetEncoderInfo(string mimeType)
		{
			ImageCodecInfo imageCodecInfo = null;

			// Get all ImageCodecInfo objects supported by GDI+
			ImageCodecInfo[] imageEncoders = ImageCodecInfo.GetImageEncoders();

			// Find the ImageCodecInfo for the specified MIME type
			for (int i = 0; i < imageEncoders.Length; i++)
			{
				if (imageEncoders[i].MimeType.Equals(mimeType))
				{
					imageCodecInfo = imageEncoders[i];
					break;
				}
			}

			return imageCodecInfo;
		}

		/// <summary>
		/// Gets the ImageFormat based on the extension of the specified fileName.
		/// </summary>
		/// <param name="fileName">The file name to get the extension from.</param>
		/// <returns>
		/// The ImageFormat of the specified fileName; otherwise <value>null</value>.
		/// </returns>
		private static ImageFormat GetImageFormat(string fileName)
		{
			ImageFormat imageFormat = null;

			switch (Path.GetFileNameWithoutExtension(fileName).ToLowerInvariant())
			{
				case ".bmp":
					imageFormat = ImageFormat.Bmp;
					break;
				case ".emf":
					imageFormat = ImageFormat.Emf;
					break;
				case ".exif":
					imageFormat = ImageFormat.Exif;
					break;
				case ".gif":
					imageFormat = ImageFormat.Gif;
					break;
				case ".ico":
					imageFormat = ImageFormat.Icon;
					break;
				case ".jpeg":
				case ".jpg":
					imageFormat = ImageFormat.Jpeg;
					break;
				case ".png":
					imageFormat = ImageFormat.Png;
					break;
				case ".tiff":
					imageFormat = ImageFormat.Tiff;
					break;
				case ".wmf":
					imageFormat = ImageFormat.Wmf;
					break;
			}

			return imageFormat;
		}

		/// <summary>
		/// Save a bitmap with the image format based on the extension
		/// </summary>
		/// <param name="bmpToSave">Bitmap to be saved</param>
		/// <param name="pathToSaveTo">Path to save to</param>
		/// <returns>
		/// Always returns true.
		/// </returns>
		public static bool SaveBitmapBasedOnExtension(Bitmap bmpToSave, string pathToSaveTo)
		{
			return ImageUtil.SaveBitmapBasedOnExtension(bmpToSave, pathToSaveTo, 100L);
		}

		/// <summary>
		/// Save a bitmap with the image format based on the extension
		/// </summary>
		/// <param name="bmpToSave">Bitmap to be saved</param>
		/// <param name="pathToSaveTo">Path to save to</param>
		/// <param name="quality">Quality (0 till 100)</param>
		/// <returns>
		/// Always returns true.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">bmpToSave
		/// or
		/// pathToSaveTo</exception>
		/// <exception cref="System.ArgumentException">Value cannot be empty.;pathToSaveTo</exception>
		/// <exception cref="System.ArgumentOutOfRangeException">quality;Value must be between 0 and 100.</exception>
		/// <exception cref="TechnicalException">SaveBitmapBasedOnExtension ran with non-implemented file extension: {0}.</exception>
		public static bool SaveBitmapBasedOnExtension(Bitmap bmpToSave, string pathToSaveTo, long quality)
		{
			if (bmpToSave == null) throw new ArgumentNullException("bmpToSave");
			if (pathToSaveTo == null) throw new ArgumentNullException("pathToSaveTo");
			if (String.IsNullOrEmpty(pathToSaveTo)) throw new ArgumentException("Value cannot be empty.", "pathToSaveTo");
			if (quality < 0 || quality > 100) throw new ArgumentOutOfRangeException("quality", quality, "Value must be between 0 and 100.");

			// Get image format based on extension
			ImageFormat imageFormat = ImageUtil.GetImageFormat(pathToSaveTo);
			if (imageFormat == null)
			{
				throw new TechnicalException("SaveBitmapBasedOnExtension ran with non-implemented file extension: {0}.", Path.GetFileNameWithoutExtension(pathToSaveTo));
			}

			// Save image
			ImageUtil.SaveImage(bmpToSave, pathToSaveTo, imageFormat, quality);

			return true;
		}

		/// <summary>
		/// Saves the image.
		/// </summary>
		/// <param name="targetImage">The target image.</param>
		/// <param name="targetFilePath">The target file path.</param>
		/// <param name="targetFormat">The target format.</param>
		/// <param name="quality">The quality.</param>
		/// <exception cref="System.ArgumentNullException">targetImage
		/// or
		/// targetFormat</exception>
		/// <exception cref="System.ArgumentOutOfRangeException">quality;Value must be between 0 and 100.</exception>
		private static void SaveImage(Image targetImage, string targetFilePath, ImageFormat targetFormat, long quality = 100L)
		{
			if (targetImage == null) throw new ArgumentNullException("targetImage");
			if (targetFormat == null) throw new ArgumentNullException("targetFormat");
			if (quality < 0 || quality > 100) throw new ArgumentOutOfRangeException("quality", quality, "Value must be between 0 and 100.");

			// Save the targetBitmap in the specified targetFormat
			if (targetFormat.Guid == ImageFormat.Jpeg.Guid)
			{
				// Save JPEG with maximum quality
				ImageCodecInfo encoder = ImageUtil.GetEncoderInfo("image/jpeg");
				EncoderParameters encoderParameters = new EncoderParameters(1);
				encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);

				targetImage.Save(targetFilePath, encoder, encoderParameters);
			}
			else if (targetFormat.Guid == ImageFormat.Gif.Guid)
			{
				// Using image quantization from: http://msdn.microsoft.com/en-us/library/aa479306.aspx
				OctreeQuantizer quantizer = new OctreeQuantizer(255, 8);
				using (Bitmap quantizedBitmap = quantizer.Quantize(targetImage))
				{
					quantizedBitmap.Save(targetFilePath, ImageFormat.Gif);
				}
			}
			else
			{
				targetImage.Save(targetFilePath, targetFormat);
			}
		}

		#endregion
	}

	#region Enums

	/// <summary>
	/// Position to originate from when cropping.
	/// </summary>
	public enum ImageCropPosition
	{
		/// <summary>
		/// Top left corner.
		/// </summary>
		TopLeft,
		/// <summary>
		/// Top center.
		/// </summary>
		TopCenter,
		/// <summary>
		/// Top right corner.
		/// </summary>
		TopRight,
		/// <summary>
		/// Center left.
		/// </summary>
		CenterLeft,
		/// <summary>
		/// Center.
		/// </summary>
		Center,
		/// <summary>
		/// Center right.
		/// </summary>
		CenterRight,
		/// <summary>
		/// Bottom left corner.
		/// </summary>
		BottomLeft,
		/// <summary>
		/// Bottom center.
		/// </summary>
		BottomCenter,
		/// <summary>
		/// Bottom right corner.
		/// </summary>
		BottomRight
	}

	/// <summary>
	/// Specifies the size mode of the image.
	/// </summary>
	public enum ImageSizeMode
	{
		/// <summary>
		/// Crop or fill the image.
		/// </summary>
		Clip,
		/// <summary>
		/// Crop and resize the image.
		/// </summary>
		Zoom,
		/// <summary>
		/// Resize or fill the image.
		/// </summary>
		Squeeze,
		/// <summary>
		/// Resize the image or fill.
		/// </summary>
		Stretch
	}

	#endregion
}