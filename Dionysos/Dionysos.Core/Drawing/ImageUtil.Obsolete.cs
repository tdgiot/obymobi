﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using Dionysos.Drawing.Quantization;

namespace Dionysos.Drawing
{
	/// <summary>
	/// Image utility class.
	/// </summary>
	public partial class ImageUtil
	{
		#region Methods

		/// <summary>
		/// Draw an Image with Margins in the color of the background
		/// </summary>
		/// <param name="imageToDraw">Image to draw (will not be resized/cropped)</param>
		/// <param name="margins">Margins around the image</param>
		/// <param name="backgroundColor">Color of the background (to paint the margins with)</param>
		/// <returns>Image with Margins</returns>
		[Obsolete("Use new AddMargin method instead.")]
		public static Bitmap AddMargins(Bitmap imageToDraw, Margins margins, Color backgroundColor)
		{
			// Create the canvas
			int totalWidth = imageToDraw.Width + margins.Left + margins.Right;
			int totalHeight = imageToDraw.Height + margins.Top + margins.Bottom;
			Bitmap toReturn = new Bitmap(totalWidth, totalHeight);

			Graphics graphics = Graphics.FromImage(toReturn);
			SolidBrush brush = new SolidBrush(backgroundColor);
			graphics.FillRectangle(brush, 0, 0, toReturn.Width, toReturn.Height); // Draw backgroundcolor
			graphics.DrawImage(imageToDraw, margins.Left, margins.Top, imageToDraw.Width, imageToDraw.Height); // Draw image

			return toReturn;
		}

		/// <summary>
		/// Crops an image and saves it as a new target filename (standard: first resize, overwrite false (if it exists it's NOT resized again), croppostion center)
		/// </summary>
		/// <param name="sourcePath">The path of the source image</param>
		/// <param name="targetPath">The path of the target to save to</param>
		/// <param name="targetWidth">The new width of the image</param>
		/// <param name="targetHeight">The new height of the image</param>
		[Obsolete("Use CropAndSave method instead.")]
		public static void CropAndSaveImage(string sourcePath, string targetPath, int targetWidth, int targetHeight)
		{
			Size targetSize = new Size(targetWidth, targetHeight);
			CropAndSaveImage(sourcePath, targetPath, targetSize, null, null, false);
		}

		/// <summary>
		/// Crops an image and saves it as a new target filename (standard: first resize, overwrite false (if it exists it's NOT resized again), croppostion center)
		/// </summary>
		/// <param name="sourcePath">The path of the source image</param>
		/// <param name="targetPath">The path of the target to save to</param>
		/// <param name="targetSize">The target size of the image</param>
		/// <param name="margins">Margins</param>
		/// <param name="backgroundColor">Background color</param>
		/// <param name="fitImage">Make sure the whole image is shown</param>
		[Obsolete("Use CropAndSave method instead.")]
		public static void CropAndSaveImage(string sourcePath, string targetPath, Size targetSize, Margins margins, Color? backgroundColor, bool fitImage)
		{
			if (File.Exists(targetPath))
			{
				// do nothing it exists // WHY??? I SAID CROP AND SAVE!!!
			}
			else
			{
				Bitmap source = new Bitmap(sourcePath);

				if (source.Width == targetSize.Width &&
					source.Height == targetSize.Height)
				{
					// It's already correct size
					if (margins != null)
					{
						source = AddMargins(source, margins, backgroundColor.Value);
						SaveBitmapBasedOnExtension(source, targetPath);
					}
					else
					{
						File.Copy(sourcePath, targetPath);
					}
				}
				else
				{
					// Resize and crop the image
					if (margins != null)
					{
						source = CropImage(source, targetSize, ImageCropPosition.Center, true, fitImage);
						source = AddMargins(source, margins, backgroundColor.Value);
						SaveBitmapBasedOnExtension(source, targetPath);
					}
					else
					{
						CropImage(sourcePath, targetPath, targetSize, ImageCropPosition.Center, true);
					}
				}
			}
		}

		/// <summary>
		/// Crops an image and saves it to disk using a postfixed filename
		/// </summary>
		/// <param name="imageDirectory">The path to directory which contains the images</param>
		/// <param name="imageFileName">The filename of the file to crop</param>
		/// <param name="targetSize">The new size of the image</param>
		/// <param name="cropPosition">The cropposition of the image</param>
		/// <param name="resizeFirst">Flag which indicates whether resizing should be done first</param>
		/// <param name="overwrite">Flag which indicates if existing files should be overwritten</param>
		[Obsolete("Use CropAndSave method instead.")]
		public static void CropAndSaveImageWithPostfix(string imageDirectory, string imageFileName, Size targetSize, ImageCropPosition cropPosition, bool resizeFirst, bool overwrite)
		{
			string imageFileNameNew = string.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(imageFileName), string.Format("_{0}_{1}", targetSize.Width, targetSize.Height), Path.GetExtension(imageFileName));
			string imagePath = Path.Combine(imageDirectory, imageFileNameNew);
			bool crop = true;

			if (File.Exists(imagePath))
			{
				// File already exists, overwrite existing?
				if (!overwrite)
				{
					crop = false;
				}
			}

			if (crop)
			{
				string sourcePath = Path.Combine(imageDirectory, imageFileName);
				string destinationPath = Path.Combine(imageDirectory, imageFileNameNew).ToLower();

				// Resize and crop the image
				CropImage(sourcePath, destinationPath, targetSize, cropPosition, resizeFirst);
			}
		}

		/// <summary>
		/// Crop an image and overwrite the original with it.
		/// </summary>
		/// <param name="sourceFilePath">File to be cropped</param>
		/// <param name="targetSize">Target size</param>
		/// <param name="cropPosition">Originating point for cropping</param>
		/// <param name="resizeFirst">Resize the image before crop (make shortest side equal to the target cropped size)</param>
		[Obsolete("Use CropAndSave method instead.")]
		public static void CropImage(string sourceFilePath, Size targetSize, ImageCropPosition cropPosition, bool resizeFirst)
		{
			CropImage(sourceFilePath, sourceFilePath, targetSize, cropPosition, resizeFirst);
		}

		/// <summary>
		/// Crop an image and overwrite the original with it.
		/// </summary>
		/// <param name="sourceFilePath">File to be cropped</param>
		/// <param name="saveAsPath">File to be saved to</param>
		/// <param name="targetSize">Target size</param>
		/// <param name="cropPosition">Originating point for cropping</param>
		/// <param name="resizeFirst">Resize the image before crop (make shortest side equal to the target cropped size)</param>
		[Obsolete("Use CropAndSave method instead.")]
		public static void CropImage(string sourceFilePath, string saveAsPath, Size targetSize, ImageCropPosition cropPosition, bool resizeFirst)
		{
			if (File.Exists(sourceFilePath))
			{
				Bitmap source = new Bitmap(sourceFilePath);
				Bitmap target = CropImage(source, targetSize, cropPosition, resizeFirst);
				source.Dispose();

				bool saved = false;
				ImageFormat format = ImageFormat.Jpeg;
				if (sourceFilePath.ToLower().Contains(".jpg"))
				{
					/*
					EncoderParameters eps = new EncoderParameters(1);
					eps.Param[0] = new EncoderParameter(Encoder.Quality, 60);
					ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
					target.Save(saveAsPath, ici, eps);
					*/
				}
				else if (sourceFilePath.ToLower().Contains(".gif"))
				{
					format = ImageFormat.Gif;
				}
				else if (sourceFilePath.ToLower().Contains(".png"))
				{
					format = ImageFormat.Png;
				}
				else if (sourceFilePath.ToLower().Contains(".tif"))
				{
					format = ImageFormat.Tiff;
				}

				if (!saved)
					target.Save(saveAsPath, format);

				target.Dispose();
			}
		}

		/// <summary>
		/// Crop an image and overwrite the original with it.
		/// </summary>
		/// <param name="bmpSource">Src</param>
		/// <param name="targetSize">Target size</param>
		/// <param name="cropPosition">Originating point for cropping</param>
		/// <param name="resizeFirst">Resize the image before crop (make shortest side equal to the target cropped size)</param>
		/// <returns></returns>
		[Obsolete("Use CropAndSave method instead.")]
		public static Bitmap CropImage(Bitmap bmpSource, Size targetSize, ImageCropPosition cropPosition, bool resizeFirst)
		{
			return CropImage(bmpSource, targetSize, cropPosition, resizeFirst, false);
		}

		/// <summary>
		/// Crop an image and overwrite the original with it.
		/// </summary>
		/// <param name="bmpSource">Src</param>
		/// <param name="targetSize">Target size</param>
		/// <param name="cropPosition">Originating point for cropping</param>
		/// <param name="resizeFirst">Resize the image before crop (make shortest side equal to the target cropped size)</param>
		/// <param name="fitImage">Make sure the whole image is shown</param>
		/// <returns></returns>
		[Obsolete("Use CropAndSave method instead.")]
		public static Bitmap CropImage(Bitmap bmpSource, Size targetSize, ImageCropPosition cropPosition, bool resizeFirst, bool fitImage)
		{
			System.Drawing.Bitmap bmpTarget = new System.Drawing.Bitmap(targetSize.Width, targetSize.Height);
			Graphics g = Graphics.FromImage(bmpTarget);
			g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
			g.FillRectangle(Brushes.White, 0, 0, targetSize.Width, targetSize.Height); // fill with white

			// Because we get 1 pixel JPG articfacts on the edges we expand 2 pixel 1 for each side
			targetSize.Width++;
			targetSize.Height++;
			targetSize.Width++;
			targetSize.Height++;

			// First resize if needed
			if (resizeFirst)
			{
				// Default resize on the shortest side, but for fitImage on longest
				bool resized = false;
				if (fitImage)
				{
					// First check on longest side
					if (bmpSource.Width > bmpSource.Height)
					{
						decimal ratio = (decimal)targetSize.Width / bmpSource.Width;
						decimal temp = bmpSource.Height * ratio;
						int newHeight = (int)temp;
						if (newHeight <= targetSize.Height)
						{
							bmpSource = ResizeImage(bmpSource, new Size(targetSize.Width, newHeight));
							resized = true;
						}
					}

					if (!resized)
					{
						decimal ratio = (decimal)targetSize.Height / bmpSource.Height;
						decimal temp = bmpSource.Width * ratio;
						int NewWidth = (int)temp;
						bmpSource = ResizeImage(bmpSource, new Size(NewWidth, targetSize.Height));
						resized = true;
					}
				}
				else
				{
					if (bmpSource.Width > bmpSource.Height)
					{
						decimal ratio = (decimal)targetSize.Height / bmpSource.Height;
						decimal temp = bmpSource.Width * ratio;
						int NewWidth = (int)temp;
						if (NewWidth >= targetSize.Width)
						{
							bmpSource = ResizeImage(bmpSource, new Size(NewWidth, targetSize.Height));
							resized = true;
						}
					}

					if (!resized)
					{
						decimal ratio = (decimal)targetSize.Width / bmpSource.Width;
						decimal temp = bmpSource.Height * ratio;
						int NewHeight = (int)temp;
						bmpSource = ResizeImage(bmpSource, new Size(targetSize.Width, NewHeight));
					}
				}
			}

			int startPositionX = 0;
			int startPositionY = 0;

			switch (cropPosition)
			{
				case ImageCropPosition.Center:
					startPositionX = Convert.ToInt32(System.Math.Round((decimal)(((bmpSource.Width - targetSize.Width) / 2) * -1), 0));
					startPositionY = Convert.ToInt32(System.Math.Round((decimal)(((bmpSource.Height - targetSize.Height) / 2) * -1), 0));
					break;
				default:
					throw new NotImplementedException("The specified ImageCropPosition is not implemented.");
			}

			if (bmpSource.Width != targetSize.Width || bmpSource.Height != targetSize.Height)
			{
				startPositionX--;
				startPositionY--;

				g.DrawImage(bmpSource, startPositionX, startPositionY, bmpSource.Width, bmpSource.Height);
				return bmpTarget;
			}
			else
			{
				return bmpSource;
			}
		}

		/// <summary>
		/// Convert a color image to black and white
		/// </summary>
		/// <param name="orgImage">orgImage</param>
		/// /// <param name="desaturateMethod">method</param>
		/// <returns>Desaturated image</returns>
		[Obsolete("Unused method.")]
		public static Bitmap DesaturateImage(Bitmap orgImage, DesaturateMethod desaturateMethod)
		{
			//Image img = Image.FromFile(dlg.FileName);
			//Bitmap bm = new Bitmap(img.Width,img.Height);
			Bitmap bm = new Bitmap(orgImage);

			Graphics g = Graphics.FromImage(bm);
			ColorMatrix cm;
			ImageAttributes ia;
			switch (desaturateMethod)
			{
				case DesaturateMethod.Simple:
					for (int y = 0; y < bm.Height; y++)
					{
						for (int x = 0; x < bm.Width; x++)
						{
							Color c = orgImage.GetPixel(x, y);
							int luma = (int)(c.R * 0.3 + c.G * 0.59 + c.B * 0.11);
							bm.SetPixel(x, y, Color.FromArgb(luma, luma, luma));
						}
					}
					break;
				case DesaturateMethod.ColorMatrixSimple:
					cm = new ColorMatrix(new float[][]{   new float[]{0.5f,0.5f,0.5f,0,0},
								  new float[]{0.5f,0.5f,0.5f,0,0},
								  new float[]{0.5f,0.5f,0.5f,0,0},
								  new float[]{0,0,0,1,0,0},
								  new float[]{0,0,0,0,1,0},
								  new float[]{0,0,0,0,0,1}});

					ia = new ImageAttributes();
					ia.SetColorMatrix(cm);
					g.DrawImage(bm, new Rectangle(0, 0, bm.Width, bm.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel, ia);
					g.Dispose();
					break;
				case DesaturateMethod.ColorMatrixAdvanced:
					cm = new ColorMatrix(new float[][]{   new float[]{0.3f,0.3f,0.3f,0,0},
									  new float[]{0.59f,0.59f,0.59f,0,0},
									  new float[]{0.11f,0.11f,0.11f,0,0},
									  new float[]{0,0,0,1,0,0},
									  new float[]{0,0,0,0,1,0},
									  new float[]{0,0,0,0,0,1}});

					ia = new ImageAttributes();
					ia.SetColorMatrix(cm);
					g.DrawImage(bm, new Rectangle(0, 0, bm.Width, bm.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel, ia);
					g.Dispose();
					break;
				default:
					break;
			}

			return bm;
		}

		/// <summary>
		/// Resize and save an image
		/// </summary>
		/// <param name="sourcePath">Source</param>
		/// <param name="targetPath">Path to save to</param>
		/// <param name="width">Width</param>
		/// <param name="height">Height</param>
		/// <returns>boolean for succes</returns>
		[Obsolete("Use Resize or ResizeAndSave method instead.")]
		public static bool ResizeImage(string sourcePath, string targetPath, int width, int height)
		{
			Size target = new Size(width, height);

			return ResizeImage(sourcePath, targetPath, target);
		}

		/// <summary>
		/// Resize
		/// </summary>
		/// <param name="sourcePath">Source File</param>
		/// <param name="targetPath">Path to Save to</param>
		/// <param name="targetSize">new size</param>
		/// <returns>If it was succesfull</returns>
		[Obsolete("Use Resize or ResizeAndSave method instead.")]
		public static bool ResizeImage(string sourcePath, string targetPath, Size targetSize)
		{
			return ResizeImage(sourcePath, targetPath, targetSize, null, null);
		}

		/// <summary>
		/// Resize
		/// </summary>
		/// <param name="sourcePath">Source File</param>
		/// <param name="targetPath">Path to Save to</param>
		/// <param name="targetSize">new size</param>
		/// <param name="margins">Margins around the image, are added to the targetSize</param>
		/// <param name="backgroundColor">backgroundColor</param>
		/// <returns>If it was succesfull</returns>
		[Obsolete("Use Resize or ResizeAndSave method instead.")]
		public static bool ResizeImage(string sourcePath, string targetPath, Size targetSize, Margins margins, Color? backgroundColor)
		{
			bool succes = false;
			Bitmap source = null;
			Bitmap target = null;

			try
			{
				// If the source image has the correct sizes, do a copy instead of new save
				source = new Bitmap(sourcePath);

				if (source.Width == targetSize.Width && source.Height == targetSize.Height ||
					targetSize.Width == 0 && source.Height == targetSize.Height ||
					source.Width == targetSize.Width && targetSize.Height == 0)
				{
					// It's already correct size
					// RB: Both width and height, or only width or height
					if (margins != null)
					{
						// Add margins
						target = ImageUtil.AddMargins(source, margins, backgroundColor.Value);

						// Dispose of source, so overwriting works
						source.Dispose();

						SaveBitmapBasedOnExtension(target, targetPath);
						succes = true;

						target.Dispose();
					}
					else
					{
						File.Copy(sourcePath, targetPath);
						succes = true;
					}
				}
				else
				{
					target = ResizeImage(source, targetSize);

					if (margins != null)
					{
						target = AddMargins(target, margins, backgroundColor.Value);
					}

					// Dispose of source, so overwriting works
					source.Dispose();

					SaveBitmapBasedOnExtension(target, targetPath);
					succes = true;

					target.Dispose();
				}
			}
			catch (Exception ex)
			{
				succes = false;
				throw new Dionysos.TechnicalException("Resize failed: {0}", ex.Message);
			}
			finally
			{
				if (source != null)
					source.Dispose();
				if (target != null)
					target.Dispose();
			}
			return succes;
		}

		/// <summary>
		/// Resize
		/// </summary>
		/// <param name="bmpSource">src</param>
		/// <param name="percentage">trgt</param>
		/// <returns>ret</returns>
		[Obsolete("Use Resize or ResizeAndSave method instead.")]
		public static Bitmap ResizeImage(Bitmap bmpSource, decimal percentage)
		{
			Size targetSize = new Size();
			targetSize.Width = Convert.ToInt32(bmpSource.Width * (percentage / 100));
			targetSize.Height = Convert.ToInt32(bmpSource.Height * (percentage / 100));
			return ResizeImage(bmpSource, targetSize);
		}

		/// <summary>
		/// Resizes the specified image to the specified target size
		/// </summary>
		/// <param name="imageFile">The bytes of the image to resize</param>
		/// <param name="maximalWidth">The maximum width of the file</param>
		/// <param name="maximalHeight">The maximum height of the file</param>
		/// <param name="fitInDimensions">If set to <c>true</c> fits the image in the specified dimensions.</param>
		/// <param name="jpgQuality">The JPG quality.</param>
		/// <returns>
		/// An array of Bytes containing the resized image
		/// </returns>
		[Obsolete("Use Resize or ResizeAndSave method instead.")]
		public static byte[] ResizeImageFile(byte[] imageFile, int maximalWidth, int maximalHeight, bool fitInDimensions = false, int jpgQuality = 80) // 80 because it makes a huge leap from 100 to 80 (ca. 75%), then it's 20% for each 10 quality.
		{
			System.Drawing.Image original = System.Drawing.Image.FromStream(new MemoryStream(imageFile));
			int targetH, targetW;

			if (maximalHeight == 0)
				maximalHeight = original.Height * (maximalWidth / original.Width);

            if(maximalWidth == 0)
                maximalWidth = original.Width * (maximalHeight / original.Height);

			if (fitInDimensions)
			{
				targetW = maximalWidth;
				targetH = maximalHeight;

				Decimal targetRatio = Decimal.Divide(maximalWidth, maximalHeight);
				Decimal sourceRatio = Decimal.Divide(original.Width, original.Height);
				// Target 300 x 400 = W/H .75
				// 500 x 250 = W / H = 2
				// 250 x 500 = W / H = .5

				if (targetRatio > sourceRatio)
				{
					// Target is wider than source, use height as limiting factor
					targetH = maximalHeight;
					targetW = Convert.ToInt32(original.Width * Decimal.Divide(maximalHeight, original.Height));
				}
				else
				{
					// Target is higher than source, use width as limiting factor.
					targetW = maximalWidth;
					targetH = Convert.ToInt32(original.Height * Decimal.Divide(maximalWidth, original.Width));
				}
			}
			else
			{
				if (original.Height < original.Width) // landscape
				{
					targetH = maximalHeight;
					targetW = (int)(original.Width * ((float)maximalHeight / (float)original.Height));
					if (targetW < maximalWidth)
					{
						targetW = maximalWidth;
						targetH = (int)(original.Height * ((float)maximalWidth / (float)original.Width));
					}
				}
				else // portrait
				{
					targetW = maximalWidth;
					targetH = (int)(original.Height * ((float)maximalWidth / (float)original.Width));
					if (targetH < maximalHeight)
					{
						targetH = maximalHeight;
						targetW = (int)(original.Width * ((float)maximalHeight / (float)original.Height));
					}
				}
			}

			System.Drawing.Image imgPhoto = System.Drawing.Image.FromStream(new MemoryStream(imageFile));

			// Create a new blank canvas.  The resized image will be drawn on this canvas.
			System.Drawing.Bitmap bmPhoto = new System.Drawing.Bitmap(targetW, targetH, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			bmPhoto.SetResolution(72, 72);

			System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto);
			grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
			grPhoto.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
			grPhoto.DrawImage(imgPhoto, new System.Drawing.Rectangle(0, 0, targetW, targetH), 0, 0, original.Width, original.Height, System.Drawing.GraphicsUnit.Pixel);

			// Save out to memory and then to a file.  We dispose of all objects to make sure the files don't stay locked.
			ImageCodecInfo encoder = GetEncoderInfo("image/jpeg");
			EncoderParameters encoderParameters = new EncoderParameters(1);
			encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, jpgQuality);

			MemoryStream mm = new MemoryStream();
			bmPhoto.Save(mm, encoder, encoderParameters);
			original.Dispose();
			imgPhoto.Dispose();
			bmPhoto.Dispose();
			grPhoto.Dispose();

			return mm.GetBuffer();
		}

		/// <summary>
		/// Resizes the image file.
		/// </summary>
		/// <param name="sourceBitmap">The source bitmap.</param>
		/// <param name="cropArea">The crop area.</param>
		/// <param name="targetSize">Size of the target.</param>
		/// <param name="targetFormat">The target format.</param>
		/// <returns></returns>
		[Obsolete("Use Resize or ResizeAndSave method instead.")]
		public static byte[] ResizeImageFile(Image sourceBitmap, Rectangle cropArea, Size targetSize, ImageFormat targetFormat)
		{
			return ResizeImageFile(sourceBitmap, cropArea, targetSize, targetFormat, 100);
		}

		/// <summary>
		/// Crops and saves the sourceFilePath to the specified targetFilePath in the specified ImageFormat using the crop area and target size.
		/// </summary>
		/// <param name="sourceBitmap">The source image.</param>
		/// <param name="cropArea">The area to copy to the target image.</param>
		/// <param name="targetSize">The size of the target image.</param>
		/// <param name="targetFormat">The image format of the target image; if NULL, uses the same format as the source image.</param>
		/// <param name="jpgQuality">The JPG quality.</param>
		/// <returns></returns>
		[Obsolete("Use Resize or ResizeAndSave method instead.")]
		public static byte[] ResizeImageFile(Image sourceBitmap, Rectangle cropArea, Size targetSize, ImageFormat targetFormat, int jpgQuality)
		{
			byte[] targetImage;

			// If targetFormat is not set, get if from the targetFilePath or sourceBitmap
			if (targetFormat == null)
			{
				targetFormat = sourceBitmap.RawFormat;
			}

            // Save JPEG with maximum quality
            ImageCodecInfo encoder = null;
		    EncoderParameters encoderParameters = null;
		    if (targetFormat.Equals(ImageFormat.Jpeg))
		    {
		        encoder = GetEncoderInfo("image/jpeg");
		        encoderParameters = new EncoderParameters(1);
		        encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, jpgQuality);
		    }

		    using (Bitmap targetBitmap = new Bitmap(targetSize.Width, targetSize.Height))
			{
				// Crop and save the image
			    using (Graphics g = Graphics.FromImage(targetBitmap))
			    {
			        // Set quality parameters
			        g.SmoothingMode = SmoothingMode.HighQuality;
			        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
			        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
			        g.CompositingQuality = CompositingQuality.HighQuality;

			        // Fill with white
			        if (targetFormat.Guid != ImageFormat.Png.Guid)
			            g.Clear(Color.White);

			        if (sourceBitmap.Size.Equals(targetBitmap.Size) && sourceBitmap.Size.Equals(cropArea.Size) && cropArea.Location.IsEmpty)
			        {
			            g.DrawImage(sourceBitmap, new Rectangle(0, 0, targetSize.Width, targetSize.Height), cropArea, GraphicsUnit.Pixel);
			        }
			        else
			        {
			            // Draw cropped Bitmap to targetBitmap (with 1px overlap to remove jaggy edges)
			            g.DrawImage(sourceBitmap, new Rectangle(-1, -1, targetSize.Width + 2, targetSize.Height + 2), cropArea, GraphicsUnit.Pixel);
			        }

			        // Save the targetBitmap in the specified targetFormat
			        if (targetFormat.Equals(ImageFormat.Jpeg))
			        {
			            using (MemoryStream memoryStream = new MemoryStream())
			            {
			                if (encoder != null)
			                    targetBitmap.Save(memoryStream, encoder, encoderParameters);
			                else
			                    targetBitmap.Save(memoryStream, targetFormat);

			                targetImage = memoryStream.ToArray();
			            }
			        }
			        else if (targetFormat.Equals(ImageFormat.Gif))
			        {
			            // Using image quantization from: http://msdn.microsoft.com/en-us/library/aa479306.aspx
			            OctreeQuantizer quantizer = new OctreeQuantizer(255, 8);

			            using (Bitmap quantizedBitmap = quantizer.Quantize(targetBitmap))
			            using (MemoryStream memoryStream = new MemoryStream())
			            {
			                quantizedBitmap.Save(memoryStream, ImageFormat.Gif);
			                targetImage = memoryStream.ToArray();
			            }
			        }
			        else
			        {
			            using (MemoryStream memoryStream = new MemoryStream())
			            {
			                targetBitmap.Save(memoryStream, targetFormat);
			                targetImage = memoryStream.ToArray();
			            }
			        }
			    }
			}

			return targetImage;
		}

		/// <summary>
		/// Creates a new Image containing the same image only rotated
		/// </summary>
		/// <param name="image">The <see cref="System.Drawing.Image" /> to rotate</param>
		/// <param name="angle">The amount to rotate the image, clockwise, in degrees</param>
		/// <returns>
		/// A new <see cref="System.Drawing.Bitmap" /> that is just large enough
		/// to contain the rotated image without cutting any corners off.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">Thrown if <see cref="System.Drawing.Image" /> is null.</exception>
		[Obsolete("Unused method.")]
		public static Bitmap RotateImage(System.Drawing.Image image, float angle)
		{
			if (image == null)
				throw new ArgumentNullException("image");

			const double pi2 = System.Math.PI / 2.0;

			// Why can't C# allow these to be const, or at least readonly
			// *sigh*  I'm starting to talk like Christian Graus :omg:
			double oldWidth = (double)image.Width;
			double oldHeight = (double)image.Height;

			// Convert degrees to radians
			double theta = ((double)angle) * System.Math.PI / 180.0;
			double locked_theta = theta;

			// Ensure theta is now [0, 2pi)
			while (locked_theta < 0.0)
				locked_theta += 2 * System.Math.PI;

			double newWidth, newHeight;
			int nWidth, nHeight; // The newWidth/newHeight expressed as ints

			#region Explaination of the calculations

			/*
			 * The trig involved in calculating the new width and height
			 * is fairly simple; the hard part was remembering that when
			 * PI/2 <= theta <= PI and 3PI/2 <= theta < 2PI the width and
			 * height are switched.
			 *
			 * When you rotate a rectangle, r, the bounding box surrounding r
			 * contains for right-triangles of empty space.  Each of the
			 * triangles hypotenuse's are a known length, either the width or
			 * the height of r.  Because we know the length of the hypotenuse
			 * and we have a known angle of rotation, we can use the trig
			 * function identities to find the length of the other two sides.
			 *
			 * sine = opposite/hypotenuse
			 * cosine = adjacent/hypotenuse
			 *
			 * solving for the unknown we get
			 *
			 * opposite = sine * hypotenuse
			 * adjacent = cosine * hypotenuse
			 *
			 * Another interesting point about these triangles is that there
			 * are only two different triangles. The proof for which is easy
			 * to see, but its been too long since I've written a proof that
			 * I can't explain it well enough to want to publish it.
			 *
			 * Just trust me when I say the triangles formed by the lengths
			 * width are always the same (for a given theta) and the same
			 * goes for the height of r.
			 *
			 * Rather than associate the opposite/adjacent sides with the
			 * width and height of the original bitmap, I'll associate them
			 * based on their position.
			 *
			 * adjacent/oppositeTop will refer to the triangles making up the
			 * upper right and lower left corners
			 *
			 * adjacent/oppositeBottom will refer to the triangles making up
			 * the upper left and lower right corners
			 *
			 * The names are based on the right side corners, because thats
			 * where I did my work on paper (the right side).
			 *
			 * Now if you draw this out, you will see that the width of the
			 * bounding box is calculated by adding together adjacentTop and
			 * oppositeBottom while the height is calculate by adding
			 * together adjacentBottom and oppositeTop.
			 */

			#endregion Explaination of the calculations

			double adjacentTop, oppositeTop;
			double adjacentBottom, oppositeBottom;

			// We need to calculate the sides of the triangles based
			// on how much rotation is being done to the bitmap.
			//   Refer to the first paragraph in the explaination above for
			//   reasons why.
			if ((locked_theta >= 0.0 && locked_theta < pi2) ||
				(locked_theta >= System.Math.PI && locked_theta < (System.Math.PI + pi2)))
			{
				adjacentTop = System.Math.Abs(System.Math.Cos(locked_theta)) * oldWidth;
				oppositeTop = System.Math.Abs(System.Math.Sin(locked_theta)) * oldWidth;

				adjacentBottom = System.Math.Abs(System.Math.Cos(locked_theta)) * oldHeight;
				oppositeBottom = System.Math.Abs(System.Math.Sin(locked_theta)) * oldHeight;
			}
			else
			{
				adjacentTop = System.Math.Abs(System.Math.Sin(locked_theta)) * oldHeight;
				oppositeTop = System.Math.Abs(System.Math.Cos(locked_theta)) * oldHeight;

				adjacentBottom = System.Math.Abs(System.Math.Sin(locked_theta)) * oldWidth;
				oppositeBottom = System.Math.Abs(System.Math.Cos(locked_theta)) * oldWidth;
			}

			newWidth = adjacentTop + oppositeBottom;
			newHeight = adjacentBottom + oppositeTop;

			nWidth = (int)System.Math.Ceiling(newWidth);
			nHeight = (int)System.Math.Ceiling(newHeight);

			//Bitmap rotatedBmp = new Bitmap(nWidth, nHeight);
			Bitmap rotatedBmp = new Bitmap(962, 818);

			RotateImageWithinCanvas(image, angle, true, rotatedBmp);

			//using (Graphics g = Graphics.FromImage(rotatedBmp))
			//{
			//    // This array will be used to pass in the three points that
			//    // make up the rotated image
			//    Point[] points;

			//    /*
			//     * The values of opposite/adjacentTop/Bottom are referring to
			//     * fixed locations instead of in relation to the
			//     * rotating image so I need to change which values are used
			//     * based on the how much the image is rotating.
			//     *
			//     * For each point, one of the coordinates will always be 0,
			//     * nWidth, or nHeight.  This because the Bitmap we are drawing on
			//     * is the bounding box for the rotated bitmap.  If both of the
			//     * corrdinates for any of the given points wasn't in the set above
			//     * then the bitmap we are drawing on WOULDN'T be the bounding box
			//     * as required.
			//     */
			//    if (locked_theta >= 0.0 && locked_theta < pi2)
			//    {
			//        points = new Point[] {
			//                                 new Point( (int) oppositeBottom, 0 ),
			//                                 new Point( nWidth, (int) oppositeTop ),
			//                                 new Point( 0, (int) adjacentBottom )
			//                             };

			//    }
			//    else if (locked_theta >= pi2 && locked_theta < System.Math.PI)
			//    {
			//        points = new Point[] {
			//                                 new Point( nWidth, (int) oppositeTop ),
			//                                 new Point( (int) adjacentTop, nHeight ),
			//                                 new Point( (int) oppositeBottom, 0 )
			//                             };
			//    }
			//    else if (locked_theta >= System.Math.PI && locked_theta < (System.Math.PI + pi2))
			//    {
			//        points = new Point[] {
			//                                 new Point( (int) adjacentTop, nHeight ),
			//                                 new Point( 0, (int) adjacentBottom ),
			//                                 new Point( nWidth, (int) oppositeTop )
			//                             };
			//    }
			//    else
			//    {
			//        points = new Point[] {
			//                                 new Point( 0, (int) adjacentBottom ),
			//                                 new Point( (int) oppositeBottom, 0 ),
			//                                 new Point( (int) adjacentTop, nHeight )
			//                             };
			//    }

			//    g.DrawImage(image, points);
			//}

			return rotatedBmp;
		}

		/// <summary>
		/// Rotate an Image
		/// </summary>
		/// <param name="bmpToRotate">Image to be rotated</param>
		/// <param name="angle">Angle to rotate image on</param>
		/// <param name="rotateOnCenter">Specify if image should rotate around it's center or upper-left corner</param>
		/// <param name="outputSize">Output size</param>
		/// <returns></returns>
		[Obsolete("Unused method.")]
		public static Bitmap RotateImageWithinCanvas(System.Drawing.Image bmpToRotate, float angle, bool rotateOnCenter, Bitmap outputSize)
		{
			//create a new empty bitmap to hold rotated image
			Bitmap returnBitmap = new Bitmap(bmpToRotate.Width * 10, bmpToRotate.Height * 10);

			//make a graphics object from the empty bitmap
			Graphics g = Graphics.FromImage(outputSize);

			//move rotation point to center of image
			if (rotateOnCenter)
			{
				//g.TranslateTransform((float)bmpToRotate.Width / 2, (float)bmpToRotate.Height / 2);
				g.TranslateTransform((float)481, (float)409);
			}

			//rotate
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			g.RotateTransform(angle);

			g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

			//move image back
			if (rotateOnCenter)
				g.TranslateTransform(-(float)bmpToRotate.Width / 2, -(float)bmpToRotate.Height / 2);

			//draw passed in image onto graphics object
			g.DrawImage(bmpToRotate, new Point(0, -50));
			return returnBitmap;
		}

		#endregion
	}

	#region Enums

	/// <summary>
	/// Method for desaturation
	/// </summary>
	public enum DesaturateMethod
	{
		/// <summary>
		/// Straight calculation like: Y=0.3RED+0.59GREEN+0.11Blue
		/// </summary>
		Simple,
		/// <summary>
		/// Calculated on a Matrix with no proper Lumniance
		/// </summary>
		ColorMatrixSimple,
		/// <summary>
		/// Calculated on a Matrix with proper Lumniance
		/// </summary>
		ColorMatrixAdvanced
	}

	#endregion
}