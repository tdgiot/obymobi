﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Globalization
{
	/// <summary>
	/// Interface for Translation Providers
	/// </summary>
	public interface ITranslationProvider
	{
		/// <summary>
		/// Gets the translation for a specific translationKey (can be cryptic, or just the string) in the language of the CurrentCulture
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <returns>Translation for the</returns>
		string GetTranslation(string translationKey, bool addIfNotFound);

		/// <summary>
		/// Gets the translation for a specific translationKey in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <returns>Translation for the</returns>
		string GetTranslation(string translationKey, string languageCode, bool addIfNotFound);

		/// <summary>
		/// Gets the translation for a specific translationKey in the language of the current Culture
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <returns>Translation for the</returns>
		string GetTranslation(string translationKey, string translationValue);

		/// <summary>
		/// Gets the translation for a specific translationKey in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <returns>Translation for the</returns>
		string GetTranslation(string translationKey, string languageCode, string translationValue);

		/// <summary>
		/// Gets the translation for a specific translationKey in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <returns>Translation for the</returns>
		string GetTranslation(string translationKey, string languageCode, string translationValue, bool addIfNotFound);


		/// <summary>
		/// Gets (/ sets if not found) the translation for a specific translatable in the language of the CurrentCulture
		/// </summary>
		/// <param name="translatable">The Translatable</param>
		/// <returns>
		/// Translation for the
		/// </returns>
		string GetTranslation(Translatable translatable);

		/// <summary>
		/// Gets (/ sets if not found) the translation for a specific translatable in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translatable">The Translatable</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>        
		/// <returns>Translation for the</returns>
		string GetTranslation(Translatable translatable, string languageCode);

		/// <summary>
		/// Sets the translation for a specific translationKey in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <returns>If action succeeded</returns>
		bool SetTranslation(string translationKey, string languageCode, string translationValue);

		/// <summary>
		/// Sets the translation for a specific translationKey in the language of the current Culture
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>        
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <returns>If action succeeded</returns>
		bool SetTranslation(string translationKey, string translationValue);


		/// <summary>
		/// Sets the translation for a specific translatable in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translatable">The Translatable</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>        
		/// <returns>If action succeeded</returns>
		bool SetTranslation(Translatable translatable, string languageCode);

		/// <summary>
		/// Sets the translation for a specific translatable in the language of the current Culture
		/// </summary>
		/// <param name="translatable">The Translatable</param>        
		/// <returns>If action succeeded</returns>
		bool SetTranslation(Translatable translatable);

		/// <summary>
		/// Fetches all translations from the persistent storage for the supplied translationKeys in to the cache in the Current Culture
		/// </summary>
		/// <param name="translationKeys">Keys to prefetch</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <returns>Translation for the</returns>
		void FetchTranslationsToCache(List<string> translationKeys, bool addIfNotFound);

		/// <summary>
		/// Fetches all translations from the persistent storage for the supplied translationKeys in to the cache in the supplied language code
		/// </summary>
		/// <param name="translationKeys">Keys to prefetch</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <returns>Translation for the</returns>
		void FetchTranslationsToCache(List<string> translationKeys, string languageCode, bool addIfNotFound);


		/// <summary>
		/// Fetches all translations from the persistent storage for the supplied translatables in to the cache in the Current Culture
		/// </summary>
		/// <param name="translatables">The translatables.</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		void FetchTranslationsToCache(List<Translatable> translatables, bool addIfNotFound);

		/// <summary>
		/// Fetches all translations from the persistent storage for the supplied translatables in to the cache in the supplied language code
		/// </summary>
		/// <param name="translatables">The translatables.</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		void FetchTranslationsToCache(List<Translatable> translatables, string languageCode, bool addIfNotFound);

		/// <summary>
		/// Specify if caching should be used.
		/// </summary>
		bool UseCaching { get; set; }

	}
}
