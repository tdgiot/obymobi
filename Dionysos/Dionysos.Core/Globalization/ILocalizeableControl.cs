﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace Dionysos.Globalization
{
	/// <summary>
	/// Interface for controls that can be localized
	/// </summary>
	public interface ILocalizableControl
	{
		/// <summary>
		/// Localization information per property
		/// </summary>
		Dictionary<string, Translatable> TranslatableProperties
		{
			get;
			set;
		}
	}
}
