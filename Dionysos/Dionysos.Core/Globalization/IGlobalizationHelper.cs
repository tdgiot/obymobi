﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Dionysos.Globalization
{
	/// <summary>
	/// Interface for globalization helpers.
	/// </summary>
	public interface IGlobalizationHelper
	{
		/// <summary>
		/// Localizes the controls.
		/// </summary>
		/// <param name="page">The page.</param>
        // GK TEMP!
		void LocalizeControls(Page page);

		/// <summary>
		/// Gets or sets a value indicating whether to automatically learn new controls.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if automatically learning new controls is enabled; otherwise, <c>false</c>.
		/// </value>
		bool AutoLearnNewControls { get; set; }

		/// <summary>
		/// Gets a value indicating whether a translation is required.
		/// </summary>
		/// <value>
		///   <c>true</c> if a translation is required; otherwise, <c>false</c>.
		/// </value>
		bool TranslationRequired { get; }
	}
}
