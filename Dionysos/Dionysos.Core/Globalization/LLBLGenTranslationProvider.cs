﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Web.Caching;
using System.Web;

namespace Dionysos.Globalization
{
	/// <summary>
	/// Implementation of the ITranslationProvider for LLBLGen data
	/// </summary>
	public class LLBLGenTranslationProvider : ITranslationProvider
	{
		#region Fields

		/// <summary>
		/// The entity name.
		/// </summary>
		private string entityName = "TranslationEntity";

		/// <summary>
		/// Indicates whether to use caching.
		/// </summary>
		private bool useCaching = true;

		/// <summary>
		/// The local cache.
		/// </summary>
		private Dictionary<string, ITranslationItem> localCache = new Dictionary<string, ITranslationItem>();

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name of the entity.
		/// </summary>
		/// <value>
		/// The name of the entity.
		/// </value>
		public string EntityName
		{
			get
			{
				return this.entityName;
			}
			set
			{
				this.entityName = value;

			}
		}

		/// <summary>
		/// Gets the current thread language code.
		/// </summary>
		/// <value>
		/// The current thread language code.
		/// </value>
		private string CurrentThreadLanguageCode
		{
			get
			{
				return System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
			}
		}

		/// <summary>
		/// Specify if caching should be used.
		/// </summary>
		public bool UseCaching
		{
			get
			{
				return this.useCaching;
			}
			set
			{
				this.useCaching = value;
			}
		}

		#endregion

		#region GetTranslation Methods

		/// <summary>
		/// Gets (/ sets if not found) the translation for a specific translatable in the language of the CurrentCulture
		/// </summary>
		/// <param name="translatable">The Translatable</param>
		/// <returns>
		/// Translation for the
		/// </returns>
		public string GetTranslation(Translatable translatable)
		{
			return this.GetTranslation(translatable.TranslationKey, this.CurrentThreadLanguageCode, translatable.TranslationValue);
		}

		/// <summary>
		/// Gets (/ sets if not found) the translation for a specific translatable in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translatable">The Translatable</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <returns>
		/// Translation for the
		/// </returns>
		public string GetTranslation(Translatable translatable, string languageCode)
		{
			return this.GetTranslation(translatable.TranslationKey, languageCode, translatable.TranslationValue);
		}

		/// <summary>
		/// Gets the translation for a specific translationKey (can be cryptic, or just the string) in the language of the CurrentCulture
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <returns>
		/// Translation for the
		/// </returns>
		public string GetTranslation(string translationKey, bool addIfNotFound)
		{
			return this.GetTranslation(translationKey, this.CurrentThreadLanguageCode, null, addIfNotFound);
		}

		/// <summary>
		/// Gets the translation for a specific translationKey in the language of the current Culture
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <returns>
		/// Translation for the
		/// </returns>
		public string GetTranslation(string translationKey, string translationValue)
		{
			return this.GetTranslation(translationKey, this.CurrentThreadLanguageCode, translationValue, false);
		}

		/// <summary>
		/// Gets the translation for a specific translationKey in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <returns>
		/// Translation for the
		/// </returns>
		public string GetTranslation(string translationKey, string languageCode, bool addIfNotFound)
		{
			return this.GetTranslation(translationKey, languageCode, null, addIfNotFound);
		}

		/// <summary>
		/// Gets the translation for a specific translationKey in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <returns>
		/// Translation for the
		/// </returns>
		public string GetTranslation(string translationKey, string languageCode, string translationValue)
		{
			return this.GetTranslation(translationKey, languageCode, translationValue, Global.GlobalizationHelper.AutoLearnNewControls);
		}

		/// <summary>
		/// Gets the translation for a specific translationKey in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <returns>
		/// Translation for the
		/// </returns>
		public string GetTranslation(string translationKey, string languageCode, string translationValue, bool addIfNotFound)
		{
			ITranslationItem translation = this.FetchTranslationEntity(translationKey, languageCode, translationValue, addIfNotFound);

			return translation.TranslationValue;
		}

		#endregion

		#region SetTranslation Methods

		/// <summary>
		/// Sets the translation for a specific translatable in the language of the current Culture
		/// </summary>
		/// <param name="translatable">The Translatable</param>
		/// <returns>
		/// If action succeeded
		/// </returns>
		public bool SetTranslation(Translatable translatable)
		{
			return this.SetTranslation(translatable.TranslationKey, this.CurrentThreadLanguageCode, translatable.TranslationValue);
		}

		/// <summary>
		/// Sets the translation for a specific translatable in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translatable">The Translatable</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <returns>
		/// If action succeeded
		/// </returns>
		public bool SetTranslation(Translatable translatable, string languageCode)
		{
			return this.SetTranslation(translatable.TranslationKey, languageCode, translatable.TranslationValue);
		}

		/// <summary>
		/// Sets the translation for a specific translationKey in the language of the current Culture
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <returns>
		/// If action succeeded
		/// </returns>
		public bool SetTranslation(string translationKey, string translationValue)
		{
			return this.SetTranslation(translationKey, this.CurrentThreadLanguageCode, translationValue);
		}

		/// <summary>
		/// Sets the translation for a specific translationKey in the language of the supplied languageCode (i.e. nl, en, fr, etc.)
		/// </summary>
		/// <param name="translationKey">The translationkey (can be cryptic, or just the text to be translated)</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="translationValue">If the item is not found it's created with the translationValue as text</param>
		/// <returns>
		/// If action succeeded
		/// </returns>
		public bool SetTranslation(string translationKey, string languageCode, string translationValue)
		{
			ITranslationItem translation = this.FetchTranslationEntity(translationKey, languageCode, translationValue, false);
			translation.TranslationValue = translationValue;

			return translation.Save();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Fetches the translation entity.
		/// </summary>
		/// <param name="translationKey">The translation key.</param>
		/// <param name="languageCode">The language code.</param>
		/// <param name="translationValue">The translation value.</param>
		/// <param name="addIfNotFound">if set to <c>true</c> [add if not found].</param>
		/// <returns></returns>
		/// <exception cref="TechnicalException">Multiple translation found for translation key '{0}' in language '{1}'.</exception>
		private ITranslationItem FetchTranslationEntity(string translationKey, string languageCode, string translationValue, bool addIfNotFound)
		{
			// First try to fetch from cache
			ITranslationItem translation = this.GetTranslationItemFromCache(translationKey, languageCode);
			if (translation == null)
			{
				IEntity translationEntityForFieldCreation = Dionysos.DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
				EntityField fieldTranslationKey = translationEntityForFieldCreation.Fields["TranslationKey"] as EntityField;
				EntityField fieldLanguageCode = translationEntityForFieldCreation.Fields["LanguageCode"] as EntityField;

				PredicateExpression filterTranslations = new PredicateExpression();
				filterTranslations.Add(fieldTranslationKey == translationKey);
				filterTranslations.Add(fieldLanguageCode == languageCode);

				IEntityCollection translations = Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as IEntityCollection;
				translations.GetMulti(filterTranslations, 2);

				if (translations.Count > 1)
				{
					throw new TechnicalException("Multiple translation found for translation key '{0}' in language '{1}'.", translationKey, languageCode);
				}
				else if (translations.Count == 1)
				{
					translation = translations[0] as ITranslationItem;
				}
				else
				{
					translation = translationEntityForFieldCreation as ITranslationItem;
					translation.TranslationKey = translationKey;
					translation.LanguageCode = languageCode;
					translation.TranslationValue = translationValue;

                    if (addIfNotFound)
                        translation.Save();
				}

                // Modified by FO: All items that are found should be cached, not just the new ones
                this.SetTranslationItemInCache(translation); 
			}

			return translation;
		}

		/// <summary>
		/// Gets the translation item from cache.
		/// </summary>
		/// <param name="translationKey">The translation key.</param>
		/// <param name="languageCode">The language code.</param>
		/// <returns></returns>
		private ITranslationItem GetTranslationItemFromCache(string translationKey, string languageCode)
		{
			if (this.UseCaching)
			{
				if (System.Web.HttpContext.Current != null &&
					System.Web.HttpContext.Current.Cache[translationKey + languageCode] != null)
				{
				    return HttpContext.Current.Cache.Get(translationKey + languageCode) as ITranslationItem;
				    //return System.Web.HttpContext.Current.Cache[translationKey + languageCode] as ITranslationItem;
				}
				else if (this.localCache != null &&
					this.localCache.ContainsKey(translationKey + languageCode))
				{
					return this.localCache[translationKey + languageCode];
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Sets the translation item in cache.
		/// </summary>
		/// <param name="translationItem">The translation item.</param>
		private void SetTranslationItemInCache(ITranslationItem translationItem)
		{
			if (this.UseCaching)
			{
				if (System.Web.HttpContext.Current != null)
				{
					System.Web.HttpContext.Current.Cache[translationItem.TranslationKey + translationItem.LanguageCode] = translationItem;
				}
				else
				{
					this.localCache[translationItem.TranslationKey + translationItem.LanguageCode] = translationItem;
				}
			}
		}

		/// <summary>
		/// Fetches all translations from the persistent storage for the supplied translatables in to the cache in the Current Culture
		/// </summary>
		/// <param name="translatables">The translatables.</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		public void FetchTranslationsToCache(List<Translatable> translatables, bool addIfNotFound)
		{
			this.FetchTranslationsToCache(translatables.Select(t => t.TranslationKey).ToList(), this.CurrentThreadLanguageCode, addIfNotFound);
		}

		/// <summary>
		/// Fetches all translations from the persistent storage for the supplied translatables in to the cache in the supplied language code
		/// </summary>
		/// <param name="translatables">The translatables.</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		public void FetchTranslationsToCache(List<Translatable> translatables, string languageCode, bool addIfNotFound)
		{
			this.FetchTranslationsToCache(translatables.Select(t => t.TranslationKey).ToList(), languageCode, addIfNotFound);
		}

		/// <summary>
		/// Fetches all translations from the persistent storage for the supplied translationKeys in to the cache in the Current Culture
		/// </summary>
		/// <param name="translationKeys">Keys to prefetch</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		public void FetchTranslationsToCache(List<string> translationKeys, bool addIfNotFound)
		{
			this.FetchTranslationsToCache(translationKeys, this.CurrentThreadLanguageCode, addIfNotFound);
		}

		/// <summary>
		/// Fetches all translations from the persistent storage for the supplied translationKeys in to the cache in the supplied language code
		/// </summary>
		/// <param name="translationKeys">Keys to prefetch</param>
		/// <param name="languageCode">Language code (i.e. nl, en, fr, etc.)</param>
		/// <param name="addIfNotFound">Add the item to the persistant storage with translationKey as the translationValue</param>
		/// <exception cref="TechnicalException">Multiple translations found for translation key '{0}' in language '{1}'.</exception>
		public void FetchTranslationsToCache(List<string> translationKeys, string languageCode, bool addIfNotFound)
		{
			IEntity translationEntityForFieldCreation = Dionysos.DataFactory.EntityFactory.GetEntity(this.EntityName) as IEntity;
			EntityField fieldTranslationKey = translationEntityForFieldCreation.Fields["TranslationKey"] as EntityField;
			EntityField fieldLanguageCode = translationEntityForFieldCreation.Fields["LanguageCode"] as EntityField;

			PredicateExpression filterTranslations = new PredicateExpression();
			filterTranslations.Add(fieldLanguageCode == languageCode);

			// filter on keys
			PredicateExpression filterTranslationKeys = new PredicateExpression();
			for (int i = 0; i < translationKeys.Count; i++)
			{
				// check if it's already in the cache, if, don't refetch
				ITranslationItem translation = this.GetTranslationItemFromCache(translationKeys[i], languageCode);
				if (translation == null)
					filterTranslationKeys.AddWithOr(fieldTranslationKey == translationKeys[i]);
			}

			// Fetch entitys and get EntityView
			IEntityCollection translations = Dionysos.DataFactory.EntityCollectionFactory.GetEntityCollection(this.EntityName) as IEntityCollection;
			translations.GetMulti(filterTranslations);
			IEntityView translationView = translations.DefaultView;

			// Loop through all supplied translationkeys and add to query results
			PredicateExpression filterOnView = new PredicateExpression();
			for (int i = 0; i < translationKeys.Count; i++)
			{
				filterOnView = new PredicateExpression();
				filterOnView.Add(fieldTranslationKey == translationKeys[i]);
				translationView.Filter = filterOnView;

				if (translationView.Count == 1)
				{
					// correct found! set in cache
					ITranslationItem item = (ITranslationItem)translationView[0];
					this.SetTranslationItemInCache(item);
				}
				else if (translationView.Count > 1)
				{
					// multiple found
					throw new TechnicalException("Multiple translations found for translation key '{0}' in language '{1}'.", translationKeys[i], languageCode);
				}
				else
				{
					// Not found, add if required
					if (addIfNotFound)
					{
						ITranslationItem itemToAdd = (ITranslationItem)translationEntityForFieldCreation;
						itemToAdd.TranslationKey = translationKeys[i];
						itemToAdd.LanguageCode = languageCode;
						itemToAdd.TranslationValue = translationKeys[i];
						itemToAdd.Save();
						this.SetTranslationItemInCache(itemToAdd);
					}
				}
			}
		}

		#endregion
	}
}

