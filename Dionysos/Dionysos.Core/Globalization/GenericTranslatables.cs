﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Globalization
{
	/// <summary>
	/// Generic translatable strings.
	/// </summary>
	public class GenericTranslatables
	{
		/// <summary>
		/// Save amd new.
		/// </summary>
		public const string SaveAndNew = "Save and New";

		/// <summary>
		/// The prefix.
		/// </summary>
		public static string prefix = "Dionysos.GenericTranslatables.";

		/// <summary>
		/// Translation for: New.
		/// </summary>
		public static Translatable New = new Translatable(prefix + "New", "Nieuw");

		/// <summary>
		/// Translation for: Save.
		/// </summary>
		public static Translatable Save = new Translatable(prefix + "Save", "Opslaan");

		/// <summary>
		/// Translation for: Generic.
		/// </summary>
		public static Translatable Generic = new Translatable(prefix + "Generic", "Algemeen");

		/// <summary>
		/// Translation for: Monday.
		/// </summary>
		public static Translatable Monday = new Translatable(prefix + "Monday", "maandag");

		/// <summary>
		/// Translation for: Tuesday.
		/// </summary>
		public static Translatable Tuesday = new Translatable(prefix + "Tuesday", "dinsdag");

		/// <summary>
		/// Translation for: Wednesday.
		/// </summary>
		public static Translatable Wednesday = new Translatable(prefix + "Wednesday", "woensdag");

		/// <summary>
		/// Translation for: Thursday.
		/// </summary>
		public static Translatable Thursday = new Translatable(prefix + "Thursday", "donderdag");

		/// <summary>
		/// Translation for: Friday.
		/// </summary>
		public static Translatable Friday = new Translatable(prefix + "Friday", "vrijdag");

		/// <summary>
		/// Translation for: Saturday.
		/// </summary>
		public static Translatable Saturday = new Translatable(prefix + "Saturday", "zaterdag");

		/// <summary>
		/// Translation for: Sunday.
		/// </summary>
		public static Translatable Sunday = new Translatable(prefix + "Sunday", "zondag");
	}
}
