﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Globalization
{
    /// <summary>
    /// Container for using Localizable Control Property information
    /// </summary>
    public class Translatable
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Translatable()
        {
        }

        /// <summary>
        /// Constructor with initialization parameters
        /// </summary>
        /// <param name="translationKey">Set to the TranslationKey property</param>
        /// <param name="translationValue">Set to the TranslationValue property</param>
        public Translatable(string translationKey, string translationValue)
        {
            this.TranslationKey = translationKey;
            this.TranslationValue = translationValue; 
        }

        /// <summary>
        /// Translation key to retrieve the translation by
        /// </summary>
        public string TranslationKey
        {
            get;
            set;
        }

        /// <summary>
        /// Translation value for the translation key
        /// </summary>
        public string TranslationValue
        {
            get;
            set;
        }
    }
}
