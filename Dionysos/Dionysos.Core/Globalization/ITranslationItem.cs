﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Globalization
{
    /// <summary>
    /// Interface for TranslationItems
    /// </summary>
    public interface ITranslationItem
    {
        /// <summary>
        /// Identifier of the TranslationItem
        /// </summary>
        string TranslationKey
        {
            get;
            set;
        }

        /// <summary>
        /// Translation value. Containing the translated text
        /// </summary>
        string TranslationValue
        {
            get;
            set;
        }


        /// <summary>
        /// Language code.
        /// </summary>
        string LanguageCode
        {
            get;
            set;
        }

        /// <summary>
        /// Save the TranslationItem
        /// </summary>
        /// <returns>Boolean indicating if the save was succefull</returns>
        bool Save();
    }
}
