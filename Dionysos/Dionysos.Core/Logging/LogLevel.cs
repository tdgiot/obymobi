﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos.Logging
{
    /// <summary>
    /// 
    /// </summary>
    public enum LogLevel : int
    {
        /// <summary>
        /// The verbose
        /// </summary>
        Verbose = 100,
        /// <summary>
        /// The debug
        /// </summary>
        Debug = 200,
        /// <summary>
        /// The information
        /// </summary>
        Information = 300,
        /// <summary>
        /// The warning
        /// </summary>
        Warning = 400,
        /// <summary>
        /// The error
        /// </summary>
        Error = 500
    }
}
