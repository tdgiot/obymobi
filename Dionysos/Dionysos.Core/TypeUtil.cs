using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace Dionysos
{
    /// <summary>
    /// Singleton class used for operations on System.Type instances
    /// </summary>
    public class TypeUtil
    {
        #region Fields

        static TypeUtil instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Type class if the instance has not been initialized yet
        /// </summary>
        public TypeUtil()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new TypeUtil();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        private void Init()
        {
        }

        /// <summary>
        /// Gets a string representation of the System.Type of an instance  
        /// </summary>
        /// <param name="instance">The instance to get the type string from</param>
        /// <returns>A string representing the System.Type</returns>
        public static string GetTypeString(object instance)
        {
            string typeString = string.Empty;

            if (!Instance.Empty(instance))
            {
                typeString = instance.GetType().ToString();
            }

            return typeString;
        }

        /// <summary>
        /// Gets a System.Type instance from the specified System.Reflection.Assembly using the specified type string
        /// </summary>
        /// <param name="typeString">The System.String representation of the type</param>
        /// <param name="assembly">The System.Reflection.Assembly instance to get the type from</param>
        /// <returns>A System.Type instance</returns>
        public static System.Type GetType(string typeString, System.Reflection.Assembly assembly)
        {
            System.Type type = null;
            
            if (Instance.ArgumentIsEmpty(typeString, "typeString"))
            {
                // Parameter 'typeString' is empty
            }
            else if (Instance.ArgumentIsEmpty(assembly, "assembly"))
            {
                // Parameter 'assembly' is empty
            }
            else
            {
                type = assembly.GetType(typeString);
            }

            return type;
        }

        /// <summary>
        /// Gets an array containing string representations of the types in the specified assembly
        /// </summary>
        /// <param name="assemblyFile">The filename of the assembly to get the type strings for</param>
        /// <returns>An System.String array containing the type strings</returns>
        public static string[] GetTypeStringsFromAssembly(string assemblyFile)
        {
            string[] typeStrings = null;

            if (assemblyFile.Length == 0)
            {
                throw new ArgumentNullException("assemblyFile");
            }
            else
            {
                Assembly assembly = Assembly.LoadFrom(assemblyFile);

                if (assembly == null)
                {
                    throw new EmptyException("assembly");
                }
                else
                {
                    System.Type[] types = assembly.GetTypes();

                    if (types == null)
                    {
                        throw new EmptyException("types");
                    }
                    else if (types.Length == 0)
                    {
                        throw new EmptyException("types");
                    }
                    else
                    {
                        typeStrings = new string[types.Length];

                        for (int i = 0; i < types.Length; i++)
                        {
                            typeStrings[i] = types[i].ToString();
                        }
                    }
                }
            }
            return typeStrings;
        }

        /// <summary>
        /// Gets an type from an assembly based on the specified assembly and type name
        /// </summary>
        /// <param name="assembly">The Assembly instance to get the type from</param>
        /// <param name="typeName">A System.String instance containing the type name</param>
        /// <returns>An System.String array containing the type strings</returns>
        public static System.Type GetTypeFromAssembly(Assembly assembly, string typeName)
        {
            System.Type type = null;

            if (assembly == null)
            {
                throw new ArgumentNullException("assembly");
            }
            else if (typeName.Length == 0)
            {
                throw new ArgumentNullException("typeName");
            }
            else
            {
                type = assembly.GetType(typeName);
            }

            return type;
        }

        /// <summary>
        /// Gets an type from an assembly based on the specified assembly and type name
        /// </summary>
        /// <param name="assemblyFileName">The Assembly instance to get the type from</param>
        /// <param name="typeName">A System.String instance containing the type name</param>
        /// <returns>An System.Type instance</returns>
        public static System.Type GetTypeFromAssembly(string assemblyFileName, string typeName)
        {
            System.Type type = null;
            
            if (assemblyFileName.Length == 0)
            {
                throw new ArgumentNullException("assemblyFileName");
            }
            else if (typeName.Length == 0)
            {
                throw new ArgumentNullException("typeName");
            }
            else if (!File.Exists(assemblyFileName))
            {
                throw new FileNotFoundException(string.Format("File {0} does not exist!", assemblyFileName));
            }
            else
            {
                Assembly assembly = Assembly.LoadFrom(assemblyFileName);

                if (assembly == null)
                {
                    throw new EmptyException("assembly");
                }
                else
                {
                    type = assembly.GetType(typeName);
                }
            }

            return type;
        }

        #endregion
    }
}