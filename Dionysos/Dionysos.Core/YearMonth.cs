﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dionysos
{
	/// <summary>
	/// Class containing a Year Month notation
	/// </summary>
	public class YearMonth : IComparable
	{

		#region Fields

		private int year;
		private int month;

		#endregion

		#region Methods
		#endregion

		#region Event Handlers

		/// <summary>
		/// Construct a new instance of YearMonth
		/// </summary>
		/// <param name="year">Year, must be a 4 digit number</param>
		/// <param name="month">Month, must be a number > 1 and &lt; 12</param>
		public YearMonth(int year, int month)
		{
			// Verification of year/month performed in properties
			this.Year = year;
			this.Month = month;
		}

		/// <summary>
		/// Compare 2 YearMonths according to standard ICompare 
		/// </summary>
		/// <param name="obj">YearMonth to compare to</param>
		/// <returns>Int</returns>
		public int CompareTo(object obj)
		{
			string s1 = this.ToString();
			string s2 = ((YearMonth)obj).ToString();

			return String.Compare(s1, s2);
		}

		/// <summary>
		/// Returns a new instance with the amount of months added
		/// </summary>
		/// <param name="months">Months to add</param>
		/// <returns></returns>
		public void AddMonths(int months)
		{
			// Lazy implementation, could be calculated in once too.
			for (int i = 0; i < months; i++)
			{
				if (this.Month == 12)
				{
					this.Year++;
					this.Month = 1;
				}
				else
				{
					this.Month++;
				}
			}
		}

		/// <summary>
		/// Clone object
		/// </summary>
		/// <returns></returns>
		public YearMonth Clone()
		{
			return new YearMonth(this.Year, this.Month);				 
		}

		/// <summary>
		/// Get a YearMonthSpan for the YearMonth of the intance of the YearMonth object
		/// </summary>
		/// <returns></returns>
		public YearMonthSpan ToYearMonthSpan()
		{
			YearMonthSpan toReturn = new YearMonthSpan(this, this);
			return toReturn;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the Month
		/// </summary>
		public int Month
		{
			get
			{
				return this.month;
			}
			set
			{
				if (value < 1 || value > 12)
					throw new Dionysos.FunctionalException("'Maand' moet tussen de 1 en 12 liggen, fout voor: '{0}'", value);
				this.month = value;
			}
		}

		/// <summary>
		/// Gets or sets the Year
		/// </summary>
		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				if (value.ToString().Length != 4)
					throw new Dionysos.FunctionalException("'Jaar' moeten 4 cijfers zijn, fout voor: '{0}'", value);
				this.year = value;
			}
		}

		/// <summary>
		/// Convert to String (Year 4 digits, month 2 digits)
		/// </summary>
		public new string ToString()
		{
			return string.Format("{0}{1}", this.Year.ToString(), this.Month.ToString().PadLeft(2, '0'));
		}

		#endregion


	}
}

