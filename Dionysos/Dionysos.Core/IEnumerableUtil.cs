﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Security.Cryptography;

namespace Dionysos
{
	/// <summary>
	/// Class providing extension methods for IEnumerables.
	/// </summary>
	public static class IEnumerableUtil
	{
		#region TakeIf

		/// <summary>
		/// Returns a specified number of contiguous elements from the start of a sequence if the predicate is true.
		/// </summary>
		/// <typeparam name="T">The type of the elements of source.</typeparam>
		/// <param name="source">The source.</param>
		/// <param name="count">The number of elements to return.</param>
		/// <param name="predicate">The predicate.</param>
		/// <returns>
		/// The specified number of contiguous elements if the predicate is true; otherwise the original source.
		/// </returns>
		public static IEnumerable<T> TakeIf<T>(this IEnumerable<T> source, int count, Func<IEnumerable<T>, bool> predicate)
		{
			return TakeIf(source, count, predicate(source));
		}

		/// <summary>
		/// Returns a specified number of contiguous elements from the start of a sequence if the predicate is true.
		/// </summary>
		/// <typeparam name="T">The type of the elements of source.</typeparam>
		/// <param name="source">The source.</param>
		/// <param name="count">The number of elements to return.</param>
		/// <param name="predicate">The predicate.</param>
		/// <returns>
		/// The specified number of contiguous elements if the predicate is true; otherwise the original source.
		/// </returns>
		public static IEnumerable<T> TakeIf<T>(this IEnumerable<T> source, int count, bool predicate)
		{
			return predicate ? source.Take(count) : source;
		}

		#endregion

		#region Nullify

		/// <summary>
		/// Returns <value>NULL</value> if the <see cref="System.Collections.IEnumerable"/> instance is <value>NULL</value> or empty; otherwise returns the original <see cref="System.Collections.IEnumerable"/>.
		/// </summary>
		/// <param name="enumerable">A <see cref="System.Collections.IEnumerable"/> instance to nullify.</param>
		/// <returns>Nullified <see cref="System.Collections.IEnumerable"/>.</returns>
		public static T Nullify<T>(this T enumerable)
			where T : IEnumerable
		{
			if (enumerable != null && !enumerable.GetEnumerator().MoveNext())
			{
				enumerable = default(T);
			}

			return enumerable;
		}

		#endregion

		#region Shuffle and TakeRandom

		/// <summary>
		/// Shuffles the IEnumerable.
		/// </summary>
		/// <typeparam name="T">The type of objects to enumerate.</typeparam>
		/// <param name="source">The source to shuffle.</param>
		/// <returns>The shuffled IEnumerable.</returns>
		public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
		{
			T[] array = source.ToArray();
			return ShuffleInternal(array, array.Length);
		}

		/// <summary>
		/// Randomly takes the specified amount of items.
		/// </summary>
		/// <typeparam name="T">The type of objects to enumerate.</typeparam>
		/// <param name="source">The source to shuffle.</param>
		/// <param name="count">The amount of items to return.</param>
		/// <returns>The shuffled IEnumerable.</returns>
		public static IEnumerable<T> TakeRandom<T>(this IEnumerable<T> source, int count)
		{
			T[] array = source.ToArray();
			return ShuffleInternal(array, System.Math.Min(count, array.Length)).Take(count);
		}

		/// <summary>
		/// Shuffles the IEnumerable.
		/// </summary>
		/// <typeparam name="T">The type of objects to enumerate.</typeparam>
		/// <param name="array">The array to shuffle.</param>
		/// <param name="count">The amount of items to return.</param>
		/// <returns>The shuffled IEnumerable.</returns>
		private static IEnumerable<T> ShuffleInternal<T>(T[] array, int count)
		{
			for (int n = 0; n < count; n++)
			{
				int k = RandomUtil.Next(n, array.Length);
				T temp = array[n];
				array[n] = array[k];
				array[k] = temp;
			}

			return array;
		}

		#endregion

		#region

		/// <summary>
		/// Returns a random element of a sequence, and throws an exception if there is no element in the sequence.
		/// </summary>
		/// <typeparam name="TSource">The type of the elements of source.</typeparam>
		/// <param name="source">The IEnumerable{T} to return the random element of.</param>
		/// <returns>
		/// A random element in the specified sequence.
		/// </returns>
		public static TSource Random<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw new ArgumentNullException("source");

			TSource[] array = source.ToArray();
			if (array.Length == 0) throw new InvalidOperationException("The source sequence is empty.");

			return RandomOrDefault<TSource>(array);
		}

		/// <summary>
		/// Returns a random element of a sequence, or a default value if the sequence contains no elements.
		/// </summary>
		/// <typeparam name="TSource">The type of the elements of source.</typeparam>
		/// <param name="source">The IEnumerable{T} to return the random element of.</param>
		/// <returns>
		/// default(TSource) if source is empty; otherwise, a random element in the specified sequence.
		/// </returns>
		public static TSource RandomOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw new ArgumentNullException("source");

			TSource[] array = source.ToArray();

			TSource random;
			if (array.Length == 0) random = default(TSource);
			else random = array[RandomUtil.NextInt(0, array.Length)];

			return random;
		}

		#endregion

		#region Shift

		/// <summary>
		/// Shifts the IEnumerable.
		/// </summary>
		/// <example>
		/// var unshifted = IEnumerable{int}.Range(1, 10); // 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
		/// unshifted.Shift(0); // ArgumentOutOfRangeException
		/// unshifted.Shift(1); // 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
		/// unshifted.Shift(2); // 1, 3, 5, 7, 9, 2, 4, 6, 8, 10
		/// unshifted.Shift(3); // 1, 4, 7, 10, 2, 5, 8, 3, 6, 9
		/// unshifted.Shift(4); // 1, 5, 9, 2, 6, 10, 3, 7, 4, 8
		/// unshifted.Shift(5); // 1, 6, 2, 7, 3, 8, 4, 9, 5, 10
		/// unshifted.Shift(6); // 1, 7, 2, 8, 3, 9, 4, 10, 5, 6
		/// unshifted.Shift(7); // 1, 8, 2, 9, 3, 10, 4, 5, 6, 7
		/// unshifted.Shift(8); // 1, 9, 2, 10, 3, 4, 5, 6, 7, 8
		/// unshifted.Shift(9); // 1, 10, 2, 3, 4, 5, 6, 7, 8, 9
		/// unshifted.Shift(10); // 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
		/// unshifted.Shift(11); // ArgumentOutOfRangeException
		/// </example>
		/// <typeparam name="T">The type of object to enumerate.</typeparam>
		/// <param name="source">The source to shift.</param>
		/// <param name="size">The size of the shift, must be greater than 0 and equal or less than the source length.</param>
		/// <returns>The shifted IEnumerable.</returns>
		public static IEnumerable<T> Shift<T>(this IEnumerable<T> source, int size)
		{
			// Validate source
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}

			// Convert source to array (for indexed access)
			T[] array = source.ToArray();

			// Validate size
			if (size < 1 || size > array.Length)
			{
				throw new ArgumentOutOfRangeException("size", "Must be greater than 0 and equal or less than the source length.");
			}

			// Shift the array
			for (int shift = 0; shift < size; shift++)
			{
				for (int i = shift; i < array.Length; i += size)
				{
					yield return array[i];
				}
			}

			yield break;
		}

		#endregion

		#region WithIndex

		/// <summary>
		/// Adds an index to the enumeration.
		/// </summary>
		/// <typeparam name="T">The type of the item.</typeparam>
		/// <param name="input">The input.</param>
		/// <returns>
		/// The enumeration with indexes.
		/// </returns>
		public static IEnumerable<IndexedItem<T>> WithIndex<T>(this IEnumerable<T> input)
		{
			int i = 0;
			foreach (T item in input) yield return new IndexedItem<T>(i++, item);
		}

		#endregion
	}

	/// <summary>
	/// Represents an indexed item.
	/// </summary>
	/// <typeparam name="T">The type of the item.</typeparam>
	public class IndexedItem<T>
	{
		#region Fields

		/// <summary>
		/// The index.
		/// </summary>
		private readonly int index;

		/// <summary>
		/// The item.
		/// </summary>
		private readonly T item;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the index.
		/// </summary>
		public int Index
		{
			get
			{
				return this.index;
			}
		}

		/// <summary>
		/// Gets the item.
		/// </summary>
		public T Item
		{
			get
			{
				return this.item;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="IndexedItem{T}"/> class.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <param name="item">The item.</param>
		public IndexedItem(int index, T item)
		{
			this.index = index;
			this.item = item;
		}

		#endregion

		#region Operators

		/// <summary>
		/// Performs an implicit conversion from <see cref="IndexedItem{T}"/> to the specified type.
		/// </summary>
		/// <param name="indexedItem">The indexed item.</param>
		/// <returns>
		/// The result of the conversion.
		/// </returns>
		public static implicit operator T(IndexedItem<T> indexedItem)
		{
			return indexedItem.Item;
		}

		#endregion
	}
}
