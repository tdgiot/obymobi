using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace Dionysos.Reflection
{
    /// <summary>
    /// Class which contains information about an assembly
    /// </summary>
    public class AssemblyInfo
    {
        #region Fields

        private System.Reflection.Assembly assembly = null;
        private string alias = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the AssemblyInfo class using the specified 
        /// System.Reflection.Assembly instance and the specified alias
        /// </summary>	
        public AssemblyInfo(Assembly assembly, string alias)
        {
            if (Instance.ArgumentIsEmpty(assembly, "assembly"))
            {
                // name is empty
            }
            else if (Instance.ArgumentIsEmpty(alias, "alias"))
            {
                // alias is empty
            }
            else
            {
                this.assembly = assembly;
                this.alias = alias;
            }
        }

        /// <summary>
        /// Constructs an instance of the AssemblyInfo class using the specified assembly file and alias
        /// </summary>	
        public AssemblyInfo(string assemblyFile, string alias)
        {
            if (Instance.ArgumentIsEmpty(assemblyFile, "assemblyFile"))
            {
                // Parameter 'assemblyFile' is empty
            }
            else if (!File.Exists(assemblyFile))
            {
                // Assembly file does not exists
                throw new FileNotFoundException("File does not exist!", assemblyFile);
            }
            else if (Instance.ArgumentIsEmpty(alias, "alias"))
            {
                // Parameter 'alias' is empty
            }
            else
            {
                this.assembly = Assembly.LoadFrom(assemblyFile);
                this.alias = alias;
            }
        }

        #endregion
        
        #region Properties

        /// <summary>
        /// Gets the assembly corresponding to this Lynx_media.Reflection.AssemblyInfo instance
        /// </summary>
        public Assembly Assembly
        {
            get
            {
                return this.assembly;
            }
        }

        /// <summary>
        /// Gets the full name of the assembly
        /// </summary>
        public string Name
        {
            get
            {
                return this.assembly.FullName;
            }
        }

        /// <summary>
        /// Gets the location of the assembly on disk
        /// </summary>
        public string Location
        {
            get
            {
                return this.assembly.Location;
            }
        }

        /// <summary>
        /// Gets the alias of the assembly 
        /// </summary>
        public string Alias
        {
            get
            {
                return this.alias;
            }
        }

        #endregion
    }
}
