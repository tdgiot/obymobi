using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Collections;

namespace Dionysos
{
    /// <summary>
    /// Static class used for invoking objects, members and methods
    /// </summary>
    public class InstanceFactory
    {
        /// <summary>
        /// Static instance of the Invoker type
        /// </summary>
        static InstanceFactory instance = null;

        /// <summary>
        /// Constructs a static instance of the Invoker type
        /// </summary>
        static InstanceFactory()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new InstanceFactory();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        /// <summary>
        /// Initializes an instance of the Invoker type 
        /// </summary>
        public void Init()
        {
        }

        #region Invoke object methods

        //#region InvokeEntity methods

        ///// <summary>
        ///// Invokes an instance of an EntityBase object
        ///// </summary>
        ///// <param name="assemblyFileName">The filename of the assembly which contains the Entity type</param>
        ///// <param name="entityTypeName">The string representation of the Entity type</param>
        ///// <returns>An EntityBase object</returns>
        //public static EntityBase InvokeEntity(string assemblyFileName, string entityTypeName)
        //{
        //    EntityBase oEntity = null;

        //    if (assemblyFileName == string.Empty || assemblyFileName.Length < 1)
        //    {
        //        throw new X_EmptyException("Assembly filename is empty!\r\n\r\nAssembly filename: " + assemblyFileName);
        //    }
        //    else if (entityTypeName == string.Empty || entityTypeName.Length < 1)
        //    {
        //        throw new X_EmptyException("Entity type is empty!\r\n\r\nEntity type: " + entityTypeName);
        //    }
        //    else
        //    {
        //        object oEntityObject = InvokeObject(assemblyFileName, entityTypeName);

        //        if (oEntityObject != null)
        //        {
        //            oEntity = (EntityBase)oEntityObject;
        //        }
        //        else
        //        {
        //            throw new X_EmptyException("Entity object is empty!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nEntity type: " + entityTypeName);
        //        }
        //    }

        //    return oEntity;
        //}

        ///// <summary>
        ///// Invokes an instance of an EntityBase object
        ///// </summary>
        ///// <param name="assemblyFileName">The filename of the assembly which contains the Entity type</param>
        ///// <param name="entityTypeName">The string representation of the Entity type</param>
        ///// <param name="args">Parameters which can be used to invoke an Entity type. This is usually a primary key value</param>
        ///// <returns>An EntityBase object</returns>
        //public static EntityBase InvokeEntity(string assemblyFileName, string entityTypeName, params object[] args)
        //{
        //    EntityBase oEntity = null;

        //    if (assemblyFileName == string.Empty || assemblyFileName.Length < 1)
        //    {
        //        throw new X_EmptyException("Assembly filename is empty!\r\n\r\nAssembly filename: " + assemblyFileName);
        //    }
        //    else if (entityTypeName == string.Empty || entityTypeName.Length < 1)
        //    {
        //        throw new X_EmptyException("Entity type is empty!\r\n\r\nEntity type: " + entityTypeName);
        //    }
        //    else
        //    {
        //        object oEntityObject = InvokeObject(assemblyFileName, entityTypeName, args);

        //        if (oEntityObject != null)
        //        {
        //            oEntity = (EntityBase)oEntityObject;
        //        }
        //        else
        //        {
        //            throw new X_EmptyException("Entity object is empty!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nEntity type: " + entityTypeName);
        //        }
        //    }

        //    return oEntity;
        //}

        //#endregion

        //#region InvokeEntityCollection methods

        ///// <summary>
        ///// Invokes an instance of an EntityCollectionBase object
        ///// </summary>
        ///// <param name="assemblyFileName">The filename of the assembly which contains the EntityCollection type</param>
        ///// <param name="entityCollectionTypeName">The string representation of the EntityCollection type</param>
        ///// <returns>An EntityCollectionBase object</returns>
        //public static EntityCollectionBase InvokeEntityCollection(string assemblyFileName, string entityCollectionTypeName)
        //{
        //    EntityCollectionBase oEntityCollection = null;

        //    if (assemblyFileName == string.Empty || assemblyFileName.Length < 1)
        //    {
        //        throw new X_EmptyException("Assembly filename is empty!\r\n\r\nAssembly filename: " + assemblyFileName);
        //    }
        //    else if (entityCollectionTypeName == string.Empty || entityCollectionTypeName.Length < 1)
        //    {
        //        throw new X_EmptyException("Entity collection type is empty!\r\n\r\nEntity collection type: " + entityCollectionTypeName);
        //    }
        //    else
        //    {
        //        object oEntityCollectionObject = InvokeObject(assemblyFileName, entityCollectionTypeName);

        //        if (oEntityCollectionObject != null)
        //        {
        //            oEntityCollection = (EntityCollectionBase)oEntityCollectionObject;
        //        }
        //        else
        //        {
        //            throw new X_EmptyException("EntityCollection object is empty!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nEntity collection type: " + entityCollectionTypeName);
        //        }
        //    }

        //    return oEntityCollection;
        //}

        //#endregion

        //#region InvokeControl methods

        ///// <summary>
        ///// Invokes an instance of a control
        ///// </summary>
        ///// <param name="assemblyFileName">The filename of the assembly which contains the Control type</param>
        ///// <param name="typeName">The string representation of the Control type</param>
        ///// <returns>A Control object</returns>
        //public static Control InvokeControl(string assemblyFileName, string typeName)
        //{
        //    Control oControl = null;

        //    if (assemblyFileName == string.Empty || assemblyFileName.Length < 1)
        //    {
        //        throw new X_EmptyException("Assembly filename is empty!\r\n\r\nAssembly filename: " + assemblyFileName);
        //    }
        //    else if (typeName == string.Empty || typeName.Length < 1)
        //    {
        //        throw new X_EmptyException("Type name is empty!\r\n\r\nType name: " + typeName);
        //    }
        //    else
        //    {
        //        if (File.Exists(assemblyFileName))
        //        {
        //            Assembly oAssembly = Assembly.LoadFrom(assemblyFileName);

        //            if (oAssembly != null)
        //            {
        //                Type oType = oAssembly.GetType(typeName);
        //                if (oType != null)
        //                {
        //                    try
        //                    {
        //                        oControl = (Control)Activator.CreateInstance(oType);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        throw ex;

        //                        //#if (DEBUG)
        //                        //    MessageBox.Show("An error occurred while invoking the control!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                        //#else
        //                        //    X_UtilityLog.WriteLine("An error occurred while invoking the control!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName);
        //                        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    #if (DEBUG)
        //                        MessageBox.Show("The type could not be retrieved from the assembly!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                    #else
        //                        X_UtilityLog.WriteLine("The type could not be retrieved from the assembly!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName);
        //                    #endif
        //                }
        //            }
        //            else
        //            {
        //                #if (DEBUG)
        //                    MessageBox.Show("Assembly could be not loaded!\r\n\r\nAssembly filename: " + assemblyFileName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                #else
        //                    X_UtilityLog.WriteLine("Assembly could not be loaded!\r\n\r\nAssembly filename: " + assemblyFileName);
        //                #endif
        //            }
        //        }
        //        else
        //        {
        //            #if (DEBUG)
        //                MessageBox.Show("Assembly file does not exist!\r\n\r\nAssembly filename: " + assemblyFileName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            #else
        //                X_UtilityLog.WriteLine("Assembly file does not exist!\r\n\r\nAssembly filename: " + assemblyFileName);
        //            #endif
        //        }
        //    }

        //    return oControl;
        //}

        //#endregion

        #region CreateInstance methods

        /// <summary>
        /// Creates an instance of an object 
        /// </summary>
        /// <param name="assembly">The System.Reflection.Assembly instance which contains the type</param>
        /// <param name="type">The System.Type of the instance to invoke</param>
        /// <returns>An Object</returns>
        public static object CreateInstance(System.Reflection.Assembly assembly, System.Type type)
        {
            object instance = null;

            if (Instance.ArgumentIsEmpty(assembly, "assembly"))
            {
                // Assembly is empty
            }
            else if (Instance.ArgumentIsEmpty(type, "type"))
            {
                // Type is empty
            }
            else
            {
                instance = Activator.CreateInstance(type);
            }

            return instance;
        }

        /// <summary>
        /// Creates an instance of an object
        /// </summary>
        /// <param name="assembly">The System.Reflection.Assembly instance which contains the Object type</param>
        /// <param name="type">The System.Type of the instance to create</param>
        /// <param name="args">Parameters which can be used to invoke an Object</param>
        /// <returns>An Object</returns>
        public static object CreateInstance(System.Reflection.Assembly assembly, System.Type type, params object[] args)
        {
            object instance = null;

            if (Instance.ArgumentIsEmpty(assembly, "assembly"))
            {
                // assembly is empty
            }
            else if (Instance.ArgumentIsEmpty(type, "type"))
            {
                // Type is empty
            }
            else
            {
                instance = Activator.CreateInstance(type, args);
            }

            return instance;
        }

        #endregion

        //#region Invoke method methods

        ///// <summary>
        ///// Invokes a method of an object
        ///// </summary>
        ///// <param name="assemblyFileName">The filename of the assembly which contains the type to invoke the method on</param>
        ///// <param name="typeName">The string representation of the type to invoke the method on</param>
        ///// <param name="methodName">The name of the method which has to be invoked</param>
        ///// <param name="sender">The sender</param>
        ///// <param name="parameters">Parameters which can be used to pass to a method</param>
        //public static void InvokeMethod(string assemblyFileName, string typeName, string methodName, object sender, object[] parameters )
        //{
        //    if (assemblyFileName == string.Empty || assemblyFileName.Length < 1)
        //    {
        //        throw new X_EmptyException("Assembly filename is empty!\r\n\r\nAssembly filename: " + assemblyFileName);
        //    }
        //    else if (typeName == string.Empty || typeName.Length < 1)
        //    {
        //        throw new X_EmptyException("Type name is empty!\r\n\r\nType name: " + typeName);
        //    }
        //    else if (methodName == string.Empty || methodName.Length < 1)
        //    {
        //        throw new X_EmptyException("Method name is empty!\r\n\r\nMethod name: " + methodName);
        //    }
        //    else
        //    {
        //        if (File.Exists(assemblyFileName))
        //        {
        //            Assembly oAssembly = Assembly.LoadFrom(assemblyFileName);

        //            if (oAssembly != null)
        //            {
        //                Type oType = oAssembly.GetType(typeName);
        //                if (oType != null)
        //                {
        //                    try
        //                    {
        //                        MethodInfo oMethod = oType.GetMethod(methodName);
        //                        if (oMethod != null)
        //                        {
        //                            oMethod.Invoke(sender, parameters);
        //                        }
        //                        else
        //                        {
        //                            #if (DEBUG)
        //                                MessageBox.Show("The supplied method is not found in methods of the type!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName + "\r\nMethod name: " + methodName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                            #else
        //                                X_UtilityLog.WriteLine("The supplied method is not found in methods of the type!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName + "\r\nMethod name: " + methodName);
        //                            #endif
        //                        }
        //                    }
        //                    catch
        //                    {
        //                        #if (DEBUG)
        //                            MessageBox.Show("An error occurred while invoking the method!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName + "\r\nMethod name: " + methodName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                        #else
        //                            X_UtilityLog.WriteLine("An error occurred while invoking the method!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName + "\r\nMethod name: " + methodName);
        //                        #endif
        //                    }
        //                }
        //                else
        //                {
        //                    #if (DEBUG)
        //                        MessageBox.Show("The type could not be retrieved from the assembly!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                    #else
        //                        X_UtilityLog.WriteLine("The type could not be retrieved from the assembly!\r\n\r\nAssembly filename: " + assemblyFileName + "\r\nType name: " + typeName);
        //                    #endif
        //                }
        //            }
        //            else
        //            {
        //                #if (DEBUG)
        //                    MessageBox.Show("Assembly could not be loaded!\r\n\r\nAssembly filename: " + assemblyFileName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                #else
        //                    X_UtilityLog.WriteLine("Assembly could not be loaded!\r\n\r\nAssembly filename: " + assemblyFileName);
        //                #endif
        //            }
        //        }
        //        else
        //        {
        //            #if (DEBUG)
        //                MessageBox.Show("Assembly file does not exist!\r\n\r\nAssembly filename: " + assemblyFileName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            #else
        //                X_UtilityLog.WriteLine("Assembly file does not exist!\r\n\r\nAssembly filename: " + assemblyFileName);
        //            #endif
        //        }
        //    }
        //}

        //#endregion

        #endregion

        //#region Invoke member methods

        ///// <summary>
        ///// Invokes a member of an object
        ///// </summary>
        ///// <param name="objectToInvokeMemberOn">The object which contains the member to invoke</param>
        ///// <param name="memberName">The name of the member to invoke</param>
        ///// <returns>An object which the member value</returns>
        //public static object InvokeMember(object objectToInvokeMemberOn, string memberName)
        //{
        //    object oMemberValue = null;

        //    if (objectToInvokeMemberOn == null)
        //    {
        //        throw new X_EmptyException("Object to invoke member on is empty!");
        //    }
        //    else if (memberName == string.Empty || memberName.Length < 1)
        //    {
        //        throw new X_EmptyException("Member name is empty!\r\n\r\nMember name: " + memberName);
        //    }
        //    else
        //    {
        //        try
        //        {
        //            Type oType = objectToInvokeMemberOn.GetType();
        //            if (oType != null)
        //            {
        //                PropertyInfo oPropertyInfo = (PropertyInfo)oType.GetMember(memberName).GetValue(0);
        //                oMemberValue = oPropertyInfo.GetValue(objectToInvokeMemberOn, null);
        //            }
        //            else
        //            {
        //                #if (DEBUG)
        //                    MessageBox.Show("The type could not be retrieved from the object to invoke member on!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                #else
        //                    X_UtilityLog.WriteLine("The type could not be retrieved from the object to invoke member on!");
        //                #endif
        //            }
        //        }
        //        catch
        //        {
        //            #if (DEBUG)
        //                MessageBox.Show("An error occurred while invoking the member!\r\n\r\nObject to invoke member on: " + objectToInvokeMemberOn.ToString() + "\r\nMember name: " + memberName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            #else
        //                X_UtilityLog.WriteLine("An error occurred while invoking the member!\r\n\r\nObject to invoke member on: " + objectToInvokeMemberOn.ToString() + "\r\nMember name: " + memberName);
        //            #endif
        //        }
        //    }
 
        //    return oMemberValue;
        //}

        ///// <summary>
        ///// Invokes a method of an object
        ///// </summary>
        ///// <param name="objectToInvokeMethodOn">The object which contains the method to invoke</param>
        ///// <param name="methodName">The name of the method to invoke</param>
        ///// <returns>The return value of the method which is being invoked</returns>
        //public static object InvokeMethod(object objectToInvokeMethodOn, string methodName)
        //{
        //    object oRetVal = null;

        //    if (objectToInvokeMethodOn == null)
        //    {
        //        throw new X_EmptyException("Object to invoke method on is empty!");
        //    }
        //    else if (methodName == string.Empty || methodName.Length < 1)
        //    {
        //        throw new X_EmptyException("Method name is empty!\r\n\r\nMethod name: " + methodName);
        //    }
        //    else
        //    {
        //        Type oType = objectToInvokeMethodOn.GetType();
        //        if (oType != null)
        //        {
        //            MethodInfo[] oMethods = oType.GetMethods();
        //            if (oMethods != null)
        //            {
        //                MethodInfo oMethod = null;

        //                for (int i = 0; i < oMethods.Length; i++)
        //                {
        //                    if (oMethods[i].ToString() == methodName)
        //                    {
        //                        oMethod = oMethods[i];
        //                        break;
        //                    }
        //                }

        //                if (oMethod != null)    // A method corresponding to the methodName parameter is found
        //                {
        //                    oRetVal = oMethod.Invoke(objectToInvokeMethodOn, null);
        //                }
        //                else // No method is found
        //                {
        //                    #if (DEBUG)
        //                        //MessageBox.Show("No corresponding method is found in the type!\r\n\r\nType name: " + oType.ToString() + "\r\nMethod name: " + methodName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                    #else
        //                        X_UtilityLog.WriteLine("No corresponding method is found in the type!\r\n\r\nType name: " + oType.ToString() + "\r\nMethod name: " + methodName);
        //                    #endif
        //                }
        //            }
        //            else
        //            {
        //                #if (DEBUG)
        //                    MessageBox.Show("The methods could not be retrieved from the type!\r\n\r\nType name: " + oType.ToString(), "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                #else
        //                    X_UtilityLog.WriteLine("The methods could not be retrieved from the type!\r\n\r\nType name: " + oType.ToString());
        //                #endif
        //            }
        //        }
        //        else
        //        {
        //            #if (DEBUG)
        //                MessageBox.Show("The type could not be retrieved from the object to invoke method on!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            #else
        //                X_UtilityLog.WriteLine("The type could not be retrieved from the object to invoke method on!");
        //            #endif
        //        }
        //    }

        //    return oRetVal;
        //}

        ///// <summary>
        ///// Invokes a method of an object
        ///// </summary>
        ///// <param name="objectToInvokeMethodOn">The object which contains the method to invoke</param>
        ///// <param name="methodName">The name of the method to invoke</param>
        ///// <param name="parameters">Parameters which can be passed to the method which is being invoked</param>
        ///// <returns>The return value of the method which is being invoked</returns>
        //public static object InvokeMethod(object objectToInvokeMethodOn, string methodName, object[] parameters)
        //{
        //    object oRetVal = null;

        //    if (objectToInvokeMethodOn == null)
        //    {
        //        throw new X_EmptyException("Object to invoke method on is empty!");
        //    }
        //    else if (methodName == string.Empty || methodName.Length < 1)
        //    {
        //        throw new X_EmptyException("Method name is empty!\r\n\r\nMethod name: " + methodName);
        //    }
        //    else
        //    {
        //        Type oType = objectToInvokeMethodOn.GetType();
        //        if (oType != null)
        //        {
        //            MethodInfo[] oMethods = oType.GetMethods();
        //            if (oMethods != null)
        //            {
        //                MethodInfo oMethod = null;

        //                for (int i = 0; i < oMethods.Length; i++)
        //                {
        //                    if (oMethods[i].ToString() == methodName)
        //                    {
        //                        oMethod = oMethods[i];
        //                        break;
        //                    }
        //                }

        //                if (oMethod != null)    // A method corresponding to the methodName parameter is found
        //                {
        //                    oRetVal = oMethod.Invoke(objectToInvokeMethodOn, parameters);
        //                }
        //                else // No method is found
        //                {
        //                    #if (DEBUG)
        //                        //MessageBox.Show("No corresponding method is found in the type!\r\n\r\nType name: " + oType.ToString() + "\r\nMethod name: " + methodName, "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                    #else
        //                        X_UtilityLog.WriteLine("No corresponding method is found in the type!\r\n\r\nType name: " + oType.ToString() + "\r\nMethod name: " + methodName);
        //                    #endif
        //                }
        //            }
        //            else
        //            {
        //                #if (DEBUG)
        //                    MessageBox.Show("The methods could not be retrieved from the type!\r\n\r\nType name: " + oType.ToString(), "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                #else
        //                    X_UtilityLog.WriteLine("The methods could not be retrieved from the type!\r\n\r\nType name: " + oType.ToString());
        //                #endif
        //            }
        //        }
        //        else
        //        {
        //            #if (DEBUG)
        //                MessageBox.Show("The type could not be retrieved from the object to invoke method on!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            #else
        //                X_UtilityLog.WriteLine("The type could not be retrieved from the object to invoke method on!");
        //            #endif
        //        }
        //    }

        //    return oRetVal;
        //}

        //#endregion
    }
}
