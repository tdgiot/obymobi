using System;
using System.Reflection;

namespace Dionysos.Reflection
{
	/// <summary>
	/// Class for actions on members of objects.
	/// </summary>
	public class Member
	{
		#region Member ethods

		/// <summary>
		/// Gets the members from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the members from</param>
		/// <returns>A System.String array containing the names of the members</returns>
		public static string[] GetMemberNames(object instance)
		{
			return GetMemberNames(instance, null);
		}

		/// <summary>
		/// Gets the members from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the members from</param>
		/// <param name="bindingFlags">Specified flags that control binding and the way in which the search for members and types is conducted by reflection</param>
		/// <returns>A System.String array containing the names of the members</returns>
		public static string[] GetMemberNames(object instance, BindingFlags? bindingFlags)
		{
			string[] members = null;

			MemberInfo[] memberInfo = GetMemberInfo(instance, bindingFlags);
			if (memberInfo == null)
			{
				throw new Dionysos.EmptyException("Variable 'memberInfo' is empty");
			}
			else
			{
				// Put the member names into the string array
				members = new string[memberInfo.Length];

				for (int i = 0; i < memberInfo.Length; i++)
				{
					members[i] = memberInfo[i].Name;
				}
			}

			return members;
		}

		/// <summary>
		/// Gets the member information from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the member information from</param>
		/// <param name="bindingFlags">Specified flags that control binding and the way in which the search for members and types is conducted by reflection</param>
		/// <returns>A System.String array containing the information of the members</returns>
		public static MemberInfo[] GetMemberInfo(object instance, BindingFlags? bindingFlags)
		{
			MemberInfo[] memberInfo = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					if (bindingFlags == null)
						memberInfo = type.GetMembers();
					else
						memberInfo = type.GetMembers(bindingFlags.Value);
				}
			}

			return memberInfo;
		}

		/// <summary>
		/// Checks whether a member exists on the specified instance
		/// according to the specified member name
		/// </summary>
		/// <param name="instance">The instance to check whether a member exists on</param>
		/// <param name="memberName">The name of the member</param>
		/// <returns>True if the instance has the member, False if not</returns>
		public static bool HasMember(object instance, string memberName)
		{
			bool hasMember = false;

			if (instance == null)
			{
				throw new TechnicalException("Instance == null, Requested member: {0}", memberName);
			}
			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				throw new TechnicalException("Instance == null, Requested member: {0}", memberName);
			}
			if (Instance.ArgumentIsEmpty(memberName, "memberName"))
			{
				throw new TechnicalException("Instance == {0}, Requested member: {1}", instance.GetType().FullName, memberName);
			}

            Type type = instance.GetType();
		    try
		    {
		        var memberInfo = type.GetMember(memberName);
                hasMember = memberInfo.Length > 0;
		    }
		    catch (Exception)
		    {
		        hasMember = false;
		    }

			return hasMember;
		}

	    public static object GetMemberValue(object instance, string memberName)
	    {
            if (instance == null)
            {
                throw new TechnicalException("Instance == null, Requested member: {0}", memberName);
            }
            if (Instance.ArgumentIsEmpty(instance, "instance"))
            {
                throw new TechnicalException("Instance == null, Requested member: {0}", memberName);
            }
            if (Instance.ArgumentIsEmpty(memberName, "memberName"))
            {
                throw new TechnicalException("Instance == {0}, Requested member: {1}", instance.GetType().FullName, memberName);
            }

            Type type = instance.GetType();
            try
            {
                MemberInfo[] memberInfo = type.GetMember(memberName);
                if (memberInfo.Length > 0)
                {
                    if (memberInfo[0].MemberType == MemberTypes.Field)
                    {
                        return ((FieldInfo)memberInfo[0]).GetValue(instance);
                    }
                    if (memberInfo[0].MemberType == MemberTypes.Property)
                    {
                        return ((PropertyInfo)memberInfo[0]).GetValue(instance, null);
                    }
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
	    }

		#endregion

		#region Property methods

		/// <summary>
		/// Gets the propertys from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the propertys from</param>
		/// <returns>A System.String array containing the names of the propertys</returns>
		public static string[] GetPropertyNames(object instance)
		{
			return GetPropertyNames(instance, null);
		}

		/// <summary>
		/// Gets the propertys from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the propertys from</param>
		/// <param name="bindingFlags">Specified flags that control binding and the way in which the search for propertys and types is conducted by reflection</param>
		/// <returns>A System.String array containing the names of the propertys</returns>
		public static string[] GetPropertyNames(object instance, BindingFlags? bindingFlags)
		{
			string[] properties = null;

			PropertyInfo[] propertyInfo = GetPropertyInfo(instance, bindingFlags);
			if (propertyInfo == null)
			{
				throw new Dionysos.EmptyException("Variable 'propertyInfo' is empty");
			}
			else
			{
				// Put the property names into the string array
				properties = new string[propertyInfo.Length];

				for (int i = 0; i < propertyInfo.Length; i++)
				{
					properties[i] = propertyInfo[i].Name;
				}
			}

			return properties;
		}

		/// <summary>
		/// Get the lowest property in the class hierarchy
		/// </summary>
		/// <param name="instance">Instance of the Object to fetch the property for</param>
		/// <param name="name">Name of the property</param>
		/// <returns></returns>
		public static PropertyInfo GetLowestProperty(object instance, string name)
		{
			Type type = instance.GetType();
			while (type != null)
			{
				var property = type.GetProperty(name, BindingFlags.DeclaredOnly |
													  BindingFlags.Public |
													  BindingFlags.Instance);
				if (property != null)
				{
					return property;
				}
				type = type.BaseType;
			}
			return null;
		}


		/// <summary>
		/// Gets the property information from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the property information from</param>
		/// <returns>A System.String array containing the information of the propertys</returns>
		public static PropertyInfo[] GetPropertyInfo(object instance)
		{
			return GetPropertyInfo(instance, null);
		}

		/// <summary>
		/// Gets the property information from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the property information from</param>
		/// <param name="bindingFlags">Specified flags that control binding and the way in which the search for propertys and types is conducted by reflection</param>
		/// <returns>A System.String array containing the information of the propertys</returns>
		public static PropertyInfo[] GetPropertyInfo(object instance, BindingFlags? bindingFlags)
		{
			PropertyInfo[] propertyInfo = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					if (bindingFlags == null)
						propertyInfo = type.GetProperties();
					else
						propertyInfo = type.GetProperties(bindingFlags.Value);
				}
			}

			return propertyInfo;
		}

		/// <summary>
		/// Checks whether a property exists on the specified instance
		/// according to the specified property name
		/// </summary>
		/// <param name="instance">The instance to check whether a property exists on</param>
		/// <param name="propertyName">The name of the property</param>
		/// <returns>True if the instance has the property, False if not</returns>
		public static bool HasProperty(object instance, string propertyName)
		{
			bool hasProperty = false;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(propertyName, "propertyName"))
			{
				// Parameter 'propertyName' is empty
			}
			else
			{
				string[] properties = GetPropertyNames(instance);
				for (int i = 0; i < properties.Length; i++)
				{
					string property = properties[i];
					if (property == propertyName)
					{
						hasProperty = true;
						break;
					}
				}
			}

			return hasProperty;
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <param name="value">The value to set to the property</param>
		/// <returns>A System.Object instance containing the value of the invoked property</returns>
		public static void SetProperty(object instance, string propertyName, object value)
		{
			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(propertyName, "propertyName"))
			{
				// Parameter 'propertyName' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					System.Reflection.MemberInfo[] memberInfo = type.GetMember(propertyName);
					if (Instance.Empty(memberInfo))
					{
						throw new Dionysos.EmptyException("Variable 'memberInfo' is empty voor property: {0} op type: {1}", propertyName, type.ToString());
					}
					else
					{
						object member = memberInfo.GetValue(0);
						if (Instance.Empty(member))
						{
							throw new Dionysos.EmptyException("Variable 'member' is empty");
						}
						else
						{
							System.Reflection.PropertyInfo propertyInfo = member as System.Reflection.PropertyInfo;
							if (Instance.Empty(propertyInfo))
							{
								throw new Dionysos.EmptyException("Variable 'propertyInfo' is empty");
							}
							else
							{
								try
								{
									// Are DEBUGGING and getting an error on the line below?
									// Er is iets mis met de opgevraagde property: 
									// Is de database structuur wel up-to-date? Zijn alle projecten al herbouwd? 
									// Er zit een probleem in het aanroepen van de property, probeer het anders handmatig om uit te vinden wat het probleem is!
									propertyInfo.SetValue(instance, value, null);
								}
								catch (Exception ex)
								{
									throw new Dionysos.TechnicalException("Er is iets mis met de opgevraagde property: Is de database structuur wel up-to-date? Zijn alle projecten al herbouwd? Er zit een probleem in het aanroepen van de property, probeer het anders handmatig om uit te vinden wat het probleem is!" + ex.ToString());
								}
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Object instance containing the value of the invoked property</returns>
		public static object InvokeProperty(object instance, string propertyName)
		{
			object value = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(propertyName, "propertyName"))
			{
				// Parameter 'propertyName' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					System.Reflection.MemberInfo[] memberInfo = type.GetMember(propertyName);
					if (Instance.Empty(memberInfo))
					{
						throw new Dionysos.EmptyException("Variable 'memberInfo' is empty voor property: {0} op type: {1}", propertyName, type.ToString());
					}
					else
					{
						object member = memberInfo.GetValue(0);
						if (Instance.Empty(member))
						{
							throw new Dionysos.EmptyException("Variable 'member' is empty");
						}
						else
						{
							System.Reflection.PropertyInfo propertyInfo = member as System.Reflection.PropertyInfo;
							if (Instance.Empty(propertyInfo))
							{
								throw new Dionysos.EmptyException("Variable 'propertyInfo' is empty");
							}
							else
							{
								try
								{
									// Are DEBUGGING and getting an error on the line below?
									// Er is iets mis met de opgevraagde property: 
									// Is de database structuur wel up-to-date? Zijn alle projecten al herbouwd? 
									// Er zit een probleem in het aanroepen van de property, probeer het anders handmatig om uit te vinden wat het probleem is!
									value = propertyInfo.GetValue(instance, null);
								}
								catch (Exception ex)
								{
									throw new Dionysos.TechnicalException("Er is iets mis met de opgevraagde property: Is de database structuur wel up-to-date? Zijn alle projecten al herbouwd? Er zit een probleem in het aanroepen van de property, probeer het anders handmatig om uit te vinden wat het probleem is!" + ex.ToString());
								}
							}
						}
					}
				}
			}

			return value;
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Boolean instance containing the value of the invoked property</returns>
		public static bool InvokePropertyAsBoolean(object instance, string propertyName)
		{
			return (bool)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Byte instance containing the value of the invoked property</returns>
		public static byte InvokePropertyAsByte(object instance, string propertyName)
		{
			return (byte)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Char instance containing the value of the invoked property</returns>
		public static char InvokePropertyAsChar(object instance, string propertyName)
		{
			return (char)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Decimal instance containing the value of the invoked property</returns>
		public static decimal InvokePropertyAsDecimal(object instance, string propertyName)
		{
			return (decimal)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Double instance containing the value of the invoked property</returns>
		public static double InvokePropertyAsDouble(object instance, string propertyName)
		{
			return (double)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Single instance containing the value of the invoked property</returns>
		public static float InvokePropertyAsFloat(object instance, string propertyName)
		{
			return (float)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Int32 instance containing the value of the invoked property</returns>
		public static int InvokePropertyAsInt(object instance, string propertyName)
		{
			return (int)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Long instance containing the value of the invoked property</returns>
		public static long InvokePropertyAsLong(object instance, string propertyName)
		{
			return (long)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.Int16 instance containing the value of the invoked property</returns>
		public static short InvokePropertyAsShort(object instance, string propertyName)
		{
			return (short)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.DateTime instance containing the value of the invoked property</returns>
		public static DateTime InvokePropertyAsDateTime(object instance, string propertyName)
		{
			return (DateTime)InvokeProperty(instance, propertyName);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.TimeStamp instance containing the value of the invoked property</returns>
		public static TimeStamp InvokePropertyAsTimeStamp(object instance, string propertyName)
		{
			return TimeStamp.Parse(InvokeProperty(instance, propertyName).ToString());
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <param name="enumType">The System.Type instance of the specified enumerator</param>
		/// <returns>A System.Int32 instance containing the value of the invoked property</returns>
		public static int InvokePropertyAsEnumValue(object instance, string propertyName, System.Type enumType)
		{
			Enum temp = (Enum)Enum.Parse(enumType, InvokePropertyAsString(instance, propertyName), true);
			return Convert.ToInt32(temp);
		}

		/// <summary>
		/// Invokes a property on the specified System.Object instance using the specified property name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the property on</param>
		/// <param name="propertyName">The System.String name of the property</param>
		/// <returns>A System.String instance containing the value of the invoked property</returns>
		public static string InvokePropertyAsString(object instance, string propertyName)
		{
			return Convert.ToString(InvokeProperty(instance, propertyName));
		}

		/// <summary>
		/// Gets the System.Type of a property from the specified System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the System.Type of the property from</param>
		/// <param name="propertyName">The System.String name of the property to get the System.Type from</param>
		/// <returns>A System.Type instance containing the type of the property</returns>
		public static System.Type GetPropertyType(object instance, string propertyName)
		{
			System.Type dataType = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(propertyName, "propertyName"))
			{
				// Parameter 'propertyName' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					System.Reflection.MemberInfo[] memberInfo = type.GetMember(propertyName);
					if (Instance.Empty(memberInfo))
					{
						throw new Dionysos.EmptyException("Variable 'memberInfo' is empty");
					}
					else
					{
						object member = memberInfo.GetValue(0);
						if (Instance.Empty(member))
						{
							throw new Dionysos.EmptyException("Variable 'member' is empty");
						}
						else
						{
							System.Reflection.PropertyInfo propertyInfo = member as System.Reflection.PropertyInfo;
							if (Instance.Empty(propertyInfo))
							{
								throw new Dionysos.EmptyException("Variable 'propertyInfo' is empty");
							}
							else
							{
								dataType = propertyInfo.PropertyType;
							}
						}
					}
				}
			}

			return dataType;
		}

		#endregion

		#region Method methods

		/// <summary>
		/// Gets the methods from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the methods from</param>
		/// <returns>A System.String array containing the names of the methods</returns>
		public static string[] GetMethodNames(object instance)
		{
			return GetMethodNames(instance, null);
		}

		/// <summary>
		/// Gets the methods from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the methods from</param>
		/// <param name="bindingFlags">Specified flags that control binding and the way in which the search for methods and types is conducted by reflection</param>
		/// <returns>A System.String array containing the names of the methods</returns>
		public static string[] GetMethodNames(object instance, BindingFlags? bindingFlags)
		{
			string[] methods = null;

			MethodInfo[] methodInfo = GetMethodInfo(instance, bindingFlags);
			if (methodInfo == null)
			{
				throw new Dionysos.EmptyException("Variable 'methodInfo' is empty");
			}
			else
			{
				// Put the method names into the string array
				methods = new string[methodInfo.Length];

				for (int i = 0; i < methodInfo.Length; i++)
				{
					methods[i] = methodInfo[i].Name;
				}
			}

			return methods;
		}

		/// <summary>
		/// Gets the method information from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the method information from</param>
		/// <param name="bindingFlags">Specified flags that control binding and the way in which the search for methods and types is conducted by reflection</param>
		/// <returns>A System.String array containing the information of the methods</returns>
		public static MethodInfo[] GetMethodInfo(object instance, BindingFlags? bindingFlags)
		{
			MethodInfo[] methodInfo = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					if (bindingFlags == null)
						methodInfo = type.GetMethods();
					else
						methodInfo = type.GetMethods(bindingFlags.Value);
				}
			}

			return methodInfo;
		}

		/// <summary>
		/// Checks whether a method exists on the specified instance
		/// according to the specified method name
		/// </summary>
		/// <param name="instance">The instance to check whether a method exists on</param>
		/// <param name="methodName">The name of the method</param>
		/// <returns>True if the instance has the method, False if not</returns>
		public static bool HasMethod(object instance, string methodName)
		{
			bool hasMethod = false;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(methodName, "methodName"))
			{
				// Parameter 'methodName' is empty
			}
			else
			{
				string[] methods = GetMethodNames(instance);
				for (int i = 0; i < methods.Length; i++)
				{
					string method = methods[i];
					if (method == methodName)
					{
						hasMethod = true;
						break;
					}
				}
			}

			return hasMethod;
		}

		/// <summary>
		/// Invokes a method on the specified System.Object instance using the specified method name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the method on</param>
		/// <param name="methodName">The System.String name of the method</param>
		/// <returns>A System.Object instance containing the return value of the invoked method</returns>
		public static object InvokeMethod(object instance, string methodName)
		{
			object value = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(methodName, "methodName"))
			{
				// Parameter 'methodName' is empty
			}
			else if (!Member.HasMember(instance, methodName))
			{
				throw new MissingMethodException(string.Format("Method '{0}' does not exist.", methodName));
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					System.Reflection.MethodInfo methodInfo = type.GetMethod(methodName);
					if (Instance.Empty(methodInfo))
					{
						throw new Dionysos.EmptyException("Variable 'methodInfo' is empty");
					}
					else
					{
						value = methodInfo.Invoke(instance, new object[] { });
					}
				}
			}

			return value;
		}

		/// <summary>
		/// Invokes a method on the specified System.Object instance using the specified method name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the method on</param>
		/// <param name="methodName">The System.String name of the method</param>
		/// <param name="parameters">A System.Object array containing the values of the parameters</param>
		/// <returns>A System.Object instance containing the return value of the invoked method</returns>
		public static object InvokeMethod(object instance, string methodName, object[] parameters)
		{
			object value = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(methodName, "methodName"))
			{
				// Parameter 'methodName' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					System.Reflection.MethodInfo methodInfo;
					if (parameters == null || parameters.Length == 0)
					{
						methodInfo = type.GetMethod(methodName, Type.EmptyTypes);
					}
					else
					{
						// GK Experimental
						// To call the correct overload, use the parameters types to get the overload
						methodInfo = null;
						try
						{
							Type[] types = new Type[parameters.Length];
							for (int i = 0; i < parameters.Length; i++)
							{
								types[i] = parameters[i].GetType();
							}

							if (types.Length == 0)
								methodInfo = type.GetMethod(methodName);
							else
								methodInfo = type.GetMethod(methodName, types);
						}
						catch
						{
							// Failover :)                            
						}

						if (methodInfo == null)
							methodInfo = type.GetMethod(methodName);
					}

					if (Instance.Empty(methodInfo))
					{
						throw new Dionysos.EmptyException("Variable 'methodInfo' is empty");

					}
					else
					{
						value = methodInfo.Invoke(instance, parameters);
					}
				}
			}

			return value;
		}

		/// <summary>
		/// Invokes a method on the specified System.Object instance using the specified method name
		/// </summary>
		/// <param name="instance">The System.Object instance to invoke the method on</param>
		/// <param name="methodName">The System.String name of the method</param>
		/// <param name="parameterTypes">A System.Type array containing the types of the parameters (in order!)</param>
		/// <param name="parameters">A System.Object array containing the values of the parameters</param>
		/// <returns>A System.Object instance containing the return value of the invoked method</returns>
		public static object InvokeMethod(object instance, string methodName, System.Type[] parameterTypes, object[] parameters)
		{
			object value = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(methodName, "methodName"))
			{
				// Parameter 'methodName' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					System.Reflection.MethodInfo methodInfo = type.GetMethod(methodName, parameterTypes);
					if (Instance.Empty(methodInfo))
					{
						throw new Dionysos.EmptyException("Variable 'methodInfo' is empty");
					}
					else
					{
						value = methodInfo.Invoke(instance, parameters);
					}
				}
			}

			return value;
		}

		#endregion

		#region Event methods

		/// <summary>
		/// Gets the events from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the events from</param>
		/// <returns>A System.String array containing the names of the events</returns>
		public static string[] GetEventNames(object instance)
		{
			return GetEventNames(instance, null);
		}

		/// <summary>
		/// Gets the events from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the events from</param>
		/// <param name="bindingFlags">Specified flags that control binding and the way in which the search for events and types is conducted by reflection</param>
		/// <returns>A System.String array containing the names of the events</returns>
		public static string[] GetEventNames(object instance, BindingFlags? bindingFlags)
		{
			string[] events = null;

			EventInfo[] eventInfo = GetEventInfo(instance, bindingFlags);
			if (eventInfo == null)
			{
				throw new Dionysos.EmptyException("Variable 'eventInfo' is empty");
			}
			else
			{
				// Put the event names into the string array
				events = new string[eventInfo.Length];

				for (int i = 0; i < eventInfo.Length; i++)
				{
					events[i] = eventInfo[i].Name;
				}
			}

			return events;
		}

		/// <summary>
		/// Gets the event information from a System.Object instance
		/// </summary>
		/// <param name="instance">The System.Object instance to get the event information from</param>
		/// <param name="bindingFlags">Specified flags that control binding and the way in which the search for events and types is conducted by reflection</param>
		/// <returns>A System.String array containing the information of the events</returns>
		public static EventInfo[] GetEventInfo(object instance, BindingFlags? bindingFlags)
		{
			EventInfo[] eventInfo = null;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else
			{
				System.Type type = instance.GetType();
				if (Instance.Empty(type))
				{
					throw new Dionysos.EmptyException("Variable 'type' is empty");
				}
				else
				{
					if (bindingFlags == null)
						eventInfo = type.GetEvents();
					else
						eventInfo = type.GetEvents(bindingFlags.Value);
				}
			}

			return eventInfo;
		}

		/// <summary>
		/// Checks whether a event exists on the specified instance
		/// according to the specified event name
		/// </summary>
		/// <param name="instance">The instance to check whether a event exists on</param>
		/// <param name="eventName">The name of the event</param>
		/// <returns>True if the instance has the event, False if not</returns>
		public static bool HasEvent(object instance, string eventName)
		{
			bool hasEvent = false;

			if (Instance.ArgumentIsEmpty(instance, "instance"))
			{
				// Parameter 'instance' is empty
			}
			else if (Instance.ArgumentIsEmpty(eventName, "eventName"))
			{
				// Parameter 'eventName' is empty
			}
			else
			{
				string[] events = GetEventNames(instance);
				for (int i = 0; i < events.Length; i++)
				{
					string eventt = events[i];
					if (eventt == eventName)
					{
						hasEvent = true;
						break;
					}
				}
			}

			return hasEvent;
		}

		#endregion

		#region Obsolete code

		///// <summary>
		///// Gets the members from a System.Object instance
		///// </summary>
		///// <param name="instance">The System.Object instance to get the members from</param>
		///// <returns>A System.String array containing the names of the members</returns>
		//[Obsolete("This method is obsolete. Use GetMemberNames(object instance) instead.")]
		//public static string[] GetMembers(object instance)
		//{
		//    return GetMemberNames(instance);
		//}

		///// <summary>
		///// Gets the members from a System.Object instance
		///// </summary>
		///// <param name="instance">The System.Object instance to get the members from</param>
		///// <returns>A System.String array containing the names of the members</returns>
		//[Obsolete("This method is obsolete. Use GetMembers(object instance, BindingFlags bindingFlags) instead.")]
		//public static string[] GetPublicMembers(object instance)
		//{
		//    return GetMemberNames(instance, BindingFlags.Public);
		//}

		#endregion
	}
}