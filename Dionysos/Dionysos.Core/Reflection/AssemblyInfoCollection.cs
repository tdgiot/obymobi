using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Dionysos.Reflection
{
	/// <summary>
	/// Collection class used for storing Lynx_media.Reflection.AssemblyInfo instances in
	/// </summary>
	public class AssemblyInfoCollection : ICollection<AssemblyInfo>
	{
		#region Fields

		private ArrayList items;
		private object threadingProblemLock = new object();

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the AssemblyInfoCollection class
		/// </summary>	
		public AssemblyInfoCollection()
		{
			this.items = new ArrayList();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds an Lynx_media.Reflection.AssemblyInfo instance to the collection
		/// </summary>
		/// <param name="assemblyInfo">The Lynx_media.Reflection.AssemblyInfo instance to add to the collection</param>
		public void Add(AssemblyInfo assemblyInfo)
		{
			try
			{
				lock (threadingProblemLock)
				{
					this.items.Add(assemblyInfo);
				}
			}
			catch (IndexOutOfRangeException exIoor)
			{
				//if (System.Environment.MachineName.ToUpper() == "XLSERVER" || System.Environment.MachineName.ToUpper() == "AMGAAB")
				//{
				// GK Fix/Workaround for XL Bedrijfskleding, maybe in the future more
				// Problem has to do with thread in-safety of adding to an ICollection object
				#region Enormous hack/fix for XL / XLSERVER Part Two
				lock (threadingProblemLock)
				{
					this.items = new ArrayList();
					this.items.Add(assemblyInfo);
				}
				// Write log
				//Exception objErr = HttpContext.Current.Server.GetLastError().GetBaseException();
				string err = "Error Caught in Application_Error event\r\n" +
						"\r\nError in: " + HttpContext.Current.Request.Url.ToString() + "\r\n" +
						"\r\nError Message: " + exIoor.Message.ToString() + "\r\n" +
					//"\r\nStack Trace: " + objErr.StackTrace.ToString() + "\r\n" +
						"\r\nReferer: " + HttpContext.Current.Request.UrlReferrer + "\r\n" +
						"\r\nIp Address: " + HttpContext.Current.Request.UserHostAddress + "\r\n" +
						"\r\nHostname: " + HttpContext.Current.Request.UserHostName + "\r\n" +
						"\r\nLanguages: " + HttpContext.Current.Request.UserLanguages + "\r\n" +
						"\r\nAgent: " + HttpContext.Current.Request.UserAgent;

				// Check for Errors folder
				string path = HttpContext.Current.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder));
				if (!System.IO.Directory.Exists(path))
				{
					System.IO.Directory.CreateDirectory(path);
				}

				// Create file
				string filePath = path + Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "XLFix[Add]Succes.log";
				System.IO.StreamWriter writer;
				writer = System.IO.File.CreateText(filePath);
				writer.WriteLine(err);
				writer.Close();

				#endregion
				//}
			}
			catch (Exception ex)
			{
				//if (System.Environment.MachineName.ToUpper() == "XLSERVER" || System.Environment.MachineName.ToUpper() == "AMGAAB")
				//{
				#region Fix not ran

				//Exception objErr = HttpContext.Current.Server.GetLastError().GetBaseException();
				string err = "Error Caught in Application_Error event\r\n" +
						"\r\nError in: " + HttpContext.Current.Request.Url.ToString() + "\r\n" +
						"\r\nError Message: " + ex.Message.ToString() + "\r\n" +
					//"\r\nStack Trace: " + objErr.StackTrace.ToString() + "\r\n" +
						"\r\nReferer: " + HttpContext.Current.Request.UrlReferrer + "\r\n" +
						"\r\nIp Address: " + HttpContext.Current.Request.UserHostAddress + "\r\n" +
						"\r\nHostname: " + HttpContext.Current.Request.UserHostName + "\r\n" +
						"\r\nLanguages: " + HttpContext.Current.Request.UserLanguages + "\r\n" +
						"\r\nAgent: " + HttpContext.Current.Request.UserAgent;

				// Check for Errors folder
				string path = HttpContext.Current.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder));
				if (!System.IO.Directory.Exists(path))
				{
					System.IO.Directory.CreateDirectory(path);
				}

				// Create file
				string filePath = path + Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "XLFix[Add]Failed.log";
				System.IO.StreamWriter writer;
				writer = System.IO.File.CreateText(filePath);
				writer.WriteLine(err);
				writer.Close();

				#endregion
				//}
			}
		}

		/// <summary>
		/// Adds an Lynx_media.Reflection.AssemblyInfo instance to the collection
		/// </summary>
		/// <param name="assembly">The System.Reflection.Assembly instance to add to the collection</param>
		/// <param name="alias">The System.String instance which represents the alias of the assembly</param>
		public void Add(System.Reflection.Assembly assembly, string alias)
		{
			try
			{
				lock (threadingProblemLock)
				{
					this.items.Add(new AssemblyInfo(assembly, alias));
				}
			}
			catch (IndexOutOfRangeException exIoor)
			{
				if (System.Environment.MachineName.ToUpper() == "XLSERVER" || System.Environment.MachineName.ToUpper() == "AMGAAB")
				{
					// GK Fix/Workaround for XL Bedrijfskleding, maybe in the future more
					// Problem has to do with thread in-safety of adding to an ICollection object
					#region Enormous hack/fix for XL / XLSERVER Part Two
					lock (threadingProblemLock)
					{
						this.items = new ArrayList();
						this.items.Add(new AssemblyInfo(assembly, alias));
					}
					// Write log
					//Exception objErr = HttpContext.Current.Server.GetLastError().GetBaseException();
					string err = "Error Caught in Application_Error event\r\n" +
							"\r\nError in: " + HttpContext.Current.Request.Url.ToString() + "\r\n" +
							"\r\nError Message: " + exIoor.Message.ToString() + "\r\n" +
						//"\r\nStack Trace: " + objErr.StackTrace.ToString() + "\r\n" +
							"\r\nReferer: " + HttpContext.Current.Request.UrlReferrer + "\r\n" +
							"\r\nIp Address: " + HttpContext.Current.Request.UserHostAddress + "\r\n" +
							"\r\nHostname: " + HttpContext.Current.Request.UserHostName + "\r\n" +
							"\r\nLanguages: " + HttpContext.Current.Request.UserLanguages + "\r\n" +
							"\r\nAgent: " + HttpContext.Current.Request.UserAgent;

					// Check for Errors folder
					string path = HttpContext.Current.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder));
					if (!System.IO.Directory.Exists(path))
					{
						System.IO.Directory.CreateDirectory(path);
					}

					// Create file
					string filePath = path + Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "XLFix[Add]Succes.log";
					System.IO.StreamWriter writer;
					writer = System.IO.File.CreateText(filePath);
					writer.WriteLine(err);
					writer.Close();

					#endregion
				}
			}
			catch (Exception ex)
			{
				if (System.Environment.MachineName.ToUpper() == "XLSERVER" || System.Environment.MachineName.ToUpper() == "AMGAAB")
				{
					#region Fix not ran

					//Exception objErr = HttpContext.Current.Server.GetLastError().GetBaseException();
					string err = "Error Caught in Application_Error event\r\n" +
							"\r\nError in: " + HttpContext.Current.Request.Url.ToString() + "\r\n" +
							"\r\nError Message: " + ex.Message.ToString() + "\r\n" +
						//"\r\nStack Trace: " + objErr.StackTrace.ToString() + "\r\n" +
							"\r\nReferer: " + HttpContext.Current.Request.UrlReferrer + "\r\n" +
							"\r\nIp Address: " + HttpContext.Current.Request.UserHostAddress + "\r\n" +
							"\r\nHostname: " + HttpContext.Current.Request.UserHostName + "\r\n" +
							"\r\nLanguages: " + HttpContext.Current.Request.UserLanguages + "\r\n" +
							"\r\nAgent: " + HttpContext.Current.Request.UserAgent;

					// Check for Errors folder
					string path = HttpContext.Current.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder));
					if (!System.IO.Directory.Exists(path))
					{
						System.IO.Directory.CreateDirectory(path);
					}

					// Create file
					string filePath = path + Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "XLFix[Add]Failed.log";
					System.IO.StreamWriter writer;
					writer = System.IO.File.CreateText(filePath);
					writer.WriteLine(err);
					writer.Close();

					#endregion
				}
			}
		}

		/// <summary>
		/// Adds an Lynx_media.Reflection.AssemblyInfo instance to the collection
		/// </summary>
		/// <param name="assemblyFile">The path to the assembly file</param>
		/// <param name="alias">The System.String instance which represents the alias of the assembly</param>
		public void Add(string assemblyFile, string alias)
		{
			try
			{
				lock (threadingProblemLock)
				{
					this.items.Add(new AssemblyInfo(assemblyFile, alias));
				}
			}
			catch (IndexOutOfRangeException exIoor)
			{
				if (System.Environment.MachineName.ToUpper() == "XLSERVER" || System.Environment.MachineName.ToUpper() == "AMGAAB")
				{
					// GK Fix/Workaround for XL Bedrijfskleding, maybe in the future more
					// Problem has to do with thread in-safety of adding to an ICollection object
					#region Enormous hack/fix for XL / XLSERVER Part Two
					lock (threadingProblemLock)
					{
						this.items = new ArrayList();
						Dionysos.Global.AssemblyInfo.Add(assemblyFile, alias);
					}
					// Write log
					//Exception objErr = HttpContext.Current.Server.GetLastError().GetBaseException();
					string err = "Error Caught in Application_Error event\r\n" +
							"\r\nError in: " + HttpContext.Current.Request.Url.ToString() + "\r\n" +
							"\r\nError Message: " + exIoor.Message.ToString() + "\r\n" +
						//"\r\nStack Trace: " + objErr.StackTrace.ToString() + "\r\n" +
							"\r\nReferer: " + HttpContext.Current.Request.UrlReferrer + "\r\n" +
							"\r\nIp Address: " + HttpContext.Current.Request.UserHostAddress + "\r\n" +
							"\r\nHostname: " + HttpContext.Current.Request.UserHostName + "\r\n" +
							"\r\nLanguages: " + HttpContext.Current.Request.UserLanguages + "\r\n" +
							"\r\nAgent: " + HttpContext.Current.Request.UserAgent;

					// Check for Errors folder
					string path = HttpContext.Current.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder));
					if (!System.IO.Directory.Exists(path))
					{
						System.IO.Directory.CreateDirectory(path);
					}

					// Create file
					string filePath = path + Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "XLFix[Add]Succes.log";
					System.IO.StreamWriter writer;
					writer = System.IO.File.CreateText(filePath);
					writer.WriteLine(err);
					writer.Close();

					#endregion
				}
			}
			catch (Exception ex)
			{
				if (System.Environment.MachineName.ToUpper() == "XLSERVER" || System.Environment.MachineName.ToUpper() == "AMGAAB")
				{
					#region Fix not ran

					//Exception objErr = HttpContext.Current.Server.GetLastError().GetBaseException();
					string err = "Error Caught in Application_Error event\r\n" +
							"\r\nError in: " + HttpContext.Current.Request.Url.ToString() + "\r\n" +
							"\r\nError Message: " + ex.Message.ToString() + "\r\n" +
						//"\r\nStack Trace: " + objErr.StackTrace.ToString() + "\r\n" +
							"\r\nReferer: " + HttpContext.Current.Request.UrlReferrer + "\r\n" +
							"\r\nIp Address: " + HttpContext.Current.Request.UserHostAddress + "\r\n" +
							"\r\nHostname: " + HttpContext.Current.Request.UserHostName + "\r\n" +
							"\r\nLanguages: " + HttpContext.Current.Request.UserLanguages + "\r\n" +
							"\r\nAgent: " + HttpContext.Current.Request.UserAgent;

					// Check for Errors folder
					string path = HttpContext.Current.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder));
					if (!System.IO.Directory.Exists(path))
					{
						System.IO.Directory.CreateDirectory(path);
					}

					// Create file
					string filePath = path + Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "XLFix[Add]Failed.log";
					System.IO.StreamWriter writer;
					writer = System.IO.File.CreateText(filePath);
					writer.WriteLine(err);
					writer.Close();

					#endregion
				}
			}
		}

		/// <summary>
		/// Clears all the items in the collection
		/// </summary>
		public void Clear()
		{
			this.items.Clear();
		}

		/// <summary>
		/// Checks whether the specified Lynx_media.Reflection.AssemblyInfo instance is already in the collection
		/// </summary>
		/// <param name="assemblyInfo">The Lynx_media.Reflection.AssemblyInfo instance to check</param>
		/// <returns>True if the Lynx_media.Reflection.AssemblyInfo instance is in the collection, False if not</returns>
		public bool Contains(AssemblyInfo assemblyInfo)
		{
			bool contains = false;

			for (int i = 0; i < this.items.Count; i++)
			{
				if ((this.items[i] as AssemblyInfo) == assemblyInfo)
				{
					contains = true;
				}
			}

			return contains;
		}

		/// <summary>
		/// Copies the items from this collection to an array at the specified index
		/// </summary>
		/// <param name="array">The array to copy the items to</param>
		/// <param name="index">The index to copy the items at</param>
		public void CopyTo(AssemblyInfo[] array, int index)
		{
			this.items.CopyTo(array, index);
		}

		/// <summary>
		/// Removes the specified Lynx_media.Reflection.AssemblyInfo instance from this collection
		/// </summary>
		/// <param name="assemblyInfo">The Lynx_media.Reflection.AssemblyInfo instance to remove</param>
		public bool Remove(AssemblyInfo assemblyInfo)
		{
			this.items.Remove(assemblyInfo);
			return true;
		}

		/// <summary>
		/// Returns an enumerator for a range of elements in the Lynx-media.Reflection.AssemblyInfoCollection
		/// </summary>
		/// <returns>The enumerator instance</returns>
		public IEnumerator<AssemblyInfo> GetEnumerator()
		{
			return (IEnumerator<AssemblyInfo>)this.items.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for a range of elements in the Lynx-media.Reflection.AssemblyInfoCollection
		/// </summary>
		/// <returns>The enumerator instance</returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private AssemblyInfo GetAssemblyInfo(string alias)
		{
			AssemblyInfo assemblyInfo = null;
			try
			{
				for (int i = 0; i < this.items.Count; i++)
				{
					AssemblyInfo temp = this.items[i] as AssemblyInfo;
					if (temp == null)
					{
						throw new Exception("An empty AssemblyInfo was requested.");
					}
					else if (temp.Alias.ToUpper() == alias.ToUpper())
					{
						assemblyInfo = temp;
					}
				}
			}
			catch
			{ }
			finally
			{
				#region Enormous hack/fix for XL / XLSERVER
				if (assemblyInfo == null && (System.Environment.MachineName.ToUpper() == "XLSERVER" || System.Environment.MachineName.ToUpper() == "AMGAAB" ||
					System.Environment.MachineName.ToUpper() == "TMPSVR"))
				{
					try
					{
						this.items = new ArrayList();
						Dionysos.Global.AssemblyInfo.Add(HttpContext.Current.Server.MapPath("~/Bin/ExtraVestiging.Data.dll"), "Data");
						assemblyInfo = items[0] as AssemblyInfo;

						// Write log
						//Exception objErr = HttpContext.Current.Server.GetLastError().GetBaseException();
						string err = "Error Caught in Application_Error event\r\n" +
								"\r\nError in: " + HttpContext.Current.Request.Url.ToString() + "\r\n" +
							//"\r\nError Message: " + objErr.Message.ToString() + "\r\n" +
							//"\r\nStack Trace: " + objErr.StackTrace.ToString() + "\r\n" +
								"\r\nReferer: " + HttpContext.Current.Request.UrlReferrer + "\r\n" +
								"\r\nIp Address: " + HttpContext.Current.Request.UserHostAddress + "\r\n" +
								"\r\nHostname: " + HttpContext.Current.Request.UserHostName + "\r\n" +
								"\r\nLanguages: " + HttpContext.Current.Request.UserLanguages + "\r\n" +
								"\r\nAgent: " + HttpContext.Current.Request.UserAgent;

						// Check for Errors folder
						string path = HttpContext.Current.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder));
						if (!System.IO.Directory.Exists(path))
						{
							System.IO.Directory.CreateDirectory(path);
						}

						// Create file
						string filePath = path + Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "XLFixSucces.log";
						System.IO.StreamWriter writer;
						writer = System.IO.File.CreateText(filePath);
						writer.WriteLine(err);
						writer.Close();
					}
					catch
					{
						//Exception objErr = HttpContext.Current.Server.GetLastError().GetBaseException();
						string err = "Error Caught in Application_Error event\r\n" +
								"\r\nError in: " + HttpContext.Current.Request.Url.ToString() + "\r\n" +
							//"\r\nError Message: " + objErr.Message.ToString() + "\r\n" +
							//"\r\nStack Trace: " + objErr.StackTrace.ToString() + "\r\n" +
								"\r\nReferer: " + HttpContext.Current.Request.UrlReferrer + "\r\n" +
								"\r\nIp Address: " + HttpContext.Current.Request.UserHostAddress + "\r\n" +
								"\r\nHostname: " + HttpContext.Current.Request.UserHostName + "\r\n" +
								"\r\nLanguages: " + HttpContext.Current.Request.UserLanguages + "\r\n" +
								"\r\nAgent: " + HttpContext.Current.Request.UserAgent;

						// Check for Errors folder
						string path = HttpContext.Current.Server.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.ErrorFileFolder));
						if (!System.IO.Directory.Exists(path))
						{
							System.IO.Directory.CreateDirectory(path);
						}

						// Create file
						string filePath = path + Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now) + "XLFixFailed.log";
						System.IO.StreamWriter writer;
						writer = System.IO.File.CreateText(filePath);
						writer.WriteLine(err);
						writer.Close();
					}
				}
				#endregion
			}

			return assemblyInfo;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the amount of items in this collection
		/// </summary>
		public int Count
		{
			get
			{
				return this.items.Count;
			}
		}

		/// <summary>
		/// Boolean value indicating whether this collection is read only or not
		/// </summary>
		public bool IsReadOnly
		{
			get
			{
				return this.items.IsReadOnly;
			}
		}

		/// <summary>
		/// Gets an Lynx_media.Reflection.AssemblyInfo instance from the collection from the specified index
		/// </summary>
		/// <param name="index">The index of the Lynx_media.Reflection.AssemblyInfo instance to get</param>
		/// <returns>An Lynx_media.Reflection.AssemblyInfo instance</returns>
		public AssemblyInfo this[int index]
		{
			get
			{
				return this.items[index] as AssemblyInfo;
			}
		}

		/// <summary>
		/// Gets an Lynx_media.Reflection.AssemblyInfo instance from the collection from the specified index
		/// </summary>
		/// <param name="alias">The alias of the Lynx_media.Reflection.AssemblyInfo instance to get</param>
		/// <returns>An Lynx_media.Reflection.AssemblyInfo instance</returns>
		public AssemblyInfo this[string alias]
		{
			get
			{
				return GetAssemblyInfo(alias);
			}
		}

		#endregion
	}
}
