﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dionysos.Reflection
{
	/// <summary>
	/// Extension methods for assemblies.
	/// </summary>
	public static class AssemblyExtensions
	{
		/// <summary>
		/// Gets the loadable types.
		/// </summary>
		/// <param name="assembly">The assembly.</param>
		/// <returns>
		/// The loadable types.
		/// </returns>
		public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
		{
			if (assembly == null) throw new ArgumentNullException("assembly");

			try
			{
				return assembly.GetTypes();
			}
			catch (ReflectionTypeLoadException e)
			{
				return e.Types.Where(t => t != null);
			}
		}

		/// <summary>
		/// Loads the types.
		/// </summary>
		/// <typeparam name="T">The type to load.</typeparam>
		/// <param name="types">The types.</param>
		/// <returns>
		/// The loaded types.
		/// </returns>
		public static IEnumerable<T> LoadTypes<T>(this IEnumerable<Type> types)
		{
			Type type = typeof(T);

			return types
				.Where(t => t.IsPublic && t.IsClass && !t.IsAbstract)
				.Where(t => type.IsAssignableFrom(t))
				.Select(t => (T)Activator.CreateInstance(t));
		}
	}
}
