﻿using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
//using Microsoft.Web.Administration;
//using Microsoft.Web.Management.Server;

namespace Dionysos.IIS
{
    /// <summary>
    /// Low level system stuff
    /// </summary>
    public class ApplicationPoolUtil
    {
        /// <summary>
        /// Get all application pools on this computer
        /// </summary>
        /// <param name="computerName">Full computer name</param>
        /// <param name="username">Optional Username</param>
        /// <param name="password">Optional Password</param>
        /// <returns></returns>
        public static List<string> GetApplicationPools(string computerName, string username = "", string password = "")
        {
            var path = @"IIS://" + computerName + "/W3SVC/AppPools";
            DirectoryEntry root = (username.Length == 0) ? new DirectoryEntry(path) : new DirectoryEntry(path, username, password, AuthenticationTypes.Secure);

            var items = new List<string>();
            foreach (DirectoryEntry entry in root.Children)
            {
                items.Add(entry.Name);
            }
            return items;
        }

        /*/// <summary>
        /// Gets the application pool collection from the server.
        /// </summary>
        /// <returns></returns>
        [ModuleServiceMethod(PassThrough = true)]
        public static List<ApplicationPool> GetApplicationPoolCollection()
        {
            // Use an ArrayList to transfer objects to the client.
            var arrayOfApplicationPools = new List<ApplicationPool>();
            try
            {
                ServerManager serverManager = new ServerManager();
                ApplicationPoolCollection applicationPoolCollection = serverManager.ApplicationPools;
                foreach (ApplicationPool applicationPool in applicationPoolCollection)
                {
                    arrayOfApplicationPools.Add(applicationPool);
                }
            }
            catch
            {
                if (System.TestUtil.IsPcDeveloper && !System.TestUtil.IsPcGabriel)
                {
                    //throw;
                }
            }
            return arrayOfApplicationPools;
        }*/

        /*/// <summary>
        /// Gets an application pool by name.
        /// </summary>
        /// <param name="name">Name of the ApplicationPool</param>
        /// <param name="applicationPool">Return object</param>
        /// <returns>ApplicationPool object, or null if it could not be found</returns>
        [ModuleServiceMethod(PassThrough = true)]
        public static bool GetApplicationPoolByName(string name, out ApplicationPool applicationPool)
        {
            applicationPool = null;

            var serverManager = new ServerManager();
            ApplicationPoolCollection applicationPoolCollection = serverManager.ApplicationPools;
            foreach (ApplicationPool appPool in applicationPoolCollection)
            {
                if (appPool.Name.Equals(name))
                {
                    applicationPool = appPool;
                    break;
                }
            }

            return (applicationPool != null);
        }*/
    }
}
