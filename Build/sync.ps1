Write-Host "=== Extracting database scheme from master database ===";
& "C:\Program Files\Microsoft SQL Server\150\DAC\bin\SqlPackage.exe" `
    /Action:Extract `
    /SourceConnectionString:$env:Master_ConnectionString `
    /TargetFile:"source.dacpac" `
    /p:IgnoreUserLoginMappings=True `
    /p:ExtractReferencedServerScopedElements=False;

Write-Host "=== Publishing database schema to $env:Release_DefinitionName ===";
& "C:\Program Files\Microsoft SQL Server\150\DAC\bin\SqlPackage.exe" `
    /Action:Publish `
    /SourceFile:source.dacpac `
    /TargetConnectionString:$env:Default_ConnectionString `
    /p:IgnoreRoleMembership=True `
    /p:IgnoreUserSettingsObjects=True `
    /p:ExcludeObjectType=Users `
	/p:GenerateSmartDefaults=True `
	/p:ScriptDatabaseOptions=False `

Write-Host "=== Removeing DACPAC file ===";
Remove-Item "source.dacpac";
