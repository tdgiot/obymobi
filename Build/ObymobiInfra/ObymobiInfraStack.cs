using System.Collections.Generic;
using Amazon.CDK;
using Amazon.CDK.AWS.ElasticBeanstalk;
using Constructs;
using Crave.Infrastructure;
using Crave.Infrastructure.CDK.Constructs;
using Crave.Infrastructure.Enums;
using Crave.Infrastructure.Models;
using ObymobiInfra.Builders;
using ObymobiInfra.Extensions;

namespace ObymobiInfra
{
    public class ObymobiInfraStack : Stack
    {
        internal ObymobiInfraStack(Construct scope, string id, CraveEnvironment craveEnvironment, IStackProps props = null) : base(scope, id, props)
        {
            CreateElasticBeanstalkInstance("cms", craveEnvironment.DomainNames.External, BuildCmsProps(craveEnvironment.Environment));
            CreateElasticBeanstalkInstance("noc", craveEnvironment.DomainNames.External, BuildNocProps(craveEnvironment.Environment));
            CreateElasticBeanstalkInstance("obymobi-api", craveEnvironment.DomainNames.External, BuildApiProps(craveEnvironment.Environment));
            CreateElasticBeanstalkInstance("obymobi-messaging", craveEnvironment.DomainNames.External, BuildMessagingProps(craveEnvironment.Environment));
            CreateElasticBeanstalkInstance("services", craveEnvironment.DomainNames.External, BuildServicesProps(craveEnvironment.Environment));
        }

        private void CreateElasticBeanstalkInstance(string recordName, string domainName, ElasticBeanstalk.ElasticBeanstalkProps props) =>
            new ElasticBeanstalk(this, $"eb-{props.ApplicationName}", props)
                .AddARecord(recordName, domainName);

        private static ElasticBeanstalkPropsBuilder DefaultProperties(EnvironmentType environmentType) => new ElasticBeanstalkPropsBuilder()
            .WithSolutionStackName("64bit Windows Server 2019 v2.10.3 running IIS 10.0")
            .WithAutoScalingMinSize(1)
            .WithAutoScalingMaxSize(1)
            .WithInstanceTypes("t3.small")
            .WithType(ElasticBeanstalk.ElasticBeanstalkType.WebTier)
            .WithSslCertificateArn(GetSslCertificateArnForEnvironment(environmentType))
            .WithAdditionalConfigurationOptions(new List<CfnEnvironment.OptionSettingProperty>
            {
                CreateSettingProperty("aws:elasticbeanstalk:healthreporting:system", "SystemType", "enhanced")
            });

        private static ElasticBeanstalk.ElasticBeanstalkProps BuildCmsProps(EnvironmentType environment) => DefaultProperties(environment)
            .WithApplicationName("Obymobi-Cms")
            .WithPublishedArtifactsFolder("artifacts/Obymobi.Cms.WebApplication.zip")
            .WithHealthEndpoint("/signon.aspx")
            .WithCnamePrefix($"cms-{environment}".ToLower())
            .WithInstanceTypes(environment == EnvironmentType.Production ? "t3.medium" : "t3.small")
            .Build();

        private static ElasticBeanstalk.ElasticBeanstalkProps BuildNocProps(EnvironmentType environment) => DefaultProperties(environment)
            .WithApplicationName("Obymobi-Noc")
            .WithPublishedArtifactsFolder("artifacts/Obymobi.Noc.WebApplication.zip")
            .WithHealthEndpoint("/signon.aspx")
            .WithCnamePrefix($"noc-{environment}".ToLower())
            .WithInstanceTypes(environment == EnvironmentType.Production ? "t3.medium" : "t3.small")
            .Build();

        private static ElasticBeanstalk.ElasticBeanstalkProps BuildApiProps(EnvironmentType environment) => DefaultProperties(environment)
            .WithApplicationName("Obymobi-Api")
            .WithPublishedArtifactsFolder("artifacts/Obymobi.Api.WebApplication.zip")
            .WithHealthEndpoint("/Test.html")
            .WithCnamePrefix($"obymobi-api-{environment}".ToLower())
            .WithInstanceTypes("t3.small")
            .WithAutoScalingMinSize(environment == EnvironmentType.Production ? 2 : 1)
            .WithAutoScalingMaxSize(environment == EnvironmentType.Production ? 2 : 1)
            .Build();

        private static ElasticBeanstalk.ElasticBeanstalkProps BuildMessagingProps(EnvironmentType environment) => DefaultProperties(environment)
            .WithApplicationName("Obymobi-Messaging")
            .WithPublishedArtifactsFolder("artifacts/Obymobi.Messaging.WebApplication.zip")
            .WithHealthEndpoint("/default.aspx")
            .WithCnamePrefix($"obymobi-messaging-{environment}".ToLower())
            .WithInstanceTypes(environment == EnvironmentType.Production ? "t3.medium" : "t3.small")
            .Build();

        private static ElasticBeanstalk.ElasticBeanstalkProps BuildServicesProps(EnvironmentType environment) => DefaultProperties(environment)
            .WithApplicationName("Obymobi-Services")
            .WithPublishedArtifactsFolder("artifacts/Obymobi.Services.WebApplication.zip")
            .WithHealthEndpoint("/default.aspx")
            .WithCnamePrefix($"obymobi-services-{environment}".ToLower())
            .Build();

        private static string GetSslCertificateArnForEnvironment(EnvironmentType environmentType)
        {
            try
            {
                return CraveCertificates.GetCraveCloudXyzCertificateArn(environmentType);
            }
            catch
            {
                return string.Empty;
            }
        }

        private static CfnEnvironment.OptionSettingProperty CreateSettingProperty(string optionNamespace, string optionName, string value)
        {
            return new CfnEnvironment.OptionSettingProperty
            {
                Namespace = optionNamespace,
                OptionName = optionName,
                Value = value
            };
        }
    }
}
