﻿using Amazon.CDK;
using Crave.Infrastructure;
using Crave.Infrastructure.Models;

namespace ObymobiInfra
{
    sealed class Program
    {
        public static void Main(string[] args)
        {
            CraveEnvironment craveEnvironment = CraveEnvironments.GetDeploymentEnvironment();

            App app = new App();

            ObymobiInfraStack stack = new ObymobiInfraStack(app, "ObymobiInfraStack", craveEnvironment, new StackProps
            {
                Env = new Environment
                {
                    Account = craveEnvironment.AccountId,
                    Region = craveEnvironment.Region
                }
            });

            Tags.Of(stack).Add(CraveTags.ProjectId, "obymobi");
            Tags.Of(stack).Add(CraveTags.EnvironmentType, craveEnvironment.Environment.ToString());

            app.Synth();
        }
    }
}
