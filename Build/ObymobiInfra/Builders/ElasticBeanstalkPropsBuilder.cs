﻿using System.Collections.Generic;
using Amazon.CDK.AWS.ElasticBeanstalk;
using Crave.Infrastructure.CDK.Constructs;

namespace ObymobiInfra.Builders
{
    public class ElasticBeanstalkPropsBuilder
    {
        private readonly ElasticBeanstalk.ElasticBeanstalkProps props = new ElasticBeanstalk.ElasticBeanstalkProps();

        public ElasticBeanstalkPropsBuilder WithApplicationName(string applicationName)
        {
            props.ApplicationName = applicationName;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithPublishedArtifactsFolder(string publishedArtifactsFolder)
        {
            props.PublishedArtifactsFolder = publishedArtifactsFolder;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithHealthEndpoint(string healthEndpoint)
        {
            props.HealthEndpoint = healthEndpoint;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithCnamePrefix(string cnamePrefix)
        {
            props.CnamePrefix = cnamePrefix;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithSolutionStackName(string solutionStackName)
        {
            props.SolutionStackName = solutionStackName;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithAutoScalingMinSize(int autoScalingMinSize)
        {
            props.AutoScalingMinSize = autoScalingMinSize;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithAutoScalingMaxSize(int autoScalingMaxSize)
        {
            props.AutoScalingMaxSize = autoScalingMaxSize;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithInstanceTypes(string instanceTypes)
        {
            props.InstanceTypes = instanceTypes;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithType(ElasticBeanstalk.ElasticBeanstalkType elasticBeanstalkType)
        {
            props.Type = elasticBeanstalkType;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithSslCertificateArn(string sslCertificateArn)
        {
            props.SslCertificateArn = sslCertificateArn;
            return this;
        }

        public ElasticBeanstalkPropsBuilder WithAdditionalConfigurationOptions(List<CfnEnvironment.OptionSettingProperty> additionalConfigurationOptions)
        {
            props.AdditionalConfigurationOptions = additionalConfigurationOptions;
            return this;
        }

        public ElasticBeanstalk.ElasticBeanstalkProps Build() => new ElasticBeanstalk.ElasticBeanstalkProps
        {
            ApplicationName = props.ApplicationName,
            PublishedArtifactsFolder = props.PublishedArtifactsFolder,
            HealthEndpoint = props.HealthEndpoint,
            CnamePrefix = props.CnamePrefix,
            SolutionStackName = props.SolutionStackName,
            AutoScalingMinSize = props.AutoScalingMinSize,
            AutoScalingMaxSize = props.AutoScalingMaxSize,
            InstanceTypes = props.InstanceTypes,
            Type = props.Type,
            SslCertificateArn = props.SslCertificateArn,
            AdditionalConfigurationOptions = props.AdditionalConfigurationOptions
        };
    }
}
